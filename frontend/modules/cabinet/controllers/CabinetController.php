<?php namespace app\modules\cabinet\controllers;

/**************************************/
/*                                    */
/*        CABINET CONTROLLER          */
/*                                    */
/**************************************/

use Yii;

use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

use yii\web\Controller;

use kartik\mpdf\Pdf;

use common\models\Companies;
use common\models\Containers;
use common\models\ContainersRelation;
use common\models\Files;
use common\models\Invoices;
use common\models\Orders;
use common\models\OrdersSearch;
use common\models\Settings;
use common\models\Taxonomies;
use common\models\Tracking;
use common\models\UploadForm;
use common\models\Wagons;
use common\models\WagonsRelation;

/**
 * Cabinet Controller for the `cabinet` module
**/
class CabinetController extends Controller
{

	/**
	 * @inheritdoc
	**/
    public $layout = 'cabinet';

    /**
     * @inheritdoc
    **/
    public function behaviors()
    {

        return [

			'access' => [

				'class' => AccessControl::className(),
				'rules' => [
					 [
						'allow' => true,
						'roles' => [
							'client'
						],
						'actions' => [
							'orders',
							'order-update',
							'order-invoice',
							'upload'
						]
					]
				],

				'denyCallback' => function( $rule, $action ) {
					\Yii::$app->response->redirect( [
						'/'
					] );
				}

			],

            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'ajax-change-guestnames' => ['POST'],
                ],
            ]

        ];

    }

	/**
	 * Lists all Orders models.
	 * @return mixed
	**/
	public function actionOrders()
	{

		//params initializations
		$searchModel  = new OrdersSearch();
		$dataProvider = $searchModel->search( ArrayHelper::merge(
			\Yii::$app->request->queryParams,
			[
				$searchModel->formName() =>  [
					'user_id' => \Yii::$app->user->id
				]
			]
		) );

		return $this->render( 'order/index', [
			'searchModel'  => $searchModel,
			'dataProvider' => $dataProvider,
			'tracking'	   => ! empty( $dataProvider->models )
				? array_reduce( Tracking::find()
					-> where( [
						'foreign_id' => ArrayHelper::getColumn(
							$dataProvider->models, 'id'
						),
						'type' => 'order'
					] )
					-> all(),
					function( $filter, $tracking ) {

						if ( ! empty( $tracking ) )
							$filter[ $tracking->foreign_id ][] = [
								'name' 	  => $tracking->name,
								'arrival' => [
									'day'  => date( 'd', strtotime( $tracking->arrival ) ),
									'week' => date( 'l', strtotime( $tracking->arrival ) ),
									'full' => date( 'F Y', strtotime( $tracking->arrival ) ),
									'date' => date( 'd.m.Y', strtotime( $tracking->arrival ) )
								],
								'departure' => [
									'day'  => date( 'd', strtotime( $tracking->departure ) ),
									'week' => date( 'l', strtotime( $tracking->departure ) ),
									'full' => date( 'F Y', strtotime( $tracking->departure ) ),
									'date' => date( 'd.m.Y', strtotime( $tracking->departure ) )
								],
								'status' => $tracking->status
							];

						return $filter;

					}, [] )
				: [],
			'statuses' => Settings::find()
				-> where( [
					'name' => [
						'confirm_option',
						'invoice_option'
					]
				] )
				-> indexBy( 'name' )
				-> asArray()
				-> all()
		] );

	}

	/**
	 * Updates an existing Orders model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	**/
    public function actionOrderUpdate( $id = null )
    {

    	//param initializations
		$model   = Orders::findOne( $id );
		$request = \Yii::$app->request;

		//load requested details
		if ( $model->load( $request->post() ) ) {

			//order updates
			if ( $model->save( false ) ) {

				Yii::$app->session->setFlash('success',
					__( 'Order was updated successfully' )
				);

			}

		}

		return $this->redirect( [
			'cabinet/orders'
		] );

    }

	/**
	 * Download Invoice For Requested Order.
	 * @param integer $id
	 * @return mixed
	**/
	public function actionOrderInvoice( $id )
	{

		//param initialization
		$model = Orders::findOne(
			$id
		);

		//is order exists
		if ( is_null( $model ) ) {

			return $this->redirect( [
				'cabinet/orders'
			] );

		}

		//filling current model with additional details
		$model->fillModel();

		//the list of ids
		$options = ! is_null( $model->containers_type )
		&& ! empty( $model->containers_type )
			? ContainersRelation::find()
				-> where( [
					'foreign_id' => $id,
					'type' 	     => 'order'
				] )
				-> asArray()
				-> all()
			: WagonsRelation::find()
				-> where( [
					'foreign_id' => $id,
					'type' 	     => 'order'
				] )
				-> asArray()
				-> all();

		//invoice details
		$invoice = Invoices::find()
			-> where( [
				'foreign_id' => $id,
				'type' 	     => 'Lotus'
			] )
			-> orderBy( [
				'id' => SORT_DESC
			] )
			-> one();

		//generate pdf from html
		$pdf = new Pdf( [
			// set to use core fonts only
			'mode' => Pdf::MODE_UTF8,
			// A4 paper format
			'format' => Pdf::FORMAT_A4,
			// portrait orientation
			'orientation' => Pdf::ORIENT_PORTRAIT ,
			// stream to browser inline
			'destination' => Pdf::DEST_BROWSER,
			// html content input
			'content' => $this->renderPartial( 'order/_pdf', [
				'model'   	     => $model,
				'transportation' => ! is_null( $model->containers_type ) && ! empty( $model->containers_type )
					? Containers::findAll( [
						'id' => ArrayHelper::getColumn(
							$options, 'container_id'
						),
						'isDeleted' => false
					] )
					: Wagons::findAll( [
						'id' => ArrayHelper::getColumn(
							$options, 'wagon_id'
						),
						'isDeleted' => false
					] ),
				'type' => Taxonomies::findOne( ! is_null( $model->containers_type ) && ! empty( $model->containers_type )
					? $model->containers_type
					: $model->wagons_type
				),
				'invoice' => $invoice,
				'company' => ! is_null( Yii::$app->user->identity->company_id )
					? Companies::findOne(
						Yii::$app->user->identity->company_id
					)
					: []
			] ),
			// set mPDF properties on the fly
			'options' => [
				'title' => __( 'InterRail Invoice' ) . ' - ' . $invoice->number
			]
		] );

		return $pdf->render();

	}

	/**
	 * Upload new file.
	 * If update is successful, action will return file url.
	 * @return mixed
	 **/
	public function actionUpload()
	{

		//param initialization
		$model = new UploadForm( [
			'path' => '@frontend/web/upload/users',
			'url'  => '@frontend_web/upload/users'
		] );

		if ( Yii::$app->request->isAjax ) {

			//change response type to json for ajax callback
			Yii::$app->response->format = 'json';

			//loading requested file details
			$model->load(
				Yii::$app->request->post()
			);

			//uploading requested file
			if ( $model->upload() ) {

				//updating file details
				$file = merge_objects( new Files(), [
					'name'  => uniqid(),
					'guide' => $model->getUrl(),
					'size'	=> (string)$model->image->size,
					'type'	=> 'users'
				] );

				//saving file
				if ( $file->save() ) {

					return [
						'id'  => $file->id,
						'url' => $file->guide
					];

				}

			} else {

				return [
					'return' => 'error'
				];

			}
		}

		return $this->redirect( [
			'cabinet'
		] );

	}

}