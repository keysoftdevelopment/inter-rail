<?php namespace app\modules\cabinet\controllers;

/**************************************/
/*                                    */
/*        CABINET CONTROLLER          */
/*                                    */
/**************************************/

use Yii;

use yii\filters\AccessControl;
use yii\web\Controller;

use common\models\User;

/**
 * Default controller for the `cabinet` module
**/
class DefaultController extends Controller
{

    /**
     * @inheritdoc
    **/
    public $layout = 'cabinet';

	/**
	 * @inheritdoc
	**/
	public function behaviors()
	{

		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
					]
				],

				'denyCallback' => function( $rule, $action ) {
					\Yii::$app->response->redirect( [
						'/'
					] );
				}

			]

		];

	}

    /**
     * Renders the index view for the module
     * @return mixed
	**/
    public function actionIndex()
    {

    	//params initializations
        $request = \Yii::$app->request;
        $model 	 = User::findOne(
        	\Yii::$app->user->id
		);

        //is post request
		if ( $request->isPost ) {

			//set customer update scenario
			$model->setScenario(
				$model::SCENARIO_UPDATE
			);

			//load requested details
			$model->load(
				\Yii::$app->request->post()
			);

			//set customer role
			$model->getRole();

			//update customer details
			if ( $model->save() ) {

				Yii::$app->session->setFlash( 'success',
					__( 'Your Profile was updated successfully' )
				);

			}

		}

        return $this->render( 'index', [
            'model' => $model
        ] );

    }

}