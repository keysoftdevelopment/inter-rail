<?php namespace app\modules\cabinet;

/**************************************/
/*                                    */
/*           CABINET MODULE           */
/*                                    */
/**************************************/

class Module extends \yii\base\Module
{

    /**
     * @inheritdoc
    **/
    public $controllerNamespace = 'app\modules\cabinet\controllers';

    /**
     * @inheritdoc
    **/
    public function init()
    {

        parent::init();

		//set layout
        $this->setLayoutPath(
        	'@frontend/themes/interrail/views/layouts'
		);

    }

}