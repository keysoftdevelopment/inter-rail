<?php namespace app\modules\articles\controllers;

/**************************************/
/*                                    */
/*        ARTICLE`s CONTROLLER        */
/*                                    */
/**************************************/

use Yii;

use yii\web\Controller;

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

use common\behaviors\TranslationBehavior;

use common\models\Files;
use common\models\Posts;
use common\models\PostsSearch;

/**
 * Default controller for the `article` module
**/
class DefaultController extends Controller
{

	/**
	 * @inheritdoc
	**/
	public $layout = 'article';

	/**
	 * @inheritdoc
	**/
	public function behaviors()
	{

		return [

			'access' => [
				'class' => AccessControl::className(),
				'only'  => [
					'index'
				],

				'rules' => [
					[
						'allow' => true,
						'roles' => [ '@' ]
					], [
						'actions' => [
							'index'
						],
						'allow' => true,
						'roles' => ['?']
					]
				],

				'denyCallback' => function( $rule, $action ) {
					\Yii::$app->response->redirect( [
						'/'
					] );
				}

			],

			'translation' => [
				'class' => TranslationBehavior::className()
			]

		];

	}

	/**
	 * Renders the index view for the module
	 * @return string
	 * @throws NotFoundHttpException
	**/
	public function actionIndex()
    {

		//param initialization
		$model   = \Yii::$app->common->currentPage( 'post' );
		$lang_id = Yii::$app->common->currentLanguage();

		//is requested page exists
		if ( empty( $model ) ) {

			throw new NotFoundHttpException(
				__( 'Page Not Found' )
			);

		}

		//model details
		$searchModel  = new PostsSearch();
		$dataProvider = $searchModel->search( ArrayHelper::merge(
			\Yii::$app->request->queryParams,
			[
				$searchModel->formName() => [
					'type' => 'posts'
				]
			]
		) );

		//set pagination
		$dataProvider->pagination->pageSize = Yii::$app->common->postsPerPage();

		//fill current page with additional information
		$model->customFields();

        return $this->render( 'index', [
            'dataProvider' => merge_objects( $dataProvider, [
            	'models' => $this->translates( $dataProvider->models, [
					'lang_id'   => $lang_id,
					'type'	    => 'posts',
					'is_single' => false
				] )
			] ),
			'searchModel'  => $searchModel,
			'page' 	       => $this->translates( $model, [
				'lang_id'   => $lang_id,
				'type'	    => 'page',
				'is_single' => true
			] ),
            'thumbnails'   => ! empty( $dataProvider->models )
				? Files::find()
					-> where( [
						'id' => ArrayHelper::getColumn(
							$dataProvider->models, 'file_id'
						)
					] )
					-> indexBy( 'id' )
					-> asArray()
					-> all()
            	: []
        ] );

    }

}