<?php namespace app\modules\articles\controllers;

/**************************************/
/*                                    */
/*       ARTICLE`s CONTROLLER         */
/*                                    */
/**************************************/

use Yii;

use yii\filters\AccessControl;
use yii\web\Controller;

use common\behaviors\TranslationBehavior;

use common\models\Posts;

/**
 * Articles controller for the `article` module
**/
class ArticlesController extends Controller
{

    /**
     * @inheritdoc
    **/
    public $layout = 'inner';

	/**
	 * @inheritdoc
	**/
	public function behaviors()
	{

		return [

			'access' => [
				'class' => AccessControl::className(),
				'only'  => [
					'single-article'
				],

				'rules' => [
					[
						'allow' => true,
						'roles' => [ '@' ]
					], [
						'actions' => [
							'single-article'
						],
						'allow' => true,
						'roles' => ['?']
					]
				],

				'denyCallback' => function( $rule, $action ) {
					\Yii::$app->response->redirect( [
						'/'
					] );
				}

			],

			'translation' => [
				'class' => TranslationBehavior::className()
			]

		];

	}

    /**
     * Renders the single article view for the module
     * @param string $slug
     * @return mixed
    **/
    public function actionSingleArticle( $slug )
    {

    	//param initialization
        $model = Posts::findOne( [
            'slug' => $slug
        ] );

        //is requested article exists
        if ( ! isset( $model->id ) ) {

			return $this->redirect(
				'articles'
			);

		}

		//fill current model with additional information
		$model->customFields();

        return $this->render( 'single-article', [
            'model' => $this->translates( $model, [
				'lang_id'   => Yii::$app->common->currentLanguage(),
				'type'	    => 'posts',
				'is_single' => true
			] )
        ] );

    }

}
