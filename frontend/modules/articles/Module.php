<?php namespace app\modules\articles;

/**************************************/
/*                                    */
/*          ARTICLE`s MODULE          */
/*                                    */
/**************************************/

class Module extends \yii\base\Module
{

    /**
     * @inheritdoc
    **/
    public $controllerNamespace = 'app\modules\articles\controllers';

    /**
     * @inheritdoc
    **/
    public function init()
    {

        parent::init();

        //set layout
        $this->setLayoutPath(
        	'@frontend/themes/interrail/views/layouts'
		);

    }

}