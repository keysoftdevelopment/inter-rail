<?php namespace app\modules\main\controllers;

/**************************************/
/*                                    */
/*        DEFAULT MAIN CONTROLLER     */
/*                                    */
/**************************************/


use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

use common\behaviors\TranslationBehavior;

use common\models\CustomFields;
use common\models\Posts;

/**
 * Default controller for the `main` module
**/
class DefaultController extends Controller
{

	/**
	 * @inheritdoc
	**/
	public $layout = 'inner';

	/**
	 * @inheritdoc
	**/
	public function behaviors()
	{

		return [

			'translation' => [
				'class' => TranslationBehavior::className()
			]

		];

	}

	/**
	 * Renders the index view for the module
	 * @return mixed
	 * @throws NotFoundHttpException
	**/
    public function actionIndex()
    {

    	//param initialization
        $model = \Yii::$app->common->currentPage( 'home' );

        //is page exists
        if ( empty( $model ) ) {

            throw new NotFoundHttpException(
            	__( 'Page Not Found' )
			);

        }

        //fill current model with additional information
		$model->customFields();

		//the list of services
		$services = $this->translates( Posts::find()
			-> where( [
				'type'      => 'service',
				'isDeleted' => false
			] )
			-> orderBy( [
				'created_at' => SORT_DESC
			] )
			-> all(), [
				'type'	    => 'service',
				'lang_id'   => \Yii::$app->common->currentLanguage(),
				'is_single' => false
			]
		);

        return $this->render( 'index', [
            'model' => $this->translates( $model, [
				'lang_id'   => \Yii::$app->common->currentLanguage(),
				'type'	    => 'page',
				'is_single' => true
			] ),
			'services' => [
				'details' => $services,
				'icons'   => CustomFields::find()
					-> where( [
						'name' 	     => 'icon',
						'foreign_id' => ArrayHelper::getColumn(
							$services, 'id'
						)
					] )
					-> indexBy( 'foreign_id' )
					-> asArray()
					-> all()
			]

        ] );

    }

}
