<?php namespace app\modules\main\controllers;

/**************************************/
/*                                    */
/*          MAIN CONTROLLER           */
/*                                    */
/**************************************/

use Yii;

use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

use common\behaviors\TranslationBehavior;

use common\models\CustomFields;
use common\models\Files;
use common\models\LogisticsRelation;
use common\models\Languages;
use common\models\LoginForm;
use common\models\Orders;
use common\models\Places;
use common\models\PlacesRelation;
use common\models\Posts;
use common\models\PostsSearch;
use common\models\Routes;
use common\models\Taxonomies;
use common\models\Tracking;
use common\models\UploadForm;
use common\models\User;

/**
 * Main controller for the `main` module
**/
class MainController extends Controller
{

    /**
     * @inheritdoc
    **/
    public $layout = 'inner';

	/**
	 * @inheritdoc
	**/
	public function behaviors()
	{

		return [

			'translation' => [
				'class' => TranslationBehavior::className()
			]

		];

	}

    /**
     * Renders the error view for the module
     * @return mixed
    **/
    public function actionError()
    {

    	//set layout
    	$this->layout = 'cabinet';

    	//is there any errors
        if ( Yii::$app->errorHandler->exception !== null )
            return $this->render( 'error', [
            	'exception' => Yii::$app->errorHandler->exception
			] );

		return $this->redirect(
			Yii::$app->getHomeUrl()
		);

    }

    /**
     * Action for ajax requests only, receive params and response data in json
     * @return mixed
    **/
    public function actionAjaxRequests()
    {

    	//param initialization
        $request = Yii::$app->request;

        //is ajax request
        if ( $request->isAjax ) {

			//change response type to json for ajax callback
			\Yii::$app->response->format = Response::FORMAT_JSON;

            if ( ! empty( $request->post( 'languageName' ) ) ) {

                //set cookies
                Yii::$app->common->setCookies(
                	'language', Languages::findOne( [
						'name' => $request->post( 'languageName' )
					] )->id, 'remove'
				);

                return $this->redirect(
                	Yii::$app->getHomeUrl()
				);

            } else if ( ! empty( $request->post( 'method' ) )
				&& $request->post( 'method' ) == 'locations'
			) {

            	//params initializations
            	$location = $request->post( 'locations' );
				$response = [
					'code'    => 200,
					'status'  => 'success',
					'message' => __( 'There is no details for current locations' )
				];

            	if ( ! empty( $location[ 'from' ] )
					&& ! empty( $location[ 'to' ] )
				) {

            		//places details due to requested details from and to
            		$places = Places::find()
						-> where( [
							'city' => [
								$location[ 'from' ],
								$location[ 'to' ]
							]
						] )
						-> indexBy( 'city' )
						-> asArray()
						-> all();

            		if ( ! empty( $places )
						&& isset( $places[ trim( $location[ 'from' ] ) ] )
						&& isset( $places[ trim( $location[ 'to' ] ) ] )
					) {

            			//route details due to requested locations
						$route = Routes::find()
							-> where( [
								'from' => $places[ trim( $location[ 'from' ] ) ][ 'id' ],
								'to'   => $places[ trim( $location[ 'to' ] ) ][ 'id' ]
							] )
							-> orWhere( [
								'and', [
									'from' => $places[ trim( $location[ 'to' ] ) ][ 'id' ]
								], [
									'to' => $places[ trim( $location[ 'from' ] ) ][ 'id' ]
								]
							] )
							-> one();

						if ( ! is_null( $route ) ) {

							//logistic details by route id
							$logistic_ids = LogisticsRelation::find()
								-> where( [
									'foreign_id' => $route->id,
									'type'		 => 'route'
								] )
								-> asArray()
								-> all();

							if ( ! empty( $logistic_ids ) ) {

								//places details
								$place_ids = PlacesRelation::find()
									-> where( [
										'foreign_id' => ArrayHelper::getColumn(
											$logistic_ids, 'logistic_id'
										)
									] )
									-> orderBy( [ new Expression( 'FIELD ( foreign_id, ' . implode( ", ", ArrayHelper::getColumn(
										$logistic_ids, 'logistic_id'
									) ) . ' )' ) ] )
									-> asArray()
									-> all();

								if ( ! empty( $place_ids ) ) {

									$response = [
										'code'     => 200,
										'status'   => 'success',
										'response' => array_reduce( Places::find()
											-> where( [
												'id' => ArrayHelper::getColumn(
													$place_ids, 'place_id'
												)
											] )
											-> orderBy( [ new Expression( 'FIELD ( id, ' . implode( ", ", ArrayHelper::getColumn(
												$place_ids, 'place_id'
											) ) . ' )' ) ] )
											-> asArray()
											-> all(), function( $filter, $place ) {

											if ( ! empty( $place[ 'type' ] ) )
												$filter[ $place[ 'type' ] ][] = $place;

											return $filter;

										}, [] ),
										'transportation_sum' => number_format( $route->rate ),
										'duration' 		     => number_format( $route->duration )
									];

								}

							}

						}

					}

				}

				return $response;

			} else if ( ! empty( $request->post( 'tracking_number' ) ) ) {

            	//param initialization
				$response = [
					'code'    => 200,
					'status'  => 'success',
					'message' => __( 'There is no order with such number' )
				];

				//requested order details
				$current_order = Orders::findOne( [
					'number' => $request->post( 'tracking_number' )
				] );

				if ( ! is_null( $current_order ) ) {

					//tracking details
					$tracking = Tracking::find()
						-> where( [
							'foreign_id' => $current_order->id
						] )
						-> asArray()
						-> all();

					$response = [
						'code' 	 => 200,
						'status' => 'success',
						'results' => [
							'id'                   => $current_order->id,
							'order_number'         => $current_order->number,
							'status'               => $current_order->status,
							'transportation_dates' => [
								'from' => ! empty( $tracking )
									? date( 'd.m.Y', strtotime( $tracking[ 0 ][ 'arrival' ] ) )
									: __( 'No information' ),
								'to'   => ! empty( $tracking )
									? date( 'd.m.Y', strtotime( $tracking[ end( array_keys( $tracking ) ) ][ 'arrival' ] ) )
									: __( 'No information' )
							],
							'notes'    => $current_order->notes,
							'tracking' => array_reduce( $tracking, function ( $filter, $trace ) {

								if ( ! empty( $trace ) )
									$filter[] = [
										'name' 	  => $trace[ 'name' ],
										'arrival' => [
											'day'  => date( 'd', strtotime( $trace[ 'arrival' ] ) ),
											'week' => date( 'l', strtotime( $trace[ 'arrival' ] ) ),
											'full' => date( 'F Y', strtotime( $trace[ 'arrival' ] ) ),
											'date' => date( 'd.m.Y', strtotime( $trace[ 'arrival' ] ) )
										],
										'departure' => [
											'day'  => date( 'd', strtotime( $trace[ 'departure' ] ) ),
											'week' => date( 'l', strtotime( $trace[ 'departure' ] ) ),
											'full' => date( 'F Y', strtotime( $trace[ 'departure' ] ) ),
											'date' => date( 'd.m.Y', strtotime( $trace[ 'departure' ] ) )
										],
										'status' => $trace[ 'status' ]
									];

								return $filter;

							}, [] )
						]
					];
				}

				return $response;

			}

        }

        return $this->redirect(
        	Yii::$app->getHomeUrl()
		);

    }

	/**
	 * Changing frontend language and save to user cookies current lang id
	 * @param int|string $lang
	 * @return mixed
	**/
	public function actionChangeLanguage( $lang )
	{

		//set cookie
		Yii::$app->common->setCookies(
			'language', $lang
		);

		return $this->redirect(
			Yii::$app->request->referrer
		);

	}

    /**
     * Renders the page view for the module
     * @param string $slug
     * @return mixed
	 * @throws NotFoundHttpException
    **/
    public function actionPage( $slug )
    {

    	//param initialization
        $model = Posts::findOne( [
			'type' => 'page',
			'slug' => str_replace( '_', '-', $slug )
		] );

        //is requested page exists
        if ( empty( $model ) ) {

			throw new NotFoundHttpException(
				__( 'Page Not Found' )
			);

		}

		//fill current model with additional information
		$model->customFields();

        return $this->render( 'pages', [
			'model' => $this->translates( $model, [
				'lang_id'   => Yii::$app->common->currentLanguage(),
				'type'	    => 'page',
				'is_single' => 'true'
			] )
        ] );

    }

	/**
	 * Renders the contact view for the module
	 * @return string
	 * @throws NotFoundHttpException
	**/
	public function actionContact()
	{

		//set layout
		$this->layout = 'contact';

		//current page due to template name
		$model = Yii::$app->common->currentPage( 'contact' );

		//is page template exists
		if ( empty( $model ) ) {

			throw new NotFoundHttpException(
				__( 'Page Not Found' )
			);

		}

		//fill current model with additional information
		$model->customFields();

		return $this->render( 'contact', [
			'model' => $this->translates( $model, [
				'lang_id'   => Yii::$app->common->currentLanguage(),
				'type'	    => 'page',
				'is_single' => 'true'
			] )
		] );

	}

	/**
	 * Renders the services view for the module
	 * @return string
	 * @throws NotFoundHttpException
	**/
	public function actionServices()
	{

		//param initializations
		$current_page = Yii::$app->common->currentPage( 'services' );
		$language	  = Yii::$app->common->currentLanguage();

		//is requested page exists
		if ( empty( $current_page ) ) {

			throw new NotFoundHttpException(
				__( 'Page Not Found' )
			);

		}

		//service model details
		$searchModel  = new PostsSearch();
		$dataProvider = $searchModel->search( ArrayHelper::merge(
			Yii::$app->request->queryParams,
			[
				$searchModel->formName() => [
					'type' 	    => 'service',
					'isDeleted' => false
				]
			]
		) );

		//set pagination
		$dataProvider->pagination->pageSize = Yii::$app->common->postsPerPage();

		//fill current page with additional information
		$current_page->customFields();

		return $this->render( 'services', [
			'page' => $this->translates( $current_page, [
				'lang_id'   => $language,
				'type'	    => 'page',
				'is_single' => true
			] ),
			'dataProvider' => merge_objects( $dataProvider, [
				'models' => $this->translates( $dataProvider->models, [
					'lang_id'   => $language,
					'type'	    => 'service',
					'is_single' => false
				] )
			] ),
			'thumbnails' => ! empty( $dataProvider->models )
				? Yii::$app->common->thumbnails(
					ArrayHelper::getColumn(
						$dataProvider->models,
						'file_id'
					)
				)
				: [],
			'icons' => ! empty( $dataProvider->models )
				? CustomFields::find()
					-> where( [
						'name' 	     => 'icon',
						'foreign_id' => ArrayHelper::getColumn(
							$dataProvider->models, 'id'
						)
					] )
					-> indexBy( 'foreign_id' )
					-> asArray()
					-> all()
				: []
		] );

	}

    /**
     * Renders the login view for the module
     * @return string
    **/
    public function actionLogin()
    {

        //param initialization
        $model = new LoginForm;

        //after success or failure request user will be redirected to home page
        if ( $model->load( Yii::$app->request->post() )
			&& $model->login()
		) {

			return $this->redirect(
				Yii::$app->getHomeUrl() . 'cabinet'
			);

		}

        //validate customer password
        if ( is_array( $model->getErrors() )
			&& $error = $model->getErrors()
			&& ! empty( $error )
			&& isset( $error[ 'password' ][ 0 ] )
		) {

			Yii::$app->session->setFlash('error',
				__( $error[ 'password' ][ 0 ] )
			);

		}

        return $this->redirect(
        	Yii::$app->getHomeUrl()
		);

    }

    /**
     * Action for logging out from the system
     * @return mixed
    **/
    public function actionLogout()
    {

        //kill user session after pressing LOG OUT btn
        Yii::$app->user->logout();

        return $this->redirect(
        	Yii::$app->getHomeUrl()
		);

    }

	/**
	 * Renders the checkout view for the module
	**/
	public function actionCheckout()
	{

		//param initialization
		$request  	  = Yii::$app->request;
		$session  	  = Yii::$app->session;
		$model 	      = new Orders();
		$current_page = Yii::$app->common->currentPage(
			'checkout'
		);

		//is requested page exists
		if ( empty( $current_page ) ) {

			throw new NotFoundHttpException(
				__( 'Page Not Found' )
			);

		}

		//assign user parameter
		$user = ! Yii::$app->user->isGuest
			? User::findOne(
				Yii::$app->user->id
			)
			: new User();

		if ( $request->isPost
			&& $model->load( $request->post() )
		) {

			if ( $this->permissionsCheck(
				$user, $request
			) ) {

				//taxanomy details
				$taxonomies = Taxonomies::find()
					-> where( [
						'name' => [
							$model->containers_type,
							$model->wagons_type
						],
						'type' => [ 'containers', 'railway' ]
					] )
					-> indexBy( 'name' )
					-> asArray()
					-> all();

				if ( ! empty( $request->post( 'documents' ) )
					&& ! is_null( $user )
				) {

					//set document parameters
					$document_name = 'Documents_' . $user->id;
					$upload_form   = new UploadForm( [
						'path' => '@frontend/web/upload/documents/' . $document_name
					] );

					//update document parameters
					$document = merge_objects( new Files(), [
						'name' => $document_name,
						'type' => 'order',
						'guide' => $upload_form->zipArchive(
							$request->post( 'documents' ),
							$document_name
						)
					] );

					//save document
					$document->save( false );

				}

				//merging model details
				$model = merge_objects( $model, [
					'user_id' => ! Yii::$app->user->isGuest
						? Yii::$app->user->id
						: $model->user_id,
					'containers_type' => isset( $taxonomies[ $model->containers_type ] )
						? $taxonomies[ $model->containers_type ][ 'id' ]
						: $model->containers_type,
					'wagons_type' => isset( $taxonomies[ $model->wagons_type ] ) || ( ! isset( $taxonomies[ $model->containers_type ] )&& isset( $taxonomies[ $model->wagons_type ] ) )
						? $taxonomies[ $model->wagons_type ][ 'id' ]
						: $model->wagons_type,
					'document_id' => isset( $document )
						? $document->id
						: $model->document_id
				] );

				//saving order
				$model->save( false );

				//set success message
				$session->setFlash( 'success',
					__('Your order was created successfully. Please wait response from sales manager' )
				);

				return $this->redirect( [
					'/cabinet/orders'
				] );

			} else {

				Yii::$app->session->setFlash('error',
					__( 'Please, enter valid password or change password by pressing `forgot password` link' )
				);

			}

		}

		//fill current page with additional information
		$current_page->customFields();

		//the list of wagon weights
		$wagon_weights = Taxonomies::find()
			-> where( [
				'type' 	    => 'weights',
				'isDeleted' => false
			] )
			-> indexBy( 'id' )
			-> asArray()
			-> all();

		return $this->render( 'checkout', [
			'page'  	 => $current_page,
			'model' 	 => $model,
			'user'		 => $user,
			'categories' => $this->translates( Taxonomies::find()
				-> where( [
					'type' => 'cargo'
				] )
				-> all(), [
				'lang_id'   => Yii::$app->common->currentLanguage(),
				'type'	    => 'taxonomy',
				'is_single' => false
			] ),
			'wagon_weights' => [
				'list' 	   => $wagon_weights,
				'relation' => array_reduce( Taxonomies::find()
					-> where( [
						'type' 	    => 'railway',
						'isDeleted' => false
					] )
					-> indexBy( 'name' )
					-> asArray()
					-> all(), function( $result, $wagon_weight ) use ( $wagon_weights ) {

					$weights = [];

					if ( ! empty( $wagon_weight[ 'weights' ] ) ) {

						foreach ( json_decode( stripslashes( $wagon_weight[ 'weights' ] ) ) as $key => $value )
							if ( isset( $wagon_weights[ $value ] ) )
								$weights[] = $wagon_weights[ $value ][ 'name' ];

						$result[ $wagon_weight[ 'name' ] ] = $weights;

					}

					return $result;

				}, [] )
			]
		] );

	}

	/**
	 * Check user access in order creation
	 * @param mixed $user
	 * @param mixed $request
	 * @return bool
	**/
	public function permissionsCheck( $user, $request )
	{

		//param initialization
		$response = true;

		if ( Yii::$app->user->isGuest ) {

			//set customer scenario
			$user->setScenario(
				$user::SCENARIO_REGISTER
			);

			//update customer details
			$user->load(
				$request->post()
			);

			//authorize user by requested credentials
			$login = merge_objects( new LoginForm, [
				'email'    => $user->email,
				'password' => $user->password
			] );

			if ( ! empty( $login->_user ) ) {

				Yii::$app->session->setFlash( 'error',
					__( 'Please, enter correct password or use another email' )
				);

				$response = false;

			} else if ( ! $login->login() ) {

				if ( $user->save( false ) ) {

					//authorise current user
					Yii::$app
						-> getUser()
						-> login( $user );

				} else {
					$response = false;
				}

			}

		}

		return $response;
	}

}