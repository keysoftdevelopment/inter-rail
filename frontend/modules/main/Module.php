<?php namespace app\modules\main;

/**************************************/
/*                                    */
/*             MAIN MODULE            */
/*                                    */
/**************************************/

class Module extends \yii\base\Module
{

    /**
     * @inheritdoc
    **/
    public $controllerNamespace = 'app\modules\main\controllers';

    /**
     * @inheritdoc
	**/
    public function init()
    {

        parent::init();

		//set layout
        $this->setLayoutPath(
        	'@frontend/themes/interrail/views/layouts'
		);

    }

}