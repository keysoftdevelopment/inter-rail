<?php namespace frontend\components;

/**************************************/
/*                                    */
/*          COMMON COMPONENT          */
/*                                    */
/**************************************/

use Yii;

use yii\base\Component;

use yii\web\Cookie;

use common\models\Files;
use common\models\Languages;
use common\models\CustomFields;
use common\models\Posts;
use common\models\Settings;

/**
 * Common component for the additional general actions
**/
class Common extends Component
{

    /**
     * Renders the header menu due to chosen language and getting data from db
     * @return array
    **/
    public function menu()
    {

    	//param initialization
        $language = $this->currentLanguage();

        //menu details
        if ( isset( $language ) && !empty( $language ) ) {

            $menu = Settings::find()
                -> select( 'description' )
                -> where( [
                    'name'    => 'menu-data',
                    'lang_id' => $language
                ] )
                -> column();

        }

        return isset( $menu[ 0 ] ) && !empty( $menu[ 0 ] )
			? json_decode( $menu[ 0 ] )
			: [];

    }

    /**
     * Return current language id from db due to chosen by user language name
     * @return int|string
    **/
    public function currentLanguage()
    {

        //set language
        if ( empty( $this->getCookies( 'language' ) ) ) {

        	//param initialization
			$default_language = Settings::findOne( [
				'name' => 'default_language'
			] );

			//set cookies
			$this->setCookies(
				'language',
				$default_language->description,
				'remove'
			);

        }

        return ! empty( $this->getCookies( 'language' ) )
			? $this->getCookies( 'language' )
			: $default_language->description;

    }

    /**
     * Return current page due to page template
	 * @param string $template
     * @return mixed
    **/
    public function currentPage( $template )
    {

    	//params initializations
        $current_page  = '';
        $template_page = CustomFields::find()
            -> select( 'foreign_id' )
            -> where( [
                'name'  	  => 'template',
                'description' => $template
            ] )
            -> asArray()
            -> all();

        //is page exists
        if ( isset( $template_page )
			&& !empty( $template_page )
		) {

        	//re-assign page parameter
            foreach ( $template_page as $page ) {

                $current = Posts::findOne( [
                    'id' => $page[ 'foreign_id' ]
                ] );

                if ( ! is_null( $current ) )
                	return $current;

            }
        }

        return $current_page;

    }

    /**
     * Setting and updating user cookies
	 * @param string $name, mixed $value, string $type
    **/
    public function setCookies( $name, $value, $type = '' )
    {

    	//param initialization
        $cookies = \Yii::$app->response->cookies;

        //removing cookie
        if ( \Yii::$app->getRequest()->getCookies()->has( $name ) && !empty ( $type ) )
            $cookies->remove( $name );

        // add a new cookie to the response to be sent
        $cookies->add( new Cookie( [
            'name'   => $name,
            'value'  => $value,
            'expire' => time() + (60 * 60 * 24),
        ] ) );

    }

    /**
     * Returns data from user cookies, i.e chosen language or location and etc ...
     * @param mixed $name
     * @return mixed
    **/
    public function getCookies( $name )
    {

    	//param initialization
        $request = \Yii::$app->getRequest()->getCookies();

        return $request->has( $name )
			? $request->getValue( $name )
			: '';

    }

    /**
     * The list of available language for showing it in page header
     * @return mixed
	**/
    public function getLanguages()
    {

        return Languages::find()
			-> asArray()
			-> all();

    }

	/**
	 * Main query to db for getting appropriate data due to name and request ids
	 * @params string $name, array $ids
	 * @return array
	**/
	public function customFieldsQuery( $name, $ids )
	{

		return CustomFields::find()
			-> select( [
				'foreign_id',
				'description'
			] )
			-> where( [
				'name'       => $name,
				'foreign_id' => $ids
			] )
			-> indexBy( 'foreign_id' )
			-> asArray()
			-> all();

	}

	/**
	 * Return the list of thumbnails due to requested file ids
	 * @param array $file_ids
	 * @return string
	 **/
	public function thumbnails( $file_ids )
	{

		return Files::find()
			-> where( [
				'id' 	    => $file_ids,
				'isDeleted' => false
			] )
			-> indexBy( 'id' )
			-> asArray()
			-> all();

	}

	/**
	 * Thumbnail due to requested file id
	 * @param int|string $file_id
	 * @param string $type
	 * @return string
	**/
    public function thumbnail( $file_id, $type = '' )
	{

		//param initialization
		$current_file = Files::findOne(
			$file_id
		);

		return ! is_null( $current_file ) && ! empty( $current_file[ 'guide' ] )
			? $current_file[ 'guide' ]
			: Yii::getAlias( '@web' ) . ( $type == 'user'
				? '/images/avatar-not-found.jpg'
				: '/images/image-not-found.png'
			);

	}

	/**
	 * Register scripts for map on the page where that method called
	 * @param mixed $view, string $section
	**/
	public function registerMap( $view, $section )
	{

		$view->registerJs( '
			var current_map = "'. $section .'";
			var map_details = JSON.parse( document.getElementById( current_map ).getAttribute( "data-details" )),
				latLng      = new google.maps.LatLng(
                	parseFloat(map_details.lat),
                	parseFloat(map_details.lng)
            	); 					 				
			
			map = new google.maps.Map( document.getElementById( current_map ), {
            	zoom            : 15,
            	minZoom         : 3,
            	maxZoom         : 15,
            	center          : latLng,
            	disableDefaultUI: true,
            	styles          : [
					{
						"elementType": "geometry",
						"stylers"    : [ { "color": "#ececec" } ]
					}, {
						"elementType": "labels.icon",
						"stylers"    : [ { "visibility": "on" } ]
					}, {
						"elementType": "labels.text.fill",
						"stylers"    : [ { "color": "#616161" } ]
					}, {
						"featureType": "administrative.land_parcel",
						"stylers"    : [ { "visibility": "on" } ]
					},  {
						"featureType": "administrative.neighborhood",
						"stylers"    : [ { "visibility": "on" } ]
					}, {
						"featureType": "poi",
						"elementType": "geometry",
						"stylers"    : [ { "color": "#eeeeee" } ]
					}, {
						"featureType": "poi",
						"elementType": "labels.text",
						"stylers"    : [ { "visibility": "on" } ]
					}, {
						"featureType": "poi.business",
						"stylers"    : [ { "visibility": "on" } ]
					}, {
						"featureType": "water",
						"stylers"    : [ { "visibility": "simplified" }, { "color": "#5AC8FA" } ]
					}, {
						"featureType": "administrative",
						"stylers"    : [ { "lightness": 45 } ]
					}, {
						"featureType": "landscape",
						"stylers"    : [ { "lightness": 90 } ]
					}, {
						"featureType": "administrative",
						"elementType": "geometry.fill",
						"stylers"    : [ { "visibility": "on" } ]
					}, {
						"featureType": "administrative",
						"elementType": "geometry.stroke",
						"stylers"    : [ { "gamma": "3" } ]
					}, {
						"featureType": "poi.park",
						"elementType": "all",
						"stylers"    : [ { "visibility": "on" } ]
					}, {
						"featureType": "road.highway",
						"elementType": "all",
						"stylers"    : [ { "visibility": "simplified" } ]
					}
				],
            	backgroundColor : "#5AC8FA"
        	} );
        	
        	var marker = new google.maps.Marker( {
            	position : map_details,
                map	     : map,
                animation: google.maps.Animation.DROP
            } );
			
			marker.setIcon( {
                url       : "' . Yii::getAlias( '@web' ) . '/images/map/map-icon.png",
                size      : new google.maps.Size(50, 65),
                scaledSize: new google.maps.Size(50, 65),
                origin    : new google.maps.Point(0, 0),
                anchor    : new google.maps.Point(30, 30)
            } );		
		' );

	}

    /**
     * Set seo title, description and tags to current page
	 * @param object $model
	 * @param string $default
    **/
    public function setSeoTags( $model, $default )
    {

		//set title
		\Yii::$app->view->title = isset( $model->title ) && !empty( $model->title )
			? $model->title
			: $default;

		//set description
		\Yii::$app->view->registerMetaTag( [
			'name'    => 'description',
			'content' => isset( $model->description ) && !empty( $model->description )
				? $model->description
				: ''
		] );

		//set tags
		\Yii::$app->view->registerMetaTag( [
			'name'    => 'keywords',
			'content' => isset( $model->tags ) && !empty( $model->tags )
				? $model->tags
				: ''
		] );

    }

	/**
	 * Excerpt of requested description
	 * @param $content mixed,
	 * @param $length int
	 * @return string
	**/
	public function excerpt( $content, $length = 150 )
	{

		//param initialization
		list( $excerpt ) = explode(
			"\n",
			wordwrap( $content, $length )
		);

		return $excerpt;

	}

	/**
	 * Maximum allowed items per page
	 * @return int
	**/
	public function postsPerPage()
	{

		//param initialization
		$post_per_page = Settings::findOne( [
			'name' => 'posts_per_page'
		] );

		return ! is_null( $post_per_page->description )
			? ( int )$post_per_page->description
			: 20;

	}

	/**
	 * Returns the list of all available transportation
	 * details due to requested type
	 *
	 * @param string $type
	 * @return mixed
	**/
	public function transportationDetails( $type = 'dimensions' )
	{

		//transportation details due to language
		$transportation = Settings::findOne( [
			'name' 	  => $type,
			'lang_id' => $this->currentLanguage()
		] );

		return ! is_null( $transportation->description )
			? json_decode( $transportation->description )
			: [];

	}

	/**
	 * Returns the list of statuses due to request type
	 * @return array
	**/
	public function statuses()
	{

		//param initialization
		$settings = Settings::findOne( [
			'name' 	  => 'statuses',
			'lang_id' => $this->currentLanguage()
		] );

		//the list of available statuses
		$statuses = ! empty( $settings->description )
			? json_decode( $settings->description )
			: [];

		return ! empty( $statuses )
			? array_reduce( $statuses->name, function( $result, $status ) use ( $statuses ) {

				if ( $statuses->role[ array_search( $status, $statuses->name ) ] == 'client' )
					$result[ str_replace( [ ' ', '-' ], '', strtolower( $status ) ) ] = $status;

				return $result;

			}, [] )
			: [];

	}

}