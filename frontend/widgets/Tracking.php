<?php namespace frontend\widgets;

/**************************************/
/*                                    */
/*      TRACE AND TRACKING WIDGET     */
/*                                    */
/**************************************/

use yii\bootstrap\Widget;

use common\models\Orders;

/**
 * Trace and Tracking Widget
**/
class Tracking extends Widget
{

	/**
	 * @var int|string option for getting current slider page by page id
	**/
	public $number;

	/**
	 * @inheritdoc
	**/
	public function init()
	{
		parent::init();
	}

	/**
	 * Rendering tracking form template
	 * @return string
	**/
	public function run()
	{

		return $this->render( 'tracking', [
			'model' => ! empty( $this->number )
				? Orders::find()
				: (object)[]
		] );

	}

}