<?php namespace frontend\widgets;

/**************************************/
/*                                    */
/*     	   CONTACT FORM WIDGET        */
/*                                    */
/**************************************/

use Yii;

use yii\bootstrap\Widget;

use common\models\Settings;

use frontend\models\Contact;

/**
 * Contact Form widget for showing block for
**/
class ContactForm extends Widget
{

    /**
     * @inheritdoc
    **/
    public function init()
    {
        parent::init();
    }

    /**
     * Rendering contact form template
	 * @return string
    **/
    public function run()
    {

    	//params initializations
		$model   = new Contact();
    	$request = Yii::$app->request;

    	if ( $request->isPost
			&& $model->load( $request->post() )
		) {

    		//support mail from settings added in admin panel
    		$support_email = Settings::findOne( [
    			'name' => 'e_mail'
			] );

    		//mail to support email
    		if ( ! empty( $support_email->description )
				&& $model->sendEmail( $support_email->description )
			) {

				Yii::$app->session->setFlash('success',
					__( 'The message was send successfully, thank you fore getting touch with us' )
				);

			}

		}

        return $this->render( 'contact-form', [
        	'model' => $model
		] );

    }

}