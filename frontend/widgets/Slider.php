<?php namespace frontend\widgets;

/**************************************/
/*                                    */
/*          SLIDER WIDGET             */
/*                                    */
/**************************************/

use yii\bootstrap\Widget;

use common\behaviors\TranslationBehavior;

use common\models\Posts;

/**
 * Slider widget for showing slider block
**/
class Slider extends Widget
{

	/**
	 * @inheritdoc
	**/
	public function behaviors()
	{

		return [

			'translation' => [
				'class' => TranslationBehavior::className()
			]

		];

	}

    /**
     * @var int|string option for getting current slider page by page id
    **/
    public $slider_id;

    /**
     * @inheritdoc
    **/
    public function init()
    {
        parent::init();
    }

    /**
     * Rendering slider template
	 * @return string
    **/
    public function run()
    {

        return $this->render( "slider", [
            'model' => $this->translates( Posts::findOne( $this->slider_id ), [
				'lang_id'   => \Yii::$app->common->currentLanguage(),
				'type'	    => 'slider',
            	'is_single' => true
			] )
        ] );

    }

}