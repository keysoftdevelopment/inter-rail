<?php namespace frontend\widgets;

/**************************************/
/*                                    */
/*          ALERT WIDGET              */
/*                                    */
/**************************************/

use yii\bootstrap\Widget;

/**
 * Alert widget for showing errors or success messages
**/
class Alert extends Widget
{

	/**
	 * @var string option
	**/
	public $support_mail;

    /**
     * @inheritdoc
	**/
    public function init()
    {
        parent::init();
    }

	/**
	 * Render flash messages html template
	 * @return mixed
	**/
    public function run()
    {

		return $this->render( "alerts", [
			'notification' => \Yii::$app->getSession()->getAllFlashes(),
			'support_mail' => $this->support_mail
		] );

    }

}
