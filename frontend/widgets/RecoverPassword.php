<?php namespace frontend\widgets;

/**************************************/
/*                                    */
/*       RECOVER PASSWORD WIDGET      */
/*                                    */
/**************************************/

use Yii;

use yii\bootstrap\Widget;

use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;

/**
 * Recover Password Widget Form
**/
class RecoverPassword extends Widget
{

	/**
	 * @var int|string option for getting current slider page by page id
	**/
	public $support_mail;

	/**
	 * @inheritdoc
	**/
	public function init()
	{
		parent::init();
	}

	/**
	 * Rendering recover password form template
	 * @return string
	**/
    public function run()
	{

		//params initialization for current method
		$request = Yii::$app->request;
		$token 	 = $request->get( 'token' );
		$model 	 = isset( $token ) && ! empty( $token )
			? new ResetPasswordForm( $token )
			: new PasswordResetRequestForm();

		//is post request
        if( $request->isPost
			&& $model->load( $request->post() )
		) {

        	if ( ! isset( $model->password ) ) {

        		//send mail to requested email address
				if ( $model->sendEmail() ) {

					Yii::$app->session->setFlash('success_restore',
						__( 'Reset password instruction was successfully send to your email' )
					);

				} else {

					Yii::$app->session->setFlash('error',
						__( 'Please check validity of entered email. Requested email does not exists in our system. Please, try to register your account with that email address' ) . ' : ' . $model->email
					);

				}


        	} else {

				//reset password with requested one
				$model->resetPassword();

				Yii::$app->session->setFlash('success_restore',
					__( 'Password was successfully changed' )
				);

			}

        }

        return $this->render( 'forgot-password', [
            'model' => $model,
			'type'  => isset( $token ) && ! empty( $token )
				? 'reset'
				: 'token-request',
			'support_mail' => $this->support_mail,
			'notification' => Yii::$app
				-> getSession()
				-> getAllFlashes(),
        ] );

    }

}