<?php /***************************************************************
 *                   SIDEBAR MENU WIDGET VIEW SECTION                *
 *********************************************************************/

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $section string */ ?>

<div class="cabinet-navigation">

    <ul class="cabinet-nav">

        <li>

			<?= Html::a( '<i class="fa fa-plus-square"></i>' . ' ' . __( 'New Order' ), Url::toRoute( [
				'/checkout'
			] ) ); ?>

        </li>

        <li class="<?= $section == 'orders' ? 'active' : '' ?>">

			<?= Html::a( '<i class="fa fa-suitcase circle"></i>' . ' ' . __( 'Orders' ), Url::toRoute( [
				'cabinet/orders'
			] ) ); ?>

        </li>

        <li class="<?= $section == 'settings' ? 'active' : '' ?>">

			<?= Html::a( '<i class="fa fa-gear"></i>' . ' ' . __( 'Settings' ), Url::toRoute( [
				'/cabinet'
			] ) ); ?>

        </li>

    </ul>

</div>