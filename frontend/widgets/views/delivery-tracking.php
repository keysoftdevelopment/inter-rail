<?php /***************************************************************
 *                  DELIVERY TRACKING FORM VIEW SECTION              *
 *********************************************************************/

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/** @var $model common\models\Settings[] */ ?>

<!-- contact-info-navigation -->
<section class="contact-info-navigation">

	<div class="container">

		<div class="row">

			<!-- support-location-container -->
			<div class="col-sm-7 col-md-7 col-xs-12 support-location-container">

				<div class="col-sm-6 col-md-6 col-xs-12">

					<i class="support-icon"></i>

					<h6>
						<?= __( 'Call center' ) ?>
					</h6>

					<ul>

						<li>

							<?= Html::a( '<i class="fa fa-paper-plane-o"></i>', isset( $model[ 'telegram' ][ 'description' ] )
                                && ! empty( $model[ 'telegram' ][ 'description' ] )
									? $model[ 'telegram' ][ 'description' ]
									: 'javascript:void(0)'
							); ?>

						</li>

						<li>

							<?= Html::a( '<i class="fa fa-weixin"></i>', isset( $model[ 'weechat' ][ 'description' ] )
							&& ! empty( $model[ 'weechat' ][ 'description' ] )
								? $model[ 'weechat' ][ 'description' ]
								: 'javascript:void(0)'
							); ?>

						</li>

						<li>

							<?= Html::a( '<i class="whatsapp-icon"></i>', isset( $model[ 'whatsapp' ][ 'description' ] )
							&& ! empty( $model[ 'whatsapp' ][ 'description' ] )
								? $model[ 'whatsapp' ][ 'description' ]
								: 'javascript:void(0)'
							); ?>

						</li>

					</ul>

					<span class="phone-number">

                        <?= __( 'Phone/Fax' ) ?>: <?= isset( $model[ 'phone' ][ 'description' ] )
							? $model[ 'phone' ][ 'description' ]
							: ''
						?>

                    </span>

				</div>

				<div class="col-sm-6 col-md-6 col-xs-12">

					<i class="location-icon"></i>

					<h6>
						<?= __( 'Location' ) ?>
					</h6>

					<p>

						<?= isset( $model[ 'address' ][ 'description' ] )
							? $model[ 'address' ][ 'description' ]
							: ''
						?>

					</p>

				</div>

			</div>
			<!-- support-location-container -->

			<!-- delivery-tracking -->
			<div class="col-sm-5 col-md-5 col-xs-12 delivery-tracking">

				<h6>
					<?= __( 'Trace and Tracking' ) ?>
				</h6>

				<?php $form = ActiveForm::begin( [
                    'action' => [
                        '/main/main/ajax-requests'
                    ],'options' => [
                        'method' => 'get'
                ] ] ); ?>

					<div class="form-group field-search">

						<?= Html::textInput(
							'search',
							'', [
							'class'       => 'form-control',
							'placeholder' => __( 'Tracking Number' ),
							'id'          => 'search'
						] ) ?>

					</div>

					<button type="submit" class="btn btn-default" data-type-id="#order-tracking-form">
						<?= __( 'Search' ) ?>
					</button>

				<?php ActiveForm::end(); ?>

			</div>
			<!-- delivery-tracking -->

		</div>

	</div>

</section>
<!-- contact-info-navigation -->
