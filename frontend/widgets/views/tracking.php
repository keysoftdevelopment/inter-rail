<?php /***************************************************************
 *               TRACE AND TRACKING WIDGET VIEW SECTION              *
 *********************************************************************/

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $model object */ ?>

<div class="tracking-popup-modal modal fade in" id="order-tracking-form" aria-hidden="true" aria-labelledby="order-tracking-form-label">

	<div class="modal-dialog modal-lg">

		<div class="modal-content">

			<?php $form = ActiveForm::begin( [
				'method'                 => 'post',
				'enableClientValidation' => true,
				'id'                     => 'order-update-form',
				'action'                 => Url::to( [
					'/cabinet/cabinet/order-update/'
				] )
			] ); ?>

                <div class="modal-header">

                    <a href="javascript:void(0)" class="close" data-dismiss="modal" type="button">
                        ×
                    </a>

                    <h4 class="modal-title" id="order-tracking-form-label">

                        <i class="fa fa-check-square-o"></i>

                        <?= __(  'Order number' ) ?> :

                        <span class="order-number">

                            <?= ! empty( $model )
                                ? $model->number
                                : ''
                            ?>

                        </span>

                    </h4>

                </div>

                <div class="modal-body">

                    <div class="tracking-title">

                        <a href="javascript:void(0)">
                            <img src="<?= Yii::getAlias( '@web' ) ?>/images/logo-white.png" />
                        </a>

                        <ul>

                            <li class="notes">

                                <label for="notes">
                                    <?= __( 'Notes' ) ?>
                                </label>

                                <div class="form-group">

                                    <?= Html::textarea( 'Orders[notes]', '', [
                                        'class'       => 'form-control',
                                        'placeholder' => __( 'Notes' )
                                    ] ); ?>

                                </div>

                            </li>

                            <li class="status">

                                <label for="status">
									<?= __( 'Status' ) ?>
                                </label>

                                <div class="form-group">

									<?= Html::dropDownList(
										'Orders[status]',
										'',
										Yii::$app->common->statuses(),
										[
											'class' => 'form-control',
										] )
									?>

                                </div>

                            </li>

                            <li class="shipment-date">

                                <label for="status">
									<?= __( 'Shipment Date' ) ?>
                                </label>

                                <div class="form-group">

									<?= Html::textInput(
										'shipment_date',
										'',
										[
											'class'    => 'form-control',
											'disabled' => true
										] )
									?>

                                </div>

                            </li>

                            <li class="arrival-date">

                                <label for="status">
									<?= __( 'Arrival Date' ) ?>
                                </label>

                                <div class="form-group">

									<?= Html::textInput(
										'arrival_date',
										'',
                                        [
										    'class'    => 'form-control',
                                            'disabled' => true
									    ] )
                                    ?>

                                </div>

                            </li>

                        </ul>

                    </div>

                    <div class="tracking-container">

                        <h4>
                            <?= __( 'Tracking Details' ) ?>
                        </h4>

                        <div class="tracking-details"></div>

                        <h6 class="no-result">
                            <?= __( 'There is no any details for tracking yet' ) ?>
                        </h6>

                        <script type="text/html" id="tracking-template">

                            <svg height="5" width="100" class="line">
                                <line x1="0" y1="0" x2="100%" y2="0"></line>
                            </svg>

                            <div class="tracking">

                                <div class="tracking-info">

                                    <div class="transportation-date">

                                        <div class="day"></div>

                                        <div class="weekday"></div>

                                    </div>

                                    <div class="depot"></div>

                                </div>

                                <div class="depot-stop"></div>

                            </div>

                        </script>

                    </div>

                </div>

                <div class="modal-footer">

                    <h4>
                        <i class="fa fa-credit-card"></i>
                        <?= __( 'Total Transportation Sum' ) ?> : <span>0</span>
                    </h4>

                    <button type="submit">
                        <i class="fa fa-spinner"></i> <?= __( 'Update' ) ?>
                    </button>

                </div>

			<?php ActiveForm::end(); ?>

		</div>

	</div>

</div>