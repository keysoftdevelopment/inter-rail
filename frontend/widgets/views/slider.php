<?php /***************************************************************
 *                      SLIDER WIDGET VIEW SECTION                   *
 *********************************************************************/

use common\models\Files;

/* @var $model common\models\Posts */

if ( ! empty( $model->description ) ) {

    //param initialization
	$slides = json_decode(
        $model->description
    );

	if ( isset( $slides->file_id ) ) {

	    //list of thumbnails due to slides file ids
		$thumbnails = Files::find()
			-> where( [
				'id' => array_map(
					function ( $slide ) {
						return is_string( $slide )
							? trim( $slide )
							: $slide;
					},
					$slides->file_id
				),
				'isDeleted' => false
			] )
			-> indexBy( 'id' )
			-> asArray()
			-> all(); ?>

		<!-- inter-real-carousel -->
		<section class="inter-real-carousel">

			<div id="inter-real-slider" class="carousel slide homepage" data-ride="carousel">

				<div class="carousel-inner" role="listbox">

					<?php foreach ( $slides->file_id as $key => $value ) {

					    //slider thumbnail
						$thumbnail = isset( $thumbnails[ trim( $value ) ] )
							? $thumbnails[ trim( $value ) ][ 'guide' ]
							: '';

						//slider description
						$description = isset( $slides->additional_desc[ $key ] ) && ! empty( $slides->additional_desc[ $key ] )
							? $slides->additional_desc[ $key ]
							: ''; ?>

						<div class="item <?= $key == 0 ? 'active' : '' ?>">

							<img src="<?= $thumbnail ?>" alt="...">

							<div class="carousel-caption">

								<?php if ( ! empty( $description ) ) { ?>

									<div class="col-md-5 col-sm-5 col-xs-12 services">

										<h1 data-animation="animated bounceInLeft">

											<?= isset( $slides->title[ $key ] )
												? $slides->title[ $key ]
												: ''
											?>

										</h1>

										<p data-animation="animated bounceInRight">

											<?= isset( $slides->desc[ $key ] )
												? $slides->desc[ $key ]
												: ''
											?>

										</p>

										<a href="<?= isset( $slides->link[ $key ] )
											? $slides->link[ $key ]
											: 'javascript:void(0)'
										?>" class="btn btn-info" data-animation="animated zoomInUp">

											<?= isset( $slides->btn_name[ $key ] )
												? $slides->btn_name[ $key ]
												: ''
											?>

										</a>

									</div>

									<div class="col-md-3 col-sm-3 col-xs-12 video-info">

										<img src="<?= Yii::getAlias( '@web' ) ?>/images/play-video.png">

										<p data-animation="animated bounceInRight">
											<?= $description ?>
										</p>

										<a href="<?= isset( $slides->additional_link[ $key ] )
											? $slides->additional_link[ $key ] :
											'javascript:void(0)'
										?>" data-animation="animated zoomInUp">
											<?= __( 'Read More' ) ?>
										</a>

									</div>

								<?php } else { ?>

									<h1 data-animation="animated fadeIn">

										<a href="<?= isset( $slides->link[ $key ] )
											? $slides->link[ $key ]
											: 'javascript:void(0)'
										?>">

											<?= isset( $slides->title[ $key ] )
												? $slides->title[ $key ]
												: ''
											?>

										</a>

									</h1>

								<?php } ?>

							</div>

						</div>

					<?php } ?>

				</div>

				<?php if ( count( $slides->file_id ) > 1 ) { ?>

					<a class="carousel-controls prev" href="#inter-real-slider" role="button" data-slide="prev">
						<i class="fa fa-angle-left"></i>
					</a>

					<a class="carousel-controls next" href="#inter-real-slider" role="button" data-slide="next">
						<i class="fa fa-angle-right"></i>
					</a>

				<?php } ?>

			</div>

		</section>
		<!-- inter-real-carousel -->

	<?php }

} ?>