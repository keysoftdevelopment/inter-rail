<?php /***************************************************************
 *                PASSWORD RECOVERY WIDGET VIEW SECTION              *
 *********************************************************************/

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $model mixed 		 */
/* @var $type string         */
/* @var $support_mail string */
/* @var $notification array  */ ?>

<div class="main-popup-modal modal fade in" id="recover-password-form" aria-hidden="<?= isset( $notification[ 'error' ] ) || isset( $notification[ 'success_restore' ] ) || $type == 'reset' ? 'false' : 'true' ?>" aria-labelledby="recover-password-form-label" data-type-id="#recover-password-form">

	<div class="modal-dialog modal-lg">

		<div class="modal-content">

			<div class="modal-header">

				<button class="close" data-dismiss="modal" type="button">
					×
				</button>

				<h4 class="modal-title" id="password-recovery-form-label">
					<?= __(  'Password Recovery Form' ) ?>
				</h4>

			</div>

			<div class="modal-body padding-none">

				<?php if ( isset( $notification[ 'error' ] )
                    || isset( $notification[ 'success_restore' ] )
                ) { ?>

                    <div class="alert alert-warning text-center" role="alert">

						<?= isset( $notification[ 'error' ] )
                            ? $notification[ 'error' ]
                            : $notification[ 'success_restore' ]
                        ?>

                    </div>

				<?php }

				if ( ! isset( $notification[ 'success_restore' ] ) ) {

                    $form = ActiveForm::begin( [
                        'method'                 => 'post',
                        'enableClientValidation' => true,
                        'id'                     => 'forgot-password-form'
                    ] ); ?>

                        <?php if ( $type === 'reset' ) { ?>

                            <?= $form
                                -> field( $model, 'password', [
                                    'template' => '{label}{input}', // Leave only input (remove label, error and hint)
                                ] )
                                -> passwordInput( [
                                    'class'       => 'form-control',
                                    'placeholder' => __( 'Password' )
                                ] )
                                -> label( __( 'Password' ) );
                            ?>

                            <div class="form-group">

                                <button type="submit" class="btn btn-auth">
                                    <?= __( 'Reset' ) ?>
                                </button>

                            </div>

                        <?php } else { ?>

                            <?= $form
                                -> field( $model, 'email', [
                                    'template' => '{label}{input}', // Leave only input (remove label, error and hint)
                                ] )
                                -> textInput( [
                                    'class'       => 'form-control',
                                    'placeholder' => __( '______@______.___' ),
                                    'required'    => true,
                                    'pattern'     => '[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$',
                                ] )
                                -> label( __( 'Email' ) );
                            ?>

                            <div class="form-group">

                                <button type="submit" class="btn btn-auth">
                                    <?= __( 'Send' ) ?>
                                </button>

                            </div>

                        <?php }

                    ActiveForm::end();

				} ?>

			</div>

			<div class="modal-footer">

				<?= __( 'Do you have some troubles?' ); ?>

				<?= Html::a( __( 'Contact with us by' ) . ' ' . $support_mail,
					'mailto:' . $support_mail
				); ?>

			</div>

		</div>

	</div>

</div>