<?php /***************************************************************
 *                       CONTACT FORM VIEW SECTION                   *
 *********************************************************************/

use yii\bootstrap\ActiveForm;

/** @var $model frontend\models\Contact */ ?>

	<section class="contact-us">

		<div class="container">

			<div class="row">

				<div class="col-sm-6 col-md-6">

					<h2 class="title">
						<?= __( 'Trust us your cargo' ) ?>
					</h2>

					<p>
						<?= __( 'All aspects of our work processes, organization and operations are focused on the needs of our customers' ) ?>
					</p>

					<?php $form = ActiveForm::begin( [
						'method'                 => 'post',
						'enableClientValidation' => true,
					] ); ?>

                        <?= $form
                            -> field( $model, 'name', [
                                'template' => '<div class="form-group сol-sm-6 col-md-6 col-xs-12">{input}</div>', // Leave only input (remove label, error and hint)
                                'options'  => [
                                    'tag' => false, // Don't wrap with "form-group" div
                                ]
                            ] )
                            -> textInput( [
                                'class'       => 'form-control',
                                'placeholder' => __( 'Name' ),
								'required'    => true
                            ] )
                            -> label( false );
                        ?>

                        <?= $form
                            -> field( $model, 'company', [
                                'template' => '<div class="form-group сol-sm-6 col-md-6 col-xs-12">{input}</div>', // Leave only input (remove label, error and hint)
                                'options'  => [
                                    'tag' => false, // Don't wrap with "form-group" div
                                ]
                            ] )
                            -> textInput( [
                                'class'       => 'form-control',
                                'placeholder' => __( 'Company' ),
								'required'    => true
                            ] )
                            -> label( false );
                        ?>

                        <?= $form
                            -> field( $model, 'email', [
                                'template' => '<div class="form-group сol-sm-6 col-md-6 col-xs-12">{input}</div>', // Leave only input (remove label, error and hint)
                                'options'  => [
                                    'tag' => false, // Don't wrap with "form-group" div
                                ]
                            ] )
                            -> textInput( [
                                'class'       => 'form-control',
                                'placeholder' => __( '______@______.___' ),
								'pattern'     => '[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$',
                                'required'    => true
                            ] )
                            -> label( false );
                        ?>

                        <?= $form
                            -> field( $model, 'phone', [
                                'template' => '<div class="form-group сol-sm-6 col-md-6 col-xs-12">{input}</div>', // Leave only input (remove label, error and hint)
                                'options'  => [
                                    'tag' => false, // Don't wrap with "form-group" div
                                ]
                            ] )
                            -> textInput( [
								'type'        => 'number',
                                'class'       => 'form-control',
                                'placeholder' => __( '+99(99)9999-9999' ),
								'required'    => true,
								'pattern'     => '[\+]\d*'
                            ] )
                            -> label( false );
                        ?>

                        <?= $form
                            -> field( $model, 'message' )
                            -> textarea( [
                                'class'       => 'form-control',
                                'placeholder' => __( 'Message' )
                            ] )
                            -> label( false );
                        ?>

						<div class="form-container">

							<div class="form-group submit-area">

								<button type="submit">
									<?= __( 'Send' ) ?>
								</button>

							</div>

						</div>

					<?php ActiveForm::end(); ?>

				</div>

			</div>

		</div>

	</section>
