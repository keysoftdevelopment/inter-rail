<?php /***************************************************************
 *                        ALERT WIDGET VIEW SECTION                  *
 *********************************************************************/

use yii\helpers\Html;

/* @var $notification mixed  */
/* @var $support_mail string */ ?>

<div class="main-popup-modal modal fade in" id="notifications" aria-hidden="<?= isset( $notification[ 'success' ] ) ? 'false' : 'true' ?>" aria-labelledby="notifications-label" data-type-id="#notifications">

    <div class="modal-dialog modal-lg">

        <div class="modal-content">

            <div class="modal-header">

                <button class="close" data-dismiss="modal" type="button">
                    ×
                </button>

                <h4 class="modal-title" id="notifications-label">
					<?= __(  'Notification' ) ?>
                </h4>

            </div>

            <div class="modal-body">

                <div class="alert alert-success text-center" role="alert">
                    <?= $notification[ 'success' ] ?>
                </div>

            </div>

            <div class="modal-footer">

				<?= __( 'Do you have some troubles?' ); ?>

				<?= Html::a( __( 'Contact with us by' ) . ' ' . $support_mail,
					'mailto:' . $support_mail
				); ?>

            </div>

        </div>

    </div>

</div>