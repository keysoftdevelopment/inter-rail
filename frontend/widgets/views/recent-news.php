<?php /***************************************************************
 *                        RECENT NEWS VIEW SECTION                   *
 *********************************************************************/

use yii\helpers\Html;

/** @var $model array */

if ( ! empty ( $model ) ) : ?>

    <section class="recent-news">

        <div class="container">

            <div class="row">

                <h2>
                    <?= __( 'Recent News' ) ?>
                </h2>

                <div id="recent-news-carousel" class="carousel slide carousel-multi-item v-2 col-md-offset-6 col-sm-6 col-md-6" data-ride="carousel">

                    <h3>
                        <?= __( 'Actual' ) ?>
                    </h3>

                    <div class="carousel-inner v-2" role="listbox">

                        <?php foreach ( $model as $key => $value ) { ?>

                            <div class="item <?= $key == 0 ? 'active' : '' ?>">

                                <h4>
                                    <?= $value->title ?>
                                </h4>

                                <p>

                                    <?= Yii::$app->common->excerpt(
                                        $value->description, 500
                                    ); ?>

                                </p>

								<?= Html::a( __(  'Read More' ), [
									'/single-article/' . $value->slug
								], [
									'class' => 'btn btn-info pull-left',
									'role'  => "button"
								] ); ?>

                            </div>

						<?php } ?>

                        <div class="controls-top">

                            <a class="btn-floating" href="#recent-news-carousel" data-slide="prev">
                                <i class="fa fa-angle-left"></i>
                            </a>

                            <a class="btn-floating" href="#recent-news-carousel" data-slide="next">
                                <i class="fa fa-angle-right"></i>
                            </a>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </section>

<?php endif; ?>
