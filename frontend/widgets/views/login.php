<?php /***************************************************************
 *                     LOGIN WIDGET VIEW SECTION                     *
 *********************************************************************/

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

/* @var $notification array            */
/* @var $model common\models\LoginForm */
/* @var $support_mail string           */ ?>

<div class="main-popup-modal modal fade in" id="login-form" aria-hidden="<?= isset( $notification[ 'error' ] ) ? 'false' : 'true' ?>" aria-labelledby="login-form-label" data-type-id="#login-form">

    <div class="modal-dialog modal-lg">

        <div class="modal-content">

            <div class="modal-header">

                <button class="close" data-dismiss="modal" type="button">
                    ×
                </button>

                <h4 class="modal-title" id="login-form-label">
					<?= __(  'Sign IN Form' ) ?>
                </h4>

            </div>

            <div class="modal-body padding-none">

                <?php if ( isset( $notification[ 'error' ] ) ) { ?>

                    <div class="alert alert-warning text-center" role="alert">
                        <?= $notification[ 'error' ] ?>
                    </div>

                <?php }

                $form = ActiveForm::begin( [
                    'method'                 => 'post',
                    'enableClientValidation' => true,
                    'id'                     => 'authorisation-form',
                    'action'                 => Url::to( [
                        '/main/main/login'
                    ] )
                ] ); ?>

                    <?= $form
                        -> field( $model, 'email', [
                            'template' => '{label}{input}', // Leave only input (remove label, error and hint)
                        ] )
                        -> textInput( [
                            'class'       => 'form-control',
                            'placeholder' => __( 'Email' )
                        ] )
                        -> label( __( 'Email' ) );
                    ?>

                    <?= $form
                        -> field( $model, 'password', [
                            'template' => '{label}{input}', // Leave only input (remove label, error and hint)
                        ] )
                        -> passwordInput( [
                            'class'       => 'form-control',
                            'placeholder' => __( 'Password' )
                        ] )
                        -> label( __( 'Password' ) );
                    ?>

                    <div class="form-group">

                        <div class="row">

                            <div class="col-md-6 col-sm-6 col-xs-12">

                                <button type="submit" class="btn btn-auth">
                                    <?= __( 'Sign IN' ) ?>
                                </button>

                            </div>

                            <div class="col-md-6 col-sm-6 col-xs-12">

                                <a href="javascript:void(0)" data-type-id="#recover-password-form" class="show-popup">
									<?= __( 'Forgot password ?' ) ?>
                                </a>

                            </div>

                        </div>

                    </div>

				<?php ActiveForm::end(); ?>

            </div>

            <div class="modal-footer">

				<?= __( 'Do you have some troubles?' ); ?>

				<?= Html::a( __( 'Contact with us by' ) . ' ' . $support_mail,
					'mailto:' . $support_mail
				); ?>

            </div>

        </div>

    </div>

</div>