<?php namespace frontend\widgets;

/**************************************/
/*                                    */
/*     DELIVERY TRACKING WIDGET       */
/*                                    */
/**************************************/

use yii\bootstrap\Widget;

use common\models\Settings;

/**
 * Delivery Tracking Form Widget showed up below slider
**/
class DeliveryTrackingForm extends Widget
{

	/**
	 * @inheritdoc
	**/
	public function init()
	{
		parent::init();
	}

	/**
	 * Rendering delivery tracking form template
	 * @return string
	**/
	public function run()
	{

		return $this->render( "delivery-tracking", [
			'model' => array_replace( Settings::find()
				-> where( [
					'name' => [
						'phone',
						'telegram',
						'whatsapp',
						'weechat'
					]
				] )
				-> indexBy( 'name' )
				-> asArray()
				-> all(),
				Settings::find()
					-> where( [
						'name' 	  => 'address',
						'lang_id' => \Yii::$app->common->currentLanguage()
					] )
					-> indexBy( 'name' )
					-> asArray()
					-> all()
			)
		] );

	}

}