<?php namespace frontend\widgets;

/**************************************/
/*                                    */
/*   	   RECENT NEWS WIDGET     	  */
/*                                    */
/**************************************/


use yii\bootstrap\Widget;

use common\behaviors\TranslationBehavior;

use common\models\Posts;

/**
 * Recent News widget for showing block for recent news
**/
class RecentNews extends Widget
{

	/**
	 * @inheritdoc
	**/
	public function behaviors()
	{

		return [

			'translation' => [
				'class' => TranslationBehavior::className()
			]

		];

	}

    /**
     * @inheritdoc
    **/
    public function init()
    {
        parent::init();
    }

    /**
	 * Rendering recent news section template
     * @return mixed
    **/
    public function run()
    {

		return $this->render( 'recent-news', [
			'model' => $this->translates( Posts::find()
				-> where( [
					'type' 	    => 'posts',
					'isDeleted' => false
				] )
				-> limit( 3 )
				-> orderBy( [
					'created_at' => SORT_DESC
				] )
				-> all(), [
				'lang_id'   => \Yii::$app->common->currentLanguage(),
				'type'	    => 'posts',
				'is_single' => false
			] )
		] );

    }

}