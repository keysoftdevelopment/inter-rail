<?php namespace frontend\widgets;

/**************************************/
/*                                    */
/*          LOGIN WIDGET              */
/*                                    */
/**************************************/

use yii\bootstrap\Widget;

use common\models\LoginForm;

/**
 * Login widget for showing login form in popup window
**/
class Login extends Widget
{

	/**
	 * @var string option
	**/
	public $support_mail;

    /**
     * @inheritdoc
    **/
    public function init()
    {
        parent::init();
    }

    /**
     * Rendering login popup window template
	 * @return string
    **/
    public function run()
    {

        return $this->render( 'login', [
			'notification' => \Yii::$app->getSession()->getAllFlashes(),
            'model'   	   => new LoginForm(),
			'support_mail' => $this->support_mail
        ] );

    }

}