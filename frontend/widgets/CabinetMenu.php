<?php namespace frontend\widgets;

/**************************************/
/*                                    */
/*    PERSONAL CABINET MENU WIDGET    */
/*                                    */
/**************************************/

use yii\bootstrap\Widget;


/**
 * Sidebar menu in personal cabinet pages Cabinet
**/
class CabinetMenu extends Widget
{

	/**
	 * @var string, need to show active menu
	**/
	public $section;

	/**
	 * @inheritdoc
	**/
	public function init()
	{
		parent::init();
	}

	/**
	 * Render personal cabinet sidebar menu
	 * @return mixed
	**/
	public function run()
	{

		return $this->render( 'sidebar-menu', [
			'section' => $this->section
		] );

	}

}