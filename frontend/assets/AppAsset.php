<?php namespace frontend\assets;

/***************************************/
/*                                     */
/*      DEFAULT SCRIPTS AND STYLES     */
/*                                     */
/***************************************/

use yii\web\AssetBundle;

class AppAsset extends AssetBundle
{

    public $basePath = '@webroot';
    public $baseUrl  = '@web';
	public $css      = [
		'font-awesome/font-awesome.min.css',
		'bootstrap/css/bootstrap.min.css',
		'css/animate.min.css',
		'css/flag-icon.min.css',
		'css/style.css',
		'css/jquery-ui.custom.css',
	];

	public $js = [
		'js/jquery-ui.min.js',
		'js/html5shiv.min.js',
		'js/respond.min.js',
		'bootstrap/js/bootstrap.min.js',
		'js/animations/counter.js',
		'js/animations/chain-fade.min.js',
		'js/animations/anime.min.js',
		'js/popper.min.js',
		'js/dropzone/dropzone.min.js',
		'js/owl.carousel.min.js',
		'js/jquery.scrollbox.min.js',
		'js/main.min.js'
	];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset'
    ];

}
