<?php /********************************/
/*                                    */
/*         FRONTEND MAIN CONFIG       */
/*                                    */
/**************************************/

use yii\helpers\ArrayHelper;

$params = ArrayHelper::merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/params.php')
);

return [
	'id'                  => 'app-frontend',
	'basePath'            => dirname( __DIR__ ),
	'bootstrap'           => [ 'log' ],
	'controllerNamespace' => 'frontend\controllers',
	'defaultRoute'        => 'main',
	'sourceLanguage'	  => 'en',
	'language'            => 'ru',
	'modules'             => [

		'main'     => [
			'class' => 'app\modules\main\Module'
		],

		'cabinet'  => [
			'class' => 'app\modules\cabinet\Module'
		],

		'articles'  => [
			'class' => 'app\modules\articles\Module'
		]

	],

	'components' => [

		'assetManager' => [
			'class'   => 'yii\web\AssetManager',
			'bundles' => [

				'yii\web\JqueryAsset' => [
					'js' => [
						Yii::getAlias( '@frontend_link' ) . '/js/jquery.min.js'
					]
				],

				'yii\bootstrap\BootstrapAsset' => [
					'css' => [
						YII_ENV_DEV
							? 'css/bootstrap.css'
							: 'css/bootstrap.min.css'
					]
				],

				'yii\bootstrap\BootstrapPluginAsset' => [
					'js' => [
						YII_ENV_DEV
							? 'js/bootstrap.js'
							: 'js/bootstrap.min.js'
					]
				]

			]

		],

		'db'           => require( dirname( __DIR__ ) . '/../common/config/db.php' ),
		'request'      => [
			'csrfParam' => '_csrf-frontend'
		],

		'view'         => [
			'theme' => [
				'class'    => 'frontend\themes\interrail\Theme',
				'basePath' => '@app/',
				'baseUrl'  => '@web/'
			]
		],

		'common' => [
			'class' => 'frontend\components\Common',
		],

		'user'         => [
			'identityClass'   => 'common\models\User',
			'enableAutoLogin' => true,
			'loginUrl'        => '/main/main/login',
			'identityCookie'  => [
				'name' 	   => '_identity-frontend',
				'httpOnly' => true
			]
		],

		'log'          => [
			'traceLevel' => YII_DEBUG ? 3 : 0,
			'targets'    => [
				[
					'class'  => 'yii\log\FileTarget',
					'levels' => [ 'error', 'warning' ],
				]
			]
		],

		'errorHandler' => [
			'errorAction' => 'main/main/error',
		],

		'urlManager'   => [
			'class'           => 'yii\web\UrlManager',
			'enablePrettyUrl' => true,
			'showScriptName'  => false,
			'rules'           => [
				'contact'					  => 'main/main/contact',
				'services'					  => 'main/main/services',
				'news'						  => 'articles',
				'page/<slug>'                 => 'main/main/page',
				'single-article/<slug>'       => 'articles/articles/single-article',
				'language/change/<lang>' 	  => 'main/main/change-language',
				'checkout'					  => 'main/main/checkout',
				'cabinet/upload'			  => 'cabinet/cabinet/upload',
				'cabinet/orders'	          => 'cabinet/cabinet/orders',
				'cabinet/order/update/<id>'   => 'cabinet/cabinet/order-update',
				'cabinet/order/invoice/<id>'   => 'cabinet/cabinet/order-invoice'
			]
		],

		'i18n'         => [
			'translations' => [
				'app*' => [
					'class'   => 'yii\i18n\PhpMessageSource',
					'fileMap' => [
						'app'       => 'app.php',
						'app/error' => 'error.php',
					]
				]
			]
		]

	],

	'as locale' => [
		'class' => 'common\behaviors\LocaleBehavior',
	],

	'params' => $params

];
