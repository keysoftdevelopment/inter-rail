﻿/**
 * Name:        Scripts For FRONTEND
 * Description: INTER-RAIL Scripts
 * Version:     1.0.0
**/

jQuery( document ).ready( function ()
{

    /**
     * @desc this class will hold functions for frontend side for current service
     * examples include auth popup modals, operations with carts and etc ....
    **/
    var frontend_inter_rail = {

        /**
         * @desc main construct method for current class
         */
        init: function ()
        {

            $( '.scrollbox-container' ).scrollbox( {
                direction       : 'h',
                autoPlay        : false,
                onMouseOverPause: false,
                listElement     : '.scrollbox',
                listItemElement : '.element'
            } );

            //click actions lists and appropriate methods
            $( document )
                .on( 'click', '.file-lists .file-lists-container .scrollbox .element a', this.remove_file )
                .on( 'click', '.scrollbox-slider .control.backward', this.file_scroll_box )
                .on( 'click', '.scrollbox-slider .control.forward', this.file_scroll_box )
                .on( 'change', '.category-lists select', this.checkout_tax )
                .on( 'click', '#main-checkout-form .submit-area a', this.checkout_wizard )
                .on ( 'click', '#category-detail li > a ', this.quantity_weight )
                .on( 'change', 'input[name="Orders[wagons_type]"]', this.wagon_weights )
                .on( 'click', '.checkout-form-progress ul li a', this.checkout_wizard )
                .on( 'click', '.cabinet-details .order-lists a.order-details', function() {
                    frontend_inter_rail.order( $( this ).data( 'details' ), this );
                } )
                .on( 'keydown', '#transportation-from, #transportation-to', function( event ) {

                    if ( event.key === 'Enter' )
                        event.preventDefault();

                } )
                .on( 'click', 'a.show-more', this.more )
                .on( 'click', 'a.show-popup', function() {
                    frontend_inter_rail.modal_display( this )
                } )
                .on( 'click', '.modal button.close', this.close_modals )
                .on( 'click', '.modal a.close', this.close_modals )
                .on( 'scroll', this.run_animation )
                .on( 'click', '.scroll-top a', this.scroll_top )
                .on( 'submit', '.delivery-tracking form', this.search_order )
                .on( 'click', '.personal-cabinet .order-lists a.confirm-payment', this.confirm_payment );

            //variables on page load
            var inter_rail_slider = $( '#inter-real-slider' ),
                slider_effects    = inter_rail_slider
                    .find( '.item:first' )
                    .find( "[data-animation ^= 'animated']" );

            //initialize carousel
            inter_rail_slider.carousel();

            //animate captions in first slide on page load
            this.animate( slider_effects );

            //other slides to be animated on carousel slide event
            inter_rail_slider.on( 'slide.bs.carousel', function(e) {

                var animating = $( e.relatedTarget ).find(
                    "[data-animation ^= 'animated']"
                );

                frontend_inter_rail.animate( animating );

            } );

            //service carousel
            $( '#services-carousel' ).owlCarousel( {
                autoplay        : true,
                autoplayTimeout : 2000,
                loop            : true,
                mouseDrag       : true,
                responsiveClass : true,
                responsive : {
                    0:{
                        items : 1
                    },
                    600 : {
                        items : 2
                    },
                    994 : {
                        items : 3
                    },
                    1200 : {
                        items : 4
                    }
                }
            } );


            //option auto fill city/country on new order create
            this.search_place();

            //upload image main method
            this.file_uploader();

            // actions with user avatar
            this.drop_zone( 'user' );

            //display modal window
            if ( $( '.modal[aria-hidden="false"]' ).attr( 'id' ) ) {

                frontend_inter_rail.modal_display(
                    '.modal[aria-hidden="false"]'
                );

            }

        },

        /**
         * @desc checkout page file uploader option
        **/
        file_uploader: function()
        {

            //params initializations
            var uploader = $( '<input type="file"/>' ),
                reader   = new FileReader();

            //upload files
            $( '.upload-file' ).on( 'click', function () {
                uploader.click()
            } );

            //change views of upload container section
            uploader.on( 'change', function () {

                reader.onload = ( function( theFile ) {

                    return function( event ) {

                        $( '.file-lists > .file-lists-container > .scrollbox' ).prepend( "<div class='element'>"
                            + "<i class='fa fa-file-text-o'></i>"
                            + "<label>"
                                + "<input type='hidden' name='documents[]' value='" + JSON.stringify( {
                                    "name"    : theFile.name,
                                    "content" : event.target.result
                                } ) +"'>"
                                + theFile.name +
                            "</label>" +
                            "<a href='javascript:void(0)'>"
                                + "x" +
                            "</a>" +
                        "</div>" );

                    };

                } )( uploader[0].files[0] );

                $( '.file-attach-container .file-lists .control' ).css( 'display', 'table' );

                reader.readAsDataURL( uploader[0].files[0] )

            } );

        },

        /**
         * @desc remove file container
        **/
        remove_file: function ()
        {

            $( this ).parent().remove();

            if ( $( '.file-lists-container .scrollbox .element' ).length < 1 )
                $( '.file-attach-container .file-lists .control' ).hide();

        },

        /**
         * @desc file scroll box option
        **/
        file_scroll_box : function ()
        {

            $( this ).parents(
                '.scrollbox-slider'
            ).find(
                '.scrollbox-container'
            ).trigger( $( this ).hasClass( 'backward' )
                ? 'backward'
                : 'forward'
            );

        },

        /**
         * @desc checkout page category options due to selected category
        **/
        checkout_tax: function (  )
        {

            //param initialization
            var tax_details = $(
                'option:selected', this
            ).data( 'details' );

            if ( tax_details
                && tax_details.wagons
                && tax_details.containers
            ) {

                //fill in containers/wagons section with chosen taxonomy details
                $.map( [
                    'containers_type',
                    'wagons_type'
                ], function( type, key ) {

                    frontend_inter_rail.tax_options( {
                        'name'   : 'Orders[' + type + ']',
                        'section': $( '#' + type.replace( '_type', '' ) + ' ul' ),
                        'list'   : tax_details[ type.replace( '_type', '' ) ]
                    } );

                } );

            }

        },

        /**
         * @desc display category options
         * @param option array
        **/
        tax_options: function ( option )
        {

            //emptify requested option
            option.section.empty();

            //fill in option
            $.each( option.list, function( key, value ) {

                option.section.append(
                    '<li>'
                        + '<label class="radio">'
                            + value
                            + '<input type="radio" name="' + option.name  +'" value="' + value + '">'
                            + '<span class="checkmark"></span>'
                        + '</label>' +
                    '</li>'
                );

            } );

        },

        /**
         * @desc auto filling transportation details
        **/
        search_place: function (  )
        {

            if ( typeof inter_rail_map !== 'undefined' ) {

                $.map( [
                    'transportation-from',
                    'transportation-to'
                ], function( type, key ) {

                    //activate field as place search
                    inter_rail_map.search = new google.maps.places.SearchBox(
                        document.getElementById( type )
                    );

                    // Listen for the event fired when the user selects a prediction and retrieve
                    // more details for that place.
                    inter_rail_map.search.addListener(
                        'places_changed',
                        inter_rail_map.places_search
                    );

                } );

            }

        },

        /**
         * @desc main checkout wizard method
        **/
        checkout_wizard: function()
        {

            // initialization params for current method
            var tabs = $( '.checkout-form-progress ul' ),
                next = $( this ).hasClass( 'checkout-progress' )
                    ? true
                    : $( 'li.active', tabs ).attr( 'data-next' ),

                current = $( this ).hasClass( 'checkout-progress' )
                    ? $( 'section.' + $( '.checkout-form-progress ul li.active a' ).attr( 'href' ).replace( '#', '' ) )
                    : $( 'section.' + $( 'li.active', tabs ).attr( 'data-current' ) ),

                section = $( this ).hasClass( 'checkout-progress' )
                    ? $( 'section.' + $( this ).attr( 'href' ).replace( '#', '' ) )
                    : $( 'section.' + next ),

                active = $( this ).hasClass( 'checkout-progress' )
                    ? $( this ).parents( 'li' )
                    : $( 'a[href="#'+ next +'"]' ).parents( 'li' ),

                validate    = true,
                from        = $( '#transportation-from' ).val(),
                to          = $( '#transportation-to' ).val(),
                calculation = $( '.delivery-details-container .delivery-form' ),
                routes      = {
                    'method'    : 'locations',
                    'locations' : {
                        'from' : from.indexOf(',') !== -1
                            ? from.substring( from.indexOf(','), 0 )
                            : from,
                        'to' : to.indexOf(',') !== -1
                            ? to.substring( to.indexOf(','), 0 )
                            : to
                    }
                };

            $( '.first-step .required .form-control' ).each( function( index, value ) {

                if ( $( value ).attr( 'name' )
                    && ( $( value ).val() === '' || $( value ).val() === null )
                ) {

                    validate = false;

                    $( value ).parent().addClass( 'has-error' );

                    $( 'p.help-block-error', $( value ).parents( '.form-group' ) ).html(
                        $( 'form#main-checkout-form' ).data( 'msg' )
                    );

                } else {

                    $( value ).parent().removeClass( 'has-error' );

                    $( 'p.help-block-error',
                        $( value ).parents( '.form-group' )
                    ).empty();

                }

            } );

            if ( validate ) {

                if ( next ) {

                    //remove active class from tab
                    $( 'li', tabs ).removeClass( 'active' );

                    //add active class
                    active.addClass( 'active' );

                    //run hide container animation
                    current.animate( {
                        opacity: 0
                    }, 300 );

                    setTimeout( function show() {
                        current.removeClass( 'is-visible' );
                        section.addClass( 'is-visible' );
                    }, 200 );

                    //run show container animation
                    section.animate( {
                        opacity:1.0
                    }, 300 );

                    $( [ document.documentElement ] ).animate( {
                        scrollTop: current.offset().top
                    }, 1000 );

                    //hide or show bottom paginations due to step
                    if ( active.data( 'current' ) === 'last-step' ) {
                        $( '.submit-area a' ).hide();
                        $( '.submit-area button' ).show();
                    } else {
                        $( '.submit-area a' ).show();
                        $( '.submit-area button' ).hide();
                    }

                }

                if ( routes.locations.from
                    && routes.locations.to
                ) {

                    $.ajax( {
                        url    : $('#main-checkout-form' ).attr( 'url' ),
                        type   : 'POST',
                        data   : routes,
                        success: function ( result ) {

                            //set routes in calculation form
                            $( 'h4', calculation ).html(
                                '(' + routes.locations.from + ', ' + routes.locations.to + ')'
                            );

                            routes.method = 'city';

                            if ( result.response ) {

                                if ( parseFloat( result.transportation_sum ) > 0 )
                                    $( '.delivery-sum', calculation ).html(
                                        result.transportation_sum
                                    );

                                routes.method   = 'route';
                                routes.route    = result.response;
                                routes.duration =  result.duration;

                            } else if ( routes.locations.from.locations && routes.locations.from.locations.lat
                                && routes.locations.from.locations.lng && routes.locations.to.locations.lat
                                && routes.locations.to.locations.lng
                            ) {

                                routes = {
                                    'method'      : 'locations',
                                    'origin'      : routes.locations.from.locations,
                                    'destination' : routes.locations.to.locations,
                                    'depots'      : [
                                        routes.locations.from,
                                        routes.locations.to
                                    ]

                                }
                            }

                            inter_rail_map.draw_lines(
                                routes
                            );

                        }

                    } );

                }

            } else {

                $( [ document.documentElement ] ).animate( {
                    scrollTop: current.offset().top
                }, 1000 );

            }

        },

        /**
         * @desc weight and quantity details displaying
        **/
        quantity_weight: function()
        {

            // initialization params for current method
            var type          = $( this ).attr( 'href' ).replace( '#', '' ).replace( 's', '_type' ),
                messages      = $( this ).data( 'msg' ),
                weights_field = $( '#wagon-weights-field' );

            $.map( [
                'quantity-packages-section',
                'weight-dimensions-section'
            ], function( container, key ) {

                $( '.' + container + ' .type' ).html(
                    container === 'quantity-packages-section'
                        ? messages.multiple
                        : messages.single
                );

            } );

            $( 'input:radio[name=' + type + ']:checked' ).prop(
                'checked', false
            );

            //show/hide wagon weight fields
            if ( type === 'wagon_type' ) {
                weights_field.show();
            } else {
                weights_field.hide();
            }

        },

        /**
         * @desc controlling weights options due to selected wagon
        **/
        wagon_weights: function()
        {

            //params initializations
            var weights   = $( '#wagons' ).data( 'relations' ),
                container = $( 'div#wagon-weights-field select' ),
                selected  = typeof weights[ $( this ).val() ] !== 'undefined'
                    ? weights[ $( this ).val() ]
                    : '';

            if ( selected !== '' ) {

                $( 'option', container ).each( function( index, value ) {

                    if ( $( value ).attr( 'value' ).replace( ' ', '' ) !== ''
                        && selected.indexOf( $( value ).attr( 'value' ).replace( ' ', '' ) ) < 0
                    ) {
                        $( 'option[value="' + $( value ).attr( 'value' ) + '"]', container ).remove()
                    }

                } );

            }

        },

        /**
         * @desc show order tracking details
         * @param order object
         * @param current string
        **/
        order: function( order, current )
        {

            //params initializations
            var tracking  = $( '.tracking-container .tracking-details' ),
                no_result = $( '.tracking-container .no-result' ),
                form      = $( 'form#order-update-form' );

            //showing no result message option
            no_result.show();

            //hiding tracking details
            tracking.hide().empty();

            if ( order ) {

                //append order id to form
                form.attr( 'action',
                    form.attr( 'action' ) + '?id=' + order.id
                );

                //append order total details to form
                $( '.modal-footer span' ).html(
                    order.total
                );

                //fill in form with order details
                $.map( {
                    'textarea' : [
                        {
                            'field' : '.modal-title span',
                            'val'   : order.order_number
                        }, {
                            'field' : 'li.notes textarea',
                            'val'   : order.notes
                        }
                    ],
                    'input' : [
                        {
                            'field' : 'li.shipment-date input',
                            'val'   : order.transportation_dates.from
                        }, {
                            'field' : 'li.arrival-date input',
                            'val'   : order.transportation_dates.to
                        }
                    ],
                    'select' : [
                        {
                            'field' : 'li.status select option[value="' + order.status + '"]'
                        }
                    ]
                }, function ( params, index ) {

                    //fill in tracking details in form
                    $.map( params, function( paramn, key ) {

                        if ( index === 'textarea' ) {
                            $( '.tracking-popup-modal ' + paramn.field ).html( paramn.val );
                        } else if ( index === 'input' ) {
                            $( '.tracking-popup-modal ' + paramn.field ).val( paramn.val );
                        } else {
                            $( '.tracking-popup-modal ' + paramn.field ).prop( 'selected', 'selected' );
                        }

                    } );

                } );

                if ( order.tracking.length > 0 ) {

                    //hiding no result msg
                    no_result.hide();

                    //changing tracking displaying
                    tracking.css( 'display', 'flex' );

                    $.map( order.tracking, function( trace, key ) {

                        //param initialization
                        var template = $( $( '#tracking-template' ).html() );

                        $.map( {
                            'day'           : trace[ 'arrival' ][ 'day' ],
                            'weekday'       : trace[ 'arrival' ][ 'week' ] + '<div class="month-year">' + trace[ 'arrival' ][ 'full' ] + '</div>',
                            'depot'         : trace[ 'name' ],
                            'line'          : trace[ 'status' ],
                            'tracking-info' : trace[ 'status' ]
                        }, function( param, index ) {

                            if ( index === 'line' ) {
                                template.find( '.' + index ).addClass( param );
                            } else if ( index === 'tracking-info' ) {
                                template.find( '.tracking-info' ).parent().addClass( param ).attr( 'id', 'tracking-stop-' + key );
                            } else {
                                template.find( '.' + index ).html( param );
                            }

                        } );

                        //appending line to tracking details view
                        if ( parseInt( key ) === 0 ) {

                            tracking.append(
                                '<hr class="vertical">'
                            );

                        }

                        //fill in tracking form
                        $( '.tracking-details' ).append(
                            template
                        );

                        if ( parseInt( key ) === ( order.tracking.length - 1 ) ) {

                            tracking.append(
                                '<hr class="horizontal ' + trace[ 'status' ] + '" />' +
                                '<hr class="vertical ' + trace[ 'status' ] + '">'
                            );

                        }

                    } );

                }

            }

            //popup window for tracking details
            frontend_inter_rail.modal_display( current );

            //animation for tracking
            frontend_inter_rail.tracking_animation();

        },

        /**
         * @desc tracking details displaying
        **/
        tracking_animation: function()
        {

            // initialization params for current method
            var lines = $( 'svg line' );

            if ( lines ) {

                $.map( lines, function( line, key ) {

                    //params initializations
                    var offset = anime.setDashoffset( line ),
                        stop   = $( '#tracking-stop-' + key );

                    //set attribute to animated line
                    line.setAttribute(
                        'stroke-dashoffset', offset
                    );

                    //drawing animated tracking details line
                    anime( {
                        targets         : line,
                        strokeDashoffset: [ offset, 0 ],
                        duration        : 2000,
                        delay           : 0,
                        loop            : false,
                        direction       : 'alternate',
                        easing          : 'easeInOutSine',
                        autoplay        : true
                    } );

                    //stop animation
                    if ( stop ) {

                        stop.fadeTo(
                            6000,
                            1
                        );

                    }

                } );

            }

        },

        /**
         * @desc show more information
        **/
        more: function()
        {

            //params initializations
            var description = $( '.description p', $( this ).parents( 'li' ) ),
                params      = JSON.parse( $( this ).attr( 'data-details' ) ),
                content     = params.msg;

            //show|hide description
            if ( params.type === 'show' ) {

                //set min and max height
                description.css( {
                    'min-height' : '100%',
                    'max-height' : '100%'
                } );

            } else {

                //set min and max height
                description.css( {
                    'min-height' : params.height + 'px',
                    'max-height' : params.height + 'px'
                } );

            }

            //change msg in form
            params.msg  = $( this ).html();
            params.type = params.type === 'show'
                ? 'hide'
                : 'show';

            $( this ).html( content ).attr(
                'data-details', JSON.stringify( params )
            );

        },

        /**
         * @desc show requested popup window
         * @param current string
        **/
        modal_display: function ( current )
        {

            //param initialization
            var container = $( 'body' );

            $( '.modal-backdrop.in' ).remove();

            //adding model open class
            container
                .addClass( 'modal-open' )
                .append(
                    '<div class="modal-backdrop in" style=""></div>'
                );

            //show up model form
            $( '.modal' ).slideUp();

            //add in class in form
            $( $( current ).data( 'type-id' ) )
                .addClass( 'in' )
                .slideDown();

        },

        /**
         * @desc close popup windows
        **/
        close_modals: function ()
        {

            //params initializations
            var container = $( 'body' ),
                modal     = $( '.main-popup-modal.modal' );

            container
                .removeClass( 'modal-open' )
                .removeAttr( 'style' );

            //hide modal window
            $( this ).parents( '.modal' ).slideUp();

            $( '.modal-backdrop.in' ).remove();

            //removing class and styles from modal window
            modal
                .removeClass( 'in' )
                .removeAttr( 'style' );

        },

        /**
         * @desc animate slider
         * @param params mixed
        **/
        animate: function ( params )
        {

            //run animation
            params.each( function() {

                //param initialization
                var current = $( this );

                current.addClass( current.data( 'animation' ) ).one( 'webkitAnimationEnd animationend', function() {
                    current.removeClass( current.data( 'animation' ) );
                } );

            } );

        },

        /**
         * @desc statistics, services and post custom animations
        **/
        run_animation : function ()
        {

            //params initializations
            var height     = $( window ).height(),
                posts      = $( 'section.posts' ),
                statistics = $( 'section.statistics' ),
                statistic  = $( 'section.statistics ul li' ),
                service    = $( '.post-item' );

            if ( $( document ).scrollTop() < 400 ) {
                $( '.scroll-top' ).fadeOut( 'slow' );
            } else if ( $( document ).scrollTop() > 1000 ) {
                $( '.scroll-top' ).fadeIn( 'slow' );
            }

            if ( typeof posts.offset() !== 'undefined' ) {

                if ( $( document ).scrollTop() >= ( ( posts.offset().top + posts.height()/2 - height/2 ) - 600 ) ) {

                    if ( ! service.parent().hasClass( 'is_visible' ) ) {

                        service.chainFade( {
                            startAt  : 0,
                            interval : 300,
                            speed    : 2500,
                            fx       : 'fade',
                            distance : 50
                        } );

                        service.parent().addClass( 'is_visible' );

                    }

                }

                if ( typeof statistics.offset() !== 'undefined' ) {

                    if ( $( document ).scrollTop() >= ( ( statistics.offset().top + statistics.height()/2 - height/2 ) - 600 )
                        && ! statistic.parent().hasClass( 'is_visible' )
                    ) {

                        statistic.each( function( index, value ) {

                            $( value ).find( 'h3' ).countTo( {
                                from           : 0,
                                to             : parseInt( $( value ).find( 'h3' ).html() ),
                                speed          : 7000,
                                refreshInterval: 50,
                                onComplete     : function( value ) {
                                    console.debug(this);
                                }
                            } );

                        } );

                        statistic.parent().addClass( 'is_visible' );

                    }

                }

            }

        },

        /**
         * @desc scrolling to top of page
        **/
        scroll_top: function()
        {

            $( 'html, body' ).animate( {
                scrollTop: 0
            }, 'slow' );

            return false;

        },

        /**
         * @desc image drop container settings
         * @param type string
        **/
        drop_zone : function ( type )
        {

            Dropzone.options[ 'upload' + type.substr(0,1).toUpperCase() + type.substr(1) + 'Form' ] = {
                url            : $( '#upload-' + type + '-form' ).attr( 'url' ),
                maxFiles       : 1,
                paramName      : "UploadForm[image]",
                thumbnailWidth : "300",
                thumbnailHeight: "300",
                success        : function ( data, file ) {

                    var current = $( '.need-image' );
                    $( '.thumbnail-view' ).attr( 'src', file.url );
                    current.find( '#file_id' ).val( file.id );

                }

            };

        },

        /**
         * @desc search order info due to tracking number
        **/
        search_order: function()
        {

            //param initialization
            var submit = $( 'button', this );

            $.ajax( {
                url    : $( this ).attr( 'action' ),
                type   : 'POST',
                data   : {
                    'tracking_number' : $( '#search' ).val()
                },
                success: function ( response ) {

                    if ( response.results ) {

                        $( '.tracking-popup-modal .modal-footer button' ).hide();

                        $( '.tracking-popup-modal .tracking-title ul li textarea' ).attr( 'disabled', true );

                        $( '.tracking-popup-modal .tracking-title ul li.status .form-group' ).html(
                            response.results.status
                        );

                        frontend_inter_rail.order(
                            response.results,
                            submit
                        );

                    }
                }
            } );

            return false;

        },

        /**
         * @desc showing options for payment confirmation
        **/
        confirm_payment: function()
        {

            $( this ).parents( 'tr' ).nextUntil( 'tr.wait-confirmation' ).css( 'display', function( i, v ) {

                return this.style.display === 'table-row'
                    ? 'none'
                    : 'table-row';

            } );

        }

    };

    //calling current class init ( construct ) method
    frontend_inter_rail.init();

} );