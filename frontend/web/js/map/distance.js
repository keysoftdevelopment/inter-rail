/**
 * Name:        DISTANCE MAP
 * Description: INTER RAIL MAP SCRIPTS
 * Version:     1.0.0
**/

/**
 * @desc params initialization
**/
var map, marker_animation, offsetId;

/**
 * @desc this class will hold functions for inter rail maps side for current service
**/
var inter_rail_map =
{

    /**
     * @desc array of places
    **/
    places : [],

    /**
     * @desc array of markers
    **/
    markers : [],

    /**
     * @desc routes duration
    **/
    duration: 0,

    /**
     * @desc array of search results
    **/
    search : [],

    /**
     * @desc the list of objects of map lines
    **/
    polyline : {
        A  : [],
        B  : [],
        C  : [],
        D  : [],
        E  : [],
        F  : [],
        G  : [],
        H  : [],
        I  : [],
        J  : [],
        K  : [],
        L  : [],
        M  : []
    },

    /**
     * @desc the list of objects of map lines
    **/
    line : {
        A    : [],
        B    : [],
        C    : [],
        D    : [],
        E    : [],
        F    : [],
        G    : [],
        H    : [],
        I    : [],
        J    : [],
        K    : [],
        L    : [],
        M    : [],
        total: []
    },

    /**
     * @desc the list of available vehicles and vehicle details
    **/
    vehicles : {
        current  : 'default',
        segments : [],
        auto     : {
            segments : [],
            color    : '#3dcd1c',
            icon     : './images/map/truck-icon.png',
            marker   : {
                icon  : './images/map/truck-marker.png',
                color : 'green'
            }
        },
        railway : {
            segments : [],
            color    : '#ff3333',
            icon     : './images/map/train-icon.png',
            marker   : {
                icon  : './images/map/train-marker.png',
                color : 'red'
            }
        },
        sea : {
            segments : [],
            color    : '#008CDC',
            icon     : './images/map/ship-icon.png',
            marker   : {
                icon  : './images/map/ship-marker.png',
                color : 'blue'
            }
        },
        default : {
            segments : [],
            color    : '#00a1b1',
            icon     : './images/map/map-icon.png',
            marker   : {
                icon  : './images/map/map-marker.png',
                color : 'skyblue'
            }
        }
    },

    /**
     * @desc map styles
    **/
    styles : [
        {
            "elementType": "geometry",
            "stylers"    : [ { "color": "#ececec" } ]
        }, {
            "elementType": "labels.icon",
            "stylers"    : [ { "visibility": "on" } ]
        }, {
            "elementType": "labels.text.fill",
            "stylers"    : [ { "color": "#616161" } ]
        }, {
            "featureType": "administrative.land_parcel",
            "stylers"    : [ { "visibility": "on" } ]
        },  {
            "featureType": "administrative.neighborhood",
            "stylers"    : [ { "visibility": "on" } ]
        }, {
            "featureType": "poi",
            "elementType": "geometry",
            "stylers"    : [ { "color": "#eeeeee" } ]
        }, {
            "featureType": "poi",
            "elementType": "labels.text",
            "stylers"    : [ { "visibility": "on" } ]
        }, {
            "featureType": "poi.business",
            "stylers"    : [ { "visibility": "on" } ]
        }, {
            "featureType": "water",
            "stylers"    : [ { "visibility": "simplified" }, { "color": "#5AC8FA" } ]
        }, {
            "featureType": "administrative",
            "stylers"    : [ { "lightness": 45 } ]
        }, {
            "featureType": "landscape",
            "stylers"    : [ { "lightness": 90 } ]
        }, {
            "featureType": "administrative",
            "elementType": "geometry.fill",
            "stylers"    : [ { "visibility": "on" } ]
        }, {
            "featureType": "administrative",
            "elementType": "geometry.stroke",
            "stylers"    : [ { "gamma": "3" } ]
        }, {
            "featureType": "poi.park",
            "elementType": "all",
            "stylers"    : [ { "visibility": "on" } ]
        }, {
            "featureType": "road.highway",
            "elementType": "all",
            "stylers"    : [ { "visibility": "simplified" } ]
        }
    ],

    /**
     * @desc initialize script for current class
    **/
    init : function ()
    {

        map = new google.maps.Map( document.getElementById( 'map' ), {
            center: {lat: 41.2827706, lng: 69.1392838},
            zoom            : 3,
            minZoom         : 3,
            maxZoom         : 15,
            disableDefaultUI: true,
            styles          : inter_rail_map.styles,
            backgroundColor : '#5AC8FA'
        } );

    },

    /**
     * @desc draw lines on map
     * @param waypoints mixed
    **/
    draw_lines: function ( waypoints )
    {

        //params initialization for current method
        var routes    = [],
            segment   = 'A',
            locations = [];

        //clear map with previous details
        inter_rail_map.clear();

        if ( waypoints.route ) {

            //set duration
            inter_rail_map.duration = waypoints.route.duration
                ? waypoints.route.duration
                : 0;

            $.each( waypoints.route, function( index, route ) {

                //params initialization for current method
                var waypoint = [],
                    segment  = 'A';

                //current vehicle type
                inter_rail_map.vehicles.current = index;

                for ( var segments in inter_rail_map.line ) {

                    if ( segments !== 'total' && inter_rail_map.vehicles.segments.indexOf(segments) < 0 ) {
                        inter_rail_map.vehicles.segments.push( segments );
                        inter_rail_map.vehicles[ inter_rail_map.vehicles.current ].segments.push( segments );
                        segment = segments;
                        break;
                    }

                }

                //push route details to waypoint
                route.map( depot => {

                    waypoint.push( {
                        'location' : {
                            'lat' : parseFloat( depot.lat ),
                            'lng' : parseFloat( depot.lng )
                        },
                        'city' : depot.city,
                        'name' : depot.name,
                        'code' : depot.country_code
                    } );

                } );

                //routing current vehicles
                routes.push( inter_rail_map.route(
                    waypoint[ 0 ][ 'location' ],
                    waypoint[ waypoint.length - 1 ][ 'location' ],
                    waypoint,
                    segment
                ) );

                //set marker with transportation place icon
                inter_rail_map.set_markers(
                    [
                        waypoint[ 0 ][ 'location' ],
                        waypoint[ waypoint.length - 1 ][ 'location' ]
                    ], waypoint
                );

            } );

        } else if ( waypoints.method === 'locations' ) {

            inter_rail_map.polyline[ segment ].push(
                new google.maps.LatLng( waypoints.origin ),
                new google.maps.LatLng( waypoints.destination )
            );

            inter_rail_map.options( {
                distance : inter_rail_map.distances(
                    waypoints.origin,
                    waypoints.destination
                ).toFixed(2) + ' км',
                duration : 0,
                segment  : segment
            } );

            //set marker with transportation place icon
            inter_rail_map.set_markers(
                [
                    waypoints.origin,
                    waypoints.destination
                ], waypoints.depots
            );

        } else {

            $.map( waypoints.locations, function ( location, key ) {

                locations.push( inter_rail_map.locations(
                    location, 'address'
                ) );

            } );

            Promise.all( locations ).then( function( response ) {

                if ( response.length > 1 ) {

                    inter_rail_map.polyline[ segment ].push(
                        new google.maps.LatLng( response[ 0 ][ 'locations' ] ),
                        new google.maps.LatLng( response[ 1 ][ 'locations' ] )
                    );

                    inter_rail_map.options( {
                        distance : inter_rail_map.distances(
                            response[ 0 ][ 'locations' ],
                            response[ 1 ][ 'locations' ]
                        ).toFixed(2) + ' км',
                        duration : 0,
                        segment  : segment
                    } );

                    //draw lines on map
                    inter_rail_map.result();

                    //set marker with transportation place icon
                    inter_rail_map.set_markers( [
                        response[ 0 ][ 'locations' ],
                        response[ 1 ][ 'locations' ]
                        ], response
                    );

                }

            }, function ( error ) {
                console.log( error.message );
            } );

        }

        Promise.all( routes ).then( function( response ) {

            if ( response[0] && response[0].code === 200 )
                inter_rail_map.result();

        }, function ( error ) {
            console.log( error.message );
        } );

    },

    /**
     * @desc fill in line, polyline and places params with requested details
     * @param origin mixed
     * @param destination mixed
     * @param waypoints mixed
     * @param segment string
    **/
    route : function( origin, destination, waypoints, segment )
    {

        return new Promise( function( resolve, reject ) {

            if ( inter_rail_map.vehicles.current === 'sea' ) {

                inter_rail_map.polyline[ segment ].push(
                    new google.maps.LatLng( origin ),
                    new google.maps.LatLng( destination )
                );

                inter_rail_map.options( {
                    distance : inter_rail_map.distances(
                        origin,
                        destination
                    ).toFixed(2) + ' км',
                    duration: parseInt( inter_rail_map.duration ) > 0
                        ? inter_rail_map.duration + ' day' + ( parseInt( inter_rail_map.duration ) > 1
                            ? 's'
                            : ''
                        )
                        : 0,
                    segment: segment
                } );

                resolve( {
                    'code'   : 200,
                    'status' : 'success',
                    'message': 'All data received'
                } );

            } else {

                inter_rail_map.request(
                    new google.maps.DirectionsService, {
                        origin      : origin,
                        destination : destination,
                        waypoints   : waypoints.map( function ( waypoint ) {
                            return {
                                'location' : waypoint[ 'location' ]
                            }
                        } ),
                        travelMode  : google.maps.TravelMode[ 'DRIVING' ]
                    },
                    segment, 'gmap'
                ).then( function( response ) {

                    if ( response.code === 200 ) {
                        resolve( response );
                    } else {

                        inter_rail_map.request( {
                            locations : {
                                'origin'     : origin.lat + ',' + origin.lng,
                                'destination': destination.lat + ',' + destination.lng
                            }
                        }, [ origin, destination ], segment, 'api' ).then( function( response ) {
                            resolve( response );
                        } );

                    }

                } );

            }

        } );

    },

    /**
     * @desc google maps directions service request
     * @param directionsService mixed
     * @param directionDetails mixed
     * @param segment string
     * @param type string
     * @return mixed
    **/
    request: function( directionsService, directionDetails, segment, type )
    {

        return new Promise( function( resolve, reject ) {

            if ( type === 'api' ) {

                inter_rail_map.polyline[ segment ].push(
                    locations[ 0],
                    locations[ 1 ]
                );

                inter_rail_map.options( {
                    distance: inter_rail_map.distances( locations[ 0], locations[ 1 ] ),
                    duration: parseInt( inter_rail_map.duration ) > 0
                        ? inter_rail_map.duration + ' day' + ( parseInt( inter_rail_map.duration ) > 1
                                ? 's'
                                : ''
                        )
                        : 0,
                    segment: segment
                } );

                resolve( {
                    'code'   : 200,
                    'status' : 'success',
                    'message': 'All data received'
                } );

            } else {

                directionsService.route( directionDetails, function ( response, status ) {

                    if ( status === google.maps.DirectionsStatus.OK ) {

                        $.each( response.routes[0].overview_path, function( i, item ) {
                            inter_rail_map.polyline[ segment ].push( item );
                        } );

                        inter_rail_map.options( {
                            distance: response.routes[ 0 ].legs.length > 1
                                ? response.routes[ 0 ].legs[ 1 ].distance.text
                                : response.routes[ 0 ].legs[ 0 ].distance.text,
                            duration: parseInt( inter_rail_map.duration ) > 0
                                ? inter_rail_map.duration + ' day' + ( parseInt( inter_rail_map.duration ) > 1
                                        ? 's'
                                        : ''
                                )
                                : response.routes[ 0 ].legs.length > 1
                                    ? response.routes[ 0 ].legs[ 1 ].duration.text
                                    : response.routes[ 0 ].legs[ 0 ].duration.text,
                            segment : segment
                        } );

                        resolve( {
                            'code'   : 200,
                            'status' : 'success',
                            'message': 'Places updated'
                        } )

                    } else {

                        resolve( {
                            'code'   : 400,
                            'status' : 'error',
                            'message': 'Bad Request'
                        } );

                    }

                } );

            }

        } );

    },

    /**
     * @desc set markers on map due to requested latitudes and longitudes
     * @param location mixed
     * @param waypoints mixed
    **/
    set_markers : function( location, waypoints )
    {

        var infoWindow = new google.maps.InfoWindow();

        $.each( location, function ( m, _marker ) {

            if ( typeof waypoints[ m ] !== 'undefined' ) {

                inter_rail_map.marker_label(
                    _marker,
                    waypoints[ m ].city !== ''
                        ? waypoints[ m ].city
                        : waypoints[ m ].name,
                    waypoints[ m ].code,
                    infoWindow
                );

            } else {

                inter_rail_map.locations( new google.maps.LatLng( _marker ), 'geocode' ).then( function( response ) {

                    if ( response.city ) {

                        inter_rail_map.marker_label(
                            _marker,
                            response.city,
                            response.country_code,
                            infoWindow
                        );

                    }

                }, function ( error ) {
                    console.log( error.message );
                } );

            }

        } );

    },

    /**
     * @desc label marker styles
     * @param location object
     * @param city string
     * @param country_code string
     * @param info mixed
    **/
    marker_label: function ( location, city, country_code, info )
    {

        //params initializations
        var option = inter_rail_map.vehicles[ inter_rail_map.vehicles.current ].marker,
            marker = new MarkerWithLabel( {
                map      : map,
                animation: google.maps.Animation.DROP,
                position : location,
                icon     : {
                    url       : option.icon,
                    size      : new google.maps.Size(40, 40),
                    scaledSize: new google.maps.Size(40, 40),
                    origin    : new google.maps.Point(0, 0),
                    anchor    : new google.maps.Point(20, 20)
                },
                labelContent     : city,
                labelAnchor      : new google.maps.Point(40, -15),
                labelClass       : 'map-label ' + option.color ,
                labelInBackground: true
            } );

        google.maps.event.addListener( marker, 'click', function() {
            info.setContent( '<div style="line-height:1.35;overflow:hidden;white-space:nowrap;">' +
                '<div><b>'+ city +'</b></div>' +
                'Country: <span class="flag-icon-small flag-icon-small-' + country_code !== 'NULL' && country_code !== null && country_code !== ''
                    ? country_code.toLowerCase()
                    : country_code
                + '"></span>' + country_code +'' +
                '<div>Lat: ' + location[ 'lat' ]  + '</div>' +
                '<div>Lng: ' + location[ 'lng' ]  + '</div>' +
                '</div>' );
            info.open( map, marker );
        } );

        this.markers.push( marker );

    },

    /**
     * @desc draw lines and add animation due to places details
     * i.e places is filled from
    **/
    result : function ()
    {

        if( inter_rail_map.places ) {

            //param initialization
            var infoWindow = new google.maps.InfoWindow();

            $.each( inter_rail_map.places, function ( index, opt ) {

                inter_rail_map.line[ opt.segment ] = new google.maps.Polyline( {
                    path         : inter_rail_map.polyline[ opt.segment ],
                    strokeColor  : inter_rail_map.current_segment( opt.segment ).color,
                    strokeWeight : 3,
                    clickable    : true,
                    strokeOpacity: 1,
                    map          : map
                } );

                google.maps.event.addListener( inter_rail_map.line[ opt.segment ], 'click', function( line ) {

                    infoWindow.setContent( '<div class="map-line-info">' +
                        '<img src="./images/logo.png">' +
                        '<div class="distance">' +
                            '<i class="fa fa-map-marker"></i> Distance: <span>' + opt.distance + '</span>' +
                        '</div>' +
                        '<div class="transit">' +
                            '<i class="fa fa-clock-o"></i> Transit Time: <span>' + opt.duration + '</span>' +
                        '</div>' +
                    '</div>' );

                    infoWindow.setPosition( line
                        ? line.latLng
                        : line
                    );

                    infoWindow.open( map );

                } );

                google.maps.event.addListener( inter_rail_map.line[ opt.segment ], 'mouseover', function() {
                    inter_rail_map.line[ opt.segment ].setOptions( {
                        'strokeWeight': 6
                    } );
                } );

                google.maps.event.addListener( inter_rail_map.line[ opt.segment ], 'mouseout', function() {
                    inter_rail_map.line[ opt.segment ].setOptions( {
                        'strokeWeight': 3
                    } );
                } );

            } );

            google.maps.event.addListener( map, "click", function () {
                infoWindow.close();
            } );

            // check array and create array with Polyline obj
            for ( var segment in inter_rail_map.polyline ) {

                if( inter_rail_map.polyline[ segment ].length !== 0 ) {

                    if ( google.maps.geometry ) {

                        var dist = google.maps.geometry.spherical.computeLength(
                            inter_rail_map.polyline[ segment ], 0
                        );

                        inter_rail_map.line.total.push( new google.maps.Polyline( {
                            path        : inter_rail_map.polyline[ segment ],
                            distance    : dist / 1000,
                            key         : segment,
                            dashed      : false,
                            strokeWeight: 0,
                            map         : map
                        } ) );

                    }

                }

            }

            //main marker animation method
            inter_rail_map.animations();

        }

    },

    /**
     * @desc adding animation to marker
    **/
    animations: function()
    {

        //params initializations
        var offset  = 0,
            count   = 0;

        //set marker animation
        marker_animation = new google.maps.Marker( {
            map: map
        } );

        //set animations interval
        offsetId = window.setInterval( function() {

            //set count due to line length
            count = count > inter_rail_map.line.total.length - 1
                ? 0
                : count;

            if( inter_rail_map.line.total[count] !== undefined ) {

                //set marker animation icon
                marker_animation.setIcon( {
                    url       : inter_rail_map.current_segment( inter_rail_map.line.total[count].key ).icon,
                    size      : new google.maps.Size(32, 40),
                    scaledSize: new google.maps.Size(32, 40),
                    origin    : new google.maps.Point(0, 0),
                    anchor    : new google.maps.Point(16, 40)
                } );

                //set new position
                marker_animation.setPosition( inter_rail_map.polyline_distance(
                    inter_rail_map.line.total[count],
                    offset
                ) );

                //set offset
                if ( inter_rail_map.line.total[ count ].distance < 300 && inter_rail_map.line.total.length > 1 )
                    offset += 10;
                else if ( inter_rail_map.line.total[ count ].distance < 1000 && inter_rail_map.line.total.length > 1 )
                    offset += 6;
                else if ( inter_rail_map.line.total[ count ].distance < 2000 && inter_rail_map.line.total.length > 1 )
                    offset += 4;
                else if ( inter_rail_map.line.total[ count ].distance < 5000 && inter_rail_map.line.total.length > 1 )
                    offset += 1;
                else
                    offset += 0.5;

                //reset to zero
                if ( offset >= 100 ) {
                    offset = 0;
                    count++;
                }

            }

        }, 100 );

    },

    /**
     * @desc calculate approximately distance between 2 points
     * @param origin object
     * @param destination object
     * @return number
    **/
    distances: function( origin, destination )
    {

        //params initializations
        var radius       = 6378137,
            latitude     = ( destination.lat - origin.lat ) * Math.PI/ 180,
            longitude    = ( destination.lng - origin.lng ) * Math.PI/ 180,
            lat_lng_calc = ( Math.sin( latitude / 2 ) * Math.sin( latitude / 2 ) ) + Math.cos( ( origin.lat ) * Math.PI/ 180 ) * Math.cos( ( destination.lat ) * Math.PI/ 180 ) * Math.sin( longitude / 2 ) * Math.sin( longitude / 2 ),
            distance        = 2 * Math.atan2(
                    Math.sqrt( lat_lng_calc ),
                    Math.sqrt( 1 - lat_lng_calc )
                );

        return radius * distance;

    },

    /**
     * @desc convert timestamp to date
     * @param timestapm number
     * @param time_unit string
     * @return string
    **/
    timestamp_to_date : function ( timestapm, time_unit )
    {

        //params initializations
        var result = '',
            dec    = 86400000;

        if( time_unit === 's' ) {
            dec = 86400;
        }

        //assign days and hours with received timestamp info
        var days  = timestapm / dec,
            hours = Math.round( ( days - Math.floor( days ) ) * 24 );

        days = days - ( days - Math.floor( days ) );

        if( days !== 0 ) {

            result += days + ( days === 1
                        ? ' day '
                        : ' days '
                );

        }

        result += hours + ( hours === 1
              ? ' hour'
              : ' hours'
            );

        return result;

    },

    /**
     * @desc get distance due to requested poly line
     * @param polyline mixed
     * @param offset integer
     * @return object|mixed
    **/
    polyline_distance: function( polyline, offset )
    {

        //params initializations
        var dist           = 0,
            distanse       = google.maps.geometry.spherical.computeLength( polyline.getPath(), 0 ),
            offsetDistanse = distanse * offset / 100.00,
            points         = polyline.getPath().getArray();

        if( points.length > 0 ) {

            for( var i = 0; i < points.length - 1; i++ ) {

                var pointStart  = new google.maps.LatLng( points[i].lat(), points[i].lng() ),
                    pointEnd    = new google.maps.LatLng( points[i + 1].lat(), points[i + 1].lng() ),
                    curDistance = google.maps.geometry.spherical.computeDistanceBetween( pointStart, pointEnd ),
                    prevDist    = dist;

                dist += curDistance;

                if ( Math.abs( pointEnd.lng() - pointStart.lng() ) > 180 )
                    i++;

                if( offsetDistanse <= dist ) {

                    var diff = offsetDistanse - prevDist;
                    diff = diff / curDistance;
                    var d = diff / (1.0 - diff);

                    return new google.maps.LatLng(
                        ( points[i].lat() + ( d * points[i + 1].lat() ) ) / ( 1.0 + d ),
                        ( points[i].lng() + ( d * points[i + 1].lng() ) ) / ( 1.0 + d )
                    );

                }

            }

        }

    },

    /**
     * @desc clear all details from map
     * i.e like markers, closing all info window and etc ...
    **/
    clear: function()
    {

        //hide infowindows
        google.maps.event.trigger( map, 'click' );

        if ( inter_rail_map.line.total.length !== 0 ) {

            //disable animation
            clearInterval( offsetId );

            //disable markers
            if( marker_animation )
                marker_animation.setMap( null );

            //hide lines
            $.each( inter_rail_map.line.total, function ( index, line ) {

                if( line )
                    line.setMap( null );

            } );

        }

        //pop vehicle segments
        $.map( inter_rail_map.vehicles, function( vehicle, index ) {

            if ( typeof vehicle.segments !== 'undefined' )
                vehicle.segments.pop();

        } );

        //reverting vehicle segments
        inter_rail_map.vehicles.segments.pop();

        //remove all info about lines
        $.map( inter_rail_map.line, function( segment, key ) {

            if ( key !== 'total' && segment.length !== 0 ) {

                segment.setMap(
                    null
                );

                inter_rail_map.polyline[ key ] = [];

            }

            inter_rail_map.line[ key ] = [];

        } );

        //empty places details
        inter_rail_map.places.pop();

        //clear markers on map
        $.map( this.markers, function( marker, key ) {
            marker.setMap( null );
        } );

        this.markers.pop();

    },

    /**
     * @desc pushing details to places param
     * @param option mixed
    **/
    options: function( option )
    {

        inter_rail_map.places.push( option );
        inter_rail_map.places.sort( function( a, b ) {

            var response  = 0,
                segment_a = a.segment.toLowerCase(),
                segment_b = b.segment.toLowerCase();

            if ( segment_a < segment_b ) {
                response = -1;
            } else if ( segment_a > segment_b ) {
                response = 1;
            }

            return response;

        } );

    },

    /**
     * @desc current vehicle due to requested segment
     * @param segment string
     * @return object
    **/
    current_segment: function( segment )
    {

        //param initialization
        var vehicle = inter_rail_map.vehicles.default;

        //vehicle options due to segment
        $.map( inter_rail_map.vehicles, function( value, index ) {

            if ( value.segments && value.segments.indexOf( segment ) >= 0 ) {

                vehicle = inter_rail_map.vehicles[ index ];

                return false;

            }

        } );

        return vehicle;

    },

    /**
     * @desc get location details due to requested latitude and longitude
     * @param request object|mixed
     * @param type string
     * @return object
    **/
    locations: function( request, type )
    {

        return new Promise( function( resolve, reject ) {

            var country     = null,
                countryCode = null,
                city        = null,
                response    = {},
                params      = {
                    'latLng' : request
                };

            if ( type = 'address' )
                params = {
                    'address' : request
                };

            new google.maps.Geocoder().geocode( params, function( results, status ) {

                if ( status === google.maps.GeocoderStatus.OK ) {

                    if ( results[1] || results[0] && type === 'address' ) {

                        for ( var r = 0, rl = results.length; r < rl; r += 1 ) {

                            var result = results[r];

                            if ( ! city && result.types[0] === 'locality' || ! country && result.types[0] === 'locality' ) {

                                for ( var c = 0; c < result.address_components.length; c += 1 ) {

                                    var component = result.address_components[c];

                                    if ( component.types[0] === 'locality' && ! city ) {

                                        city = component.long_name;

                                        if ( country )
                                            break;

                                    }

                                    if ( component.types[0] === 'country' ) {
                                        country     = component.long_name;
                                        countryCode = component.short_name;
                                        break;
                                    }

                                }

                            } else if ( ! country && result.types[0] === 'country' ) {
                                country     = result.address_components[0].long_name;
                                countryCode = result.address_components[0].short_name;
                            }

                            if ( city && country )
                                break;

                        }

                        if ( type === 'address' ) {

                            response = {
                                'locations' : {
                                    'lat' : results[0].geometry.location.lat(),
                                    'lng' : results[0].geometry.location.lng()
                                },
                                'country' : country,
                                'code'    : countryCode,
                                'city'    : request
                            };

                        } else {

                            response = {
                                'country'     : country,
                                'country_code': countryCode,
                                'city'        : city
                            }

                        }

                        resolve(
                            response
                        );

                    } else {

                        resolve( {
                            'code'   : 400,
                            'status' : 'error',
                            'message': 'Bad Request'
                        } );

                    }

                }

            } );

        } );

    },

    /**
     * @desc auto filling checkout fields
     * with appropriate google results
    **/
    places_search: function ()
    {
        inter_rail_map.search.getPlaces();
    }

};

/**
 * @desc calling current class init ( construct ) method
**/
inter_rail_map.init();