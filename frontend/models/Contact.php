<?php namespace frontend\models;

/**************************************/
/*                                    */
/*          CONTACT MODEL             */
/*                                    */
/**************************************/

use Yii;

use yii\base\Model;

use common\commands\SendEmailCommand;

/**
 * Contact is the model behind the contact form.
**/
class Contact extends Model
{

	/**
	 * @inheritdoc
	**/
    public $name;

	/**
	 * @inheritdoc
	**/
	public $company;

	/**
	 * @inheritdoc
	**/
    public $email;

	/**
	 * @inheritdoc
	**/
    public $phone;

	/**
	 * @inheritdoc
	**/
    public $message;

    /**
     * @inheritdoc
    **/
    public function rules()
    {

        return [
            [
            	[
            		'name',
					'company',
					'email',
					'phone',
					'message'
				], 'required'
			], [
				'email', 'email'
			]
        ];

    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     * @param string $email the target email address
     * @return boolean whether the email was sent
    **/
    public function sendEmail( $email )
    {

		return Yii::$app->commandBus->handle( new SendEmailCommand( [
			'to'      => $email,
			'subject' => __( 'Contact Us' ),
			'view'    => 'contact',
			'params'  => [
				'contact' => [
					'company' => $this->company,
					'email'   => $this->email,
					'name'    => $this->name,
					'phone'   => $this->phone,
					'message' => $this->message
				]
			]
		] ) );

    }

}