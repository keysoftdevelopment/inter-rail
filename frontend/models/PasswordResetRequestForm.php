<?php namespace frontend\models;

/**************************************/
/*                                    */
/*    PASSWORD RESET REQUEST MODEL    */
/*                                    */
/**************************************/


use Yii;

use yii\base\Model;

use common\models\User;
use common\commands\SendEmailCommand;

/**
 * Password reset request form
**/
class PasswordResetRequestForm extends Model
{

	/**
	 * @inheritdoc
	**/
    public $email;

    /**
     * @inheritdoc
    **/
    public function rules()
    {

        return [
            [
            	'email',
				'trim'
			], [
				'email',
				'required'
			], [
				'email',
				'email'
			], [
                'email', 'exist',
                'targetClass' => '\common\models\User',
                'filter'      => [
                	'status' => User::STATUS_ACTIVE
				],
                'message' => __( 'There is no user with such email' )
            ]
        ];

    }

    /**
     * Sends an email with a link, for resetting the password
     * @return boolean
    **/
    public function sendEmail()
    {

    	//param initialization
        $user = User::findOne( [
            'status' => User::STATUS_ACTIVE,
            'email'  => $this->email,
        ] );

        if ( $user && ! User::isPasswordResetTokenValid(
        	$user->password_reset_token
		) ) {

        	//generate password reset token
            $user->generatePasswordResetToken();

            //set update scenario
			$user->setScenario(
				$user::SCENARIO_UPDATE
			);

            if ( ! $user->save() )
                return false;

        }

        return ! $user
			? false
			: Yii::$app->commandBus->handle( new SendEmailCommand( [
				'to'      => $this->email,
				'subject' => __( 'Password reset for' ) . ' ' . Yii::$app->name,
				'view'    => 'passwordReset',
				'params'  => [
					'user' => $user
				]
			] ) );

    }

}