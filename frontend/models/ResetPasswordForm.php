<?php namespace frontend\models;

/**************************************/
/*                                    */
/*    PASSWORD RESET REQUEST MODEL    */
/*                                    */
/**************************************/

use yii\base\Model;
use yii\base\InvalidParamException;

use common\models\User;

/**
 * Password reset form
**/
class ResetPasswordForm extends Model
{

	/**
	 * @inheritdoc
	**/
    public $password;

    /**
     * @var \common\models\User
    **/
    private $_user;

	/**
	 * @inheritdoc
	**/
	public function rules()
	{

		return [
			[
				'password',
				'required'
			], [
				'password',
				'string',
				'min' => 6
			]
		];

	}

    /**
     * Creates a form model given a token.
     * @param string $token
     * @param array $config name-value pairs that will be used to initialize the object properties
     * @throws \yii\base\InvalidParamException if token is empty or not valid
    **/
    public function __construct( $token, $config = [ ] )
    {

		//is requested token empty
        if ( empty( $token )
			|| !is_string( $token )
		) {

			throw new InvalidParamException(
				__( 'Password reset token cannot be blank' )
			);

		}

		//find user by requested token
        $this->_user = User::findByPasswordResetToken(
        	$token
		);

        //token validation
        if ( !$this->_user ) {

			throw new InvalidParamException(
				__( 'Wrong password reset token' )
			);

		}

        //set configurations
        parent::__construct(
        	$config
		);

    }

    /**
     * Resets password.
     * @return boolean if password was reset.
    **/
    public function resetPassword()
    {

    	//param initialization
		$customer = $this->_user;

    	//set new password
		$customer->setPassword( $this->password );

		//remove password reset token
		$customer->removePasswordResetToken();

		//set update scenario
		$customer->setScenario(
			$customer::SCENARIO_UPDATE
		);

        return $customer->save(
        	false
		);

    }

}