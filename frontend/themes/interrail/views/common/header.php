<?php /***************************************************************
 *         		 	   	      HEADER SECTION                         *
 *********************************************************************/

use yii\helpers\Url;

use common\models\Languages;
use common\models\User;

/** @var $this \yii\web\View  */

//the list of all available languages in system
$languages = \Yii::$app->common->getLanguages();

//current language, which was chosen by customer or which is default one
$current_language = \Yii::$app->common->currentLanguage(); ?>

    <!-- header top navigation section -->
    <section class="top">

        <div class="container">

            <div class="row">

                <!-- logo section -->
                <div class="col-sm-5 col-md-5">

                    <a href="<?= Yii::getAlias( '@web' ) ?>/" class="logo">
                        <img src="<?= Yii::getAlias( '@web' ) ?>/images/logo.png" />
                    </a>

                </div>
                <!-- logo section -->

                <!-- top navigation -->
                <div class="col-sm-7 col-md-7">

                    <ul class="top-navigation">

                        <li class="personal-info">

                            <?php if ( Yii::$app->user->isGuest ) { ?>

                                <a href="javascript:void(0)" data-type-id="#login-form" class="nav show-popup">
                                    <i class="fa fa-user"></i>
                                    <?= __( 'Authorization' ) ?>
                                </a>

                            <?php } else {

								$current_user = User::findOne(
                                    Yii::$app->user->id
                                ); ?>

                                <div class="dropdown">

                                    <a href="javascript:void(0)" class="nav dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">

                                        <i class="fa fa-user"></i>

                                        <?= ! empty( $current_user->first_name ) || ! empty( $current_user->last_name )
                                            ? $current_user->first_name . ' ' . $current_user->last_name
                                            : $current_user->email
                                        ?>

                                    </a>

                                    <ul class="dropdown-menu">

                                        <li>
                                            <a href="<?= Url::to( [ '/cabinet' ] ) ?>">
                                                <?= __( 'Personal Cabinet' ) ?>
                                            </a>
                                        </li>

                                        <li>
                                            <a href="<?= Url::to( [ '/main/main/logout' ] ) ?>">
												<?= __( 'Log Out' ) ?>
                                            </a>
                                        </li>


                                    </ul>

                                </div>

                            <?php } ?>

                        </li>

                        <li>

                            <ul class="language-list">

                                <?php if ( isset( $languages )
                                && !empty ( $languages )
                                ) {

                                    $current_language = Languages::findOne(
                                        \Yii::$app->common->currentLanguage()
                                    ); ?>

                                    <li class="dropdown">

                                        <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                            <?= $current_language->description ?>
                                            <span class="caret"></span>
                                        </a>

                                        <ul class="dropdown-menu">

                                            <?php foreach ( $languages as $language ) { ?>

                                                <li>

                                                    <a href="<?= Url::to( [
														'/language/change/'. $language[ 'id' ]
													] ) ?>">
                                                        <?= $language[ 'description' ] ?>
                                                    </a>

                                                </li>

                                            <?php } ?>

                                        </ul>

                                    </li>

                                <?php } ?>

                            </ul>

                        </li>

                        <li>

                            <a href="<?= Url::to( [
                                '/checkout'
                            ] ) ?>" class="btn btn-info">
                                <?= __( 'Order for transportation' ) ?>
                            </a>

                        </li>

                    </ul>

                </div>
                <!-- top navigation -->

                <!-- mobile main menu navigation -->
                <div class="navbar-header">

                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main_navbar" aria-expanded="false">

                        <span class="bars">
                            <span></span>
                            <span></span>
                            <span></span>
                        </span>

                    </button>

                </div>
                <!-- mobile main menu navigation -->

            </div>

        </div>

    </section>
    <!-- header top navigation section -->