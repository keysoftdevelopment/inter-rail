<?php /***************************************************************
 *         		 	   	      FOOTER SECTION                         *
 *********************************************************************/

use common\models\Settings;

use frontend\widgets\Login;
use frontend\widgets\RecoverPassword;
use frontend\widgets\Tracking;
use yii\helpers\Html;

//settings details, added in admin panel -> settings tab
$settings = Settings::find()
	-> where( [
		'name' => [
			'address',
			'e_mail',
			'ga_code',
			'gplus',
			'phone',
			'facebook',
			'telegram',
			'whatsapp',
			'weechat',
            'youtube'
		]
	] )
	-> indexBy( 'name' )
	-> asArray()
	-> all(); ?>

    <!-- login popup modal window -->
    <?= Login::widget( [
        'support_mail' => isset( $settings[ 'e_mail' ] )
            ? $settings[ 'e_mail' ][ 'description' ]
            : ''
    ] ); ?>
    <!-- login popup modal window -->

    <!-- password recovery popup modal window -->
    <?= RecoverPassword::widget( [
        'support_mail' => isset( $settings[ 'e_mail' ] )
            ? $settings[ 'e_mail' ][ 'description' ]
            : ''
    ] ); ?>
    <!-- password recovery popup modal window -->

    <!-- trace and tracking popup modal window -->
    <?= Tracking::widget(); ?>
    <!-- trace and tracking popup modal window -->

    <!-- footer -->
    <footer>

        <!-- footer-details-area -->
        <div class="footer-details-area">

            <div class="container">

                <div class="row">

                    <div class="col-sm-5 col-md-5">

                        <a href="javascript:void(0)">
                            <img src="<?= Yii::getAlias( '@web' ) ?>/images/logo.png">
                        </a>

                    </div>

                    <div class="col-sm-7 col-md-7 support-location-container">

                        <div class="col-sm-6 col-md-6">

                            <i class="support-icon"></i>

                            <h6>
								<?= __( 'Call center' ) ?>
                            </h6>

                            <ul>

                                <li>

									<?= Html::a( '<i class="fa fa-paper-plane-o"></i>', isset( $model[ 'telegram' ][ 'description' ] )
									&& ! empty( $model[ 'telegram' ][ 'description' ] )
										? $model[ 'telegram' ][ 'description' ]
										: 'javascript:void(0)'
									); ?>

                                </li>

                                <li>

									<?= Html::a( '<i class="fa fa-weixin"></i>', isset( $model[ 'weechat' ][ 'description' ] )
									&& ! empty( $model[ 'weechat' ][ 'description' ] )
										? $model[ 'weechat' ][ 'description' ]
										: 'javascript:void(0)'
									); ?>

                                </li>

                                <li>

									<?= Html::a( '<i class="whatsapp-icon"></i>', isset( $model[ 'whatsapp' ][ 'description' ] )
									&& ! empty( $model[ 'whatsapp' ][ 'description' ] )
										? $model[ 'whatsapp' ][ 'description' ]
										: 'javascript:void(0)'
									); ?>

                                </li>

                            </ul>

                            <span class="phone-number">

                                <?= __( 'Phone/Fax' ) ?>: <?= isset( $settings[ 'phone' ][ 'description' ] )
									? $settings[ 'phone' ][ 'description' ]
									: ''
								?>

                            </span>

                        </div>

                        <div class="col-sm-6 col-md-6">

                            <i class="location-icon"></i>

                            <h6>
                                <?= __( 'Location' ) ?>
                            </h6>

                            <p>

								<?= isset( $settings[ 'address' ][ 'description' ] )
									? $settings[ 'address' ][ 'description' ]
									: ''
								?>

                            </p>

                        </div>

                    </div>

                </div>

            </div>

        </div>
        <!-- footer-details-area -->

        <!-- copyright -->
        <div class="copyright text-center">

            <h5 class="text-center">
                <?= __( 'Deliver on time' ) ?>
            </h5>

            <ul>

                <li>

					<?= Html::a( '<i class="fa fa-facebook"></i>', isset( $settings[ 'facebook' ][ 'description' ] )
					&& ! empty( $settings[ 'facebook' ][ 'description' ] )
						? $settings[ 'facebook' ][ 'description' ]
						: 'javascript:void(0)', [
                            'class' => 'facebook'
                        ]
					); ?>

                </li>

                <li>

					<?= Html::a( '<i class="whatsapp-icon"></i>', isset( $settings[ 'whatsapp' ][ 'description' ] )
					&& ! empty( $settings[ 'whatsapp' ][ 'description' ] )
						? $settings[ 'whatsapp' ][ 'description' ]
						: 'javascript:void(0)', [
							'class' => 'whatsapp'
						]
					); ?>

                </li>

                <li>

					<?= Html::a( '<i class="fa fa-youtube-play"></i>', isset( $settings[ 'youtube' ][ 'description' ] )
					&& ! empty( $settings[ 'youtube' ][ 'description' ] )
						? $settings[ 'youtube' ][ 'description' ]
						: 'javascript:void(0)', [
							'class' => 'youtube'
						]
					); ?>

                </li>

                <li>

					<?= Html::a( '<i class="fa fa-google-plus"></i>', isset( $settings[ 'gplus' ][ 'description' ] )
					&& ! empty( $settings[ 'gplus' ][ 'description' ] )
						? $settings[ 'gplus' ][ 'description' ]
						: 'javascript:void(0)', [
							'class' => 'gplus'
						]
					); ?>

                </li>

            </ul>

            <div class="site-info text-center">
                © <?= date( 'Y', strtotime( 'today' ) ) ?> InterRail Services AG
            </div>

        </div>
        <!-- copyright -->

    </footer>
    <!-- footer -->

    <!-- scroll to top -->
    <div class="scroll-top hidden-elem">

        <a href="javascript:void(0)">
            <i class="fa fa-angle-double-up"></i>
        </a>

    </div>
    <!-- scroll to top -->

    <!-- Google Analytics -->
    <script type="text/javascript">

        ( function ( i, s, o, g, r, a, m ) {

            i[ 'GoogleAnalyticsObject' ] = r;

            i[ r ] = i[ r ] || function () {
                (i[ r ].q = i[ r ].q || []).push( arguments )
            }, i[ r ].l = 1 * new Date();

            a = s.createElement( o ),
            m = s.getElementsByTagName( o )[ 0 ];

            a.async = 1;
            a.src   = g;
            m.parentNode.insertBefore( a, m )

        } )( window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga' );

        ga( 'create', '<?= isset( $settings[ 'ga_code' ][ 'description' ] ) ? $settings[ 'ga_code' ][ 'description' ] : '' ?>', 'auto' );
        ga( 'send', 'pageview' );

    </script>
    <!-- End Google Analytics -->