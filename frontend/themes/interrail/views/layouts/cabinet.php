<?php /***************************************************************
 *     		           PERSONAL CABINET LAYOUT                       *
 *********************************************************************/

/* @var $this \yii\web\View */
/* @var $content string     */

use yii\helpers\Html;

use frontend\assets\AppAsset;

AppAsset::register( $this );

$this->beginPage();

//notification success messages
$notification = \Yii::$app->getSession()->getAllFlashes() ?>

	<!DOCTYPE html>
	<html lang="en">

        <head>

            <meta charset="<?= Yii::$app->charset ?>">

            <meta name="viewport" content="width=device-width">

            <title>
                <?= Html::encode( $this->title ) ?>
            </title>

            <link href="<?= Yii::getAlias('@web') ?>/favicon.ico" rel="shortcut icon" type="image/x-icon">

            <meta name="robots" content="noodp">

            <meta property="og:url" content="<?= Yii::getAlias('@frontend_web') ?>">

            <meta name="mailru-domain" content="yRsyOWOh41U7iIFL" />

            <!----------------------------- SEO TAGS --------------------------->

            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

            <?= Html::csrfMetaTags() ?>

            <?php $this->head() ?>

        </head>

        <body>

            <?php $this->beginBody(); ?>

                <!-- header -->
                <header>
                    <?= $this->render('//common/header'); ?>
                </header>
                <!-- header -->

                <?php if ( isset( $notification[ 'success' ] ) ) : ?>

                    <!-- notification message -->
                    <section class="notification">

                        <div class="container">

                            <div class="row">

                                <div class="alert alert-success text-center" role="alert">
                                    <?= $notification[ 'success' ] ?>
                                </div>

                            </div>

                        </div>

                    </section>
                    <!-- notification message -->

                <?php endif; ?>

                <?= $content ?>

                <?= $this->render( '//common/footer' );

            $this->endBody(); ?>

        </body>

	</html>

<?php $this->endPage(); ?>