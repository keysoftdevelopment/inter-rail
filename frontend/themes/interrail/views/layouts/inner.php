<?php /***************************************************************
 *         		 	   	      DEFAULT LAYOUT                         *
 *********************************************************************/

/* @var $this \yii\web\View */
/* @var $content string     */

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\bootstrap\Nav;

use common\models\Settings;

use frontend\assets\AppAsset;

use frontend\widgets\Alert;
use frontend\widgets\ContactForm;
use frontend\widgets\RecentNews;

AppAsset::register( $this );

$this->beginPage();

//support email, added in admin panel -> settings tab
$settings = Settings::findOne( [
    'name' => 'e_mail'
] ); ?>

<!DOCTYPE html>

<html lang="en">

    <head>

        <meta charset="<?= Yii::$app->charset ?>">

        <meta name="viewport" content="width=device-width">

        <title>
            <?= Html::encode( $this->title ) ?>
        </title>

        <link href="<?= Yii::getAlias('@web') ?>/favicon.ico" rel="shortcut icon" type="image/x-icon">

        <meta name="robots" content="noodp">

        <meta property="og:url" content="<?= Yii::getAlias('@frontend_web') ?>">

        <meta name="mailru-domain" content="yRsyOWOh41U7iIFL" />

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <?= Html::csrfMetaTags() ?>

        <?php $this->head() ?>

    </head>

    <body>

        <?php $this->beginBody(); ?>

        <!-- header -->
        <header>

            <?= $this->render('//common/header'); ?>

            <!-- header bottom navigation section -->
            <section class="bottom">

                <div class="container">

                    <div class="row">

                        <div class="container">

                            <!-- main menu container -->
                            <div class="collapse navbar-collapse" id="main_navbar">

								<?php $menus     = \Yii::$app->common->menu();
								$menuItems       = [];
								$current_action  = str_replace( 'page/', '', Yii::$app->request->pathInfo );
								$templates       = \Yii::$app->common->customFieldsQuery(
									'template',
									ArrayHelper::getColumn(
										$menus, 'id'
									)
								);

								foreach ( $menus as $menu ) {

									//params initializations
									$active   = false;
									$template = isset( $templates[ $menu->id ][ 'description' ] )
										? $templates[ $menu->id ][ 'description' ]
										: '';

									//generate link due to template
									$url = in_array( $template, [ 'contact', 'services', 'home', 'post' ] )
										? '/' . str_replace( [ 'home', 'post' ], [ '', 'news' ], $template )
										: $menu->link;

									if ( empty( $current_action ) && $url == '/' ) {
										$active = true;
									} else if ( $current_action == str_replace( [
											'/', 'page'
										], '', $url ) ) {
										$active = true;
									}

									//update menu items array
									$menuItems[] = [
										'id'     => $menu->id,
										'active' => $active,
										'label'  => $menu->name,
										'url'    => [
											$url
										]
									];

								}

								echo Nav::widget( [
									'options' => [
										'class' => 'nav navbar-nav'
									],
									'items' => $menuItems,
								] ); ?>

                            </div>
                            <!-- main menu container -->

                        </div>

                    </div>

                </div>

            </section>
            <!-- header bottom navigation section -->

        </header>
        <!-- header -->

        <?= $content ?>

        <!-- recent-news -->
		<?= RecentNews::widget(); ?>
        <!-- recent-news -->

        <!-- contact-us -->
		<?= ContactForm::widget(); ?>
        <!-- contact-us -->

        <!-- notifications popup modal window -->
		<?= Alert::widget( [
			'support_mail' => ! is_null( $settings )
				? $settings->description
				: ''
		] ); ?>
        <!-- notifications popup modal window -->

        <?= $this->render( '//common/footer' );

        $this->endBody(); ?>

    </body>

</html>

<?php $this->endPage(); ?>