<?php /***************************************************************
 *  	    	             ARTICLE LIST PAGE                       *
 *********************************************************************/

/* @var $this yii\web\View                        */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel common\models\PostsSearch    */
/* @var $page common\models\Posts 				  */
/* @var $thumbnails array                	      */

use yii\helpers\Url;
use yii\widgets\LinkPager;

use frontend\widgets\DeliveryTrackingForm;
use frontend\widgets\Slider;

//current page title, that will displayed in head tags
\Yii::$app->common->setSeoTags(
	! empty( $page->seo )
		? json_encode( $page->seo )
		: '',
	$page->title
); ?>

    <!-- slider section -->
	<?= Slider::widget( [
		'slider_id' => $page->slider
	] ); ?>
    <!-- slider section -->

    <!-- delivery tracking form -->
	<?= DeliveryTrackingForm::widget(); ?>
    <!-- delivery tracking form -->

	<!-- articles lists section -->
	<section class="articles">

		<div class="container">

			<div class="row">

				<h2 class="title text-center">
					<?= $page->title ?>
				</h2>

                <?php if ( ! empty( $dataProvider->models ) ) :  ?>

                    <ul>

                        <?php foreach( $dataProvider->models as $model ) :

                            $thumbnail = isset( $thumbnails[ $model->file_id ] )
                                ? $thumbnails[ $model->file_id ][ 'guide' ]
                                : Yii::getAlias( '@web' ) . '/images/image-not-found.jpg'

                            ?>

                            <li class="col-sm-4 col-md-4 col-xs-12">

                                <div class="article-title">

                                    <a href="<?= Url::to( [ '/single-article/' . $model->slug ] ) ?>">
                                        <img src="<?= $thumbnail ?>">
                                    </a>

                                    <h4>
                                        <a href="<?= Url::to( [ '/single-article/' . $model->slug ] ) ?>">
                                            <?= $model->title ?>
                                        </a>
                                    </h4>

                                </div>

                                <div class="article-content">

                                    <?= Yii::$app->common->excerpt( $model->description, 200 ); ?> ...

                                    <a href="<?= Url::to( [ '/single-article/' . $model->slug ] ) ?>" class="read-more">
										<?= __( 'Read More' ) ?>
                                    </a>

                                </div>

                            </li>

                         <?php endforeach; ?>

                    </ul>

                <?php endif; ?>

                <!-- pagination container -->
				<?php if ( $dataProvider->pagination->totalCount ) : ?>

					<div class="pagination-container">

						<?= LinkPager::widget( [
							'pagination'     => $dataProvider->pagination,
							'maxButtonCount' => 7,
                            'class'          => 'pagination'
						] ); ?>

					</div>

				<?php endif; ?>
				<!-- pagination container -->

			</div>

		</div>

	</section>
    <!-- articles lists section -->