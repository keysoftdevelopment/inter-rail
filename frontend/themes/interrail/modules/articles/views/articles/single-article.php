<?php /***************************************************************
 *  	    	            SINGLE ARTICLE PAGE                      *
 *********************************************************************/

/* @var $this yii\web\View         */
/* @var $model common\models\Posts */

use frontend\widgets\DeliveryTrackingForm;
use frontend\widgets\Slider;

//current page title, that will displayed in head tags
\Yii::$app->common->setSeoTags(
	! empty( $model->seo )
		? json_encode( $model->seo )
		: '',
	$model->title
); ?>

    <!-- slider section -->
	<?= Slider::widget( [
		'slider_id' => $model->slider
	] ); ?>
    <!-- slider section -->

    <!-- delivery tracking form -->
	<?= DeliveryTrackingForm::widget(); ?>
    <!-- delivery tracking form -->

    <!-- article details -->
	<section class="section-single-article">

		<div class="container">

			<div class="row">

				<div class="page-title">

					<h2 class="title text-center">
						<?= $model->title ?>
					</h2>

				</div>

				<div class="page-description">
					<?= $model->description ?>
				</div>

			</div>

		</div>

	</section>
    <!-- article details -->
