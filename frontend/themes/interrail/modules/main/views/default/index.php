<?php /***************************************************************
 *         		 	             HOME PAGE                           *
 *********************************************************************/

/* @var $this yii\web\View         */
/* @var $model common\models\Posts */
/* @var $services array            */

use yii\helpers\ArrayHelper;

use common\models\Files;

use frontend\widgets\DeliveryTrackingForm;
use frontend\widgets\Slider;

//current page title, that will displayed in head tags
\Yii::$app->common->setSeoTags(
    ! empty( $model->seo )
        ? json_encode( $model->seo )
        : '',
    __( 'Home Page' )
);?>

    <!-- slider section -->
    <?= Slider::widget( [
        'slider_id' => $model->slider
    ] ); ?>
    <!-- slider section -->

    <!-- delivery tracking form -->
    <?= DeliveryTrackingForm::widget(); ?>
    <!-- delivery tracking form -->

    <?php if ( ! empty( $services[ 'details' ] ) ) {

        $thumbnails = Files::find()
            -> where( [
                'id' => ArrayHelper::getColumn(
                    $services[ 'details' ], 'file_id'
                ),
                'isDeleted' => false
            ] )
            -> indexBy( 'id' )
            -> asArray()
            -> all(); ?>

        <!-- our-services -->
        <section class="our-services posts">

            <div class="container">

                <div class="row">

                    <h2 class="title text-center">
                        <?= __( 'Our Services' ) ?>
                    </h2>

                    <div id="services-carousel" class="owl-carousel owl-theme">

                        <div class="owl-stage-outer">

                            <div class="owl-stage" >

                                <?php foreach ( $services[ 'details' ] as $key => $service ) { ?>

                                    <div class="col-12 col-sm-3 col-md-3 owl-item">

                                        <div class="service-img-container">

                                            <a href="javascript:void(0)">

                                                <img src="<?= isset( $thumbnails[ $service[ 'file_id' ] ] )
                                                    ? $thumbnails[ $service[ 'file_id' ] ][ 'guide' ]
                                                    : ''
                                                ?>">

                                            </a>

                                            <?php if ( isset( $services[ 'icons' ][ $service[ 'id' ] ] ) ) { ?>

                                                <div class="service-icon">
                                                    <i class="<?= $services[ 'icons' ][ $service[ 'id' ] ][ 'description' ] ?>-icon"></i>
                                                </div>

                                            <?php } ?>

                                            <h3>
                                                <?= $service[ 'title' ]  ?>
                                            </h3>

                                        </div>

                                        <p>
                                            <?= Yii::$app->common->excerpt( $service[ 'description' ], 500 ); ?>
                                        </p>

                                    </div>

                                <?php } ?>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </section>
        <!-- our-services -->

    <?php }

    if ( ! empty( $model->company ) ) {

        //re-assign company details
		$model->company = ! is_array( $model->company )
            ? (array)json_decode( $model->company )
            : $model->company;
		?>

        <!-- guarantee-info -->
        <section class="guarantee-info">

            <div class="col-sm-12 col-md-12 col-xs-12 guarantee-container">

                <div class="col-sm-6 col-md-6 col-xs-12">

                    <h2>

                        <?= isset( $model->company[ 'title' ] )
                            ? $model->company[ 'title' ]
                            : ''
                        ?>

                    </h2>

                    <p>
                        <?= isset( $model->company[ 'desc' ] )
                            ? $model->company[ 'desc' ]
                            : ''
                        ?>
                    </p>

                </div>

                <div class="col-sm-6 col-md-6" style="<?= isset( $model->company[ 'file_id' ] )
                    ? 'background: url( ' . Yii::$app->common->thumbnail( $model->company[ 'file_id' ] ) . ' )'
                    : ''
                ?> "></div>

            </div>

        </section>
        <!-- guarantee-info -->

    <?php }

    if ( ! empty( $model->statistics ) ) {

        $statistics = json_decode( $model->statistics );

        if ( isset( $statistics->title ) ) { ?>

            <!-- statistics -->
            <section class="statistics">

                <div class="container">

                    <div class="row">

                        <h2 class="title text-center">
                            <?= __( 'Our Statistics' ) ?>
                        </h2>

                        <ul>

                            <?php foreach ( $statistics->title as $key => $statistic ) { ?>

                                <li class="col-sm-3 col-md-3">

                                    <?php if ( isset( $statistics->icon[ $key ] ) ) { ?>

                                        <i class="fa <?= in_array( $statistics->icon[ $key ], [ 'customers-icon', 'world-icon', 'truck-icon-black', 'train-icon-black', 'products-icon' ] )
                                            ? $statistics->icon[ $key ]
                                            : 'fa-' . $statistics->icon[ $key ]
                                         ?>"></i>

                                    <?php } ?>

                                    <h3>

                                        <?= isset( $statistics->count[ $key ] )
                                            ? $statistics->count[ $key ]
                                            : ''
                                        ?>

                                    </h3>

                                    <div class="stat-desc">

                                        <h4>
                                            <?= $statistic ?>
                                        </h4>

                                        <p>

                                            <?= isset( $statistics->desc[ $key ] )
                                                ? $statistics->desc[ $key ]
                                                : ''
                                            ?>

                                        </p>

                                    </div>

                                </li>


                            <?php } ?>

                        </ul>

                    </div>

                </div>

            </section>
            <!-- statistics -->

        <?php }
    } ?>