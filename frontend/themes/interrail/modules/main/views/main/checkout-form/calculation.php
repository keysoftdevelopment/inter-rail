<?php /***************************************************************
 *  	   CHECKOUT TRANSPORTATION CALCULATION FORM SECTION          *
 *********************************************************************/ ?>

	<div class="delivery-details-header">
		<div id="map"></div>
	</div>

	<div class="delivery-details-container">

		<div class="container">

			<div class="row">

				<div class="delivery-form">

					<h3>
						<?= __( 'Transportation Cost' ) ?> :
					</h3>

					<div class="delivery-detail">
						<h4></h4>
					</div>

					<span class="delivery-sum pull-right">
						<?= __( 'calculation result after request' ) ?>
					</span>

				</div>

			</div>

		</div>

	</div>
