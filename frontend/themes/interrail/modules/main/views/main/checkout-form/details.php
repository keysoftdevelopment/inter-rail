<?php /***************************************************************
 *  	    	        CHECKOUT DETAILS FORM SECTION                *
 *********************************************************************/

use yii\helpers\ArrayHelper;
use yii\helpers\Html;

use common\models\Places;

/* @var $form yii\widgets\ActiveForm           */
/* @var $model common\models\Orders            */
/* @var $categories common\models\Taxonomies[] */
/* @var $wagon_weights array                   */

//the list of available cities in db
$cities = array_reduce( Places::find()
	-> where( [
		'!=', 'city', ''
	] )
	-> all(), function( $filter, $place ) {

	if ( ! empty( $place ) ) {

		$filter[] = [
			'id'           => $place->id,
			'country'      => $place->country,
			'country_code' => $place->country_code,
			'name'         => $place->city,
			'type'         => 'city',
			'syno_nym'     => $place->city,
			'lat'          => $place->lat,
			'lng'          => $place->lng,
		];

    }

	return $filter;

}, [] ); ?>

	<h2 class="title text-center">
		<?= __( 'Calculation price for transportation' ) ?>
	</h2>

	<div class="delivery-scheme text-center">
		<img src="<?= Yii::getAlias( '@web' ) ?>/images/calculation-scheme.png" />
	</div>

	<div class="col-sm-12 col-md-12 col-xs-12 transportation-details-section">

		<label class="control-label">
            <?= __( 'Transportation details' ) ?>
            <span class="required-mark">*</span>
		</label>

		<?= $form
			-> field( $model, 'trans_from', [
				'template' => '<div class="form-group col-sm-6 col-md-6 col-xs-12 required">{input}{error}</div>', // Leave only input (remove label, error and hint)
				'options'  => [
					'tag' => false, // Don't wrap with "form-group" div
				]
			] )
			-> textInput( [
				'class'       => 'form-control',
				'placeholder' => __( 'From' ),
				'id'          => 'transportation-from',
				'required'    => true
			] );
		?>

		<?= $form
			-> field( $model, 'trans_to', [
				'template' => '<div class="form-group col-sm-6 col-md-6 col-xs-12 required">{input}{error}</div>', // Leave only input (remove label, error and hint)
				'options'  => [
					'tag' => false, // Don't wrap with "form-group" div
				]
			] )
			-> textInput( [
				'class'       => 'form-control',
				'placeholder' => __( 'To' ),
				'id'          => 'transportation-to',
				'required'    => true
			] );
		?>

	</div>

    <div class="col-sm-12 col-md-12 col-xs-12 delivery-terms-section">

        <label class="control-label">
            <?= __( 'Delivery Terms' ) ?>
        </label>

		<?= $form
			-> field( $model, 'delivery_terms[from]', [
				'template' => '{label}<div class="form-group required col-sm-6 col-md-6 col-xs-12">{input}{error}</div>', // Leave only input (remove label, error and hint)
				'options'  => [
					'tag' => false, // Don't wrap with "form-group" div
				]
			] )
			-> dropDownList( Yii::$app->common->transportationDetails( 'terms' ), [
				'prompt'   => __(  'Choose Term of Delivery' ),
				'class'    => 'form-control'
			] )
			-> label( false );
		?>

		<?= $form
			-> field( $model, 'delivery_terms[to]', [
				'template' => '{label}<div class="form-group required col-sm-6 col-md-6 col-xs-12">{input}{error}</div>', // Leave only input (remove label, error and hint)
				'options'  => [
					'tag' => false, // Don't wrap with "form-group" div
				]
			] )
			-> dropDownList( Yii::$app->common->transportationDetails( 'terms' ), [
				'prompt'   => __(  'Choose Term of Delivery' ),
				'class'    => 'form-control'
			] )
			-> label( false );
		?>

    </div>

	<div class="col-sm-12 col-md-12 col-xs-12 cargo-taxonomy-section">

		<label class="control-label">
			<?= __( 'Select Category' ) ?>
            <span class="required-mark">*</span>
		</label>

		<div class="col-sm-4 col-md-4 col-xs-12 category-lists form-group required">

            <select name="Orders[cargo_tax]" size="4" required class="form-control">

                <?php foreach ( $categories as $cargo ) : ?>

                    <option value="<?= $cargo->id ?>" data-details="<?= htmlentities( json_encode( [
						'containers' => explode( ',' , $cargo->containers ),
                        'wagons'     => explode( ',' , $cargo->wagons )
                    ] ) ) ?>">
						<?= $cargo->icon ?> <?= $cargo->name ?>
                    </option>

                <?php endforeach; ?>

            </select>

            <p class="help-block help-block-error"></p>

		</div>

		<div class="col-sm-8 col-md-8 col-xs-12 category-detail-tabs" role="tabpanel" data-id="category-detail-tabs">

			<ul id="category-detail" class="nav nav-tabs bar_tabs" role="tablist">

				<li class="active">

					<a href="#containers" role="tab" data-toggle="tab" aria-expanded="true" data-msg="<?= htmlentities( json_encode( [
                        'single'   => __( 'container' ),
                        'multiple' => __( 'containers' )
                    ] ) ) ?>">
						<?= __( 'Containers' ) ?>
					</a>

				</li>

				<li>

					<a href="#wagons" role="tab" data-toggle="tab" aria-expanded="true" data-msg="<?= htmlentities( json_encode( [
						'single'   => __( 'wagon' ),
						'multiple' => __( 'wagons' )
					] ) ) ?>">
						<?= __( 'Wagons' ) ?>
					</a>

				</li>

			</ul>

			<div id="category-detail-content" class="tab-content">

				<div role="tabpanel" class="tab-pane fade active in text-center" id="containers">
					<img src="<?= Yii::getAlias( '@web' ) ?>/images/container-transfering.png" />
					<ul></ul>
				</div>

                <div role="tabpanel" class="tab-pane fade text-center" id="wagons" data-relations="<?= htmlentities( json_encode(
                    $wagon_weights[ 'relation' ]
                ) ) ?>">
                    <img src="<?= Yii::getAlias( '@web' ) ?>/images/container-transfering.png" />
                    <ul></ul>
                </div>

			</div>

		</div>

	</div>

    <div class="col-sm-12 col-md-12 col-xs-12 quantity-packages-section">

        <label class="control-label">
            <?= __( 'Quantity of' ) ?> <span class="type"><?= __( 'containers' ) ?></span> <span class="required-mark">*</span> <?= __( 'and type of packages' ) ?>
        </label>

        <?= $form
            -> field( $model, 'packages[quantity]', [
                'template' => '<div class="form-group col-sm-4 col-md-4 col-xs-12 required">{input}{error}</div>', // Leave only input (remove label, error and hint)
                'options'  => [
                    'tag' => false, // Don't wrap with "form-group" div
                ]
            ] )
            -> textInput( [
				'type'        => 'number',
				'min'         => '0',
				'step'        => '1',
				'pattern'     => '\d*',
                'class'       => 'form-control',
                'placeholder' => __( 'Quantity' ),
                'required'    => true
            ] )
            -> label( false );
        ?>

        <?= $form
            -> field( $model, 'packages[type]', [
                'template' => '<div class="form-group col-sm-8 col-md-8 col-xs-12">{input}{error}</div>', // Leave only input (remove label, error and hint)
                'options'  => [
                    'tag' => false, // Don't wrap with "form-group" div
                ]
            ] )
            -> dropDownList( Yii::$app->common->transportationDetails( 'packages' ), [
                'prompt'   => __(  'Choose Package' ),
                'class'    => 'form-control',
                'required' => false
            ] )
            -> label( false );
        ?>

    </div>

	<div class="col-sm-12 col-md-12 col-xs-12 weight-dimensions-section">

		<label class="control-label">
			<?= __( 'Weight Dimensions in each' ) ?> <span class="type"><?= __( 'container' ) ?></span>
            <span class="required-mark">*</span> <?= __( 'in' ) ?> <?= __( 'kg' ) ?>
		</label>

        <?php if ( ! empty( $wagon_weights[ 'relation' ] ) ) { ?>

            <div class="col-sm-3 col-md-3 col-xs-12" style="display: none" id="wagon-weights-field">

                <?= $form
                    -> field( $model, 'weight_dimensions[wagon]', [
                        'template' => '<div class="form-group">{input}</div>', // Leave only input (remove label, error and hint)
                        'options'  => [
                            'tag' => false, // Don't wrap with "form-group" div
                        ]
                    ] )
                    -> dropDownList( ArrayHelper::map(
                        $wagon_weights[ 'list' ],
                        'name',
                        function( $model ) {

                            return $model[ 'name'] . ' (' . str_replace(
                                    [
                                       'from',
                                        'till'
                                    ],
                                    [
                                        __( 'fr' ),
                                        __( 'till' )
                                    ],
                                    $model[ 'description' ]
                            ) . ')';

                        }
                    ), [
                        'prompt'   => __(  'Choose Wagon Weight' ),
                        'class'    => 'form-control',
                        'required' => false
                    ] )
                    -> label( false );
                ?>

            </div>

        <?php } ?>

        <div class="<?= ! empty( $wagon_weights[ 'relation' ] )
            ? 'col-sm-3 col-md-3'
            : 'col-sm-4 col-md-4'
        ?> col-xs-12">

            <?= $form
                -> field( $model, 'weight_dimensions[net]', [
                    'template' => '<div class="form-group">{input}{error}</div>', // Leave only input (remove label, error and hint)
                    'options'  => [
                        'tag' => false, // Don't wrap with "form-group" div
                    ]
                ] )
                -> textInput( [
                    'type'        => 'number',
					'min'         => '0',
					'step'        => '1',
					'pattern'     => '\d*',
                    'class'       => 'form-control',
                    'placeholder' => __( 'Weight Net' ),
                    'required'    => true
                ] )
                -> label( false );
            ?>

        </div>

        <div class="<?= ! empty( $wagon_weights[ 'relation' ] )
			? 'col-sm-3 col-md-3'
			: 'col-sm-4 col-md-4'
		?> col-xs-12">

            <?= $form
                -> field( $model, 'weight_dimensions[gross]', [
                    'template' => '<div class="form-group required">{input}{error}</div>', // Leave only input (remove label, error and hint)
                    'options'  => [
                        'tag' => false, // Don't wrap with "form-group" div
                    ]
                ] )
                -> textInput( [
					'type'        => 'number',
					'min'         => '0',
					'step'        => '1',
					'pattern'     => '\d*',
                    'class'       => 'form-control',
                    'placeholder' => __( 'Weight Gross' ),
                    'required'    => true
                ] )
                -> label( false );
            ?>

        </div>

        <div class="form-group col-sm-3 col-md-3 col-xs-12">

            <label class="cargo-insurance checkbox">

				<?= __( 'Insurance' ) ?>
                <input name="Orders[insurance]" type="checkbox">

                <span class="checkmark"></span>

            </label>

        </div>

	</div>

    <?= Html::hiddenInput( 'locations', '', [
        'id' => 'location-details'
    ] ) ?>