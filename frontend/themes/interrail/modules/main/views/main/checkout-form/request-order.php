<?php/****************************************************************
 *  	         CHECKOUT NEW ORDER REQUEST FORM SECTION             *
 *********************************************************************/

/* @var $form yii\widgets\ActiveForm */
/* @var $model common\models\Orders  */
/* @var $user common\models\User     */ ?>

	<h2 class="title text-center">
		<?= __( 'Checkout Form' ) ?>
	</h2>

    <?php if ( Yii::$app->user->isGuest ) { ?>

        <div class="col-sm-12 col-md-12 col-xs-12">

            <label class="control-label">
				<?= __( 'Company Details' ) ?>
            </label>

			<?= $form
				-> field( $user, 'company_name', [
					'template' => '<div class="form-group col-sm-3 col-md-3 col-xs-12">{input}</div>', // Leave only input (remove label, error and hint)
					'options'  => [
						'tag' => false, // Don't wrap with "form-group" div
					]
				] )
				-> textInput( [
					'class'       => 'form-control',
					'placeholder' => __( 'Company Name' )
				] )
				-> label( false )
			?>

			<?= $form
				-> field( $user, 'country', [
					'template' => '<div class="form-group col-sm-3 col-md-3 col-xs-12">{input}</div>', // Leave only input (remove label, error and hint)
					'options'  => [
						'tag' => false, // Don't wrap with "form-group" div
					]
				] )
				-> textInput( [
					'class'       => 'form-control',
					'placeholder' => __( 'Country' )
				] )
				-> label( false )
			?>

			<?= $form
				-> field( $user, 'city', [
					'template' => '<div class="form-group col-sm-3 col-md-3 col-xs-12">{input}</div>', // Leave only input (remove label, error and hint)
					'options'  => [
						'tag' => false, // Don't wrap with "form-group" div
					]
				] )
				-> textInput( [
					'class'       => 'form-control',
					'placeholder' => __( 'City' )
				] )
				-> label( false )
			?>

			<?= $form
				-> field( $user, 'street', [
					'template' => '<div class="form-group col-sm-3 col-md-3 col-xs-12">{input}</div>', // Leave only input (remove label, error and hint)
					'options'  => [
						'tag' => false, // Don't wrap with "form-group" div
					]
				] )
				-> textInput( [
					'class'       => 'form-control',
					'placeholder' => __( 'Address' )
				] )
				-> label( false )
			?>

			<?= $form
				-> field( $user, 'bank', [
					'template' => '<div class="form-group col-sm-4 col-md-4 col-xs-12">{input}</div>', // Leave only input (remove label, error and hint)
					'options'  => [
						'tag' => false, // Don't wrap with "form-group" div
					]
				] )
				-> textInput( [
					'class'       => 'form-control',
					'placeholder' => __( 'Bank' )
				] )
				-> label( false )
			?>

			<?= $form
				-> field( $user, 'mfo', [
					'template' => '<div class="form-group col-sm-4 col-md-4 col-xs-12">{input}</div>', // Leave only input (remove label, error and hint)
					'options'  => [
						'tag' => false, // Don't wrap with "form-group" div
					]
				] )
				-> textInput( [
                    'type'        => 'number',
					'class'       => 'form-control',
					'placeholder' => __( 'MFO' ),
					'min'         => '0',
                    'step'        => '1',
					'pattern'     => '\d*'
				] )
				-> label( false )
			?>

			<?= $form
				-> field( $user, 'oked', [
					'template' => '<div class="form-group col-sm-4 col-md-4 col-xs-12">{input}</div>', // Leave only input (remove label, error and hint)
					'options'  => [
						'tag' => false, // Don't wrap with "form-group" div
					]
				] )
				-> textInput( [
					'class'       => 'form-control',
					'placeholder' => __( 'OKED' )
				] )
				-> label( false )
			?>

        </div>

        <div class="col-sm-12 col-md-12 col-xs-12">

            <label class="control-label">
                <?= __( 'Personal Details' ) ?>
                <span class="required-mark">*</span>
            </label>

			<?= $form
				-> field( $user, 'first_name', [
					'template' => '<div class="form-group col-sm-6 col-md-6 col-xs-12">{input}</div>', // Leave only input (remove label, error and hint)
					'options'  => [
						'tag' => false, // Don't wrap with "form-group" div
					]
				] )
				-> textInput( [
					'class'       => 'form-control',
					'placeholder' => __( 'First Name' )
				] )
				-> label( false )
			?>

			<?= $form
				-> field( $user, 'last_name', [
					'template' => '<div class="form-group col-sm-6 col-md-6 col-xs-12">{input}</div>', // Leave only input (remove label, error and hint)
					'options'  => [
						'tag' => false, // Don't wrap with "form-group" div
					]
				] )
				-> textInput( [
					'class'       => 'form-control',
					'placeholder' => __( 'Last Name' )
				] )
				-> label( false )
			?>

			<?= $form
				-> field( $user, 'phone', [
					'template' => '<div class="form-group col-sm-4 col-md-4 col-xs-12">{input}</div>', // Leave only input (remove label, error and hint)
					'options'  => [
						'tag' => false, // Don't wrap with "form-group" div
					]
				] )
				-> textInput( [
				    'type'        => 'number',
					'class'       => 'form-control',
					'placeholder' => __( '+99(99)9999-9999' ),
                    'pattern'     => '[\+]\d*'
				] )
				-> label( false )
			?>

			<?= $form
				-> field( $user, 'email', [
					'template' => '<div class="form-group col-sm-4 col-md-4 col-xs-12">{input}</div>', // Leave only input (remove label, error and hint)
					'options'  => [
						'tag' => false, // Don't wrap with "form-group" div
					]
				] )
				-> textInput( [
					'class'       => 'form-control',
					'placeholder' => __( '______@______.___' ),
                    'required'    => true,
					'pattern'     => '[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$',
				] )
				-> label( false )
			?>

			<?= $form
				-> field( $user, 'password', [
					'template' => '<div class="form-group col-sm-4 col-md-4 col-xs-12">{input}</div>', // Leave only input (remove label, error and hint)
					'options'  => [
						'tag' => false, // Don't wrap with "form-group" div
					]
				] )
				-> passwordInput( [
					'class'       => 'form-control',
					'placeholder' => __( 'Password' )
				] )
				-> label( false )
			?>

        </div>

	<?php } ?>

	<div class="col-sm-12 col-md-12 col-xs-12">

		<label class="control-label">
			<?= __( 'Comment' ) ?>
		</label>

		<?= $form
			-> field( $model, 'notes', [
				'template' => '<div class="form-group col-sm-12 col-md-12 col-xs-12">{input}</div>', // Leave only input (remove label, error and hint)
				'options'  => [
					'tag' => false, // Don't wrap with "form-group" div
				]
			] )
			-> textarea( [
				'class'       => 'form-control',
				'placeholder' => __(  'Comment' )
			] )
			-> label( false );
		?>

	</div>

	<div class="col-sm-12 col-md-12 col-xs-12">

		<label class="control-label">
			<?= __( 'Attach Files' ) ?>
		</label>

		<div class="file-attach-container">

			<a href="javascript:void(0)" class="col-sm-1 col-md-1 upload-file">

				<i class="fa fa-cloud-upload"></i>

				<label>
					<?= __( 'Upload file' ) ?>
				</label>

			</a>

			<div class="col-sm-11 col-md-11">

				<div class="file-lists scrollbox-slider">

					<a href="javascript:void(0)" class="control backward text-left">
						<i class="fa fa-angle-left"></i>
					</a>

					<a href="javascript:void(0)" class="control forward text-right">
						<i class="fa fa-angle-right"></i>
					</a>

					<div class="file-lists-container scrollbox-container">
						<div class="scrollbox"></div>
					</div>

				</div>

			</div>

		</div>

	</div>

    <div class="col-sm-12 col-md-12 col-xs-12 terms-conditions">

        <div class="form-row">

            <label class="checkbox">

                <?= __( 'Agree with' ) ?> <a href="javascript:void(0)"><?= __( 'terms and conditions' ) ?></a>

                <input type="checkbox" name="agree_with_terms">
                <span class="checkmark"></span>

            </label>

        </div>

    </div>