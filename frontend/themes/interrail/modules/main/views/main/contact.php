<?php /***************************************************************
 *         		 	            CONTACT PAGE                         *
 *********************************************************************/

use common\models\Settings;

use frontend\assets\AppAsset;

/* @var $this yii\web\View         */
/* @var $model common\models\Posts */

//current page title, that will displayed in head tags
\Yii::$app->common->setSeoTags(
	! empty( $model->seo )
		? json_encode( $model->seo )
		: '',
	$model->title
);

//google map api key
$gmap_api_key = Settings::findOne( [
	'name' => 'gmap_api_key'
] );

if ( ! is_null( $gmap_api_key ) ) {

	//registering scripts for google maps
	foreach ( [
	    'https://maps.googleapis.com/maps/api/js?libraries=places,geometry&key='. $gmap_api_key->description,
		'@web/js/map/gmaps-markerwithlabel.min.js',
	] as $js_file ) {

		$this->registerJsFile(
			$js_file, [
				'depends' => [
					AppAsset::className()
				]
			]
		);

	}

} ?>

<!-- contact page section -->
<section class="contact-page">

    <div class="contact-details" >

        <div class="container">

            <div class="row">
				<?= $model->description ?>
            </div>

        </div>

    </div>

	<?php if ( ! empty( $model->contact_info ) ) {

		$locations = json_decode( $model->contact_info );

		Yii::$app->common->registerMap( $this, 'company-map' ); ?>

		<div id="company-map" data-details="<?= htmlspecialchars( json_encode( [
			'lat' => $locations->lat,
			'lng' => $locations->lng
		] ) ); ?>"></div>

	<?php } ?>

</section>
<!-- contact page section -->