<?php /***************************************************************
 *  	    	             CHECKOUT FORM PAGE                      *
 *********************************************************************/

/* @var $this yii\web\View                     */
/* @var $page common\models\Posts 	           */
/* @var $model common\models\Orders            */
/* @var $user common\models\User               */
/* @var $categories common\models\Taxonomies[] */
/* @var $wagon_weights array                   */

use yii\helpers\Url;

use yii\bootstrap\ActiveForm;

use common\models\Settings;

use frontend\assets\AppAsset;
use frontend\widgets\DeliveryTrackingForm;
use frontend\widgets\Slider;

//current page title, that will displayed in head tags
\Yii::$app->common->setSeoTags(
	! empty( $page->seo )
		? json_encode( $page->seo )
		: '',
	$page->title
);

//google map api key
$gmap_api_key = Settings::findOne( [
	'name' => 'gmap_api_key'
] );

if ( ! is_null( $gmap_api_key ) ) {

    //registering scripts for google maps
    foreach ( [
        'https://maps.googleapis.com/maps/api/js?libraries=places,geometry&key='. $gmap_api_key->description,
		'@web/js/map/gmaps-markerwithlabel.min.js',
        '@web/js/map/distance.min.js',
	] as $js_file ) {

	    $this->registerJsFile(
		    $js_file, [
			    'depends' => [
				    AppAsset::className()
			    ]
		    ]
	    );

    }

} ?>

    <!-- slider section -->
	<?= Slider::widget( [
		'slider_id' => $page->slider
	] ); ?>
    <!-- slider section -->

    <!-- delivery tracking form -->
	<?= DeliveryTrackingForm::widget(); ?>
    <!-- delivery tracking form -->

    <!-- checkout progress status -->
    <div class="checkout-form-progress">

        <div class="container">

            <div class="row">

                <ul data-details="<?= htmlentities( json_encode( [
                    'first-step' => [
                        'submit' => __( 'Calculate transportation cost' ),
                        'next'   => 'second-step'
                    ],
                    'second-step' => [
						'submit' => __( 'Request Order' ),
                        'next'   => 'last-step'
                    ],
                    'last-step' => [
						'submit' => __( 'Request Order' ),
						'next'   => 'second-step'
                    ]
                ] ) ) ?>">

                    <li class="active" data-current="first-step" data-next="second-step">

                        <a href="#first-step" class="checkout-progress">
                            1. <?= __( 'Calculation price for transportation' ) ?>
                        </a>

                        <i class="fa fa-angle-double-right"></i>

                    </li>

                    <li data-current="second-step" data-next="last-step">

                        <a href="#second-step" class="checkout-progress">
                            2. <?= __( 'Route and transportation cost' ) ?>
                        </a>

                        <i class="fa fa-angle-double-right"></i>

                    </li>

                    <li data-current="last-step">

                        <a href="#last-step" class="checkout-progress">
                            3. <?= __( 'Checkout Form' ) ?>
                        </a>

                    </li>

                </ul>

            </div>

        </div>

    </div>
    <!-- checkout progress status -->

    <!-- main checkout form -->
	<section class="checkout-form">

        <?php $form = ActiveForm::begin( [
            'enableAjaxValidation'   => false,
            'enableClientValidation' => false,
            'id'                     => 'main-checkout-form',
            'options'                => [
				'enctype' => 'multipart/form-data',
                'url'     => Url::toRoute( [
                    '/main/main/ajax-requests'
                ] ),
                'data-msg'   => __( 'Please, fill in required fields before to continue' ),
                'novalidate' => true
            ]
        ] ); ?>

            <div class="checkout-container clearfix">

                <!-- checkout form section first step screen -->
                <section class="first-step checkout-steps is-visible">

                    <div class="container">

                        <div class="row">

                            <?= $this->render( 'checkout-form/details', [
                                'form'          => $form,
                                'model'         => $model,
								'categories'    => $categories,
                                'wagon_weights' => $wagon_weights
                            ] ); ?>

                        </div>

                    </div>

                </section>
                <!-- checkout form section first step screen -->

                <!-- checkout form section second step screen -->
                <section class="second-step checkout-steps">

                    <?= $this->render( 'checkout-form/calculation', [
                        'form'  => $form,
                        'model' => $model
                    ] ); ?>

                </section>
                <!-- checkout form section second step screen -->

                <!-- checkout form section last step screen -->
                <section class="last-step checkout-steps">

                    <div class="container">

                        <div class="row">

                            <?= $this->render( 'checkout-form/request-order', [
                                'form'  => $form,
                                'model' => $model,
                                'user'  => $user
                            ] ); ?>

                        </div>

                    </div>

                </section>
                <!-- checkout form section last step screen -->

            </div>

            <!-- checkout submit section -->
            <div class="form-container text-center clearfix">

                <div class="form-group submit-area">

                    <a href="javascript:void(0)" class="btn btn-info" data-next="second-step" data-current="first-step">
                        <?= __( 'Calculate' ) ?>
                    </a>

                    <button class="btn btn-info hidden-elem" type="submit">
                        <?= __( 'Place order' ) ?>
                    </button>

                </div>

            </div>
            <!-- checkout submit section -->

        <?php ActiveForm::end(); ?>

    </section>
    <!-- main checkout form -->