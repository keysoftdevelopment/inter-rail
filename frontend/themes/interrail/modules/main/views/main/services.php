<?php /***************************************************************
 *         		 	      SERVICES LIST PAGE                          *
 *********************************************************************/

/* @var $this yii\web\View                        */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $page common\models\Posts	 			  */
/* @var $thumbnails array	 	     		      */
/* @var $icons common\models\CustomFields[]       */

use yii\widgets\LinkPager;

use frontend\widgets\DeliveryTrackingForm;
use frontend\widgets\Slider;

//current page title, that will displayed in head tags
\Yii::$app->common->setSeoTags(
	! empty( $page->seo )
		? json_encode( $page->seo )
		: '',
	$page->title
); ?>

    <!-- slider section -->
	<?= Slider::widget( [
		'slider_id' => $page->slider
	] ); ?>
    <!-- slider section -->

    <!-- delivery tracking form -->
	<?= DeliveryTrackingForm::widget(); ?>
    <!-- delivery tracking form -->

	<!-- our-services -->
	<section class="our-services posts">

		<div class="container">

			<div class="row">

				<h2 class="title text-center">
					<?= __( 'Our Services' ) ?>
				</h2>

				<?php if ( ! empty( $dataProvider->models ) ) : ?>

					<ul>

						<?php foreach ( $dataProvider->models as $model ) {

                            //param initialization
							$thumbnail = isset( $thumbnails[ $model->file_id ] )
								? $thumbnails[ $model->file_id ][ 'guide' ]
								: Yii::getAlias( 'web' ) . 'images/image-not-found.png' ?>

							<li class="col-sm-3 col-md-3 post-item">

								<div class="service-img-container">

                                    <a href="javascript:void(0)">
									    <img src="<?= $thumbnail ?>">
                                    </a>

									<?php if ( isset( $icons[ $model->id ] ) ) { ?>

                                        <div class="service-icon">
                                            <i class="<?= $icons[ $model->id ][ 'description' ] ?>-icon"></i>
                                        </div>

									<?php } ?>

									<h3>
										<?= $model->title ?>
									</h3>

								</div>

								<div class="description">

                                    <p>
                                        <?= strip_tags( $model->description ); ?>
                                    </p>

                                    <?php if ( count( explode(
                                          " ", trim(
                                                preg_replace("/[^(\w|\d|\'|\"|\.|\!|\?|;|,|\\|\/|\-\-|:|\&|@)]+/", " ",  strip_tags( $model->description ) ) )
                                        ) ) > 35 ) { ?>

                                        <a href="javascript:void(0)" class="show-more bg-underline" data-details="<?= htmlentities( json_encode( [
					                        'type' 	  => 'show',
					                        'msg'  	  => __( 'less info' ),
                                            'height'  => 100
				                            ] ) )
                                        ?>">
                                            <?= __( 'more info' ) ?>
                                        </a>

                                    <?php } ?>

								</div>

							</li>

						<?php } ?>

					</ul>

				<?php else : ?>

				<?php endif; ?>

				<!-- pagination container -->
				<?php if ( $dataProvider->pagination->totalCount ) : ?>

					<div class="pagination-container">

						<?= LinkPager::widget( [
							'pagination'     => $dataProvider->pagination,
							'maxButtonCount' => 7,
							'class'          => 'pagination'
						] ); ?>

					</div>

				<?php endif; ?>
				<!-- pagination container -->

			</div>

		</div>

	</section>
	<!-- our-services -->