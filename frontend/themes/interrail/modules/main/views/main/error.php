<?php /***************************************************************
 *         		 	             ERROR PAGE                          *
 *********************************************************************/

use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $name string       */

//current page title, that will displayed in head tags
\Yii::$app->common->setSeoTags( '',
    '404 ' .  __( 'Error' )
); ?>

<section class="page-not-found">

    <div class="row">

        <div class="not-found-container">

                <h1>
                    404 <?= __( 'Error' ) ?>
                </h1>

                <p>
                    <?= __( 'The page you are looking for not exist yet' ) ?>
                    <br />
					<?= __( 'Please try anything else' ) ?>
                </p>

                <a href="<?= Url::to( [ '/' ] ) ?>">
					<?= __( 'Go to homepage' ) ?>
                </a>

            </div>

    </div>

</section>

