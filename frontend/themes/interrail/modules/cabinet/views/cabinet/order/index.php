<?php /***************************************************************
 *                THE LIST OF ALL AVAILABLE ORDERS PAGE              *
 *********************************************************************/

/* @var $this yii\web\View                        */
/* @var $searchModel common\models\OrdersSearch   */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $tracking array                           */
/* @var $statuses common\models\Settings[]        */

use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\widgets\LinkPager;

use frontend\widgets\CabinetMenu;

//current page title, that will displayed in head tags
\Yii::$app->common->setSeoTags( '',
    $this->title = __( 'Personal Cabinet' )
);

//the list of available delivery terms
$terms = Yii::$app->common->transportationDetails( 'terms' ) ?>

<!-- personal cabinet section -->
<section class="personal-cabinet">

    <div class="container">

        <div class="row">

            <div class="cabinet-main-container">

                <!-- personal cabinet sidebar menu -->
				<?= CabinetMenu::widget( [
					'section' => 'orders'
				] ); ?>
                <!-- personal cabinet sidebar menu -->

                <!-- order details -->
                <div class="cabinet-details">

                    <h2>
                        <?= __( 'My Orders' ) ?>
                    </h2>

                    <table class="order-lists">

                        <thead>

                            <tr>

                                <th>
                                    №
                                </th>

                                <th>
                                    <?= __( 'Order Number' ) ?>
                                </th>

                                <th>
									<?= __( 'Status' ) ?>
                                </th>

                                <th>
									<?= __( 'Shipment Date' ) ?>
                                </th>

                                <th>
									<?= __( 'Arrival Date' ) ?>
                                </th>

                                <th>
									<?= __( 'Route' ) ?>
                                </th>

                                <th>
									<?= __( 'Order Sum' ) ?>
                                </th>

                                <th></th>

                            </tr>

                        </thead>

                        <tbody>

                            <?php if ( ! empty( $dataProvider->models ) ) {

                                foreach ( $dataProvider->models as $key => $model ) {

									//params initializations
                                    $date_from     = isset( $tracking[ $model->id ][ 0 ] ) && ! empty( $tracking[ $model->id ][ 0 ] )
                                        ? date( 'd.m.Y', strtotime( $tracking[ $model->id ][ 0 ][ 'departure' ][ 'date' ] ) )
                                        : __( 'Not available yet' );

                                    $date_to = isset( $tracking[ $model->id ] ) && isset( $tracking[ $model->id ][ end(array_keys( $tracking[ $model->id ] ) ) ] )
                                        ? date( 'd.m.Y', strtotime( $tracking[ $model->id ][ end(array_keys( $tracking[ $model->id ] ) ) ][ 'arrival' ][ 'date' ] ) )
                                        : __( 'Not available yet' );

                                    //delivery terms
                                    $term = json_decode( $model->delivery_terms ); ?>

                                    <tr class="<?= isset( $statuses[ 'confirm_option' ] ) && $statuses[ 'confirm_option' ][ 'description' ] == $model->status
                                        ? 'wait-confirmation'
                                        : ''
                                    ?>">

                                        <td>
                                            <?= $key + 1 ?>
                                        </td>

                                        <td>
                                            <?= $model->number ?>
                                        </td>

                                        <td>
                                            <i class="fa fa-check-circle-o"></i> <?= __( $model->status ) ?>
                                        </td>

                                        <td>
                                            <?= $date_from ?>
                                        </td>

                                        <td>
                                            <?= $date_to ?>
                                        </td>

                                        <td>
											<?= $model->trans_from ?>(<?= $terms[ $term->from ] ?>) - <?= $model->trans_to ?>(<?= $terms[ $term->to ] ?>)
                                        </td>

                                        <td class="order-total">
											<?= number_format( $model->total ) ?>
                                        </td>

                                        <td>

                                            <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                <i class="fa fa-ellipsis-v"></i>
                                            </a>

                                            <ul class="dropdown-menu pull-right">

                                                <li>

                                                    <a href="javascript:void(0)" class="order-details" data-details="<?= htmlentities( json_encode( [
                                                        'id'                   => $model->id,
                                                        'order_number'         => $model->number,
														'status'               => $model->status,
                                                        'transportation_dates' => [
                                                            'from' => $date_from,
                                                            'to'   => $date_to
                                                        ],
                                                        'notes'    => $model->notes,
                                                        'tracking' => isset( $tracking[ $model->id ] ) && ! empty( $tracking[ $model->id ] )
                                                            ? $tracking[ $model->id ]
                                                            : [],
                                                        'total' => number_format( $model->total )
                                                    ] ) ) ?>" data-type-id="#order-tracking-form">

                                                        <i class="fa fa-pencil"></i><?= __( 'Edit' ) ?>

                                                    </a>

                                                </li>

									            <?php if ( isset( $statuses[ 'invoice_option' ] )
                                                    && $statuses[ 'invoice_option' ][ 'description' ] == $model->status
                                                ) { ?>

                                                    <li>

                                                        <a href="<?= Url::to( [ '/cabinet/order/invoice/' .  $model->id ] ) ?>" target="_blank" class="invoice-details">
                                                            <i class="fa fa-file-pdf-o"></i><?= __( 'Download Invoice' ) ?>
                                                        </a>

                                                    </li>

                                                <?php }

                                                if ( isset( $statuses[ 'confirm_option' ] )
                                                    && $statuses[ 'confirm_option' ][ 'description' ] == $model->status
                                                ) { ?>

                                                    <li>

                                                        <a href="javascript:void(0)" class="confirm-payment">
                                                            <i class="fa fa-check-square-o"></i><?= __( 'Confirm' ) ?>
                                                        </a>

                                                    </li>

												<?php } ?>

                                            </ul>

                                        </td>

                                    </tr>

									<?php if ( isset( $statuses[ 'confirm_option' ] )
                                        && $statuses[ 'confirm_option' ][ 'description' ] == $model->status
                                    ) { ?>

                                        <tr class="order-confirm">

                                            <td colspan="8">

												<?php $form = ActiveForm::begin( [
													'method'                 => 'post',
													'enableClientValidation' => true,
													'id'                     => 'order-update-form',
													'action'                 => Url::to( [
														'/cabinet/cabinet/order-update/',
                                                        'id' => $model->id
													] )
												] ); ?>

                                                    <div class="col-sm-5 col-md-5 col-xs-12">

														<?= $form
															-> field( $model, 'shipper' )
															-> textInput( [
																'id'          => 'shipper',
																'placeholder' => __(  'Shipper' ),
															] )
															-> label( false );
														?>

                                                    </div>

                                                    <div class="col-sm-5 col-md-5 col-xs-12">

														<?= $form
															-> field( $model, 'consignee' )
															-> textInput( [
																'id'          => 'consignee',
																'placeholder' => __(  'Consignee' ),
															] )
															-> label( false );
														?>

                                                    </div>

                                                    <div class="col-sm-2 col-md-2 col-xs-12">

                                                        <button class="btn btn-info">
                                                            <?= __( 'save' ) ?>
                                                        </button>

                                                    </div>

												<?php ActiveForm::end(); ?>

                                            </td>

                                        </tr>

                                    <?php } ?>

								<?php }

                            } else { ?>

                                <tr>

                                    <td colspan="8">
                                        <?= __( 'You do not have any orders yet' ) ?>
                                    </td>

                                </tr>

							<?php } ?>

                        </tbody>

                    </table>

                    <!-- pagination container -->
					<?php if ( $dataProvider->pagination->totalCount ) : ?>

                        <div class="pagination-container">

							<?= LinkPager::widget( [
								'pagination'     => $dataProvider->pagination,
								'maxButtonCount' => 7,
								'class'          => 'pagination'
							] ); ?>

                        </div>

					<?php endif; ?>
                    <!-- pagination container -->

                </div>
                <!-- order details -->

            </div>

        </div>

    </div>

</section>
<!-- personal cabinet section -->