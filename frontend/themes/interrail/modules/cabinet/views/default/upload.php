<?php /***************************************************************
 *          		 	   	  FILE UPLOAD SECTION                    *
 *********************************************************************/

use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $model common\models\User */

$form = ActiveForm::begin( [
    'id'      => 'upload-user-form',
    'options' => [
        'enctype' => 'multipart/form-data',
        'class'   => 'dropzone',
        'url'     => Url::to( [
            '/cabinet/upload'
        ] )
    ]
] ); ?>

    <a href="javascript:void(0)">
        <img src="<?= Yii::$app->common->thumbnail( $model->file_id, 'user' ) ?>" class="thumbnail-view" />
    </a>

<?php ActiveForm::end(); ?>