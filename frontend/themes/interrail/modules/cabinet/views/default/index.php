<?php /***************************************************************
 *                 PERSONAL CABINET USER SETTINGS PAGE               *
 *********************************************************************/

/* @var $this yii\web\View            */
/* @var $model common\models\User     */
/* @var $company common\models\Posts  */

use yii\bootstrap\ActiveForm;

use frontend\widgets\CabinetMenu;

//current page title, that will displayed in head tags
\Yii::$app->common->setSeoTags( '',
    $this->title = __( 'Personal Cabinet' )
); ?>

<!-- personal cabinet section -->
<section class="personal-cabinet">

    <div class="container">

        <div class="row">

            <div class="cabinet-main-container">

                <!-- personal cabinet sidebar menu -->
				<?= CabinetMenu::widget( [
					'section' => 'settings'
				] ); ?>
                <!-- personal cabinet sidebar menu -->

                <!-- personal details -->
                <div class="cabinet-details">

                    <h2>
                        <?= __( 'Personal Details' ) ?>
                    </h2>

                        <div class="col-md-8 col-sm-8 col-xs-12 personal-details">

							<?php $form = ActiveForm::begin( [
								'options' => [
									'class'  => 'form-horizontal form-label-left ajax-submitting-form need-image',
									'method' => 'post'
								]
							] ); ?>

							<?= $form
								-> field( $model, 'file_id' )
								-> hiddenInput( [
									'id' => 'file_id'
								] )
								-> label( false );
							?>

                            <div class="form-group">

								<?= $form
                                    -> field( $model, 'first_name', [
									    'template' => '{label}<div class="col-sm-8 col-md-8 aligncenter">{input}</div>', // Leave only input (remove label, error and hint)
									    'options'  => [
										    'tag'  => false, // Don't wrap with "form-group" div
									    ],
								    ] )
                                    -> textInput( [
									    'class'       => 'form-control',
									    'placeholder' => __(  'First Name' )
								    ] )
                                    -> label( __( 'First Name' ), [
                                        'class' => 'col-sm-4 col-md-4'
                                    ] )
                                ?>

                            </div>

                            <div class="form-group">

								<?= $form
									-> field( $model, 'last_name', [
										'template' => '{label}<div class="col-sm-8 col-md-8 aligncenter">{input}</div>', // Leave only input (remove label, error and hint)
										'options'  => [
											'tag'  => false, // Don't wrap with "form-group" div
										],
									] )
									-> textInput( [
										'class'       => 'form-control',
										'placeholder' => __(  'Last Name' )
									] )
									-> label( __( 'Last Name' ), [
										'class' => 'col-sm-4 col-md-4'
									] )
								?>

                            </div>

                            <div class="form-group">

								<?= $form
									-> field( $model, 'phone', [
										'template' => '{label}<div class="col-sm-8 col-md-8 aligncenter">{input}</div>', // Leave only input (remove label, error and hint)
										'options'  => [
											'tag'  => false, // Don't wrap with "form-group" div
										],
									] )
									-> textInput( [
										'type'        => 'number',
										'class'       => 'form-control',
										'placeholder' => __( '+99(99)9999-9999' ),
										'pattern'     => '[\+]\d*'
									] )
									-> label( __( 'Phone Number' ), [
										'class' => 'col-sm-4 col-md-4'
									] )
								?>

                            </div>

                            <div class="form-group">

								<?= $form
									-> field( $model, 'email', [
										'template' => '{label}<div class="col-sm-8 col-md-8 aligncenter">{input}</div>', // Leave only input (remove label, error and hint)
										'options'  => [
											'tag'  => false, // Don't wrap with "form-group" div
										],
									] )
									-> textInput( [
										'class'       => 'form-control',
										'placeholder' => __( '______@______.___' ),
										'pattern'     => '[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$',
									] )
									-> label( __( 'Email Address' ), [
										'class' => 'col-sm-4 col-md-4'
									] )
								?>

                            </div>

                            <div class="form-group">

								<?= $form
									-> field( $model, 'password', [
										'template' => '{label}<div class="col-sm-8 col-md-8 aligncenter">{input}</div>', // Leave only input (remove label, error and hint)
										'options'  => [
											'tag'  => false, // Don't wrap with "form-group" div
										],
									] )
									-> passwordInput( [
										'class'       => 'form-control',
										'placeholder' => __(  'Password' )
									] )
									-> label( __( 'Password' ), [
										'class' => 'col-sm-4 col-md-4'
									] )
								?>

                            </div>

                            <div class="form-container text-center">

                                <div class="form-group submit-area">

                                    <button class="btn btn-info" type="submit">
                                        <?= __( 'Update' ) ?>
                                    </button>

                                </div>

                            </div>

							<?php ActiveForm::end(); ?>

                        </div>

                        <div class="col-sm-4 col-md-4 col-xs-12">

							<?= $this->render( 'upload', [
								'model' => $model
							] ) ?>

                        </div>

                </div>
                <!-- personal details -->

            </div>


        </div>

    </div>

</section>
<!-- personal cabinet section -->
