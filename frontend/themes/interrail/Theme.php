<?php namespace frontend\themes\interrail;

/**************************************/
/*                                    */
/*    INTERRAIL MAIN VIEW ELEMENTS    */
/*                                    */
/**************************************/

/**
 * InterRail Theme for html views
**/
class Theme extends \yii\base\Theme
{

	/**
	 * @var array option for getting appropriate views for modules|models
	**/
    public $pathMap = [
        '@frontend/views'   => '@frontend/themes/interrail/views',
        '@frontend/modules' => '@frontend/themes/interrail/modules'
    ];

	/**
	 * @inheritdoc
	**/
    public function init()
    {
        parent::init();
    }

}