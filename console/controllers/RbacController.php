<?php namespace console\controllers;

/**************************************/
/*                                    */
/*  USERS ACCESS CONTROL CONTROLLER   */
/*                                    */
/**************************************/

use Yii;

use yii\console\Controller;

use common\models\User;

class RbacController extends Controller
{

	/**
	 * Roles and permissions details.
	 * I. E. Also admin user creation in case of admin not exist
	*/
	public function actionInit()
	{

		// option allowing get|create|update roles and permissions in system
        $auth = Yii::$app->authManager;

		// removing old roles from database...
        $auth->removeAll();

        // available role list
        $admin  = $auth->createRole( 'admin' );
        $client = $auth->createRole( 'client' );

        // creating roles in db
        $auth->add( $admin );
        $auth->add( $client );

		// allows user update and change logistic by auto details
		foreach ( [ [
			'name' => 'auto',
			'desc' => 'Permission allowing read and write logistic by auto details'
		], [
			'name' => 'blockTrain',
			'desc' => 'Permission allowing read and write block train details'
		], [
			'name' => 'codes',
			'desc' => 'Permission allowing read codes details'
		], [
			'name' => 'companies',
			'desc' => 'Permission allowing read and write company details'
		], [
			'name' => 'containers',
			'desc' => 'Permission allowing change container details'
		], [
			'name' => 'dashboard',
			'desc' => 'Permission allowing read admin dashboard'
		], [
			'name' => 'expenses',
			'desc' => 'Permission allowing read and write expenses details'
		], [
			'name' => 'invoices',
			'desc' => 'Permission allowing read and write invoice details'
		], [
			'name' => 'logistics',
			'desc' => 'Permission allowing change logistics details'
		], [
			'name' => 'logs',
			'desc' => 'Permission allowing view logs details'
		], [
			'name' => 'media',
			'desc' => 'Permission allowing read and write media files'
		], [
			'name' => 'orders',
			'desc' => 'Permission allowing change order details'
		], [
			'name' => 'ownOrders',
			'desc' => 'Permission allowing change own order details'
		], [
			'name' => 'pages',
			'desc' => 'Permission allowing read and write pages'
		], [
			'name' => 'posts',
			'desc' => 'Permission allowing read and write posts, store and etc...'
		], [
			'name' => 'profile',
			'desc' => 'Permission allowing change profile details'
		], [
			'name' => 'rates',
			'desc' => 'Permission allowing read and write rates details'
		], [
			'name' => 'railway',
			'desc' => 'Permission allowing read and write logistic by railway details'
		], [
			'name' => 'routes',
			'desc' => 'Permission allowing change routes details'
		], [
			'name' => 'sea',
			'desc' => 'Permission allowing read and write logistic by sea details'
		], [
			'name' => 'settings',
			'desc' => 'Permission allowing change system settings and configurations'
		], [
			'name' => 'terminals',
			'desc' => 'Permission allowing change terminal details'
		], [
			'name' => 'train',
			'desc' => 'Permission allowing read trains details'
		], [
			'name' => 'users',
			'desc' => 'Permission allowing read and write users'
		], [
			'name' => 'wagons',
			'desc' => 'Permission allowing change wagons details'
		] ] as $permission ) {

			//declare permission details
			$permit 			 = $auth->createPermission( $permission[ 'name' ] );
			$permit->description = $permission[ 'desc' ];

			//writing permissions to database for further controlling that
			$auth->add( $permit );

			//role permissions
			$auth->addChild(
				$permission[ 'name' ] == 'ownOrders'
					? $client
					: $admin,
				$permit
			);

		}

		// admin role permissions
		$auth->addChild(
			$admin,
			$client
		);

        // user admin details
		$user = User::findOne( [
        	'email'    => 'admin@interrail.uz',
			'username' => 'interrail'
		] );

		// checking is current user exists or not. if not creating new one
        if ( ! isset( $user->id ) ) {

			$user = new User();
			$user->setScenario( User::SCENARIO_REGISTER );

			$user->username = "interrail";
			$user->email 	= "admin@interrail.uz";

			$user->setPassword( "interrail" );

			$user->generateAuthKey();
			$user->save( false );

		}

        // adding admin role for current user
        $auth->assign( $admin, $user->id );

    }

}