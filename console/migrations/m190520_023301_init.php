<?php use yii\db\Migration;

/**************************************/
/*                                    */
/*             USER TABLE             */
/*                                    */
/**************************************/

class m190520_023301_init extends Migration
{

	/**
	 * This method contains the logic to be executed
	 * when applying this migration.
	**/
    public function up()
    {

        $tableOptions = null;

        if ( $this->db->driverName === 'mysql' )
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

        $this->createTable( '{{%user}}', [
            'id'                   => $this->primaryKey(),
            'username'             => $this->string()->null()->unique(),
			'company_id'		   => $this->integer()->null(),
            'auth_key'             => $this->string( 32 )->notNull(),
            'password_hash'        => $this->string()->notNull(),
            'password_reset_token' => $this->string()->unique(),
            'file_id'			   => $this->integer()->null(),
            'email'                => $this->string()->notNull()->unique(),
            'first_name'		   => $this->string()->null(),
			'last_name'		   	   => $this->string()->null(),
			'phone'		   	   	   => $this->string()->null(),
            'status'               => $this->smallInteger()->notNull()->defaultValue( 10 ),
			'isDeleted'        	   => $this->boolean()->notNull()->defaultValue( false ),
			'updated_at'           => $this->integer()->notNull(),
            'created_at'           => $this->integer()->notNull()
        ], $tableOptions );
        
    }

	/**
	 * This method contains the logic to be executed
	 * when removing this migration.
	 *
	 * The default implementation throws an exception
	 * indicating the migration cannot be removed.
	**/
    public function down()
    {
        $this->dropTable( '{{%user}}' );
    }

}
