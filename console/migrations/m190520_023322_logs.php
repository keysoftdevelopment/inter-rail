<?php use yii\db\Migration;

/**************************************/
/*                                    */
/*       	 LOGGING TABLE        	  */
/*                                    */
/**************************************/

class m190520_023322_logs extends Migration
{

	/**
	 * This method contains the logic to be executed
	 * when applying this migration.
	**/
    public function up()
    {

		$tableOptions = null;

		if ( $this->db->driverName === 'mysql' )
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

		$this->createTable( '{{%logs}}', [
			'id'         => $this->primaryKey(),
			'foreign_id' => $this->integer()->notNull(),
			'changed_by' => $this->integer()->null(),
			'action'   	 => $this->text(),
			'type'   	 => $this->string( 100 )->null(),
			'updated_at' => $this->dateTime(),
			'created_at' => $this->dateTime()
		], $tableOptions );

		// creates index for column `changed_by`
		$this->createIndex(
			'idx-logs-changed_by',
			'{{%logs}}',
			'changed_by'
		);

		// add foreign key for table `user`
		$this->addForeignKey(
			'fk-logs-changed_by',
			'{{%logs}}',
			'changed_by',
			'{{%user}}',
			'id'
		);

    }

	/**
	 * This method contains the logic to be executed
	 * when removing this migration.
	 *
	 * The default implementation throws an exception
	 * indicating the migration cannot be removed.
	**/
    public function down()
    {

		// drops foreign key for table `user`
		$this->dropForeignKey(
			'fk-logs-changed_by',
			'{{%logs}}'
		);

		// drops index for column `changed_by`
		$this->dropIndex(
			'idx-logs-changed_by',
			'{{%logs}}'
		);

		$this->dropTable( '{{%logs}}' );

    }

}