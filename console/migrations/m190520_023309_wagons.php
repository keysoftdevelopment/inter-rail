<?php use yii\db\Migration;

/**************************************/
/*                                    */
/*     		  WAGONS TABLE     	      */
/*                                    */
/**************************************/

class m190520_023309_wagons extends Migration
{

	/**
	 * This method contains the logic to be executed
	 * when applying this migration.
	**/
	public function up()
	{

		$tableOptions = null;

		if ( $this->db->driverName === 'mysql' )
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

		$this->createTable( '{{%wagons}}', [
			'id'          => $this->primaryKey(),
			'number'	  => $this->string()->notNull()->unique(),
			'owner'		  => $this->string()->null(),
			'isDeleted'   => $this->boolean()->notNull()->defaultValue( false ),
			'updated_at'  => $this->dateTime(),
			'created_at'  => $this->dateTime()
		], $tableOptions );

	}

	/**
	 * This method contains the logic to be executed
	 * when removing this migration.
	 *
	 * The default implementation throws an exception
	 * indicating the migration cannot be removed.
	**/
	public function down()
	{
		$this->dropTable( '{{%wagons}}' );
	}

}
