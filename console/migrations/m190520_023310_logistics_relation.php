<?php use yii\db\Migration;

/**************************************/
/*                                    */
/*     LOGISTICS CONNECTION TABLE     */
/*                                    */
/**************************************/

class m190520_023310_logistics_relation extends Migration
{

	/**
	 * This method contains the logic to be executed
	 * when applying this migration.
	**/
	public function up()
	{

		$tableOptions = null;

		if ( $this->db->driverName === 'mysql' )
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

		$this->createTable( '{{%logistics_relation}}', [
			'id'          => $this->primaryKey(),
			'foreign_id'  => $this->integer()->defaultValue( 0 ),
			'logistic_id' => $this->integer()->defaultValue( 0 ),
			'type'		  => $this->string()
		], $tableOptions );

		// creates index for column `logistic_id`
		$this->createIndex(
			'idx-logistics_relation-logistic_id',
			'{{%logistics_relation}}',
			'logistic_id'
		);

		// add foreign key for table `logistics`
		$this->addForeignKey(
			'fk-logistics_relation-logistic_id',
			'{{%logistics_relation}}',
			'logistic_id',
			'{{%logistics}}',
			'id'
		);

	}

	/**
	 * This method contains the logic to be executed
	 * when removing this migration.
	 *
	 * The default implementation throws an exception
	 * indicating the migration cannot be removed.
	**/
	public function down()
	{

		// drops foreign key for table `logistics`
		$this->dropForeignKey(
			'fk-logistics_relation-logistic_id',
			'{{%logistics_relation}}'
		);

		// drops index for column `logistic_id`
		$this->dropIndex(
			'idx-logistics_relation-logistic_id',
			'{{%logistics_relation}}'
		);

		$this->dropTable( '{{%logistics_relation}}' );

	}

}
