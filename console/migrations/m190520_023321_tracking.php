<?php use yii\db\Migration;

/**************************************/
/*                                    */
/*       TRACK AND TRACE TABLE        */
/*                                    */
/**************************************/

class m190520_023321_tracking extends Migration
{

	/**
	 * This method contains the logic to be executed
	 * when applying this migration.
	**/
    public function up()
    {

		$tableOptions = null;

		if ( $this->db->driverName === 'mysql' )
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

		$this->createTable( '{{%tracking}}', [
			'id'            => $this->primaryKey(),
			'name'  	    => $this->string(),
			'description'   => $this->text(),
			'place_id'      => $this->integer()->null(),
			'check_in_date' => $this->dateTime()->notNull(),
			'foreign_id'    => $this->integer()->null(),
			'trans_from'    => $this->string(),
			'trans_to'      => $this->string(),
			'departure'	    => $this->dateTime()->notNull(),
			'arrival'  	    => $this->dateTime()->notNull(),
			'distance'	    => $this->float()->defaultValue( 0 ),
			'type'	        => $this->string()->defaultValue( 'order' ),
			'status'        => $this->string( 50 )
		], $tableOptions );

		// creates index for column `place_id`
		$this->createIndex(
			'idx-tracking-place_id',
			'{{%tracking}}',
			'place_id'
		);

		// add foreign key for table `places`
		$this->addForeignKey(
			'fk-tracking-place_id',
			'{{%tracking}}',
			'place_id',
			'{{%places}}',
			'id'
		);

    }

	/**
	 * This method contains the logic to be executed
	 * when removing this migration.
	 *
	 * The default implementation throws an exception
	 * indicating the migration cannot be removed.
	**/
    public function down()
    {

		// drops foreign key for table `places`
		$this->dropForeignKey(
			'fk-tracking-place_id',
			'{{%tracking}}'
		);

		// drops index for column `place_id`
		$this->dropIndex(
			'idx-tracking-place_id',
			'{{%tracking}}'
		);

		$this->dropTable( '{{%tracking}}' );

    }

}