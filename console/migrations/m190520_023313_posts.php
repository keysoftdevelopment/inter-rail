<?php use yii\db\Migration;

/**************************************/
/*                                    */
/*       PAGES, ARTICLES TABLE        */
/*                                    */
/**************************************/

class m190520_023313_posts extends Migration
{

	/**
	 * This method contains the logic to be executed
	 * when applying this migration.
	**/
    public function up()
    {

        $tableOptions = null;

        if ( $this->db->driverName === 'mysql' )
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

        $this->createTable( '{{%posts}}', [
            'id'          => $this->primaryKey(),
            'title'       => $this->string()->notNull(),
            'description' => $this->text(),
			'seo'   	  => $this->text(),
            'file_id'     => $this->integer()->null(),
			'lang_id'     => $this->integer()->null(),
            'type'   	  => $this->string()->defaultValue( 'variation' ),
            'slug'        => $this->string(),
            'isDeleted'   => $this->boolean()->notNull()->defaultValue( false ),
			'updated_at'  => $this->dateTime(),
			'created_at'  => $this->dateTime()
        ], $tableOptions );

		// creates index for column `file_id`
		$this->createIndex(
			'idx-posts-file_id',
			'{{%posts}}',
			'file_id'
		);

		// add foreign key for table `files`
		$this->addForeignKey(
			'fk-posts-file_id',
			'{{%posts}}',
			'file_id',
			'{{%files}}',
			'id'
		);

    }

	/**
	 * This method contains the logic to be executed
	 * when removing this migration.
	 *
	 * The default implementation throws an exception
	 * indicating the migration cannot be removed.
	**/
    public function down()
    {

		// drops foreign key for table `files`
		$this->dropForeignKey(
			'fk-posts-file_id',
			'{{%posts}}'
		);

		// drops index for column `file_id`
		$this->dropIndex(
			'idx-posts-file_id',
			'{{%posts}}'
		);

        $this->dropTable( '{{%posts}}' );

    }

}