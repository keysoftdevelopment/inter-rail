<?php use yii\db\Migration;

/**************************************/
/*                                    */
/*      TERMINAL RENTALS TABLE        */
/*                                    */
/**************************************/

class m190520_023318_terminal_storage extends Migration
{

	/**
	 * This method contains the logic to be executed
	 * when applying this migration.
	**/
	public function up()
	{

		$tableOptions = null;

		if ( $this->db->driverName === 'mysql' )
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

		$this->createTable( '{{%terminal_storage}}', [
			'id'           => $this->primaryKey(),
			'container_id' => $this->integer()->notNull(),
			'order_id'     => $this->integer()->notNull(),
			'terminal_id'  => $this->integer()->notNull(),
			'from'		   => $this->dateTime()->null(),
			'to'		   => $this->dateTime()->null()
		], $tableOptions );

		// creates index for column `terminal_id`
		$this->createIndex(
			'idx-terminal_storage-terminal_id',
			'{{%terminal_storage}}',
			'terminal_id'
		);

		// add foreign key for table `terminals`
		$this->addForeignKey(
			'fk-terminal_storage-terminal_id',
			'{{%terminal_storage}}',
			'terminal_id',
			'{{%terminals}}',
			'id'
		);

		// creates index for column `order_id`
		$this->createIndex(
			'idx-terminal_storage-order_id',
			'{{%terminal_storage}}',
			'order_id'
		);

		// add foreign key for table `orders`
		$this->addForeignKey(
			'fk-terminal_storage-order_id',
			'{{%terminal_storage}}',
			'order_id',
			'{{%orders}}',
			'id'
		);

		// creates index for column `container_id`
		$this->createIndex(
			'idx-terminal_storage-container_id',
			'{{%terminal_storage}}',
			'container_id'
		);

		// add foreign key for table `containers`
		$this->addForeignKey(
			'fk-terminal_storage-container_id',
			'{{%terminal_storage}}',
			'container_id',
			'{{%containers}}',
			'id'
		);

	}

	/**
	 * This method contains the logic to be executed
	 * when removing this migration.
	 *
	 * The default implementation throws an exception
	 * indicating the migration cannot be removed.
	**/
	public function down()
	{

		// drops foreign key for table `terminals`
		$this->dropForeignKey(
			'fk-terminal_storage-terminal_id',
			'{{%terminal_storage}}'
		);

		// drops index for column `terminal_id`
		$this->dropIndex(
			'idx-terminal_storage-terminal_id',
			'{{%terminal_storage}}'
		);

		// drops foreign key for table `orders`
		$this->dropForeignKey(
			'fk-terminal_storage-order_id',
			'{{%terminal_storage}}'
		);

		// drops index for column `order_id`
		$this->dropIndex(
			'idx-terminal_storage-order_id',
			'{{%terminal_storage}}'
		);

		// drops foreign key for table `containers`
		$this->dropForeignKey(
			'fk-terminal_storage-container_id',
			'{{%terminal_storage}}'
		);

		// drops index for column `container_id`
		$this->dropIndex(
			'idx-terminal_storage-container_id',
			'{{%terminal_storage}}'
		);

		$this->dropTable( '{{%terminal_storage}}' );

	}

}