<?php use yii\db\Migration;

/**************************************/
/*                                    */
/*    CONTAINERS CONNECTION TABLE     */
/*                                    */
/**************************************/

class m190520_023308_containers_relation extends Migration
{

	/**
	 * This method contains the logic to be executed
	 * when applying this migration.
	**/
    public function up()
    {

        $tableOptions = null;

        if ( $this->db->driverName === 'mysql' )
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

        $this->createTable( '{{%containers_relation}}', [
            'id'           => $this->primaryKey(),
            'foreign_id'   => $this->integer()->defaultValue( 0 ),
            'container_id' => $this->integer()->defaultValue( 0 ),
			'type'		   => $this->string()
        ], $tableOptions );

        // creates index for column `container_id`
        $this->createIndex(
            'idx-containers_relation-container_id',
            '{{%containers_relation}}',
            'container_id'
        );

        // add foreign key for table `containers`
        $this->addForeignKey(
            'fk-containers_relation-container_id',
            '{{%containers_relation}}',
            'container_id',
            '{{%containers}}',
            'id'
        );

    }

	/**
	 * This method contains the logic to be executed
	 * when removing this migration.
	 *
	 * The default implementation throws an exception
	 * indicating the migration cannot be removed.
	**/
    public function down()
    {

        // drops foreign key for table `containers`
        $this->dropForeignKey(
            'fk-containers_relation-container_id',
            '{{%containers_relation}}'
        );

        // drops index for column `container_id`
        $this->dropIndex(
            'idx-containers_relation-container_id',
            '{{%containers_relation}}'
        );

        $this->dropTable( '{{%containers_relation}}' );

    }

}
