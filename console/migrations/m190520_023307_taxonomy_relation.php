<?php use yii\db\Migration;

/**************************************/
/*                                    */
/*    CATEGORIES CONNECTION TABLE     */
/*                                    */
/**************************************/

class m190520_023307_taxonomy_relation extends Migration
{

	/**
	 * This method contains the logic to be executed
	 * when applying this migration.
	**/
    public function up()
    {

        $tableOptions = null;

        if ( $this->db->driverName === 'mysql' )
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

        $this->createTable( '{{%taxonomy_relation}}', [
            'id'         => $this->primaryKey(),
            'foreign_id' => $this->integer()->defaultValue( 0 ),
            'tax_id'     => $this->integer()->defaultValue( 0 ),
			'type'       => $this->string()
        ], $tableOptions );

        // creates index for column `tax_id`
        $this->createIndex(
            'idx-taxonomy_relation-tax_id',
            '{{%taxonomy_relation}}',
            'tax_id'
        );

        // add foreign key for table `taxonomies`
        $this->addForeignKey(
            'fk-taxonomy_relation-tax_id',
            '{{%taxonomy_relation}}',
            'tax_id',
            '{{%taxonomies}}',
            'id'
        );

    }

	/**
	 * This method contains the logic to be executed
	 * when removing this migration.
	 *
	 * The default implementation throws an exception
	 * indicating the migration cannot be removed.
	**/
    public function down()
    {

        // drops foreign key for table `taxonomies`
        $this->dropForeignKey(
            'fk-taxonomy_relation-tax_id',
            '{{%taxonomy_relation}}'
        );

        // drops index for column `tax_id`
        $this->dropIndex(
            'idx-taxonomy_relation-tax_id',
            '{{%taxonomy_relation}}'
        );

        $this->dropTable( '{{%taxonomy_relation}}' );

    }

}