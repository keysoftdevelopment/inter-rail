<?php use yii\db\Migration;

/**************************************/
/*                                    */
/*          COMPANIES TABLE           */
/*                                    */
/**************************************/

class m190520_023305_companies extends Migration
{

	/**
	 * This method contains the logic to be executed
	 * when applying this migration.
	**/
	public function up()
	{

		$tableOptions = null;

		if ( $this->db->driverName === 'mysql' )
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

		$this->createTable( '{{%companies}}', [
			'id'          => $this->primaryKey(),
			'name'        => $this->string()->notNull(),
			'description' => $this->text(),
			'bank'   	  => $this->string(),
			'mfo'   	  => $this->string(),
			'oked'   	  => $this->string(),
			'file_id'     => $this->integer()->null(),
			'place_id'    => $this->integer()->null(),
			'isDeleted'   => $this->boolean()->notNull()->defaultValue( false ),
			'updated_at'  => $this->dateTime(),
			'created_at'  => $this->dateTime()
		], $tableOptions );

		// creates index for column `file_id`
		$this->createIndex(
			'idx-companies-file_id',
			'{{%companies}}',
			'file_id'
		);

		// add foreign key for table `files`
		$this->addForeignKey(
			'fk-companies-file_id',
			'{{%companies}}',
			'file_id',
			'{{%files}}',
			'id'
		);

		// creates index for column `place_id`
		$this->createIndex(
			'idx-companies-place_id',
			'{{%companies}}',
			'place_id'
		);

		// add foreign key for table `places`
		$this->addForeignKey(
			'fk-companies-place_id',
			'{{%companies}}',
			'place_id',
			'{{%places}}',
			'id'
		);

	}

	/**
	 * This method contains the logic to be executed
	 * when removing this migration.
	 *
	 * The default implementation throws an exception
	 * indicating the migration cannot be removed.
	**/
	public function down()
	{

		// drops foreign key for table `files`
		$this->dropForeignKey(
			'fk-companies-file_id',
			'{{%companies}}'
		);

		// drops index for column `file_id`
		$this->dropIndex(
			'idx-companies-file_id',
			'{{%companies}}'
		);

		// drops foreign key for table `places`
		$this->dropForeignKey(
			'fk-companies-place_id',
			'{{%companies}}'
		);

		// drops index for column `place_id`
		$this->dropIndex(
			'idx-companies-place_id',
			'{{%companies}}'
		);

		$this->dropTable( '{{%companies}}' );

	}

}