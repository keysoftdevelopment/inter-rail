<?php use yii\db\Migration;

/**************************************/
/*                                    */
/*     		TERMINALS TABLE     	  */
/*                                    */
/**************************************/

class m190520_023312_terminals extends Migration
{

	/**
	 * This method contains the logic to be executed
	 * when applying this migration.
	**/
	public function up()
	{

		$tableOptions = null;

		if ( $this->db->driverName === 'mysql' )
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

		$this->createTable( '{{%terminals}}', [
			'id'           => $this->primaryKey(),
			'name'		   => $this->string()->notNull(),
			'description'  => $this->text(),
			'place_id'	   => $this->integer()->notNull(),
			'email'        => $this->string()->notNull(),
			'phone'		   => $this->string()->null(),
			'storage'  	   => $this->float()->notNull(),
			'lading'	   => $this->float()->null(),
			'unloading'	   => $this->float()->null(),
			'exp_price'    => $this->float()->defaultValue( 0 ),
			'imp_price'    => $this->float()->defaultValue( 0 ),
			'surcharge'    => $this->float()->defaultValue( 0 ),
			'isDeleted'    => $this->boolean()->notNull()->defaultValue( false ),
			'updated_at'   => $this->dateTime(),
			'created_at'   => $this->dateTime()
		], $tableOptions );

		// creates index for column `place_id`
		$this->createIndex(
			'idx-terminals-place_id',
			'{{%terminals}}',
			'place_id'
		);

		// add foreign key for table `places`
		$this->addForeignKey(
			'fk-terminals-place_id',
			'{{%terminals}}',
			'place_id',
			'{{%places}}',
			'id'
		);

	}

	/**
	 * This method contains the logic to be executed
	 * when removing this migration.
	 *
	 * The default implementation throws an exception
	 * indicating the migration cannot be removed.
	**/
	public function down()
	{

		// drops foreign key for table `places`
		$this->dropForeignKey(
			'fk-terminals-place_id',
			'{{%terminals}}'
		);

		// drops index for column `place_id`
		$this->dropIndex(
			'idx-terminals-place_id',
			'{{%terminals}}'
		);

		$this->dropTable( '{{%terminals}}' );

	}

}
