<?php use yii\db\Migration;

/**************************************/
/*                                    */
/*      ADDITIONAL SETTINGS TABLE     */
/*                                    */
/**************************************/

class m190520_023306_settings extends Migration
{

	/**
	 * This method contains the logic to be executed
	 * when applying this migration.
	**/
    public function up()
    {

        $tableOptions = null;

        if ( $this->db->driverName === 'mysql' )
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

        $this->createTable( '{{%settings}}', [
            'id'          => $this->primaryKey(),
            'name'        => $this->string()->notNull(),
            'description' => $this->text(),
			'lang_id'     => $this->integer()->null(),
            'isDeleted'   => $this->boolean()->notNull()->defaultValue( false )
        ], $tableOptions );

    }

	/**
	 * This method contains the logic to be executed
	 * when removing this migration.
	 *
	 * The default implementation throws an exception
	 * indicating the migration cannot be removed.
	**/
    public function down()
    {
        $this->dropTable( '{{%settings}}' );
    }

}
