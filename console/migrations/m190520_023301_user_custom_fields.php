<?php use yii\db\Migration;

/***************************************/
/*                                     */
/*    ADDITIONAL DATA FOR USER TABLE   */
/*                                     */
/***************************************/

class m190520_023301_user_custom_fields extends Migration
{

	/**
	 * This method contains the logic to be executed
	 * when applying this migration.
	**/
    public function up()
    {

        $tableOptions = null;

        if ( $this->db->driverName === 'mysql' )
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

        $this->createTable( '{{%user_custom_fields}}', [
            'id'          => $this->primaryKey(),
            'user_id'     => $this->integer()->defaultValue( 0 ),
            'name'        => $this->string()->notNull(),
            'description' => $this->text()
        ], $tableOptions );

        // creates index for column `user_id`
        $this->createIndex(
            'idx-user_custom_fields-user_id',
            '{{%user_custom_fields}}',
            'user_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-user_custom_fields-user_id',
            '{{%user_custom_fields}}',
            'user_id',
            '{{%user}}',
            'id',
			'CASCADE'
        );

    }

	/**
	 * This method contains the logic to be executed
	 * when removing this migration.
	 *
	 * The default implementation throws an exception
	 * indicating the migration cannot be removed.
	**/
    public function down()
    {

        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk-user_custom_fields-user_id',
            '{{%user_custom_fields}}'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            'idx-user_custom_fields-user_id',
            '{{%user_custom_fields}}'
        );

        $this->dropTable( '{{%user_custom_fields}}' );

    }

}
