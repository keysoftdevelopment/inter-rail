<?php use yii\db\Migration;

/**************************************/
/*                                    */
/*   ADDITIONAL DATA FOR PAGES TABLE  */
/*                                    */
/**************************************/

class m190520_023314_custom_fields extends Migration
{

	/**
	 * This method contains the logic to be executed
	 * when applying this migration.
	**/
    public function up()
    {

        $tableOptions = null;

        if ( $this->db->driverName === 'mysql' )
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

        $this->createTable( '{{%custom_fields}}', [
            'id'          => $this->primaryKey(),
            'foreign_id'  => $this->integer()->defaultValue( 0 ),
            'name'	      => $this->string()->notNull(),
            'description' => $this->text(),
			'type'   	  => $this->string()->defaultValue( 'page' )
        ], $tableOptions );

    }

	/**
	 * This method contains the logic to be executed
	 * when removing this migration.
	 *
	 * The default implementation throws an exception
	 * indicating the migration cannot be removed.
	**/
    public function down()
    {
        $this->dropTable( '{{%custom_fields}}' );
    }

}
