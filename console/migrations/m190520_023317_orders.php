<?php use yii\db\Migration;

/**************************************/
/*                                    */
/*            ORDERS TABLE            */
/*                                    */
/**************************************/

class m190520_023317_orders extends Migration
{

	/**
	 * This method contains the logic to be executed
	 * when applying this migration.
	**/
    public function up()
    {

        $tableOptions = null;

        if ( $this->db->driverName === 'mysql' )
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

        $this->createTable( '{{%orders}}', [
            'id'             	=> $this->primaryKey(),
            'number'			=> $this->string()->unique(),
            'user_id'        	=> $this->integer(),
            'manager_id'   	    => $this->integer()->null(),
			'terminal_id'		=> $this->integer()->null(),
            'gu_number'			=> $this->string(),
			'gu_file_id'		=> $this->integer()->null(),
            'contractors'	    => $this->string( 100 ),
            'packages' 			=> $this->string()->notNull(),
			'weight_dimensions' => $this->string()->notNull(),
            'trans_from'  		=> $this->string(),
			'trans_to'  	 	=> $this->string(),
			'transportation'    => $this->string( 100 )->defaultValue( 'import' ),
			'delivery_terms'	=> $this->text(),
			'consignee'	      	=> $this->string()->null(),
			'shipper'	      	=> $this->string()->null(),
			'document_id'		=> $this->integer()->null(),
			'terminals'	    	=> $this->text(),
			'auto'				=> $this->text(),
			'insurance'		 	=> $this->boolean()->defaultValue( false ),
            'commission'	 	=> $this->float()->defaultValue( 0 ),
			'zad'		    	=> $this->float()->defaultValue( 0 ),
			'surcharge'	    	=> $this->float()->defaultValue( 0 ),
            'total'			 	=> $this->float()->defaultValue( 0 ),
            'status'         	=> $this->string( 50 ),
            'notes'          	=> $this->string(),
            'isDeleted'      	=> $this->boolean()->notNull()->defaultValue( false ),
			'updated_at'     	=> $this->dateTime(),
			'created_at' 	 	=> $this->dateTime()
        ], $tableOptions );

		// creates index for columns `user_id` and `manager_id`
		$this->createIndex(
			'idx-orders-user_id',
			'{{%orders}}', [
				'user_id',
				'manager_id'
			]
		);

		// add foreign key for table `user`
		$this->addForeignKey(
			'fk-orders-user_id',
			'{{%orders}}',
			'user_id',
			'{{%user}}',
			'id'
		);

		// creates index for column `terminal_id`
		$this->createIndex(
			'idx-orders-terminal_id',
			'{{%orders}}',
			'terminal_id'
		);

		// add foreign key for table `terminals`
		$this->addForeignKey(
			'fk-orders-terminal_id',
			'{{%orders}}',
			'terminal_id',
			'{{%terminals}}',
			'id'
		);

		// creates index for columns `gu_file_id` and `document_id`
		$this->createIndex(
			'idx-orders-gu_file_id',
			'{{%orders}}', [
				'gu_file_id',
				'document_id'
			]
		);

		// add foreign key for table `files`
		$this->addForeignKey(
			'fk-orders-gu_file_id',
			'{{%orders}}',
			'gu_file_id',
			'{{%files}}',
			'id'
		);

    }

	/**
	 * This method contains the logic to be executed
	 * when removing this migration.
	 *
	 * The default implementation throws an exception
	 * indicating the migration cannot be removed.
	**/
    public function down()
    {

        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk-orders-user_id',
            '{{%orders}}'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            'idx-orders-user_id',
            '{{%orders}}'
        );

		// drops foreign key for table `terminals`
		$this->dropForeignKey(
			'fk-orders-terminal_id',
			'{{%orders}}'
		);

		// drops index for column `terminal_id`
		$this->dropIndex(
			'idx-orders-terminal_id',
			'{{%orders}}'
		);

		// drops foreign key for table `files`
		$this->dropForeignKey(
			'fk-orders-gu_file_id',
			'{{%orders}}'
		);

		// drops index for column `gu_file_id`
		$this->dropIndex(
			'idx-orders-gu_file_id',
			'{{%orders}}'
		);

        $this->dropTable( '{{%orders}}' );

    }

}