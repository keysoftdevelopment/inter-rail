<?php use yii\db\Migration;

/**************************************/
/*                                    */
/*            EXPENSES TABLE          */
/*                                    */
/**************************************/

class m190520_023318_expenses extends Migration
{

	/**
	 * This method contains the logic to be executed
	 * when applying this migration.
	**/
	public function up()
	{

		$tableOptions = null;

		if ( $this->db->driverName === 'mysql' )
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

		$this->createTable( '{{%expenses}}', [
			'id'             	  => $this->primaryKey(),
			'order_id'        	  => $this->integer()->notNull(),
			'sale'				  => $this->float()->defaultValue( 0 ),
			'purchase'			  => $this->float()->defaultValue( 0 ),
			'rental'			  => $this->float()->defaultValue( 0 ),
			'deposit'			  => $this->float()->defaultValue( 0 ),
			'container_surcharge' => $this->float()->defaultValue( 0 ),
			'container_cost'	  => $this->float()->defaultValue( 0 ),
			'sf_rate'			  => $this->float()->defaultValue( 0 ),
			'dthc'			  	  => $this->float()->defaultValue( 0 ),
			'othc'			  	  => $this->float()->defaultValue( 0 ),
			'bl'			  	  => $this->float()->defaultValue( 0 ),
			'sf_id'        	      => $this->integer()->null(),
			'sf_surcharge'		  => $this->float()->defaultValue( 0 ),
			'auto_id'     	      => $this->integer()->null(),
			'auto_rate'			  => $this->float()->defaultValue( 0 ),
			'auto_surcharge'	  => $this->float()->defaultValue( 0 ),
			'rf_details'		  => $this->text(),
			'rf_rental'	    	  => $this->float()->defaultValue( 0 ),
			'rf_commission'		  => $this->float()->defaultValue( 0 ),
			'pf_rate'			  => $this->float()->defaultValue( 0 ),
			'pf_surcharge'		  => $this->float()->defaultValue( 0 ),
			'pf_commission'		  => $this->float()->defaultValue( 0 ),
			'storage'			  => $this->float()->defaultValue( 0 ),
			'lading'			  => $this->float()->defaultValue( 0 ),
			'unloading'  		  => $this->float()->defaultValue( 0 ),
			'exp_price'			  => $this->float()->defaultValue( 0 ),
			'imp_price'			  => $this->float()->defaultValue( 0 ),
			'terminal_surcharge'  => $this->float()->defaultValue( 0 ),
			'thc'				  => $this->float()->defaultValue( 0 ),
			'tracking'			  => $this->float()->defaultValue( 0 ),
			'surcharge'	    	  => $this->float()->defaultValue( 0 ),
			'total'			 	  => $this->float()->defaultValue( 0 )
		], $tableOptions );

		// creates index for column `order_id`
		$this->createIndex(
			'idx-expenses-order_id',
			'{{%expenses}}',
			'order_id'
		);

		// add foreign key for table `orders`
		$this->addForeignKey(
			'fk-expenses-order_id',
			'{{%expenses}}',
			'order_id',
			'{{%orders}}',
			'id'
		);

	}

	/**
	 * This method contains the logic to be executed
	 * when removing this migration.
	 *
	 * The default implementation throws an exception
	 * indicating the migration cannot be removed.
	 **/
	public function down()
	{

		// drops foreign key for table `orders`
		$this->dropForeignKey(
			'fk-expenses-order_id',
			'{{%expenses}}'
		);

		// drops index for column `order_id`
		$this->dropIndex(
			'idx-expenses-order_id',
			'{{%expenses}}'
		);

		$this->dropTable( '{{%expenses}}' );

	}

}