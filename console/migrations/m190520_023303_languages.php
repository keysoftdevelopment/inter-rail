<?php use yii\db\Migration;

/**************************************/
/*                                    */
/*           LANGUAGES TABLE          */
/*                                    */
/**************************************/

class m190520_023303_languages extends Migration
{

	/**
	 * This method contains the logic to be executed
	 * when applying this migration.
	**/
    public function up()
    {

        $tableOptions = null;

        if ( $this->db->driverName === 'mysql' )
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

        $this->createTable( '{{%languages}}', [
            'id'          => $this->primaryKey(),
            'name'        => $this->string()->notNull(),
            'description' => $this->text(),
            'file_id'     => $this->integer()->defaultValue( 0 ),
            'isDeleted'   => $this->boolean()->notNull()->defaultValue( false )
        ], $tableOptions );

		// creates index for column `file_id`
		$this->createIndex(
			'idx-languages-file_id',
			'{{%languages}}',
			'file_id'
		);

		// add foreign key for table `files`
		$this->addForeignKey(
			'fk-languages-file_id',
			'{{%languages}}',
			'file_id',
			'{{%files}}',
			'id',
			'CASCADE'
		);

    }

	/**
	 * This method contains the logic to be executed
	 * when removing this migration.
	 *
	 * The default implementation throws an exception
	 * indicating the migration cannot be removed.
	**/
    public function down()
    {

		// drops foreign key for table `files`
		$this->dropForeignKey(
			'fk-languages-file_id',
			'{{%languages}}'
		);

		// drops index for column `file_id`
		$this->dropIndex(
			'idx-languages-file_id',
			'{{%languages}}'
		);

        $this->dropTable( '{{%languages}}' );

    }

}
