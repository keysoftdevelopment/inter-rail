<?php use yii\db\Migration;

/**************************************/
/*                                    */
/*             MEDIA TABLE            */
/*                                    */
/**************************************/

class m190520_023302_files extends Migration
{

	/**
	 * This method contains the logic to be executed
	 * when applying this migration.
	**/
    public function up()
    {

        $tableOptions = null;

        if ( $this->db->driverName === 'mysql' )
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

        $this->createTable( '{{%files}}', [
            'id'         => $this->primaryKey(),
            'name'       => $this->string()->notNull(),
            'guide'      => $this->string()->notNull(),
            'type'       => $this->string(),
            'size'       => $this->string(),
            'isDeleted'  => $this->boolean()->notNull()->defaultValue( false ),
            'created_at' => $this->dateTime()
        ], $tableOptions );

    }

	/**
	 * This method contains the logic to be executed
	 * when removing this migration.
	 *
	 * The default implementation throws an exception
	 * indicating the migration cannot be removed.
	**/
    public function down()
    {
        $this->dropTable( '{{%files}}' );
    }

}
