<?php use yii\db\Migration;

/**************************************/
/*                                    */
/*           INVOICES TABLE           */
/*                                    */
/**************************************/

class m190520_023321_invoices extends Migration
{

	/**
	 * This method contains the logic to be executed
	 * when applying this migration.
	 **/
	public function up()
	{

		$tableOptions = null;

		if ( $this->db->driverName === 'mysql' )
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

		$this->createTable( '{{%invoices}}', [
			'id'          => $this->primaryKey(),
			'customer_id' => $this->integer()->notNull(),
			'foreign_id'  => $this->integer()->notNull(),
			'number'  	  => $this->string(),
			'date'		  => $this->dateTime(),
			'status'      => $this->string( 50 ),
			'tax'		  => $this->float()->defaultValue( 0 ),
			'amount'	  => $this->float()->defaultValue( 0 ),
			'balance_due' => $this->float()->defaultValue( 0 ),
			'type'		  => $this->string()->notNull(),
			'creator' 	  => $this->integer()->notNull(),
			'updated_at'  => $this->dateTime(),
			'created_at'  => $this->dateTime()
		], $tableOptions );

		// creates index for columns `customer_id` and `creator`
		$this->createIndex(
			'idx-invoices-customer_id',
			'{{%invoices}}', [
				'customer_id',
				'creator'
			]
		);

		// add foreign key for table `user`
		$this->addForeignKey(
			'fk-invoices-customer_id',
			'{{%invoices}}',
			'customer_id',
			'{{%user}}',
			'id'
		);

	}

	/**
	 * This method contains the logic to be executed
	 * when removing this migration.
	 *
	 * The default implementation throws an exception
	 * indicating the migration cannot be removed.
	 **/
	public function down()
	{

		// drops foreign key for table `user`
		$this->dropForeignKey(
			'fk-invoices-customer_id',
			'{{%invoices}}'
		);

		// drops index for column `customer_id`
		$this->dropIndex(
			'idx-invoices-customer_id',
			'{{%invoices}}'
		);

		$this->dropTable( '{{%invoices}}' );

	}

}