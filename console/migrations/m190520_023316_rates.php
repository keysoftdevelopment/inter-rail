<?php use yii\db\Migration;

/**************************************/
/*                                    */
/*            RATES TABLE             */
/*                                    */
/**************************************/

class m190520_023316_rates extends Migration
{

	/**
	 * This method contains the logic to be executed
	 * when applying this migration.
	**/
	public function up()
	{

		$tableOptions = null;

		if ( $this->db->driverName === 'mysql' ) {
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		}


		$this->createTable( '{{%rates}}', [
			'id'             => $this->primaryKey(),
			'from'			 => $this->string(),
			'to'			 => $this->string(),
			'term_from'	     => $this->integer(),
			'term_to'	     => $this->integer(),
			'wd' 		 	 => $this->float()->defaultValue( 0 ),
			'forming'    	 => $this->float()->defaultValue( 0 ),
			'forwarding' 	 => $this->float()->defaultValue( 0 ),
			'rental'     	 => $this->float()->defaultValue( 0 ),
			'othc'	  	     => $this->float()->defaultValue( 0 ),
			'margin' 	 	 => $this->float()->defaultValue( 0 ),
			'rental_period'  => $this->integer()->defaultValue( 0 ),
			'surcharge'		 => $this->float()->defaultValue( 0 ),
			'transportation' => $this->string( 100 )->defaultValue( 'import' ),
			'customs'		 => $this->text(),
			'updated_at'     => $this->dateTime(),
			'created_at' 	 => $this->dateTime()
		], $tableOptions );

	}

	/**
	 * This method contains the logic to be executed
	 * when removing this migration.
	 *
	 * The default implementation throws an exception
	 * indicating the migration cannot be removed.
    **/
	public function down()
	{

		$this->dropTable( '{{%rates}}' );

	}

}