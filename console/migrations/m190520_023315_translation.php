<?php use yii\db\Migration;

/**************************************/
/*                                    */
/*     LANGUAGES CONNECTION TABLE     */
/*                                    */
/**************************************/


class m190520_023315_translation extends Migration
{

	/**
	 * This method contains the logic to be executed
	 * when applying this migration.
	**/
    public function up()
    {

		$tableOptions = null;

		if ( $this->db->driverName === 'mysql' )
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

		$this->createTable( '{{%translation}}', [
			'id'          => $this->primaryKey(),
			'title'	      => $this->string()->null(),
			'description' => $this->text(),
			'seo'	      => $this->text(),
			'lang_id'     => $this->integer(),
			'foreign_id'  => $this->integer(),
			'type'        => $this->string()
		], $tableOptions );

		// creates index for column `lang_id`
		$this->createIndex(
			'idx-translation-lang_id',
			'{{%translation}}',
			'lang_id'
		);

		// add foreign key for table `languages`
		$this->addForeignKey(
			'fk-translation-lang_id',
			'{{%translation}}',
			'lang_id',
			'{{%languages}}',
			'id'
		);

    }

	/**
	 * This method contains the logic to be executed
	 * when removing this migration.
	 *
	 * The default implementation throws an exception
	 * indicating the migration cannot be removed.
	**/
    public function down()
    {

		// drops foreign key for table `languages`
		$this->dropForeignKey(
			'fk-translation-lang_id',
			'{{%translation}}'
		);

		// drops index for column `lang_id`
		$this->dropIndex(
			'idx-translation-lang_id',
			'{{%translation}}'
		);

		$this->dropTable( '{{%translation}}' );

    }

}