<?php use yii\db\Migration;

/**************************************/
/*                                    */
/*       WAGONS CONNECTION TABLE      */
/*                                    */
/**************************************/

class m190520_023309_wagons_relation extends Migration
{

	/**
	 * This method contains the logic to be executed
	 * when applying this migration.
	**/
	public function up()
	{

		$tableOptions = null;

		if ( $this->db->driverName === 'mysql' )
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

		$this->createTable( '{{%wagons_relation}}', [
			'id'         => $this->primaryKey(),
			'foreign_id' => $this->integer()->defaultValue( 0 ),
			'wagon_id'   => $this->integer()->defaultValue( 0 ),
			'type'		 => $this->string()
		], $tableOptions );

		// creates index for column `wagon_id`
		$this->createIndex(
			'idx-wagons_relation-wagon_id',
			'{{%wagons_relation}}',
			'wagon_id'
		);

		// add foreign key for table `wagons`
		$this->addForeignKey(
			'fk-wagons_relation-wagon_id',
			'{{%wagons_relation}}',
			'wagon_id',
			'{{%wagons}}',
			'id'
		);

	}

	/**
	 * This method contains the logic to be executed
	 * when removing this migration.
	 *
	 * The default implementation throws an exception
	 * indicating the migration cannot be removed.
	**/
	public function down()
	{

		// drops foreign key for table `wagons`
		$this->dropForeignKey(
			'fk-wagons_relation-wagon_id',
			'{{%wagons_relation}}'
		);

		// drops index for column `wagon_id`
		$this->dropIndex(
			'idx-wagons_relation-wagon_id',
			'{{%wagons_relation}}'
		);

		$this->dropTable( '{{%wagons_relation}}' );

	}

}
