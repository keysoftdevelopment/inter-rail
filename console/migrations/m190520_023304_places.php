<?php use yii\db\Migration;

/**************************************/
/*                                    */
/*     		 PLACES TABLE      	      */
/*                                    */
/**************************************/

class m190520_023304_places extends Migration
{


	/**
	 * This method contains the logic to be executed
	 * when applying this migration.
	**/
    public function up()
    {

        $tableOptions = null;

        if ( $this->db->driverName === 'mysql' )
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

        $this->createTable( '{{%places}}', [
            'id'           => $this->primaryKey(),
            'name'		   => $this->string()->defaultValue( '' ),
            'lat'          => $this->string()->defaultValue( 0 ),
            'lng'          => $this->string()->defaultValue( 0 ),
			'foreign_id'   => $this->integer()->null(),
            'country_code' => $this->string()->null(),
            'country'      => $this->string()->defaultValue( '' ),
            'city'         => $this->string()->defaultValue( '' ),
            'district'     => $this->string()->defaultValue( '' ),
            'street'       => $this->string()->defaultValue( '' ),
			'postal_code'  => $this->string()->defaultValue( '' ),
			'type'		   => $this->string()->notNull()
        ], $tableOptions );

    }

	/**
	 * This method contains the logic to be executed
	 * when removing this migration.
	 *
	 * The default implementation throws an exception
	 * indicating the migration cannot be removed.
	**/
    public function down()
    {
        $this->dropTable( '{{%places}}' );
    }

}
