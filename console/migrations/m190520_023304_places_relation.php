<?php use yii\db\Migration;

/**************************************/
/*                                    */
/*   	 PLACES RELATION TABLE  	  */
/*                                    */
/**************************************/

class m190520_023304_places_relation extends Migration
{

	/**
	 * This method contains the logic to be executed
	 * when applying this migration.
	**/
	public function up()
	{

		$tableOptions = null;

		if ( $this->db->driverName === 'mysql' )
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

		$this->createTable( '{{%places_relation}}', [
			'id'         => $this->primaryKey(),
			'foreign_id' => $this->integer()->notNull(),
			'place_id'	 => $this->integer()->notNull(),
			'type'		   => $this->string()
		], $tableOptions );

		// creates index for column `place_id`
		$this->createIndex(
			'idx-places_relation-place_id',
			'{{%places_relation}}',
			'place_id'
		);

		// add foreign key for table `places`
		$this->addForeignKey(
			'fk-places_relation-place_id',
			'{{%places_relation}}',
			'place_id',
			'{{%places}}',
			'id'
		);

	}

	/**
	 * This method contains the logic to be executed
	 * when removing this migration.
	 *
	 * The default implementation throws an exception
	 * indicating the migration cannot be removed.
	 **/
	public function down()
	{

		// drops foreign key for table `places`
		$this->dropForeignKey(
			'fk-places_relation-place_id',
			'{{%places_relation}}'
		);

		// drops index for column `place_id`
		$this->dropIndex(
			'idx-places_relation-place_id',
			'{{%places_relation}}'
		);

		$this->dropTable( '{{%places_relation}}' );

	}

}
