<?php use yii\db\Migration;

/**************************************/
/*                                    */
/*     		LOGISTICS TABLE     	  */
/*                                    */
/**************************************/

class m190520_023310_logistics extends Migration
{

	/**
	 * This method contains the logic to be executed
	 * when applying this migration.
	**/
    public function up()
    {

		$tableOptions = null;

		if ( $this->db->driverName === 'mysql' )
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

		$this->createTable( '{{%logistics}}', [
			'id'              	  => $this->primaryKey(),
			'name'		      	  => $this->string()->null(),
			'from'			  	  => $this->string( 100 )->null(),
			'to'			  	  => $this->string( 100 )->null(),
			'duration'	      	  => $this->integer()->null(),
			'description'     	  => $this->text(),
			'owner'		      	  => $this->string()->null(),
			'transportation'      => $this->string( 100 )->defaultValue( 'export' ),
			'c_type' 	  		  => $this->string( 100 )->defaultValue( 'coc' ),
			'transport_no'    	  => $this->string()->null(),
			'transport_ind'	  	  => $this->string()->null(),
			'weight'	  	  	  => $this->integer()->null(),
			'railway_details' 	  => $this->text()->null(),
			'rate'		      	  => $this->float()->defaultValue( 0 ),
			'dthc'			  	  => $this->float()->defaultValue( 0 ),
			'othc'			  	  => $this->float()->defaultValue( 0 ),
			'bl'			  	  => $this->float()->defaultValue( 0 ),
			'surcharge'       	  => $this->float()->defaultValue( 0 ),
			'total' 	      	  => $this->float()->defaultValue( 0 ),
			'type'		      	  => $this->string()->notNull(),
			'status'		 	  => $this->string( 100 )->defaultValue( 'empty' ),
			'updated_at'      	  => $this->dateTime(),
			'created_at'      	  => $this->dateTime()
		], $tableOptions );

    }

	/**
	 * This method contains the logic to be executed
	 * when removing this migration.
	 *
	 * The default implementation throws an exception
	 * indicating the migration cannot be removed.
	**/
    public function down()
    {
		$this->dropTable( '{{%logistics}}' );
    }

}