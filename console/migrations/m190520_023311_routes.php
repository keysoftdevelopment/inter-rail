<?php use yii\db\Migration;

/**************************************/
/*                                    */
/*   		 ROUTES TABLE  		      */
/*                                    */
/**************************************/

class m190520_023311_routes extends Migration
{

	/**
	 * This method contains the logic to be executed
	 * when applying this migration.
	**/
	public function up()
	{

		$tableOptions = null;

		if ( $this->db->driverName === 'mysql' )
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

		$this->createTable( '{{%routes}}', [
			'id'       => $this->primaryKey(),
			'from'     => $this->integer()->notNull(),
			'to'       => $this->integer()->notNull(),
			'rate'     => $this->integer()->defaultValue( 0 ),
			'duration' => $this->integer()->null()
		], $tableOptions );

		// creates index for columns `from` and `to`
		$this->createIndex(
			'idx-routes-from',
			'{{%routes}}', [
				'from',
				'to'
			]
		);

		// add foreign key for table `places`
		$this->addForeignKey(
			'fk-routes-from',
			'{{%routes}}',
			'from',
			'{{%places}}',
			'id'
		);

	}

	/**
	 * This method contains the logic to be executed
	 * when removing this migration.
	 *
	 * The default implementation throws an exception
	 * indicating the migration cannot be removed.
	**/
	public function down()
	{

		// drops foreign key for table `places`
		$this->dropForeignKey(
			'fk-routes-from',
			'{{%routes}}'
		);

		// drops index for column `from`
		$this->dropIndex(
			'idx-routes-from',
			'{{%routes}}'
		);

		$this->dropTable( '{{%routes}}' );

	}

}
