<?php use yii\db\Migration;

/**************************************/
/*                                    */
/*           CATEGORIES TABLE         */
/*                                    */
/**************************************/

class m190520_023307_taxonomies extends Migration
{

	/**
	 * This method contains the logic to be executed
	 * when applying this migration.
	**/
    public function up()
    {

        $tableOptions = null;

        if ( $this->db->driverName === 'mysql' )
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

        $this->createTable( '{{%taxonomies}}', [
            'id'          => $this->primaryKey(),
			'icon'		  => $this->string()->null(),
            'name'        => $this->string()->notNull(),
            'description' => $this->text(),
            'file_id'     => $this->integer()->null(),
            'lang_id'	  => $this->integer()->null(),
            'parent'      => $this->integer()->null()->defaultValue( 0 ),
            'type'        => $this->string(),
			'containers'  => $this->string()->null(),
			'wagons'      => $this->string()->null(),
			'weights'     => $this->string()->null(),
            'slug'        => $this->string(),
            'isDeleted'   => $this->boolean()->notNull()->defaultValue( false ),
			'updated_at'  => $this->dateTime(),
			'created_at'  => $this->dateTime()
        ], $tableOptions );

        // creates index for column `file_id`
        $this->createIndex(
            'idx-taxonomies-file_id',
            '{{%taxonomies}}',
            'file_id'
        );

        // add foreign key for table `files`
        $this->addForeignKey(
            'fk-taxonomies-file_id',
            '{{%taxonomies}}',
            'file_id',
            '{{%files}}',
            'id'
        );

    }

	/**
	 * This method contains the logic to be executed
	 * when removing this migration.
	 *
	 * The default implementation throws an exception
	 * indicating the migration cannot be removed.
	**/
    public function down()
    {

        // drops foreign key for table `files`
        $this->dropForeignKey(
            'fk-taxonomies-file_id',
            '{{%taxonomies}}'
        );

        // drops index for column `file_id`
        $this->dropIndex(
            'idx-taxonomies-file_id',
            '{{%taxonomies}}'
        );

        $this->dropTable( '{{%taxonomies}}' );

    }

}
