<?php use yii\db\Migration;

/**************************************/
/*                                    */
/*        RAILWAY CODES TABLE         */
/*                                    */
/**************************************/

class m190520_023319_railway_codes extends Migration
{

	/**
	 * This method contains the logic to be executed
	 * when applying this migration.
	**/
	public function up()
	{

		$tableOptions = null;

		if ( $this->db->driverName === 'mysql' )
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

		$this->createTable( '{{%railway_codes}}', [
			'id'         => $this->primaryKey(),
			'order_id'   => $this->integer()->notNull(),
			'country'    => $this->string()->defaultValue( '' ),
			'route'      => $this->string()->defaultValue( '' ),
			'forwarder'  => $this->string()->defaultValue( '' ),
			'code'       => $this->string()->defaultValue( '' ),
			'rate'		 => $this->float()->defaultValue( 0 ),
			'surcharge'	 => $this->float()->defaultValue( 0 ),
			'created_at' => $this->dateTime()
		], $tableOptions );

		// creates index for column `order_id`
		$this->createIndex(
			'idx-railway_codes-order_id',
			'{{%railway_codes}}',
			'order_id'
		);

		// add foreign key for table `orders`
		$this->addForeignKey(
			'fk-railway_codes-order_id',
			'{{%railway_codes}}',
			'order_id',
			'{{%orders}}',
			'id'
		);

	}

	/**
	 * This method contains the logic to be executed
	 * when removing this migration.
	 *
	 * The default implementation throws an exception
	 * indicating the migration cannot be removed.
	**/
	public function down()
	{

		// drops foreign key for table `orders`
		$this->dropForeignKey(
			'fk-railway_codes-order_id',
			'{{%railway_codes}}'
		);

		// drops index for column `order_id`
		$this->dropIndex(
			'idx-railway_codes-order_id',
			'{{%railway_codes}}'
		);

		$this->dropTable( '{{%railway_codes}}' );

	}

}
