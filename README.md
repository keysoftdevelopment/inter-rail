                                                                                                    ------------------------------------
                                                                                                    INTER-RAIL WEB-SERVICE DOCUMENTATION 
                                                                                                    ------------------------------------
         
Current web service build on **YII Framework** version **2.0** 
        
Web Service allows **admin** panel and **website** views: 
1. Admin panel controllers, views and etc.. located at **backend** directory 
2. Web Service controllers, views and etc.. located at **fronted** directory

Models for backend and fronted located at **common** directory that allows connections between backend and frontend side.
All Migrations for current service located at **console** directory and allows table migration to database.

***The aim of that service:***

1. show customer **transportation routes**;
2. allow customer see progress of his **transportation packages**; 
3. allow customer get **invoice details** and see **progress** and **process** of all **transportation** details;
4. **connect** customer with **service owners** and allow **automate** all **logistics processes**
5. allows **service owners** to get **reports** for **expenses**, **revenue**, **transportation** and etc ... 
6. statistics of **orders**, **customer registrations** and etc ...
7. connection with **Lotus** system and auto invoice creation on that service
8. connections with **google map** service for route **drawing** and show which **vehicle** will be used for **package** delivering
9. auto **reports** to **terminals** with **container** details 
10. **mail** reports to **customer** and **managers** due to progress in order  
and etc...
           
**Bellow given short information about service configurations, used libraries and project structure.**

                                                                                                        -------------------------
                                                                                                        I. SERVICE CONFIGURATIONS
                                                                                                        ------------------------
       
Below given short information about service configurations:

```
I. server services
------------------------------------------------------------------------ 
Below given the list of required services on server 
------------------------------------------------------------------------
       
#php > 7.0 & < 7.3
install php version not less than 7.0, but less than 7.3
     
#php-fpm
install that service for connection php and nginx part
    
#mysql    
that database was used in current service
       
#nginx
that server service allow to run current web service               
      
------------------------------------------------------------------------
------------------------------------------------------------------------
------------------------------------------------------------------------
    
II. service configurations
------------------------------------------------------------------------ 
Below given helper for nginx configurations:
------------------------------------------------------------------------
        
#add service links        
edit bootstrap.php file located at .../common/config/bootstrap.php and add service links
                
#add database configurations
edit db.php file located at .../comon/config/db.php and add database details,
i.e: username, pswd, host, db name
       
#run command for all dependencies installing
run command `composer install`, i.e. if you do not have composer on your server,
install composer firstly
       
#run database migrations command 
yii migrate
        
#rbac initializations                
yii rbac/init
       
#database rbac tables migrations                
yii migrate --migrationPath=@yii/rbac/migrations
        
#user credentials
login: admin@interrail.uz
password: interrail
           
------------------------------------------------------------------------
------------------------------------------------------------------------
------------------------------------------------------------------------
         
III. nginx configurations
------------------------------------------------------------------------ 
Below given helper for nginx configurations:
------------------------------------------------------------------------
        
#Backend Configurations        
         
server
{
    listen 80;
    server_name admin.interrail.uz;
    root /var/www/inter-rail/backend/web;

    index index.php;

     location / {
        try_files $uri $uri/ /index.php?$query_string;
     }

    location ~ \.php$ {
        try_files $uri =404;
        include snippets/fastcgi-php.conf;
        fastcgi_pass unix:/var/run/php/php7.1-fpm.sock;
    }

   location ~ \.css {
        add_header  Content-Type    text/css;
   }

   location ~ \.js {
        add_header  Content-Type    application/x-javascript;
   }

}
      
------------------------------------------------------------------------    
    
#Frontend Configurations
        
server
{
    listen 80;
    server_name www.dev-interrail.uz dev-interrail.uz;
    root /var/www/inter-rail/frontend/web;

    index index.php;

     location / {
        try_files $uri $uri/ /index.php?$query_string;
     }

    location ~ \.php$ {
        try_files $uri =404;
        include snippets/fastcgi-php.conf;
        fastcgi_pass unix:/var/run/php/php7.1-fpm.sock;

    }

   location ~ \.css {
            add_header  Content-Type    text/css;
   }

   location ~ \.js {
           add_header  Content-Type    application/x-javascript;
   }

}
                
------------------------------------------------------------------------
------------------------------------------------------------------------
------------------------------------------------------------------------
```

                                                                           -----------------------------------------------------------------
                                                                           II. YII2 installation of libraries and short description of them.
                                                                           -----------------------------------------------------------------
        
Current service consist of the list of next libraries, which should be installed by running command **componser install**

```
I. zyx/zyx-phpmailer                   

------------------------------------------------------------------------------------
DESCRIPTION: This extension adds integration of PHPMailer to Yii 2 framework
LINK:        https://packagist.org/packages/zyx/zyx-phpmailer
------------------------------------------------------------------------------------
-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
------------------------------------------------------------------------------------
             
II. mongosoft/yii2-upload-behavior
------------------------------------------------------------------------------------
DESCRIPTION: This behavior automatically uploads file and fills the specified attribute with a value of the name of the uploaded file
LINK:        https://packagist.org/packages/mongosoft/yii2-upload-behavior     
------------------------------------------------------------------------------------
-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
------------------------------------------------------------------------------------
             
III. arogachev/yii2-many-to-many
------------------------------------------------------------------------------------
DESCRIPTION: Implementation of Many-to-many relationship for Yii 2 framework.
LINK:        https://packagist.org/packages/arogachev/yii2-many-to-many
------------------------------------------------------------------------------------
-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
------------------------------------------------------------------------------------
             
IV. yii2tech/ar-softdelete
------------------------------------------------------------------------------------
DESCRIPTION: This extension provides support for ActiveRecord soft delete, i.e data 
will not be removed from database and just be marked that it is deleted
             
LINK: https://packagist.org/packages/yii2tech/ar-softdelete
             
------------------------------------------------------------------------------------
-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
------------------------------------------------------------------------------------
             
V. yiisoft/yii2-httpclient
------------------------------------------------------------------------------------
DESCRIPTION: This extension provides the HTTP client for the Yii framework 2.0.
LINK:        https://packagist.org/packages/yiisoft/yii2-httpclient     
------------------------------------------------------------------------------------
-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
------------------------------------------------------------------------------------
             
VI. guzzlehttp/guzzle
------------------------------------------------------------------------------------
DESCRIPTION: Guzzle is a PHP HTTP client that makes it easy to send HTTP requests and 
trivial to integrate with web services.
             
LINK: https://packagist.org/packages/guzzlehttp/guzzle     
             
------------------------------------------------------------------------------------
-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
------------------------------------------------------------------------------------
             
VII. kartik-v/yii2-widget-typeahead
------------------------------------------------------------------------------------
DESCRIPTION: The TypeaheadBasic widget is a Yii 2 wrapper for for the Twitter Typeahead.js 
plugin with certain custom enhancements. This input widget is a jQuery based replacement for
text inputs providing search and typeahead functionality.
             
LINK: https://packagist.org/packages/kartik-v/yii2-widget-typeahead
             
------------------------------------------------------------------------------------
-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
------------------------------------------------------------------------------------
             
VIII. kartik-v/yii2-datecontrol
------------------------------------------------------------------------------------
DESCRIPTION: The Date Control module allows controlling date formats of attributes 
separately for View and Model for Yii Framework 2.0. It thus allows an easy way to work 
with dates when displaying to users in one way (format / timezone) but saving it in the database 
in another way (format / timezone).
             
LINK: https://packagist.org/packages/kartik-v/yii2-datecontrol     
             
------------------------------------------------------------------------------------
-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
------------------------------------------------------------------------------------
             
IX. kartik-v/yii2-widget-datepicker
------------------------------------------------------------------------------------
DESCRIPTION: The DatePicker widget is a Yii 2 wrapper for the Bootstrap DatePicker plugin 
with various enhancements.
             
LINK: https://packagist.org/packages/kartik-v/yii2-widget-datepicker    
             
------------------------------------------------------------------------------------
-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
------------------------------------------------------------------------------------
             
X. kartik-v/yii2-field-range
------------------------------------------------------------------------------------
DESCRIPTION: A Yii 2 extension that allows you to easily setup ActiveField range fields 
with Bootstrap 3 addons markup and more
             
LINK: https://packagist.org/packages/kartik-v/yii2-field-range    
             
------------------------------------------------------------------------------------
-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
------------------------------------------------------------------------------------
             
XI. trntv/yii2-command-bus
------------------------------------------------------------------------------------
DESCRIPTION: The idea of a command bus is that you create command objects that represent 
what you want your application to do. Then, you toss it into the bus and the bus makes sure 
that the command object gets to where it needs to go. So, the command goes in -> the bus 
hands it off to a handler -> and then the handler actually does the job. The command essentially 
represents a method call to your service layer.
             
LINK: https://packagist.org/packages/trntv/yii2-command-bus   
             
------------------------------------------------------------------------------------
-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
------------------------------------------------------------------------------------
             
XII. intervention/image
------------------------------------------------------------------------------------
DESCRIPTION: Intervention Image is an open source PHP image handling and manipulation 
library. It provides an easier and expressive way to create, edit, and compose images 
and supports currently the two most common image processing libraries GD Library and 
Imagick.
             
LINK: https://packagist.org/packages/intervention/image   
             
------------------------------------------------------------------------------------
-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
------------------------------------------------------------------------------------
             
XIII. kartik-v/yii2-date-range
------------------------------------------------------------------------------------
DESCRIPTION: An advanced date range picker input for Yii Framework 2 based on 
dangrossman/bootstrap-daterangepicker plugin.
             
LINK: https://packagist.org/packages/kartik-v/yii2-date-range   
             
------------------------------------------------------------------------------------
-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
------------------------------------------------------------------------------------
             
XIV. hscstudio/yii2-export
------------------------------------------------------------------------------------
DESCRIPTION: Export to All Formats (pdf, docx, doc, odt, odf, xls, xlsx, ppt, pptx) 
With Yii 2.0 plus import from .xls or .xlsx
             
LINK: https://packagist.org/packages/hscstudio/yii2-export   
             
------------------------------------------------------------------------------------
-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
------------------------------------------------------------------------------------
             
XV. 2amigos/yii2-tinymce-widget
------------------------------------------------------------------------------------
DESCRIPTION: Renders a TinyMCE WYSIWYG text editor plugin widget.
LINK       : https://packagist.org/packages/2amigos/yii2-tinymce-widget 
------------------------------------------------------------------------------------
-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
------------------------------------------------------------------------------------
             
XVI. kartik-v/yii2-mpdf
------------------------------------------------------------------------------------
DESCRIPTION: The yii2-mpdf extension is a Yii2 wrapper component for the mPDF library 
with enhancements. The mPDF library offers ability to generate PDF files from UTF-8 
encoded HTML
             
LINK: https://packagist.org/packages/kartik-v/yii2-mpdf   
             
------------------------------------------------------------------------------------
-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
------------------------------------------------------------------------------------
```

                                                                                                     ------------------------
                                                                                                     III. DIRECTORY STRUCTURE
                                                                                                     ------------------------
                                                        
Short description of service directory structure

```
backend - backend web application.
    assets/              contains application assets such as JavaScript and CSS
    components/          contains backend helper methods
    config/              contains backend configurations
    controllers/         contains Web controller classes
    messages/            contains backend translations details
    runtime/             contains files generated during runtime
    views/               contains view files for the Web application
    web/                 contains the entry script and Web resources
    widgets/             contains additional backend view helpers
common - files common to all applications.
    behaviors/           contains main system behaviours
    command/             contains system commands
    config/              contains shared configurations
    mail/                contains view files for e-mails
    models/              contains model classes used in both backend and frontend
    widgets/             contains additional common view helpers
console - console application.
    config/              contains console configurations
    controllers/         contains console controllers (commands)
    migrations/          contains database migrations
    models/              contains console-specific model classes
    runtime/             contains files generated during runtime
environments/            contains environment-based overrides
frontend -  frontend web application.
    assets/              contains application assets such as JavaScript and CSS
    components/          contains frontend helper methods
    config/              contains frontend configurations
    messages/            contains frontend translations details
    models/              contains frontend-specific model classes
    modules/             contains frontend controllers
    runtime/             contains files generated during runtime
    themes/              contains view files for the Web application
    web/                 contains the entry script and Web resources
    widgets/             contains frontend widgets
tests                    contains various tests for the advanced application
    codeception/         contains tests developed with Codeception PHP Testing Framework
vendor/                  contains dependent 3rd-party packages
```
