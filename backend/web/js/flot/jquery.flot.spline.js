/**
 * Flot plugin that provides spline interpolation for line graphs
 * author: Alex Bardas < alex.bardas@gmail.com >
 * modified by: Avi Kohn https://github.com/AMKohn
 * based on the spline interpolation described at:
 *         http://scaledinnovation.com/analytics/splines/aboutSplines.html
 *
 * Example usage: (add in plot options series object)
 *        for linespline:
 *            series: {
 *				...
 *				lines: {
 *					show: false
 *				},
 *				splines: {
 *					show: true,
 *					tension: x, (float between 0 and 1, defaults to 0.5),
 *					lineWidth: y (number, defaults to 2),
 *					fill: z (float between 0 .. 1 or false, as in flot documentation)
 *				},
 *				...
 *			}
 *        areaspline:
 *            series: {
 *				...
 *				lines: {
 *					show: true,
 *					lineWidth: 0, (line drawing will not execute)
 *					fill: x, (float between 0 .. 1, as in flot documentation)
 *					...
 *				},
 *				splines: {
 *					show: true,
 *					tension: 0.5 (float between 0 and 1)
 *				},
 *				...
 *			}
 *
 */

!function(f){"use strict";function d(i,e,s,t,n,l,o){var a,r,c,p=Math.pow,h=Math.sqrt;return[s+(r=o*(a=h(p(s-i,2)+p(t-e,2)))/(a+h(p(n-s,2)+p(l-t,2))))*(i-n),t+r*(e-l),s-(c=o-r)*(i-n),t-c*(e-l)]}var x=[];function v(i,e,s,t){(void 0===e||"bezier"!==e&&"quadratic"!==e)&&(e="quadratic"),e+="CurveTo",0==x.length?x.push([s[0],s[1],t.concat(s.slice(2)),e]):"quadraticCurveTo"==e&&2==s.length?(t=t.slice(0,2).concat(s),x.push([s[0],s[1],t,e])):x.push([s[2],s[3],t.concat(s.slice(2)),e])}function e(i,e,s){if(!0===s.splines.show){var t,n,l,o=[],a=s.splines.tension||.5,r=s.datapoints.points,c=s.datapoints.pointsize,p=i.getPlotOffset(),h=r.length,u=[];if(x=[],h/c<4)f.extend(s.lines,s.splines);else{for(t=0;t<h;t+=c)n=r[t],l=r[t+1],null==n||n<s.xaxis.min||n>s.xaxis.max||l<s.yaxis.min||l>s.yaxis.max||u.push(s.xaxis.p2c(n)+p.left,s.yaxis.p2c(l)+p.top);for(h=u.length,t=0;t<h-2;t+=2)o=o.concat(d.apply(this,u.slice(t,t+6).concat([a])));for(e.save(),e.strokeStyle=s.color,e.lineWidth=s.splines.lineWidth,v(0,"quadratic",u.slice(0,4),o.slice(0,2)),t=2;t<h-3;t+=2)v(0,"bezier",u.slice(t,t+4),o.slice(2*t-2,2*t+2));v(0,"quadratic",u.slice(h-2,h),[o[2*h-10],o[2*h-9],u[h-4],u[h-3]]),function(i,e,s,t,n){var l=f.color.parse(n);l.a="number"==typeof t?t:.3,l.normalize(),l=l.toString(),e.beginPath(),e.moveTo(i[0][0],i[0][1]);for(var o=i.length,a=0;a<o;a++)e[i[a][3]].apply(e,i[a][2]);e.stroke(),e.lineWidth=0,e.lineTo(i[o-1][0],s),e.lineTo(i[0][0],s),e.closePath(),!1!==t&&(e.fillStyle=l,e.fill())}(x,e,i.height()+10,s.splines.fill,s.color),e.restore()}}}f.plot.plugins.push({init:function(i){i.hooks.drawSeries.push(e)},options:{series:{splines:{show:!1,lineWidth:2,tension:.5,fill:!1}}},name:"spline",version:"0.8.2"})}(jQuery);
