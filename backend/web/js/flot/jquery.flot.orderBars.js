/*
 * Flot plugin to order bars side by side.
 *
 * Released under the MIT license by Benjamin BUFFET, 20-Sep-2010.
 * Modifications made by Steven Hall <github.com/emmerich>, 01-May-2013.
 *
 * This plugin is an alpha version.
 *
 * To activate the plugin you must specify the parameter "order" for the specific serie :
 *
 *  $.plot($("#placeholder"), [{ data: [ ... ], bars :{ order = null or integer }])
 *
 * If 2 series have the same order param, they are ordered by the position in the array;
 *
 * The plugin adjust the point by adding a value depanding of the barwidth
 * Exemple for 3 series (barwidth : 0.1) :
 *
 *          first bar décalage : -0.15
 *          second bar décalage : -0.05
 *          third bar décalage : 0.05
 *
 */

// INFO: decalage/decallage is French for gap. It's used to denote the spacing applied to each
// bar.
jQuery.plot.plugins.push({init:function(r){var f,c,g,p,v=1,W=!1,w={};function M(r,a){for(var n=new Array,t=0;t<r.length;t++)n[0]=r[t].data[0]?r[t].data[0][a]:null,n[1]=r[t].data[r[t].data.length-1]?r[t].data[r[t].data.length-1][a]:null;return n}function y(r,a){var n=r.bars.order,t=a.bars.order;return n<t?-1:t<n?1:0}function D(r,a,n){for(var t=0,e=a;e<=n;e++)t+=r[e].bars.barWidth+2*p;return t}r.hooks.processDatapoints.push(function(r,a,n){var t,e,o,s,i,l,d,u=null;if(null!=(l=a).bars&&l.bars.show&&null!=l.bars.order&&(a.bars.horizontal&&(W=!0),e=r,o=W?e.getPlaceholder().innerHeight():e.getPlaceholder().innerWidth(),s=M(e.getData(),W?1:0),i=s[1]-s[0],v=i/o,f=function(r){for(var a=new Array,n=[],t=0;t<r.length;t++)null!=r[t].bars.order&&r[t].bars.show&&n.indexOf(r[t].bars.order)<0&&(n.push(r[t].bars.order),a.push(r[t]));return a.sort(y)}(r.getData()),c=f.length,g=void 0!==(t=a).bars.lineWidth?t.bars.lineWidth:2,p=g*v,2<=c)){var h=function(r){for(var a=0,n=0;n<f.length;++n)if(r==f[n]){a=n;break}return a+1}(a),b=(c%2!=(d=0)&&(d=f[Math.ceil(c/2)].bars.barWidth/2),d);void 0===w[a.bars.order]&&(h<=Math.ceil(c/2)?w[a.bars.order]=-1*D(f,h-1,Math.floor(c/2)-1)-b:w[a.bars.order]=D(f,Math.ceil(c/2),h-2)+b+2*p),u=function(r,a,n){for(var t=r.pointsize,e=r.points,o=0,s=W?1:0;s<e.length;s+=t)e[s]+=n,a.data[o][3]=e[s],o++;return e}(n,a,w[a.bars.order]),n.points=u}return u})},options:{series:{bars:{order:null}}},name:"orderBars",version:"0.2"});