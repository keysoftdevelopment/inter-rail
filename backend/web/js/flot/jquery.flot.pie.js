/* Flot plugin for rendering pie charts.

 Copyright (c) 2007-2014 IOLA and Ole Laursen.
 Licensed under the MIT license.

 The plugin assumes that each series has a single data value, and that each
 value is a positive integer or zero.  Negative numbers don't make sense for a
 pie chart, and have unpredictable results.  The values do NOT need to be
 passed in as percentages; the plugin will calculate the total and per-slice
 percentages internally.

 * Created by Brian Medendorp

 * Updated with contributions from btburnett3, Anthony Aragues and Xavi Ivars

 The plugin supports these options:

 series: {
 pie: {
 show: true/false
 radius: 0-1 for percentage of fullsize, or a specified pixel length, or 'auto'
 innerRadius: 0-1 for percentage of fullsize or a specified pixel length, for creating a donut effect
 startAngle: 0-2 factor of PI used for starting angle (in radians) i.e 3/2 starts at the top, 0 and 2 have the same result
 tilt: 0-1 for percentage to tilt the pie, where 1 is no tilt, and 0 is completely flat (nothing will show)
 offset: {
 top: integer value to move the pie up or down
 left: integer value to move the pie left or right, or 'auto'
 },
 stroke: {
 color: any hexidecimal color value (other formats may or may not work, so best to stick with something like '#FFF')
 width: integer pixel width of the stroke
 },
 label: {
 show: true/false, or 'auto'
 formatter:  a user-defined function that modifies the text/style of the label text
 radius: 0-1 for percentage of fullsize, or a specified pixel length
 background: {
 color: any hexidecimal color value (other formats may or may not work, so best to stick with something like '#000')
 opacity: 0-1
 },
 threshold: 0-1 for the percentage value at which to hide labels (if they're too small)
 },
 combine: {
 threshold: 0-1 for the percentage value at which to combine slices (if they're too small)
 color: any hexidecimal color value (other formats may or may not work, so best to stick with something like '#CCC'), if null, the plugin will automatically use the color of the first slice to be combined
 label: any text value of what the combined slice should be labeled
 }
 highlight: {
 opacity: 0-1
 }
 }
 }

 More detail and specific examples can be found in the included HTML file.

 */

!function(A){var e={series:{pie:{show:!1,radius:"auto",innerRadius:0,startAngle:1.5,tilt:1,shadow:{left:5,top:15,alpha:.02},offset:{top:0,left:"auto"},stroke:{color:"#fff",width:1},label:{show:"auto",formatter:function(e,i){return"<div style='font-size:13px;text-align:center;padding:2px;color:"+i.color+";'>"+e+" "+Math.round(i.percent)+"%</div>"},radius:1,background:{color:null,opacity:0},threshold:0},combine:{threshold:-1,color:null,label:"Other"},highlight:{opacity:.5}}}};A.plot.plugins.push({init:function(h){var a=null,w=null,k=null,g=null,M=null,P=null,o=!1,c=null,u=[];function p(e){if(0<k.series.pie.innerRadius){e.save();var i=1<k.series.pie.innerRadius?k.series.pie.innerRadius:g*k.series.pie.innerRadius;e.globalCompositeOperation="destination-out",e.beginPath(),e.fillStyle=k.series.pie.stroke.color,e.arc(0,0,i,0,2*Math.PI,!1),e.fill(),e.closePath(),e.restore(),e.save(),e.beginPath(),e.strokeStyle=k.series.pie.stroke.color,e.arc(0,0,i,0,2*Math.PI,!1),e.stroke(),e.closePath(),e.restore()}}function d(e,i){for(var s=!1,t=-1,r=e.length,a=r-1;++t<r;a=t)(e[t][1]<=i[1]&&i[1]<e[a][1]||e[a][1]<=i[1]&&i[1]<e[t][1])&&i[0]<(e[a][0]-e[t][0])*(i[1]-e[t][1])/(e[a][1]-e[t][1])+e[t][0]&&(s=!s);return s}function t(e){i("plothover",e)}function r(e){i("plotclick",e)}function i(e,i){var s,t,r,a=h.offset(),l=function(e,i){for(var s,t,r=h.getData(),a=h.getOptions(),l=1<a.series.pie.radius?a.series.pie.radius:g*a.series.pie.radius,n=0;n<r.length;++n){var o=r[n];if(o.pie.show){if(c.save(),c.beginPath(),c.moveTo(0,0),c.arc(0,0,l,o.startAngle,o.startAngle+o.angle/2,!1),c.arc(0,0,l,o.startAngle+o.angle/2,o.startAngle+o.angle,!1),c.closePath(),s=e-M,t=i-P,c.isPointInPath){if(c.isPointInPath(e-M,i-P))return c.restore(),{datapoint:[o.percent,o.data],dataIndex:0,series:o,seriesIndex:n}}else if(d([[0,0],[l*Math.cos(o.startAngle),l*Math.sin(o.startAngle)],[l*Math.cos(o.startAngle+o.angle/4),l*Math.sin(o.startAngle+o.angle/4)],[l*Math.cos(o.startAngle+o.angle/2),l*Math.sin(o.startAngle+o.angle/2)],[l*Math.cos(o.startAngle+o.angle/1.5),l*Math.sin(o.startAngle+o.angle/1.5)],[l*Math.cos(o.startAngle+o.angle),l*Math.sin(o.startAngle+o.angle)]],[s,t]))return c.restore(),{datapoint:[o.percent,o.data],dataIndex:0,series:o,seriesIndex:n};c.restore()}}return null}(parseInt(i.pageX-a.left),parseInt(i.pageY-a.top));if(k.grid.autoHighlight)for(var n=0;n<u.length;++n){var o=u[n];o.auto!=e||l&&o.series==l.series||f(o.series)}l&&(s=l.series,t=e,-1==(r=v(s))?(u.push({series:s,auto:t}),h.triggerRedrawOverlay()):t||(u[r].auto=!1));var p={pageX:i.pageX,pageY:i.pageY};w.trigger(e,[p,l])}function f(e){null==e&&(u=[],h.triggerRedrawOverlay());var i=v(e);-1!=i&&(u.splice(i,1),h.triggerRedrawOverlay())}function v(e){for(var i=0;i<u.length;++i)if(u[i].series==e)return i;return-1}h.hooks.processOptions.push(function(e,i){i.series.pie.show&&(i.grid.show=!1,"auto"==i.series.pie.label.show&&(i.legend.show?i.series.pie.label.show=!1:i.series.pie.label.show=!0),"auto"==i.series.pie.radius&&(i.series.pie.label.show?i.series.pie.radius=.75:i.series.pie.radius=1),1<i.series.pie.tilt?i.series.pie.tilt=1:i.series.pie.tilt<0&&(i.series.pie.tilt=0))}),h.hooks.bindEvents.push(function(e,i){var s=e.getOptions();s.series.pie.show&&(s.grid.hoverable&&i.unbind("mousemove").mousemove(t),s.grid.clickable&&i.unbind("click").click(r))}),h.hooks.processDatapoints.push(function(e,i,s,t){var r;e.getOptions().series.pie.show&&(r=e,o||(o=!0,a=r.getCanvas(),w=A(a).parent(),k=r.getOptions(),r.setData(function(e){for(var i=0,s=0,t=0,r=k.series.pie.combine.color,a=[],l=0;l<e.length;++l){var n=e[l].data;A.isArray(n)&&1==n.length&&(n=n[0]),A.isArray(n)?!isNaN(parseFloat(n[1]))&&isFinite(n[1])?n[1]=+n[1]:n[1]=0:n=!isNaN(parseFloat(n))&&isFinite(n)?[1,+n]:[1,0],e[l].data=[n]}for(var l=0;l<e.length;++l)i+=e[l].data[0][1];for(var l=0;l<e.length;++l){var n=e[l].data[0][1];n/i<=k.series.pie.combine.threshold&&(s+=n,t++,r||(r=e[l].color))}for(var l=0;l<e.length;++l){var n=e[l].data[0][1];(t<2||n/i>k.series.pie.combine.threshold)&&a.push(A.extend(e[l],{data:[[1,n]],color:e[l].color,label:e[l].label,angle:n*Math.PI*2/i,percent:n/(i/100)}))}return 1<t&&a.push({data:[[1,s]],color:r,label:k.series.pie.combine.label,angle:s*Math.PI*2/i,percent:s/(i/100)}),a}(r.getData()))))}),h.hooks.drawOverlay.push(function(e,i){e.getOptions().series.pie.show&&function(e,i){var s=e.getOptions(),t=1<s.series.pie.radius?s.series.pie.radius:g*s.series.pie.radius;i.save(),i.translate(M,P),i.scale(1,s.series.pie.tilt);for(var r=0;r<u.length;++r)a(u[r].series);function a(e){e.angle<=0||isNaN(e.angle)||(i.fillStyle="rgba(255, 255, 255, "+s.series.pie.highlight.opacity+")",i.beginPath(),1e-9<Math.abs(e.angle-2*Math.PI)&&i.moveTo(0,0),i.arc(0,0,t,e.startAngle,e.startAngle+e.angle/2,!1),i.arc(0,0,t,e.startAngle+e.angle/2,e.startAngle+e.angle,!1),i.closePath(),i.fill())}p(i),i.restore()}(e,i)}),h.hooks.draw.push(function(e,i){e.getOptions().series.pie.show&&function(e,i){if(w){var v=e.getPlaceholder().width(),b=e.getPlaceholder().height(),s=w.children().filter(".legend").children().width()||0;c=i,o=!1,g=Math.min(v,b/k.series.pie.tilt)/2,P=b/2+k.series.pie.offset.top,M=v/2,"auto"==k.series.pie.offset.left?(k.legend.position.match("w")?M+=s/2:M-=s/2,M<g?M=g:v-g<M&&(M=v-g)):M+=k.series.pie.offset.left;for(var l=e.getData(),t=0;0<t&&(g*=.95),t+=1,r(),k.series.pie.tilt<=.8&&a(),!n()&&t<10;);10<=t&&(r(),w.prepend("<div class='error'>Could not draw pie with labels contained inside canvas</div>")),e.setSeries&&e.insertLegend&&(e.setSeries(l),e.insertLegend())}function r(){c.clearRect(0,0,v,b),w.children().filter(".pieLabel, .pieLabelBackground").remove()}function a(){var e=k.series.pie.shadow.left,i=k.series.pie.shadow.top,s=k.series.pie.shadow.alpha,t=1<k.series.pie.radius?k.series.pie.radius:g*k.series.pie.radius;if(!(v/2-e<=t||t*k.series.pie.tilt>=b/2-i||t<=10)){c.save(),c.translate(e,i),c.globalAlpha=s,c.fillStyle="#000",c.translate(M,P),c.scale(1,k.series.pie.tilt);for(var r=1;r<=10;r++)c.beginPath(),c.arc(0,0,t,0,2*Math.PI,!1),c.fill(),t-=r;c.restore()}}function n(){var t=Math.PI*k.series.pie.startAngle,r=1<k.series.pie.radius?k.series.pie.radius:g*k.series.pie.radius;c.save(),c.translate(M,P),c.scale(1,k.series.pie.tilt),c.save();for(var a=t,e=0;e<l.length;++e)l[e].startAngle=a,i(l[e].angle,l[e].color,!0);if(c.restore(),0<k.series.pie.stroke.width){c.save(),c.lineWidth=k.series.pie.stroke.width,a=t;for(var e=0;e<l.length;++e)i(l[e].angle,k.series.pie.stroke.color,!1);c.restore()}return p(c),c.restore(),!k.series.pie.label.show||function(){for(var e=t,f=1<k.series.pie.label.radius?k.series.pie.label.radius:g*k.series.pie.label.radius,i=0;i<l.length;++i){if(l[i].percent>=100*k.series.pie.label.threshold&&!s(l[i],e,i))return!1;e+=l[i].angle}return!0;function s(e,i,s){if(0==e.data[0][1])return!0;var t,r=k.legend.labelFormatter,a=k.series.pie.label.formatter;t=r?r(e.label,e):e.label,a&&(t=a(t,e));var l=(i+e.angle+i)/2,n=M+Math.round(Math.cos(l)*f),o=P+Math.round(Math.sin(l)*f)*k.series.pie.tilt,p="<span class='pieLabel' id='pieLabel"+s+"' style='position:absolute;top:"+o+"px;left:"+n+"px;'>"+t+"</span>";w.append(p);var h=w.children("#pieLabel"+s),g=o-h.height()/2,c=n-h.width()/2;if(h.css("top",g),h.css("left",c),0<0-g||0<0-c||b-(g+h.height())<0||v-(c+h.width())<0)return!1;if(0!=k.series.pie.label.background.opacity){var u=k.series.pie.label.background.color;null==u&&(u=e.color);var d="top:"+g+"px;left:"+c+"px;";A("<div class='pieLabelBackground' style='position:absolute;width:"+h.width()+"px;height:"+h.height()+"px;"+d+"background-color:"+u+";'></div>").css("opacity",k.series.pie.label.background.opacity).insertBefore(h)}return!0}}();function i(e,i,s){e<=0||isNaN(e)||(s?c.fillStyle=i:(c.strokeStyle=i,c.lineJoin="round"),c.beginPath(),1e-9<Math.abs(e-2*Math.PI)&&c.moveTo(0,0),c.arc(0,0,r,a,a+e/2,!1),c.arc(0,0,r,a+e/2,a+e,!1),c.closePath(),a+=e,s?c.fill():c.stroke())}}}(e,i)})},options:e,name:"pie",version:"1.1"})}(jQuery);