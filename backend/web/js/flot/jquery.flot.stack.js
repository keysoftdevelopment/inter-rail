/* Flot plugin for stacking data sets rather than overlyaing them.

 Copyright (c) 2007-2014 IOLA and Ole Laursen.
 Licensed under the MIT license.

 The plugin assumes the data is sorted on x (or y if stacking horizontally).
 For line charts, it is assumed that if a line has an undefined gap (from a
 null point), then the line above it should have the same gap - insert zeros
 instead of "null" if you want another behaviour. This also holds for the start
 and end of the chart. Note that stacking a mix of positive and negative values
 in most instances doesn't make sense (so it looks weird).

 Two or more series are stacked when their "stack" attribute is set to the same
 key (which can be any number or string or just "true"). To specify the default
 stack, you can set the stack option like this:

 series: {
 stack: null/false, true, or a key (number/string)
 }

 You can also specify it for a single series, like this:

 $.plot( $("#placeholder"), [{
 data: [ ... ],
 stack: true
 }])

 The stacking order is determined by the order of the data series in the array
 (later series end up on top of the previous).

 Internally, the plugin modifies the datapoints in each series, adding an
 offset to the y value. For line series, extra data points are inserted through
 interpolation. If there's a second y value, it's also adjusted (e.g for bar
 charts or filled areas).

 */
jQuery.plot.plugins.push({init:function(s){s.hooks.processDatapoints.push(function(s,n,t){if(null!=n.stack&&!1!==n.stack){var i=function(s,n){for(var t=null,i=0;i<n.length&&s!=n[i];++i)n[i].stack==s.stack&&(t=n[i]);return t}(n,s.getData());if(i){for(var l,o,e,u,f,p,a,r,h=t.pointsize,c=t.points,g=i.datapoints.pointsize,k=i.datapoints.points,v=[],m=n.lines.show,z=n.bars.horizontal,d=2<h&&(z?t.format[2].x:t.format[2].y),y=m&&n.lines.steps,D=!0,b=z?1:0,j=z?0:1,w=0,x=0;!(w>=c.length);){if(a=v.length,null==c[w]){for(r=0;r<h;++r)v.push(c[w+r]);w+=h}else if(x>=k.length){if(!m)for(r=0;r<h;++r)v.push(c[w+r]);w+=h}else if(null==k[x]){for(r=0;r<h;++r)v.push(null);D=!0,x+=g}else{if(l=c[w+b],o=c[w+j],u=k[x+b],f=k[x+j],p=0,l==u){for(r=0;r<h;++r)v.push(c[w+r]);v[a+j]+=f,p=f,w+=h,x+=g}else if(u<l){if(m&&0<w&&null!=c[w-h]){for(e=o+(c[w-h+j]-o)*(u-l)/(c[w-h+b]-l),v.push(u),v.push(e+f),r=2;r<h;++r)v.push(c[w+r]);p=f}x+=g}else{if(D&&m){w+=h;continue}for(r=0;r<h;++r)v.push(c[w+r]);m&&0<x&&null!=k[x-g]&&(p=f+(k[x-g+j]-f)*(l-u)/(k[x-g+b]-u)),v[a+j]+=p,w+=h}D=!1,a!=v.length&&d&&(v[a+2]+=p)}if(y&&a!=v.length&&0<a&&null!=v[a]&&v[a]!=v[a-h]&&v[a+1]!=v[a-h+1]){for(r=0;r<h;++r)v[a+h+r]=v[a+r];v[a+1]=v[a-h+1]}}t.points=v}}})},options:{series:{stack:null}},name:"stack",version:"1.2"});
