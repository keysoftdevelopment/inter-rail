/*
 Flot plugin for automatically redrawing plots when the placeholder
 size changes, e.g. on window resizes.

 It works by listening for changes on the placeholder div (through the
 jQuery resize event plugin) - if the size changes, it will redraw the
 plot.

 There are no options. If you need to disable the plugin for some
 plots, you can just fix the size of their placeholders.
 */


/* Inline dependency: 
 * jQuery resize event - v1.2 - 3/14/2010
 * http://benalman.com/projects/jquery-resize-plugin/
 * 
 * Copyright (c) 2010 "Cowboy" Ben Alman
 * Dual licensed under the MIT and GPL licenses.
 * http://benalman.com/about/license/
 */
!function(a,i,o){var e,n=a([]),h=a.resize=a.extend(a.resize,{}),s="setTimeout",r="resize",u=r+"-special-event",d="delay",c="throttleWindow";h[d]=250,h[c]=!0,a.event.special[r]={setup:function(){if(!h[c]&&this[s])return!1;var t=a(this);n=n.add(t),a.data(this,u,{w:t.width(),h:t.height()}),1===n.length&&function t(){e=i[s](function(){n.each(function(){var t=a(this),i=t.width(),e=t.height(),n=a.data(this,u);i===n.w&&e===n.h||t.trigger(r,[n.w=i,n.h=e])}),t()},h[d])}()},teardown:function(){if(!h[c]&&this[s])return!1;var t=a(this);n=n.not(t),t.removeData(u),n.length||clearTimeout(e)},add:function(t){if(!h[c]&&this[s])return!1;var r;function i(t,i,e){var n=a(this),h=a.data(this,u);h.w=i!==o?i:n.width(),h.h=e!==o?e:n.height(),r.apply(this,arguments)}if(a.isFunction(t))return r=t,i;r=t.handler,t.handler=i}}}(jQuery,this),function(e){var n=0;e.plot.plugins.push({init:function(t){t.hooks.bindEvents.push(function(i,t){n||i.getPlaceholder().resize(function(){var t=i.getPlaceholder();0!=t.width()&&0!=t.height()&&(++n,e.plot(t,i.getData(),i.getOptions()),--n)})})},options:{},name:"resize",version:"1.0"})}(jQuery);
