/* The MIT License

 Copyright (c) 2011 by Michael Zinsmaier and nergal.dev
 Copyright (c) 2012 by Thomas Ritou

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 */

/*
 ____________________________________________________

 what it is:
 ____________________________________________________

 curvedLines is a plugin for flot, that tries to display lines in a smoother way.
 This is achieved through adding of more data points. The plugin is a data processor and can thus be used
 in combination with standard line / point rendering options.

 => 1) with large data sets you may get trouble
 => 2) if you want to display the points too, you have to plot them as 2nd data series over the lines
 => 3) consecutive x data points are not allowed to have the same value

 Feel free to further improve the code

 ____________________________________________________

 how to use it:
 ____________________________________________________

 var d1 = [[5,5],[7,3],[9,12]];

 var options = { series: { curvedLines: {  active: true }}};

 $.plot($("#placeholder"), [{data: d1, lines: { show: true}, curvedLines: {apply: true}}], options);

 _____________________________________________________

 options:
 _____________________________________________________

 active:           bool true => plugin can be used
 apply:            bool true => series will be drawn as curved line
 monotonicFit:	   bool true => uses monotone cubic interpolation (preserve monotonicity)
 tension:          int          defines the tension parameter of the hermite spline interpolation (no effect if monotonicFit is set)
 nrSplinePoints:   int 			defines the number of sample points (of the spline) in between two consecutive points

 deprecated options from flot prior to 1.0.0:
 ------------------------------------------------
 legacyOverride	   bool true => use old default
 OR
 legacyOverride    optionArray
 {
 fit: 	             bool true => forces the max,mins of the curve to be on the datapoints
 curvePointFactor	 int  		  defines how many "virtual" points are used per "real" data point to
 emulate the curvedLines (points total = real points * curvePointFactor)
 fitPointDist: 	     int  		  defines the x axis distance of the additional two points that are used
 }						   		   	  to enforce the min max condition.
 */

/*
 *  v0.1   initial commit
 *  v0.15  negative values should work now (outcommented a negative -> 0 hook hope it does no harm)
 *  v0.2   added fill option (thanks to monemihir) and multi axis support (thanks to soewono effendi)
 *  v0.3   improved saddle handling and added basic handling of Dates
 *  v0.4   rewritten fill option (thomas ritou) mostly from original flot code (now fill between points rather than to graph bottom), corrected fill Opacity bug
 *  v0.5   rewritten instead of implementing a own draw function CurvedLines is now based on the processDatapoints flot hook (credits go to thomas ritou).
 * 		   This change breakes existing code however CurvedLines are now just many tiny straight lines to flot and therefore all flot lines options (like gradient fill,
 * 	       shadow) are now supported out of the box
 *  v0.6   flot 0.8 compatibility and some bug fixes
 *  v0.6.x changed versioning schema
 *
 *  v1.0.0 API Break marked existing implementation/options as deprecated
 *  v1.1.0 added the new curved line calculations based on hermite splines
 *  v1.1.1 added a rough parameter check to make sure the new options are used
 */

!function(r){var n={series:{curvedLines:{active:!1,apply:!1,monotonicFit:!1,tension:.5,nrSplinePoints:20,legacyOverride:void 0}}};jQuery.plot.plugins.push({init:function(r){function e(r,n,e){var i=e.points.length/e.pointsize;if(!function(r){if(void 0!==r.fit||void 0!==r.curvePointFactor||void 0!==r.fitPointDist)throw new Error("CurvedLines detected illegal parameters. The CurvedLines API changed with version 1.0.0 please check the options object.");return!1}(n.curvedLines)&&1==n.curvedLines.apply&&void 0===n.originSeries&&1.005<i)if(n.lines.fill){var t=a(e,n.curvedLines,1),o=a(e,n.curvedLines,2);e.pointsize=3,e.points=[];for(var s=0,u=0,v=0;v<t.length||s<o.length;)t[v]==o[s]?(e.points[u]=t[v],e.points[u+1]=t[v+1],e.points[u+2]=o[s+1],s+=2,v+=2):t[v]<o[s]?(e.points[u]=t[v],e.points[u+1]=t[v+1],e.points[u+2]=0<u?e.points[u-1]:null,v+=2):(e.points[u]=o[s],e.points[u+1]=1<u?e.points[u-2]:null,e.points[u+2]=o[s+1],s+=2),u+=3}else 0<n.lines.lineWidth&&(e.points=a(e,n.curvedLines,1),e.pointsize=2)}function a(r,n,e){if(void 0!==n.legacyOverride&&0!=n.legacyOverride){var i={fit:!1,curvePointFactor:20,fitPointDist:void 0};return function(r,n,e){var i=r.points,t=r.pointsize,o=Number(n.curvePointFactor)*(i.length/t),s=new Array,u=new Array,v=-1,a=-1,p=0;if(n.fit){var h;if(void 0===n.fitPointDist){var f=i[0],l=i[i.length-t];h=(l-f)/5e4}else h=Number(n.fitPointDist);for(var c=0;c<i.length;c+=t){var d,g;a=(v=c)+e,d=i[v]-h,g=i[v]+h;for(var y=2;d==i[v]||g==i[v];)d=i[v]-h*y,g=i[v]+h*y,y++;s[p]=d,u[p]=i[a],s[++p]=i[v],u[p]=i[a],s[++p]=g,u[p]=i[a],p++}}else for(var c=0;c<i.length;c+=t)a=(v=c)+e,s[p]=i[v],u[p]=i[a],p++;var w=s.length,L=new Array,P=new Array;L[0]=0,L[w-1]=0,P[0]=0;for(var c=1;c<w-1;++c){var m=s[c+1]-s[c-1];if(0==m)return[];var A=(s[c]-s[c-1])/m,z=A*L[c-1]+2;L[c]=(A-1)/z,P[c]=(u[c+1]-u[c])/(s[c+1]-s[c])-(u[c]-u[c-1])/(s[c]-s[c-1]),P[c]=(6*P[c]/(s[c+1]-s[c-1])-A*P[c-1])/z}for(var p=w-2;0<=p;--p)L[p]=L[p]*L[p+1]+P[p];var b=(s[w-1]-s[0])/(o-1),D=new Array,F=new Array,O=new Array;for(D[0]=s[0],F[0]=u[0],O.push(D[0]),O.push(F[0]),p=1;p<o;++p){D[p]=D[0]+p*b;for(var N=w-1,j=0;1<N-j;){var k=Math.round((N+j)/2);s[k]>D[p]?N=k:j=k}var S=s[N]-s[j];if(0==S)return[];var C=(s[N]-D[p])/S,Q=(D[p]-s[j])/S;F[p]=C*u[j]+Q*u[N]+((C*C*C-C)*L[j]+(Q*Q*Q-Q)*L[N])*(S*S)/6,O.push(D[p]),O.push(F[p])}return O}(r,jQuery.extend(i,n.legacyOverride),e)}return function(r,n,e){for(var i=r.points,t=r.pointsize,o=function(r,n,e){for(var i=r.points,t=r.pointsize,o=[],s=[],u=0;u<i.length-t;u+=t){var v=u,a=u+e,p=i[v+t]-i[v],h=i[a+t]-i[a];o.push(p),s.push(h/p)}var f=[s[0]];if(n.monotonicFit)for(var u=1;u<o.length;u++){var l=s[u],c=s[u-1];if(l*c<=0)f.push(0);else{var d=o[u],g=o[u-1],y=d+g;f.push(3*y/((y+d)/c+(y+g)/l))}}else for(var u=t;u<i.length-t;u+=t){var v=u,a=u+e;f.push(Number(n.tension)*(i[a+t]-i[a-t])/(i[v+t]-i[v-t]))}f.push(s[s.length-1]);var w=[],L=[];for(u=0;u<o.length;u++){var P=f[u],m=f[u+1],l=s[u],A=1/o[u],y=P+m-l-l;w.push(y*A*A),L.push((l-y-P)*A)}for(var z=[],u=0;u<o.length;u++)z.push(function(i,t,o,s,u){return function(r){var n=r-i,e=n*n;return t*n*e+o*e+s*n+u}}(i[u*t],w[u],L[u],f[u],i[u*t+e]));return z}(r,n,e),s=[],u=0,v=0;v<i.length-t;v+=t){var a=v,p=v+e,h=i[a],f=i[a+t],l=(f-h)/Number(n.nrSplinePoints);s.push(i[a]),s.push(i[p]);for(var c=h+=l;c<f;c+=l)s.push(c),s.push(o[u](c));u++}return s.push(i[i.length-t]),s.push(i[i.length-t+e]),s}(r,n,e)}r.hooks.processOptions.push(function(r,n){n.series.curvedLines.active&&r.hooks.processDatapoints.unshift(e)})},options:n,name:"curvedLines",version:"1.1.1"})}();