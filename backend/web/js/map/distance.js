/**
 * Name:        DISTANCE MAP
 * Description: INTER RAIL MAP SCRIPTS
 * Version:     1.0.0
**/

/**
 * @desc params initialization
**/
var map, marker_animation, offsetId, search;

/**
 * @desc this class will hold functions for inter rail maps side for current service
**/
var inter_rail_map =
{

    /**
     * @desc array of places
    **/
    places : [],

    /**
     * @desc routes duration
    **/
    duration: 0,

    /**
     * @desc array of markers
    **/
    markers : [],

    /**
     * @desc the list of objects of map lines
    **/
    polyline : {
        A  : [],
        B  : [],
        C  : [],
        D  : [],
        E  : [],
        F  : [],
        G  : [],
        H  : [],
        I  : [],
        J  : [],
        K  : [],
        L  : []
    },

    /**
     * @desc the list of objects of map lines
    **/
    line : {
        A    : [],
        B    : [],
        C    : [],
        D    : [],
        E    : [],
        F    : [],
        G    : [],
        H    : [],
        I    : [],
        J    : [],
        K    : [],
        L    : [],
        total: []
    },

    /**
     * @desc the list of available vehicles and vehicle details
    **/
    vehicles : {
        current  : 'railway',
        segments : [],
        auto     : {
            segments : [],
            color    : '#3dcd1c',
            icon     : '../images/map/truck-icon.png',
            marker   : {
                icon  : '../images/map/truck-marker.png',
                color : 'green'
            }
        },
        railway : {
            segments : [],
            color    : '#ff3333',
            icon     : '../images/map/train-icon.png',
            marker   : {
                icon  : '../images/map/train-marker.png',
                color : 'red'
            }
        },
        sea : {
            segments : [],
            color    : '#008CDC',
            icon     : '../images/map/ship-icon.png',
            marker   : {
                icon  : '../images/map/ship-marker.png',
                color : 'blue'
            }
        }
    },

    /**
     * @desc map styles
    **/
    styles : [
        {
            "elementType": "geometry",
            "stylers"    : [ { "color": "#ececec" } ]
        }, {
            "elementType": "labels.icon",
            "stylers"    : [ { "visibility": "on" } ]
        }, {
            "elementType": "labels.text.fill",
            "stylers"    : [ { "color": "#616161" } ]
        }, {
            "featureType": "administrative.land_parcel",
            "stylers"    : [ { "visibility": "on" } ]
        },  {
            "featureType": "administrative.neighborhood",
            "stylers"    : [ { "visibility": "on" } ]
        }, {
            "featureType": "poi",
            "elementType": "geometry",
            "stylers"    : [ { "color": "#eeeeee" } ]
        }, {
            "featureType": "poi",
            "elementType": "labels.text",
            "stylers"    : [ { "visibility": "on" } ]
        }, {
            "featureType": "poi.business",
            "stylers"    : [ { "visibility": "on" } ]
        }, {
            "featureType": "water",
            "stylers"    : [ { "visibility": "simplified" }, { "color": "#5AC8FA" } ]
        }, {
            "featureType": "administrative",
            "stylers"    : [ { "lightness": 45 } ]
        }, {
            "featureType": "landscape",
            "stylers"    : [ { "lightness": 90 } ]
        }, {
            "featureType": "administrative",
            "elementType": "geometry.fill",
            "stylers"    : [ { "visibility": "on" } ]
        }, {
            "featureType": "administrative",
            "elementType": "geometry.stroke",
            "stylers"    : [ { "gamma": "3" } ]
        }, {
            "featureType": "poi.park",
            "elementType": "all",
            "stylers"    : [ { "visibility": "on" } ]
        }, {
            "featureType": "road.highway",
            "elementType": "all",
            "stylers"    : [ { "visibility": "simplified" } ]
        }
    ],

    /**
     * @desc initialize script for current class
    **/
    init : function ()
    {

        //params initialization for current method
        var locations = JSON.parse( document.getElementById( 'map' ).getAttribute( 'data-details' ) ),
            latLng    = new google.maps.LatLng(
                locations.lat,
                locations.lng
            );

        //set map
        map = new google.maps.Map( document.getElementById( 'map' ), {
            zoom            : 3,
            minZoom         : 3,
            maxZoom         : 15,
            center          : latLng,
            disableDefaultUI: true,
            styles          : inter_rail_map.styles,
            backgroundColor : '#5AC8FA'
        } );

        //set markers
        if ( locations.type === 'marker' ) {

            //set marker
            inter_rail_map.set_markers(
                latLng,
                'marker'
            );

            //allow search option
            search = new google.maps.places.SearchBox(
                document.getElementById( 'location-name' )
            );

            // Listen for the event fired when the user selects a prediction and retrieve
            // more details for that place.
            search.addListener(
                'places_changed',
                inter_rail_map.search
            );

            //add click event
            google.maps.event.addListener( map, 'click', function( event ) {
                inter_rail_map.set_markers( event.latLng, 'marker' );
            } );

        } else {

            //show route on map
            this.draw_lines();

            //show route on map on click
            document.getElementById(
                'show-on-map'
            ).onclick = this.draw_lines;

        }

    },

    /**
     * @desc map search result options
    **/
    search: function ()
    {

        //param initialization
        var places = search.getPlaces();

        if ( places.length === 0 )
            return;

        // Clear out the old markers.
        if ( this.markers ) {

            this.markers.forEach( function( marker ) {
                marker.setMap( null );
            } );

        }

        //emptify markers
        this.markers = [];

        // For each place, get the icon, name and location.
        places.forEach( function( place ) {

            if ( !place.geometry )
                return;

            // Create a marker for each place.
            inter_rail_map.set_markers(
                place.geometry.location,
                'marker'
            );

        } );

    },

    /**
     * @desc drawing routes on map
    **/
    draw_lines: function()
    {

        //params initialization for current method
        var locations        = [],
            waypoints        = [],
            route_directions = [],
            segment          = 'A',
            origin           = document.getElementById( 'origin' ),
            destination      = document.getElementById( 'destination' ),
            depots           = document.getElementsByName( "place[]" ),
            places           = document.querySelector( ".places-container" )
                ? JSON.parse( document.querySelector( ".places-container" ).getAttribute( "data-details" ) )
                : [],

            routes = document.querySelector( '.logistic-container' )
                ? JSON.parse( document.querySelector( '.logistic-container' ).getAttribute( 'data-details' ) )
                : [],

            logistics = document.getElementsByName( 'Routes[logistics][]' );

        //set routes duration
        inter_rail_map.duration = document.querySelector( ".places-container" )
            ? parseFloat( document.getElementById( 'duration' ).value )
            : 0;

        //clear map with previous details
        inter_rail_map.clear();

        $.map( [ 'places-container',
            'logistic-container'
        ], function( type ) {

            if ( document.querySelector( '.' + type )
                && ( ( type === 'places-container' && depots.length > 0 && places.length > 0 )
                || ( logistics.length > 0 && type === 'logistic-container' ) )
            ) {

                $.map( type === 'places-container'
                    ? depots
                    : logistics, function( option
                ) {

                    if ( option && option.value
                        && ( ( type === 'places-container' && places.length > 0 )
                        || ( routes[ option.value ] &&  type === 'logistic-container' ) )
                    ) {

                        var place_locations = type === 'places-container'
                            ? places
                            : logistics;

                        if ( type === 'logistic-container' ) {
                            waypoints       = [];
                            place_locations = routes[ option.value ].places;
                        }

                        place_locations.map( place => {

                            if ( type === 'logistic-container' )
                                locations.push( place.id );
                            else if ( parseInt( place.id ) === parseInt( option.value ) )
                                locations.push( option.value );

                            if ( type === 'logistic-container'
                                || ( parseInt( place.id ) === parseInt( option.value ) )
                            ) {

                                waypoints.push( {
                                    "location" : {
                                        "lat" : parseFloat( place.lat ),
                                        "lng" : parseFloat( place.lng )
                                    },
                                    'city' : place.city,
                                    'name' : place.name,
                                    'code' : place.country_code
                                } );

                            }

                        } );

                        //change segments for routes pages
                        if ( type === 'logistic-container' ) {

                            //current vehicle type
                            inter_rail_map.vehicles.current = routes[ option.value ].direction;

                            //set duration
                            inter_rail_map.duration = routes[ option.value ].duration;

                            for ( var segments in inter_rail_map.line ) {

                                if ( segments !== 'total'
                                    && inter_rail_map.vehicles.segments.indexOf(segments) < 0
                                ) {

                                    //
                                    inter_rail_map.vehicles.segments.push( segments );

                                    //
                                    inter_rail_map.vehicles[ inter_rail_map.vehicles.current ].segments.push( segments );

                                    segment = segments;
                                    break;

                                }

                            }

                            //routing current vehicles
                            route_directions.push( inter_rail_map.route(
                                waypoints[ 0 ][ 'location' ],
                                waypoints[ waypoints.length - 1 ][ 'location' ],
                                waypoints,
                                segment
                            ) );

                            //set marker with transportation place icon
                            inter_rail_map.set_markers(
                                [
                                    waypoints[ 0 ][ 'location' ],
                                    waypoints[ waypoints.length - 1 ][ 'location' ]
                                ], 'direction', waypoints
                            );

                        }

                    }

                } );

                if ( type === 'places-container' ) {

                    //current vehicle type
                    inter_rail_map.vehicles.current = document.getElementById(
                        'logistic-type'
                    ).value;

                    inter_rail_map.vehicles[ inter_rail_map.vehicles.current ].segments.push( 'A' );

                    route_directions.push( inter_rail_map.route(
                        waypoints[ 0 ][ 'location' ],
                        waypoints[ waypoints.length - 1 ][ 'location' ],
                        waypoints,
                        inter_rail_map.vehicles[ inter_rail_map.vehicles.current ].segments[ 0 ]
                    ) );

                    //set marker with transportation place icon
                    inter_rail_map.set_markers( [
                            waypoints[ 0 ][ 'location' ],
                            waypoints[ waypoints.length - 1 ][ 'location' ]
                        ], 'direction', waypoints
                    );

                    if ( origin )
                        origin.value = waypoints[ 0 ][ 'city' ] !== ''
                            ? waypoints[ 0 ][ 'city' ]
                            : waypoints[ 0 ][ 'name' ];

                    if ( destination )
                        destination.value = waypoints[ waypoints.length - 1 ][ 'city' ] !== ''
                            ? waypoints[ waypoints.length - 1 ][ 'city' ]
                            : waypoints[ waypoints.length - 1 ][ 'name' ];

                }

            }

        } );

        Promise.all( route_directions ).then( function( response ) {

            if ( response[0] && response[0].code === 200 )
                inter_rail_map.result();

        }, function ( error ) {
            console.log( error.message );
        } );

    },

    /**
     * @desc fill in line, polyline and places params with requested details
     * @param origin mixed
     * @param destination mixed
     * @param waypoints mixed
     * @param segment string
    **/
    route : function( origin, destination, waypoints, segment )
    {

        return new Promise( function( resolve, reject ) {

            if ( inter_rail_map.vehicles.current === 'sea' ) {

                inter_rail_map.polyline[ segment ].push(
                    new google.maps.LatLng( origin ),
                    new google.maps.LatLng( destination )
                );

                inter_rail_map.options( {
                    distance : inter_rail_map.distances(
                        origin,
                        destination
                    ).toFixed(2) + ' km',
                    duration: parseInt( inter_rail_map.duration ) > 0
                        ? inter_rail_map.duration + ' day' + ( inter_rail_map.duration > 1 )
                            ? 's'
                            : ''
                        : inter_rail_map.duration,
                    segment : segment
                } );

                resolve( {
                    'code'   : 200,
                    'status' : 'success',
                    'message': 'All data received'
                } );

            } else {

                inter_rail_map.request(
                    new google.maps.DirectionsService, {
                        origin      : origin,
                        destination : destination,
                        waypoints   : waypoints.map( function ( waypoint ) {
                            return {
                                'location' : waypoint[ 'location' ]
                            }
                        } ),
                        travelMode: google.maps.TravelMode[ 'DRIVING' ]
                    },
                    segment, 'gmap'
                ).then( function( response ) {

                    if ( response.code === 200 ) {
                        resolve( response );
                    } else {

                        inter_rail_map.request( {
                            locations : {
                                'origin'     : origin.lat + ',' + origin.lng,
                                'destination': destination.lat + ',' + destination.lng
                            }
                        }, [ origin, destination ], segment, 'api' ).then( function( response ) {
                            resolve( response );
                        } );

                    }

                } );

            }

        } );

    },

    /**
     * @desc google maps directions service request
     * @param directionsService mixed
     * @param directionDetails mixed
     * @param segment string
     * @param type string
     * @return mixed
    **/
    request: function( directionsService, directionDetails, segment, type )
    {

        return new Promise( function( resolve, reject ) {

            if ( type === 'api' ) {

                inter_rail_map.polyline[ segment ].push(
                    locations[ 0],
                    locations[ 1 ]
                );

                inter_rail_map.options( {
                    distance : inter_rail_map.distances( locations[ 0], locations[ 1 ] ),
                    duration : inter_rail_map.duration,
                    segment  : segment
                } );

                resolve( {
                    'code'   : 200,
                    'status' : 'success',
                    'message': 'All data received'
                } );

            } else {

                directionsService.route( directionDetails, function ( response, status ) {

                    if ( status === google.maps.DirectionsStatus.OK ) {

                        $.each( response.routes[0].overview_path, function( i, item ) {
                            inter_rail_map.polyline[ segment ].push( item );
                        } );

                        inter_rail_map.options( {
                            distance: response.routes[ 0 ].legs.length > 1
                                ? response.routes[ 0 ].legs[ 1 ].distance.text
                                : response.routes[ 0 ].legs[ 0 ].distance.text,
                            duration: parseInt( inter_rail_map.duration ) > 0
                                ? inter_rail_map.duration + ' day' + ( inter_rail_map.duration > 1
                                    ? 's'
                                    : ''
                                )
                                : response.routes[ 0 ].legs.length > 1
                                    ? response.routes[ 0 ].legs[ 1 ].duration.text
                                    : response.routes[ 0 ].legs[ 0 ].duration.text,
                            segment: segment
                        } );

                        resolve( {
                            'code'   : 200,
                            'status' : 'success',
                            'message': 'Places updated'
                        } )

                    } else {

                        resolve( {
                            'code'   : 400,
                            'status' : 'error',
                            'message': 'Bad Request'
                        } );

                    }

                } );

            }

        } );

    },

    /**
     * @desc set markers on map due to requested latitudes and longitudes
     * @param location mixed
     * @param type string
     * @param waypoints mixed
    **/
    set_markers : function( location, type, waypoints )
    {

        //put place marker in case of type map
        if ( type === 'marker' ) {

            if ( typeof marker === 'undefined' ) {

                marker = new google.maps.Marker( {
                    position : location,
                    map	     : map,
                    animation: google.maps.Animation.DROP
                } );

            } else {
                marker.setPosition( location );
            }

            marker.setIcon( {
                url       : '../images/map/map-icon.png',
                size      : new google.maps.Size(50, 65),
                scaledSize: new google.maps.Size(50, 65),
                origin    : new google.maps.Point(0, 0),
                anchor    : new google.maps.Point(30, 30)
            } );

            //fill in fields with values
            $.map( {
                'lat-field' : location.lat(),
                'lng-field' : location.lng(),
                'location'  : JSON.stringify( location )
            }, function( option, key ) {

                if ( document.getElementById( key ) )
                    document.getElementById( key ).value = option;

            } );

        } else {

            //param initialization
            var infoWindow = new google.maps.InfoWindow();

            $.each( waypoints, function ( key, waypoint ) {

                if ( typeof waypoint !== 'undefined' ) {

                    //set markers
                    inter_rail_map.marker_label(
                        waypoint.location,
                        waypoint.city !== ''
                            ? waypoint.city
                            : waypoint.name,
                        waypoint.code,
                        infoWindow
                    );

                } else {

                    //set markers
                    inter_rail_map.locations( new google.maps.LatLng( waypoint.location ) ).then( function( response ) {

                        if ( response.city ) {

                            inter_rail_map.marker_label(
                                waypoint.location,
                                response.city,
                                response.country_code,
                                infoWindow
                            );

                        }

                    }, function ( error ) {
                        console.log( error.message );
                    } );

                }

            } );

        }

    },

    /**
     * @desc label marker styles
     * @param location object
     * @param city string
     * @param country_code string
     * @param info mixed
    **/
    marker_label: function ( location, city, country_code, info )
    {

        //params initializations
        var vehicle = inter_rail_map.vehicles[ inter_rail_map.vehicles.current ].marker,
            marker  = new MarkerWithLabel( {
                map      : map,
                animation: google.maps.Animation.DROP,
                position : location,
                icon     : {
                    url       : vehicle.icon,
                    size      : new google.maps.Size(40, 40),
                    scaledSize: new google.maps.Size(40, 40),
                    origin    : new google.maps.Point(0, 0),
                    anchor    : new google.maps.Point(20, 20)
                },
                labelContent     : city,
                labelAnchor      : new google.maps.Point(40, -15),
                labelClass       : 'map-label ' + vehicle.color ,
                labelInBackground: true
            } );

        //set marker content
        google.maps.event.addListener( marker, 'click', function() {
            info.setContent( '<div style="line-height:1.35;overflow:hidden;white-space:nowrap;">' +
                '<div><b>'+ city +'</b></div>' +
                'Country: <span class="flag-icon-small flag-icon-small-' + country_code.toLowerCase() + '"></span>' + country_code +'' +
                '<div>Lat: ' + location[ 'lat' ]  + '</div>' +
                '<div>Lng: ' + location[ 'lng' ]  + '</div>' +
            '</div>' );
            info.open( map, marker );
        } );

        //set marker
        this.markers.push( marker );

    },

    /**
     * @desc draw lines and add animation due to places details
     * i.e places is filled from
    **/
    result : function ()
    {

        if( inter_rail_map.places ) {

            //params initialization for current method
            var infoWindow = new google.maps.InfoWindow();

            $.each( inter_rail_map.places, function ( index, opt ) {

                inter_rail_map.line[ opt.segment ] = new google.maps.Polyline( {
                    path         : inter_rail_map.polyline[ opt.segment ],
                    strokeColor  : inter_rail_map.current_segment( opt.segment ).color,
                    strokeWeight : 3,
                    clickable    : true,
                    strokeOpacity: 1,
                    map          : map
                } );

                google.maps.event.addListener( inter_rail_map.line[ opt.segment ], 'click', function( line ) {

                    infoWindow.setContent( '<div class="map-line-info">' +
                        '<img src="../images/logo.png">' +
                        '<div class="distance">' +
                            '<i class="fa fa-map-marker"></i> Distance: <span>' + opt.distance + '</span>' +
                        '</div>' +
                        '<div class="transit">' +
                            '<i class="fa fa-clock-o"></i> Transit Time: <span>' + opt.duration + '</span>' +
                        '</div>' +
                    '</div>' );

                    infoWindow.setPosition( line
                        ? line.latLng
                        : line
                    );

                    infoWindow.open( map );

                } );

                google.maps.event.addListener( inter_rail_map.line[ opt.segment ], 'mouseover', function() {
                    inter_rail_map.line[ opt.segment ].setOptions( {
                        'strokeWeight': 6
                    } );
                } );

                google.maps.event.addListener( inter_rail_map.line[ opt.segment ], 'mouseout', function() {
                    inter_rail_map.line[ opt.segment ].setOptions( {
                        'strokeWeight': 3
                    } );
                } );

            } );

            google.maps.event.addListener( map, "click", function () {
                infoWindow.close();
            } );

            // check array and create array with Polyline obj
            $.map( inter_rail_map.polyline, function( segment, key ) {

                if( segment.length !== 0 ) {

                    if ( google.maps.geometry ) {

                        inter_rail_map.line.total.push( new google.maps.Polyline( {
                            path        : segment,
                            distance    : ( google.maps.geometry.spherical.computeLength(
                                segment, 0
                            ) ) / 1000,
                            key         : key,
                            dashed      : false,
                            strokeWeight: 0,
                            map         : map
                        } ) );

                    }

                }

            } );

            //main marker animation method
            inter_rail_map.animations();

        }

    },

    /**
     * @desc adding animation to marker
    **/
    animations: function()
    {

        //params initializations
        var offset  = 0,
            count   = 0;

        //set marker animation
        marker_animation = new google.maps.Marker( {
            map: map
        } );

        //set animations interval
        offsetId = window.setInterval( function() {

            //set count due to line length
            count = count > inter_rail_map.line.total.length - 1
                ? 0
                : count;

            if( inter_rail_map.line.total[count] !== undefined ) {

                //set marker animation icon
                marker_animation.setIcon( {
                    url       : inter_rail_map.current_segment( inter_rail_map.line.total[count].key ).icon,
                    size      : new google.maps.Size(32, 40),
                    scaledSize: new google.maps.Size(32, 40),
                    origin    : new google.maps.Point(0, 0),
                    anchor    : new google.maps.Point(16, 40)
                } );

                //set new position
                marker_animation.setPosition( inter_rail_map.polyline_distance(
                    inter_rail_map.line.total[count],
                    offset
                ) );

                //set offset
                if ( inter_rail_map.line.total[ count ].distance < 300 && inter_rail_map.line.total.length > 1 )
                    offset += 10;
                else if ( inter_rail_map.line.total[ count ].distance < 1000 && inter_rail_map.line.total.length > 1 )
                    offset += 6;
                else if ( inter_rail_map.line.total[ count ].distance < 2000 && inter_rail_map.line.total.length > 1 )
                    offset += 4;
                else if ( inter_rail_map.line.total[ count ].distance < 5000 && inter_rail_map.line.total.length > 1 )
                    offset += 1;
                else
                    offset += 0.5;

                //reset to zero
                if ( offset >= 100 ) {
                    offset = 0;
                    count++;
                }

            }

        }, 100 );

    },

    /**
     * @desc calculate approximately distance between 2 points
     * @param origin object
     * @param destination object
     * @return number
    **/
    distances: function( origin, destination )
    {

        //params initializations
        var radius       = 6378137,
            latitude     = ( destination.lat - origin.lat ) * Math.PI/ 180,
            longitude    = ( destination.lng - origin.lng ) * Math.PI/ 180,
            lat_lng_calc = ( Math.sin( latitude / 2 ) * Math.sin( latitude / 2 ) ) + Math.cos( ( origin.lat ) * Math.PI/ 180 ) * Math.cos( ( destination.lat ) * Math.PI/ 180 ) * Math.sin( longitude / 2 ) * Math.sin( longitude / 2 ),
            distance        = 2 * Math.atan2(
                    Math.sqrt( lat_lng_calc ),
                    Math.sqrt( 1 - lat_lng_calc )
                );

        return radius * distance;

    },

    /**
     * @desc convert timestamp to date
     * @param timestapm number
     * @param time_unit string
     * @return string
    **/
    timestamp_to_date : function ( timestapm, time_unit )
    {

        //params initializations
        var result = '',
            days  = timestapm / ( time_unit === 's' ? 86400 : 86400000 ),
            hours = Math.round( ( days - Math.floor( days ) ) * 24 );

        //set days
        days = days - ( days - Math.floor( days ) );

        //set label for days
        if( days !== 0 )
            result += days + ( days === 1
                ? ' day '
                : ' days '
            );

        //set hours
        result += hours + ( hours === 1
              ? ' hour'
              : ' hours'
            );

        return result;

    },

    /**
     * @desc get distance due to requested poly line
     * @param polyline mixed
     * @param offset integer
     * @return object|mixed
    **/
    polyline_distance: function( polyline, offset )
    {

        //params initializations
        var dist           = 0,
            distanse       = google.maps.geometry.spherical.computeLength( polyline.getPath(), 0 ),
            offsetDistanse = distanse * offset / 100.00,
            points         = polyline.getPath().getArray();

        if( points.length > 0 ) {

            for( var i = 0; i < points.length - 1; i++ ) {

                var pointStart  = new google.maps.LatLng( points[i].lat(), points[i].lng() ),
                    pointEnd    = new google.maps.LatLng( points[i + 1].lat(), points[i + 1].lng() ),
                    curDistance = google.maps.geometry.spherical.computeDistanceBetween( pointStart, pointEnd ),
                    prevDist    = dist;

                dist += curDistance;

                if ( Math.abs( pointEnd.lng() - pointStart.lng() ) > 180 )
                    i++;

                if( offsetDistanse <= dist ) {

                    var diff = offsetDistanse - prevDist;
                    diff = diff / curDistance;
                    var d = diff / (1.0 - diff);

                    return new google.maps.LatLng(
                        ( points[i].lat() + ( d * points[i + 1].lat() ) ) / ( 1.0 + d ),
                        ( points[i].lng() + ( d * points[i + 1].lng() ) ) / ( 1.0 + d )
                    );

                }

            }

        }

    },

    /**
     * @desc clear all details from map
     * i.e like markers, closing all info window and etc ...
    **/
    clear: function()
    {

        //hide infowindows
        google.maps.event.trigger( map, 'click' );

        if ( inter_rail_map.line.total.length !== 0 ) {

            //disable animation
            clearInterval( offsetId );

            //disable markers
            if( marker_animation )
                marker_animation.setMap( null );

            //hide lines
            $.each( inter_rail_map.line.total, function ( index, line ) {

                if( line )
                    line.setMap( null );

            } );

        }

        //pop vehicle segments
        $.map( inter_rail_map.vehicles, function( vehicle, index ) {

            if ( typeof vehicle.segments !== 'undefined' )
                vehicle.segments.pop();

        } );

        inter_rail_map.vehicles.segments.pop();

        //remove all info about lines
        $.map( inter_rail_map.line, function( segment, key ) {

            if ( key !== 'total' && segment.length !== 0 ) {

                segment.setMap(
                    null
                );

                inter_rail_map.polyline[ key ] = [];

            }

            inter_rail_map.line[ key ] = [];

        } );

        //empty places details
        inter_rail_map.places.pop();

        //clear markers on map
        $.map( this.markers, function( marker, key ) {
            marker.setMap( null );
        } );

        this.markers.pop();

    },

    /**
     * @desc pushing details to places param
     * @param option mixed
    **/
    options: function( option )
    {

        inter_rail_map.places.push( option );
        inter_rail_map.places.sort( function( a, b ) {

            //params initializations
            var response  = 0,
                segment_a = a.segment.toLowerCase(),
                segment_b = b.segment.toLowerCase();

            //set response due to segment
            if ( segment_a < segment_b )
                response = -1;
            else if ( segment_a > segment_b )
                response = 1;

            return response;

        } );

    },

    /**
     * @desc current vehicle due to requested segment
     * @param segment string
     * @return object
    **/
    current_segment: function( segment )
    {

        //param initialization
        var vehicle = inter_rail_map.vehicles.auto;

        //vehicle options due to segment
        $.map( inter_rail_map.vehicles, function( value, index ) {

            if ( value.segments && value.segments.indexOf( segment ) >= 0 ) {

                vehicle = inter_rail_map.vehicles[ index ];

                return false;

            }

        } );

        return vehicle;

    },

    /**
     * @desc location details due to requested latitude and longitude
     * @param latLng object|mixed
     * @return object
    **/
    locations: function( latLng )
    {

        return new Promise( function( resolve, reject ) {

            new google.maps.Geocoder().geocode( {
                'latLng' : latLng
            }, function( results, status ) {

                if ( status === google.maps.GeocoderStatus.OK ) {

                    if ( results[1] ) {

                        //params initializations
                        var country     = null,
                            countryCode = null,
                            city        = null;

                        //set city, country name and code
                        $.map( results, function( result, key ) {

                            //set city
                            if ( !city && result.types[0] === 'locality' ) {

                                $.map( result.address_components, function( component, index ) {

                                    if ( component.types[0] === 'locality' ) {

                                        city = component.long_name;

                                        return false;

                                    }

                                } );

                            } else if (!country && result.types[0] === 'country') {
                                country     = result.address_components[0].long_name;
                                countryCode = result.address_components[0].short_name;
                            }

                            //is params empty
                            if ( city && country )
                                return false;

                        } );

                        resolve( {
                            'country'     : country,
                            'country_code': countryCode,
                            'city'        : city
                        } );

                    } else {

                        reject( {
                            'code'   : 400,
                            'status' : 'error',
                            'message': 'Bad Request'
                        } );

                    }

                }

            } );

        } );

    }

};

/**
 * @desc calling current class init ( construct ) method
**/
inter_rail_map.init();