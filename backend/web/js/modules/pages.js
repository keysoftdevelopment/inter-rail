/**
 * Name:        Pages Scripts
 * Description: Scripts used in pages
 * Version:     1.0.0
**/

$( function () {

    /**
     * @desc this class will hold functions for backend side for current service
     * examples include sidebar menu actions, modal windows for images uploading and etc ...
    **/
    var interrail_pages = {

        /**
         * @desc initialize script for all pages
        **/
        init: function ()
        {

            //actions lists and appropriate methods
            $( document )
                .on( 'click', 'a.add-item', this.add_item )
                .on( 'click', '.remove-current-option', this.remove_item )
                .on( 'submit', 'form.need-seo', this.seo )
                .on( 'click', '.icons-lists .dropdown-menu li', this.icon_options )
                .on( 'click', '.stats-icons-lists .dropdown-menu li', this.icon_options )
                .on( 'click', '#add-place', this.places_details )
                .on( 'click', '#add-filter-params', this.add_item )
                .on( 'click', '#add-custom-rate', this.add_item );

            // action with file uploading through modal window
            $.map( [
                'categories',
                'sliders',
                'services',
                'companies',
                'pages',
                'posts',
                'user'
            ], function( type, index ) {
                backend_interrail.drop_zone( type );
            } );

            //actions with theme templates, i.e according to template appropriate view will be showed on frontend side
            $( '.templates-sidebar' ).find( '#select-template select' ).on(
                'change',
                this.theme_templates
            );

            //actions with theme templates
            this.theme_templates();

        },

        /**
         * @desc home page template icon options
        **/
        icon_options: function()
        {

            //params initializations
            var option = JSON.parse( $( this ).attr( 'data-option' ) );

            //set icon class
            $( '.selected-icon', $( this ).parents( '.form-group' ) ).html(
                '<i class="fa ' + option.value + '"></i>'
            );

            //set icon
            $( this ).parents( '.form-group' ).hasClass( 'stats-icons-lists' )
                ? $( '.statistic-icon', $( this ).parents( '.statistic' ) ).val( option.key )
                : $( '#icon' ).val( option.key );

        },

        /**
         * @desc fill in container with places details
        **/
        places_details: function ()
        {

            //params initializations
            var container = $( 'div.places-container' ),
                template  = $( $( '#places-template' ).html() ),
                field     = template.find( 'select' );

            //set options
            if ( container.attr( 'data-details' ) ) {

                backend_interrail.select_options( {
                    'select'  : field,
                    'options' : JSON.parse( container.attr( 'data-details' ) )
                } );

            }

            //append place template to requested container
            container.append(
                template
            );

            ///activate select 2 option
            field.select2( {
                placeholder     : field.data( 'msg' ).option,
                allowClear      : true,
                formatNoMatches : function () {
                    return field.data( 'msg' ).no_result;
                }
            } );

        },

        /**
         * @desc add new item
        **/
        add_item: function ()
        {

            //fill container with appropriate template
            $( '.' + $( this ).data( 'type' ) + '-container' ).append(
                $( '#' + $( this ).data( 'type' ) + '-template' ).html()
            );

            $( '.' + $( this ).data( 'type' ) + '-container .option-section, .' + $( this ).data( 'type' ) + '-container .rate-filter' ).each( function( index, value ) {


                if ( $( value ).find( '.transportation-field' ) ) {
                    $( value ).find( '.transportation-field' ).attr( 'name', 'filters[transportation][' + index + ']' );
                }

                //params initializations
                var count          = index,
                    select_tags    = $( value ).find( '.select2-tags' ),
                    invoice_number = $( value ).find( '.invoice-number-field' );

                //set index
                $( value ).find( '.option-index' ).html( count + 1 );

                //set invoice number
                if ( typeof invoice_number.val() !== 'undefined' && invoice_number.val() === '' ) {
                    invoice_number.val( 'U000' + ( parseInt( invoice_number.data( 'count' ) ) + count ) );
                }

                //activate select 2 tags option
                if ( typeof select_tags !== 'undefined' ) {

                    select_tags.select2( {
                        allowClear  : true,
                        tags        : true,
                        placeholder : select_tags.data( 'msg' )
                    } );

                }


            } );

            //notify user with message that action was completed
            if ( $( this ).data( 'translations' ) ) {

                backend_interrail.notify_message(
                    $( this ).data( 'translations' ),
                    'notice'
                );

            }

        },

        /**
         * @desc remove requested item
        **/
        remove_item: function()
        {

            //param initialization
            var current = $( this );

            //set flash message
            backend_interrail.notify_message(
                $( this ).data( 'translations' ),
                'confirm'
            ).get().on( 'pnotify.confirm', function() {

                if ( current ) {

                    current
                        .parents( '.' + current.data( 'parent' ) )
                        .remove();

                    //run calculations
                    backend_interrail.calculate_expenses();

                }

            } );

        },

        /**
         * @desc fill in model seo hidden field
         * with filled in seo details
        **/
        seo: function ()
        {

            //add seo details before saving
            $( '#seo-model' ).val( JSON.stringify( {
                'title'       : $( '#seo-title' ).val(),
                'description' : $( '#seo-description' ).val(),
                'tags'        : $( '#seo-tags' ).val()
            } ) );

        },

        /**
         * @desc actions with interrail templates
        **/
        theme_templates: function ()
        {

            // initialization params for current method
            var template           = $( '#theme-template' ),
                current_template   = template.val(),
                company_section    = $( '#company-section' ),
                statistics_section = $( '#statistics-section' ),
                contact_section    = $( '#contact-section' );

            //set current template param
            if ( $( this ).hasClass( 'template-lists' ) ) {

                current_template = $( this ).val();

                //apply chosen template to current model
                template.val(
                    $( this ).val()
                );

            }

            //hide all sections
            company_section.slideUp();
            statistics_section.slideUp();
            contact_section.slideUp();

            //show sections due to selected template
            if ( current_template === 'home' ) {
                company_section.slideDown();
                statistics_section.slideDown();
            } else if ( current_template === 'contact' ) {
                contact_section.slideDown();
            }

        }

    };

    /**
     * @desc calling current class init ( construct ) method
     **/
    interrail_pages.init();

} );