/**
 * Name:        Menus Scripts
 * Description: Scripts used in menus pages
 * Version:     1.0.0
**/

$( function () {

    /**
     * @desc params initializations
    **/
    var body         = $( 'body' ),
        sidebar_menu = $( '#sidebar-menu' );

    /**
     * @desc this class will hold functions for backend menu side for current service
     * examples include sidebar menu actions, modal windows and etc...
    **/
    var interrail_menus = {

        /**
         * @desc script initializations
        **/
        init: function ()
        {

            // initialization params for current method
            var menu         = $( '.menu-items' ),
                menu_details = $( '#domenu-1-output' ).find( '.jsonOutput' );

            //click actions lists and appropriate methods
            $( document )
                .on( 'click', '#menu_toggle', this.menu_toggle )
                .on( 'click', '.add-to-menu-action .add-new-menu-item', this.add_menu_item )
                .on( 'click', '.collapse-link', this.panel_collapse )
                .on( 'click', '.close-link', this.panel_close )
                .on( 'click', '.expand', this.expand );

            //sidebar menu actions
            sidebar_menu.find( 'a' ).on(
                'click', this.side_bar_menu
            );

            if ( typeof menu[0] !== 'undefined' ) {

                //param initialization
                var menus = menu.domenu();

                //activate menu additional options
                menu.domenu( {
                    slideAnimationDuration: 0,
                    itemClass: 'dd-item',
                    select2: {
                        support: true,
                        params: {
                            tags: true
                        }
                    }, data: '[]'
                } )
                .onCreateItem( function ( blueprint ) {

                    $( blueprint ).find( '.custom-button-example' ).click( function () {
                        blueprint.find( '.dd3-content span' ).first().click();
                    } );

                } )
                .parseJson( menu_details.val() ).on( [
                    'onItemCollapsed',
                    'onItemExpanded',
                    'onItemAdded',
                    'onSaveEditBoxInput',
                    'onItemDrop',
                    'onItemDrag',
                    'onItemRemoved',
                    'onItemEndEdit',
                    'onParseJson'
                ], function ( a, b, c ) {
                    menu_details.val( menus.toJson() );
                } );

                //set menus
                menu_details.val( menus.toJson() );

            }

            //check active menu
            sidebar_menu.find( 'a[href="' + URL + '"]' ).parent( 'li' ).addClass( 'current-page' );

            //sidebar changing urls and etc...
            sidebar_menu.find( 'a' ).filter( function () {
                return this.href === window.location;
            } ).parent( 'li' ).addClass( 'current-page' ).parents( 'ul' ).slideDown( function () {
                this.set_height();
            } ).parent().addClass( 'active' );

        },

        /**
         * @desc new menu item
        **/
        add_menu_item: function ()
        {

            //initialization params for current method
            var menu         = $( '.menu-items' ).domenu(),
                title        = $( '#customTitle' ),
                link         = $( '#customLink' ),
                menu_details = {};

            $( '.menu-items .list-view div input:checked' ).each( function () {

                //params initializations
                var menu_details = $( this ).data(),
                    argument     = menu_details.link.split( '/' );

                //disable checked
                $( this ).attr(
                    'checked', false
                );

                //set link
                menu_details.link = argument.join( '/' ) + "/" + slug( argument.pop(), {
                    replacement: '_', lower: true
                } );

                //set menu details
                menu.parseJson( JSON.stringify(
                    [ menu_details ] )
                );

            } );

            if ( title.val() && link.val() ) {

                //set link and name
                menu_details.link = link.val();
                menu_details.name = title.val();

                //set menu details
                menu.parseJson( JSON.stringify(
                    [ menu_details ]
                ) );

                //emptify title and link fields
                title.val( "" );
                link.val( "" );

            }

        },

        /**
         * @desc actions with active/not active menu items
        **/
        side_bar_menu: function( ev )
        {

            //param initialization
            var menu = $( this ).parent();

            if ( menu.is( '.active' ) ) {

                //remove active class
                menu.removeClass( 'active' );

                //hide menu details
                $( 'ul:first', menu ).slideUp( function () {
                    interrail_menus.set_height();
                } );

            } else {

                // prevent closing menu if we are on child menu
                if ( ! menu.parent().is( '.child_menu' ) ) {
                    sidebar_menu.find( 'li' ).removeClass( 'active' );
                    sidebar_menu.find( 'li ul' ).slideUp();
                }

                //set active class
                menu.addClass( 'active' );

                //show menu details
                $( 'ul:first', menu ).slideDown( function () {
                    interrail_menus.set_height();
                } );

            }

        },

        /**
         * @desc sidebar menu toggle action
        **/
        menu_toggle: function ()
        {

            if ( body.hasClass( 'nav-md' ) ) {

                //allow drag and drop
                body.removeClass( 'nav-md' ).addClass( 'nav-sm' );

                //disable scroll
                $( '.left_col' ).removeClass( 'scroll-view' ).removeAttr( 'style' );

                //remove active class
                if ( sidebar_menu.find( 'li' ).hasClass( 'active' ) ) {

                    sidebar_menu.find( 'li.active' )
                        .addClass( 'active-sm' )
                        .removeClass( 'active' );

                }

            } else {

                //disable drag and drop
                body.removeClass( 'nav-sm' ).addClass( 'nav-md' );

                //set active class
                if ( sidebar_menu.find( 'li' ).hasClass( 'active-sm' ) ) {

                    sidebar_menu.find( 'li.active-sm' )
                        .addClass( 'active' )
                        .removeClass( 'active-sm' );

                }

            }

            //set sidebar height
            this.set_height();

        },

        /**
         * @desc main collapse action
        **/
        panel_collapse: function ()
        {

            //params initializations
            var panel   = $( this ).closest( '.x_panel' ),
                content = panel.find( '.x_content' );

            // fix for some div with hardcoded fix class
            if ( panel.attr( 'style' ) ) {

                content.slideToggle( 200, function () {
                    panel.removeAttr( 'style' );
                } );

            } else {

                content.slideToggle( 200 );
                panel.css( 'height', 'auto' );

            }

            //change icon type
            $( this ).find( 'i' ).toggleClass(
                'fa-chevron-up fa-chevron-down'
            );

        },

        /**
         * @desc panel collapse action
        **/
        panel_close: function ()
        {

            $( this )
                .closest( '.x_panel' )
                .remove();

        },

        /**
         * @desc collapse expand action
        **/
        expand: function ()
        {

            $( this ).next().slideToggle( 200 );

            //param initialization
            var expand = $( this ).find( ">:first-child" );

            //set param
            if ( expand.text() === "+" )
                expand.text( "-" );
            else
                expand.text( "+" );

        },

        /**
         * @desc set height for right sidebar
        **/
        set_height: function ()
        {

            //params initializations
            var height         = body.height(),
                left_height    = $( '.left_col' ).eq( 1 ).height() + $( '.sidebar-footer' ).height(),
                content_height = height < left_height ? left_height : height,
                right_column   = $( '.right_col' );

            // reset height
            right_column.css(
                'min-height', $( window ).height()
            );

            // normalize content
            content_height -= $( '.nav_menu' ).height() + $( 'footer' ).height();

            //set height
            right_column.css(
                'min-height', content_height
            );

        }

    };

    /**
     * @desc calling current class init ( construct ) method
    **/
    interrail_menus.init();

} );