/**
 * Name:        Scripts For BACKEND
 * Description: INTERRAIL Scripts
 * Version:     1.0.0
**/

/**
 * @desc params initialization for admin panel
**/
var thumbnails, image, image_src,
    body   = $( 'body' ),
    footer = $( 'footer' ),
    event  = new Event( 'input', {
        bubbles   : true,
        cancelable: false
    } );

/**
 * @desc this class will hold functions for backend side for current service
 * examples include sidebar menu actions, modal windows for images uploading and etc ...
**/
var backend_interrail = {

    /**
     * @desc initialize script for all pages
    **/
    init: function ()
    {

        //actions lists and appropriate methods
        $( document )
            .on( 'click', 'table tr.show-details', this.table_expand )
            .on( 'click', '.avatar-view', function () {
                $( '#avatar-modal' ).modal( 'toggle' );
            } )
            .on( 'click', '[data-thumbnail-index]', this.current_thumbnail )
            .on( 'change', '.slider-sidebar select.slider-lists', function () {
                $( '#slider-id' ).val( $( this ).val() );
            } )
            .on( 'change', '.need-calculation .price-field, .need-calculation .surcharge-field', this.calculation )
            .on( 'submit', '#logistics-form, #routes-form', function() {

                //trigger click action to show route
                $( '#show-on-map' ).click();

                return true;

            } )
            .on( 'click', '.image-select-container', this.select_thumbnail )
            .on( 'click', '.image-save', this.save_image );

        //select multiple files option
        if ( $( 'select[multiple="multiple"]' ).attr( 'value' ) ) {

            $( 'select[multiple="multiple"] option' ).each( function( value, index ) {

                if ( $( 'select[multiple="multiple"]' ).attr( 'value' ).includes( $( this ).val() ) ) {

                    $( this ).attr(
                        'selected',
                        'selected'
                    );

                }

            } );

        }

        //change table view for mobile devices
        this.responsive_table();

        //delays loading of images in long web pages
        $( '.lazyload' ).lazyload();

        // tooltip
        $( '[data-toggle="tooltip"]' ).tooltip();

        //activate select 2 lib
        this.select_2( '.form-control.select2_single' );

        //import xlsx files
        this.import_options();

        //select 2 tags option
        $( '.form-control.select2-tags' ).select2( {
            tags        : true,
            placeholder :  $( '.form-control.select2-tags' ).data( 'msg' )
        } );

        // NProgress
        if ( typeof NProgress !== 'undefined' ) {

            $( document ).ready( function () {
                NProgress.start();
            } );

            $( window ).load( function () {
                NProgress.done();
            } );

        }

        //is switchery exists
        if ( $( '.switchery' )[0] ) {

            //param initialization
            var checkboxes = Array.prototype.slice.call(
                document.querySelectorAll( '.switchery' )
            );

            //activate switchery option
            checkboxes.forEach( function ( value ) {

                $( value ).find( '.switchery' ).addClass( 'activated' );

                new Switchery( value, {
                    color: '#00a2b3'
                } );

            } );

        }

        //disable pressing `ENTER` key
        $( '.disable-form-update' ).keypress( function( e ) {

            if ( e.which === 13 )
                return false;

        } );

    },

    /**
     * @desc total and surcharge fields calculations
    **/
    calculation: function ()
    {

        if ( $( this ).hasClass( 'surcharge-field' ) ) {

            //param initialization
            var commission_field = $(
                'input[name="Orders[commission]"]' , $( this ).parents( '.payment-details' )
            );

            //set new rate
            commission_field.attr( 'value',
                parseFloat( parseFloat( commission_field.val() ) + parseFloat( $( this ).attr( 'data-price' ) ) ) - parseFloat( $( this ).val() )
            );

            //set old rate
            $( this ).attr( 'data-price', $( this ).val() );

        } else {

            $( '.need-calculation .rate-details-container' ).each( function( index, value ) {

                //param initialization
                var total = 0;

                $( '.price-field', this ).each( function( index, value ) {

                    if ( ! isNaN ( parseInt( $( this ).val() ) ) ) {

                        total = parseFloat( total ) + ( [ 'orders-commission' ].indexOf( $( this ).attr( 'id' ) ) > -1
                            ? parseFloat( $( this ).val() ) * parseFloat( $( '#packages-quantity' ).val() )
                            : parseFloat( $( this ).val() )
                        );

                    }

                } );

                //set rate
                $( 'input.total-sum', this ).val( total );

            } );

        }

    },

    /**
     * @desc expenses details calculation
     * @return integer
    **/
    calculate_expenses: function ()
    {

        //param initialization
        var total    = 0,
            expenses = $( '.payment-details input[name="expenses"]' );

        //expenses calculations
        $( '.option-section .total-expenses' ).each( function() {

            if ( parseFloat( $( this ).val() ) > 0 ) {
                total += parseFloat( $( this ).val() );
            }

        } );

        //append total value to expenses field
        if ( total > 0 ) {
            expenses.val( total.toFixed(2) );
        }

        expenses.trigger( 'change' );

    },

    /**
     * @desc popup window select thumbnail
    **/
    select_thumbnail: function()
    {

        //param initializations
        image     = $( this ).data( 'id' );
        image_src = $( this ).data( 'src' );

        $( '.img-chosen' ).removeClass( 'active' );

        $( this ).find( '.img-chosen' ).addClass( 'active' );

    },

    /**
     * @desc append thumbnail details to custom container
    **/
    save_image: function()
    {

        //param initialization
        var thumbnail = $( '.thumbnail-view' );

        if ( image_src ) {

            if ( thumbnails ) {

                thumbnails.parent().parent().find( 'img' ).attr( 'src', image_src );
                thumbnails.parent().parent().find( '.file-id' ).val( image );
                image_src = '';
                image     = '';

            } else {

                $( '#file_id' ).val( image );
                thumbnail.attr( 'src', image_src );
                thumbnail.show();

            }

        }

    },

    /**
     * @desc allows import data
     * to requested controller
    **/
    import_options: function()
    {

        // initialization params for current method
        var element   = $( '<div></div>' ),
            xlsx_file = $( '<input type="file">' );

        element.append( xlsx_file );

        // actions with xlsx file after uploading it
        xlsx_file.on( 'change', function( evt ) {

            var file = evt.currentTarget.files[ 0 ];

            if ( file.name.indexOf('.xlsx') >= 0 || file.name.indexOf('.xls') >= 0 ) {

                //param initializations
                var details  = $( '.import-xlsx-file' ),
                    formData = new FormData();

                formData.append(
                    'xlsx_file',
                    file,
                    file.name
                );

                //adding spinner option in files importing
                if ( ! details.data( 'type' ) ) {

                    details.find( 'i' )
                        .removeClass( 'fa-file-text' )
                        .addClass( 'fa-spin fa-refresh' );

                }

                //uploading import file to requested table
                $.ajax( $('body').data('url') + details.data( 'details' ).url, {
                    method     : "POST",
                    data       : formData,
                    processData: false,
                    contentType: false,
                    success     : function( file ) {
                        //window.location.reload();
                    },
                    error: function () {
                        //window.location.reload();
                    }
                } );

            }

        } );

        // calling action after uploading file
        $( document ).on( 'click', '.import-xlsx-file', function() {
            xlsx_file.click();
        } );

    },

    /**
     * @desc add options to requested select
     * with appropriate details
     * @param params
    **/
    select_options: function ( params )
    {

        //emptify select
        params.select.empty();

        //fill select with requested options
        $.each( params.options, function( key, value ) {

            params.select.append(
                new Option(
                    value[ 'name' ],
                    value[ 'id' ]
                )
            );

        } );

    },

    /**
     * @desc table expanding
    **/
    table_expand: function ()
    {

        $( this ).nextUntil( 'tr.show-details' ).css( 'display', function( i, v ) {

            return this.style.display === 'table-row'
                ? 'none'
                : 'table-row';

        } );

    },

    /**
     * @desc current method allow change table view for mobile devices
    **/
    responsive_table: function()
    {

        //params initializations
        var headertext = [],
            headers    = document.querySelectorAll( '#table-responsive th' ),
            tablebody  = document.querySelector( '#table-responsive tbody' );

        if  ( tablebody !== null ) {

            $.map( headers, function( header, index ) {

                headertext.push(
                    header.textContent.replace( /\r?\n|\r/, "" )
                );

            } );

            for ( var i = 0, row; row = tablebody.rows[ i ]; i++ ) {

                for (var j = 0, col; col = row.cells[ j ]; j++ ) {

                    col.setAttribute( "data-th", headertext[ j ] );

                }

            }

        }

    },

    /**
     * @desc selected thumbnail area
    **/
    current_thumbnail : function()
    {
        thumbnails = $( this );
    },

    /**
     * @desc image drop container settings
     * @param type string
    **/
    drop_zone : function ( type )
    {

        Dropzone.options[ 'upload' + type.substr(0,1).toUpperCase() + type.substr(1) + 'Form' ] = {
            url            : $( 'body' ).data( 'url' ) + '/' + type + '/upload',
            maxFiles       : 1,
            paramName      : "UploadForm[image]",
            thumbnailWidth : "300",
            thumbnailHeight: "300",
            success        : function ( data, file ) {

                $( '.dropzone .form-group' ).hide();

                if ( thumbnails ) {

                    thumbnails.parent().parent().find( 'img' ).attr( 'src', file.url );
                    thumbnails.parent().parent().find( '.file-id' ).val( file.id );

                } else {

                    var current = $( '.need-image' );
                    current.find( 'img' ).val( file.url );
                    current.find( '#file_id' ).val( file.id );
                    $( '.thumbnail-view' ).attr( 'src', file.url );

                }

            }

        };

    },

    /**
     * @desc current method allow adding search to drop-down menu
     * i.e. select2 library is used
     * @param element string
    **/
    select_2: function ( element )
    {

        //params initializations
        var option     = "Select item",
            no_results = 'No results found';

        //set option and no result params
        if ( typeof $( element ).data( 'msg' ) !== 'undefined' ) {
            option     = JSON.parse( $( element ).attr( 'data-msg' ) ).option;
            no_results = JSON.parse( $( element ).attr( 'data-msg' ) ).no_result
        }

        // activate select 2 library
        $( '.select2_single' ).select2( {
            placeholder     : option,
            allowClear      : true,
            formatNoMatches : function () {
                return no_results;
            }
        } );

    },

    /**
     * @desc beautiful user notifications with requested params
     * @param params array
     * @param type string
    **/
    notify_message: function ( params, type )
    {

        if ( type === 'confirm' ) {

            return new PNotify( {
                title   : params[ 'title' ],
                text    : params[ 'message' ],
                icon    : 'glyphicon glyphicon-question-sign',
                hide    : false,
                confirm : {
                    confirm : true
                },
                buttons : {
                    closer : false,
                    sticker: false
                },
                history : {
                    history : false
                },
                addclass : 'stack-modal',
                stack    : {
                    'dir1' : 'down',
                    'dir2' : 'right',
                    'modal': true
                },
                type    : 'success',
                styling : 'bootstrap3'
            } );

        }

        new PNotify( {
            title  : params[ 'title' ],
            text   : params[ 'message' ],
            type   : params[ 'type' ],
            styling: 'bootstrap3'
        } );

    }

};


/**
 * @desc resize function without multiple trigger
**/
( function ( $, sr ) {

    // debouncing function from John Hann
    // http://unscriptable.com/index.php/2009/03/20/debouncing-javascript-methods/
    var debounce = function ( func, threshold, execAsap ) {

        var timeout;

        return function debounced() {

            var obj  = this,
                args = arguments;

            function delayed() {

                if ( !execAsap )
                    func.apply( obj, args );

                timeout = null;

            }

            if ( timeout )
                clearTimeout( timeout );
            else if ( execAsap )
                func.apply( obj, args );

            timeout = setTimeout( delayed, threshold || 100 );

        };

    };

    // smartresize
    jQuery.fn[ sr ] = function ( fn ) {

        return fn
            ? this.bind( 'resize', debounce( fn ) )
            : this.trigger( sr );

    };

} )( jQuery, 'smartresize' );

/**
 * @desc calling current class init ( construct ) method
**/
backend_interrail.init();