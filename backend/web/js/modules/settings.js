/**
 * Name:        Settings Scripts
 * Description: Scripts used in settings pages
 * Version:     1.0.0
**/

$( function () {

    /**
     * @desc this class will hold functions for backend side for setting pages
    **/
    var interrail_settings = {

        /**
         * @desc initialize script for setting pages
        **/
        init: function ()
        {

            //click actions lists and appropriate methods
            $( document )
                .on( 'click', '.add-new-option', this.add_option )
                .on( 'change', '.lotus-sandbox',  function() {

                    var current_container = $( this ).parents( '.tab-pane' );

                    if ( $( this ).is( ':checked' ) ) {
                        $( '.col-sm-12.col-md-12', current_container ).removeClass( 'hide' );
                    } else {
                        $( '.col-sm-12.col-md-12:last-child', current_container ).addClass( 'hide' );
                    }

                } );

            //set language
            $.each( [
                'packages',
                'terms',
                'statuses',
                'footer'
            ], function( index, setting ) {

                // actions with language in settings page
                $( '.' + setting + '-container:gt(0)' ).hide();

                // actions with language in settings page for about us field
                $( '#' + setting + '-lang' ).on( 'change', function () {
                    $( '.' + setting + '-container' ).hide();
                    $( '#' + setting + '-container-' + $( this ).val() ).show();
                } );

            } );

        },

        /**
         * @desc adding new option for requested action
        **/
        add_option: function()
        {

            //params initializations
            var type      = $( this ).data( 'type' ),
                lang      = $( '#' + type + '-lang' ).val(),
                template = $( '#' + type + '-container-template-' + lang ).html();

            //show or hide appropriate requested template
            if ( typeof lang !== 'undefined' ) {

                $( '#' + type + '-container-' + lang ).append(
                    template
                );

            } else {

                $( '.' + type + '-container' ).append(
                    $( '#' + type + '-template' ).html()
                );

            }

            //show notify message on adding new item in settings section
            if ( $( this ).data( 'translations' ) )
                backend_interrail.notify_message(
                    $( this ).data( 'translations' ),
                    'notice'
                )

        }

    };

    /**
     * @desc calling current class init ( construct ) method
    **/
    interrail_settings.init();

} );