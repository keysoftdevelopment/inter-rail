/**
 * Name:        Categories Scripts
 * Description: Scripts used in taxonomies pages
 * Version:     1.0.0
**/

$( function () {

    /**
     * @desc this class will hold functions for backend side for current service
     * examples include sidebar menu actions, modal windows for images uploading and etc ...
     **/
    var interrail_categories = {

        /**
         * @desc initialize script for all pages
        **/
        init: function ()
        {

            //initialization params for current method
            var categories = $( '#categories-table' ).find( 'tbody' );

            //click actions lists and appropriate methods
            $( document )
                .on( 'click', '.actions ul.dropdown-menu a.update-current-type', this.update_options )
                .on( 'click', '#categories-table .save-type', this.save )
                .on( 'submit', 'form.need-seo', this.before_save );


            //adding scrolling to categories sections
            categories.css( 'overflow-x', 'auto' );

            //categories sorting options
            categories.sortable( {
                tolerance: 'pointer',
                items    : ".pointer",
                handle   : ".handle"
            } );

            //option for emoji select
            window.emojiPicker = new EmojiPicker( {
                emojiable_selector: '[data-emojiable=true]',
                assetsPath        : $( 'body' ).data( 'url' ) + '/images/emoji-picker/',
                popupButtonClasses: 'fa fa-smile-o'
            } );

            //set navigation options
            window.emojiPicker.discover();

            // input activation with container types tags options
            this.tag_input( '.container-types' );

            // input activation with wagons tags options
            this.tag_input( '.wagons' );

        },

        /**
         * @desc initialize tag input due to requested css class
        **/
        tag_input: function ( req_class )
        {

            if ( typeof $.fn.tagsInput !== 'undefined'  ) {

                $( req_class ).tagsInput( {
                    width       : '100%',
                    defaultText : $( req_class ).data( 'message' ),
                    autocomplete: {
                        selectFirst: true,
                        autoFill   : true
                    }
                } );

            }

        },

        /**
         * @desc show additional fields for updates
        **/
        update_options: function()
        {

            //hide options
            $( this ).parents( 'tr' ).find( '.item-name' ).hide();
            $( this ).parents( 'tr' ).find( '.description' ).hide();
            $( this ).parents( 'tr' ).find( '.actions' ).hide();

            //show additional options
            $( this ).parents( 'tr' ).find( '.update-type-name' ).show();
            $( this ).parents( 'tr' ).find( '.update-description' ).show();
            $( this ).parents( 'tr' ).find( '.save-type' ).show();

        },

        /**
         * @desc show additional fields for updates
        **/
        save: function()
        {

            //params initializations
            var current = $( this ).parents( 'tr' ),
                tax_name     = current.find( '.update-type-name input' ).val(),
                tax_desc     = current.find( '.update-description input' ).val(),
                lang_id      = $( 'input[name="lang_id"]' ).val(),
                params       = {
                    'Taxonomies[name]' : tax_name
                };

            //set description
            if ( typeof tax_desc !== 'undefined' )
                params[ 'Taxonomies[description]' ] = tax_desc;

            //set appropriate language
            if ( parseInt( lang_id ) > 0 )
                params[ 'Taxonomies[lang_id]' ] = lang_id;

            //update category with requested details
            $.ajax( $( 'body' ).data( 'url' ) + '/categories/update?id=' + current_item.find( 'input[name="type_id"]' ).val(), {
                method : "POST",
                data   : params,
                success: function ( msg ) {

                    //set name and description
                    current.find( '.item-name' ).html( tax_name );
                    current.find( '.description' ).html( tax_desc );

                    //hide additional fields
                    current.find( '.save-type' ).hide();
                    current.find( '.update-description' ).hide();
                    current.find( '.update-type-name' ).hide();

                    //show fields
                    current.find( '.update-hide' ).show();
                    current.find( '.description' ).show();
                    current.find( '.item-name' ).show();
                    current.find( '.actions' ).show();

                },
                error : function ( msg ) {}
            } );

        },

        /**
         * @desc fill in model with selected taxonomies
         * i.e triggering that method before form submit
        **/
        before_save: function()
        {

            //params initializations
            var taxonomies_lists = [],
                model            = $( '#taxonomy-lists' );

            if ( typeof model !== 'undefined' ) {

                $( 'ul.taxonomies-list li' ).each( function( index, value ) {

                    if ( $( value ).find( 'input:checked' ) )
                        taxonomies_lists.push(
                            $( value ).find( 'input:checked' ).data( 'id' )
                        );

                } );

                model.val(
                    JSON.stringify( taxonomies_lists )
                );

            }

        }

    };

    /**
     * @desc calling current class init ( construct ) method
     **/
    interrail_categories.init();

} );