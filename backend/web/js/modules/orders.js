/**
 * Name:        Orders Scripts
 * Description: Scripts used in orders pages
 * Version:     1.0.0
**/

$( function () {

    /**
     * @desc this class will hold functions for backend side for current service
     * examples include sidebar menu actions, modal windows for images uploading and etc ...
    **/
    var interrail_orders = {

        /**
         * @desc initialize script for all pages
        **/
        init: function ()
        {

            //expenses calculations
            backend_interrail.calculate_expenses();

            //actions lists and appropriate methods
            $( document )
                .on( 'click', '.order-form-title a.dropdown-toggle', this.panel_expand )
                .on( 'click', '#add-tracking', this.add_tracking )
                .on( 'focus', '#expenses-main-form .date-field, .single-tracking .tracking .tracking-date, .expenses-option-section .invoice-date, .invoice-details-container .invoice-date', this.date_picker )
                .on( 'change', '#expenses-main-form .date-field', this.storage )
                .on( 'click', '.order-form-content .expenses-form-show', this.expenses_details )
                .on( 'change', '.order-form-content .container-field, .order-form-content .wagon-field', this.transportation_types )
                .on( 'change', '.gu-file-upload-wrapper input', this.gu_file )
                .on( 'change', 'form#expenses-main-form select[name="Expenses[sf_id]"]', this.set_rate )
                .on( 'change', 'form#expenses-main-form select[name="Expenses[auto_id]"]', this.set_rate )
                .on( 'change', 'form#expenses-main-form select.route-field', this.railway )
                .on( 'change', '#sea-freight-form select', this.sea_freight_rate )
                .on( 'change', '#order-form #orders-terminal_id', this.terminals )
                .on( 'click', '#order-form .containers-container .option-section', this.select_container )
                .on( 'click', '#expenses-form .modal-footer a', this.expenses )
                .on( 'click', '#generate-report', this.generate_report )
                .on( 'change', '#order-form #orders-status', function (){

                    //param initialization
                    var dthc_container = $( 'form#order-form .order-dthc' );

                    if( dthc_container
                        && dthc_container.data( 'display' )
                        && dthc_container.data( 'display' ).status === $( this ).val()
                    ) {
                        dthc_container.show();
                    } else {
                        dthc_container.hide();
                    }

                } );

        },

        /**
         * @desc panel expanding in order update page
        **/
        panel_expand: function ()
        {

            //param initialization
            var active_panel = $( 'i.dropdown', $( this ).parents( '.order-form-title' ) );

            //set panel icon
            if ( $( this ).attr( 'aria-expanded' ) === 'true' ) {

                active_panel
                    .removeClass( 'fa-chevron-down' )
                    .addClass( 'fa-chevron-up' );

            } else {

                active_panel
                    .removeClass( 'fa-chevron-up' )
                    .addClass( 'fa-chevron-down' );

            }

        },

        /**
         * @desc adding new tracking option in order update page
        **/
        add_tracking: function()
        {

            //param initialization
            var template = $( $( '#' + $( this ).data( 'type' ) + '-template' ).html() );

            //activate select 2 option
            template.find( 'select.select2_single' ).select2( {
                allowClear  : true,
                placeholder : template.find( 'select.select2_single' ).data( 'msg' )
            } );

            //append template to container
            $( '.' + $( this ).data( 'type' ) + '-container' ).append(
                template
            );

            //notify user with message that action was completed
            if ( $( this ).data( 'translations' ) ) {

                backend_interrail.notify_message(
                    $( this ).data( 'translations' ),
                    'notice'
                );

            }


        },

        /**
         * @desc date picker option in orders pages
        **/
        date_picker: function()
        {

            $( this ).daterangepicker( {
                numberOfMonths  : 100,
                timePicker      : false,
                singleDatePicker: true,
                minDate         : new Date(),
                locale          : {
                    format: 'DD/MM/YYYY'
                }
            } );

        },

        /**
         * @desc expenses details in order update page
        **/
        expenses_details: function()
        {

            //current method params initializations
            var container_tab    = $( 'div#expenses-form .modal-body ul.nav-tabs li:first-child' ),
                params           = $( this ).data( 'details' ),
                railway          = $( '.railway_expenses-container' ),
                railway_rate     = [],
                surcharge_rate   = [],
                routes           = [],
                container_number = $( '.container-field :selected', $( this ).parents( '.option-section' ) ),
                wagon_number     = $( '.wagon-field :selected', $( this ).parents( '.option-section' ) ),
                foreign_key      = {
                    'auto_id': $( 'div#expenses-form select.select2-tagsauto_id-field' ).val(),
                    'sf_id'  : $( 'div#expenses-form select.select2-tagssf_id-field' ).val()
                };

            //fill in railway prices
            $( '.expenses-option-section', railway ).each( function( key, val ) {

                //update rate param
                railway_rate.push(
                    $( val ).find( '.rate-field' ).val()
                );

                //update surcharge param
                surcharge_rate.push(
                    $( val ).find( '.surcharge-field' ).val()
                );

            } );

            //hide or show container fields
            if ( typeof $( '.container-field', $( this ).parents( '.option-section' ) ).val() === 'undefined' ) {
                $( '#expenses-form .tab-content .tab-pane' ).removeClass( 'active in' );
                $( '#expenses-form .tab-content .tab-pane:nth-child(2)' ).addClass( 'active in' );
                container_tab.css( 'display', 'none' );
            } else {
                container_tab.css( 'display', 'block' );
            }

            //remove current class from all sections
            $( '.order-form-content .option-section' ).removeClass( 'current' );

            //adding class for current section for further calculations
            $( this ).parents( '.option-section' ).addClass( 'current' );

            //emptify railway container
            railway.empty();

            $.ajax( $( 'body' ).data( 'url' ) + '/expenses', {
                method  : "POST",
                data    : Object.assign( params, {
                    'container_id': $( this ).data( 'container-id' )
                        ? $( this ).data( 'container-id' )
                        : params.container_id,
                    'wagon_id': $( this ).data( 'wagon-id' )
                        ? $( this ).data( 'wagon-id' )
                        : params.wagon_id
                } ),
                success : function ( result ) {

                    if ( result.code === 200 ) {

                        //params initializations
                        var invoices       = result.response.invoices,
                            railway_params = result.response.expenses !== null && result.response.expenses.id
                                ? JSON.parse(
                                    result.response.expenses.rf_details
                                )
                                : '';

                        if( invoices.length > 0 ) {
                            $( '#invoices .invoice-container' ).empty();
                        }

                        //add container number to header of popup expenses form
                        if ( typeof container_number.html() !== 'undefined'
                            || typeof wagon_number.html() !== 'undefined'
                        ) {

                            $( 'form#expenses-main-form span.container-number' ).html(
                                ' - ' + typeof container_number.html() !== 'undefined'
                                    ? container_number.html()
                                    : wagon_number.html()
                            );

                        }

                        //fill in params details
                        params = Object.assign( params, {
                            container_id: params.container_id === ''
                                ? container_number.val()
                                : params.container_id,
                            wagon_id: params.wagon_id === ''
                                ? wagon_number.val()
                                : params.wagon_id
                        } );

                        //fill in expenses form with expenses id, container id, order id and with wagon id details
                        $.each( params, function ( key, value ) {
                            $( '#expenses-' + key.replace( '_', '-' ) + '' ).val( value );
                        } );

                        //fill in expenses form with terminal id
                        $( '#expenses-terminal-id' ).val( $( '#orders-terminal_id' ).val() );

                        //fill in fields in expenses form
                        $( 'div#expenses-form .form-control' ).each( function( value ) {

                            //param initialization
                            var name = $( this ).attr( 'name' ).replace(
                                'Expenses[', '' ).replace( ']', ''
                            );

                            //set rate
                            if ( result.response.expenses !== null
                                && ( result.response.expenses[ name ] === null || result.response.expenses[ name ] )
                                && name !== 'rf_details'
                                && name !== 'storage'
                            ) {

                                if ( [ 'auto_id', 'sf_id' ].indexOf( name ) > -1 ) {

                                    $( this ).val( ! result.response.expenses[ name ] && [ 'auto_id', 'sf_id' ].indexOf( name ) > -1
                                        ? foreign_key[ name ]
                                        : result.response.expenses[ name ]
                                    );

                                } else {

                                    $( this ).val( result.response.expenses[ name ]
                                        ? result.response.expenses[ name ]
                                        : [ 'lading', 'unloading', 'exp_price', 'imp_price', 'terminal_surcharge' ].indexOf( name ) > -1
                                            ? ''
                                            : 0
                                    );

                                }

                            } else if ( [
                                'terminal_date_from', 'terminal_date_to'
                            ].indexOf( name ) > -1 ) {

                                if ( result.response.rentals
                                    && result.response.rentals[ params.container_id ]
                                ) {

                                    $( this ).val(
                                        result.response.rentals[ params.container_id ][ name.replace( 'terminal_date_', '' ) ]
                                    );

                                }

                            }

                        } );

                        //set routes details
                        $( 'div#code-details .option-section' ).each( function( index, value ) {

                            routes.push( {
                                'route'   : $( value ).find( '.route-field' ).val(),
                                'rg_start': $( value ).find( '.code-rg-start-field' ).val(),
                                'rg_end'  : $( value ).find( '.code-rg-end-field' ).val()
                            } );

                        } );

                        //fill in railway and invoices containers
                        $.map( {
                            'railway_expenses' : {
                                'params' : routes,
                                'fields' : [ 'route-field', 'code-field', 'rate-field', 'surcharge-field' ]
                            },
                            'invoice': {
                                'params' : invoices,
                                'fields' : [ 'invoice-number-field', 'invoice-date', 'user-id-field', 'amount-field' ]
                            }
                        }, function( options, index ) {

                            $.map( options.params, function( option, key ) {

                                //railway, invoice template
                                var template = $(
                                    $( '#' + index + '-template' ).html()
                                );

                                $.map( options.fields, function( field, field_key ) {

                                    //param initialization
                                    var param = field === 'user-id-field'
                                        ? option.customer_id
                                        : option[ field.replace( 'invoice-', '' ).replace( '-field', '' ) ];

                                    //set railway param
                                    if ( railway_params && railway_params.route[ key ]
                                        && railway_params.route[ key ] === option.route
                                    ) {

                                        param = field === 'rate-field' && railway_params.rate && parseFloat( railway_params.rate[ key ] ) > 0
                                            ? parseFloat( railway_params.rate[ key ] )
                                            : field === 'surcharge-field' && railway_params.surcharge && parseFloat( railway_params.surcharge[ key ] ) > 0
                                                ? parseFloat( railway_params.surcharge[ key ] )
                                                : railway_params[ field.replace( '-field', '' ) ]
                                                    ? railway_params[ field.replace( '-field', '' ) ][ key ]
                                                    : field === 'surcharge-field'
                                                        ? surcharge_rate[ key ]
                                                            ? surcharge_rate[ key ]
                                                            : 0
                                                        : railway_rate[ key ]
                                                            ? railway_rate[ key ]
                                                            : 0;

                                    }

                                    //fill in code field section
                                    if( field === 'code-field' && (
                                        ( option.rg_end ) - parseInt( option.rg_start ) + 1
                                    ) ) {

                                        template.find(
                                            'input[name="rf_details[code][]"]'
                                        ).remove();

                                        backend_interrail.select_options( {
                                            'select'  : template.find( '.' + field ),
                                            'options' : Array( parseInt( option.rg_end ) - parseInt( option.rg_start ) + 1 ).fill().map( ( _, idx ) => {

                                                //param initialization
                                                var code = parseInt( option.rg_start ) + idx;

                                                return {
                                                    'id'  : code,
                                                    'name': code
                                                }

                                            } ).filter( ( code ) => {

                                                if ( result.response.codes.length > 0
                                                    && result.response.codes[key] && ( ! railway_params.code || (
                                                        railway_params.code &&
                                                        railway_params.code.length > 0 &&
                                                        railway_params.code[ key ] &&
                                                        railway_params.code[ key ] !== code.name.toString()
                                                    ) )
                                                ) {
                                                    return result.response.codes[key].indexOf( code.name.toString() ) < 0
                                                } else {

                                                    return result.response.codes.length === 0 || ! result.response.codes[key] || result.response.codes[key].indexOf( code.name.toString() ) < 0 || ( railway_params.code &&
                                                        railway_params.code.length > 0 &&
                                                        railway_params.code[ key ] &&
                                                        railway_params.code[ key ] === code.name.toString()
                                                    );

                                                }

                                            } )

                                        } );

                                    } else if ( routes.length > 0
                                        && field === 'code-field'
                                    ) {

                                        //remove select option field
                                        template.find( 'select.' + field ).remove();

                                        //add input field except select field
                                        if ( railway_params ) {

                                            template.find( 'input[name="rf_details[code][]"]' ).val( railway_params.code && railway_params.code.length > 0 && railway_params.code[ key ]
                                                ? railway_params.code[key]
                                                : ''
                                            );

                                        }

                                    }

                                    //fill in requested field
                                    if ( [
                                        'code-field', 'invoice-number-field', 'invoice-date', 'user-id-field'
                                    ].indexOf( field ) < 0 ) {

                                        template.find( '.' + field ).val( field === 'route-field'
                                            ? option.route
                                            : param > 0
                                                ? param
                                                : field === 'surcharge-field'
                                                    ? surcharge_rate[ key ]
                                                        ? surcharge_rate[ key ]
                                                        : 0
                                                    : railway_rate[ key ]
                                                        ? railway_rate[ key ]
                                                        : 0
                                        );

                                    } else if ( [
                                        'invoice-number-field', 'invoice-date'
                                    ].indexOf( field ) >= 0 ) {

                                        template.find( '.' + field ).val(
                                            param
                                        );

                                    }

                                } );

                                //append template to railway or invoice container
                                $( '.' + index + '-container' ).append(
                                    template
                                );

                            } );

                        } );

                        backend_interrail.calculation();

                    }

                },
                error : function ( result ) {
                    console.log( result.message );
                }
            } );
        },

        /**
         * @desc add warning messages
         * if container or wagon type not chosen
        **/
        transportation_types: function()
        {

            //params initializations
            var param    = {},
                expenses = $( '.expenses-form-show', $( this ).parents( '.option-section' ) ),
                method   = $( this ).hasClass( 'wagon-field' ) ? 'Wagons' : 'Containers',
                type     = $( this ).hasClass( 'wagon-field' )
                    ? $( '#wagon-type' )
                    : $( '#container-type' );

            if ( typeof $( ':selected', $( this ) ).attr( 'data-select2-tag' ) !== 'undefined'
                && $( this ).val() !== ''
            ) {

                if ( type.val() === '' ) {

                    //open first section of order and display error
                    $( 'a[href="#general-form"]' ).trigger( 'click' );

                    //add border to show up error field
                    type.css(
                        'border',
                        '1px solid red'
                    );

                    //removing selected option due to appeared error
                    $( 'option[value="' + $( this ).val() + '"]', $( this ) ).remove();

                    //removing selected option due to appeared error
                    $( this ).select2( 'val', '' );

                } else {

                    param[ method + '[number] '] = $( this ).val();
                    param[ method + '[type] ']   = type.val();

                    //remove border style
                    type.removeAttr( 'style' );

                    //add new order
                    interrail_orders.order_new_item( {
                        method   : method.toLowerCase(),
                        param    : param,
                        expenses : expenses,
                        current  : $( this )
                    } );

                }

            } else if ( $( this ).val() !== '' ) {

                //activate expenses option
                interrail_orders.expenses_activation(
                    expenses,
                    method.toLowerCase(),
                    $( this )
                );

            }

        },

        /**
         * @desc replace gu file details
        **/
        gu_file: function()
        {

            $( this ).parent( '.gu-file-upload-wrapper' ).attr(
                'data-text',
                $( this ).val().replace( /.*(\/|\\)/, '' )
            );

        },

        /**
         * @desc fill in expenses sea freight
         * rate and surcharge fields
        **/
        set_rate: function()
        {

            //select field name
            var type = $( this ).attr( 'name' ).includes( 'sf_id' )
                ? 'sea'
                : 'auto';

            if ( $( this ).data( 'details' )[ type ] ) {

                //param initialization
                var names = $( this ).data( 'details' )[ type ][ $( this ).val() ];

                if ( names ) {

                    $.each( names, function( key, value ) {

                        //params initializations
                        var name = type === 'auto' && [ 'rate', 'surcharge' ].indexOf( key ) >= 0
                            ? 'auto_' + key
                            : [ 'rate', 'surcharge' ].indexOf( key ) >= 0
                               ? 'sf_' + key
                               : key,
                            current_expenses = $( 'div#expenses-form input[name="Expenses[' + name + ']"]' );

                        //set sea freight expenses rates
                        if ( typeof current_expenses !== 'undefined' ) {
                            current_expenses.val( value );
                        }

                    } );

                }

            }

        },

        /**
         * @desc fill in expenses railway rate fields
        **/
        railway: function ()
        {

            //param initialization
            var rf_details = $( this ).data( 'details' );

            //fill in rate field
            if ( rf_details && rf_details[ $( this ).val() ] ) {

                $( '.rate-field', $( this ).parents( '.expenses-option-section' ) ).val(
                    rf_details[ $( this ).val() ].rate
                )

            }


        },

        /**
         * @desc fill in expenses sea freight rate field
        **/
        sea_freight_rate: function()
        {

            //param initialization
            var sea_freight = JSON.parse(
                $( this ).attr( 'data-details' )
            );

            //fill in rate field
            if ( sea_freight && sea_freight[ parseInt( $( this ).val() ) ] ) {

                $( '#sea-rate' ).val(
                    sea_freight[ parseInt( $( this ).val(), 10 ) ].price > 0
                        ? sea_freight[ parseInt( $( this ).val(), 10 ) ].price
                        : 0
                );

            }

        },

        /**
         * @desc fill in terminal rates in expenses form
        **/
        terminals: function()
        {

            //param initialization
            var terminal = $( this ).data( 'details' );

            //set expenses for terminal fields
            if ( terminal[ $( this ).val() ] ) {

                $.each( terminal[ $( this ).val() ], function( key, value ) {

                    //param initialization
                    var expenses = $( 'div#expenses-form input[name="Expenses[' + key + ']"]' );

                    //set expenses amount
                    if ( typeof expenses !== 'undefined' && expenses.val() === '' ) {
                        expenses.val( value );
                    }

                } );

            }

            //fill in auto details
            $( '.containers-container .option-section' ).each( function( index, value ) {



            } );

        },

        /**
         * @desc allows selecting the list of containers
         * for including them to terminal report
        **/
        select_container: function()
        {

            if ( $( this ).hasClass( 'selected' ) ) {
                $( this ).removeClass( 'selected' );
            } else {
                $( this ).addClass( 'selected' );
            }

        },

        /**
         * @desc expenses calculations
         * @param params object
        **/
        order_new_item: function( params )
        {

            $.ajax( $( 'body' ).data( 'url' ) + '/' + params[ 'method' ] + '/create', {
                method  : "POST",
                data    : params.param,
                success : function ( result ) {

                    if ( result[ 'response' ][ 'number' ] )
                        $( 'option[value="' + result[ 'response' ][ 'number' ] + '"]', params[ 'current' ] ).attr(
                            'value',
                            result[ 'response' ][ 'id' ]
                        );

                    if ( result.code === 200 ) {

                        interrail_orders.expenses_activation(
                            params[ 'expenses' ],
                            params[ 'method' ],
                            params[ 'current' ]
                        );

                    }

                },
                error : function ( response ) {
                    console.log( response );
                }
            } );

        },

        /**
         * @desc fill in expenses form with container and wagon ids
         * @param expenses mixed
         * @param type string
         * @param current mixed
        **/
        expenses_activation: function( expenses, type, current )
        {

            if ( type === 'containers' ) {

                expenses.attr(
                    'data-container-id',
                    current.val()
                );

            } else {

                expenses.attr(
                    'data-wagon-id',
                    current.val()
                );

            }

        },

        /**
         * @desc terminal storage details
        **/
        storage: function()
        {

            //params initializations
            var terminal       = $( '#orders-terminal_id' ),
                start_date     = moment( $( '.terminal-enter-date' ).val(), 'DD.MM.YYYY' ),
                end_date       = moment( $( '.terminal-export-date' ).val(), 'DD.MM.YYYY'),
                free_detention = $( '.free-detention-field' ).val(),
                storage_period = Math.floor( ( end_date.diff( start_date ) )/( 1000 * 60 * 60 * 24 ) );

            //fill in storage expenses field
            if ( storage_period > 0 && terminal.data( 'details' ) && terminal.data( 'details' )[ terminal.val() ]
                && terminal.data( 'details' )[ terminal.val() ].storage
            ) {

                //free detention sum calculation
                var detention_sum = ( storage_period - ( free_detention !== ''
                    ? parseInt( free_detention )
                    : 0
                ) );

                $( 'div#expenses-form .storage-field' ).val( detention_sum > 0
                    ? parseFloat( terminal.data( 'details' )[ terminal.val() ].storage ).toFixed(2) * detention_sum
                    : 0
                )

            }

        },

        /**
         * @desc order expenses operations
        **/
        expenses: function()
        {

            //params initializations
            var expenses_form = $( '#expenses-main-form' ).serializeArray(),
                total         = 0,
                codes         = {},
                code_routes   = {};

            //total calculations
            $( 'div#expenses-form .form-control.expenses-price-field' ).each( function() {

                if ( ! isNaN( parseInt( $( this ).val() ) )
                    && ! $( this ).hasClass( 'total-sum' )
                    && $( this ).attr( 'name' ) !== 'rf_details[route][]'
                ) {

                    total += parseFloat( ( $( this ).attr( 'name' ) === 'Expenses[storage]' && parseInt( $( this ).val(), 10 ) < 0 )
                        ? 0
                        : parseFloat( $( this ).val() )
                    );

                }

            } );

            //total and surcharge fields in railway codes
            $( 'div#expenses-form .railway_expenses-container .expenses-option-section' ).each( function( index, value ) {

                codes[ $( value ).find( '.route-field' ).val() ] = {
                    rate : codes[ $( value ).find( '.route-field' ).val() ]
                        ? parseFloat( codes[ $( value ).find( '.route-field' ).val() ].rate ) + parseFloat( $( value ).find( '.rate-field' ).val() )
                        : parseFloat( $( value ).find( '.rate-field' ).val() ),
                    surcharge : codes[ $( value ).find( '.route-field' ).val() ]
                        ? parseFloat( codes[ $( value ).find( '.route-field' ).val() ].surcharge ) + parseFloat( $( value ).find( '.surcharge-field' ).val() )
                        : parseFloat( $( value ).find( '.surcharge-field' ).val() )
                };

            } );

            $.ajax( $( 'body' ).data( 'url' ) + '/expenses/update', {
                method : 'POST',
                data   : expenses_form.concat( {
                    'name' : 'Expenses[total]',
                    'value': total.toFixed(2)
                } ),
                success: function ( response ) {

                    //param initialization
                    var current = $(
                            '.order-form-content .option-section.current'
                        );

                    if ( typeof current !== 'undefined' ) {

                        //param initialization
                        var expenses_details = JSON.parse(
                            current.find( '.expenses-form-show' ).attr( 'data-details' )
                        );

                        //fill in expenses details with appropriate data
                        expenses_details = Object.assign( expenses_details, {
                            id          : response.result.id,
                            container_id: response.result.container_id
                                ? response.result.container_id
                                : expenses_details.container_id,
                            wagon_id: response.result.wagon_id
                                ? response.result.wagon_id
                                : expenses_details.wagon_id
                        } );

                        //fill in order input column with expenses id
                        current.find( '.order-expenses-ids' ).attr(
                            'value',
                            response.result.id
                        );

                        //fill in total expenses field
                        current.find( '.total-expenses' ).val( total.toFixed(2) );

                        //fill in railway expenses field
                        current.find( '.railway-expenses' ).val( JSON.stringify( codes ) );

                        //fill in container details with expenses id
                        current.find( '.expenses-form-show' ).data( 'details', expenses_details );

                    }

                    //remove class from selected section
                    $( '.order-form-content .option-section' ).removeClass( 'current' );

                    //calculate code details
                    $( '.option-section .railway-expenses' ).each( function() {

                        //railway rates
                        var railway_rates = $( this ).val()
                            ? JSON.parse( $( this ).val() )
                            : [];

                        if ( railway_rates.length > 0 ) {

                            $.map( railway_rates, function( value, key ) {

                                code_routes[ key ] = {
                                    rate     : parseFloat( value.rate ),
                                    surcharge: parseFloat( value.surcharge )
                                };

                            } );

                        }

                    } );

                    //fill in railway fields in expenses form
                    $( 'div#code-details .option-section' ).each( function( index, value ) {

                        if ( code_routes[ $( value ).find( '.route-field' ).val() ] ) {

                            //fill in surcharge field
                            $( value ).find( '.surcharge-field' ).attr(
                                'value',  code_routes[ $( value ).find( '.route-field' ).val() ].surcharge
                            );

                            //fill in rate field
                            $( value ).find( '.rate-field' ).attr(
                                'value',  code_routes[ $( value ).find( '.route-field' ).val() ].rate
                            );

                        }

                    } );

                    //expenses calculation for current order
                    backend_interrail.calculate_expenses();

                    //close expenses popup window
                    $( 'div#expenses-form .close' ).trigger( 'click' );

                },
                error : function ( response ) {
                    console.log( response );
                }
            } );

        },

        /**
         * @desc order pdf report generation
        **/
        generate_report: function()
        {

            //params initializations
            var container_ids = [],
                order_id      = $( this ).data( 'details' )[ 'order_id' ],
                message       = $( this ).data( 'message' ),
                loader        = $( 'i', $( this ) );

            //activate loader
            loader.show();

            //fill in container ids param
            $( 'form#order-form .option-section.selected .container-field' ).each( function() {

                container_ids.push(
                    $( this ).val()
                );

            } );

            if ( container_ids.length > 0 ) {

                $.ajax( $( 'body' ).data( 'url' ) + '/orders/export-pdf?order_id=' + order_id, {
                    method : 'POST',
                    data   : {
                        terminal_id   : $( '#orders-terminal_id' ).val(),
                        container_ids : container_ids,
                        transportation: $( '#terminal-transportation' ).val(),
                        status        : $( '#orders-terminals-status' ).val(),
                        free_detention: $( '#orders-terminals-free_detention' ).val(),
                        is_vgm        : $( 'input[name="Orders[terminals][vgm_document]"]:checked' ).length > 0
                    },
                    success: function ( response ) {

                        //deactivate loader
                        loader.hide();

                        if ( response.code === 200 ) {

                            for( var link in response.result ) {

                                $.confirm( {
                                    title   : message[ 'title' ],
                                    content : '<iframe style="width: 100%; height: 450px;" src="' + response.result[ link ] + '" />',
                                    buttons : {
                                        formSubmit : {
                                            text     : message[ 'submit' ],
                                            btnClass : 'btn-blue',
                                            action   : function () {

                                                $.ajax( $( 'body' ).data( 'url' ) + '/terminals/send-mail?order_id=' + order_id, {
                                                    method : 'POST',
                                                    success: function ( response ) {

                                                    }, error : function ( result ) {
                                                        console.log( result.message );
                                                    }

                                                } );

                                            }
                                        },
                                        cancel : {
                                            'text'  : message[ 'cancel' ],
                                            'action': function(){}
                                        }
                                    },
                                    onContentReady: function () {

                                        // bind to events
                                        var current = this;

                                        this.$content.find( 'form' ).on( 'submit', function  ( e ) {

                                            // if the user submits the form by pressing enter in the field.
                                            e.preventDefault();

                                            current.$$formSubmit.trigger( 'click' ); // reference the button and click it

                                        } );

                                    }

                                } );

                            }

                        } else {

                            //show error message
                            backend_interrail.notify_message( Object.assign( $( '#generate-report' ).data( 'translations' ), {
                                'message' : response.message
                            } ), 'error' );

                        }

                    },
                    error : function ( result ) {
                        console.log( result.message );
                    }
                } );

            } else {

                //deactivate loader
                loader.hide();

                //show error message
                backend_interrail.notify_message(
                    $( this ).data( 'translations' ),
                    'error'
                );

            }
        }

    };

    /**
     * @desc calling current class init ( construct ) method
    **/
    interrail_orders.init();

} );