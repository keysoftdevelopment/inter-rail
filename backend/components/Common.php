<?php namespace backend\components;

/***************************************/
/*                                     */
/*         MAIN COMMON COMPONENT       */
/*                                     */
/***************************************/

use Yii;
use yii\base\Component;

use yii\helpers\ArrayHelper;
use yii\web\Cookie;

use common\models\Files;
use common\models\Taxonomies;
use common\models\TaxonomyRelation;
use common\models\Settings;

/**
 * Common Component is main component that allows to get custom information
 * I.E Allows to get data such as the list of media files, thumbnail link due to requested file id and etc ...
**/
class Common extends Component
{

	/**
	 * Return current language id from db due to chosen by user language name
	 * @return int|string
	**/
	public function currentLanguage()
	{

		//set language
		if ( empty( $this->getCookies( 'language' ) ) ) {

			//param initialization
			$default_language = Settings::findOne( [
				'name' => 'default_language'
			] );

			//set cookies
			$this->setCookies(
				'language',
				$default_language->description,
				'remove'
			);

		}

		return ! empty( $this->getCookies( 'language' ) )
			? $this->getCookies( 'language' )
			: ( isset( $default_language )
				? $default_language->description
				: ''
			);

	}

	/**
	 * Return the list of thumbnails due to requested file ids
	 * @param array $file_ids
	 * @return string
	**/
	public function thumbnails( $file_ids )
	{

		return Files::find()
			-> where( [
				'id' 	    => $file_ids,
				'isDeleted' => false
			] )
			-> indexBy( 'id' )
			-> asArray()
			-> all();

	}

    /**
     * Return thumbnail due to requested file id
     * @param integer $file_id
     * @return string
    **/
    public function thumbnail( $file_id )
    {

		//params initializations for current method
		$thumbnail = Files::findOne(
			$file_id
		);

        return ! is_null( $thumbnail ) && ! empty( $thumbnail[ 'guide' ] )
			? Yii::getAlias( '@frontend_link' ) . $thumbnail[ 'guide' ]
			: Yii::getAlias( '@web' ) . '\images\image-not-found.png';

    }

    /**
	 * Excerpt of product description
     * @param string $content - source text
     * @return string - excerpt
    **/
    public function excerpt( $content )
    {

		//params initializations for current method
        list( $excerpt ) = explode(
        	"\n",
			wordwrap( $content, 150 )
		);

        return $excerpt;

    }

	/**
	 * Setting and updating user cookies
	 * @param string $name
	 * @param mixed $value
	 * @param string $type
	**/
	public function setCookies( $name, $value, $type = '' )
	{

		//initializations params for current method
		$cookies = \Yii::$app->response->cookies;

		//removing cookie
		if ( \Yii::$app->getRequest()->getCookies()->has( $name ) && !empty ( $type ) )
			$cookies->remove( $name );

		// add a new cookie to the response to be sent
		$cookies->add( new Cookie( [
			'name'   => $name,
			'value'  => $value,
			'expire' => time() + (60 * 60 * 24),
		] ) );

	}

	/**
	 * Returns data from user cookies, i.e chosen language or location and etc ...
	 * @param mixed $name
	 * @return mixed
	**/
	public function getCookies( $name )
	{

		//params initializations for current method
		$request = \Yii::$app->getRequest()->getCookies();

		return $request->has( $name )
			? $request->getValue( $name )
			: '';

	}

	/**
	 * Returns the list of taxonomy details due to requested type
	 * @param string $type
	 * @param integer $tax_id
	 * @return array
	**/
	public function taxonomyDetails( $type = 'containers', $tax_id = 0 )
	{

		//initializations params for current method
		$tax_request = TaxonomyRelation::find()
			-> where( [
				'type' => $type
			] );

		//set tax id in query
		if ( $tax_id > 0 ) {

			$tax_request
				-> andWhere( [
					'tax_id' => $tax_id
				] );

		}

		//taxonomy ids
		$taxonomy_ids = $tax_request
			-> indexBy( 'foreign_id' )
			-> asArray()
			-> all();

		return $tax_id > 0
			? ArrayHelper::getColumn(
				$taxonomy_ids, 'foreign_id'
			)
			: array_reduce( Taxonomies::find()
				-> where( [
					'id' 		=> ArrayHelper::getColumn( $taxonomy_ids, 'tax_id' ),
					'isDeleted' => false
				] )
				-> indexBy( 'id' )
				-> asArray()
				-> all(), function( $result, $taxonomy ) use ( $taxonomy_ids ) {

					if ( ! empty( $taxonomy ) ) {
						foreach ( $taxonomy_ids as $key => $value ) {
							if ( $taxonomy[ 'id' ] == $value[ 'tax_id' ] ) {
								$result[ $key ] = $taxonomy;
							}

						}

					}

					return $result;

			}, [] );

	}

	/**
	 * Returns the list of statuses due to request type
	 * @param string $type
	 * @return array
	**/
	public function statuses( $type = 'containers' )
	{

		//params initializations for current method
		$status = $type == 'tracking'
			? [
				'pre_transit '    => __( 'Pre Transit' ),
				'in_transit'   	  => __( 'In Transit' ),
				'out_of_delivery' => __( 'Out Of Delivery' ),
				'delivered' 	  => __( 'Delivered' )
			]
			: [
			'on way'    => __( 'On way' ),
			'pending'   => __( 'Pending' ),
			'terminal'  => __( 'Terminal' ),
			'completed' => __( 'Completed' )
		];

		//set order status details
		if ( $type == 'order' ) {

			//system default language
			$language = Settings::findOne( [
				'name' => 'default_language'
			] );

			if ( ! is_null( $language ) ) {

				//the list of available order statuses
				$statuses = Settings::findOne( [
					'lang_id' => $language->description,
					'name'	  => 'statuses'
				] );

				//set status param with appropriate details
				if ( ! is_null( $statuses )
					&& ! empty( $statuses->description )
				) {

					$status = array_reduce( json_decode( $statuses->description )->name, function( $statuses, $status ){

						if ( ! empty( $status ) ) {

							$statuses[
								str_replace( [ ' ', '-' ], '', strtolower( $status ) )
							] = $status;

						}

						return $statuses;

					},[] );

				}
			}

		}

		return $status;

	}

	/**
	 * Returns transportation types
	 * @param string $type
	 * @return array
	**/
	public function transportationTypes( $type = 'status' )
	{

		//params initializations for current method
		$response = $type == 'containers_type'
			? [
				'coc' => 'COC',
				'soc' => 'SOC'
			]
			:[
			'empty' => __( 'Empty' ),
			'laden' => __( 'Laden' )
		];

		return $type == 'transportation'
			? $response = [
				'export' => __( 'Export' ),
				'import' => __( 'Import' ),
			]
			: $response;

	}

	/**
	 * Returns logistic types
	 * @param array $request
	 * @return mixed
	**/
	public function logisticTypes( $request = [] )
	{

		//params initializations for current method
		$query = Taxonomies::find()
			-> where( [
				'type' 	    => 'logistics',
				'isDeleted' => false
			] );

		if ( isset( $request[ 'name' ] ) ) {

			$query
				-> andWhere( [
					'like', 'name', $request[ 'name' ]
				] );

		} else {

			$query
				-> indexBy( 'id' );

		}

		return isset( $request[ 'serialize' ] )
			? array_map( function( $type ) {

				return ! empty( $type )
					? [
						'id'   => $type[ 'id' ],
						'slug' => strtolower( $type[ 'name' ] ),
						'name' => __( $type[ 'name' ] )
					]
					: [];

			}, $query
				-> asArray()
				-> all()
			)
			: $query
				-> asArray()
				-> all();

	}

	/**
	 * Returns the list of all available transportation
	 * details due to requested type
	 *
	 * @param string $type
	 * @return mixed
	**/
	public function transportationDetails( $type = 'terms' )
	{

		//params initializations for current method
		$language = Settings::findOne( [
			'name' => 'default_language'
		] );

		$transportation = ! is_null( $language )
			? Settings::findOne( [
				'name' 	  => $type,
				'lang_id' => $language->description
			] )
			: Settings::findOne( [
				'name' => $type
			] );

		return isset( $transportation->description )
			&& ! empty( $transportation->description )
			? json_decode( $transportation->description )
			: [];

	}

	/**
	 * Returns the list of icons
	 * @param string $type
	 * @return array
	**/
	public function icons( $type = 'general' )
	{

		return $type == 'statistics'
			? [
				'automobile' 	   => 'fa-automobile',
				'bicycle'	 	   => 'fa-bicycle',
				'bus'		 	   => 'fa-bus',
				'car'		 	   => 'fa-car',
				'world-icon' 	   => 'fa-globe',
				'customers-icon'   => 'fa-male',
				'plane'		 	   => 'fa-plane',
				'ship'		 	   => 'fa-ship',
				'taxi'		 	   => 'fa-taxi',
				'products-icon'    => 'fa-hdd-o',
				'truck-icon-black' => 'fa-truck',
				'train-icon-black' => 'fa-train',
			]
			: [
				'automobile' => 'fa-automobile',
				'bicycle'	 => 'fa-bicycle',
				'bus'		 => 'fa-bus',
				'car'		 => 'fa-car',
				'globe' 	 => 'fa-globe',
				'male'		 => 'fa-male',
				'plane'		 => 'fa-plane',
				'ship'		 => 'fa-ship',
				'taxi'		 => 'fa-taxi',
				'truck' 	 => 'fa-truck',
				'train'		 => 'fa-train',
			];

	}

	/**
	 * Returns the list of available user roles
	 * @return array
	**/
	public function roles()
	{

		return array_reduce( Yii::$app->authManager->getRoles(), function( $roles, $role ) {

			if ( ! empty( $role ) ) {
				$roles[ $role->name  ] = __( $role->name );
			}

			return $roles;

		}, [] );

	}

}