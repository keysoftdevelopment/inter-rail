<?php namespace backend\components;

/**************************************/
/*                                    */
/*       EXCEL EXPORT COMPONENT       */
/*                                    */
/**************************************/

use Yii;
use yii\base\Component;

use yii\helpers\ArrayHelper;

use hscstudio\export\OpenTBS;

use common\models\Containers;
use common\models\ContainersRelation;
use common\models\Settings;
use common\models\Taxonomies;
use common\models\TaxonomyRelation;
use common\models\User;
use common\models\Wagons;
use common\models\WagonsRelation;

/**
 * Excel Export Component allow to gather order details data and export them in xlsx file
**/
class ExcelExportComponent extends Component
{

	/**
	 * Logistic details exporting
	 * @param array $params
	**/
	public function logisticsExport( $params )
	{

		//params initializations for current method
		$types = Taxonomies::find()
			-> where( [
				'type' => $params[ 'type' ] == 'railway'
					? $params[ 'type' ]
					: 'containers',
				'isDeleted' => false
			] )
			-> indexBy( 'id' )
			-> asArray()
			-> all();

		if ( ! empty( $params ) ) {

			//logistic relations due to types
			$logistic_relation = TaxonomyRelation::find()
				-> where( [
					'type' => $params[ 'type' ]
				] )
				-> indexBy( 'foreign_id' )
				-> asArray()
				-> all();

			$this->excelExportMethod(
				\Yii::getAlias('@backend') . '/web/reports/' . $params[ 'report' ] . '.xlsx',
				__( $params[ 'file_name' ] ) . '.xlsx',
				array_reduce( $params[ 'details' ], function( $result, $param ) use ( $logistic_relation, $types, $params ) {

					if ( ! empty( $param ) ) {

						$logistic = [
							'no'   => count( $result ) + 1,
							'from' => $param->from,
							'to'   => $param->to,
							'type' => isset( $types[ $logistic_relation[ $param->id ][ 'tax_id' ] ] )
								? $types[ $logistic_relation[ $param->id ][ 'tax_id' ] ][ 'name' ] . ' ' . $param->c_type
								: $param->c_type,
							'rate'	   => $param->rate,
							'validity' => date('t-m-Y', strtotime( $param->updated_at ) )
						];

						if ( $params[ 'type' ] == 'sea' ) {

							$result[] = array_replace( $logistic, [
								'line_name'    => $param->name,
								'transit_time' => (int)$param->duration / 86400
							] );

						} else if ( $params[ 'type' ] == 'auto' ) {

							$result[] = array_replace( $logistic, [
								'transportation' => $param->transportation,
								'status'   	     => $param->status,
							] );

						} else if ( $params[ 'type' ] == 'railway' ) {

							$result[] = array_replace( $logistic, [
								'weight' 	   => $param->weight,
								'transit_time' => (int)$param->duration / 86400,
							] );

						}

					}

					return $result;

				}, [] )
			);

		}

	}

	/**
	 * Logistic details exporting
	 * @param array $params
	**/
	public function expensesExports( $params )
	{

		//initializations params for current method
		$role  			  = key( Yii::$app->authManager->getRolesByUser( Yii::$app->user->id ) );
		$contractor_role  = Settings::findOne( [ 'name' => 'contractor_role' ] );
		$expenses_details = [
			'expenses' => [],
			'railway'  => [],
			'invoice'  => []
		];

		//additional fields due to roles permissions
		if ( $role != 'admin' ) {

			$permissions = Settings::findOne( [
				'name' => $role . '_order_permissions'
			] );

			//the list of permission options
			$permission_options = [
				'expenses_general' 	   	   => [ 'thc', 'tracking', 'surcharge' ],
				'expenses_container'   	   => [ 'sale', 'purchase', 'rental', 'deposit', 'container_surcharge', 'container_cost' ],
				'expenses_sea_freight' 	   => [ 'sf_rate', 'dthc', 'othc', 'bl', 'sf_surcharge' ],
				'expenses_auto' 	   	   => [ 'auto_rate', 'auto_surcharge' ],
				'expenses_railway' 	   	   => [ 'rf_details', 'rf_rental', 'rf_commission' ],
				'expenses_port_forwarding' => [ 'pf_rate', 'pf_surcharge', 'pf_commission' ],
				'expenses_terminal' 	   => [ 'storage', 'lading', 'unloading', 'exp_price', 'imp_price', 'terminal_surcharge' ]
			];

		}

		//used to show customer information in reports
		$contractors = isset( $contractor_role->description )
			? User::find()
				-> where( [
					'id' 	    => Yii::$app->authManager->getUserIdsByRole( $contractor_role->description ),
					'isDeleted' => false
				] )
				-> indexBy( 'id' )
				-> asArray()
				-> all()
			: [];

		//requested container relations query
		$containers_relation_query = $params[ 'type' ] == 'wagons'
			? WagonsRelation::find()
			: ContainersRelation::find();

		//requested container query
		$containers_query = $params[ 'type' ] == 'wagons'
			? Wagons::find()
			: Containers::find();

		//the list of containers ids
		$containers_ids = $containers_relation_query
			-> where( [
				'foreign_id' => ArrayHelper::getColumn( $params[ 'expenses' ], 'id' ),
				'type'		 => 'expenses'
			] )
			-> asArray()
			-> all();

		//the list of containers details
		$containers = $containers_query
			-> where( [
				'id' => ArrayHelper::getColumn( $containers_ids, $params[ 'type' ] == 'wagons'
					? 'wagon_id'
					: 'container_id'
				)
			] )
			-> indexBy( 'id' )
			-> asArray()
			-> all();

		//foreign numbers of containers
		$foreign_number = array_reduce( $containers_ids, function( $filter, $container ) use ( $containers, $params ) {

			if ( isset( $containers[ $container[ $params[ 'type' ] == 'wagons'
				? 'wagon_id'
				: 'container_id'
			] ] ) ) {

				$filter[ $container[ 'foreign_id' ] ] = $containers[ $container[ $params[ 'type' ] == 'wagons'
					? 'wagon_id'
					: 'container_id'
				] ][ 'number' ];

			}


			return $filter;

		}, [] );

		//report details generations
		if ( $role == 'admin'
			|| isset( $permissions )
			&& ! empty( $permissions->description )
		) {

			foreach ( $params[ 'expenses' ] as $key => $expense ) {

				$expense[ rtrim( $params[ 'type' ], 's' ) . '_number' ] = isset( $foreign_number[ $expense[ 'id' ] ] )
					? $foreign_number[ $expense[ 'id' ] ]
					: '';

				$expense[ 'no' ] = $key + 1;

				if ( $role != 'admin' ) {

					$permissions = json_decode(
						$permissions->description
					);

					foreach ( $permission_options as $perm_key => $permission ) {
						if ( ! isset( $permissions->{$perm_key} ) ) {
							foreach ( $permission as $perm ) {
								$expense[ $perm ] = 0;
							}
						}
					}

				}

				$expenses_details[ 'expenses' ][ $key ] = $expense;
				$railway_details 						= ! empty( $expense[ 'rf_details' ] )
					? json_decode( $expense[ 'rf_details' ] )
					: [];

				if ( isset( $railway_details->route ) ) {

					foreach ( $railway_details->route as $rf_key => $value ) {

						$expenses_details[ 'railway' ][ $key ] = [
							'no'   					      			            => $expense[ 'no' ],
							rtrim( $params[ 'type' ], 's' ) . '_number' => isset( $foreign_number[ $expense[ 'id' ] ] )
								? $foreign_number[ $expense[ 'id' ] ]
								: '',
							'code' => isset( $railway_details->code[ $rf_key ] )
								? $railway_details->code[ $rf_key ]
								: 0,
							'rate' => isset( $railway_details->rate[ $rf_key ] )
								? $railway_details->rate[ $rf_key ]
								: 0,
							'surcharge' => isset( $railway_details->surcharge[ $rf_key ] )
								? $railway_details->surcharge[ $rf_key ]
								: 0
						];

					}

				}

				if ( $role == 'admin' || isset( $permissions->expenses_invoices )
					&& $permissions->expenses_invoices == 'on'
				) {

					if ( isset( $params[ 'invoices' ][ $expense[ 'id' ] ] ) ) {

						foreach ( $params[ 'invoices' ][ $expense[ 'id' ] ] as $invoice_key => $invoice ) {

							$invoice[ 'no' ] 						  = $invoice_key + 1;
							$invoice[ $params[ 'type' ] . '_number' ] = isset( $foreign_number[ $expense[ 'id' ] ] )
								? $foreign_number[ $expense[ 'id' ] ]
								: '';

							$invoice[ 'customer' ] = isset( $contractors[ $invoice[ 'user_id' ] ] )
								? $contractors[ $invoice[ 'user_id' ] ][ 'email' ]
								: __( 'Contractor was not chosen' );

							$expenses_details[ 'invoice' ][] = $invoice;

						}

					}

				}

			}

		}

		// Initalize the TBS instance
		$OpenTBS = new OpenTBS;

		$OpenTBS->LoadTemplate( Yii::getAlias('@backend') . '/web/reports/' . $params[ 'report' ] . '.xlsx', 'utf-8' );

		$OpenTBS->SetOption( 'charset', 'UTF-8' );

		$OpenTBS->MergeBlock('title', $params[ 'model' ] );
		$OpenTBS->MergeBlock('expenses', $expenses_details[ 'expenses' ] );

		$OpenTBS->PlugIn(OPENTBS_SELECT_SHEET, 2 );
		$OpenTBS->MergeBlock('railway', $expenses_details[ 'railway' ] );

		$OpenTBS->PlugIn(OPENTBS_SELECT_SHEET, 3 );
		$OpenTBS->MergeBlock('invoice', $expenses_details[ 'invoice' ] );

		// Output the result as a file on the server. You can change output file
		$OpenTBS->Show(OPENTBS_DOWNLOAD, __( $params[ 'file_name' ] ) . '.xlsx' );

		exit;

	}

	/**
	 * General export details for requested details
	 * @param array $params
	**/
	public function export( $params )
	{

		//initializations params for current method
		$models	= in_array( $params[ 'type' ], [
			'balance_report',
			'balance_detailed_report',
			'bt_transportations',
			'conter_agent_receivables',
			'customers_receivables',
			'incomes',
			'terminal_storage',
			'transportations'
		] )
			? $params[ 'details' ][ 'models' ]
			: $params[ 'details' ][ $params[ 'type' ] == 'codes'
				? 'codes'
				: 'invoices'
			];

		//report exporting
		$this->excelExportMethod(
			\Yii::getAlias('@backend') . '/web/reports/' . $params[ 'report' ] . '.xlsx',
			__( $params[ 'file_name' ] ) . '.xlsx',
			array_reduce( $params[ 'type' ] == 'containers'
				? $params[ 'models' ]
				: $models, function( $result, $model ) use ( $params ) {

				if ( ! empty( $model ) ) {

					//the list of customers
					$customers = isset( $params[ 'details' ][ 'customers' ] )
						? $params[ 'details' ][ 'customers' ]
						: [];

					//expenses details
					$expenses = isset( $params[ 'details' ][ 'expenses' ] ) && isset( $params[ 'details' ][ 'expenses' ][ $model[ 'id' ] ] )
						? $params[ 'details' ][ 'expenses' ][ $model[ 'id' ] ]
						: 0;

					//order details
					if ( in_array( $params[ 'type' ], [ 'codes', 'profit_lost' ] ) ) {

						$order = isset( $params[ 'details' ][ 'models' ][ $params[ 'type' ] == 'profit_lost'
								? $model[ 'foreign_id' ]
								: $model->order_id
							] )
							? $params[ 'details' ][ 'models' ][ $params[ 'type' ] == 'profit_lost'
								? $model[ 'foreign_id' ]
								: $model->order_id
							]
							: 0;

					}

					if ( $params[ 'type' ] == 'containers' ) {

						$result[] = [
							'no' 	 => count( $result ) + 1,
							'number' => $model->number,
							'type'	 => isset( $params[ 'taxonomies' ][ $model->id ] )
								? $params[ 'taxonomies' ][ $model->id ]
								: __( 'Type not chosen' ),
							'owner'  => $model->owner,
							'status' => $model->status
						];

					} else if ( $params[ 'type' ] == 'balance_report' ) {

						//total amount details
						$total = is_array( $expenses )
							? (float)$expenses[ 'total_cost' ]
							: (float)$expenses;

						//invoice amount details
						$invoice = isset( $params[ 'details' ][ 'invoices' ][ $model->id ][ 'total' ] )
							? (float)$params[ 'details' ][ 'invoices' ][ $model->id ][ 'total' ]
							: 0;

						//quantity of items
						$qty = ! empty( $model->packages )
							? json_decode( $model->packages )->quantity
							: 0;

						$result[] = [
							'no' 	   => count( $result ) + 1,
							'train_no' => isset( $params[ 'details' ][ 'logistics' ][ $model->id ] )
								? $params[ 'details' ][ 'logistics' ][ $model->id ]
								: 0,
							'order_no' => $model->number,
							'customer' => isset( $customers[ $model->user_id ] )
								? $customers[ $model->user_id ][ 'first_name' ] . ' ' . $customers[ $model->user_id ][ 'last_name' ]
								: __( 'Unknown Customer' ),
							'invoiced_amount' => $invoice,
							'qty'		      => $qty,
							'container_cost'  => $expenses[ 'container_cost' ],
							'sea' 		  	  => $expenses[ 'sea' ],
							'auto' 		      => $expenses[ 'auto' ],
							'railway' 		  => $expenses[ 'railway' ],
							'terminals' 	  => $expenses[ 'terminals' ],
							'surcharges' 	  => $expenses[ 'surcharges' ],
							'total'  	      => $total,
							'margin' 		  => $invoice > 0
								? $invoice - $total
								: 0
						];

					} else if ( $params[ 'type' ] == 'balance_detailed_report' ) {

						//container details
						$container = isset( $params[ 'details' ][ 'container' ][ 'numbers' ][ $model[ 'id' ] ] )
							? $params[ 'details' ][ 'container' ][ 'numbers' ][ $model[ 'id' ] ]
							: '';

						//railway details
						$railway = ! empty( $model[ 'rf_details' ] )
							? json_decode( $model[ 'rf_details' ] )
							: [];

						$result[] = [
							'no' 	   => count( $result ) + 1,
							'train_no' => isset( $params[ 'details' ][ 'logistics' ][ $model[ 'order_id' ] ] )
								? $params[ 'details' ][ 'logistics' ][ $model[ 'order_id' ] ]
								: 0,
							'order_no' => isset( $model[ 'order' ]->number )
								? $model[ 'order' ]->number
								: '',
							'customer' => isset( $customers[ $model[ 'order' ]->user_id ] )
								? $customers[ $model[ 'order' ]->user_id ][ 'first_name' ] . ' ' . $customers[ $model[ 'order' ]->user_id ][ 'last_name' ]
								: __( 'Unknown Customer' ),
							'container_no' => ! empty( $container )
								? $container[ 'number' ]
								: '',
							'wagon_no' => ! empty( $container ) && isset( $params[ 'details' ][ 'container' ][ 'wagons' ][ $container[ 'id' ] ] )
								? $params[ 'details' ][ 'container' ][ 'wagons' ][ $container[ 'id' ] ][ 'number' ]
								: '',
							'container_type' => ! empty( $container ) && isset( $params[ 'details' ][ 'container' ][ 'types' ][ $container[ 'id' ] ] )
								? $params[ 'details' ][ 'container' ][ 'types' ][ $container[ 'id' ] ][ 'name' ]
								: '',
							'container_cost' => $model[ 'container_cost' ],
							'sea'			 => ! empty( $model )
								? array_sum( array_intersect_key( $model, array_flip( [
									'dthc', 'othc', 'bl', 'sf_rate', 'sf_surcharge'
								] ) ) )
								: 0,
							'auto' => ! empty( $model )
								? array_sum( array_intersect_key( $model, array_flip( [
									'auto_rate', 'auto_surcharge'
								] ) ) )
								: 0,
							'railway' => array_reduce( ! empty( $railway )
								? array_keys( $railway->route )
								: [], function( $result, $key ) use ( $railway ) {

									if( ! empty( $railway ) ) {
										$result += (float)$railway->rate[ $key ] + (float)$railway->surcharge[ $key ];
									}

									return $result;

							}, 0 ),
							'terminals' => ! empty( $model )
								? array_sum( array_intersect_key( $model, array_flip( [
									'storage', 'lading', 'unloading', 'exp_price', 'imp_price', 'terminal_surcharge'
								] ) ) )
								: 0,
							'surcharges' => $model[ 'surcharge' ],
							'cost'		 => $model[ 'total' ]
						];

					} else if ( $params[ 'type' ] == 'bt_transportations' ) {

						//container type due to taxonomy relations
						$container_type = isset( $params[ 'details' ][ 'taxonomies' ][ 'relations' ][ $model->id ] )
							? $params[ 'details' ][ 'taxonomies' ][ 'relations' ][ $model->id ]
							: __( 'Type of Container Not Chosen' );

						$result[] = [
							'no' 	=> count( $result ) + 1,
							'bt_no' => isset( $params[ 'details' ][ 'logistics' ][ $model->id ] )
								? $params[ 'details' ][ 'logistics' ][ $model->id ]
								: 0,
							'order_no' => $model->number,
							'customer' => isset( $customers[ $model->user_id ] )
								? $customers[ $model->user_id ][ 'first_name' ] . ' ' . $customers[ $model->user_id ][ 'last_name' ]
								: __( 'Unknown Customer' ),
							'container_type' => is_array( $container_type ) && isset( $params[ 'details' ][ 'taxonomies' ][ 'list' ][ $container_type[ 'tax_id' ] ] )
								? $params[ 'details' ][ 'taxonomies' ][ 'list' ][ $container_type[ 'tax_id' ] ][ 'name' ]
								: __( 'Type of Container Not Chosen' ),
							'qty' => isset( $params[ 'details' ][ 'container_quantity' ][ $model->id ] )
								? $params[ 'details' ][ 'container_quantity' ][ $model->id ][ 'quantity' ]
								: 0,
							'total_invoice' => isset( $params[ 'details' ][ 'invoices' ][ $model->id ] )
								? $params[ 'details' ][ 'invoices' ][ $model->id ][ 'quantity' ]
								: 0,
							'total'  => $model->total,
							'margin' => isset( $params[ 'details' ][ 'invoices' ][ $model->id ] )
								? (float)$params[ 'details' ][ 'container_quantity' ][ $model->id ][ 'quantity' ] - (float)$model->total
								: 0
						];

					} else if ( $params[ 'type' ] == 'codes' ) {

						//quantity of order packages
						$quantity = isset( $order->packages ) && ! empty( $order->packages )
							? json_decode( $order->packages )
							: 0;

						//weight dimensions of current order
						$volume = isset( $order->weight_dimensions ) && ! empty( $order->weight_dimensions )
							? json_decode( $order->weight_dimensions )
							: 0;

						$result[] = [
							'no'   => count( $result ) + 1,
							'date' => ! empty( $model->created_at )
								? date( 'd.m.Y', strtotime( $model->created_at ) )
								: __( 'Date not chosen' ),
							'from' => isset( $order->trans_from )
								? $order->trans_from
								: __( 'Not chosen' ),
							'to' => isset( $order->trans_to )
								? $order->trans_to
								: __( 'Not chosen' ),
							'country' => $model->country,
							'route'   => $model->route,
							'code' 	  => $model->code,
							'qty'     => isset( $quantity->quantity )
								? $quantity->quantity
								: 0,
							'volume' => isset( $volume->gross )
								? $volume->gross
								: 0,
							'rate' 	  	 => $model->rate,
							'surcharges' => $model->surcharge,
							'total'		 => (float)$model->rate + (float)$model->surcharge
						];

					}  else if ( $params[ 'type' ] == 'conter_agent_receivables' ) {

						$result[] = [
							'no' 	   => count( $result ) + 1,
							'order_no' => $model[ 'order_number' ],
							'qty'	   => $model[ 'quantity' ],
							'volume'   => $model[ 'volume' ],
							'total'    => $model[ 'total' ],
							'date' 	   => $model[ 'date' ]
						];

					}  else if ( $params[ 'type' ] == 'customers_receivables' ) {

						//invoice due to requested order id
						$current_invoice = isset( $params[ 'details' ][ 'invoices' ][ $model->id ] )
							? ( isset ( $params[ 'details' ][ 'invoices' ][ $model->id ][ 'total' ] )
								? (float)$params[ 'details' ][ 'invoices' ][ $model->id ][ 'total' ]
								: (float)$params[ 'details' ][ 'invoices' ][ $model->id ]
							)
							: 0;

						$result[] = [
							'no' 	   => count( $result ) + 1,
							'train_no' => isset( $params[ 'details' ][ 'logistics' ][ $model->id ] )
								? $params[ 'details' ][ 'logistics' ][ $model->id ]
								: 0,
							'order_no' => $model->number,
							'customer' => isset( $customers[ $model->user_id ] )
								? $customers[ $model->user_id ][ 'first_name' ] . ' ' . $customers[ $model->user_id ][ 'last_name' ]
								: __( 'Unknown Customer' ),
							'total_invoice' => $current_invoice,
							'total' 	    => $model->total,
							'date' 		    => isset( $current_invoice[ 'date' ] )
								? date( 'd.m.Y', strtotime( $current_invoice[ 'date' ] ) )
								: __( 'Date not chosen' ),
							'debt' => (float)$model->total - $current_invoice
						];

					} else if ( $params[ 'type' ] == 'incomes' ) {

						$result[] = [
							'no' 	   	   => count( $result ) + 1,
							'customer' => isset( $customers[ $model->customer_id ] )
								? $customers[ $model->customer_id ][ 'first_name' ] . ' ' . $customers[ $model->customer_id ][ 'last_name' ]
								: __( 'Unknown Customer' ),
							'date'     => $model->date,
							'amount'   => $model->amount,
							'notes'    => $model->status
						];

					} else if ( $params[ 'type' ] == 'profit_lost' ) {

						//total price details
						$total = isset( $order->total )
							? (float)$order->total
							: (float)$order;

						$result[] = [
							'no' 	   => count( $result ) + 1,
							'train_no' => isset( $params[ 'details' ][ 'logistics' ][ $order->id ] )
								? $params[ 'details' ][ 'logistics' ][ $order->id ]
								: 0,
							'order_no' => isset( $order->number )
								? $order->number
								: $order,
							'date' => ! empty( $model[ 'date' ] )
								? date( 'd.m.Y', strtotime( $model[ 'date' ] ) )
								: __( 'Date not chosen' ),
							'customer' => isset( $customers[ $model[ 'customer_id' ] ] )
								? $customers[ $model->customer_id ][ 'first_name' ] . ' ' . $customers[ $model[ 'customer_id' ] ][ 'last_name' ]
								: __( 'Unknown Customer' ),
							'total_invoice' => $model[ 'total' ],
							'total' 	    => $total,
							'margin' 	    => (float)$model[ 'total' ] - $total
						];

					}  else if ( $params[ 'type' ] == 'terminal_storage' ) {

						//terminal rentals periods
						$rentals = isset( $params[ 'details' ][ 'rentals' ][ $model[ 'id' ] ] )
							? $params[ 'details' ][ 'rentals' ][ $model[ 'id' ] ]
							: __( 'Rentals Date not Chosen' );

						$result[] = [
							'no' 	   => count( $result ) + 1,
							'order_no' => $expenses[ 'order' ][ 'number' ],
							'train_no' => isset( $params[ 'details' ][ 'logistics' ][ $expenses[ 'order' ][ 'id' ] ] )
								? $params[ 'details' ][ 'logistics' ][ $expenses[ 'order' ][ 'id' ] ]
								: 0,
							'container_no'   => $model[ 'number' ],
							'container_type' => isset( $params[ 'details' ][ 'taxonomies' ][ $model[ 'container_type' ] ] )
								? $params[ 'details' ][ 'taxonomies' ][ $model[ 'container_type' ] ][ 'name' ]
								: __( 'Type of Container Not Chosen' ),
							'owner'      	   => $model[ 'owner' ],
							'enter_date'	   => isset( $rentals[ 'from' ] )
								? date('d.m.Y', strtotime( $rentals[ 'from' ] ) )
								: $rentals,
							'export_date' => isset( $rentals[ 'to' ] )
								? date('d.m.Y', strtotime( $rentals[ 'to' ] ) )
								: $rentals,
							'storage' => isset( $expenses[ 'storage' ] )
								? $expenses[ 'storage' ]
								: $expenses,
							'loading' => isset( $expenses[ 'loading' ] )
								? $expenses[ 'loading' ]
								: $expenses,
							'unloading' => isset( $expenses[ 'unloading' ] )
								? $expenses[ 'unloading' ]
								: $expenses,
							'surcharges' => isset( $expenses[ 'surcharges' ] )
								? $expenses[ 'surcharges' ]
								: $expenses,
							'total' => array_reduce( ! empty( $expenses )
								? $expenses
								: [], function( $result, $expenses ) {

								if( ! empty( $expenses ) ) {
									$result += $expenses;
								}

								return $result;

							}, 0 )
						];

					}  else if ( $params[ 'type' ] == 'transportations' ) {

						//package quantity
						$quantity = ! empty( $model->packages )
							? json_decode( $model->packages )
							: 0;

						//weight dimension information
						$volume = ! empty( $model->weight_dimensions )
							? json_decode( $model->weight_dimensions )
							: 0;

						$result[] = [
							'no'   			 => count( $result ) + 1,
							'from' 		     => $model->trans_from,
							'to'   		     => $model->trans_to,
							'type' 	         => $model->transportation,
							'container_type' => isset( $params[ 'details' ][ 'container_types' ][ $model->id ] )
								? $params[ 'details' ][ 'container_types' ][ $model->id ][ 'name' ]
								: '',
							'qty' => isset( $quantity->quantity )
								? $quantity->quantity
								: 0,
							'volume' => isset( $volume->gross )
								? $volume->gross
								: 0,
							'customer' => isset( $customers[ $model->user_id ] )
								? $customers[ $model->user_id ][ 'first_name' ] . ' ' . $customers[ $model->user_id ][ 'last_name' ]
								: __( 'Unknown Customer' )
						];

					}

				}

				return $result;

			}, [] ),
			$params[ 'type' ] != 'containers'
				? $params[ 'model' ] :
				[]
		);

	}

	/**
	 * Export to excel file due to requested data with requested template
	 * @param string $template
	 * @param array $export_details
	 * @param string $export_file_name
	 * @param array $title
	**/
	public function excelExportMethod( $template, $export_file_name, $export_details, $title = [] )
	{

		// Initalize the TBS instance
		$OpenTBS = new OpenTBS;

		$OpenTBS->LoadTemplate( $template, 'utf-8' );
		$OpenTBS->SetOption( 'charset', 'UTF-8' );

		if ( ! empty( $title ) )
			$OpenTBS->MergeBlock('title', $title );

		$OpenTBS->MergeBlock('general', $export_details );

		// Output the result as a file on the server. You can change output file
		$OpenTBS->Show(OPENTBS_DOWNLOAD, $export_file_name );

		exit;

	}

}