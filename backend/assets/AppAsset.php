<?php namespace backend\assets;

/***************************************/
/*                                     */
/*      DEFAULT SCRIPTS AND STYLES     */
/*                                     */
/***************************************/

use yii\web\AssetBundle;

class AppAsset extends AssetBundle
{

    public $basePath = '@webroot';
    public $baseUrl  = '@web';
    public $css      = [
        'fonts/css/font-awesome.min.css',
        'css/animate.min.css',
        'css/icheck/flat/green.css',
        'css/select/select2.min.css',
        'css/switchery/switchery.min.css',
        'css/cropper.min.css',
        'css/bootstrap-colorpicker.min.css',
		'css/flag-icon.min.css',
		'css/jquery-ui.min.css',
		'css/jquery-confirm/jquery-confirm.min.css',
		'css/pnotify/pnotify.min.css',
        'css/custom.css'
    ];

    public $js = [
        'js/nprogress.min.js',
        'js/bootstrap.min.js',
        'js/progressbar/bootstrap-progressbar.min.js',
        'js/icheck/icheck.min.js',
        'js/switchery/switchery.min.js',
        'js/select/select2.full.js',
        'js/moment/moment.min.js',
        'js/dropzone/dropzone.min.js',
        'js/slug.min.js',
        'js/menu.min.js',
        'js/jquery_ui/jquery-ui.min.js',
        'js/lazyload.js',
        'js/cropper.min.js',
        'js/bootstrap-colorpicker.min.js',
		'js/emoji-picker/config.js',
		'js/emoji-picker/util.js',
		'js/emoji-picker/jquery.emojiarea.js',
		'js/emoji-picker/emoji-picker.js',
		'js/tagsinput/tagsinput.min.js',
		'js/date-range-picker.min.js',
		'js/country-picker.min.js',
		'js/jquery-confirm.min.js',
		'js/pnotify/pnotify.min.js',
		'js/pnotify/pnotify.confirm.min.js',
		'js/pnotify/pnotify.buttons.min.js',
        'js/modules/common.js',
		'js/modules/categories.js',
		'js/modules/menus.js',
		'js/modules/orders.js',
		'js/modules/pages.js',
		'js/modules/settings.js'
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset'
    ];
}