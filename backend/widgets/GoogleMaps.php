<?php namespace backend\widgets;

/**************************************/
/*                                    */
/*       GOOGLE MAPS WIDGET           */
/*                                    */
/**************************************/

use yii\base\Widget;

/**
 * Google maps widget for showing google map
**/
class GoogleMaps extends Widget
{

	/**
	 * @var mixed view of current section
	**/
	public $view;

	/**
	 * @var mixed requested new map registration for current model
	**/
	public $model;

	/**
	 * @var bool param allowing hide searching fields
	**/
	public $hideUI = false;

	/**
	 * @var string google map type
	**/
	public $type = 'marker';

	/**
	 * @var array the locations
	**/
	protected $locations = [
		'lat' => 41.2994958,
		'lng' => 69.24007340000003
	];

	/**
	 * @inheritdoc
	 */
	public function init()
	{
		parent::init();
	}

	/**
	 * rendering google map
	 * @return string html
	**/
	public function run()
	{

		return $this->render('google-map', [
			'view'      => $this->view,
			'locations' => isset( $this->model->lat )
				&& isset( $this->model->lng )
				&& ! empty( $this->model->lat )
				&& ! empty( $this->model->lng )
				? [
					'lat' => $this->model->lat,
					'lng' => $this->model->lng
				]
				: $this->locations,
			'hideUI' => $this->hideUI,
			'type'	 => $this->type
		] );

	}

}