<?php namespace backend\widgets;

/**************************************/
/*                                    */
/*       MEDIA POPUP WIDGET           */
/*                                    */
/**************************************/

use Yii;

use yii\base\Widget;
use yii\helpers\ArrayHelper;

use common\models\FilesSearch;

/**
 * Media Popup widget for showing upload and select image form to page
**/
class MediaPopup extends Widget
{

	/**
	 * @var mixed current model upload form
	*/
    public $uploadForm;

	/**
	 * @var mixed upload form type
	**/
	public $formType = "avatar-modal";

    /**
     * @inheritdoc
    **/
    public function init()
    {
        parent::init();
    }

    /**
     * rendering media popup window
     * @return mixed
    **/
    public function run()
    {

		//params initializations
		$searchModel = new FilesSearch();
		$link 		 = isset( \Yii::$app->request->get()[ 'id' ] ) && Yii::$app->controller->action->id !== 'index'
			? Yii::$app->controller->action->id . '?id=' . \Yii::$app->request->get()[ 'id' ]. '&'
			: Yii::$app->controller->action->id . '?';

		//filtering files due to requests
		$filesProvider = $searchModel->search( ArrayHelper::merge(
			Yii::$app->request->queryParams, [
				$searchModel->formName() => isset( \Yii::$app->request->get()[ 'media-type' ] )
					? [
						'type' 		=> \Yii::$app->request->get()[ 'media-type' ],
						'isDeleted' => false
					]
					: [
						'isDeleted' => false
					]
			]
		) );

    	//set elements per page
		$filesProvider->pagination->pageSize = 18;

		return $this->render('media-popup', [
			'filesProvider' => $filesProvider,
			'uploadForm'    => $this->uploadForm,
			'formType'	    => $this->formType,
			'currentUrl'    => isset( \Yii::$app->request->get()[ 'id' ] ) && Yii::$app->controller->action->id === 'index'
				? Yii::$app->controller->action->controller->id . '?id=' . \Yii::$app->request->get()[ 'id' ]. '&'
				: Yii::$app->controller->action->id === 'index'
					? Yii::$app->controller->action->controller->id . '?'
					: $link,
		] );

    }

}