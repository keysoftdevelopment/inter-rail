<?php namespace backend\widgets;

/**************************************/
/*                                    */
/*       EXPENSES FORM WIDGET         */
/*                                    */
/**************************************/

use Yii;

use yii\base\Widget;
use yii\helpers\ArrayHelper;

use common\models\Logistics;
use common\models\Settings;

/**
 * Expenses Form widget allow manipulations with expenses data
**/
class ExpensesForm extends Widget
{

	/**
	 * @inheritdoc
	**/
	public function init()
	{
		parent::init();
	}

	/**
	 * expenses popup window
	 * @return mixed
	**/
	public function run()
	{

		//authenticated user role
		$role = Yii::$app->authManager->getRolesByUser(
			Yii::$app->user->id
		);

		return $this->render( 'expenses-form', [
			'permissions' => isset( $role ) && ! empty( $role )
				? [
					'role'   => key( $role ),
					'access' => Settings::findOne( [
						'name' => key( $role ) . '_order_permissions'
					] )
				]
				: [],
			'logistics' => ! empty( Yii::$app->common->logisticTypes() )
				? array_reduce( Logistics::findAll( [
					'type' => ArrayHelper::getColumn(
						Yii::$app->common->logisticTypes(),'slug'
					)
				] ), function( $filter, $logistic ) {

					if ( ! empty( $logistic ) ) {

						$filter[ $logistic->type ][ $logistic->id ] = [
							'id'    		 => $logistic->id,
							'route' 		 => $logistic->from . '-' . $logistic->to,
							'rate'  		 => $logistic->rate,
							'transportation' => $logistic->transportation,
							'othc'  		 => isset( $logistic->othc )
								? $logistic->othc
								: 0,
							'dthc' => isset( $logistic->dthc )
								? $logistic->dthc
								: 0,
							'bl' => isset( $logistic->bl )
								? $logistic->bl
								: 0,
							'surcharge' => isset( $logistic->surcharge )
								? $logistic->surcharge
								: 0
						];

					}

					return $filter;

				}, [] )
				: [],
			'expenses' => [
				'containers_expenses' => [
					'class' => 'rate-details-container',
					'items' => [
						[
							'label' => __( 'Purchase' ),
							'key'   => 'purchase',
							'class' => 'purchase-field price-field'
						], [
							'label' => __( 'Sale' ),
							'key'   => 'sale',
							'class' => 'sale-field price-field'
						], [
							'label' => __( 'Rental' ),
							'key'   => 'rental',
							'class' => 'rental-field price-field'
						], [
							'label' => __( 'Surcharge' ),
							'key'   => 'container_surcharge',
							'class' => 'container-surcharge-field'
						], [
							'label' => __( 'Container Cost' ),
							'key'   => 'container_cost',
							'class' => 'container-cost-field total-sum'
						], [
							'label' => __( 'Container Usage' ),
							'key'   => 'deposit',
							'class' => 'deposit-field'
						]
					]
				],
				'sea_freight' => [
					'class' => 'rate-details-container',
					'items' => [
						[
							'label' => __( 'Name' ),
							'key'   => 'logistics[sea]',
							'type'  => 'order',
							'class' => 'sf-name'
						], [
							'label' => __( 'OTHC' ),
							'key'   => 'othc',
							'class' => 'othc-field price-field'
						], [
							'label' => __( 'Rate' ),
							'key'   => 'sf_rate',
							'class' => 'sf-rate-field price-field'
						], [
							'label' => __( 'DTHC' ),
							'key'   => 'dthc',
							'class' => 'dthc-field price-field'
						], [
							'label' => __( 'BL' ),
							'key'   => 'bl',
							'class' => 'bl-field price-field'
						], [
							'label' => __( 'Surcharge' ),
							'key'   => 'sf_surcharge',
							'class' => 'sf-surcharge-field price-field'
						], [
							'label' => __( 'Total' ),
							'key'   => 'total',
							'class' => 'total-sum'
						]
					]
				],
				'auto_expenses' => [
					'class' => 'rate-details-container',
					'items' => [
						[
							'label' => __( 'Name' ),
							'key'   => 'logistics[auto]',
							'type'  => 'order',
							'class' => 'auto-name'
						], [
							'label' => __( 'Rate' ),
							'key'   => 'auto_rate',
							'class' => 'auto-rate-field price-field'
						], [
							'label' => __( 'Surcharge' ),
							'key'   => 'auto_surcharge',
							'class' => 'auto-surcharge-field price-field'
						], [
							'label' => __( 'Total' ),
							'key'   => 'total',
							'class' => 'total-sum'
						]
					]
				],
				'railway_expenses' => [
					'class' => 'rf-container'
				],
				'port_forwarding_expenses' => [
					'class' => 'pf-container',
					'items' => [
						[
							'label' => __( 'Rate' ),
							'key'   => 'pf_rate'
						], [
							'label' => __( 'Commission' ),
							'key'   => 'pf_commission'
						], [
							'label' => __( 'Surcharge' ),
							'key'   => 'pf_surcharge'
						]
					]
				],
				'terminal_expenses' => [
					'class' => 'rate-details-container',
					'items' => [
						[
							'label' => __( 'Enter Date' ),
							'key'   => 'terminal_date_from',
							'class' => 'terminal-enter-date date-field'
						], [
							'label' => __( 'Export Date' ),
							'key'   => 'terminal_date_to',
							'class' => 'terminal-export-date date-field'
						], [
							'label' => __( 'Storage' ),
							'key'   => 'storage',
							'class' => 'storage-field price-field'
						], [
							'label' => __( 'Lading Crane' ),
							'key'   => 'lading',
							'class' => 'lading-crane-field price-field'
						],[
							'label' => __( 'Unloading Crane' ),
							'key'   => 'unloading',
							'class' => 'unloading-crane-field price-field'
						], [
							'label' => __( 'Export Price' ),
							'key'   => 'exp_price',
							'class' => 'export-price-field price-field'
						], [
							'label' => __( 'Import Price' ),
							'key'   => 'imp_price',
							'class' => 'import-price-field price-field'
						], [
							'label' => __( 'Surcharge' ),
							'key'   => 'terminal_surcharge',
							'class' => 'terminal-surcharge-field price-field'
						], [
							'label' => __( 'Total' ),
							'key'   => 'total',
							'class' => 'total-sum'
						]
					]
				]
			]
		] );

	}

}