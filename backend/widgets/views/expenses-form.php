<?php /***************************************************************
 *	                    EXPENSES FORM WIDGET VIEW                    *
 *             I.E ALLOW ADDING OR UPDATING EXPENSES DETAILS         *
 *********************************************************************/

use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

use common\models\Settings;
use common\models\User;

/* @var $permissions array */
/* @var $expenses array    */

//contractor role name
$contractor_role = Settings::findOne( [
	'name' => 'contractor_role'
] );

//the list of available permissions for authenticated user
$permission = ! empty( $permissions[ 'access' ] )
    ? json_decode( $permissions[ 'access' ]->description )
    : []; ?>

<div class="general-form-modal modal fade in" id="expenses-form" aria-labelledby="expenses-form-label" aria-hidden="true" role="dialog">

    <?php $form = ActiveForm::begin([
		'id'      => 'expenses-main-form',
        'options' => [
			'onsubmit' => 'return false',
			'class'    => 'need-calculation',
        ]
    ] ); ?>

	    <?= Html::hiddenInput( 'expenses_id', '', [
		    'id' => 'expenses-id'
	    ] ); ?>

        <?= Html::hiddenInput( 'Expenses[container_id]', '', [
            'id' => 'expenses-container-id'
        ] ); ?>

        <?= Html::hiddenInput( 'Expenses[order_id]', '', [
            'id' => 'expenses-order-id'
        ] ); ?>

        <?= Html::hiddenInput( 'Expenses[terminal_id]', '', [
            'id' => 'expenses-terminal-id'
        ] ); ?>

	    <?= Html::hiddenInput( 'Expenses[wagon_id]', '', [
			'id' => 'expenses-wagon-id'
		] ); ?>

        <div class="modal-dialog modal-lg">

            <div class="modal-content">

                <div class="modal-header">

                    <button class="close" data-dismiss="modal" type="button">
                        ×
                    </button>

                    <h4 class="modal-title" id="expenses-form-label">
                        <?= __(  'Expenses Details' ) ?> <span class="container-number"></span>
                    </h4>

                </div>

                <div class="modal-body">

                    <ul class="nav nav-tabs bar_tabs" role="tablist">

                        <?php if ( ( isset( $permission->expenses_container )
                            && $permission->expenses_container == 'on' ) || $permissions[ 'role' ] == 'admin'
                        ) { ?>

                            <li class="active">

                                <a href="#containers_expenses" id="container_expenses-tab" role="tab" data-toggle="tab" aria-expanded="true">
                                    <?= __(  'Container' ) ?>
                                </a>

                            </li>

						<?php } if ( ( isset( $permission->expenses_sea_freight )
							&& $permission->expenses_sea_freight == 'on' ) || $permissions[ 'role' ] == 'admin'
						) { ?>

                            <li>

                                <a href="#sea_freight" role="tab" id="sea_freight-tab" data-toggle="tab" aria-expanded="false">
                                    <?= __(  'Sea Freight' ) ?>
                                </a>

                            </li>

						<?php } if ( ( isset( $permission->expenses_auto )
							&& $permission->expenses_auto == 'on' ) || $permissions[ 'role' ] == 'admin'
						) { ?>

                            <li>

                                <a href="#auto_expenses" role="tab" id="auto_expenses-tab" data-toggle="tab" aria-expanded="false">
                                    <?= __(  'Auto' ) ?>
                                </a>

                            </li>

						<?php } if ( ( isset( $permission->expenses_railway )
							&& $permission->expenses_railway == 'on' ) || $permissions[ 'role' ] == 'admin'
						) { ?>

                            <li>

                                <a href="#railway_expenses" role="tab" id="railway_expenses-tab" data-toggle="tab" aria-expanded="false">
                                    <?= __(  'Railway' ) ?>
                                </a>

                            </li>

						<?php } if ( ( isset( $permission->expenses_port_forwarding )
							&& $permission->expenses_port_forwarding == 'on' )
                            || $permissions[ 'role' ] == 'admin'
						) { ?>

                            <li>

                                <a href="#port_forwarding_expenses" role="tab" id="port_forwarding_expenses-tab" data-toggle="tab" aria-expanded="false">
                                    <?= __(  'Port Forwarding' ) ?>
                                </a>

                            </li>

						<?php } if ( ( isset( $permission->expenses_terminal )
							&& $permission->expenses_terminal == 'on' ) || $permissions[ 'role' ] == 'admin'
						) { ?>

                            <li>

                                <a href="#terminal_expenses" role="tab" id="terminal_expenses-tab" data-toggle="tab" aria-expanded="false">
                                    <?= __(  'Terminal' ) ?>
                                </a>

                            </li>

						<?php } if ( ( isset( $permission->expenses_invoices )
							&& $permission->expenses_invoices == 'on' ) || $permissions[ 'role' ] == 'admin'
						) { ?>

                            <li>

                                <a href="#invoices" role="tab" id="invoices-tab" data-toggle="tab" aria-expanded="false">
                                    <?= __(  'Invoices' ) ?>
                                </a>

                            </li>

                        <?php } ?>

                    </ul>

                    <div class="tab-content">

                        <?php foreach ( $expenses as $key => $items ) {

                            //param initialization
                            $access_key = $key == 'containers_expenses'
                                ? 'expenses_container'
                                : 'expenses_' . str_replace( '_expenses', '', $key );

                            if  ( ( isset( $permission->{$access_key} )
                                && $permission->{$access_key} == 'on' )
                                || $permissions[ 'role' ] == 'admin'
							) { ?>

                                <div role="tabpanel" class="tab-pane fade <?= $key == 'containers_expenses'
                                    ? 'active in'
                                    : ''
                                ?>" id="<?= $key ?>" aria-labelledby="<?= $key ?>">

                                    <div class="col-sm-12 col-md-12 col-xs-12 <?= $items[ 'class' ] ?> <?= in_array( $key, [ 'railway_expenses' ] ) ? $key . '-container' : '' ?>" style="padding: 0px;">

                                        <?php if ( ! in_array( $key, [ 'railway_expenses' ] )  ) {

                                            foreach ( $items[ 'items' ] as $item ) {

                                                if ( isset( $item[ 'type' ] ) && $item[ 'type' ] == 'order' ) { ?>

                                                    <div class="col-sm-12 col-md-12 col-xs-12">

                                                        <div class="form-group">

                                                            <label class="control-label" for="<?= $item[ 'key' ] ?>">
                                                                <?= $item[ 'label' ] ?>
                                                            </label>

                                                            <?= Html::dropDownList(
																strpos( $item[ 'key' ], 'sea' )
                                                                    ? 'Expenses[sf_id]'
                                                                    : 'Expenses[auto_id]',
                                                                '',
                                                                ArrayHelper::map(
                                                                    isset( $logistics[ strpos( $item[ 'key' ], 'sea' )
																			? 'sea'
																			: 'auto'
                                                                        ] )
                                                                        ? $logistics[ strpos( $item[ 'key' ], 'sea' )
																		    ? 'sea'
																		    : 'auto'
                                                                        ]
                                                                        : [],
                                                                    'id',
																	function( $model ) {
																		return $model[ 'route'] . '(' . __( $model[ 'transportation' ] ) . ')';
																	}
                                                                ), [
                                                                    'class'        => 'form-control select2-tags' . ( strpos( $item[ 'key' ], 'sea' )
                                                                        ? 'sf_id-field'
                                                                        : 'auto_id-field'
                                                                    ),
                                                                    'prompt'       => __( 'Route' ),
																	'data-details' => json_encode( $logistics ),
																	'style'        => 'width:100%',
																	'data-msg'     => json_encode( [
																		'option'    => __( 'Choose Route' ),
																		'no_result' => __( 'No result found' )
																	] )
                                                                ]
                                                            ); ?>

                                                        </div>

                                                    </div>

                                                <?php } else { ?>

                                                    <div class="col-sm-4 col-md-4 col-xs-12">

                                                        <div class="form-group">

                                                            <label class="control-label" for="<?= $item[ 'key' ] ?>">
                                                                <?= $item[ 'label' ] ?>
                                                            </label>

                                                            <?= Html::textInput(
                                                                $item[ 'key' ] !== 'total'
                                                                    ? 'Expenses[' . $item[ 'key' ] . ']'
                                                                    : '',
                                                                '', [
                                                                    'class' => in_array( $item[ 'key' ], [ 'terminal_date_from', 'terminal_date_to' ] )
                                                                        ? 'form-control ' . $item[ 'class' ]
                                                                        : 'form-control expenses-price-field ' . $item[ 'class' ],
                                                                    'placeholder' => $item[ 'label' ],
                                                                ]
                                                            ); ?>

                                                        </div>

                                                    </div>

                                                <?php }

                                            }

                                        } ?>

                                    </div>

                                    <?php if ( in_array( $key, [ 'railway_expenses' ] )  ) { ?>

                                        <div class="col-sm-12 col-md-12 col-xs-12 railway-expenses">

                                            <div class="col-sm-6 col-md-6 col-xs-12">

                                                <div class="form-group">

                                                    <?= Html::textInput(
                                                        'Expenses[rf_commission]',
                                                        '', [
                                                            'class'       => 'form-control expenses-price-field',
                                                            'placeholder' => __( 'Commission' ),
                                                            'style'       => '    background: none; border: 0px; border-bottom: 1px solid #e6e6e6; padding-left: 0px;'
                                                        ]
                                                    ); ?>

                                                </div>

                                            </div>

                                            <div class="col-sm-6 col-md-6 col-xs-12">

                                                <div class="form-group">

                                                    <?= Html::textInput(
                                                        'Expenses[rf_rental]',
                                                        '', [
                                                            'class'       => 'form-control expenses-price-field',
                                                            'placeholder' => __( 'Rental' ),
                                                            'style'       => '    background: none; border: 0px; border-bottom: 1px solid #e6e6e6; padding-left: 0px;'
                                                        ]
                                                    ); ?>

                                                </div>

                                            </div>

                                        </div>

                                        <script id="<?= $key ?>-template" type="text/html">

                                            <?= $this->render( 'templates/railway', [
                                                'logistics' => isset( $logistics[ 'railway' ] )
                                                    ? $logistics[ 'railway' ]
                                                    : []
                                            ] ); ?>

                                        </script>

                                    <?php } ?>

                                </div>

                            <?php }
                        } if ( ( isset( $permission->expenses_invoices )
							&& $permission->expenses_invoices == 'on' )
                            || $permissions[ 'role' ] == 'admin'
                        ) {

                            //the list of contr agent users
							$contractors = ArrayHelper::map( ( empty( $contractor_role )
								? []
								: User::find()
									-> where( [
										'id'       => Yii::$app->authManager->getUserIdsByRole( $contractor_role->description ),
										'isDeleted' => false
									] )
									-> all()
							),'id', 'username' ); ?>

                            <div role="tabpanel" class="tab-pane fade" id="invoices" aria-labelledby="invoices" style="padding: 0px;">

                                <?php if ( ( isset( $permission->expenses_invoices_add_delete_options )
									&& $permission->expenses_invoices_add_delete_options == 'on' )
								|| $permissions[ 'role' ] == 'admin'
								) { ?>

                                    <a href="javascript:void(0)" class="add-item add-invoice" data-type="invoice">
                                        <?= __( 'Add invoice' ) ?>
                                    </a>

                                <?php } ?>

                                <div class="col-sm-12 col-md-12 col-xs-12 invoice-container"></div>

                                <script id="invoice-template" type="text/html">

                                    <?= $this->render( 'templates/invoice', [
                                        'is_full_access' => ( ( isset( $permission->expenses_invoices_add_delete_options )
												&& $permission->expenses_invoices_add_delete_options == 'on' )
											|| $permissions[ 'role' ] == 'admin' )
                                            ? true
                                            : false,
                                        'logistics' => isset( $logistics[ 'railway' ] )
                                            ? $logistics[ 'railway' ]
                                            : [],
                                        'contractors' => $contractors
                                    ] ); ?>

                                </script>

                            </div>

                        <?php } ?>

                    </div>

                </div>

                <div class="modal-footer aligncenter">

					<?php if ( ( isset( $permission->expenses_general )
						&& $permission->expenses_general == 'on' )
					|| $permissions[ 'role' ] == 'admin'
					) { ?>

                        <div class="col-sm-3 col-md-3 col-xs-12">

                            <label class="col-sm-8 col-md-8 col-xs-12">
                                <i class="fa fa-money"></i>
                                <?= __( 'THC') ?>
                            </label>

							<?= Html::textInput(
								'Expenses[thc]',
								0, [
									'class' => 'form-control expenses-price-field',
								]
							); ?>

                        </div>

                        <div class="col-sm-3 col-md-3 col-xs-12">

                            <label class="col-sm-8 col-md-8 col-xs-12">
                                <i class="fa fa-money"></i>
                                <?= __( 'Tracking') ?>
                            </label>

							<?= Html::textInput(
								'Expenses[tracking]',
								0, [
									'class' => 'form-control expenses-price-field',
								]
							); ?>

                        </div>

                        <div class="col-sm-4 col-md-4 col-xs-12">

                            <label class="col-sm-8 col-md-8 col-xs-12">
                                <i class="fa fa-money"></i>
                                <?= __( 'Surcharges' ) ?>
                            </label>

							<?= Html::textInput(
								'Expenses[surcharge]',
								0, [
									'class' => 'form-control expenses-price-field',
								]
							); ?>

                        </div>

                    <?php } ?>

                    <div class="col-sm-2 col-md-2 col-xs-12" style="margin-top: 10px;">

                        <a href="javascript:void(0)">
                            <i class="fa fa-spinner"></i>
                            <?= __(  'Update') ?>
                        </a>
                    </div>

                </div>

            </div>

        </div>

	<?php ActiveForm::end(); ?>

</div>
