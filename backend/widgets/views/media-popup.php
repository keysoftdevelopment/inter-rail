<?php /***************************************************************
 *	                     MEDIA POPUP WIDGET VIEW                     *
 *               I.E FOR UPLOAD AND SELECT CUSTOM FILES              *
 *********************************************************************/

use yii\widgets\ListView;
use yii\widgets\Pjax;

/** @var $filesProvider yii\data\ActiveDataProvider */
/** @var $uploadForm mixed                          */
/** @var $currentUrl string                         */
/** @var $formType string                           */ ?>

<div class="media-popup-modal modal fade in" id="<?= $formType ?>" aria-hidden="true" aria-labelledby="<?= $formType ?>-label" role="dialog" tabindex="-1">

    <div class="modal-dialog modal-lg">

        <div class="modal-content">

            <div class="modal-header">

                <button class="close" data-dismiss="modal" type="button">
                    ×
                </button>

                <h4 class="modal-title" id="<?= $formType ?>-label">
                    <?= __(  'Change Image' ) ?>
                </h4>

            </div>

            <div class="modal-body" style="padding: 0px;">

                <ul class="nav nav-tabs bar_tabs" role="tablist">

                    <li class="active">

                        <a href="#upload_tab" id="upload-tab" role="tab" data-toggle="tab" aria-expanded="true">
                            <?= __(  'Upload' ) ?>
                        </a>

                    </li>

                    <li>

                        <a href="#media_list" role="tab" id="media-lies" data-toggle="tab" aria-expanded="false">
                            <?= __(  'Media' ) ?>
                        </a>

                    </li>

                </ul>

                <div class="tab-content">

                    <div role="tabpanel" class="tab-pane fade active in" id="upload_tab" aria-labelledby="upload-tab" style="margin-bottom: 15px; padding-top: 0px;">

                        <div class="upload-new-image">
                            <?= $uploadForm ?>
                        </div>

                    </div>

                    <div role="tabpanel" class="tab-pane fade" id="media_list" aria-labelledby="media-lies">

                        <div class="list-of-files">

                            <div class="container">

                                <?php Pjax::begin( [
                                    'id'                 => 'media_popup_pjax',
                                    'enablePushState'    => false,
                                    'enableReplaceState' => false,
                                    'timeout'            => 10000
                                ] ); ?>

                                    <div class="tabs col-md-2 col-sm-12 col-xs-12">

                                        <ul>

                                            <li>

                                                <a href="<?= $currentUrl ?>">
                                                    <?= __( 'All' ) ?>
                                                </a>

                                            </li>

                                            <li>

                                                <a href="<?= $currentUrl . 'media-type=pages' ?>">
                                                    <?= __( 'Pages' ) ?>
                                                </a>

                                            </li>

                                            <li>

                                                <a href="<?= $currentUrl . 'media-type=posts' ?>">
                                                    <?= __( 'Posts' ) ?>
                                                </a>

                                            </li>

                                            <li>

                                                <a href="<?= $currentUrl . 'media-type=services' ?>">
                                                    <?= __( 'Services' ) ?>
                                                </a>

                                            </li>

                                            <li>

                                                <a href="<?= $currentUrl . 'media-type=sliders' ?>">
                                                    <?= __( 'Sliders' ) ?>
                                                </a>

                                            </li>

                                            <li>

                                                <a href="<?= $currentUrl . 'media-type=categories' ?>">
                                                    <?= __( 'Categories' ) ?>
                                                </a>

                                            </li>

                                        </ul>

                                    </div>

                                    <div class="col-md-10 col-sm-12 col-xs-12 aligncenter" style="padding: 0px;">

                                        <?= ListView::widget( [

                                            'dataProvider' => $filesProvider,
                                            'options'      => [
                                                'tag'   => 'div',
                                                'class' => 'list-media-files'
                                            ],

                                            'itemOptions' => [
                                                'tag' => false
                                            ],

                                            'itemView' => function ( $model, $key, $index ) {

                                                return $this->render( 'templates/media-files', [
                                                    'model' => $model
                                                ] );

                                            },
                                            'summary' => '',
                                            'pager'   => [
                                                'options' => [
                                                    'class' => 'pagination'
                                                ]
                                            ],
                                            'layout' => '<div class="panel panel-default" style="border-radius: 0px;">
            
                                                <div class="panel-body">
                                                    {items}
                                                </div>
                                                                
                                                <div class="panel-footer">
                                                                
                                                    <div class="pagination-container">
                                                        {pager}
                                                    </div>
                                                                    
                                                    <div class="clearfix"></div>
                                                                    
                                                </div>
                                                                
                                            </div>'

                                        ] ); ?>

                                    </div>

                                <?php Pjax::end(); ?>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

            <div class="modal-footer aligncenter">

                <button class="btn btn-default image-save" data-dismiss="modal" type="button">
                    <?= __(  'Save') ?>
                </button>

            </div>

        </div>

    </div>

</div>
