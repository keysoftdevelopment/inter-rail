<?php /***************************************************************
 *	    		 	   	  SINGLE FILE SECTION VIEW                   *
 *********************************************************************/

/** @var $model common\models\Files */

//current file extension due to it`s path
$file_type = pathinfo(Yii::getAlias( '@frontend_link' ) . $model->guide, PATHINFO_EXTENSION ); ?>

<div class="col-md-2 col-sm-3 col-xs-6 image-select-container" data-id="<?= $model->id ?>" data-src="<?= Yii::getAlias( '@frontend_link' ) . $model->guide ?>">

    <div class="img-chosen">
        <i class="fa fa-plus-circle"></i>
    </div>

	<?php if ( in_array( $file_type, [ 'zip', 'xlsx', 'xls', 'word', 'sql' ] ) ) { ?>
        <i class="fa fa-file"></i>
	<?php } else { ?>
        <img src="<?= Yii::getAlias( '@frontend_link' ) . $model->guide ?>" />
	<?php } ?>

</div>