<?php /***************************************************************
 *	 	    		     INVOICE DETAILS TEMPLATE 				     *
 *********************************************************************/

use yii\helpers\Html;

/* @var $is_full_access boolean            */
/* @var $contractors common\models\User[] */ ?>

<div class="expenses-option-section">

	<div class="col-md-3 col-sm-3 col-xs-12">

		<div class="form-group">

			<label class="control-label" for="route">
				<?= __( 'Number' ) ?>
			</label>

			<?= Html::textInput(
				'invoices[number][]',
				'',
				[
					'class'       => 'invoice-number-field form-control',
					'placeholder' => __( 'Number' )
				]
			) ?>

		</div>

	</div>

	<div class="col-md-3 col-sm-3 col-xs-12">

		<div class="form-group">

			<label class="control-label" for="rate">
				<?= __( 'Date' ) ?>
			</label>

			<?= Html::textInput(
				'invoices[date][]',
				'',
				[
					'class'       => 'form-control invoice-date',
					'placeholder' => __( 'Date' )
				]
			); ?>

		</div>

	</div>

	<div class="col-md-3 col-sm-3 col-xs-12">

		<div class="form-group">

			<label class="control-label" for="surcharge">
				<?= __( 'Customer' ) ?>
			</label>

			<?= Html::dropDownList(
				'invoices[customer_id][]',
				'',
				$contractors, [
					'class'    => 'user-id-field form-control',
					'data-msg' => __( 'Choose Customer' )
				]
			) ?>

		</div>

	</div>

	<div class="col-md-3 col-sm-3 col-xs-12">

		<div class="form-group">

			<label class="control-label" for="rate">
				<?= __( 'Amount' ) ?>
			</label>

			<?= Html::textInput(
				'invoices[amount][]',
				'',
				[
					'class'       => 'form-control amount-field',
					'placeholder' => __( 'Amount' )
				]
			); ?>

		</div>

	</div>

    <?php if ( $is_full_access ) { ?>

        <div class="remove-current-option remove-expenses-option" data-parent="expenses-option-section" data-translations="<?= htmlentities( json_encode( [
			'title'   => __( 'CONFIRMATION MODULE' ),
			'message' => __( 'Do you really want to delete this option?' )
		] ) ) ?>">
            <i class="fa fa fa-close"></i>
        </div>

    <?php } ?>

</div>
