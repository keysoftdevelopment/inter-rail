<?php /***************************************************************
 *	 	    		     RAILWAY DETAILS TEMPLATE 				     *
 *********************************************************************/

use yii\helpers\Html; ?>

<div class="expenses-option-section">

    <div class="col-md-3 col-sm-3 col-xs-12">

        <label class="control-label" for="route">
			<?= __( 'Route' ) ?>
        </label>

		<?= Html::textInput(
			'rf_details[route][]',
			'',
			[
				'class'       => 'route-field form-control',
				'readonly'    => true,
				'placeholder' => __( 'Route' )
			]
		) ?>

    </div>

    <div class="col-md-3 col-sm-3 col-xs-12">

        <label class="control-label" for="route">
			<?= __( 'Code' ) ?>
        </label>

        <?= Html::textInput(
			'rf_details[code][]',
            '', [
				'class'       => 'code-field form-control',
				'placeholder' => __( 'Code' )
            ]
        ) ?>

		<?= Html::dropDownList(
			'rf_details[code][]',
			'',
			[],
			[
				'class'       => 'code-field form-control',
				'placeholder' => __( 'Code' )
			]
		) ?>

    </div>

	<div class="col-md-3 col-sm-3 col-xs-12">

        <label class="control-label" for="rate">
            <?= __( 'Rate' ) ?>
        </label>

        <?= Html::textInput(
            'rf_details[rate][]',
            '',
            [
                'class'       => 'rate-field form-control expenses-price-field',
                'placeholder' => __( 'Rate' )
            ]
        ) ?>

	</div>

	<div class="col-md-3 col-sm-3 col-xs-12">

        <label class="control-label" for="surcharge">
            <?= __( 'Surcharge' ) ?>
        </label>

        <?= Html::textInput(
            'rf_details[surcharge][]',
            '',
            [
                'class'       => 'surcharge-field form-control expenses-price-field',
                'placeholder' => __( 'Surcharge' )
            ]
        ); ?>

	</div>

</div>