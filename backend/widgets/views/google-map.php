<?php /***************************************************************
 *	    		 	   	  WIDGET VIEW FOR MAP SECTION                *
 *********************************************************************/

use common\models\Settings;

use backend\assets\AppAsset;

/* @var $view yii\web\View */
/* @var $locations array   */
/* @var $hideUI bool       */
/* @var $type string       */

//google map api key
$gmap_api_key = Settings::findOne( [
    'name' => 'gmap_api_key'
] );

if ( ! is_null( $gmap_api_key ) ) :

    //registering scripts for google maps
    foreach ( [
        'https://maps.googleapis.com/maps/api/js?libraries=places,geometry&key='. $gmap_api_key->description,
        '@web/js/map/gmaps-markerwithlabel.min.js',
        '@web/js/map/distance.min.js'
    ] as $js_file )
        $view->registerJsFile(
            $js_file, [
                'depends' => [
                    AppAsset::className()
                ]
            ]
        );

    if ( ! $hideUI ) { ?>

        <div class="form-group" style="margin-bottom: 10px;">
            <input type="text" id="location-name" class="form-control disable-form-update" placeholder="<?= __( 'ENTER LOCATION' ) ?>">
        </div>

    <?php } ?>

    <div id="map" data-details="<?= htmlspecialchars( json_encode( [
        'lat'  => $locations[ 'lat' ],
        'lng'  => $locations[ 'lng' ],
        'type' => $type
    ] ) ); ?>"></div>

<?php endif; ?>