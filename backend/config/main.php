<?php

use yii\helpers\ArrayHelper;

$params = ArrayHelper::merge(
    require( __DIR__ . '/../../common/config/params.php' ),
    require( __DIR__ . '/params.php' )
);

return [
    'id'                  => 'app-backend',
    'basePath'            => dirname( __DIR__ ),
    'controllerNamespace' => 'backend\controllers',
    'defaultRoute'        => 'dashboard',
	'sourceLanguage'	  => 'en',
    'language'            => 'ru',
    'bootstrap'           => [ 'log' ],
    'modules'             => [ ],
    'components'          => [

        'db' => require( dirname( __DIR__ ) . '/../common/config/db.php' ),

        'request' => [
            'csrfParam' => '_csrf-backend'
        ],

		'assetManager' => [
			'class'   => 'yii\web\AssetManager',
			'bundles' => [

				'yii\web\JqueryAsset' => [
					'js' => [
						Yii::getAlias( '@frontend_link' ) . '/js/jquery.min.js'
					]
				],

				'yii\bootstrap\BootstrapAsset' => [
					'css' => [
						YII_ENV_DEV
							? 'css/bootstrap.css'
							: 'css/bootstrap.min.css'
					]
				],

				'yii\bootstrap\BootstrapPluginAsset' => [
					'js' => [
						YII_ENV_DEV
							? 'js/bootstrap.js'
							: 'js/bootstrap.min.js'
					]
				]

			]

		],

        'user' => [
            'identityClass'   => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie'  => [
                'name'     => '_identity-backend',
                'httpOnly' => true
            ],

            'loginUrl' => [
                'dashboard/login'
            ]
        ],

        'common' => [
            'class' => 'backend\components\Common'
        ],

		'excel_export' => [
			'class' => 'backend\components\ExcelExportComponent'
		],

        'session' => [
            'name' => 'interrail-backend'
        ],

        'log'          => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets'    => [
                [
                    'class'  => 'yii\log\FileTarget',
                    'levels' => [
                    	'error',
						'warning'
					]
                ]
            ]
        ],

        'errorHandler' => [
            'errorAction' => 'dashboard/error'
        ],

        'i18n'         => [
            'translations' => [
                'app*' => [
                    'class'   => 'yii\i18n\PhpMessageSource',
                    'fileMap' => [
                        'app'       => 'app.php',
                        'app/error' => 'error.php'
                    ]
                ]
            ]
        ]
    ],

	'as locale' => [
		'class' => 'common\behaviors\LocaleBehavior',
	],

    'params' => $params

];