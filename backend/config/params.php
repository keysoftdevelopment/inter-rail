<?php return [
    'adminEmail'  => 'admin@example.com',
    'maxPageSize' => 20,
    'templates'   => [
        'default'  => 'Default',
        'home'     => 'Home Page',
		'contact'  => 'Contact Page',
		'post' 	   => 'Posts Page',
		'services' => 'Services Page',
		'checkout' => 'CheckOut Page'
    ],

    'menu' => [
        [
			'action' 	  => 'dashboard',
			'icon'   	  => 'fa-pie-chart',
            'name'   	  => 'Dashboard',
            'permissions' => [ 'dashboard' ],
            'items'  	  => [
                [
                    'label' 	  => 'Dashboard',
					'permissions' => [ 'dashboard' ],
                    'url'   	  => [ '/' ]
                ]
            ]
        ], [
			'action' 	  => 'blockTrain',
			'icon'   	  => 'fa-train',
			'name'   	  => 'Trains',
			'permissions' => [
				'blockTrain',
				'codes',
				'trains'
			],
			'items' => [
				[
					'label' 	  => 'Block Train',
					'permissions' => [ 'blockTrain' ],
					'url'   	  => [
						'/logistics?type=blockTrain'
					]
				], [
					'label' 	  => 'Train',
					'permissions' => [ 'train' ],
					'url'   	  => [
						'/logistics?type=train'
					]
				], [
					'label' 	  => 'Codes',
					'permissions' => [ 'codes' ],
					'url'   	  => [
						'/railway-codes'
					]
				]
			]
		], [
			'action' 	  => 'containers',
			'icon'   	  => 'fa-tasks',
			'name'   	  => 'Containers',
			'permissions' => [ 'containers' ],
			'items'  	  => [
				[
					'label' 	  => 'All Containers',
					'permissions' => [ 'containers' ],
					'url'   	  => [
						'/containers'
					]
				], [
					'label' 	  => 'Add New',
					'permissions' => [ 'containers' ],
					'url'   	  => [
						'/containers/create'
					]
				], [
					'label'  	  => 'Types',
					'permissions' => [ 'containers' ],
					'url'    	  => [
						'/categories/types?type=containers'
					]
				]
			]
		], [
			'action' 	  => 'wagons',
			'icon'   	  => 'fa-stack-exchange',
			'name'  	  => 'Wagons',
			'permissions' => [ 'wagons', 'railway' ],
			'foreign'	  => [
				'categories' => [
					'type' => [ 'weights', 'railway' ]
				]
			],
			'items'  => [
				[
					'label' 	  => 'All Wagons',
					'permissions' => [ 'wagons' ],
					'url'   	  => [
						'/wagons'
					]
				], [
					'label' 	  => 'Add New',
					'permissions' => [ 'wagons' ],
					'url'   	  => [
						'/wagons/create'
					]
				], [
					'label'  	  => 'Weights',
					'permissions' => [ 'wagons' ],
					'url'    	  => [
						'/categories/types?type=weights'
					]
				], [
					'label' 	  => 'Categories',
					'permissions' => [ 'railway' ],
					'url'   	  => [
						'/categories?type=railway'
					]
				]
			]
		], [
			'action' 	  => 'logistics',
			'icon'   	  => 'fa-truck',
			'name'   	  => 'Transportation',
			'permissions' => [ 'auto', 'logistics', 'sea', 'railway', 'routes' ],
			'foreign'	  => [
				'categories' => [
					'type' => [ 'cargo' ]
				]
			],
			'items'  => [
				[
					  'label' 	    => 'Places',
					  'permissions' => [ 'routes' ],
					  'url'   	    => [
						  '/places'
					  ]
				], [
					'label' 	  => 'Settings',
					'permissions' => [ 'logistics' ],
					'url'   	  => [
						'/logistics/settings'
					]
				], [
					'label'  	  => 'Cargo Categories',
					'permissions' => [ 'logistics' ],
					'url'   	  => [
						'/categories?type=cargo'
					]
				], [
					'label'  	  => 'Types',
					'permissions' => [ 'logistics' ],
					'url'    	  => [
						'/categories/types?type=logistics'
					]
				]
			]
		], [
			'action' 	  => 'routes',
			'icon'   	  => 'fa-globe',
			'name'   	  => 'Routes',
			'permissions' => [ 'routes' ],
			'items'  	  => [
				[
					'label' 	  => 'All routes',
					'permissions' => [ 'routes' ],
					'url'  	      => [
						'/routes'
					]
				], [
					'label' 	  => 'Add New',
					'permissions' => [ 'routes' ],
					'url'   	  => [
						'/routes/create'
					]
				]
			]
		], [
			'action' 	  => 'terminals',
			'icon'   	  => 'fa-building-o',
			'name'   	  => 'Terminals',
			'permissions' => [ 'terminals' ],
			'items'  	  => [
				[
					'label' 	  => 'All terminals',
					'permissions' => [ 'terminals' ],
					'url'   	  => [
						'/terminals'
					]
				], [
					'label' 	  => 'Add New',
					'permissions' => [ 'terminals' ],
					'url'   	  => [
						'/terminals/create'
					]
				], [
					'label' 	  => 'Terminals Storage',
					'permissions' => [ 'terminals' ],
					'url'   	  => [
						'/terminals/storage'
					]
				]
			]
		], [
			'action' 	  => 'orders',
			'icon'   	  => 'fa-list-ul',
            'name'   	  => 'Orders',
            'permissions' => [ 'expenses', 'invoices', 'orders', 'ownOrders', 'rates' ],
            'items' => [
                [
                    'label' 	  => 'All Orders',
					'permissions' => [ 'orders', 'ownOrders' ],
                    'url'   	  => [
                    	'/orders'
					]
                ], [
					'label' 	  => 'Expenses',
					'permissions' => [ 'expenses' ],
					'url'   	  => [
						'/expenses'
					]
				], [
					'label' 	  => 'Invoices',
					'permissions' => [ 'invoices' ],
					'url'   	  => [
						'/invoices'
					]
				], [
				   'label' 	     => 'Rates',
				   'permissions' => [ 'rates' ],
				   'url'   	     => [
					   '/rates'
				   ]
				]
            ]
        ], [
			'action' 	  => 'sliders',
			'icon'   	  => 'fa-slideshare',
			'name'   	  => 'Sliders',
			'permissions' => [ 'pages', 'posts' ],
			'items'  	  => [
				[
					'label' 	  => 'All Sliders',
					'permissions' => [ 'pages', 'posts' ],
					'url'   	  => [
						'/sliders'
					]
				], [
					'label' 	  => 'Add New',
					'permissions' => [ 'pages', 'posts' ],
					'url'   	  => [
						'/sliders/create'
					]
				]
			]
		], [
			'action' 	  => 'files',
			'icon'   	  => 'fa-film',
            'name'   	  => 'Media',
            'permissions' => [ 'media' ],
            'items'  	  => [
                [
                    'label' 	  => 'Library',
					'permissions' => [ 'media' ],
                    'url'   	  => [
                    	'/files'
					]
                ]
            ]
        ], [
			'action' 	  => 'services',
			'icon'   	  => 'fa-tty',
			'name'   	  => 'Services',
			'permissions' => [ 'pages' ],
			'items'  	  => [
				[
					'label' 	  => 'All Services',
					'permissions' => [ 'pages' ],
					'url'   	  => [
						'/services'
					]
				], [
					'label' 	  => 'Add New',
					'permissions' => [ 'pages' ],
					'url'   	  => [
						'/services/create'
					]
				]
			]
		], [
			'action' 	  => 'companies',
			'icon'   	  => 'fa-building-o',
			'name'   	  => 'Companies',
			'permissions' => [ 'companies' ],
			'items'  	  => [
				[
					'label' 	  => 'All Companies',
					'permissions' => [ 'companies' ],
					'url'   	  => [
						'/companies'
					]
				], [
					'label' 	  => 'Add New',
					'permissions' => [ 'companies' ],
					'url'   	  => [
						'/companies/create'
					]
				]
			]
		], [
			'action' 	  => 'pages',
			'icon'   	  => 'fa-files-o',
            'name'   	  => 'Pages',
            'permissions' => [ 'pages' ],
            'items'  	  => [
                [
                    'label' 	  => 'All Pages',
					'permissions' => [ 'pages' ],
                    'url'   	  => [
                    	'/pages'
					]
                ],
                [
                    'label' 	  => 'Add New',
					'permissions' => [ 'pages' ],
                    'url'   	  => [
                    	'/pages/create'
					]
                ]
            ]
        ], [
			'action' 	  => 'posts',
			'icon'   	  => 'fa-edit',
			'name'   	  => 'Posts',
			'permissions' => [ 'posts' ],
			'items'  	  => [
				[
					'label' 	  => 'All posts',
					'permissions' => [ 'posts' ],
					'url'   	  => [
						'/posts'
					]
				], [
					'label' 	  => 'Add New',
					'permissions' => [ 'posts' ],
					'url'   	  => [
						'/posts/create'
					]
				], [
					'label' 	  => 'Categories',
					'permissions' => [ 'posts' ],
					'url'   	  => [
						'/categories?type=posts'
					]
				]
			]
		], [
			'action' 	  => 'settings',
			'icon'   	  => 'fa-cogs',
            'name'   	  => 'Appearance',
            'permissions' => [ 'settings', 'logs' ],
            'items' 	  => [
                [
                    'label' 	  => 'Menu',
					'permissions' => [ 'settings' ],
                    'url'   	  => [
                    	'/settings/create'
					]
                ], [
					'label' 	  => 'Roles',
					'permissions' => [ 'settings' ],
					'url'   	  => [
						'/roles'
					]
				], [
					'label' 	  => 'Logs',
					'permissions' => [ 'logs' ],
					'url'   	  => [
						'/logs/'
					]
				], [
                    'label' 	  => 'Languages',
					'permissions' => [ 'settings' ],
                    'url'   	  => [
                    	'/languages/'
					]
                ], [
                    'label' 	  => 'Settings',
					'permissions' => [ 'settings' ],
                    'url'   	  => [
                    	'/settings'
					]
                ]
            ]
        ], [
			'action' 	  => 'user',
			'icon'   	  => 'fa-male',
            'name'   	  => 'Users',
            'permissions' => [ 'users', 'profile' ],
            'items'  	  => [
                [
                    'label'  	  => 'All Users',
					'permissions' => [ 'users' ],
                    'url'    	  => [
                    	'/user'
					]
                ], [
                    'label' 	  => 'Add New',
					'permissions' => [ 'users' ],
                    'url'   	  => [
                    	'/user/create'
					]
                ], [
                    'label' 	  => 'Your Profile',
					'permissions' => [ 'profile' ],
                    'url'   	  => [
                    	'/user/update'
					]
                ]
            ]
        ]
    ]
];