<?php namespace backend\controllers;

/**************************************/
/*                                    */
/*          LOGS CONTROLLER           */
/*                                    */
/**************************************/

use Yii;

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

use yii\web\Controller;

use common\models\LogsSearch;
use common\models\Orders;
use common\models\User;

/**
 * Logs Controller is the controller behind the Expenses model.
**/
class LogsController extends Controller
{

	/**
	 * @inheritdoc
	 **/
	public function behaviors()
	{

		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'actions' => [
							'index'
						],
						'allow' => true,
						'roles' => [
							'logs'
						]
					]
				]
			]

		];

	}

	/**
	 * Lists all Logs models.
	 * @return mixed
	**/
	public function actionIndex()
	{

		//params initializations for current method
		$searchModel  = new LogsSearch();
		$dataProvider = $searchModel->search( ArrayHelper::merge(
			Yii::$app->request->queryParams, [
				$searchModel->formName() => [
					'type' => 'orders'
				] ]
		) );

		return $this->render( 'index', [
			'searchModel'  => $searchModel,
			'dataProvider' => $dataProvider,
			'orders'	   => ! empty( $dataProvider->models )
				? Orders::find()
					-> where( [
						'id' => ArrayHelper::getColumn(
							$dataProvider->models, 'foreign_id'
						)
					] )
					-> indexBy( 'id' )
					-> asArray()
					-> all()
				: [],
			'users' => User::find()
				-> where( [
					'isDeleted' => false
				] )
				-> indexBy( 'id' )
				-> asArray()
				-> all()
		] );

	}

}