<?php namespace backend\controllers;

/**************************************/
/*                                    */
/*         SERVICES CONTROLLER        */
/*                                    */
/**************************************/

use Yii;

use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\filters\AccessControl;

use yii\web\NotFoundHttpException;
use yii\web\Response;

use common\behaviors\TranslationBehavior;

use common\models\Files;
use common\models\Posts;
use common\models\PostsSearch;
use common\models\Translation;
use common\models\UploadForm;


/**
 * Services Controller is the controller behind the Posts model.
**/
class ServicesController extends Controller
{

    /**
     * @inheritdoc
    **/
    public function behaviors()
    {

        return [

            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [
                        	'create',
							'update',
							'index',
							'delete',
							'upload'
						],
                        'allow' => true,
                        'roles' => [
                        	'pages'
						],
                    ],
                ],
            ],

            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => [
                    	'POST'
					]
                ]
            ],

			'translation' => [
				'class' => TranslationBehavior::className()
			]

        ];

    }

    /**
     * Lists all Services models.
     * @return mixed
    **/
    public function actionIndex()
    {

		//params initialization for current method
		$searchModel  = new PostsSearch();
        $lang_id 	  = Yii::$app->request->get( 'lang_id', null );
        $dataProvider = $searchModel->search( ArrayHelper::merge(
            Yii::$app->request->queryParams, [
            	$searchModel->formName() => [
                	'type'    => 'service',
                	'lang_id' => is_null( $lang_id )
						? Yii::$app->common->currentLanguage()
						: $lang_id
            ] ]
        ) );

        return $this->render( 'index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
            'thumbnails'   => ! empty( $dataProvider->models )
				? Yii::$app->common->thumbnails( ArrayHelper::getColumn(
					$dataProvider->models, 'file_id'
				) )
				: [],
			'is_models_translated' => ! empty( $dataProvider->models )
				? Translation::find()
					-> select( [
						'lang_id',
						'foreign_id'
					] )
					-> where( [
						'foreign_id' => ArrayHelper::getColumn(
							$dataProvider->models, 'id'
						),
						'type' => 'service'
					] )
					-> asArray()
					-> all()
				: []
        ] );

    }

    /**
     * Creates a new service.
     * If creation is successful, the browser will be redirected to the 'index' page.
     * @return mixed
    **/
    public function actionCreate()
    {

		//params initialization for current method
        $model = new Posts();

		//create new item for current model due to requested details
        if ( $model->load( Yii::$app->request->post() )
			&& $model->save()
		) {

			//set success response message
			Yii::$app->session->setFlash( 'success',
				__( 'Service was created successfully' )
			);

            return $this->redirect( [
                'index'
            ] );

        }

		return $this->render( 'create', [
			'model' => $model
		] );

    }

    /**
     * Updates an existing service due to requested service id.
     * If update is successful, the browser will be redirected to the 'index' page.
     * @param integer $id, integer $lang_id
     * @return mixed
    **/
    public function actionUpdate( $id, $lang_id = 0 )
    {

		//params initialization for current method
        $model = $this->findModel( $id );

		//updating current model with requested details
        if ( $model->load( Yii::$app->request->post() )
			&& $model->save()
		) {

			//set success response message
			Yii::$app->session->setFlash( 'success',
				__( 'Service was updated successfully' )
			);

            return $this->redirect( [
                'index'
            ] );

        }

		//fill current page with additional information
		$model->customFields();

		return $this->render( 'update', [
			'model' => $this->translates( $model, [
				'lang_id' => $lang_id > 0
					? $lang_id
					: $model->lang_id,
				'type'	    => 'service',
				'is_single' => true
			] ),
			'lang_id' => $lang_id > 0
				? $lang_id
				: $model->lang_id
		] );

    }

	/**
	 * Upload new file.
	 * If update is successful, action will return object with file id and url.
	 * @return mixed
	**/
	public function actionUpload()
	{

		//initializations params for current method
		$request = Yii::$app->request;
		$model 	 = new UploadForm( [
			'path' => '@frontend/web/upload/services',
			'url'  => '@frontend_web/upload/services'
		] );

		//current request is ajax
		if ( $request->isAjax ) {

			//loading requested file details
			$model->load( $request->post() );

			//change response type to json for ajax callback
			Yii::$app->response->format = Response::FORMAT_JSON;

			//uploading requested file
			if ( $model->upload() ) {

				//file details due to requested file id
				$file = Files::findOne(
					$request->post('file_id' )
				);

				//is requested file exists
				if( is_null( $file ) )
					$file = new Files();

				//updating file details
				$file = merge_objects( $file, [
					'name'  => uniqid(),
					'guide' => $model->getUrl(),
					'size'	=> (string)$model->image->size,
					'type'	=> 'services'
				] );

				//saving file
				if ( $file->save() ) {

					return [
						'id'  => $file->id,
						'url' => Yii::getAlias( '@frontend_link' ) . $file->guide
					];

				}

			} else {

				return [
					'return' => json_encode(
						$model->errors
					)
				];

			}
		}

		return $this->redirect( [
			'create'
		] );

	}

    /**
     * Deletes an existing service due to requested service id.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id, integer $lang_id
     * @return mixed
    **/
    public function actionDelete( $id )
    {

		//set success response message
        if ( $this->findModel( $id )->delete() ) {

			Yii::$app->session->setFlash( 'success',
				__( 'Service was deleted successfully' )
			);

        }

        return $this->redirect( [
            'index'
        ] );

    }

	/**
	 * Finds the Posts model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return Posts the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	**/
	protected function findModel( $id )
	{

		if ( ( $model = Posts::findOne( $id ) ) !== null ) {
			return $model;
		} else {

			throw new NotFoundHttpException(
				__( 'The requested page does not exist' )
			);

		}

	}

}