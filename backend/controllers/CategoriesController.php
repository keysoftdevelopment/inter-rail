<?php namespace backend\controllers;

/**************************************/
/*                                    */
/*   	 CATEGORIES CONTROLLER    	  */
/*                                    */
/**************************************/

use Yii;

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Response;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

use common\behaviors\TranslationBehavior;

use common\models\Files;
use common\models\Settings;
use common\models\Taxonomies;
use common\models\TaxonomiesSearch;
use common\models\Translation;
use common\models\UploadForm;

/**
 * Categories Controller is the controller behind the Taxonomies model.
**/
class CategoriesController extends Controller
{

    /**
     * @inheritdoc
    **/
    public function behaviors()
    {

        return [

            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [
                        	'create',
							'update',
							'index',
							'types',
							'delete',
							'upload',
							'sorter'
						],
                        'allow' => true,
                        'roles' => [
                        	'posts'
						]
                    ], [
						'actions' => [
							'create',
							'update',
							'types',
							'delete'
						],
						'allow' => true,
						'roles' => [
							'containers'
						]
					], [
						'actions' => [
							'create',
							'update',
							'types',
							'delete'
						],
						'allow' => true,
						'roles' => [
							'wagons'
						]
					], [
						'actions' => [
							'create',
							'update',
							'index',
							'delete',
							'sorter'
						],
						'allow' => true,
						'roles' => [
							'railway'
						]
					], [
						'actions' => [
							'create',
							'update',
							'index',
							'types',
							'delete',
							'sorter'
						],
						'allow' => true,
						'roles' => [
							'blockTrain', 'logistics', 'sea'
						]
					]
                ]
            ],

            'verbs' => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => [
                    	'POST'
					]
                ]
            ],

			'translation' => [
				'class' => TranslationBehavior::className()
			]

        ];

    }

	/**
	 * @inheritdoc
	**/
	public function beforeAction( $action )
	{

		//disable validation due to action id
		if ( $action->id == 'update' )
			$this->enableCsrfValidation = false;

		return parent::beforeAction(
			$action
		);

	}

	/**
	 * Lists all Categories models.
	 * @param string $type
	 * @return mixed
	 * @throws NotFoundHttpException
	**/
    public function actionIndex( $type = 'posts' )
    {

    	//current user permissions
		$permissions = Yii::$app->authManager->getPermissionsByUser(
			Yii::$app->user->id
		);

		//check user access permissions
		if ( ( $type == 'cargo' &&  empty( array_intersect( [
			'logistics',
			'sea',
			'blockTrain',
		], array_keys( $permissions ) ) ) )
			|| ( $type != 'cargo' && ! in_array( $type, array_keys( $permissions ) )
		) ) {

			throw new NotFoundHttpException(
				__( 'The requested page does not exist' )
			);

		}

		//params initializations for current method
        $searchModel = new TaxonomiesSearch();

		//model details
		$dataProvider = $searchModel->search( ArrayHelper::merge(
			\Yii::$app->request->queryParams, [
				$searchModel->formName() => [
					'lang_id' => Yii::$app->common->currentLanguage(),
					'type'	  => $type
				]
			]
		) );

		//disable pagination
        $dataProvider->pagination = false;

        return $this->render( 'index', [
            'searchModel'  		   => $searchModel,
            'dataProvider' 		   => $dataProvider,
			'is_models_translated' => Translation::find()
				-> select( [
					'lang_id',
					'foreign_id'
				] )
				-> where( [
					'foreign_id' => ! empty( $dataProvider->models )
						? ArrayHelper::getColumn( $dataProvider->models, 'id' )
						: [],
					'type' => 'taxonomy'
				] )
				-> asArray()
				-> all()

        ] );

    }

	/**
	 * Creates a new category.
	 * If creation is successful, the browser will be redirected to the 'index' page.
	 * @return mixed
	 * @throws NotFoundHttpException
	**/
    public function actionCreate()
    {

		//params initializations for current method
		$request = Yii::$app->request;
        $model   = new Taxonomies();

		//create new item for current model due to requested details
        if ( $model->load( $request->post() ) ) {

        	//fill in current model with containers and wagons
			$model->containers = implode( ',', $request->post( 'container_types', [] ) );
			$model->wagons 	   = implode( ',', $request->post( 'wagons', [] ) );

			//current user permissions
			$permissions = Yii::$app->authManager->getPermissionsByUser(
				Yii::$app->user->id
			);

			//check user access permissions
			if ( ( $model->type == 'cargo' && empty( array_intersect( [
				'logistics',
				'sea',
				'blockTrain'
			], array_keys( $permissions ) ) ) )
				|| ( $model->type == 'weights' && ! in_array( 'wagons', array_keys( $permissions ) ) )
				|| ( ! in_array( $model->type, [
					'cargo',
					'weights'
				] ) && ! in_array( $model->type, array_keys( $permissions ) )
			) ) {

				throw new NotFoundHttpException(
					__( 'The requested page does not exist' )
				);

			}

			//default language, which was set in settings page
			$lang_id = Settings::findOne( [
				'name' => 'default_language'
			] );

			if ( ! is_null( $lang_id ) ) {

				//assign default language for new item
				$model->lang_id = $lang_id->description;

				//check is weights for current model is array or not
				$model->weights = ! empty( $model->weights ) && is_array( $model->weights )
					? json_encode( $model->weights )
					: $model->weights;

				//set success flash message
				if ( $model->save() ) {

					Yii::$app->session->setFlash( 'success',
						__(  ucfirst( $model->type ) . ' was created successfully' )
					);
				}

			}

            return $this->redirect( [
                'index',
                'type' => $model->type
            ] );

        } else {

            return $this->render( 'create', [
                'model' => $model,
            ] );

        }

    }

	/**
	 * Updates an existing category due to requested category id.
	 * If update is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id ,
	 * @param integer $lang_id
	 * @return mixed
	 * @throws NotFoundHttpException
	**/
    public function actionUpdate( $id, $lang_id = 0 )
    {

		//params initializations for current method
        $model    = $this->findModel( $id );
        $request  = Yii::$app->request;
		$response = [
			'code'	  => 400,
			'title'	  => __( 'Bad Request' ),
			'message' => __( 'Please modify params and try again' )
		];

		//current user permissions
		$permissions = Yii::$app->authManager->getPermissionsByUser(
			Yii::$app->user->id
		);

		//check user access permissions
		if ( ( in_array( $model->type, [ 'cargo', 'weights' ] ) &&  empty( array_intersect( [
			'logistics',
			'sea',
			'blockTrain',
			'wagons',
		], array_keys( $permissions ) ) ) )
			|| ( ! in_array( $model->type, [
				'cargo',
				'weights'
			] ) && ! in_array( $model->type, array_keys( $permissions ) ) )
		) {

			throw new NotFoundHttpException(
				__( 'The requested page does not exist' )
			);

		}

		//current request is ajax
        if ( $request->isAjax ) {

			//change response type to json for ajax callback
			Yii::$app->response->format = Response::FORMAT_JSON;

			//updating model with requested details
        	if ( $model->load( $request->post() )
				&& $model->save()
			) {

				$response = [
					'code'	  => 200,
					'status'  => __( 'success' ),
					'message' => __( 'Type was successfully updated' )
				];

        	}

			return $response;


		} else if ( $model->load( $request->post() ) ) {

			//fill in current model with containers and wagons
			$model->containers = implode( ',', $request->post( 'container_types', [] ) );
			$model->wagons 	   = implode( ',', $request->post( 'wagons', [] ) );

			//check is weights for current model is array or not
			$model->weights = ! empty( $model->weights ) && is_array( $model->weights )
				? json_encode( $model->weights )
				: $model->weights;

			//set success flash message
			if ( $model->save() ) {

				Yii::$app->session->setFlash( 'success',
					__(  ucfirst( $model->type ) . ' was updated successfully' )
				);

			}

        }

        return $this->render( 'update', [
            'model' => $this->translates( $model, [
				'lang_id' => $lang_id > 0
					? $lang_id
					: $model->lang_id,
				'type'	    => 'taxonomy',
				'is_single' => true
			] ),
			'lang_id' => $lang_id > 0
				? $lang_id
				: $model->lang_id
        ] );

    }

	/**
	 * Lists all logistic types.
	 * @param string  $type
	 * @param integer $lang_id
	 * @return mixed
	 * @throws NotFoundHttpException
	**/
	public function actionTypes( $type = 'containers', $lang_id = 0 )
	{

		//check user access permissions
		if ( ( $type == 'logistics'
			&& ! Yii::$app->user->can( 'logistics' ) )
			|| ( $type == 'weights' && ! Yii::$app->user->can( 'wagons' ) )
			|| ( $type != 'weights' && ! Yii::$app->user->can( $type ) )
		) {

			throw new NotFoundHttpException(
				__( 'The requested page does not exist' )
			);

		}

		//params initializations for current method
		$model   = new Taxonomies();
		$request = Yii::$app->request;

		//updating current model with requested details
		if ( $model->load( $request->post() ) ) {

			//default language, which was set in settings page
			$lang_id = Settings::findOne( [
				'name' => 'default_language'
			] );

			if ( ! is_null( $lang_id ) ) {

				//assign default language for new item
				$model->lang_id = $lang_id->description;

				//set success message
				if ( $model->save() ) {

					Yii::$app->session->setFlash( 'success',
						__(  ucfirst( $model->type ) . ' was updated successfully' )
					);

				}
			}

			return $this->redirect(
				'types?type=' . $model->type
			);

		}

		//taxonomy details due to requested params
		$searchModel  = new TaxonomiesSearch();
		$dataProvider = $searchModel->search( ArrayHelper::merge(
			$request->queryParams, [
				$searchModel->formName() => [
					'type' => $type
				]
			]
		) );

		return $this->render( 'types', [
			'model'		   => $model,
			'searchModel'  => $searchModel,
			'dataProvider' => merge_objects( $dataProvider, [
				'models' => $this->translates( $dataProvider->models, [
					'lang_id'   => Yii::$app->common->currentLanguage(),
					'type'	    => 'taxonomy',
					'is_single' => false
				] )
			] ),
			'lang_id' => Yii::$app->common->currentLanguage()
		] );

	}

	/**
	 * Deletes an existing category due to requested category id.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @param string  $type
	 * @return mixed
	 * @throws NotFoundHttpException
	**/
    public function actionDelete( $id, $type = 'posts' )
    {

		//current user permissions
		$permissions = Yii::$app->authManager->getPermissionsByUser(
			Yii::$app->user->id
		);

		//check user access permissions
		if ( ( $type == 'cargo'
			&& empty( array_intersect( [
				'logistics',
				'sea',
				'blockTrain',
			], array_keys( $permissions ) ) )
		) || ( $type == 'weights' && ! in_array( 'wagons', array_keys( $permissions ) ) )
			|| ( ! in_array( $type, [ 'cargo', 'weights' ] ) && ! in_array( $type, array_keys( $permissions ) ) )
		) {

			throw new NotFoundHttpException(
				__( 'The requested page does not exist' )
			);

		}

		//delete record for requested id
		if ( $this->findModel( $id )->delete() ) {

			Yii::$app->session->setFlash( 'success',
				__(  ucfirst( $type ) . ' was deleted successfully' )
			);

		}

        return $this->redirect( $type != 'posts'
			? [
				in_array( $type, [ 'weights', 'containers' ] )
					? 'types'
					: 'index',
				'type' => $type
			]
			: [ 'index' ]
		);

    }

	/**
	 * Upload new file.
	 * If update is successful, action will return object with file id and url.
	 * @return mixed
	**/
	public function actionUpload()
	{

		//initializations params for current method
		$request = Yii::$app->request;
		$model   = new UploadForm( [
			'path' => '@frontend/web/upload/categories',
			'url'  => '@frontend_web/upload/categories'
		] );

		//current request is ajax
		if ( $request->isAjax ) {

			//loading requested file details
			$model->load( $request->post() );

			//change response type to json for ajax callback
			Yii::$app->response->format = Response::FORMAT_JSON;

			//uploading requested file
			if ( $model->upload() ) {

				//file details due to requested file id
				$file = Files::findOne(
					$request->post('file_id' )
				);

				//is requested file exists
				if( is_null( $file ) )
					$file = new Files();

				//updating file details
				$file = merge_objects( $file, [
					'name'  => uniqid(),
					'guide' => $model->getUrl(),
					'size'	=> (string)$model->image->size,
					'type'	=> 'categories'
				] );

				//saving file
				if ( $file->save() ) {

					return [
						'id'  => $file->id,
						'url' => Yii::getAlias( '@frontend_link' ) . $file->guide
					];

				}

			} else {

				return [
					'return' => json_encode(
						$model->errors
					)
				];

			}
		}

		return $this->redirect( [
			'create'
		] );

	}

    /**
     * Save categories order
     * @return mixed
    **/
    public function actionSorter()
	{

		//initializations params for current method
        $categories = array_map( 'intval', Yii::$app->request->post('categories', [] ) );
        $setting 	= Settings::findOne( [
			'name' => 'category_sorting'
		] );

        //assigning setting name
        if( is_null( $setting ) ) {

        	//set settings name
			$setting = merge_objects( new Settings(), [
				'name' => 'category_sorting'
			] );

        }

        //set description
		$setting->description = json_encode( $categories );

        //save setting
		$setting->save();

        return $this->redirect( [
            'index'
        ] );

    }

	/**
	 * Finds the Taxonomies model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return Taxonomies the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	**/
	protected function findModel( $id )
	{

		if ( ( $model = Taxonomies::findOne( $id ) ) !== null ) {
			return $model;
		} else {

			throw new NotFoundHttpException(
				__( 'The requested page does not exist' )
			);

		}

	}

}
