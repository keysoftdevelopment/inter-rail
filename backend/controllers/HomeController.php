<?php namespace backend\controllers;

/**************************************/
/*                                    */
/*       DASHBOARD CONTROLLER         */
/*                                    */
/**************************************/

use Yii;

use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

use common\models\LoginForm;

/**
 * Home Controller is the controller for showing statistics details
**/
class HomeController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {

        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [
                        	'login',
							'error'
						],
                        'allow' => true,
                    ],
                    [
                        'actions' => [
                        	'logout'
						],
                        'allow' => true,
                        'roles' => [ '@' ],
                    ],
                    [
                        'actions' => [
                        	'index'
						],
                        'allow' => true,
                        'roles' => [
                        	'dashboard'
						],
                    ]
                ],

				'denyCallback' => function( $rule, $action ) {
					\Yii::$app->response->redirect( [
						'/user/update'
					] );
				}

            ],

            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => [
                    	'POST'
					]
                ]
            ]

        ];

    }

    /**
     * @param $action
     * @return bool
    **/
    public function beforeAction( $action )
    {

        if ( parent::beforeAction( $action ) ) {

            // change layout for error action
            if ( $action->id == 'error' )
                $this->layout = 'errorHandler';

            return true;

        } else {
            return false;
        }

    }

    /**
     * @inheritdoc
    **/
    public function actions()
    {

        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];

    }

    /**
     * Displays homepage.
     *
     * @return string
    **/
    public function actionIndex()
    {

        return $this->render(
        	'index'
		);

    }

    /**
     * Login action.
     *
     * @return string
    **/
    public function actionLogin()
    {

        $this->layout = false;

        if ( !Yii::$app->user->isGuest )
            return $this->goHome();

        $model = new LoginForm();

        if ( $model->load( Yii::$app->request->post() ) && $model->login() ) {
            return $this->goBack();
        } else {

            return $this->render( 'login', [
                'model' => $model,
            ] );

        }

    }

    /**
     * Logout action.
     *
     * @return string
    **/
    public function actionLogout()
    {

        Yii::$app->user->logout();

        return $this->goHome();

    }

}
