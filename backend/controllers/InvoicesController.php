<?php namespace backend\controllers;

/**************************************/
/*                                    */
/*        INVOICES CONTROLLER         */
/*                                    */
/**************************************/

use common\models\Companies;
use common\models\Containers;
use common\models\ContainersRelation;
use common\models\Taxonomies;
use common\models\Wagons;
use common\models\WagonsRelation;
use kartik\mpdf\Pdf;
use Yii;

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

use common\models\Expenses;
use common\models\Invoices;
use common\models\InvoicesSearch;
use common\models\Orders;
use common\models\User;

/**
 * Invoices Controller is the controller behind the Invoices model.
**/
class InvoicesController extends Controller
{

	/**
	 * @inheritdoc
	**/
	public function behaviors()
	{

		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'actions' => [
							'update',
							'index',
							'generate-invoice',
							'exports'
						],
						'allow' => true,
						'roles' => [
							'invoices'
						]
					]
				]
			]

		];

	}

	/**
	 * Lists all Invoices.
	 * @return mixed
	**/
	public function actionIndex()
	{

		//params initializations for current method
		$searchModel  = new InvoicesSearch();
		$dataProvider = $searchModel->search( ArrayHelper::merge(
			Yii::$app->request->queryParams,
			[
				$searchModel->formName() => [
					'type' => 'Lotus'
				]
			]
		) );

		return $this->render( 'index', [
			'searchModel'  => $searchModel,
			'dataProvider' => $dataProvider,
			'users'		   => ! empty( $dataProvider->models )
				? User::find()
					-> where( [
						'id' => ArrayHelper::merge(
							ArrayHelper::getColumn(
								$dataProvider->models,
								'customer_id'
							),
							ArrayHelper::getColumn(
								$dataProvider->models,
								'creator'
							)
						),
						'isDeleted' => false
					] )
					-> indexBy( 'id' )
					-> asArray()
					-> all()
				: [],
			'orders' => ! empty( $dataProvider->models )
				? Orders::find()
					-> where( [
						'id' => ArrayHelper::getColumn(
							$dataProvider->models, 'foreign_id'
						)
					] )
					-> indexBy( 'id' )
					-> asArray()
					-> all()
				: []
		] );

	}


	/**
	 * Allow download report in xlsx file due to requested type
	 * Filtering data due to request params in url
	 * @param string $type
	 * @param integer $order_id
	 * @return mixed
	**/
	public function actionExports( $type, $order_id = 0 )
	{

		//is order id exist
		if ( $order_id > 0 ) {

			//order details
			$order = Orders::findOne(
				$order_id
			);

			//allow export for existing order
			if ( ! is_null( $order ) ) {

				//expenses details
				$expenses = Expenses::find()
					-> where( [
						'order_id' => $order_id
					] )
					-> asArray()
					-> all();

				//export xlsx file with requested details
				Yii::$app->excel_export->expensesExports( [
					'type'	=> $type,
					'model' => [
						'title' => [
							'from'   	   => $order->trans_from,
							'to'     	   => $order->trans_to,
							'order_number' => $order->number
						]
					],
					'expenses' => $expenses,
					'invoices' => array_reduce( Invoices::find()
						-> where( [
							'foreign_id' => ArrayHelper::getColumn(
								$expenses, 'id'
							),
							'type' => 'expenses'
						] )
						-> asArray()
						-> all(), function ( $invoice_result, $invoice ) {

						if ( $invoice )
							$invoice_result[ $invoice[ 'foreign_id' ] ][] = $invoice;

						return $invoice_result;

					}, [] ),
					'report' => $type == 'wagons'
						? 'EXPENSES-WAGONS'
						: 'EXPENSES-CONTAINERS',
					'file_name' => $type == 'wagons'
						? __( 'Wagons Expenses Report' )
						: __( 'Containers Expenses Report' )
				] );

			}

		}

		return $this->redirect( Url::to( [
			'/invoices'
		] ) );

	}


	/**
	 * Download Invoice For Requested Order.
	 * @param integer $id
	 * @return mixed
	**/
	public function actionGenerateInvoice( $id )
	{

		//param initialization
		$model = Orders::findOne(
			$id
		);

		//is order exists
		if ( is_null( $model ) ) {

			return $this->redirect( [
				'cabinet/orders'
			] );

		}

		//filling current model with additional details
		$model->fillModel();

		//the list of ids
		$options = ! is_null( $model->containers_type )
		&& ! empty( $model->containers_type )
			? ContainersRelation::find()
				-> where( [
					'foreign_id' => $id,
					'type' 	     => 'order'
				] )
				-> asArray()
				-> all()
			: WagonsRelation::find()
				-> where( [
					'foreign_id' => $id,
					'type' 	     => 'order'
				] )
				-> asArray()
				-> all();

		//invoice details
		$invoice = Invoices::find()
			-> where( [
				'foreign_id' => $id,
				'type' 	     => 'Lotus'
			] )
			-> orderBy( [
				'id' => SORT_DESC
			] )
			-> one();

		//generate pdf from html
		$pdf = new Pdf( [
			// set to use core fonts only
			'mode' => Pdf::MODE_UTF8,
			// A4 paper format
			'format' => Pdf::FORMAT_A4,
			// portrait orientation
			'orientation' => Pdf::ORIENT_PORTRAIT ,
			// stream to browser inline
			'destination' => Pdf::DEST_BROWSER,
			// html content input
			'content' => $this->renderPartial( '_pdf', [
				'model'   	     => $model,
				'transportation' => ! is_null( $model->containers_type ) && ! empty( $model->containers_type )
					? Containers::findAll( [
						'id' => ArrayHelper::getColumn(
							$options, 'container_id'
						),
						'isDeleted' => false
					] )
					: Wagons::findAll( [
						'id' => ArrayHelper::getColumn(
							$options, 'wagon_id'
						),
						'isDeleted' => false
					] ),
				'type' => Taxonomies::findOne( ! is_null( $model->containers_type ) && ! empty( $model->containers_type )
					? $model->containers_type
					: $model->wagons_type
				),
				'invoice' => $invoice,
				'company' => ! is_null( Yii::$app->user->identity->company_id )
					? Companies::findOne(
						Yii::$app->user->identity->company_id
					)
					: []
			] ),
			// set mPDF properties on the fly
			'options' => [
				'title' => __( 'InterRail Invoice' ) . ' - ' . $invoice->number
			]
		] );

		return $pdf->render();

	}


	/**
	 * Finds the Invoices model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return Invoices the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	**/
	protected function findModel( $id )
	{

		if ( ( $model = Invoices::findOne( $id ) ) !== null ) {
			return $model;
		} else {

			throw new NotFoundHttpException(
				__( 'The requested page does not exist' )
			);

		}

	}

}