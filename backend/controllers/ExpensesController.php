<?php namespace backend\controllers;

/**************************************/
/*                                    */
/*        EXPENSES CONTROLLER         */
/*                                    */
/**************************************/

use Yii;

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

use common\models\ContainersRelation;
use common\models\Expenses;
use common\models\ExpensesSearch;
use common\models\Invoices;
use common\models\Orders;
use common\models\TerminalStorage;
use common\models\User;
use common\models\WagonsRelation;

/**
 * Expenses Controller is the controller behind the Expenses model.
**/
class ExpensesController extends Controller
{

	/**
	 * @inheritdoc
	**/
	public function behaviors()
	{

		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'actions' => [
							'update',
							'index',
							'exports'
						],
						'allow' => true,
						'roles' => [
							'expenses'
						]
					], [
						'actions' => [
							'index',
							'update'
						],
						'allow' => true,
						'roles' => [
							'terminals'
						]
					]
				]
			]

		];

	}

	/**
	 * Lists all Expenses models.
	 * @return mixed
	**/
	public function actionIndex()
	{

		//params initializations for current method
		$request	 = Yii::$app->request;
		$searchModel = new ExpensesSearch();
		$orders      = $expenses = [];

		//is ajax request
		if ( $request->isAjax ) {

			//expenses ids due to requested container id
			$expenses_ids = ContainersRelation::find()
				-> where( [
					'container_id' => $request->post( 'container_id' ),
					'type'		   => 'expenses'
				] )
				-> asArray()
				-> all();

			//expenses ids due to requested wagon id
			if ( ! empty( $request->post( 'wagon_id' ) ) ) {

				$expenses_ids = WagonsRelation::find()
					-> where( [
						'wagon_id' => $request->post( 'wagon_id' ),
						'type'	   => 'expenses'
					] )
					-> asArray()
					-> all();

			}

			//expenses details due to expenses ids
			if ( ! empty( $expenses_ids )
				|| ! empty( $request->post( 'id' ) )
			) {

				$expenses = Expenses::findOne( [
					'id' => ! empty( $request->post( 'id' ) )
						? $request->post( 'id' )
						: ArrayHelper::getColumn(
							$expenses_ids, 'foreign_id'
						),
					'order_id' => $request->post( 'order_id' )
				] );

				if ( ! is_null( $expenses ) ) {

					//add container id to current model, i.e used for getting rental periods
					$expenses->container_id = $request->post( 'container_id' );

					//fill in current model with details
					$expenses->fillStorageDates();

				}



			}

			//change response type to json for ajax callback
			Yii::$app->response->format = Response::FORMAT_JSON;

			return [
				'code'     => 200,
				'status'   => 'success',
				'response' => [
					'expenses' => $expenses,
					'invoices' => ! empty( $expenses )
						? Invoices::find()
							-> where( [
								'foreign_id' => $expenses->id,
								'type'		 => 'expenses'
							] )
							-> asArray()
							-> all()
						: [],
					'rentals' => ! empty( $expenses )
						? array_reduce( TerminalStorage::find()
							-> where( [
								'order_id' => $expenses->order_id
							] )
							-> indexBy( 'container_id' )
							-> asArray()
							-> all(), function( $result, $rental ){

								if ( ! empty( $rental ) ) {

									$result[ $rental[ 'container_id' ] ] = [
										'from' => date( 'd/m/Y', strtotime( ! empty( $rental[ 'from' ] )
											? $rental[ 'from' ]
											: 'today'
										) ),
										'to' => date( 'd/m/Y', strtotime( ! empty( $rental[ 'to' ] )
											? $rental[ 'to' ]
											: 'today'
										) )
									];

								}

								return $result;

							}, [] )
						: [],
					'codes' => ! empty( $request->post( 'order_id' ) )
						? array_filter( array_reduce( Expenses::find()
							-> where( [
								'order_id' => $request->post( 'order_id' )
							] )
							-> asArray()
							-> all(), function( $result, $expense ) {

								if ( ! empty( $expense ) ) {

									$railway_details = ! empty( $expense[ 'rf_details' ] )
										? json_decode( $expense[ 'rf_details' ] )
										: '';

									if ( ! empty( $railway_details )
										&& ! empty( $railway_details->code )
									) {

										foreach ( $railway_details->code as $key => $value ) {
											$result[$key][] = $value;
										}

									}

								}

								return $result;

						}, [] ), function( $filter ) {
							return ! empty( $filter );
						} )
						: []
				]
			];
		}

		//current model details
		$dataProvider = $searchModel->search( ArrayHelper::merge(
			Yii::$app->request->queryParams,
			[
				$searchModel->formName() => []
			]
		) );

		//order details for current model
		if ( ! empty( $dataProvider->models ) ) {

			$orders = Orders::find()
				-> select( [
					'id',
					'number',
					'user_id'
				] )
				-> where( [
					'id' => ArrayHelper::getColumn(
						$dataProvider->models, 'order_id'
					)
				] )
				-> indexBy( 'id' )
				-> groupBy( 'id' )
				-> asArray()
				-> all();

		}

		return $this->render( 'index', [
			'searchModel'  => $searchModel,
			'dataProvider' => $dataProvider,
			'orders'	   => $orders,
			'customers'	   => ! empty( $orders )
				? User::find()
					-> where( [
						'id' => ArrayHelper::getColumn(
							$orders, 'user_id'
						),
						'isDeleted' => false
					] )
					-> indexBy( 'id' )
					-> asArray()
					-> all()
				: []
		] );

	}

	/**
	 * Updates an existing Expenses model.
	 * If update is successful, the browser will be redirected to the 'index' page.
	 * @return array
	 * @throws NotFoundHttpException
	**/
	public function actionUpdate()
	{

		//params initializations for current method
		$request 	 = Yii::$app->request;
		$expenses_id = $request->post( 'expenses_id' );
		$response	 = [
			'code'    => 400,
			'status'  => 'error',
			'message' => __( 'Bad request' )
		];

		//is ajax request
		if ( $request->isAjax ) {

			//current model due to requested expenses id
			$model = Expenses::findOne(
				$expenses_id
			);

			//assign new option
			$model = is_null( $model )
				? new Expenses()
				: $model;

			//updating current model with requested details
			if ( $model->load( $request->post() ) ) {

				//assign railway and invoices details
				$model = merge_objects( $model, [
					'rf_details' => json_encode( $request->post( 'rf_details' ) ),
					'invoices'	 => json_encode( $request->post( 'invoices' ) )
				] );

				//save expenses
				$model->save( false );

				//set appropriate response
				$response = [
					'code' 	 => 200,
					'status' => 'success',
					'result' => $model
				];

			}

			//change response type to json for ajax callback
			Yii::$app->response->format = Response::FORMAT_JSON;

			return $response;

		}

		throw new NotFoundHttpException(
			__( 'The requested page does not exist' )
		);

	}

	/**
	 * Allow download report in xlsx file due to requested type
	 * Filtering data due to request params in url
	 * @param string $type
	 * @param integer $order_id
	 * @return mixed
	**/
	public function actionExports( $type, $order_id = 0 )
	{

		//is order id exist
		if ( $order_id > 0 ) {

			//order details
			$order = Orders::findOne(
				$order_id
			);

			//allow export for existing order only
			if ( ! is_null( $order ) ) {

				//expenses details for requested order
				$expenses = Expenses::find()
					-> where( [
						'order_id' => $order_id
					] )
					-> asArray()
					-> all();

				//export xlsx file with requested details
				Yii::$app->excel_export->expensesExports( [
					'type'	=> $type,
					'model' => [
						'title' => [
							'from'   	   => $order->trans_from,
							'to'     	   => $order->trans_to,
							'order_number' => $order->number
						]
					],
					'expenses' => $expenses,
					'invoices' => array_reduce( Invoices::find()
						-> where( [
							'foreign_id' => ArrayHelper::getColumn(
								$expenses, 'id'
							),
							'type' => 'expenses'
						] )
						-> asArray()
						-> all(), function ( $invoice_result, $invoice ) {

						if ( $invoice )
							$invoice_result[ $invoice[ 'foreign_id' ] ][] = $invoice;

						return $invoice_result;

					}, [] ),
					'report' => $type == 'wagons'
						? 'EXPENSES-WAGONS'
						: 'EXPENSES-CONTAINERS',
					'file_name' => $type == 'wagons'
						? __( 'Wagons Expenses Report' )
						: __( 'Containers Expenses Report' )
				] );

			}

		}

		return $this->redirect( Url::to( [
			'/orders'
		] ) );

	}


	/**
	 * Finds the Expenses model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return Expenses the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	**/
	protected function findModel( $id )
	{

		if ( ( $model = Expenses::findOne( $id ) ) !== null ) {
			return $model;
		} else {

			throw new NotFoundHttpException(
				__( 'The requested page does not exist' )
			);

		}

	}

}