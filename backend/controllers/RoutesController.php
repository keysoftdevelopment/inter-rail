<?php namespace backend\controllers;

/**************************************/
/*                                    */
/*          ROUTES CONTROLLER         */
/*                                    */
/**************************************/

use Yii;

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use common\models\Routes;
use common\models\RoutesSearch;
use common\models\Places;
use common\models\Logistics;

/**
 * Routes Controller is the controller behind the Routes model.
**/
class RoutesController extends Controller
{

	/**
	 * @inheritdoc
	**/
	public function behaviors()
	{

		return [

			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'actions' => [
							'create',
							'update',
							'index',
							'graphhopper',
							'delete'
						],
						'allow' => true,
						'roles' => [
							'routes'
						]
					]
				]
			],

			'verbs'  => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => [
						'POST'
					]
				]
			]

		];

	}

	/**
	 * Lists all Routes models.
	 * @return mixed
	**/
	public function actionIndex()
	{

		//params initializations for current method
		$searchModel  = new RoutesSearch();
		$dataProvider = $searchModel->search(
			Yii::$app->request->queryParams
		);

		return $this->render( 'index', [
			'searchModel'  => $searchModel,
			'dataProvider' => $dataProvider,
			'locations'	   => ! empty( $dataProvider->models )
				? Places::find()
					-> where( [
						'id' => ArrayHelper::merge(
							ArrayHelper::getColumn(
								$dataProvider->models, 'from'
							),
							ArrayHelper::getColumn(
								$dataProvider->models, 'to'
							)
						)
					] )
					-> indexBy( 'id' )
					-> asArray()
					-> all()
				: []
		] );

	}

	/**
	 * Creates a new Routes model.
	 * If creation is successful, the browser will be redirected to the 'index' page.
	 *
	 * @return mixed
	**/
	public function actionCreate()
	{

		//params initializations for current method
		$model 	 = new Routes();
		$request = Yii::$app->request;

		//add new route
		if ( $request->isPost
			&& $model->load( $request->post() )
			&& $model->save( false )
		) {

			//set success response message
			Yii::$app->session->setFlash( 'success',
				__( 'Route was created successfully' )
			);

			return $this->redirect( [
				'index'
			] );

		}

		//current model logistic ids details
		$model->getLogistics();

		return $this->render( 'create', [
			'model' 	=> $model,
			'logistics' => Logistics::find()
				-> select( [
					'id',
					'CONCAT(`from`, " - ", `to`) as route',
					'duration'
				] )
				-> where( [
					'!=', 'type', 'blockTrain'
				] )
				-> indexBy( 'id' )
				-> groupBy( 'id' )
				-> asArray()
				-> all()
		] );

	}

	/**
	 * Updates an existing Routes model.
	 * If update is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param integer $id
	 * @return mixed
	**/
	public function actionUpdate( $id )
	{

		//params initializations for current method
		$model = $this->findModel( $id );

		//updating current model with requested details
		if ( $model->load( Yii::$app->request->post() )
			&& $model->save( false )
		) {

			//set success response message
			Yii::$app->session->setFlash( 'success',
				__( 'Route was updated successfully' )
			);

			return $this->redirect( [
				'index'
			] );

		}

		//model logistic details
		$model->getLogistics();

		return $this->render( 'update', [
			'model'  	=> $model,
			'logistics' => Logistics::find()
				-> select( [
					'id',
					'CONCAT(`from`, " - ", `to`) as route',
					'duration'
				] )
				-> where( [
					'!=', 'type', 'blockTrain'
				] )
				-> indexBy( 'id' )
				-> groupBy( 'id' )
				-> asArray()
				-> all()
		] );

	}

	/**
	 * Deletes an existing Routes model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param integer $id
	 * @return mixed
	**/
	public function actionDelete( $id )
	{

		//set success response message
		if ( $this->findModel( $id )->delete() ) {

			Yii::$app->session->setFlash( 'success',
				__( 'Route was deleted successfully' )
			);

		}

		return $this->redirect( [
			'index'
		] );

	}

	/**
	 * Finds the Routes model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param integer $id
	 * @return Routes the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	**/
	protected function findModel( $id )
	{

		if ( ( $model = Routes::findOne( $id ) ) !== null ) {
			return $model;
		} else {

			throw new NotFoundHttpException(
				__( 'The requested page does not exist' )
			);

		}

	}

}