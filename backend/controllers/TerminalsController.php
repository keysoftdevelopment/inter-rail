<?php namespace backend\controllers;

/**************************************/
/*                                    */
/*        TERMINALS CONTROLLER        */
/*                                    */
/**************************************/

use Yii;

use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\web\Response;

use common\commands\SendEmailCommand;

use common\models\ContainersRelation;
use common\models\Containers;
use common\models\Orders;
use common\models\Taxonomies;
use common\models\TaxonomyRelation;
use common\models\TerminalStorage;
use common\models\TerminalStorageSearch;
use common\models\Terminals;
use common\models\TerminalsSearch;

/**
 * Terminals Controller is the controller behind the Terminals model.
**/
class TerminalsController extends Controller
{

	/**
	 * @inheritdoc
	**/
	public function behaviors()
	{

		return [

			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'actions' => [
							'create',
							'update',
							'index',
							'storage',
							'send-mail',
							'delete'
						],
						'allow' => true,
						'roles' => [
							'terminals'
						]
					]
				]
			],

			'verbs'  => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => [
						'POST'
					]
				]
			]

		];

	}

	/**
	 * Lists all Terminals models.
	 * @return mixed
	**/
	public function actionIndex()
	{

		//params initializations for current method
		$searchModel  = new TerminalsSearch();
		$dataProvider = $searchModel->search(
			Yii::$app->request->queryParams
		);

		//container ids due to terminal storage details
		$container_ids = ! empty( $dataProvider->models )
			? TerminalStorage::find()
				-> where( [
					'terminal_id' => ArrayHelper::getColumn(
						$dataProvider->models, 'id'
					)
				] )
				-> indexBy( 'container_id' )
				-> asArray()
				-> all()
			: [];

		return $this->render( 'index', [
			'searchModel'  => $searchModel,
			'dataProvider' => $dataProvider,
			'types' 	   => Yii::$app->common->taxonomyDetails( 'terminal' ),
			'containers'   => ! empty( $container_ids )
				? array_reduce( Containers::findAll( [
					'id' => ArrayHelper::getColumn( $container_ids, 'container_id' )
				] ), function( $result, $container ) use ( $container_ids ) {

					if( isset( $container_ids[ $container->id ] ) ) {
						$result[ $container_ids[ $container->id ][ 'terminal_id' ] ][] = $container;
					}

					return $result;

				}, [] )
				: []
		] );

	}

	/**
	 * Lists all Terminals Storage Details.
	 * @return mixed
	**/
	public function actionStorage()
	{

		//params initializations for current method
		$searchModel  = new TerminalStorageSearch();
		$dataProvider = $searchModel->search(
			Yii::$app->request->queryParams
		);

		return $this->render( 'storage', [
			'searchModel'  => $searchModel,
			'dataProvider' => $dataProvider,
			'taxonomies'   => Yii::$app->common->taxonomyDetails(),
			'containers'   => ! empty( $dataProvider->models )
				? Containers::find()
					-> where( [
						'id' => ArrayHelper::getColumn(
							$dataProvider->models, 'container_id'
						)
					] )
					-> indexBy( 'id' )
					-> asArray()
					-> all()
				: [],
			'terminals' => ! empty( $dataProvider->models )
				? Terminals::find()
					-> where( [
						'id' => ArrayHelper::getColumn(
							$dataProvider->models, 'terminal_id'
						),
						'isDeleted' => false
					] )
					-> indexBy( 'id' )
					-> asArray()
					-> all()
				: []
		] );
	}

	/**
	 * Creates a new Terminals model.
	 * If creation is successful, the browser will be redirected to the 'index' page.
	 * @return mixed
	**/
	public function actionCreate()
	{

		//params initializations for current method
		$model = new Terminals();

		//create new item for current model due to requested details
		if ( $model->load( Yii::$app->request->post() )
			&& $model->save()
		) {

			//set success response message
			Yii::$app->session->setFlash( 'success',
				__( 'Terminal was created successfully' )
			);

			return $this->redirect( [
				'index'
			] );

		}

		return $this->render( 'create', [
			'model' => $model
		] );

	}

	/**
	 * Updates an existing Terminals model.
	 * If update is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	**/
	public function actionUpdate( $id )
	{

		//params initializations for current method
		$model = $this->findModel( $id );

		//updating current model with requested details
		if ( $model->load( Yii::$app->request->post() )
			&& $model->save()
		) {

			//set success response message
			Yii::$app->session->setFlash( 'success',
				__( 'Terminal was updated successfully' )
			);

			return $this->redirect( [
				'index'
			] );

		}

		//terminal geo details
		$model->getLocation();

		//terminal container type id
		$model->containerType();

		return $this->render( 'update', [
			'model' => $model
		] );

	}

	/**
	 * Send mail to terminal with attachment
	 * @param $order_id
	 * @return array
	**/
	public function actionSendMail( $order_id )
	{

		//params initialization for current method
		$order 	  = Orders::findOne( $order_id );
		$storage  = TerminalStorage::findOne( [ 'order_id' => $order_id ] );
		$response = [
			'code'    => 400,
			'status'  => __( 'Bad Request' ),
			'message' => __( 'Please modify params and try again' )
		];

		//is there any storage details
		if ( ! is_null( $storage ) ) {

			//fill in current order
			$order->fillModel();

			//current terminal
			$model = Terminals::findOne(
				$storage->terminal_id
			);

			//attachment file name
			$attachment_name = $order->terminals[ 'vgm_document' ] == "on"
				? __( 'VGM' ) . ' ' .  __( 'from' ) . ' ' . date( 'd.m.Y', strtotime( $storage->from ) ) . '.pdf'
				: __( ucfirst( $order->transportation ) ) . ' ' .  __( 'from' ) . ' ' . date( 'd.m.Y', strtotime( $storage->from ) )  . '.pdf';


			if ( Yii::$app->commandBus->handle( new SendEmailCommand( [
				'to'      => $model->email,
				'subject' => __( 'Request to' ) . $order->terminals[ 'vgm_document' ] == "on"
					? __( 'VGM' )
					: __( ucfirst( $order->transportation ) ),
				'view'   => 'terminal',
				'params' => [
					'order' => [
						'id' 	 => $order_id,
						'number' => $order->number
					],
					'dates' => [
						'from' => $storage->from,
						'to'   => $storage->to
					],
					'status' 	 => $order->terminals[ 'status' ],
					'attachment' => [
						'file_name' => $attachment_name,
						'link' 		=> Yii::getAlias( '@backend' ) . '/web/reports/terminals/'. $order->number . '/' . $attachment_name
					]

				]
			] ) ) ) {

				$response = [
					'code'   => 200,
					'status' => 'success',
					'result' => __( 'Mail to terminal was sent successfully' )
				];

			}


		}

		//change response type to json for ajax callback
		Yii::$app->response->format = Response::FORMAT_JSON;

		return $response;

	}

	/**
	 * Deletes an existing Terminals model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	**/
	public function actionDelete( $id )
	{

		//set success response message
		if ( $this->findModel( $id )->delete() ) {

			Yii::$app->session->setFlash( 'success',
				__( 'Terminal was deleted successfully' )
			);

		}

		return $this->redirect( [
			'index'
		] );

	}

	/**
	 * Finds the Terminals model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return Terminals the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	**/
	protected function findModel( $id )
	{

		if ( ( $model = Terminals::findOne( $id ) ) !== null ) {
			return $model;
		} else {

			throw new NotFoundHttpException(
				__( 'The requested page does not exist' )
			);

		}

	}

}