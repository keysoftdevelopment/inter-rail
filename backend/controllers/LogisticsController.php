<?php namespace backend\controllers;

/**************************************/
/*                                    */
/*        LOGISTICS CONTROLLER        */
/*                                    */
/**************************************/

use Yii;

use PHPExcel_IOFactory;

use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\Response;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;

use common\models\Logistics;
use common\models\LogisticsSearch;
use common\models\Places;
use common\models\PlacesRelation;
use common\models\Settings;
use common\models\SettingsForm;
use common\models\Taxonomies;

/**
 * Logistics Controller is the controller behind the Logistics model.
**/
class LogisticsController extends Controller
{

	/**
	 * @inheritdoc
	**/
	public function behaviors()
	{

		return [

			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'actions' => [
							'create',
							'update',
							'index',
							'settings',
							'import',
							'export',
							'delete'
						],
						'allow' => true,
						'roles' => [
							'auto',
							'logistics',
							'railway',
							'sea',
							'train'
						],
					]
				]
			],

			'verbs'  => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => [
						'POST'
					]
				]
			]

		];

	}

	/**
	 * Lists all Logistics models.
	 * @param string $type
	 * @return mixed
	 * @throws NotFoundHttpException
	**/
	public function actionIndex( $type = 'railway' )
	{

		//check user access permissions
		if ( ! Yii::$app->user->can( $type ) ) {

			throw new NotFoundHttpException(
				__( 'The requested page does not exist' )
			);

		}

		//params initializations for current method
		$searchModel  = new LogisticsSearch();
		$dataProvider = $searchModel->search( ArrayHelper::merge(
			Yii::$app->request->queryParams, [
				$searchModel->formName() => [
					'type' => $type
				]
			]
		) );

		return $this->render( 'index', [
			'searchModel'  => $searchModel,
			'dataProvider' => $dataProvider,
			'type'		   => $type,
			'taxonomies'   => [
				'container' => Yii::$app->common->taxonomyDetails( $type ),
				'railway'	=> $type == 'railway'
					? Yii::$app->common->taxonomyDetails( 'railway_type' )
					: []
			]
		] );

	}

	/**
	 * Creates a new Logistics model.
	 * If creation is successful, the browser will be redirected to the 'index' page.
	 * @param string $type
	 * @return mixed
	 * @throws NotFoundHttpException
	**/
	public function actionCreate( $type = 'railway' )
	{

		//check user access permissions
		if ( ! Yii::$app->user->can( $type ) ) {

			throw new NotFoundHttpException(
				__( 'The requested page does not exist' )
			);

		}

		//initializations params for current method
		$model = merge_objects( new Logistics(), [
			'type' 		   => strtolower( $type ),
			'location_ids' => json_encode(
				Yii::$app->request->post( 'place', [] )
			)
		] );

		//create new item for current model due to requested details
		if ( $model->load( Yii::$app->request->post() )
			&& $model->save()
		) {

			//set success response message
			Yii::$app->session->setFlash( 'success',
				__( ucfirst( $model->type ) . ' was created successfully' )
			);

			return $this->redirect( [
				'index',
				'type' => $model->type
			] );

		}

		return $this->render( 'create', [
			'model' 	 => $model,
			'containers' => Taxonomies::find()
				-> where( [
					'type'      => 'containers',
					'isDeleted' => false
				] )
				-> all(),
			'places' => Places::find()
				-> where( [
					'type' => strtolower( $type )
				] )
				-> asArray()
				-> all()
		] );

	}

	/**
	 * Updates an existing Logistics model.
	 * If update is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 * @throws NotFoundHttpException
	**/
	public function actionUpdate( $id )
	{

		//initializations params for current method
		$model 				 = $this->findModel( $id );
		$model->location_ids = json_encode(
			Yii::$app->request->post( 'place', [] )
		);

		//check user access permissions
		if ( ! Yii::$app->user->can( $model->type ) ) {

			throw new NotFoundHttpException(
				__( 'The requested page does not exist' )
			);

		}

		//updating current model with requested details
		if ( $model->load( Yii::$app->request->post() )
			&& $model->save()
		) {

			//set success response message
			Yii::$app->session->setFlash( 'success',
				__( ucfirst( $model->type ) . ' was updated successfully' )
			);

			return $this->redirect( [
				'index',
				'type' => $model->type
			] );

		}

		//fill inn model with place ids
		$model->getLocation();

		//fill in model with container and railway type
		$model->fillTypes();

		return $this->render( 'update', [
			'model'		 => $model,
			'containers' => Taxonomies::find()
				-> where( [
					'type'      => 'containers',
					'isDeleted' => false
				] )
				-> all(),
			'places' => Places::find()
				-> where( [
					'type' => strtolower( $model->type )
				] )
				-> asArray()
				-> all()
		] );

	}

	/**
	 * Deletes an existing Logistics model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 * @throws NotFoundHttpException
	**/
	public function actionDelete( $id )
	{

		//initializations params for current method
		$model = $this->findModel( $id );

		//check user access permissions
		if ( ! Yii::$app->user->can( $model->type ) ) {

			throw new NotFoundHttpException(
				__( 'The requested page does not exist' )
			);

		}

		//delete requested record
		if ( $model->delete() ) {

			Yii::$app->session->setFlash( 'success',
				__( ucfirst( $model->type ) . ' was deleted successfully' )
			);

		}

		return $this->redirect( [
			'index',
			'type' => $model->type
		] );

	}

	/**
	 * Additional logistic setting parameters
	 * I.E packages, weight dimensions and terms
	 * @return string
	 * @throws NotFoundHttpException
	**/
	public function actionSettings()
	{

		//current user permissions
		$permissions = Yii::$app->authManager->getPermissionsByUser(
			Yii::$app->user->id
		);

		//check user access permissions
		if ( empty( array_intersect( [
			'logistics',
			'sea',
			'blockTrain',
		], array_keys( $permissions ) ) ) ) {

			throw new NotFoundHttpException(
				__( 'The requested page does not exist' )
			);

		}

		//initializations params for current method
		$model 	 = new SettingsForm();
		$request = Yii::$app->request;

		//update logistic setting details
		if ( $request->isPost ) {

			//set update scenario
			$model->setScenario(
				SettingsForm::SCENARIO_UPDATE
			);

			//updating current model with requested details
			if ( $model->load( $request->post() )
				&& $model->save()
			) {

				//set success response message
				Yii::$app->session->setFlash( 'success',
					__( 'Settings was updated successfully' )
				);

				$this->redirect( [
					'logistics/settings'
				] );

			}

		}

		//fill model with details
		$model->fill( 'logistics' );

		return $this->render( 'settings', [
			'model' => $model
		] );

	}

	/**
	 * Import logistic details.
	 * @param string $type
	 * @return mixed
	 * @throws NotFoundHttpException
	**/
	public function actionImport( $type = 'sea' )
	{

		//check user access permissions
		if ( ! Yii::$app->user->can( $type ) ) {

			throw new NotFoundHttpException(
				__( 'The requested page does not exist' )
			);

		}

		//params initializations for current method
		$file 	  = UploadedFile::getInstanceByName( 'xlsx_file' );
		$file_name = Yii::getAlias( '@backend/web/' . $file->baseName . '.' . $file->extension  );

		//saving requested file
		$file->saveAs( $file_name );

		//xlsx file details
		$xlsx_sheets = PHPExcel_IOFactory::load( $file_name );

		if ( $type == 'sea' ) {

			//default sea port from logistics settings page
			$sea_port = Settings::findOne( [
				'name' => 'sea_port'
			] );

			//is default sea port has chosen
			if ( ! is_null( $sea_port ) ) {

				//delete all sea route relations
				PlacesRelation::deleteAll( [
					'type' => 'sea'
				] );

				//delete all sea logistics
				Logistics::deleteAll( [
					'name' => [
						'CMA UZB',
						'MCC',
						'CMA TSES'
					],
					'type' => 'sea'
				] );

				//sea departure details
				$route_from = Places::findOne(
					$sea_port->description
				);

				//the list of available places in system
				$places = Places::find()
					-> where( [
						'type' => 'sea'
					] )
					-> indexBy( 'name' )
					-> asArray()
					-> all();

				foreach ( $xlsx_sheets->getAllSheets() as $key => $sheet ) {

					//params initializations
					$columns = [ 'C', 'D', 'E', 'F', 'G', 'H', 'L', 'M', 'N', 'O','P', 'Q' ];
					$transportation = $key == 0
						? 'export'
						: 'import';

					//sea freight type
					$types = array_reduce( $columns, function( $result, $section ) use ( $sheet ) {

						$result[ $section ] = (string)trim(
							$sheet->getCell( $section . 6
						)->getValue() );

						return $result;

					}, [] );

					$taxonomies = Taxonomies::find()
						-> where( [
							'name' => array_values ( $types ),
							'type' => 'containers'
						] )
						-> indexBy( 'name' )
						-> asArray()
						-> all();

					for ( $row = 7; $row <= $sheet->getHighestRow( 'K' ); $row++ ) {

						//params initializations
						$place_coc    = trim( $sheet->getCell( 'B' . $row )->getValue() );
						$duration_coc = str_replace( [ ' дней', ' day', ' days', ' дня', ' день', ' ', 'дней', 'day', 'days', 'дня', 'день' ], '', $sheet->getCell( 'I' . $row )->getValue() );
						$place_soc    = trim( $sheet->getCell( 'K' . $row )->getValue() );
						$duration_soc = str_replace( [ ' дней', ' day', ' days', ' дня', ' день', ' ', 'дней', 'day', 'days', 'дня', 'день' ], '', $sheet->getCell( 'R' . $row )->getValue() );

						if ( isset( $places[ $place_coc ] )
							|| isset( $places[ $place_soc ] )
						) {

							foreach ( $columns as $column ) {

								//sea freight rate
								$rate = (float)trim(preg_replace('/\s\s+/', ' ', $sheet->getCell( $column . $row )->getValue() ));

								if ( isset( $taxonomies[ $types[ $column ] ] )
									&& $rate > 0
								) {

									//params initializations
									$type_id = $taxonomies[ $types[ $column ] ][ 'id' ];

									//new logistic creation
									$sea 	   = new Logistics();
									$sea->name = ! in_array( $column, [ 'E', 'F', 'N', 'O' ] )
										? in_array( $column, [ 'C', 'D', 'L', 'M' ] )
											? 'CMA UZB'
											: 'CMA TSES'
										: 'MCC';

									//logistics details
									$sea->rate 	   	      = $rate;
									$sea->containers_type = $type_id;
									$sea->transportation  = $transportation;
									$sea->type			  = 'sea';
									$sea->total    	      = $rate;

									if ( in_array( $column, [
										'C', 'D', 'E', 'F', 'G', 'H'
									] ) ) {

										//set logistic from
										$sea->from = $key == 0
											? $route_from->name
											: $place_coc;

										//set logistic to
										$sea->to = $key == 0
											? $place_coc
											: $route_from->name;

										//set logistic duration
										if ( ! empty( $duration_coc ) )
											$sea->duration = $duration_coc;

										//set logistic locations
										$sea->c_type	   = 'coc';
										$sea->location_ids = $key == 0
											? json_encode( [
												$places[ $place_coc ][ 'id' ],
												$route_from->id
											] )
											: json_encode( [
												$route_from->id,
												$places[ $place_coc ][ 'id' ]
											] );

									} else {

										//set logistic from
										$sea->from = $key == 0
											? $route_from->name
											: $place_soc;

										//set logistic to
										$sea->to = $key == 0
											? $place_soc
											: $route_from->name;

										if ( ! empty( $duration_coc ) )
											$sea->duration = $duration_soc;

										//set logistic locations
										$sea->c_type	   = 'soc';
										$sea->location_ids = $key == 0
											? json_encode( [
												$places[ $place_soc ][ 'id' ],
												$route_from->id
											] )
											: json_encode( [
												$route_from->id,
												$places[ $place_soc ][ 'id' ]
											] );

									}

									//save logistic
									$sea->save( false );

								}

							}

						}

					}

				}

			}

		} else {

			foreach ( $xlsx_sheets->getAllSheets() as $key => $sheet ) {

				//param initialization
				$logistics = [];

				for ( $row = 6; $row <= $sheet->getHighestRow( 'A' ); $row++ ) {

					if ( ! empty( $sheet->getCell( 'B' . $row )->getValue() )
						&& $sheet->getCell( 'C' . $row )->getValue()
					) {

						//set main logistic details
						$logistics[ $row ] = [
							'from' 	   => $sheet->getCell( 'B' . $row )->getValue(),
							'to'   	   => $sheet->getCell( 'C' . $row )->getValue(),
							'type'     => $sheet->getCell( 'D' . $row )->getValue(),
							'validity' => $sheet->getCell( 'H' . $row )->getValue(),
						];

						//set auto or railway logistic details
						if ( $type == 'auto' ) {
							$logistics[ $row ][ 'transportation' ] = $sheet->getCell( 'E' . $row )->getValue();
							$logistics[ $row ][ 'status' ] 	       = $sheet->getCell( 'F' . $row )->getValue();
							$logistics[ $row ][ 'rate' ] 		   = (float)trim(preg_replace('/\s\s+/', ' ', $sheet->getCell( 'G' . $row )->getValue() ));
						} else {
							$logistics[ $row ][ 'weight' ]   = $sheet->getCell( 'E' . $row )->getValue();
							$logistics[ $row ][ 'rate' ] 	 = (float)trim(preg_replace('/\s\s+/', ' ', $sheet->getCell( 'F' . $row )->getValue() ));
							$logistics[ $row ][ 'duration' ] = $sheet->getCell( 'G' . $row )->getValue();
						}

					}

				}

				if ( ! empty( $logistics ) ) {

					//locations due to import details
					$places = Places::find()
						-> where( [
							'name' => array_unique( ArrayHelper::merge( ArrayHelper::getColumn(
								$logistics, 'from'
							), ArrayHelper::getColumn(
								$logistics, 'to'
							) ) )
						] )
						-> indexBy( 'name' )
						-> asArray()
						-> all();

					//delete all sea route relations
					if ( ! empty( $places ) ) {

						PlacesRelation::deleteAll( [
							'id' => ArrayHelper::getColumn(
								$places, 'id'
							),
							'type' => $type
						] );

					}

					//taxonomy details due to import info
					$taxonomies = Taxonomies::find()
						-> where( [
							'name' => ArrayHelper::getColumn(
								$logistics, 'type'
							),
							'type' => $type == 'auto'
								? 'containers'
								: 'railway'
						] )
						-> indexBy( 'name' )
						-> asArray()
						-> all();

					//delete all auto or railway logistics details
					Logistics::deleteAll( $type == 'auto'
						? [
							'type' => $type,
							'from' => ArrayHelper::getColumn(
								$logistics, 'from'
							),
							'to' => ArrayHelper::getColumn(
								$logistics, 'to'
							),
							'status' => ArrayHelper::getColumn(
								$logistics, 'status'
							)[0],
							'transportation' => ArrayHelper::getColumn(
								$logistics, 'transportation'
							)[0]
						]
						: [
							'type' => $type,
							'from' => ArrayHelper::getColumn(
								$logistics, 'from'
							),
							'to' => ArrayHelper::getColumn(
								$logistics, 'to'
							)
						]
					);

					foreach ( $logistics as $log_key => $value ) {

						//logistic details
						$logistic = merge_objects( new Logistics(), [
							'from' 		 => $value[ 'from' ],
							'to'   	     => $value[ 'to' ],
							'rate' 		 => $value[ 'rate' ],
							'type' 		 => $type,
							'updated_at' => ! empty( $value[ 'validity' ] )
								? date( 'Y-m-d H:i:s', strtotime( $value[ 'validity' ] ) )
								: date( 'Y-m-d H:i:s', strtotime( 'today' ) ),
							'location_ids' => isset( $places[ $value[ 'from' ] ] )
								&& isset( $places[ $value[ 'to' ] ] )
								? json_encode( [
									$places[ $value[ 'from' ] ][ 'id' ],
									$places[ $value[ 'to' ] ][ 'id' ]
								] )
								: '',
							'status' => $type == 'auto'
								? trim( $value[ 'status' ] )
								: null,
							'transportation' => $type == 'auto'
								? trim( $value[ 'transportation' ] )
								: null,
							'weight' => $type != 'auto'
								? $value[ 'weight' ]
								: null,
							'duration' => $type != 'auto'
								? $value[ 'duration' ]
								: null,
						] );

						//set container type
						$logistic->containers_type = isset( $taxonomies[ trim( $value[ 'type' ] ) ] )
							? $taxonomies[ trim( $value[ 'type' ] ) ][ 'id' ]
							: $logistic->containers_type;

						//save logistic
						$logistic->save( false );

					}

				}


			}

		}

		//remove saved file
		unlink( $file_name );

		//change response type to json for ajax callback
		Yii::$app->response->format = Response::FORMAT_JSON;

		return [
			'code'	  => 200,
			'status'  => 'success',
			'message' => __( 'Logistic Details Successfully imported' )
		];

	}

	/**
	 * Allow download report in xlsx file due to requested type
	 * Filtering data due to request params in url
	 * @param string $type
	 * @return mixed
	 * @throws NotFoundHttpException
	**/
	public function actionExport( $type )
	{

		//check user access permissions
		if ( ! Yii::$app->user->can( $type ) ) {

			throw new NotFoundHttpException(
				__( 'The requested page does not exist' )
			);

		}

		//params initializations
		$searchModel  = new LogisticsSearch();
		$dataProvider = $searchModel->search( ArrayHelper::merge(
			Yii::$app->request->queryParams, [
				$searchModel->formName() => [
					'type' => $type
				]
			]
		) );

		//disable pagination
		$dataProvider->pagination = false;

		//export excel file
		Yii::$app->excel_export->logisticsExport( [
			'type' 	 => $type,
			'report' => $type == 'sea'
				? 'SEAFREIGHT'
				: strtoupper( $type ),
			'file_name' => $type == 'sea'
				? __( 'SEA FREIGHT' )
				: __( ucfirst( $type ) . '`s report' ),
			'details' 	=> $dataProvider->models
		] );

		return $this->redirect( Url::to( [
			'/logistics',
			'type' => $type
		] ) );

	}

	/**
	 * Finds the Logistics model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return Logistics the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	**/
	protected function findModel( $id )
	{

		if ( ( $model = Logistics::findOne( $id ) ) !== null ) {
			return $model;
		} else {

			throw new NotFoundHttpException(
				__( 'The requested page does not exist' )
			);

		}

	}

}