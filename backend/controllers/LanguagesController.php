<?php namespace backend\controllers;

/**************************************/
/*                                    */
/*       LANGUAGES CONTROLLER         */
/*                                    */
/**************************************/

use Yii;

use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use common\models\Languages;
use common\models\LanguagesSearch;

/**
 * Languages Controller is the controller behind the Languages model.
**/
class LanguagesController extends Controller
{

    /**
     * @inheritdoc
    **/
    public function behaviors()
    {

        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [
                        	'create',
							'update',
							'index',
							'delete',
							'upload'
						],
                        'allow' => true,
                        'roles' => [
                        	'settings'
						],
                    ]
                ],
            ],

            'verbs' => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => [
                    	'POST'
					]
                ],
            ]

        ];

    }

    /**
     * Lists all Languages models.
     * @return mixed
    **/
    public function actionIndex()
    {

		//params initializations for current method
        $searchModel  = new LanguagesSearch();
        $dataProvider = $searchModel->search(
        	Yii::$app->request->queryParams
		);

        return $this->render( 'index', [
            'dataProvider' => $dataProvider,
        ] );

    }

    /**
     * Creates a new Languages model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
    **/
    public function actionCreate()
    {

		//params initializations for current method
        $model = new Languages();

		//create new item for current model due to requested details
        if ( $model->load( Yii::$app->request->post() )
			&& $model->save()
		) {

			//set success response message
			Yii::$app->session->setFlash( 'success',
				__( 'Language was created successfully' )
			);

            return $this->redirect( [
                'update',
                'id' => $model->id
            ] );

        }

		return $this->render( 'create', [
			'model' => $model,
		] );

    }

    /**
     * Updates an existing Languages model.
     * If update is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
    **/
    public function actionUpdate( $id )
    {

		//params initializations for current method
        $model = $this->findModel( $id );

		//updating current model with requested details
        if ( $model->load( Yii::$app->request->post() ) ) {

			//set success response message
			if ( $model->save() ) {

				Yii::$app->session->setFlash( 'success',
					__( 'Language was updated successfully' )
				);
			}

			return $this->redirect( 'index' );

		}

        return $this->render( 'update', [
            'model' => $model,
        ] );

    }

    /**
     * Deletes an existing Languages model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
    **/
    public function actionDelete( $id )
    {

		//set success response message
        if ( $this->findModel( $id )->delete() ) {

			Yii::$app->session->setFlash( 'success',
				__( 'Language was deleted successfully' )
			);
        }

        return $this->redirect( [
            'index'
        ] );

    }

    /**
     * Finds the Languages model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Languages the loaded model
     * @throws NotFoundHttpException if the model cannot be found
    **/
    protected function findModel( $id )
    {

        if ( ( $model = Languages::findOne( $id ) ) !== null ) {
            return $model;
        } else {

            throw new NotFoundHttpException(
            	__( 'The requested page does not exist' )
			);

        }

    }

}
