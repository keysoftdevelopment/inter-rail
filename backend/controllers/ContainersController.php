<?php namespace backend\controllers;

/**************************************/
/*                                    */
/*        CONTAINERS CONTROLLER       */
/*                                    */
/**************************************/

use common\models\Wagons;
use common\models\WagonsRelation;
use Yii;

use PHPExcel_IOFactory;

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\web\UploadedFile;

use common\behaviors\TranslationBehavior;

use common\models\Containers;
use common\models\ContainersRelation;
use common\models\ContainersSearch;
use common\models\Orders;
use common\models\Taxonomies;
use common\models\TaxonomyRelation;
use common\models\Terminals;
use common\models\TerminalStorage;

/**
 * Containers Controller is the controller behind the Containers model.
**/
class ContainersController extends Controller
{

	/**
	 * @inheritdoc
	**/
	public function behaviors()
	{

		return [

			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'actions' => [
							'create',
							'update',
							'index',
							'import',
							'export',
							'delete'
						],
						'allow' => true,
						'roles' => [
							'containers'
						],
					],
				],
			],

			'verbs'  => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => [
						'POST'
					]
				]
			],

			'translation' => [
				'class' => TranslationBehavior::className(),
			]

		];

	}

	/**
	 * Lists all containers model.
	 * @return mixed
	**/
	public function actionIndex()
	{

		//params initializations for current method
		$searchModel  = new ContainersSearch();
		$dataProvider = $searchModel->search(
			Yii::$app->request->queryParams
		);

		return $this->render( 'index', [
			'searchModel'  => $searchModel,
			'dataProvider' => $dataProvider,
			'types'		   => Yii::$app->common->taxonomyDetails(),
			'orders' 	   => ! empty( $dataProvider->models )
				? ContainersRelation::find()
					-> where( [
						'container_id' => ArrayHelper::getColumn(
							$dataProvider->models,
							'id'
						),
						'type' => 'order'
					] )
					-> asArray()
					-> all()
				: []

		] );

	}

	/**
	 * Creates a new container.
	 * If creation is successful, the browser will be redirected to the 'index' page.
	 * @return mixed
	**/
	public function actionCreate()
	{

		//params initializations for current method
		$model   = new Containers();
		$request = Yii::$app->request;

		//create new item for current model due to requested details
		if ( $model->load(
			$request->post()
		) ) {

			//is request container exists in system
			$is_exists = Containers::findOne( [
				'number'    => $model->number,
				'isDeleted' => true
			] );

			if ( ! is_null( $is_exists ) ) {

				$model 			  = $this->findModel( $is_exists->id );
				$model->isDeleted = false;

				//load container requested details
				$model->load(
					$request->post()
				);

			}

			if ( $model->save() ) {

				//current request is ajax
				if ( $request->isAjax ) {

					//change response type to json for ajax callback
					Yii::$app->response->format = Response::FORMAT_JSON;

					return [
						'code' 	   => 200,
						'status'   => 'success',
						'response' => [
							'id' 	 => $model->id,
							'number' => $model->number
						]
					];

				}

				//set success response message
				Yii::$app->session->setFlash( 'success',
					__( 'Container was created successfully' )
				);

				return $this->redirect( [
					'index'
				] );

			}

		}

		return $this->render( 'create', [
			'model' => $model,
			'types' => Taxonomies::find()
				-> where( [
					'type'		=> 'containers',
					'isDeleted' => false
				] )
				-> all()
		] );

	}

	/**
	 * Updates an existing container due to requested container id
	 * If update is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	**/
	public function actionUpdate( $id )
	{

		//params initializations for current method
		$model = $this->findModel( $id );

		//updating current model with requested details
		if ( $model->load( Yii::$app->request->post() )
			&& $model->save()
		) {

			//set success response message
			Yii::$app->session->setFlash( 'success',
				__( 'Container was updated successfully' )
			);

			return $this->redirect( [
				'index'
			] );

		}

		//fill in current model with taxonomy option
		$model->getType();

		return $this->render( 'update', [
			'model' => $model,
			'types' => Taxonomies::find()
				-> where( [
					'type'		=> 'containers',
					'isDeleted' => false
				] )
				-> all()
		] );

	}

	/**
	 * Import container details.
	 * @return mixed
	 * @throws NotFoundHttpException
	**/
	public function actionImport()
	{

		//params initializations for current method
		$file 	   = UploadedFile::getInstanceByName( 'xlsx_file' );
		$file_name = Yii::getAlias( '@backend/web/' . $file->baseName . '.' . $file->extension );
		$order	   = ! is_null( Yii::$app->getRequest()->getQueryParam( 'order_id' ) )
			? Orders::findOne(
				Yii::$app->getRequest()->getQueryParam( 'order_id' )
			)
			: NULL;

		//saving requested file
		$file->saveAs( $file_name );

		//xlsx file details
		$xlsx_sheets = PHPExcel_IOFactory::load( $file_name );

		foreach ( $xlsx_sheets->getAllSheets() as $key => $sheet ) {

			//xlsx import details
			$import_details = [];

			for ( $row = ! is_null( $order ) ? 10 : 4; $row <= $sheet->getHighestRow( 'A' ); $row++ ) {

				$import_details[] = ! is_null( $order )
					? [
						'number' => $sheet->getCell( 'B' . $row )->getValue(),
						'owner'  => $sheet->getCell( 'C' . $row )->getValue(),
						'wagon'  => $sheet->getCell( 'D' . $row )->getValue(),
						'type'   => trim( $sheet->getCell( 'E' . $row )->getValue() )
					]
					: [
						'number' 	    => $sheet->getCell( 'B' . $row )->getValue(),
						'type'   	    => $sheet->getCell( 'C' . $row )->getValue(),
						'owner'  	    => $sheet->getCell( 'D' . $row )->getValue(),
						'enter_date'    => $sheet->getCell( 'E' . $row )->getValue(),
						'export_date'   => $sheet->getCell( 'F' . $row )->getValue(),
						'terminal_name' => $sheet->getCell( 'G' . $row )->getValue(),
						'order_number'  => $sheet->getCell( 'H' . $row )->getValue()
					];

			}

			if ( ! empty( $import_details ) ) {

				//fill order with appropriate details
				if ( ! is_null( $order ) ) {
					$order->fillModel();
				}

				//container details due to requested imported container numbers
				$containers = Containers::find()
					-> where( [
						'number' => ArrayHelper::getColumn(
							$import_details,
							'number'
						),
						'isDeleted' => false
					] )
					-> indexBy( 'number' )
					-> asArray()
					-> all();

				//taxonomy details due to requested container types
				$taxonomies = Taxonomies::find()
					-> where( [
						'name' => ArrayHelper::getColumn(
							$import_details,
							'type'
						),
						'type' => is_null( $order )
							? 'containers'
							: 'railway'
					] )
					-> indexBy( 'name' )
					-> asArray()
					-> all();

				if ( is_null( $order ) ) {

					//terminal details due to requested terminal names
					$terminals = Terminals::find()
						-> where( [
							'name' => ArrayHelper::getColumn(
								$import_details,
								'terminal_name'
							)
						] )
						-> indexBy( 'name' )
						-> asArray()
						-> all();

					//order details due to requested order numbers
					$orders = Orders::find()
						-> where( [
							'number' => ArrayHelper::getColumn(
								$import_details,
								'order_number'
							)
						] )
						-> indexBy( 'number' )
						-> asArray()
						-> all();

				}

				//terminal storage removes in case if record exists
				if ( ! empty( $orders )
					&& ! empty( $terminals )
					&& ! empty( $containers )
				) {

					TerminalStorage::deleteAll( [
						'container_id' => ArrayHelper::getColumn(
							$containers, 'id'
						),
						'order_id' => ArrayHelper::getColumn(
							$orders,'id'
						),
						'terminal_id' => ArrayHelper::getColumn(
							$terminals, 'id'
						)
					] );

				} else if ( is_null( $order) ) {

					//remove all wagon relations with container
					WagonsRelation::deleteAll( [
						'foreign_id' => array_column( $containers, 'id' ),
						'type'		 => 'container-'. $order->id
					] );

				}

				//delete all container relations with order
				ContainersRelation::deleteAll( [
					'foreign_id' => ! is_null( $order )
						? $order->id
						: ArrayHelper::getColumn(
							$orders, 'id'
						),
					'type' => 'order'
				] );

				foreach ( $import_details as $details ) {

					//save container details
					if ( isset( $taxonomies[ $details[ 'type' ] ] )
						|| ! is_null( $order )
					) {

						//container main properties
						$container = merge_objects( isset( $containers[ $details[ 'number' ] ] )
							? Containers::findOne(
								$containers[ $details[ 'number' ] ][ 'id']
							)
							: new Containers(), [
							'number' => $details[ 'number' ],
							'owner'  => $details[ 'owner' ],
							'type'	 => ! is_null( $order )
								? $order->containers_type
								: $taxonomies[ $details[ 'type' ] ][ 'id' ]
						] );

						//container update
						$container->save( false );

						if ( isset( $taxonomies[ $details[ 'type' ] ] ) ) {

							//wagon details
							$current_wagon = Wagons::findOne( [
								'number' => $details[ 'wagon' ]
							] );

							//wagon main properties
							$wagon = merge_objects( ! is_null( $current_wagon )
								? $current_wagon
								: new Wagons(), [
								'number' => $details[ 'wagon' ],
								'type'	 => $taxonomies[ $details[ 'type' ] ][ 'id' ]
							] );

							//save wagon details
							$wagon->save( false );

						}

					}

					if( ! is_null( $order )
						&& isset( $container )
					) {

						//assign container relations
						$containers_rel = merge_objects( new ContainersRelation(), [
							'foreign_id'   => $order->id,
							'container_id' => $container->id,
							'type'		   => 'order'
						] );

						//save container order details
						$containers_rel->save();

						if ( isset( $wagon ) ) {

							//assign wagon relations
							$wagon_rel = merge_objects( new WagonsRelation(), [
								'foreign_id' => $container->id,
								'wagon_id'   => $wagon->id,
								'type'		 => 'container-'. $order->id
							] );

							//save wagon relations
							$wagon_rel->save();

						}

					} else if ( isset( $container )
						&& ! is_null( $container )
						&& ! empty( $details[ 'enter_date' ] )
						&& ! empty( $details[ 'export_date' ] )
						&& isset( $terminals[ $details[ 'terminal_name' ] ] )
						&& isset( $orders[ $details[ 'order_number' ] ] )
					) {

						//container relations main properties
						$relation = merge_objects( new ContainersRelation(), [
							'container_id' => $container->id,
							'foreign_id'   => $orders[ $details[ 'order_number' ] ][ 'id' ],
							'type'		   => 'order'
						] );

						//container relations update
						if ( $relation->save() ) {

							//terminal storage main properties
							$terminal_storage = merge_objects( new TerminalStorage(), [
								'container_id' => $container->id,
								'order_id'	   => $orders[ $details[ 'order_number' ] ][ 'id' ],
								'terminal_id'  => $terminals[ $details[ 'terminal_name' ] ][ 'id' ],
								'from'		   => date( 'Y-m-d', strtotime( str_replace( '/', '-', $details[ 'enter_date' ] ) ) ),
								'to'		   => date( 'Y-m-d', strtotime( str_replace( '/', '-', $details[ 'export_date' ] ) ) )
							] );

							//terminal storage update
							$terminal_storage->save();

						}

					}

				}

			}

		}

		//remove saved file
		unlink( $file_name );

		//change response type to json for ajax callback
		Yii::$app->response->format = Response::FORMAT_JSON;

		return [
			'code'	  => 200,
			'status'  => 'success',
			'message' => __( 'Container Details Successfully imported' )
		];

	}

	/**
	 * Allow download report in xlsx file due to requested type
	 * Filtering data due to request params in url
	 * @return mixed
	**/
	public function actionExport()
	{

		//params initializations for current method
		$searchModel  = new ContainersSearch();
		$dataProvider = $searchModel->search(
			Yii::$app->request->queryParams
		);

		//disabling limits for current model
		$dataProvider->pagination = false;

		//taxonomies ids due to container ids
		$tax_ids = TaxonomyRelation::find()
			-> where( [
				'foreign_id' => ! empty( $dataProvider->models )
					? ArrayHelper::getColumn(
						$dataProvider->models, 'id'
					)
					: [],
				'type' => 'containers'
			] )
			-> asArray()
			-> all();

		//taxonomy details due to tax ids
		$taxonomies = Taxonomies::find()
			-> where( [
				'id' => ArrayHelper::getColumn(
					$tax_ids, 'tax_id'
				)
			] )
			-> indexBy( 'id' )
			-> asArray()
			-> all();

		//export excel file
		Yii::$app->excel_export->export( [
			'models'	 => $dataProvider->models,
			'taxonomies' => array_reduce( $tax_ids,
				function( $container_type, $tax ) use ( $taxonomies ) {

					if ( isset( $taxonomies[ $tax[ 'tax_id' ] ] ) )
						$container_type[ $tax[ 'foreign_id' ] ] = $taxonomies[ $tax[ 'tax_id' ] ][ 'name' ];

					return $container_type;

				}, []
			),
			'type'       => 'containers',
			'report'     => 'CONTAINERS',
			'file_name'  => __( 'Container`s report' )
		] );

		return $this->redirect( Url::to( [
			'/containers'
		] ) );

	}


	/**
	 * Deletes an existing container due to requested container id.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	**/
	public function actionDelete( $id )
	{

		//set success response message
		if ( $this->findModel( $id )->delete() ) {

			Yii::$app->session->setFlash( 'success',
				__( 'Container was deleted successfully' )
			);

		}

		return $this->redirect( [
			'index'
		] );

	}

	/**
	 * Finds the Containers model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return Containers the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	**/
	protected function findModel( $id )
	{

		if ( ( $model = Containers::findOne( $id ) ) !== null ) {
			return $model;
		} else {

			throw new NotFoundHttpException(
				__( 'The requested page does not exist' )
			);

		}

	}

}