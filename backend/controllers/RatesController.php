<?php namespace backend\controllers;

/**************************************/
/*                                    */
/*          RATES CONTROLLER          */
/*                                    */
/**************************************/

use Yii;

use yii\filters\AccessControl;
use yii\filters\VerbFilter;

use yii\helpers\ArrayHelper;

use yii\web\Controller;
use yii\web\NotFoundHttpException;

use common\behaviors\TranslationBehavior;

use common\models\Rates;
use common\models\RatesSearch;
use common\models\Taxonomies;
use common\models\TaxonomyRelation;

/**
 * Rates Controller is the controller behind the Rates model.
**/
class RatesController extends Controller
{

	/**
	 * @inheritdoc
	 **/
	public function behaviors()
	{

		return [

			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'actions' => [
							'create',
							'update',
							'index',
							'delete'
						],
						'allow' => true,
						'roles' => [
							'rates'
						],
					],
				],
			],

			'verbs'  => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => [
						'POST'
					]
				]
			],

			'translation' => [
				'class'   => TranslationBehavior::className(),
			]

		];

	}

	/**
	 * Lists all Pages model.
	 * @return mixed
	 **/
	public function actionIndex()
	{

		//params initializations for current method
		$searchModel = new RatesSearch();
		$request     = Yii::$app->getRequest();
		$filters 	 = $request->getQueryParam( 'filters' );

		if ( ! empty( $request->getQueryParam( 'from' ) )
			&& ! empty( $request->getQueryParam( 'to' ) )
			&& ! empty( $request->getQueryParam( 'filters' ) )
		) {

			//rates container types
			$rates_types = TaxonomyRelation::find()
				-> where( [
					'tax_id' => $request->getQueryParam( 'filters' )[ 'containers_type' ],
					'type'	 => 'rates'
				] )
				-> asArray()
				-> indexBy( 'foreign_id' )
				-> all();

			//the list of available rates
			$rates = array_reduce( Rates::find()
				-> where( [
					'id' => ArrayHelper::getColumn(
						$rates_types,
						'foreign_id'
					),
				'from' => count( $filters[ 'transportation' ] ) > 1
					?  [
						$request->getQueryParam( 'from' ),
						$request->getQueryParam( 'to' )
					]
					: [
						$request->getQueryParam( 'from' )
					],
				'term_from' => count( $filters[ 'transportation' ] ) > 1
					? [
						(int)$request->getQueryParam( 'term_from' ),
						(int)$request->getQueryParam( 'term_to' )
					]
					: [
						(int)$request->getQueryParam( 'term_from' ),
					]
				] )
				-> andWhere( count( array_intersect( [
					'export', 'import'
				], $filters[ 'transportation' ] ) ) > 0
					? []
					: [
						'to' 	  => $request->getQueryParam( 'to' ),
						'term_to' => $request->getQueryParam( 'term_to' )
					] )
				-> asArray()
				-> indexBy( 'id' )
				-> all(), function( $result, $rate ) use( $rates_types ) {

					if( ! empty( $rate )
						&& isset( $rates_types[ $rate[ 'id' ] ][ 'id' ] )
					) {

						$result[ $rate[ 'transportation' ] . $rates_types[ $rate[ 'id' ] ][ 'tax_id' ] ] = array_replace( $rate, [
							'container_type' => [
								'id'   => $rates_types[ $rate[ 'id' ] ][ 'tax_id' ],
								'name' => $rates_types[ $rate[ 'id' ] ][ 'name' ]
							]
						] );

					}

				return $result;

			}, [] );

			if( ! empty( $rates ) ) {

				//requested container type
				$container_types = Taxonomies::find()
					-> where( [
						'id' => $request->getQueryParam( 'filters' )[ 'containers_type' ]
					] )
					-> indexBy( 'id' )
					-> asArray()
					-> all();

				//re-assign rates param
				$rates = array_reduce( array_keys(
					$request->getQueryParam( 'filters' )[ 'containers_type' ]
				), function( $result, $key ) use ( $container_types, $filters, $rates ) {

					//current type
					$current_type = isset( $container_types[ $filters[ 'containers_type' ] [ $key ] ]  )
						? $container_types[ $filters[ 'containers_type' ] [ $key ] ]
						: false;

					if ( $current_type
						&& isset( $rates[ $filters[ 'transportation' ][ $key ] . $current_type[ 'id' ] ] )
					) {

						//current rate due to transportation details
						$current_rate = $rates[ $filters[ 'transportation' ][ $key ] . $current_type[ 'id'] ];

						//gather containers quantities
						$qty = array_reduce( array_keys(
							$filters[ 'transportation' ]
						), function ( $result, $trans_key ) use ( $filters, $current_rate ) {

							if( $filters[ 'transportation' ][ $trans_key ] == $current_rate[ 'transportation' ] ) {
								$result += (int)$filters[ 'quantity' ][ $trans_key ];
							}

							return $result;

						}, 0 );

						//wagons quantities due to selected container types
						$wagons_qty = (int)$qty/( (float)$filters[ 'weight' ][ $key ]/1000 > 33
								? 1
								: ( strpos( $current_type[ 'name' ], '20' ) !== false && (float)$filters[ 'weight' ][ $key ]/1000 <= 23
									? 3
									: 2
								)
							);

						$result[] = [
							'id'    		 => $current_rate[ 'id' ],
							'wagons' 		 => $wagons_qty < 38 ? 38 : $wagons_qty,
							'qty' 			 => (int)$filters[ 'quantity' ][ $key ],
							'transportation' => $filters[ 'transportation' ][ $key ],
							'prime' 	     => $this->calculateRates( array_replace( $current_rate, [
								'weight' 	 	 => (float)$filters[ 'weight' ][ $key ],
								'qty'	     	 => (int)$qty,
								'days'			 => (int)$filters[ 'days' ][ $key ],
								'container_type' => $current_type[ 'name' ],
								'wagons_qty' 	 => $wagons_qty < 38
									? 38
									: $wagons_qty
							] ) ),
							'rate' => isset( $filters[ 'rate' ][ $key ] )
								? $filters[ 'rate' ][ $key ]
								: 0
						];

					}

					return $result;

				}, [] );

			}

		}

		$dataProvider = $searchModel->search( ArrayHelper::merge(
			Yii::$app->request->queryParams,
			[
				$searchModel->formName() => ! empty( $rates )
					? [
						'id' => ArrayHelper::getColumn(
							$rates, 'id'
						)
					]
					: []
			]
		) );

		return $this->render( 'index', [
			'searchModel'  => $searchModel,
			'dataProvider' => $dataProvider,
			'types' 	   => Yii::$app->common->taxonomyDetails( 'rates' ),
			'rates'		   => isset( $rates )
				? $rates
				: []
		] );

	}

	/**
	 * Creates a new rate.
	 * If creation is successful, the browser will be redirected to the 'index' page.
	 * @return mixed
	**/
	public function actionCreate()
	{

		//params initializations for current method
		$request        = Yii::$app->request;
		$model   	    = new Rates();
		$model->customs = json_encode(
			$request->post( 'customs', [] )
		);

		//add new page
		if ( $model->load( $request->post() )
			&& $model->save( false )
		) {

			//set success response message
			Yii::$app->session->setFlash( 'success',
				__( 'Rate was created successfully' )
			);

			return $this->redirect( [
				'index'
			] );

		}

		return $this->render( 'create', [
			'model' => $model,
			'types' => Taxonomies::find()
				-> where( [
					'type'      => 'containers',
					'isDeleted' => false
				] )
				-> all()
		] );

	}

	/**
	 * Updates an existing rate due to requested rate id.
	 * If update is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	**/
	public function actionUpdate( $id )
	{

		//params initializations for current method
		$request 	    = Yii::$app->request;
		$model   		= $this->findModel( $id );
		$model->customs = $request->isPost
			? json_encode(
				$request->post( 'customs', [] )
			)
			: $model->customs;

		//updating current model with requested details
		if ( $model->load( $request->post() )
			&& $model->save( false )
		) {

			//set success response message
			Yii::$app->session->setFlash( 'success',
				__( 'Rate was updated successfully' )
			);

			return $this->redirect( [
				'index'
			] );

		}

		//set container type and fill customs rates
		$model->fillModel();

		return $this->render( 'update', [
			'model' => $model,
			'types' => Taxonomies::find()
				-> where( [
					'type'      => 'containers',
					'isDeleted' => false
				] )
				-> all()
		] );

	}

	/**
	 * Deletes an existing rate due to requested rate id.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	**/
	public function actionDelete( $id )
	{

		//set success response message
		if ( $this->findModel( $id )->delete() ) {

			Yii::$app->session->setFlash( 'success',
				__( 'Rate was deleted successfully' )
			);

		}

		return $this->redirect( [
			'index'
		] );

	}

	/**
	 * Finds the Rates model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return Rates the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	**/
	protected function findModel( $id )
	{

		if ( ( $model = Rates::findOne( $id ) ) !== null ) {
			return $model;
		} else {

			throw new NotFoundHttpException(
				__( 'The requested page does not exist' )
			);

		}

	}

	/**
	 * Rates calculations
	 * @param $params array
	 * @return float
	**/
	protected function calculateRates( $params )
	{

		//current rate customs details
		$customs = array_reduce( ! empty( $params[ 'customs' ] )
			? (array)json_decode( $params[ 'customs' ] )->rate
			: [], function( $result, $rate ){

			if( ! empty( $rate ) ) {
				$result += $rate;
			}

			return $result;

		}, 0 );

		//rental calculation
		$rental = (int)$params[ 'qty' ] > 0
			? ( (float)$params[ 'rental' ] * (int)$params[ 'rental_period' ] * (float)$params[ 'wagons_qty' ] )/(int)$params[ 'qty' ]
			: 0;

		//wagon detention price
		$wd = ( $params[ 'transportation' ] == 'export'
			? ( ( (float)$params[ 'wagons_qty' ] * (float)$params[ 'surcharge' ] ) + ( (float)$params[ 'wd' ] * (int)$params[ 'qty' ] ) )/(int)$params[ 'qty' ]
			: ( (float)$params[ 'wagons_qty' ] * $params[ 'days' ] * (float)$params[ 'wd' ] + ( (float)$params[ 'surcharge' ] * ceil( (float)$params[ 'wagons_qty' ]/10 )  ) ) / (int)$params[ 'qty' ]
		);

		//re-assign weight detention param
		if( ( $params[ 'transportation' ] == 'import'
			&& strpos( $params[ 'container_type' ], '20' ) !== false )
		) {
			$wd = (float)$wd/1.5;
		}

		return (int)$params[ 'qty' ] > 0
			? (float)$params[ 'margin' ] + $wd + $rental + (float)$params[ 'forwarding' ] + (float)$params[ 'thc' ] + (float) + $customs + ( $params[ 'transportation' ] == 'import'
				? (float)$params[ 'forming' ]/(int)$params[ 'qty' ]
				: $params[ 'forming' ]
			)
			: 0;

	}

}