<?php namespace backend\controllers;

/**************************************/
/*                                    */
/*       DASHBOARD CONTROLLER         */
/*                                    */
/**************************************/

use Yii;

use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

use common\models\Containers;
use common\models\LoginForm;
use common\models\Orders;
use common\models\User;

/**
 * Home Controller is the controller for showing statistics details
**/
class DashboardController extends Controller
{

    /**
     * @inheritdoc
    **/
    public function behaviors()
    {

        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [
                        	'login',
							'error',
							'logout'
						],
                        'allow' => true,
                    ], [
                        'actions' => [
                        	'index'
						],
                        'allow' => true,
                        'roles' => [
                        	'dashboard'
						],
                    ]
                ],

				'denyCallback' => function( $rule, $action ) {
					\Yii::$app->response->redirect( [
						'/user/update'
					] );
				}

            ],

            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => [
                    	'POST'
					]
                ]
            ]

        ];

    }

    /**
     * @param $action
     * @return bool
    **/
    public function beforeAction( $action )
    {

		//params initializations for current method
    	$response = false;

    	//assign appropriate layout
        if ( parent::beforeAction( $action ) ) {

            // change layout for error action
            if ( $action->id == 'error' )
                $this->layout = 'errorHandler';

			$response = true;

        }

		return $response;

    }

    /**
     * @inheritdoc
    **/
    public function actions()
    {

        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];

    }

    /**
     * Dashboard statistic page.
     * @return string
    **/
    public function actionIndex()
    {

        return $this->render( 'index', [
			'orders' => [
				'today' => Orders::find()
					-> where( [
						'>=', 'created_at', date( 'Y-m-d', strtotime( 'today' ) )
					] )
					-> andWhere( [
						'isDeleted' => false
					] )
					-> count(),
				'month' => Orders::find()
					-> where( [
						'>=', 'created_at', date( 'Y-m-d h:i:s', strtotime( 'last month' ) )
					] )
					-> andWhere( [
						'isDeleted' => false
					] )
					-> groupBy( 'EXTRACT(DAY FROM created_at)' )
					-> select( [
						'created_at',
						'count(*) as count'
					] )
					-> all()
			],
			'users' => [
				'today' => User::find()
					-> where( [
						'id' 	    => \Yii::$app->authManager->getUserIdsByRole( 'client' ),
						'isDeleted' => false
					] )
					-> andWhere( [
						'>=', 'created_at', strtotime( 'today' )
					] )
					-> count(),
				'month' => User::find()
					-> select( [
						'created_at',
						'count(*) as count'
					] )
					-> where( [
						'id' 	    => \Yii::$app->authManager->getUserIdsByRole( 'client' ),
						'isDeleted' => false
					] )
					-> andWhere( [
						'>=', 'created_at', strtotime( 'first day of last month' )
					] )
					-> groupBy( 'created_at' )
					-> asArray()
					-> all()
			],
			'containers' => [
				'terminal' => Containers::find()
					-> where( [
						'status'    => 'terminal',
						'isDeleted' => false
					] )
					-> count(),
				'on_way' => Containers::find()
					-> where( [
						'status'    => 'on way',
						'isDeleted' => false
					] )
					-> count()
			]
		] );

    }

    /**
     * Login action.
     * @return string
    **/
    public function actionLogin()
    {
    	
		//params initializations for current method
        $this->layout = false;
		$model 	      = new LoginForm();

        if ( ! Yii::$app->user->isGuest )
            return $this->goHome();

        //log in user with requested params
        if ( $model->load( Yii::$app->request->post() )
			&& $model->login()
		) {
            return $this->goBack();
        } else {

            return $this->render( 'login', [
                'model' => $model,
            ] );

        }

    }

    /**
     * Logout action.
     * @return string
    **/
    public function actionLogout()
    {

    	//log out user
        Yii::$app->user->logout();

        return $this->goHome();

    }

}
