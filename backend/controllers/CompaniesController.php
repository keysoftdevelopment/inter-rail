<?php namespace backend\controllers;

/**************************************/
/*                                    */
/*        COMPANIES CONTROLLER        */
/*                                    */
/**************************************/

use Yii;

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

use common\models\Companies;
use common\models\CompaniesSearch;
use common\models\Files;
use common\models\UploadForm;

/**
 * Companies Controller is the controller behind the Companies model.
**/
class CompaniesController extends Controller
{

	/**
	 * @inheritdoc
	**/
	public function behaviors()
	{

		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'actions' => [
							'update',
							'index',
							'create',
							'delete',
							'upload'
						],
						'allow' => true,
						'roles' => [
							'companies'
						]
					]
				]
			],

			'verbs'  => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => [
						'POST'
					]
				]
			]

		];

	}

	/**
	 * Lists all Companies models.
	 * @return mixed
	**/
	public function actionIndex()
	{

		//params initializations for current method
		$searchModel  = new CompaniesSearch();
		$dataProvider = $searchModel->search( ArrayHelper::merge(
			Yii::$app->request->queryParams,
			[
				$searchModel->formName() => []
			]
		) );

		return $this->render( 'index', [
			'searchModel'  => $searchModel,
			'dataProvider' => $dataProvider,
			'thumbnails'   => ! empty( $dataProvider->models )
				? Yii::$app->common->thumbnails( ArrayHelper::getColumn(
					$dataProvider->models,
					'file_id'
				) )
				: []
		] );

	}

	/**
	 * Creates a new company.
	 * If creation is successful, the browser will be redirected to the 'index' page.
	 * @return mixed
	**/
	public function actionCreate()
	{

		//params initializations for current method
		$model   = new Companies();
		$request = Yii::$app->request;

		//create new item for current model due to requested details
		if ( $model->load( $request->post() )
			&& $model->save( false )
		) {

			//set success response message
			Yii::$app->session->setFlash( 'success',
				__( 'Company was created successfully' )
			);

			return $this->redirect( [
				'index'
			] );

		}

		return $this->render( 'create', [
			'model' => $model
		] );

	}

	/**
	 * Updates an existing Companies model.
	 * If update is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	**/
	public function actionUpdate( $id )
	{

		//params initializations for current method
		$model   = $this->findModel( $id );
		$request = Yii::$app->request;

		//updating current model with requested details
		if ( $model->load( $request->post() )
			&& $model->save( false )
		) {

			//set success response message
			Yii::$app->session->setFlash( 'success',
				__( 'Company was updated successfully' )
			);

			return $this->redirect( [
				'index'
			] );

		}

		//company location details
		$model->getLocation();

		return $this->render( 'update', [
			'model' => $model
		] );

	}

	/**
	 * Upload new file.
	 * If update is successful, action will return object with file id and url.
	 * @return mixed
	**/
	public function actionUpload()
	{

		//params initializations for current method
		$request = Yii::$app->request;
		$model   = new UploadForm( [
			'path' => '@frontend/web/upload/companies',
			'url'  => '@frontend_web/upload/companies'
		] );

		//current request is ajax
		if ( $request->isAjax ) {

			//loading requested file details
			$model->load( $request->post() );

			//change response type to json for ajax callback
			Yii::$app->response->format = Response::FORMAT_JSON;

			//uploading requested file
			if ( $model->upload() ) {

				//file details due to requested file id
				$file = Files::findOne(
					$request->post('file_id' )
				);

				//is requested file exists
				$file = is_null( $file )
					? new Files()
					: $file;

				//updating file details
				$file = merge_objects( $file, [
					'name'  => uniqid(),
					'guide' => $model->getUrl(),
					'size'	=> (string)$model->image->size,
					'type'	=> 'companies'
				] );

				//saving file
				if ( $file->save() ) {

					return [
						'id'  => $file->id,
						'url' => Yii::getAlias( '@frontend_link' ) . $file->guide
					];

				}

			} else {

				return [
					'return' => json_encode(
						$model->errors
					)
				];

			}
		}

		return $this->redirect( [
			'create'
		] );

	}

	/**
	 * Deletes an existing Companies model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	**/
	public function actionDelete( $id )
	{

		//set success response message
		if ( $this->findModel( $id )->delete() ) {

			Yii::$app->session->setFlash( 'success',
				__( 'Company was deleted successfully' )
			);

		}

		return $this->redirect( [
			'index'
		] );

	}

	/**
	 * Finds the Companies model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return Companies the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	**/
	protected function findModel( $id )
	{

		if ( ( $model = Companies::findOne( $id ) ) !== null ) {
			return $model;
		} else {

			throw new NotFoundHttpException(
				__( 'The requested page does not exist' )
			);

		}

	}

}
