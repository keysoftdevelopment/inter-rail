<?php namespace backend\controllers;

/**************************************/
/*                                    */
/*        	ROLES CONTROLLER          */
/*                                    */
/**************************************/

use Yii;

use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;

use common\models\Settings;

/**
 * Roles Controller is the controller allowing to CRUD operations with roles
**/
class RolesController extends Controller
{

	/**
	 * @inheritdoc
	 **/
	public function behaviors()
	{

		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'actions' => [
							'index',
							'update',
							'create',
							'delete'
						],
						'allow' => true,
						'roles' => [
							'settings'
						]
					]
				]
			],

			'verbs'  => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => [
						'POST'
					]
				]
			]

		];

	}

	/**
	 * Lists of all available Roles
	 * @return mixed
	**/
	public function actionIndex()
	{

		//params initializations for current method
		$auth   = Yii::$app->authManager;
		$models = $auth->getRoles();

		//removing admin and client from model list
		unset( $models[ 'admin' ] );
		unset( $models[ 'client' ] );

		return $this->render( 'index', [
			'models' 	  => $models,
			'permissions' => array_reduce( $models, function( $permission, $role ) use ( $auth ) {

				if ( ! empty( $role ) )
					$permission[ $role->name ] = implode( ', ', array_keys( $auth->getChildren( $role->name ) ) );

				return $permission;

			}, [] )
		] );

	}

	/**
	 * Creates a new role.
	 * If creation is successful, the browser will be redirected to the 'index' page.
	 * @return mixed
	**/
	public function actionCreate()
	{

		//params initializations for current method
		$auth 	 = Yii::$app->authManager;
		$request = Yii::$app->request;

		//is post request
		if ( $request->isPost ) {

			//create new role
			$role 			   = $auth->createRole( $request->post( 'name' ) );
			$role->description = $request->post( 'name' );

			//creating roles in db
			$auth->add( $role );

			//assigning permissions to requested role
			foreach ( $auth->getPermissions() as $key => $value ) {

				if ( ! is_null( $request->post( $value->name ) ) ) {
					$auth->addChild( $role, $value );
				}

			}

			//order permissions details
			$settings = Settings::findOne( [
				'name' => $role->name . '_order_permissions'
			] );

			//assign settings param
			$settings = is_null( $settings )
				? merge_objects( new Settings(), [
					'name' => $role->name . '_order_permissions'
				] )
				: $settings;

			//set settings description option
			$settings->description = json_encode(
				$request->post( 'permissions' )
			);

			//saving permissions details
			if ( $settings->save() ) {

				//set success response message
				Yii::$app->session->setFlash( 'success',
					__( 'Role was created successfully' )
				);

				return $this->redirect( [
					'index'
				] );

			}

		}

		return $this->render( 'create', [
			'model' 	  => [],
			'permissions' => [
				'list' => [
					'general' => $auth->getPermissions(),
					'order'   => [
						'general',
						'expenses_general',
						'expenses_container',
						'expenses_sea_freight',
						'expenses_auto',
						'expenses_railway',
						'expenses_port_forwarding',
						'expenses_terminal',
						'expenses_terminal_costs',
						'expenses_invoices',
						'expenses_invoices_add_delete_options',
						'containers',
						'containers_and_wagons_add_delete_export_options',
						'wagons',
						'customer_information',
						'blockTrain',
						'code_details',
						'code_details_add_delete_options',
						'code_details_expenses',
						'auto',
						'terminals',
						'tracking',
						'invoices',
						'invoices_add_delete_options',
						'payment'
					]
				]
			]
		] );

	}

	/**
	 * Updates an existing Role
	 * If update is successful, the browser will be redirected to the 'index' page.
	 * @param string $name
	 * @return mixed
	**/
	public function actionUpdate( $name )
	{

		// option allowing get|create|update roles and permissions in system
		$auth    = Yii::$app->authManager;
		$model   = $auth->getRole( $name );
		$request = Yii::$app->request;

		//redirecting for admin and client roles requesting
		if ( $name == 'admin'
			|| $name == 'client'
		) {

			$this->redirect(
				'index'
			);

		}

		//is post request
		if ( $request->isPost ) {

			//removing old permissions for current role
			$auth->removeChildren( $model );

			//assigning permissions to requested role
			foreach ( $auth->getPermissions() as $key => $value ) {

				if ( ! is_null( $request->post( $value->name ) ) ) {
					$auth->addChild( $model, $value );
				}

			}

			//order permissions details
			$settings = Settings::findOne( [
				'name' => $model->name . '_order_permissions'
			] );

			//assign settings param
			$settings = is_null( $settings )
				? merge_objects( new Settings(), [
					'name' => $model->name . '_order_permissions'
				] )
				: $settings;

			//set requested permissions to settings
			$settings->description = json_encode(
				$request->post( 'permissions' )
			);

			//saving permissions details
			if ( $settings->save() ) {

				//set success response message
				Yii::$app->session->setFlash( 'success',
					__( 'Role was updated successfully' )
				);

				return $this->redirect( [
					'index'
				] );

			}

		}

		return $this->render( 'update', [
			'model' 	  => $model,
			'permissions' => [
				'list' => [
					'general' => $auth->getPermissions(),
					'order'   => [
						'general',
						'expenses_general',
						'expenses_container',
						'expenses_sea_freight',
						'expenses_auto',
						'expenses_railway',
						'expenses_port_forwarding',
						'expenses_terminal',
						'expenses_terminal_costs',
						'expenses_invoices',
						'expenses_invoices_add_delete_options',
						'containers',
						'containers_and_wagons_add_delete_options',
						'containers_and_wagons_export_options',
						'wagons',
						'customer_information',
						'blockTrain',
						'code_details',
						'code_details_add_delete_options',
						'code_details_expenses',
						'auto',
						'terminals',
						'tracking',
						'invoices',
						'invoices_add_delete_options',
						'payment'
					]
				],
				'selected' => [
					'general' => array_flip( array_keys(
						$auth->getChildren( $model->name )
					) ),
					'order' => Settings::findOne( [
						'name' => $model->name . '_order_permissions'
					] )
				]
			]
		] );

	}

}