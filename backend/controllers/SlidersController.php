<?php namespace backend\controllers;

/**************************************/
/*                                    */
/*         SLIDERS CONTROLLER         */
/*                                    */
/**************************************/

use Yii;

use yii\helpers\ArrayHelper;
use yii\filters\AccessControl;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

use yii\filters\VerbFilter;

use common\behaviors\TranslationBehavior;

use common\models\Files;
use common\models\Posts;
use common\models\PostsSearch;
use common\models\UploadForm;

/**
 * Sliders Controller is the controller behind the Posts model.
**/
class SlidersController extends Controller
{

	/**
	 * @inheritdoc
	**/
	public function behaviors()
	{

		return [

			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'actions' => [
							'create',
							'update',
							'index',
							'delete',
							'upload'
						],
						'allow' => true,
						'roles' => [
							'posts', 'pages'
						]
					]
				]
			],

			'verbs'  => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => [
						'POST'
					]
				]
			],

			'translation' => [
				'class'   => TranslationBehavior::className()
			]

		];

	}

	/**
	 * Lists all Posts models.
	 * @return mixed
	**/
	public function actionIndex()
	{

		//params initialization for current method
		$searchModel   = new PostsSearch();
		$dataProvider  = $searchModel->search( ArrayHelper::merge(
			Yii::$app->request->queryParams, [
				$searchModel->formName() => [
					'type'    => 'slider',
					'lang_id' => is_null( Yii::$app->request->get( 'lang_id', null ) )
						? Yii::$app->common->currentLanguage()
						: Yii::$app->request->get( 'lang_id', null )
				] ]
		) );

		//set thumbnails ids from data provider
		$thumbnail_ids = ! empty( $dataProvider->models )
			? array_reduce( $dataProvider->models, function( $result, $model ) {

				if ( ! empty( $model->description ) ) {

					$slide_details = json_decode(
						$model->description
					);

					if ( isset( $slide_details->file_id[0] ) )
						$result[ $model->id ] = $slide_details->file_id[0];

				}

				return $result;

			}, [] )
			: [];

		return $this->render( 'index', [
			'searchModel'  => $searchModel,
			'dataProvider' => $dataProvider,
			'thumbnails'   => ! empty( $thumbnail_ids )
				? [
					'ids' 	=> $thumbnail_ids,
					'files' => Yii::$app->common->thumbnails( $thumbnail_ids )
				]
				: []
		] );

	}

	/**
	 * Creates a new slider
	 * If creation is successful, the browser will be redirected to the 'index' page.
	 * @return mixed
	**/
	public function actionCreate()
	{

		//params initialization for current method
		$model = new Posts();

		//load requested details
		if ( $model->load( Yii::$app->request->post() ) ) {

			//set description
			$model->description = json_encode(
				Yii::$app->request->post( 'slide', '' )
			);

			//set success response message
			if ( $model->save() ) {

				Yii::$app->session->setFlash( 'success',
					__( 'Slider was created successfully' )
				);

			}

			return $this->redirect( [
				'index'
			] );

		}

		return $this->render( 'create', [
			'model' => $model
		] );

	}

	/**
	 * Updates an existing slider
	 * If update is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id,
	 * @param integer $lang_id
	 *
	 * @return mixed
	 **/
	public function actionUpdate( $id, $lang_id = 0 )
	{

		//params initialization for current method
		$model = $this->findModel( $id );

		//load requested details for current model
		if ( $model->load( Yii::$app->request->post() ) ) {

			//set description
			$model->description = json_encode(
				Yii::$app->request->post( 'slide', '' )
			);

			//set success response message
			if ( $model->save() ) {

				Yii::$app->session->setFlash( 'success',
					__( 'Slider was updated successfully' )
				);

			}

			return $this->redirect( [
				'index'
			] );

		}

		return $this->render( 'update', [
			'model' => $this->translates( $model, [
				'lang_id' => $lang_id > 0
					? $lang_id
					: $model->lang_id,
				'type'	    => 'slider',
				'is_single' => true
			] ),
			'lang_id' => $lang_id > 0
				? $lang_id
				: $model->lang_id
		] );

	}

	/**
	 * Upload new file.
	 * If update is successful, action will return object with file id and url.
	 * @return mixed
	**/
	public function actionUpload()
	{

		//params initializations for current method
		$request = Yii::$app->request;
		$model   = new UploadForm( [
			'path' => '@frontend/web/upload/sliders',
			'url'  => '@frontend_web/upload/sliders'
		] );

		//is ajax request
		if ( $request->isAjax ) {

			//loading requested file details
			$model->load(
				$request->post()
			);

			//change response type to json for ajax callback
			Yii::$app->response->format = Response::FORMAT_JSON;

			//uploading requested file
			if ( $model->upload() ) {

				//file details due to requested file id
				$file = Files::findOne(
					$request->post('file_id' )
				);

				//is requested file exists
				if( is_null( $file ) )
					$file = new Files();

				//updating file details
				$file = merge_objects( $file, [
					'name'  => uniqid(),
					'guide' => $model->getUrl(),
					'size'	=> (string)$model->image->size,
					'type'	=> 'sliders'
				] );

				//saving file
				if ( $file->save() ) {

					return [
						'id'  => $file->id,
						'url' => Yii::getAlias( '@frontend_link' ) . $file->guide
					];

				}

			} else {

				return [
					'return' => json_encode(
						$model->errors
					)
				];

			}
		}

		return $this->redirect( [
			'create'
		] );

	}

	/**
	 * Deletes an existing Posts model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	**/
	public function actionDelete( $id )
	{

		//set success response message
		if ( $this->findModel( $id )->delete() ) {

			Yii::$app->session->setFlash( 'success',
				__( 'Slider was deleted successfully' )
			);

		}

		return $this->redirect( [
			'index'
		] );

	}

	/**
	 * Finds the Posts model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return Posts the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	**/
	protected function findModel( $id )
	{

		if ( ( $model = Posts::findOne( $id ) ) !== null ) {
			return $model;
		} else {

			throw new NotFoundHttpException(
				__( 'The requested page does not exist' )
			);

		}

	}

}