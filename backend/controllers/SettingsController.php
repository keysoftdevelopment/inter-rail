<?php namespace backend\controllers;

/**************************************/
/*                                    */
/*        SETTINGS CONTROLLER         */
/*                                    */
/**************************************/

use common\behaviors\TranslationBehavior;
use Yii;

use yii\filters\AccessControl;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use common\models\Posts;
use common\models\Settings;
use common\models\SettingsForm;

/**
 * Settings Controller is the controller behind the Settings model.
**/
class SettingsController extends Controller
{

    /**
     * @inheritdoc
    **/
    public function behaviors()
    {

        return [

            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [
                        	'create',
							'update',
							'index',
							'delete',
							'upload'
						],
                        'allow' => true,
                        'roles' => [
                        	'settings'
						]
                    ]
                ]
            ],

            'verbs' => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => [
                    	'POST'
					]
                ]
            ],

			'translation' => [
				'class'   => TranslationBehavior::className(),
			]

        ];

    }

	/**
	 * Additional setting parameters
	 * i.e. phone number, email, social network links and etc ...
	 * @return mixed
	**/
    public function actionIndex()
    {

		//params initialization for current method
		$model = new SettingsForm();

		//set update scenario for current model
		$model->setScenario(
			SettingsForm::SCENARIO_UPDATE
		);

		//updating current model with requested details
		if ( $model->load( Yii::$app->request->post() ) ) {
			$model->save();
		}

		//fill in current model
		$model->fill();

		return $this->render( 'index', [
			'model' => $model,
		] );

    }

    /**
     * Creates a new Settings model.
     * If creation is successful, the browser will be redirected to the 'update' page.
     * @param int $lang_id
     * @return mixed
    **/
    public function actionCreate( $lang_id = 0 )
    {

		//params initialization for current method
		$model = new Settings();
        $query = Settings::find()
			-> where( [
            	'name' => 'menu-data'
        	] );

        //pages lists
		$pages = new ActiveDataProvider( [
			'query' => Posts::find()
				-> where( [
					'type' => 'page'
				] )
		] );

        //set default lang id
		$lang_id = $lang_id == 0
			? Settings::findOne( [
				'name' => 'default_language'
			] )
			: $lang_id;

        //set lang id
		$query
			-> andWhere( [
				'lang_id' => isset( $lang_id->description )
					? $lang_id->description
					: $lang_id
		] );

        //menu details due to request details
        $menu = $query->one();

        //set lang id for current model
		$model->lang_id = isset( $lang_id->description )
			? $lang_id->description
			: $lang_id;

        if ( ! is_null( $menu ) ) {

            return $this->redirect( [
            	'update',
				'id' => $menu->id
			] );

        }

		//updating current model with requested details
        if ( $model->load( Yii::$app->request->post() )
			&& $model->save()
		) {

            return $this->redirect( [
                'update',
                'id' => $model->id
            ] );

        }

		return $this->render( 'create', [
			'model' => $model,
			'pages' => merge_objects( $pages, [
				'models' => $this->translates( $pages->models, [
					'lang_id'   => $lang_id,
					'type'	    => 'page',
					'is_single' => false
				] )
			] )
		] );

    }

    /**
     * Updates an existing Settings model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
    **/
    public function actionUpdate( $id )
    {

		//params initialization for current method
        $model = $this->findModel( $id );
		$pages = new ActiveDataProvider( [
			'query' => Posts::find()
				-> where( [
					'type' => 'page'
				] )
		] );

        //set success response message
        if ( $model->load( Yii::$app->request->post() )
			&& $model->save()
		) {

			Yii::$app->session->setFlash( 'success',
				__( 'Settings was updated successfully' )
			);

        }

        return $this->render( 'update', [
            'model' => $model,
			'pages' => merge_objects( $pages, [
				'models' => $this->translates( $pages->models, [
					'lang_id'   => $model->lang_id,
					'type'	    => 'page',
					'is_single' => false
				] )
			] )
        ] );

    }

    /**
     * Finds the Settings model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Settings the loaded model
     * @throws NotFoundHttpException if the model cannot be found
    **/
    protected function findModel( $id )
    {

        if ( ( $model = Settings::findOne( $id ) ) !== null ) {
            return $model;
        } else {

            throw new NotFoundHttpException(
            	__( 'The requested page does not exist' )
			);

        }

    }

}
