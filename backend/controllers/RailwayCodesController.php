<?php namespace backend\controllers;

/**************************************/
/*                                    */
/*      RAILWAY CODES CONTROLLER      */
/*                                    */
/**************************************/

use common\models\RailwayCodes;
use common\models\RailwayCodesSearch;
use Yii;

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

use yii\web\Controller;

use common\models\LogsSearch;
use common\models\Orders;
use common\models\User;

/**
 * Railway Codes Controller is the controller behind the Codes model.
**/
class RailwayCodesController extends Controller
{

	/**
	 * @inheritdoc
	**/
	public function behaviors()
	{

		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'actions' => [
							'index'
						],
						'allow' => true,
						'roles' => [
							'codes'
						]
					]
				]
			]

		];

	}

	/**
	 * Lists all Railway Codes models.
	 * @return mixed
	**/
	public function actionIndex()
	{

		//params initializations for current method
		$searchModel  = new RailwayCodesSearch();
		$dataProvider = $searchModel->search( ArrayHelper::merge(
			Yii::$app->request->queryParams,
			[
				$searchModel->formName() => []
			]
		) );

		return $this->render( 'index', [
			'searchModel'  => $searchModel,
			'dataProvider' => $dataProvider,
			'orders'	   => ! empty( $dataProvider->models )
				? Orders::find()
					-> where( [
						'id' => ArrayHelper::getColumn(
							$dataProvider->models, 'order_id'
						)
					] )
					-> indexBy( 'id' )
					-> asArray()
					-> all()
				: []
		] );

	}

}