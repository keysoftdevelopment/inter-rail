<?php namespace backend\controllers;

/**************************************/
/*                                    */
/*           MEDIA CONTROLLER         */
/*                                    */
/**************************************/

use Yii;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

use common\models\Files;
use common\models\FilesSearch;

/**
 * Files Controller is the controller behind the Files model.
**/
class FilesController extends Controller
{

    /**
     * @inheritdoc
    **/
    public function behaviors()
    {

        return [

            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [
							'index',
							'delete'
						],
                        'allow' => true,
                        'roles' => [
                        	'media'
						]
                    ]
                ]
            ],

            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => [
                    	'POST'
					]
                ]
            ]

        ];

    }

    /**
     * Lists all Files models.
     * @return mixed
    **/
    public function actionIndex()
    {

		//params initializations for current method
        $searchModel                        = new FilesSearch();
        $dataProvider                       = $searchModel->search( Yii::$app->request->queryParams );
        $dataProvider->pagination->pageSize = 15;

        return $this->render( 'index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ] );

    }

    /**
     * Deletes an existing Files model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
    **/
    public function actionDelete( $id )
    {

		//delete requested file
        $this->findModel( $id )->delete();

        return $this->redirect( [
            'index'
        ] );

    }

    /**
     * Finds the Files model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Files the loaded model
     * @throws NotFoundHttpException if the model cannot be found
    **/
    protected function findModel( $id )
    {

        if ( ( $model = Files::findOne( $id ) ) !== null ) {
            return $model;
        } else {

            throw new NotFoundHttpException(
            	__( 'The requested page does not exist' )
			);

        }

    }

}
