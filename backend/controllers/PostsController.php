<?php namespace backend\controllers;

/**************************************/
/*                                    */
/*          POSTS CONTROLLER          */
/*                                    */
/**************************************/

use Yii;

use yii\filters\AccessControl;

use yii\helpers\ArrayHelper;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

use yii\filters\VerbFilter;

use common\behaviors\TranslationBehavior;

use common\models\Files;

use common\models\Posts;
use common\models\PostsSearch;
use common\models\Taxonomies;
use common\models\UploadForm;

/**
 * Posts Controller is the controller behind the Posts model.
**/
class PostsController extends Controller
{

    /**
     * @inheritdoc
    **/
    public function behaviors()
    {

        return [

            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [
                        	'create',
							'update',
							'index',
							'delete',
							'upload'
						],
                        'allow' => true,
                        'roles' => [
                        	'posts'
						]
                    ]
                ]
            ],

            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => [
                    	'POST'
					]
                ]
            ],

			'translation' => [
				'class' => TranslationBehavior::className()
			]

        ];

    }

    /**
     * Lists all Posts models.
     * @return mixed
    **/
    public function actionIndex()
    {

		//params initializations for current method
        $searchModel  = new PostsSearch();
        $dataProvider = $searchModel->search( ArrayHelper::merge(
            Yii::$app->request->queryParams,
            [ $searchModel->formName() => [
                'type' 	  => 'posts',
                'lang_id' => is_null( Yii::$app->request->get( 'lang_id', null ) )
					? Yii::$app->common->currentLanguage()
					: Yii::$app->request->get( 'lang_id', null )
            ] ]
        ) );

        return $this->render( 'index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
            'thumbnails'   => ! empty( $dataProvider->models )
				? Yii::$app->common->thumbnails( ArrayHelper::getColumn(
					$dataProvider->models, 'file_id'
				) )
				: []
        ] );

    }

    /**
     * Creates a new Posts model.
     * If creation is successful, the browser will be redirected to the 'index' page.
     * @return mixed
    **/
    public function actionCreate()
    {

		//params initializations for current method
        $model = new Posts();

		//add new post
        if ( $model->load( Yii::$app->request->post() )
			&& $model->save()
		) {

			//set success response message
			Yii::$app->session->setFlash( 'success',
				__( 'Post was created successfully' )
			);

            return $this->redirect( [
                'index'
            ] );

        }

		return $this->render( 'create', [
			'model'      => $model,
			'categories' => Taxonomies::find()
				-> where( [
					'isDeleted' => false,
					'type'		=> 'posts'
				] )
				-> all(),
		] );

    }

    /**
     * Updates an existing Posts model.
     * If update is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
	 * @param integer $lang_id
     * @return mixed
    **/
    public function actionUpdate( $id, $lang_id = 0 )
    {

		//params initializations for current method
        $model = $this->findModel( $id );

		//updating current model with requested details
        if ( $model->load( Yii::$app->request->post() )
			&& $model->save()
		) {

			//set success response message
			Yii::$app->session->setFlash( 'success',
				__( 'Post was updated successfully' )
			);

            return $this->redirect( [
                'index'
            ] );

        }

		//fill current model with additional information
		$model->customFields();

		//fill current model with taxonomy details
		$model->getTaxonomies();

		return $this->render( 'update', [
			'model' => $this->translates( $model, [
				'lang_id' => $lang_id > 0
					? $lang_id
					: $model->lang_id,
				'type'	    => 'posts',
				'is_single' => true
			] ),
			'lang_id' => $lang_id > 0
				? $lang_id
				: $model->lang_id,
			'categories' => Taxonomies::find()
				-> where( [
					'isDeleted' => false,
					'type'		=> 'posts'
				] )
				-> all()
		] );

    }

	/**
	 * Upload new file.
	 * If update is successful, action will return object with file id and url.
	 * @return mixed
	**/
	public function actionUpload()
	{

		//params initializations for current method
		$request = Yii::$app->request;
		$model 	 = new UploadForm( [
			'path' => '@frontend/web/upload/posts',
			'url'  => '@frontend_web/upload/posts'
		] );

		//is ajax request
		if ( $request->isAjax ) {

			//loading requested file details
			$model->load( $request->post() );

			//change response type to json for ajax callback
			Yii::$app->response->format = Response::FORMAT_JSON;

			//uploading requested file
			if ( $model->upload() ) {

				//file details due to requested file id
				$file = Files::findOne(
					$request->post('file_id' )
				);

				//is requested file exists
				if( is_null( $file ) )
					$file = new Files();

				//updating file details
				$file = merge_objects( $file, [
					'name'  => uniqid(),
					'guide' => $model->getUrl(),
					'size'	=> (string)$model->image->size,
					'type'	=> 'posts'
				] );

				//saving file
				if ( $file->save() ) {

					return [
						'id'  => $file->id,
						'url' => Yii::getAlias( '@frontend_link' ) . $file->guide
					];

				}

			} else {

				return [
					'return' => 'error'
				];

			}
		}

		return $this->redirect( [
			'create'
		] );

	}

    /**
     * Deletes an existing Posts model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
    **/
    public function actionDelete( $id )
    {

		//set success response message
        if ( $this->findModel( $id )->delete() ) {

			Yii::$app->session->setFlash( 'success',
				__( 'Post was deleted successfully' )
			);

        }

        return $this->redirect( [
            'index'
        ] );

    }

    /**
     * Finds the Posts model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Posts the loaded model
     * @throws NotFoundHttpException if the model cannot be found
    **/
    protected function findModel( $id )
    {

        if ( ( $model = Posts::findOne( $id ) ) !== null ) {
            return $model;
        } else {

            throw new NotFoundHttpException(
            	__( 'The requested page does not exist' )
			);

        }

    }

}
