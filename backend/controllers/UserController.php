<?php namespace backend\controllers;

 /**************************************/
 /*                                    */
 /*         USERS CONTROLLER           */
 /*                                    */
/**************************************/

use Yii;

use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Response;

use common\models\Files;
use common\models\Invoices;
use common\models\Orders;
use common\models\User;
use common\models\UserSearch;
use common\models\UploadForm;

/**
 * User Controller implements the CRUD actions for User model.
**/
class UserController extends Controller
{

    /**
     * @inheritdoc
	**/
	public function behaviors()
    {

        return [

            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [
                        	'create',
							'update',
							'index',
							'delete',
							'upload',
						],
                        'allow' => true,
                        'roles' => [
                        	'users'
						]
                    ], [
						'actions' => [
							'update',
							'upload',
						],
						'allow' => true,
						'roles' => [
							'profile'
						]
					]
                ]
            ],

            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => [
                    	'POST'
					],
                ],
            ]

        ];

    }

    /**
     * Lists all User models.
     * @return mixed
	**/
	public function actionIndex()
    {

		//params initializations for current method
        $searchModel                        = new UserSearch();
        $dataProvider                       = $searchModel->search( Yii::$app->request->queryParams );
        $dataProvider->pagination->pageSize = Yii::$app->params[ 'maxPageSize' ];

        return $this->render( 'index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
			'invoices'	   => [
				'paid' => ! empty( $dataProvider->models )
					? Invoices::find()
						-> select( [
							'customer_id',
							'sum(amount) as paid',
						] )
						-> where( [
							'customer_id' => ArrayHelper::getColumn(
								$dataProvider->models, 'id'
							)
						] )
						-> indexBy( 'customer_id' )
						-> groupBy( 'customer_id' )
						-> asArray()
						-> all()
					: [],
				'debt' => ! empty( $dataProvider->models )
					? Orders::find()
						-> select( [
							'user_id',
							'sum(total) as total',
						] )
						-> where( [
							'user_id' => ArrayHelper::getColumn(
								$dataProvider->models, 'id'
							)
						] )
						-> indexBy( 'user_id' )
						-> groupBy( 'user_id' )
						-> asArray()
						-> all()
					: [],
			]
        ] );

    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
	**/
	public function actionCreate()
    {

		//params initializations for current method
        $model = new User();

		//is post request
		if ( Yii::$app->request->isPost  ) {

			//set register scenario
			$model->setScenario(
				$model::SCENARIO_REGISTER
			);

			//add new user
			if ( $model->load( Yii::$app->request->post() ) ) {

				//is request container exists in system
				$is_exists = User::findOne( [
					'email'    => $model->email,
					'isDeleted' => true
				] );

				if ( ! is_null( $is_exists ) ) {

					$model 			  = $this->findModel( $is_exists->id );
					$model->isDeleted = false;

					//set register scenario
					$model->setScenario(
						$model::SCENARIO_REGISTER
					);

					//load user requested details
					$model->load(
						Yii::$app->request->post()
					);

				}

				if ( $model->validate()
					&& $model->save()
				) {

					//set success response message
					Yii::$app->session->setFlash( 'success',
						__( 'User was created successfully' )
					);

					//is ajax request
					if ( Yii::$app->request->isAjax ) {
						return $model->id;
					}

					return $this->redirect( [
						'update',
						'id' => $model->id
					] );

				}

			}

		}

		return $this->render( 'create', [
			'model' => $model
		] );

    }

	/**
	 * Upload new file.
	 * If update is successful, action will return file url.
	 * @return mixed
	 * @throws NotFoundHttpException
	**/
	public function actionUpload()
    {

    	//check user access permissions
		if ( ! Yii::$app->user->can( 'profile' ) ) {

			throw new NotFoundHttpException(
				__( 'The requested page does not exist' )
			);

		}

		//params initializations for current method
		$request = Yii::$app->request;
        $model 	 = new UploadForm( [
            'path' => '@frontend/web/upload/users',
            'url'  => '@frontend_web/upload/users'
        ] );

		//is ajax request
        if ( $request->isAjax ) {

			//loading requested file details
			$model->load(
				$request->post()
			);

			//change response type to json for ajax callback
			Yii::$app->response->format = Response::FORMAT_JSON;

			//uploading requested file
            if ( $model->upload() ) {

				//file details due to requested file id
				$file = Files::findOne(
					$request->post('file_id' )
				);

				//is requested file exists
				if( is_null( $file ) )
					$file = new Files();

				//updating file details
				$file = merge_objects( $file, [
					'name'  => uniqid(),
					'guide' => $model->getUrl(),
					'size'	=> (string)$model->image->size,
					'type'	=> 'users'
				] );

				//saving file
				if ( $file->save() ) {

					return [
						'id'  => $file->id,
						'url' => Yii::getAlias( '@frontend_link' ) . $file->guide
					];

				}

            } else {

                return [
                    'return' => 'error'
                ];

            }
        }

        return $this->redirect( [
            'create'
        ] );

    }

	/**
	 * Updates an existing User model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 * @throws NotFoundHttpException
	**/
	public function actionUpdate( $id = 0 )
    {

		//current user permissions
		$permissions = Yii::$app->authManager->getPermissionsByUser(
			Yii::$app->user->id
		);

		//check user access permissions
    	if ( empty( array_intersect( [
    		$id,
			'users'
		], array_merge(
			array_keys( $permissions ), [
				0,
				Yii::$app->user->id
			] )
		) ) && in_array(
			'profile', array_keys( $permissions )
		) ) {

			throw new NotFoundHttpException(
				__( 'The requested page does not exist' )
			);

    	}

		//params initializations
		$request = Yii::$app->request;
        $model   = $this->findModel( $id == 0
			? Yii::$app->user->id
			: $id
		);

    	//is post request
        if ( $request->isPost ) {

			//set system language due to user selects
			if ( ! empty( $request->post( 'language' ) ) ) {

				Yii::$app->common->setCookies(
					'language',
					$request->post( 'language' ),
					'remove'
				);

			}

        	//set update scenario for current model
            $model->setScenario(
            	$model::SCENARIO_UPDATE
			);

			//updating current model with requested details
			if ( $model->load( $request->post() )
				&& $model->validate()
			) {

				//current user role
				if ( ! Yii::$app->user->can( 'users' ) ) {
					$model->getRole();
				}

				//save user details
				if ( $model->save() ) {

					Yii::$app->session->setFlash( 'success',
						__( 'Profile was updated successfully' )
					);

				}

			}

        }

        //current user company details
		$model->getCompany();

		//current user role
		$model->getRole();

        return $this->render( 'update', [
            'model'  => $model,
            'avatar' => new UploadForm()
        ] );

    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
	**/
    public function actionDelete( $id )
    {

		//set success response message
        if ( $this->findModel( $id )->delete() ) {

			Yii::$app->session->setFlash( 'success',
				__( 'User was deleted successfully' )
			);

        }

        return $this->redirect( [
            'index'
        ] );

    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
    **/
    protected function findModel( $id )
    {

        if ( ( $model = User::findOne( $id ) ) !== null ) {
            return $model;
        } else {

            throw new NotFoundHttpException(
            	__( 'The requested page does not exist' )
			);

        }

    }

}
