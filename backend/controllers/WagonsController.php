<?php namespace backend\controllers;

/**************************************/
/*                                    */
/*        	WAGONS CONTROLLER         */
/*                                    */
/**************************************/

use common\models\Orders;
use common\models\WagonsRelation;
use PHPExcel_IOFactory;
use Yii;

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

use common\behaviors\TranslationBehavior;

use common\models\Wagons;
use common\models\WagonsSearch;
use common\models\Taxonomies;
use yii\web\UploadedFile;

/**
 * Wagons Controller is the controller behind the Wagons model.
**/
class WagonsController extends Controller
{

	/**
	 * @inheritdoc
	**/
	public function behaviors()
	{

		return [

			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'actions' => [
							'create',
							'update',
							'index',
							'import',
							'delete'
						],
						'allow' => true,
						'roles' => [
							'wagons'
						]
					]
				]
			],

			'verbs'  => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => [
						'POST'
					]
				]
			],

			'translation' => [
				'class' => TranslationBehavior::className(),
			]

		];

	}

	/**
	 * Lists all wagons.
	 * @return mixed
	**/
	public function actionIndex()
	{

		//params initializations for current method
		$searchModel  = new WagonsSearch();
		$dataProvider = $searchModel->search(
			Yii::$app->request->queryParams
		);

		return $this->render( 'index', [
			'searchModel'  => $searchModel,
			'dataProvider' => $dataProvider,
			'types'		   => Yii::$app->common->taxonomyDetails( 'wagons' )
		] );

	}

	/**
	 * Creates a new container.
	 * If creation is successful, the browser will be redirected to the 'index' page.
	 * @return mixed
	**/
	public function actionCreate()
	{

		//params initializations for current method
		$model   = new Wagons();
		$request = Yii::$app->request;

		//create new item for current model due to requested details
		if ( $model->load(
			$request->post()
		) ) {

			//is request container exists in system
			$is_exists = Wagons::findOne( [
				'number'    => $model->number,
				'isDeleted' => true
			] );

			if ( ! is_null( $is_exists ) ) {

				$model 			  = $this->findModel( $is_exists->id );
				$model->isDeleted = false;

				//load wagon requested details
				$model->load(
					$request->post()
				);

			}

			if ( $model->save() ) {

				//is ajax request
				if ( $request->isAjax ) {

					//change response type to json for ajax callback
					Yii::$app->response->format = Response::FORMAT_JSON;

					return [
						'code' 	   => 200,
						'status'   => 'success',
						'response' => [
							'id' 	 => $model->id,
							'number' => $model->number
						]
					];

				}

				//set success response message
				Yii::$app->session->setFlash( 'success',
					__( 'Wagon was created successfully' )
				);

				return $this->redirect( [
					'index'
				] );

			}

		}

		return $this->render( 'create', [
			'model' => $model,
			'types' => Taxonomies::find()
				-> where( [
					'type'		=> 'railway',
					'isDeleted' => false
				] )
				-> all()
		] );

	}

	/**
	 * Updates an existing container due to requested container id
	 * If update is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	**/
	public function actionUpdate( $id )
	{

		//params initializations for current method
		$model = $this->findModel( $id );

		if ( $model->load( Yii::$app->request->post() )
			&& $model->save()
		) {

			//set success response message
			Yii::$app->session->setFlash( 'success',
				__( 'Wagon was updated successfully' )
			);

			return $this->redirect( [
				'index'
			] );

		}

		//fill inn current model with taxonomy
		$model->fillType();

		return $this->render( 'update', [
			'model' => $model,
			'types' => Taxonomies::find()
				-> where( [
					'type'		=> 'railway',
					'isDeleted' => false
				] )
				-> all()
		] );

	}

	/**
	 * Import container details.
	 * @return mixed
	 * @throws NotFoundHttpException
	 **/
	public function actionImport()
	{

		//params initializations for current method
		$file 	   = UploadedFile::getInstanceByName( 'xlsx_file' );
		$file_name = Yii::getAlias( '@backend/web/' . $file->baseName . '.' . $file->extension );
		$order	   = ! is_null( Yii::$app->getRequest()->getQueryParam( 'order_id' ) )
			? Orders::findOne(
				Yii::$app->getRequest()->getQueryParam( 'order_id' )
			)
			: NULL;

		//saving requested file
		$file->saveAs( $file_name );

		//xlsx file details
		$xlsx_sheets = PHPExcel_IOFactory::load( $file_name );

		foreach ( $xlsx_sheets->getAllSheets() as $key => $sheet ) {

			//xlsx import details
			$import_details = [];

			for ( $row = ! is_null( $order ) ? 10 : 4; $row <= $sheet->getHighestRow( 'A' ); $row++ ) {

				$import_details[] = [
					'number' => $sheet->getCell( 'B' . $row )->getValue(),
					'owner'  => $sheet->getCell( 'C' . $row )->getValue()
				];

			}

			if ( ! empty( $import_details )
				&& ! is_null( $order )
			) {

				//fill order with appropriate details
				$order->fillModel();

				//wagons details due to requested imported wagon numbers
				$wagons = Wagons::find()
					-> where( [
						'number' => ArrayHelper::getColumn(
							$import_details,
							'number'
						),
						'isDeleted' => false
					] )
					-> indexBy( 'number' )
					-> asArray()
					-> all();

				//remove all wagon relations with current order
				WagonsRelation::deleteAll( [
					'foreign_id' => $order->id,
					'type'		 => 'order'
				] );

				foreach ( $import_details as $wagon ) {

					//wagon main properties
					$current_wagon = merge_objects( isset( $wagons[ $wagon[ 'number' ] ] )
						? Wagons::findOne(
							$wagons[ $wagon[ 'number' ] ][ 'id']
						)
						: new Wagons(), [
						'number' => (string)$wagon[ 'number' ],
						'owner'  => $wagon[ 'owner' ],
						'type'	 => $order->wagons_type
					] );

					//wagon update
					if ( $current_wagon->save() ) {

						//assign container relations
						$wagons_rel = merge_objects( new WagonsRelation(), [
							'foreign_id' => $order->id,
							'wagon_id'   => $current_wagon->id,
							'type'		 => 'order'
						] );

						//update wagons relations
						$wagons_rel->save();

					}

				}

			}

		}

		//remove saved file
		unlink( $file_name );

		//change response type to json for ajax callback
		Yii::$app->response->format = Response::FORMAT_JSON;

		return [
			'code'	  => 200,
			'status'  => 'success',
			'message' => __( 'Wagons Details Successfully imported' )
		];

	}

	/**
	 * Deletes an existing container due to requested container id.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	**/
	public function actionDelete( $id )
	{

		//set success response message
		if ( $this->findModel( $id )->delete() ) {

			Yii::$app->session->setFlash( 'success',
				__( 'Wagon was deleted successfully' )
			);

		}

		return $this->redirect( [
			'index'
		] );

	}

	/**
	 * Finds the Wagons model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return Wagons the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	**/
	protected function findModel( $id )
	{

		if ( ( $model = Wagons::findOne( $id ) ) !== null ) {
			return $model;
		} else {

			throw new NotFoundHttpException(
				__( 'The requested page does not exist' )
			);

		}

	}

}