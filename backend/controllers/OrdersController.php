<?php namespace backend\controllers;

/**************************************/
/*                                    */
/*         ORDERS CONTROLLER          */
/*                                    */
/**************************************/

use Yii;

use PHPExcel_IOFactory;

use kartik\mpdf\Pdf;

use yii\filters\AccessControl;
use yii\helpers\FileHelper;
use yii\filters\VerbFilter;

use yii\helpers\ArrayHelper;
use yii\helpers\Url;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;

use common\models\ContainersRelation;
use common\models\Containers;
use common\models\Expenses;
use common\models\Files;
use common\models\Invoices;
use common\models\LogisticsRelation;
use common\models\Logistics;
use common\models\Orders;
use common\models\OrdersSearch;
use common\models\RailwayCodes;
use common\models\Settings;
use common\models\Taxonomies;
use common\models\TaxonomyRelation;
use common\models\TerminalStorage;
use common\models\Terminals;
use common\models\Tracking;
use common\models\UploadForm;
use common\models\UserCustomFields;
use common\models\User;
use common\models\Wagons;
use common\models\WagonsRelation;

/**
 * Orders Controller is the controller behind the Orders model.
**/
class OrdersController extends Controller
{

    /**
     * @inheritdoc
    **/
    public function behaviors()
    {

        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [
							'update',
							'index',
							'delete',
							'import',
							'tracking-import',
							'export-pdf',
							'exports'
						],
                        'allow' => true,
                        'roles' => [
                        	'orders'
						]
                    ], [
						'actions' => [
							'update',
							'index',
							'tracking-import'
						],
						'allow' => true,
						'roles' => [
							'ownOrders'
						]
					], [
						'actions' => [
							'export-pdf'
						],
						'allow' => true,
						'roles' => [
							'terminals'
						]
					]
                ]
            ],

            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => [
                    	'POST'
					]
                ]
            ]

        ];

    }

	/**
	 * Lists all Orders.
	 * @return mixed
	 * @throws NotFoundHttpException
	**/
    public function actionIndex()
    {

		//current user permissions
		$permissions = Yii::$app->authManager->getPermissionsByUser(
			Yii::$app->user->id
		);

		//check user access permissions
        if ( empty( array_intersect( [
			'orders',
			'ownOrders'
		], array_keys( $permissions ) ) ) ) {

			throw new NotFoundHttpException(
				__( 'The requested page does not exist' )
			);

		}

		//params initializations for current method
        $searchModel  = new OrdersSearch();
		$status 	  = Yii::$app->request->get( 'status', null );
        $dataProvider = $searchModel->search( ArrayHelper::merge(
            Yii::$app->request->queryParams, [
                $searchModel->formName() => Yii::$app->user->can( 'orders' )
					? [
						'status' => $status == null
							? 'prequote'
							: $status
					] : [
						'manager_id'  => Yii::$app->user->id,
						'contractors' => Yii::$app->user->id
					]
        ] ) );

		//set pagination
		$dataProvider->pagination->pageSize = 21;

        return $this->render( 'index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
			'users'		   => ! empty( $dataProvider->models )
				? User::find()
					-> where( [
						'id' => ArrayHelper::merge(
							ArrayHelper::getColumn(
								$dataProvider->models,
								'user_id'
							),
							ArrayHelper::getColumn(
								$dataProvider->models,
								'manager_id'
							)
						),
						'isDeleted' => false
					] )
					-> indexBy( 'id' )
					-> asArray()
					-> all()
				: [],
			'status_count' => Orders::find()
				-> select( [
					'status',
					'count' => 'count(*)'
				] )
				-> indexBy( 'status' )
				-> groupBy( 'status' )
				-> asArray()
				-> all(),
			'notes' => ! empty( $dataProvider->models )
				? UserCustomFields::find()
					-> select( [
						'user_id',
						'description'
					] )
					-> where( [
						'user_id' => ArrayHelper::getColumn(
							$dataProvider->models,'manager_id'
						)
					] )
					-> andFilterWhere( [
						'like', 'name', 'noteForOrder'
					] )
					-> indexBy( 'name' )
					-> asArray()
					-> all()
				: []
        ] );

    }

	/**
	 * Updates an existing Orders model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 * @throws NotFoundHttpException
	**/
	public function actionUpdate( $id )
	{

		//params initializations for current method
		$request    	 = Yii::$app->request;
		$model      	 = $this->findModel( $id );
		$containers 	 = $request->post( 'containers', [] );
		$wagons			 = $request->post( 'wagons', [] );
		$model->codes 	 = $request->post( 'codes', [] );
		$model->tracking = $request->post( 'tracking', [] );
		$status			 = 'on way';
		$current_role    = Yii::$app->authManager->getRolesByUser(
			Yii::$app->user->id
		);

		//check user access permissions
		if ( ! Yii::$app->user->can( 'orders' )
			&& Yii::$app->user->can( 'ownOrders' )
			&& $model->manager_id != Yii::$app->user->id
			&& ! in_array(
				Yii::$app->user->id, explode( ',', $model->contractors )
			)
		) {

			throw new NotFoundHttpException(
				__( 'The requested page does not exist' )
			);

		}

		//logistic relations for block train
		$logistic_relation = LogisticsRelation::findOne( [
			'foreign_id' => $id,
			'type' 	     => [
				'blockTrain',
				'train'
			]
		] );

		//block train due to order id
		$blockTrain = ! is_null( $logistic_relation )
			? Logistics::findOne(
				$logistic_relation->logistic_id
			)
			: new Logistics();

		//update order details
		if ( $request->isPost
			&& $model->load( $request->post() )
		) {

			//uploading gu file
			$gu_file = $this->uploads(
				Yii::$app->request, $model
			);

			//assign gu file id
			$model->gu_file_id = (int)$gu_file > 0
				? (int)$gu_file
				: $model->gu_file_id;

			if ( ! empty( $containers ) ) {

				//remove all container relations
				ContainersRelation::deleteAll( [
					'foreign_id' => $id,
					'type'		 => 'order'
				] );

				//remove all wagon relations with container
				WagonsRelation::deleteAll( [
					'foreign_id' => $containers[ 'id' ],
					'type'		 => 'container-'. $id
				] );

				if ( ! empty( $model->terminal_id ) ) {
					$status = 'terminal';
				}

				//update containers with appropriate status
				Containers::updateAll( [
					'status' => strtolower( $model->status ) == 'complete'
						? 'complete'
						: $status
				], [
					'id' => $containers[ 'id' ]
				] );

				foreach ( $containers[ 'id' ] as $key => $container ) {

					//assign container relations
					$containers_rel = merge_objects( new ContainersRelation(), [
						'foreign_id' => $id,
						'container_id'   => $container,
						'type'		 => 'order'
					] );

					if ( $containers_rel->save()
						&& isset( $containers[ 'wagon' ][ $key ] )
					) {

						//assign wagon relations
						$wagon_rel = merge_objects( new WagonsRelation(), [
							'foreign_id' => $container,
							'wagon_id'   => $containers[ 'wagon' ][ $key ],
							'type'		 => 'container-'. $id
						] );

						//save wagon relations
						$wagon_rel->save();

					}

				}

			}

			if ( ! empty( $wagons ) ) {

				//remove all wagon relations with container
				WagonsRelation::deleteAll( [
					'foreign_id' => $id,
					'type'		 => 'order'
				] );

				foreach ( $wagons as $wagon ) {

					//assign wago relations
					$wagon_rel = merge_objects( new WagonsRelation(), [
						'foreign_id' => $id,
						'wagon_id'   => $wagon,
						'type'		 => 'order'
					] );

					//save wagon relations
					$wagon_rel->save();

				}

			}

			if ( $blockTrain->load( $request->post() )
				&& isset( $blockTrain->transport_no )
				&& ! empty( $blockTrain->transport_no )
			) {

				//find current block train details
				$current_bt = Logistics::findOne( [
					'transport_no'   => $blockTrain->transport_no,
					'transportation' => $model->transportation,
					'type'  		 => ! empty( $blockTrain->transport_ind )
						? 'blockTrain'
						: 'train'
				] );

				//block train details
				$block_train = merge_objects( ! is_null( $current_bt )
					? Logistics::findOne( $current_bt->id )
					: new Logistics(), [
						'transport_no'  => $blockTrain->transport_no,
						'transport_ind' => ! empty( $blockTrain->transport_ind )
							? $blockTrain->transport_ind
							: NULL,
						'transportation'  => $model->transportation,
						'railway_details' => ! empty( $blockTrain->railway_details )
							? $blockTrain->railway_details
							: $blockTrain->railway_details . '' . implode( ',', Wagons::find()
								-> select( 'number' )
								-> where( [
									'id' => array_replace(
										$wagons, isset( $containers[ 'wagon' ] )
										? $containers[ 'wagon' ]
										: []
									)
								] )
								-> asArray()
								-> indexBy( 'number' )
								-> groupBy( 'number' )
								-> column()
							),
						'type' => ! empty( $blockTrain->transport_ind )
							? 'blockTrain'
							: 'train'
					]
				);

				//save train details
				if ( $block_train->save() ) {

					$model->logistics[ ! empty( $block_train->transport_ind )
						? 'blockTrain'
						: 'train'
					] = $block_train->id;

				}

			}

			$model->save( false );

			Yii::$app->session->setFlash(
				'success',
				__(  'Order was updated successfully' )
			);

			return $this->redirect( Url::to( [
				'/orders',
				'status' => $model->status
			] ) );

		}

		//fill current model with additional information
		$model->fillModel();

		//fill current model with logistics information
		$model->getLogistics();

		return $this->render( 'update', [
			'model'      => $model,
			'blockTrain' => $blockTrain,
			'expenses'   => Expenses::find()
				-> where( [
					'order_id' => $id
				] )
				-> indexBy( 'id' )
				-> asArray()
				-> all(),
			'logistics' => ! empty( Yii::$app->common->logisticTypes() )
				? array_reduce( Logistics::findAll( [
					'type' => ArrayHelper::getColumn(
						Yii::$app->common->logisticTypes(),'slug'
					)
				] ), function( $filter, $logistic ) {

					if ( ! empty( $logistic ) )
						$filter[ $logistic->type ][ $logistic->id ] = [
							'id'    => $logistic->id,
							'route' => $logistic->from . '-' . $logistic->to,
							'rate'  => $logistic->rate,
						];

					return $filter;

				}, [] )
				: [],
			'containers' => ! empty( $model->containers_type )
				? Containers::findAll( [
					'id'   		=> Yii::$app->common->taxonomyDetails( 'containers', $model->containers_type ),
					'isDeleted' => false
				] )
				: Containers::findAll( [
					'isDeleted' => false
				] ),
			'wagons' => ! empty( $model->wagons_type )
				? Wagons::findAll( [
					'id' => Yii::$app->common->taxonomyDetails(
						'wagons', $model->wagons_type
					),
					'isDeleted' => false
				] )
				: Wagons::findAll( [
					'isDeleted' => false
				] ),
			'expenses_sum' => Expenses::find()
				->	select( [
					'sum(total) as total'
				] )
				-> where( [
					'order_id' => $id
				] )
				-> column(),
			'invoices' => Invoices::find()
				-> where( [
					'foreign_id' => $model->id,
					'type'	     => 'Lotus'
				] )
				-> all(),
			'permissions' => ! empty( $current_role )
				? [
					'role'   => key( $current_role ),
					'access' => Settings::findOne( [
						'name' => key( $current_role ) . '_order_permissions'
					] )
				]
				: []
		] );

	}

	/**
	 * Order details importing
	 * @return mixed
	**/
	public function actionImport()
	{

		//params initializations
		$file 	  = UploadedFile::getInstanceByName( 'xlsx_file' );
		$fileName = Yii::getAlias( '@backend/web/' . $file->baseName . '.' . $file->extension  );

		//saving requested file
		$file->saveAs( $fileName );

		//xlsx file details
		$xlsx_sheets = PHPExcel_IOFactory::load( $fileName );

		foreach ( $xlsx_sheets->getAllSheets() as $key => $sheet ) {

			$orders = [];

			for ( $row = 2; $row <= $sheet->getHighestRow( 'A' ); $row++ ) {

				$user_id = (int)$sheet
					-> getCell( 'B' . $row )
					-> getValue();

				$container_type = $sheet
					-> getCell( 'D' . $row )
					-> getValue();

				$bt_number = $sheet
					-> getCell( 'N' . $row )
					-> getValue();

				$expenses = [
					'rate' => [
						(float)trim( $sheet
							-> getCell( 'Q' . $row )
							-> getValue()
						),
						(float)trim( $sheet
							-> getCell( 'R' . $row )
							-> getValue()
						)
					],
					'surcharge' => [
						(float)trim( $sheet
							-> getCell( 'S' . $row )
							-> getValue()
						),
						(float)trim( $sheet
							-> getCell( 'S' . $row )
							-> getValue()
						),
					]
				];

				$expenses_total = array_reduce( array_merge( $expenses[ 'rate' ], $expenses[ 'surcharge' ] ), function( $result, $expenses ){

					$result += $expenses;

					return $result;

				}, 0 );

				if ( ! isset( $orders[ $user_id . $container_type . $bt_number ] ) ) {
					$containers = $wagons = [];
				}

				$containers[] = [
					'number' =>  $sheet
						-> getCell( 'C' . $row )
						-> getValue(),
					'type'     => $container_type,
					'expenses' => [
						'route' => [
							'KZH',
							'UZB'
						],
						'rate' 	    => $expenses[ 'rate' ],
						'surcharge' => $expenses[ 'surcharge' ],
						'total'     => $expenses_total
					]
				];

				$wagons[] = [
					'number' => $sheet
						-> getCell( 'K' . $row )
						-> getValue(),
					'type' => $sheet
						-> getCell( 'V' . $row )
						-> getValue()
				];

				$total = (float)trim( $sheet
					-> getCell( 'P' . $row )
					-> getValue()
				);

				$orders[ $user_id . $container_type . $bt_number ] = [
					'user_id' => $user_id,
					'from'    => $sheet
						-> getCell( 'F' . $row )
						-> getValue(),
					'to' => $sheet
						-> getCell( 'G' . $row )
						-> getValue(),
					'delivery_terms' => json_encode( [
						'from' => $sheet
							-> getCell( 'T' . $row )
							-> getValue(),
						'to' => $sheet
							-> getCell( 'U' . $row )
							-> getValue(),
					] ),
					'containers_type' => $container_type,
					'packages' 	      => json_encode( [
						'quantity' => isset( $orders[ $user_id . $container_type . $bt_number ] )
							? count( $orders[ $user_id . $container_type . $bt_number ][ 'containers' ] ) + 1
							: 1,
						'type' => $sheet
							-> getCell( 'E' . $row )
							-> getValue()
					] ),
					'consignee' => $sheet
						-> getCell( 'M' . $row )
						-> getValue(),
					'cargo_tax' => $sheet
						-> getCell( 'J' . $row )
						-> getValue(),
					'shipper' => $sheet
						-> getCell( 'L' . $row )
						-> getValue(),
					'containers' 	    => $containers,
					'wagons'	 	    => $wagons,
					'weight_dimensions' => json_encode( [
						'net' => (float)trim( $sheet
							-> getCell( 'H' . $row )
							-> getValue()) * 1000,
						'gross' => (float)trim( $sheet
							-> getCell( 'I' . $row )
							-> getValue()) * 1000
					] ),
					'transportation' => $sheet
						-> getCell( 'W' . $row )
						-> getValue(),
					'bt' => [
						'transport_no' => $bt_number,
						'transport_ind' => $sheet
							-> getCell( 'O' . $row )
							-> getValue(),
						'transportation' => $sheet
							-> getCell( 'W' . $row )
							-> getValue()
					],
					'codes' => [ [
						'country'   => 'Казакстан',
						'route'     => 'KZH',
						'surcharge' => array_reduce( $containers, function( $result, $container ){

							if( ! empty( $container[ 'expenses' ] ) ) {
								$result += $container[ 'expenses' ][ 'surcharge' ][ 0 ];
							}

							return $result;
						}, 0 ),
						'rate' => array_reduce( $containers, function( $result, $container ){

							if( ! empty( $container[ 'expenses' ] ) ) {
								$result += $container[ 'expenses' ][ 'rate' ][ 0 ];
							}

							return $result;
						}, 0 )
					], [
						'country'   => 'Узбекистан',
						'route'     => 'UZB',
						'surcharge' => array_reduce( $containers, function( $result, $container ){

							if( ! empty( $container[ 'expenses' ] ) ) {
								$result += $container[ 'expenses' ][ 'surcharge' ][ 1 ];
							}

							return $result;
						}, 0 ),
						'rate' => array_reduce( $containers, function( $result, $container ){

							if( ! empty( $container[ 'expenses' ] ) ) {
								$result += $container[ 'expenses' ][ 'rate' ][ 1 ];
							}

							return $result;
						}, 0 )
					] ],
					'surcharge' => isset( $orders[ $user_id . $container_type . $bt_number ] )
						? $orders[ $user_id . $container_type . $bt_number ][ 'surcharge' ] + ( $total - $expenses_total )
						: $total - $expenses_total,
					'total' => isset( $orders[ $user_id . $container_type . $bt_number ] )
						? $orders[ $user_id . $container_type . $bt_number ][ 'total' ] + $total
						: $total
				];

			}

			foreach ( $orders as $order ) {

				//create new containers and wagons
				foreach ( $order[ 'containers' ] as $key => $value ) {

					//find container by requested number
					$current_container = Containers::findOne( [
						'number' => $value[ 'number' ]
					] );

					//container details
					$current_container = merge_objects( is_null( $current_container )
						? new Containers()
						: $current_container, [
						'number' => $value[ 'number' ],
						'type'   => $value[ 'type' ],
						'status' => 'on way'
					] );

					if ( $current_container->save() ) {

						//set container id to
						$order[ 'containers' ][ $key ][ 'id' ] = $current_container->id;

						if ( isset( $order[ 'wagons' ][ $key ] ) ) {

							//find wagon by requested number
							$current_wagon = Wagons::findOne( [
								'number' => (string)$order[ 'wagons' ][ $key ][ 'number' ]
							] );

							//wagon details
							$current_wagon = merge_objects( is_null( $current_wagon )
								? new Wagons()
								: $current_wagon, [
								'number' => (string)$order[ 'wagons' ][ $key ][ 'number' ],
								'type' 	  => $order[ 'wagons' ][ $key ][ 'type' ]
							] );

							if ( $current_wagon->save() ) {

								//set container id to
								$order[ 'wagons' ][ $key ][ 'id' ] = $current_wagon->id;

							}

						}

					}

				}

				//is block train exists
				$block_train = Logistics::findOne( [
					'transport_no'  => $order[ 'bt' ][ 'transport_no' ],
					'transport_ind' => $order[ 'bt' ][ 'transport_ind' ],
					'type' 			=> 'blockTrain'
				] );

				//
				$block_train = merge_objects( is_null( $block_train )
					? new Logistics()
					: $block_train,[
					'from'			  => $order[ 'from' ],
					'to'			  => $order[ 'to' ],
					'transport_no'    => (string)$order[ 'bt' ][ 'transport_no' ],
					'transport_ind'   => $order[ 'bt' ][ 'transport_ind' ],
					'status'		  => 'laden',
					'railway_details' => implode( ',', array_column( $order[ 'wagons' ], 'numbers' ) ),
					'transportation'  => $order[ 'transportation' ],
					'type' 		      => 'blockTrain'
				] );

				//save block train info
				$block_train->save( false );

				//set bt id
				$order[ 'bt' ][ 'id' ] = $block_train->id;

				//order details
				$current_order = merge_objects( new Orders(), [
					'trans_from'        => $order[ 'from' ],
					'trans_to'          => $order[ 'to' ],
					'transportation'    => $order[ 'transportation' ],
					'weight_dimensions' => $order[ 'weight_dimensions' ],
					'shipper' 			=> $order[ 'shipper' ],
					'cargo_tax' 		=> $order[ 'cargo_tax' ],
					'consignee' 		=> $order[ 'consignee' ],
					'packages' 			=> $order[ 'packages' ],
					'containers_type' 	=> $order[ 'containers_type' ],
					'delivery_terms' 	=> $order[ 'delivery_terms' ],
					'user_id' 			=> $order[ 'user_id' ],
					'codes' 			=> $order[ 'codes' ],
					'surcharge' 		=> $order[ 'surcharge' ],
					'total' 			=> $order[ 'total' ]
				] );

				//set block train id
				$current_order->logistics[ 'blockTrain' ] = $block_train->id;

				//save current order
				$current_order->save( false );

				if ( ! empty( $order[ 'containers' ] )
					&& isset( $current_order->id )
				) {

					//remove all container relations
					ContainersRelation::deleteAll( [
						'foreign_id' => $current_order->id,
						'type'		 => 'order'
					] );

					//remove all wagon relations with container
					WagonsRelation::deleteAll( [
						'foreign_id' => array_column( $order[ 'containers' ], 'id' ),
						'type'		 => 'container-'. $current_order->id
					] );

					foreach ( $order[ 'containers' ] as $key => $container ) {

						//assign container relations
						$containers_rel = merge_objects( new ContainersRelation(), [
							'foreign_id'   => $current_order->id,
							'container_id' => $container[ 'id' ],
							'type'		   => 'order'
						] );

						if ( $containers_rel->save()
							&& isset( $order[ 'wagons' ][ $key ] )
						) {

							//assign wagon relations
							$wagon_rel = merge_objects( new WagonsRelation(), [
								'foreign_id' => $container[ 'id' ],
								'wagon_id'   => $order[ 'wagons' ][ $key ][ 'id' ],
								'type'		 => 'container-'. $current_order->id
							] );

							//save wagon relations
							$wagon_rel->save();

						}

						//assign new option
						$expenses = merge_objects( new Expenses(), [
							'container_id' => $container[ 'id' ],
							'order_id'	   => $current_order->id,
							'rf_details'   => json_encode( [
								'route'     => $container[ 'expenses' ][ 'route' ],
								'rate'      => $container[ 'expenses' ][ 'rate' ],
								'surcharge' => $container[ 'expenses' ][ 'surcharge' ],
							] ),
							'total' => $container[ 'expenses' ][ 'total' ]
						] );

						$expenses->save( false );

					}

				}

			}

		}

		unlink( $fileName );

		//change response type to json for ajax callback
		Yii::$app->response->format = Response::FORMAT_JSON;

		return [
			'code'	  => 200,
			'status'  => 'success',
			'message' => __( 'Order details was successfully imported' ),
		];

	}

	/**
	 * Option allowing tracking details import through xlsx file upload
	 * @param integer $order_id
	 * @return mixed
	**/
	public function actionTrackingImport( $order_id )
	{

		//params initializations
		$file 	  = UploadedFile::getInstanceByName( 'xlsx_file' );
		$fileName = Yii::getAlias( '@backend/web/' . $file->baseName . '.' . $file->extension  );

		//saving requested file
		$file->saveAs( $fileName );

		//xlsx file details
		$xlsx_sheets = PHPExcel_IOFactory::load( $fileName );

		//block train id due to requested order id
		$bt_id = LogisticsRelation::findOne( [
			'foreign_id' => $order_id,
			'type'		 => 'blockTrain'
		] );

		//wagon ids due to requested order id
		$wagon_ids = WagonsRelation::find()
			-> where( [
				'foreign_id' => $order_id,
				'type' 		 => 'order'
			] )
			-> asArray()
			-> all();

		if ( ! is_null( $bt_id )
			&& ! empty( $wagon_ids )
		) {

			//block train details
			$blockTrain = Logistics::findOne( [
				'id'   => $bt_id,
				'type' => 'blockTrain'
			] );

			//wagons lists details
			$wagons = Wagons::find()
				-> select( [ 'id', 'number' ] )
				-> where( [
					'id' => ArrayHelper::getColumn(
						$wagon_ids, 'wagon_id'
					),
					'isDeleted' => false
				] )
				-> asArray()
				-> column();

			if ( ! is_null( $blockTrain ) ) {

				foreach ( $xlsx_sheets->getAllSheets() as $key => $sheet ) {

					for ( $row = 2; $row <= $sheet->getHighestRow( 'L' ); $row++ ) {

						if ( $sheet->getCell( 'L' . $row )->getValue() == $blockTrain->transport_ind
							&& in_array( $sheet->getCell( 'B' . $row )->getValue(), $wagons )
						) {

							//tracking details
							$tracking = merge_objects( new Tracking(), [
								'foreign_id' => $order_id,
								'name' 		 => $sheet
									-> getCell( 'H' . $row )
									-> getValue(),
								'description' => $sheet
									-> getCell( 'F' . $row )
									-> getValue(),
								'check_in_date' => $sheet
									-> getCell( 'G' . $row )
									-> getValue(),
								'trans_from' => $sheet
									-> getCell( 'D' . $row )
									-> getValue(),
								'trans_to' => $sheet
									-> getCell( 'E' . $row )
									-> getValue(),
								'departure' => $sheet
									-> getCell( 'C' . $row )
									-> getValue(),
								'arrival' => $sheet
									-> getCell( 'J' . $row )
									-> getValue(),
								'distance' => $sheet
									-> getCell( 'K' . $row )
									-> getValue(),
								'type' => 'order'

							] );

							//saving tracking
							if ( $tracking->save() )
								continue;

						}

					}

				}

			}

		}

		unlink( $fileName );

		//change response type to json for ajax callback
		Yii::$app->response->format = Response::FORMAT_JSON;

		return [
			'code'	  => 200,
			'status'  => 'success',
			'message' => __( 'Tracking details was successfully imported' ),
		];

	}

	/**
	 * @param $order_id
	 * @return array
	 * @throws NotFoundHttpException
	**/
	public function actionExportPdf( $order_id )
	{

		//params initializations
		$request       = Yii::$app->request;
		$pdf_files     = [];
		$upload_path   = Yii::getAlias( '@backend' ) . '/web/reports/terminals/';
		$is_vgm		   = $request->post( 'is_vgm' );
		$container_ids = $request->post( 'container_ids' );
		$response      = [
			'code'    => 400,
			'status'  => __( 'Bad Request' ),
			'message' => __( 'Please modify params and try again' )
		];

		//allow only ajax requests
		if ( ! Yii::$app->request->isAjax ) {

			throw new NotFoundHttpException(
				__( 'The requested page does not exist' )
			);

		}

		if ( ! empty( $request->post( 'terminal_id' )
			&& ! empty( $request->post( 'container_ids' ) ) )
		) {

			//terminal rentals period
			$rentals = array_reduce( TerminalStorage::find()
				-> where( [
					'container_id' => $container_ids,
					'order_id'	   => $order_id,
					'terminal_id'  => $request->post( 'terminal_id' )
				] )
				-> all(), function( $result, $rental ) {

				if ( !empty( $rental ) ) {

					$result[ $rental->from ] = array_replace( ! is_null( $result[ $rental->from ] )
						? $result[ $rental->from ]
						: [], [
						'from'		    => $rental->from,
						'to'		    => $rental->to,
						'container_ids' => isset( $result[ $rental->from ] )
							? $result[ $rental->from ][ 'container_ids' ] . ',' . $rental->container_id
							: $rental->container_id
					] );

				}

				return $result;

			}, [] );

			if ( ! empty( $rentals ) ) {

				//current order details
				$model = $this->findModel( $order_id );
				$model = merge_objects( $model, [
					'transportation' => $request->post( 'transportation' ),
					'terminals' 	 => empty( $model->terminals )
						? (array)json_decode( $model->terminals )
						: $model->terminals
				] );

				//current terminal due to terminal id
				$terminal = Terminals::findOne(
					$request->post( 'terminal_id' )
				);

				//taxonomy details
				$taxonomies = Taxonomies::find()
					-> select( [
						'id', 'name'
					] )
					-> where( [
						'type' 	    => 'containers',
						'isDeleted' => false
					] )
					-> indexBy( 'id' )
					-> asArray()
					-> all();

				//saving current order if vgm option is checked
				if ( ! empty( $model->terminals ) ) {

					$model->terminals = is_array( $model->terminals ) || is_object( $model->terminals )
						? $model->terminals
						: (array)json_decode( $model->terminals );

					$model->terminals = array_replace( $model->terminals, [
						'status'         => $request->post( 'status' ),
						'free_detention' => $request->post( 'free_detention' ),
						'vgm_document'   => $is_vgm != "false"
							? 'on'
							: ''
					] );

					$model->save( false );

				}

				foreach ( $rentals as $key => $value ) {

					//generate pdf from html
					$pdf = new Pdf([
						//set to use core fonts only
						'mode' => Pdf::MODE_UTF8,
						//A4 paper format
						'format' => Pdf::FORMAT_A4,
						//portrait orientation
						'orientation' => Pdf::ORIENT_PORTRAIT ,
						//stream to browser inline
						'destination' => Pdf::DEST_BROWSER,
						//html content input
						'content' => $this->renderPartial( '_pdf', [
							'model' => $model,
							'dates'	=> [
								'from' 		    => str_replace( [ '.', '/' ], '-', $value[ 'from' ] ),
								'to'   		    => str_replace( [ '.', '/' ], '-', $value[ 'to' ] ),
								'container_ids' => explode( ',', $value[ 'container_ids' ] )
							],
							'terminal'   => $terminal,
							'is_vgm'	 => $is_vgm,
							'status'	 => $request->post( 'status' ),
							'containers' => [
								'details' => Containers::find()
									-> where( [
										'id' 	    => $container_ids,
										'isDeleted' => false
									] )
									-> asArray()
									-> all(),
								'types' => array_reduce( TaxonomyRelation::find()
									-> where( [
										'foreign_id' => $container_ids,
										'type'		 => 'containers'
									] )
									-> asArray()
									-> all(), function( $containers_type, $relation ) use ( $taxonomies ) {

									if ( isset( $taxonomies[ $relation[ 'tax_id' ] ] ) )
										$containers_type[ $relation[ 'foreign_id' ] ] = $taxonomies[ $relation[ 'tax_id' ] ];

									return $containers_type;

								}, [] )
							]
						] )
					] );

					//upload pdf file
					if ( FileHelper::createDirectory( $upload_path . $model->number . '/', 0775, true ) ) {

						$file_name = $is_vgm != "false"
							? __( 'VGM-' ) . $terminal->name . '.pdf'
							: $terminal->name . '.pdf';

						file_put_contents(
							$upload_path . $model->number . '/' . $file_name,
							$pdf->render()
						);

						$pdf_files[] = Yii::getAlias( '@web' ) . '/reports/terminals/'. $model->number . '/' . $file_name;

					}

				}

			}

			$response = empty( $rentals )
				? [
					'code' 	  => 400,
					'status'  => __( 'ERROR' ),
					'message' => __( 'Fill in rental dates' )
				]
				: [
					'code'   => 200,
					'status' => 'success',
					'result' => $pdf_files
				];

		}

		//change response type to json for ajax callback
		Yii::$app->response->format = Response::FORMAT_JSON;

		return $response;

	}

	/**
	 * Allow download report in xlsx file due to requested type
	 * Filtering data due to request params in url
	 * @param string $type
	 * @return mixed
	**/
    public function actionExports( $type )
	{

		//params initializations
		$request      = Yii::$app->getRequest();
		$searchModel  = new OrdersSearch();
		$status       = Yii::$app->request->get( 'status', null );
		$trains		  = [];
		$dataProvider = $searchModel->search( ArrayHelper::merge( Yii::$app->request->queryParams, [
			$searchModel->formName() => [
					'status' => $status == null
						? 'prequote'
						: $status
				]
			]
		) );

		//disable pagination
		$dataProvider->pagination = false;

		//invoice details
		$invoice_query = in_array( $type, [
			'bt_transportations',
			'profit_lost',
			'balance_report',
			'customers_receivables'
		] ) && ! empty( $dataProvider->models )
			? Invoices::find()
				-> select( [
					'sum(amount) as total',
					'date',
					'customer_id',
					'foreign_id',
					'count(*) as quantity'
				] )
				-> where( [
					'foreign_id' => ArrayHelper::getColumn(
						$dataProvider->models, 'id'
					),
					'type' => 'Lotus'
				] )
			: [];

		//expenses details
		$expenses = in_array( $type, [
			'balance_report',
			'balance_detailed_report',
			'conter_agent_receivables',
		] ) && ! empty( $dataProvider->models )
			? Expenses::find()
				-> where( [
					'order_id' => ArrayHelper::getColumn(
						$dataProvider->models, 'id'
					)
				] )
				-> indexBy( 'id' )
				-> asArray()
				-> all()
			: [];

		//taxonomies details
		$taxonomies = in_array( $type, [
			'balance_detailed_report',
			'bt_transportations',
			'terminal_storage',
			'transportations'
		] ) ? Taxonomies::find()
			-> where( [
				'type'      => 'containers',
				'isDeleted' => false
			] )
			-> indexBy( 'id' )
			-> asArray()
			-> all()
			: [];

		//logistic details for specific reports
		if ( ! empty( $dataProvider->models ) && in_array( $type, [
			'balance_report',
			'balance_detailed_report',
			'bt_transportations',
			'customers_receivables',
			'profit_lost',
			'terminal_storage'
		] ) ) {

			//logistic ids due to requested foreign ids, i.e. in this case order ids
			$logistics_relations = LogisticsRelation::find()
				-> where( [
					'foreign_id' => ArrayHelper::getColumn(
						$dataProvider->models, 'id'
					),
					'type' => 'blockTrain'
				] )
				-> asArray()
				-> all();

			//the list of logistics according to bt ids
			$logistics = ! empty( $logistics_relations )
				? Logistics::find()
					-> where( [
						'id' => ArrayHelper::getColumn(
							$logistics_relations, 'logistic_id'
						),
						'type' => $type == 'bt_transportations'
							? 'blockTrain'
							: [
								'blockTrain',
								'train'
							]
					] )
					-> indexBy( 'id' )
					-> asArray()
					-> all()
				: [];

			//train and block number due to order id
			$trains = ! empty( $logistics_relations ) && isset( $logistics_relations ) && isset( $logistics )
				? array_reduce( $logistics_relations,
				function ( $result, $logistic ) use ( $logistics ) {

					if ( isset( $logistics[ $logistic[ 'logistic_id' ] ] ) )
						$result[ $logistic[ 'foreign_id' ] ] = $logistics[ $logistic[ 'logistic_id' ] ][ 'transport_no' ];

					return $result;

				}, [] )
				: [];

		}

		//additional condition to invoice details
		if ( $type == 'customers_receivables' ) {

			$invoice_query->andWhere( [
				'customer_id' => $searchModel->user_id,
			] );

		}

		//default parameters for export
		$params = [
			'models' => in_array( $type, [
				'terminal_storage'
			] )
				? []
				: $dataProvider->models,
			'customers' => in_array( $type, [
				'balance_report',
				'balance_detailed_report',
				'transportations',
				'profit_lost',
				'bt_transportations',
				'customers_receivables',
				'incomes' ]
			) ? User::find()
				-> where( [
					'id'		=> Yii::$app->authManager->getUserIdsByRole( 'client' ),
					'isDeleted' => false
				] )
				-> indexBy( 'id' )
				-> asArray()
				-> all()
				: [],
			'invoices' => in_array( $type, [
				'bt_transportations',
				'profit_lost',
				'balance_report',
				'customers_receivables'
			] ) && ! empty( $dataProvider->models )
				? $invoice_query
					-> indexBy( 'foreign_id' )
					-> groupBy( 'foreign_id' )
					-> asArray()
					-> all()
			 	: [],
			'logistics' => in_array( $type, [
				'balance_report',
				'balance_detailed_report',
				'bt_transportations',
				'customers_receivables',
				'profit_lost',
				'terminal_storage'
			] )
				? $trains
				: [],
		];

		if ( $type == 'balance_report'
			&& ! empty( $dataProvider->models )
		) {

			//the list of invoices due to expenses ids
			$invoices = array_reduce( Invoices::find()
				-> select( [
					'foreign_id',
					'sum(amount) as invoice_cost'
				] )
				-> where( [
					'foreign_id' => ArrayHelper::getColumn(
						$expenses, 'id'
					),
					'type' => 'expenses'
				] )
				-> indexBy( 'customer_id' )
				-> groupBy( 'customer_id' )
				-> asArray()
				-> all(), function( $results, $invoice ) {

				if ( ! empty( $invoice ) )
					$results[ $invoice[ 'foreign_id' ] ][ 'costs' ][] = $invoice[ 'invoice_cost' ];

				return $results;

			}, [] );

			//the list of expenses due to invoices
			$params = array_replace( $params, [
				'expenses' => array_reduce( $expenses, function( $results, $expense ) use ( $invoices ) {

					if ( isset( $results[ $expense[ 'order_id' ] ] ) ) {
						$results[ $expense[ 'order_id' ] ][ 'total_cost' ] = (float)$results[ $expense[ 'order_id' ] ][ 'total_cost' ] + (float)$expense[ 'total' ];
					} else {

						//railway details
						$railway = ! empty( $expense[ 'rf_details' ] )
							? json_decode( $expense[ 'rf_details' ] )
							: [];

						$results[ $expense[ 'order_id' ] ] = [
							'total_cost' 	 => $expense[ 'total' ],
							'costs'		 	 => isset( $invoices[ $expense[ $expense[ 'id' ] ] ] ),
							'container_cost' => array_sum( array_intersect_key( $expense, array_flip( [
								'container_cost'
							] ) ) ),
							'sea'  => array_sum( array_intersect_key( $expense, array_flip( [
								'dthc', 'othc', 'bl', 'sf_rate', 'sf_surcharge'
							] ) ) ),
							'auto' => array_sum( array_intersect_key( $expense, array_flip( [
								'auto_rate', 'auto_surcharge'
							] ) ) ),
							'terminals' => array_sum( array_intersect_key( $expense, array_flip( [
								'storage', 'lading', 'unloading', 'exp_price', 'imp_price', 'terminal_surcharge'
							] ) ) ),
							'railway' => array_reduce( ! empty( $railway )
								? array_keys( $railway->route )
								: [], function( $result, $key ) use ( $railway ) {

								if( ! empty( $railway ) ) {
									$result += (float)$railway->rate[ $key ] + (float)$railway->surcharge[ $key ];
								}

								return $result;

							}, 0 ),
							'surcharges' => array_sum( array_intersect_key( $expense, array_flip( [
								'pf_rate', 'pf_commission', 'pf_surcharge', 'container_surcharge'
							] ) ) )
						];

					}

					return $results;

				}, [] )
			] );

		} else if ( $type == 'balance_detailed_report' ) {

			//container relations due to expenses id
			$container_relations = ContainersRelation::find()
				-> where( [
					'foreign_id' => array_column( $expenses, 'id' ),
					'type' 	     => 'expenses'
				] )
				-> indexBy( 'container_id' )
				-> asArray()
				-> all();

			if ( ! empty( $container_relations ) ) {

				//container lists
				$containers = Containers::find()
					-> where( [
						'id' => array_column(
							$container_relations, 'container_id'
						)
					] )
					-> indexBy( 'id' )
					-> asArray()
					-> all();

				//wagons relations
				$wagons_relations = WagonsRelation::find()
					-> where( [
						'foreign_id' => array_column(
							$container_relations, 'container_id'
						)
					] )
					-> andFilterWhere( [
						'like', 'type', 'container'
					] )
					-> asArray()
					-> all();

				//the list of wagons due to id
				$wagons = ! empty( $wagons_relations )
					? Wagons::find()
						-> where( [
							'id' => array_column(
								$wagons_relations, 'wagon_id'
							)
						] )
						-> indexBy( 'id' )
						-> asArray()
						-> all()
					: [];

			}

			//the list of expenses
			$params = array_replace( $params, [
				'models' => array_reduce( $expenses, function( $result, $expenses ) use ( $dataProvider ) {

					//param initialization
					$orders = $dataProvider->models;

					if ( isset( $orders[ $expenses[ 'order_id' ] ] ) ) {

						$result[ $expenses[ 'id' ] ] = array_merge( $expenses, [
							'order' => $dataProvider->models[ $expenses[ 'order_id' ] ]
						] );

					}

					return $result;

				}, [] ),
				'container' => isset( $containers )
					? [
						'numbers' => array_reduce(
							$container_relations, function( $result, $container ) use ( $containers ) {

							if ( ! empty( $container )
								&& isset( $containers[ $container[ 'container_id' ] ] )
							) {
								$result[ $container[ 'foreign_id' ] ] = $containers[ $container[ 'container_id' ] ];
							}

							return $result;

						}, [] ),
						'wagons' => isset( $wagons_relations ) && isset( $wagons )
							? array_reduce( $wagons_relations, function( $result, $wagon ) use ( $wagons ) {

								if ( ! empty( $wagon )
									&& isset( $wagons[ $wagon[ 'wagon_id' ] ] )
								) {
									$result[ $wagon[ 'foreign_id' ] ] = $wagons[ $wagon[ 'wagon_id' ] ];
								}

								return $result;

							}, [] )
							: [],
						'types' => array_reduce( ! empty( $containers )
							? TaxonomyRelation::find()
								-> where( [
									'type' 		 => 'containers',
									'foreign_id' => array_column( $containers, 'id' )
								] )
								-> asArray()
								-> all()
							: [], function( $result, $tax ) use ( $taxonomies ) {

								if ( ! empty( $tax )
									&& isset( $taxonomies[ $tax[ 'tax_id' ] ] )
								) {
									$result[ $tax[ 'foreign_id' ] ] = $taxonomies[ $tax[ 'tax_id' ] ];
								}

								return $result;

						}, [] )
					]
					: []
			] );

		} else if ( $type == 'bt_transportations'
			&& ! empty( $dataProvider->models )
		) {

			//assign additional parameters to params parameter
			$params = array_replace( $params, [
				'container_quantity' => ContainersRelation::find()
					-> select( [
						'count(*) as quantity',
						'foreign_id'
					] )
					-> where( [
						'foreign_id' => ArrayHelper::getColumn(
							$dataProvider->models, 'id'
						),
						'type' => 'order'
					] )
					-> indexBy( 'foreign_id' )
					-> groupBy( 'foreign_id' )
					-> asArray()
					-> all(),
				'taxonomies' => [
					'list' 	    => $taxonomies,
					'relations' => TaxonomyRelation::find()
						-> where( [
							'foreign_id' => ArrayHelper::getColumn(
								$dataProvider->models, 'id'
							),
							'type' => 'containers_type'
						] )
						-> indexBy( 'foreign_id' )
						-> asArray()
						-> all()
				]
			] );

		} else if ( $type == 'codes'
			&& ! empty( $dataProvider->models )
		) {

			$params = array_replace( $params, [
				'codes' => RailwayCodes::find()
					-> where( [
						'order_id' => ArrayHelper::getColumn(
							$dataProvider->models, 'id'
						)
					] )
					-> andWhere( [
						'IS NOT', 'code', NULL
					] )
					-> all()
			] );

		} else if ( $type == 'conter_agent_receivables'
			&& ! empty( $dataProvider->models )
		) {

			if ( ! empty( $expenses ) ) {

				$params = array_replace( $params, [
					'models' => array_reduce( Invoices::find()
						-> where( [
							'customer_id' => $searchModel->contractors,
							'foreign_id'  => ArrayHelper::getColumn(
								$expenses, 'id'
							),
							'type' => 'expenses'
						] )
						-> asArray()
						-> all(), function( $result, $invoice ) use ( $dataProvider, $expenses ) {

						//current order id
						$order_id = isset( $expenses[ $invoice[ 'foreign_id' ] ][ 'order_id' ] )
							? $expenses[ $invoice[ 'foreign_id' ] ][ 'order_id' ]
							: 0;

						if ( isset( $dataProvider->models[ $order_id ] ) ) {

							//order package quantities
							$quantity = ! empty( $dataProvider->models[ $order_id ]->packages )
								? json_decode( $dataProvider->models[ $order_id ]->packages )
								: 0;

							//order weight dimensions
							$volume = ! empty( $dataProvider->models[ $order_id ]->weight_dimensions )
								? json_decode( $dataProvider->models[ $order_id ]->weight_dimensions )
								: 0;

							$result[ $order_id ] = isset( $result[ $order_id ] )
								? array_merge( $result[ $order_id ], [
									'total' => (float)$result[ $order_id ][ 'total' ] + (float)$invoice[ 'amount' ]
								] )
								: [
									'order_number' => $dataProvider->models[ $order_id ]->number,
									'quantity'	   => isset( $quantity->quantity )
										? $quantity->quantity
										: 0,
									'volume' => isset( $volume->gross )
										? $volume->gross
										: 0,
									'total' => (float)$invoice[ 'amount' ],
									'date'	=> ! empty( $invoice[ 'date' ] )
										? date( 'd.m.Y', strtotime( $invoice[ 'date' ] ) )
										: __( 'Invoice Date Not Available' )
								];

						}

						return $result;

					}, [] )
				] );

			} else {
				$params[ 'models' ] = [];
			}

		} else if ( $type == 'incomes'
			&& ! empty( $dataProvider->models )
		) {

			$params = array_replace( $params, [
				'models' => Invoices::findAll( [
					'foreign_id' => ArrayHelper::getColumn(
						$dataProvider->models, 'id'
					),
					'type' => 'Lotus'
				] )
			] );

		} else if ( $type == 'terminal_storage'
			&& ! empty( $searchModel->terminal_id )
		) {

			//terminal rentals due to requested terminal id
			$terminal_rentals = TerminalStorage::find()
				-> where( [
					'terminal_id' => $searchModel->terminal_id
				] )
				-> andWhere( [
					'>=', 'from', date( 'Y-m-d', strtotime( $request->getQueryParam('date_from' ) ) )
				] )
				-> andWhere( [
					'<=', 'to', date( 'Y-m-d', strtotime(  $request->getQueryParam( 'date_to' ) ) )
				] )
				-> indexBy( 'container_id' )
				-> asArray()
				-> all();

			if ( ! empty( $terminal_rentals ) ) {

				//the list of expenses ids due to it is relations with container ids
				$relations = ContainersRelation::find()
					-> where( [
						'container_id' => ArrayHelper::getColumn(
							$terminal_rentals, 'container_id'
						),
						'type' => 'expenses'
					] )
					-> asArray()
					-> all();

				//list of expenses
				$expenses = Expenses::find()
					-> where( [
						'id' => ArrayHelper::getColumn(
							$relations, 'foreign_id'
						)
					] )
					-> indexBy( 'id' )
					-> asArray()
					-> all();

				//list of containers
				$containers = Containers::find()
					-> where( [
						'id' => ArrayHelper::getColumn(
							$terminal_rentals, 'container_id'
						),
						'isDeleted' => false
					] )
					-> indexBy( 'id' )
					-> asArray()
					-> all();

				//re-assign params parameter
				$params = array_replace( $params, [
					'models' 	 => array_reduce( TaxonomyRelation::findAll( [
						'foreign_id' => ArrayHelper::getColumn( $containers, 'id' ),
						'type'		 => 'containers'
					] ), function ( $result, $type ) use ( $containers ) {

						if( isset( $containers[ $type->foreign_id ] ) ) {

							$result[] = array_replace( $containers[ $type->foreign_id ], [
								'container_type' => $type->tax_id
							] );

						}

						return $result;

					}, [] ),
					'rentals'	 => $terminal_rentals,
					'taxonomies' => $taxonomies,
					'expenses'   => array_reduce( $relations, function( $result, $expense ) use ( $expenses, $dataProvider ) {

						if ( isset( $expenses[ $expense[ 'foreign_id' ] ] ) ) {

							$result[ $expense[ 'container_id' ] ] = [
								'order' => [
									'id'     => $expense[ 'order_id' ],
									'number' => isset( $dataProvider->models[ $expense[ 'order_id' ] ] )
										? $dataProvider->models[ $expense[ 'order_id' ] ]
										: ''
								],
								'storage' => isset( $result[ $expense[ 'container_id' ] ] )
									? ( float ) $result[ $expense[ 'container_id' ] ][ 'storage' ] + (float)$expenses[ $expense[ 'foreign_id' ] ][ 'storage' ]
									: $expenses[ $expense[ 'foreign_id' ] ][ 'storage' ],
								'loading' => isset( $result[ $expense[ 'container_id' ] ] )
									? ( float ) $result[ $expense[ 'container_id' ] ][ 'lading' ] + (float)$expenses[ $expense[ 'foreign_id' ] ][ 'lading' ]
									: $expenses[ $expense[ 'foreign_id' ] ][ 'lading' ],
								'unloading'  => isset( $result[ $expense[ 'container_id' ] ] )
									? ( float ) $result[ $expense[ 'container_id' ] ][ 'unloading' ] + (float)$expenses[ $expense[ 'foreign_id' ] ][ 'unloading' ]
									: $expenses[ $expense[ 'foreign_id' ] ][ 'unloading' ],
								'surcharges' => isset( $result[ $expense[ 'container_id' ] ] )
									? ( float ) $result[ $expense[ 'container_id' ] ][ 'surcharges' ] + (float)$expenses[ $expense[ 'foreign_id' ] ][ 'terminal_surcharge' ]
									: $expenses[ $expense[ 'foreign_id' ] ][ 'terminal_surcharge' ]
							];

						}

						return $result;

					} , [] )
				] );

			}

		} else if ( $type == 'transportations'
			&& ! empty( $dataProvider->models )
		) {

			//assign the list of container types
			$params = array_replace( $params, [
				'container_types' => array_reduce( TaxonomyRelation::find()
					-> where( [
						'type' 		 => 'containers_type',
						'foreign_id' => array_column( $dataProvider->models, 'id' )
					] )
					-> indexBy( 'tax_id' )
					-> asArray()
					-> all(), function( $result, $tax ) use ( $taxonomies ) {

					if ( ! empty( $tax )
						&& isset( $taxonomies[ $tax[ 'tax_id' ] ] )
					) {
						$result[ $tax[ 'foreign_id' ] ] = $taxonomies[ $tax[ 'tax_id' ] ];
					}

					return $result;

				}, [] )
			] );

		}

		//export to xslx file method
		Yii::$app->excel_export->export( [
			'type'	=> $type,
			'model' => [
				'title' => [
					'from' 		 =>  $request->getQueryParam( 'date_from' ),
					'to'   	     =>  $request->getQueryParam( 'date_to' ),
					'type' 	     => $searchModel->transportation,
					'expeditor'  =>  $request->getQueryParam( 'forwarder' ),
					'agent' 	 => ! empty( $searchModel->contractors )
						? array_map( function( $user ) {
							return ! is_null( $user ) && isset( $user[ 'first_name' ] )
								? $user[ 'first_name' ] . ' ' . $user[ 'last_name' ]
								: __( 'Contractor not chosen' );
						}, User::find()
							-> where( [
								'id' 	    => $searchModel->contractors,
								'isDeleted' => false
							] )
							-> asArray()
							-> all()
						)[0]
						: __( 'Contractor not chosen' ),
					'customer' => ! empty( $searchModel->user_id )
						? array_map( function( $user ) {
							return ! is_null( $user ) && isset( $user[ 'first_name' ] )
								? $user[ 'first_name' ] . ' ' . $user[ 'last_name' ]
								: __( 'Customer not chosen' );
						}, User::find()
							-> where( [
								'id' 	    => $searchModel->user_id,
								'isDeleted' => false
							] )
							-> asArray()
							-> all()
						)[0]
						: __( 'Customer not chosen' ),
					'terminal' => ! empty( $searchModel->terminal_id )
						? array_map( function( $terminal ) {

							return ! is_null( $terminal ) && isset( $terminal[ 'name' ] )
								? $terminal[ 'name' ]
								: __( 'Terminal not chosen' );
						}, Terminals::find()
							-> where( [
								'id' => $searchModel->terminal_id
							] )
							-> asArray()
							-> all()
						)[0]
						: __( 'Terminal not chosen' )
				]
			],
			'details' 	=> $params,
			'report'    => strtoupper( str_replace( '_', ' ',  $type ) ),
			'file_name' => __( ucfirst( str_replace( '_', ' ',  $type ) ) )
		] );

		return $this->redirect( Url::to( [
			'/orders'
		] ) );

	}

	/**
	 * Uploading new file
	 * @param mixed $request
	 * @param object $model
	 * @return integer
	**/
	public function uploads( $request, $model )
	{

		//params initializations
		$response	 = 0;
		$upload_form = new UploadForm( [
			'path' => '@frontend/web/upload/orders',
			'url'  => '@frontend_web/upload/orders'
		] );

		//loading requested file details
		$upload_form->load(
			$request->post()
		);

		//uploading requested file
		if ( $upload_form->upload(
			'file'
		) ) {

			//file details due to requested file id
			$file = Files::findOne(
				$model->gu_file_id
			);

			//is requested file exists
			$file = is_null( $file )
				? new Files()
				: $file;

			//updating file details
			$file = merge_objects( $file, [
				'name'  => $upload_form->file->name,
				'guide' => $upload_form->getUrl(),
				'size'	=> (string)$upload_form->image->size,
				'type'	=> 'orders'
			] );

			//saving file
			if ( $file->save() ) {
				$response = $file->id;
			}

		}

		return $response;

	}

    /**
     * Deletes an existing Orders model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id, string $status
     * @return mixed
    **/
    public function actionDelete( $id, $status = 'pending' )
    {

		//set success response message
        if ( $this->findModel( $id )->delete() ) {

        	//delete all expenses
			Expenses::deleteAll( [
				'order_id' => $id
			] );

			//delete all invoices
			Invoices::deleteAll( [
				'foreign_id' => $id
			] );

			Yii::$app->session->setFlash( 'success',
				__( 'Order was deleted successfully' )
			);

        }

        return $this->redirect( [
            'index',
			'status' => $status
        ] );

    }

    /**
     * Finds the Orders model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Orders the loaded model
     * @throws NotFoundHttpException if the model cannot be found
    **/
    protected function findModel( $id )
    {

        if ( ( $model = Orders::findOne( $id ) ) !== null ) {
            return $model;
        } else {

            throw new NotFoundHttpException(
            	__( 'The requested page does not exist' )
			);

        }

    }

}