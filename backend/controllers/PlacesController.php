<?php namespace backend\controllers;

/**************************************/
/*                                    */
/*          PLACES CONTROLLER         */
/*                                    */
/**************************************/

use Yii;

use yii\filters\AccessControl;
use yii\filters\VerbFilter;

use yii\helpers\ArrayHelper;

use yii\web\Controller;
use yii\web\NotFoundHttpException;

use common\models\Places;
use common\models\PlacesSearch;

/**
 * Places Controller is the controller behind the Places model.
**/
class PlacesController extends Controller
{

	/**
	 * @inheritdoc
	**/
	public function behaviors()
	{

		return [

			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'actions' => [
							'create',
							'update',
							'index',
							'delete'
						],
						'allow' => true,
						'roles' => [
							'routes'
						]
					]
				]
			],

			'verbs'  => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => [
						'POST'
					]
				]
			]

		];

	}

	/**
	 * Lists all Places models.
	 * @param string $logistic_type
	 * @return mixed
	**/
	public function actionIndex( $logistic_type = 'sea' )
	{

		//params initializations
		$searchModel  = new PlacesSearch();
		$dataProvider = $searchModel->search( ArrayHelper::merge(
			Yii::$app->request->queryParams, [
				$searchModel->formName() => [
					'type' => $logistic_type
				] ]
		) );

		return $this->render( 'index', [
			'searchModel'  => $searchModel,
			'dataProvider' => $dataProvider
		] );

	}

	/**
	 * Creates a new Places model.
	 * If creation is successful, the browser will be redirected to the 'index' page.
	 * @return mixed
	**/
	public function actionCreate()
	{

		//params initializations
		$model = new Places();

		//add new place
		if ( $model->load( Yii::$app->request->post() )
			&& $model->save()
		) {

			//set success response message
			Yii::$app->session->setFlash( 'success',
				__( 'Place was created successfully' )
			);

			return $this->redirect( [
				'index'
			] );

		}

		return $this->render( 'create', [
			'model' => $model
		] );

	}

	/**
	 * Updates an existing Places model.
	 * If update is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	**/
	public function actionUpdate( $id )
	{

		//params initializations
		$model = $this->findModel( $id );

		//updating current model with requested details
		if ( $model->load( Yii::$app->request->post() )
			&& $model->save()
		) {

			//set success response message
			Yii::$app->session->setFlash( 'success',
				__( 'Place was updated successfully' )
			);

			return $this->redirect( [
				'index'
			] );

		}

		return $this->render( 'update', [
			'model' => $model
		] );

	}

	/**
	 * Deletes an existing Places model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	**/
	public function actionDelete( $id )
	{

		//set success response message
		if ( $this->findModel( $id )->delete() ) {

			Yii::$app->session->setFlash( 'success',
				__( 'Place was deleted successfully' )
			);

		}

		return $this->redirect( [
			'index'
		] );

	}

	/**
	 * Finds the Places model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return Places the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	**/
	protected function findModel( $id )
	{

		if ( ( $model = Places::findOne( $id ) ) !== null ) {
			return $model;
		} else {

			throw new NotFoundHttpException(
				__( 'The requested page does not exist' )
			);

		}

	}

}