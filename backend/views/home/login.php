<?php /**********************************************************
 *                          LOGIN PAGE                          *
 ****************************************************************/

/* @var $this yii\web\View              */
/* @var $form yii\bootstrap\ActiveForm  */
/* @var $model \common\models\LoginForm */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

//current page title, that will displayed in head tags
$this->title = __(  'Login' );

// registering all styles and scripts for backend section
AppAsset::register( $this );

$this->beginPage() ?>

    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">

        <head>

            <meta charset="<?= Yii::$app->charset ?>">
            <meta name="viewport" content="width=device-width, initial-scale=1">

            <?= Html::csrfMetaTags() ?>

            <title>
                <?= Html::encode( $this->title ) ?>
            </title>

            <?php $this->head() ?>

        </head>

        <body id="login-page">

            <?php $this->beginBody() ?>

                <div class="">

                    <div class="logo-section">

                        <div class="container">

                            <a href="/" class="">
                                <img src="<?= Yii::getAlias( '@web' ) ?>/images/logo.png">
                            </a>

                        </div>

                    </div>

                    <div id="wrapper">

                        <div id="login_form">

                            <?php $form = ActiveForm::begin( [
                                'id' => 'login-form'
                            ] ); ?>

                                <h1>
                                    <?= __(  'Login Form' ) ?>
                                </h1>

                                <div class="group">

                                    <?= $form->field( $model, 'email' )
                                        -> textInput( [
                                            'class'       => 'login-fields',
                                            'placeholder' => __(  'Email' )
                                        ] )
                                        -> label( false );
                                    ?>

                                </div>

                                <div class="group">

                                    <?= $form->field( $model, 'password' )
                                        -> passwordInput( [
                                            'class'       => 'login-fields',
                                            'placeholder' => __(  'Password' )
                                        ] )
                                        -> label( false );
                                    ?>

                                </div>

                                <button class="button buttonBlue">
                                    <?= __(  'Log In' ) ?>
                                </button>

                            <?php ActiveForm::end() ?>

                        </div>

                    </div>

                </div>

            <?php $this->endBody() ?>

        </body>

    </html>

<?php $this->endPage() ?>