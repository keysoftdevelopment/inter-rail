<?php /***************************************************************
 *                  DASHBOARD STATISTICS SECTION                     *
 *    I.E CURRENT SECTION ADDING SCRIPTS FOR STATS TO CURRENT PAGE   *
 *********************************************************************/

/* @var $this yii\web\View */

use backend\assets\AppAsset;

$jsFiles = [
	'@web/js/flot/jquery.flot.js',
	'@web/js/flot/jquery.flot.pie.js',
	'@web/js/flot/jquery.flot.orderBars.js',
	'@web/js/flot/jquery.flot.time.min.js',
	'@web/js/flot/date.js',
	'@web/js/flot/jquery.flot.spline.js',
	'@web/js/flot/jquery.flot.stack.js',
	'@web/js/flot/curvedLines.js',
	'@web/js/flot/jquery.flot.resize.js'
];

foreach ( $jsFiles as $file ) {

	$this->registerJsFile(
		$file,
		[
			'depends' => [
				AppAsset::className()
			]
		]
	);

}

$this->registerJs( '$( function() {
    
      var user_stat  = $( "#user-statistic" ),
          order_stat = $( "#order-statistics" ),
          users      = user_stat.data( "details" ),
          orders     = order_stat.data( "details" ),
	 	  day        = [];
      
      user_stat.length && $.plot(
      
            user_stat, [
                Object.entries( users ).map( 
                	( [ date, count ] ) => [
                 		+( date + "000" ), 
                 		count 
                	] 
                ) 
            ], {
            
                series: {
                
                    lines: {
                        show : false,
                        fill : true
                    },
                    
                    splines: {
                        show      : true,
                        tension   : 0.4,
                        lineWidth : 1,
                        fill      : 0.4
                    },
                    
                    points: {
                        radius : 0,
                        show   : true
                    },
                    
                    shadowSize : 2
                },
                
                grid: {
                    verticalLines : true,
                    hoverable     : true,
                    clickable     : true,
                    tickColor     : "#d5d5d5",
                    borderWidth   : 1,
                    color         : \'#fff\'
                },
                
                colors: [ 
                	"#74c2cab8", 
                	"#3db7c4a8"
                ],
                
                xaxis: {
                    tickColor               : "rgba(51, 51, 51, 0.06)",
                    mode                    : "time",
                    tickSize                : [1, "day"],                    
                    axisLabel               : "Date",
                    axisLabelUseCanvas      : true,
                    axisLabelFontSizePixels : 12,                    
                    axisLabelPadding        : 10,
                    timeformat			    : "%d.%m"                    
                },
                
                yaxis: {
                    ticks     : 8,
                    tickColor : "rgba(51, 51, 51, 0.06)",
                },
                
                tooltip : false
                
            }
            
      );
      
      for ( var i = 30; i > 0; i-- ) {
	
            var count = 0,
        	    date  = new Date( Date.today().add( -i ).days() );
        	
        	for( var dt in orders ) 
            	if( moment( date ).isSame( dt,"day" ) )
                	count = orders[dt];
            	
        	day.push( [ 
        		date.getTime(), 
        		count 
        	] );
        	        		
      }

      var chartMinDate = day[ 0 ][ 0 ],
    	  chartMaxDate = day[ 29 ][ 0 ],
          tickSize 	   = [ 1, "day" ];
      	  tformat 	   = "%d.%m",
    	  options      = {
    	    	
                grid: {
                    show			 : true,
                    aboveData		 : true,
                    color			 : "#3f3f3f",
                    labelMargin		 : 10,
                    axisMargin		 : 0,
                    borderWidth		 : 0,
                    borderColor	 	 : null,
                    minBorderMargin  : 5,
                    clickable		 : true,
                    hoverable	     : true,
                    autoHighlight	 : true,
                    mouseActiveRadius: 100
                }, 
        	       	
                series: {
                    lines: {
                        show	 : true,
                        fill	 : true,
                        lineWidth: 2,
                        steps	 : false
                    },
                    points: {
                        show     : true,
                        radius	 : 4.5,
                        symbol	 : "circle",
                        lineWidth: 3.0
                    }
                },
        	        	
                legend: {
                    position  		   : "ne",
                    margin	  		   : [ 0, -25 ],
                    noColumns 		   : 0,
                    labelBoxBorderColor: null,
                    labelFormatter	   : function( label, series ) {          		            
                        return label + \'&nbsp;&nbsp;\';
                    },
                    width : 40,
                    height: 1
                },
        	
                shadowSize : 0,
                tooltip	   : true, //activate tooltip
                tooltipOpts: {
                    content	   : "%s: %y.0",
                    xDateFormat: "%d/%m",
                    shifts	   : {
                        x: -30,
                        y: -50
                    },
                    defaultTheme: false
                },
                
                yaxis: {
                    min: 0
                },
                
                xaxis: {
                    mode	   : "time",
                    minTickSize: tickSize,
                    timeformat : tformat,
                    min	       : chartMinDate,
                    max		   : chartMaxDate
                },
                
                colors: [ "#00a2b3" ]
      };      
      	
      order_stat.length && $.plot( order_stat, [ {
      		label: "'. __(  'Orders' ) .'",
            data : day,
            lines: {
          	    fillColor: "rgba(150, 202, 89, 0.12)"
            }, 
            points: {
          	    fillColor: "#fff"
            }
            
      } ], options );
        
} );',
$this::POS_END ); ?>