<?php /***************************************************************
 *                        DASHBOARD STATISTICS PAGE                  *
 *********************************************************************/
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */

// current page title, that will displayed in head tags
$this->title = __(  'Dashboard' );

// order statistics data
$stats = Yii::$app->home->getStatsByOrders();

echo $this->render( 'statistics/register-scripts' ); ?>

    <div class="page-title">

        <div class="title_left">
            <h3>
                <?= __(  'Dashboard' ) ?>
            </h3>
        </div>

    </div>

    <div class="col-sm-12 col-md-12 col-xs-12 no-top-border dashboard-user-stat">

        <div class="x_panel">

            <div class="x_title">

                <h2>
                    <?= __(  'User Statistics for current month' ) ?>
                </h2>

                <div class="clearfix"></div>

            </div>

            <div class="x_content">

                <div class="col-md-12 col-sm-12 col-xs-12">

                    <div id="user-statistic" class="statistics-container" data-details="<?= htmlentities( json_encode(
                        array_map( 'intval', ArrayHelper::map(
                            Yii::$app->home->users_stat(),
                            'created_at',
                            'count'
                        ) )
                    ) ) ?>"></div>

                </div>

            </div>

        </div>

    </div>

    <div class="col-sm-12 col-md-12 col-xs-12 dashboard-order-stat">

        <div class="col-md-4 col-sm-4 col-xs-12">

            <ul>

                <li>

                    <div class="col-sm-6 col-md-6 col-xs-6">

                        <h4>
                            <?= $stats[ 'today_orders' ] ?>
                        </h4>

                        <span>
                            <?= __( 'Order per today' ) ?>
                        </span>

                    </div>

                    <div class="col-sm-6 col-md-6 col-xs-6">
                        <img src="<?= Yii::getAlias( '@web' ) ?>/images/order-quantity-graph.png">
                    </div>

                </li>

                <li>

                    <div class="col-sm-6 col-md-6 col-xs-6">

                        <img src="<?= Yii::getAlias( '@web' ) ?>/images/truck-icon.png">

                        <h4>
							<?= $stats[ 'containers_on_way' ] ?>
                        </h4>

                        <span>
                            <?= __( 'Containers on the way' ) ?>
                        </span>

                    </div>

                    <div class="col-sm-6 col-md-6 col-xs-6">

                        <img src="<?= Yii::getAlias( '@web' ) ?>/images/containers-terminal-icon.png">

                        <h4>
							<?= $stats[ 'terminal_containers' ] ?>
                        </h4>

                        <span>
                            <?= __( 'Containers in terminal' ) ?>
                        </span>

                    </div>

                </li>

                <li>

                    <div class="col-sm-6 col-md-6 col-xs-6">

                        <h4>
							<?= $stats[ 'register_count' ] ?>
                        </h4>

                        <span>
                            <?= __( 'Registered clients' ) ?>
                        </span>

                    </div>

                    <div class="col-sm-6 col-md-6 col-xs-6">
                        <img src="<?= Yii::getAlias( '@web' ) ?>/images/user-avatar-blue.png">
                    </div>

                </li>

            </ul>

        </div>

        <div class="col-md-8 col-sm-8 col-xs-12 no-top-border">

            <div class="x_panel">

                <div class="x_title">

                    <h2>
						<?= __(  'Order Statistics for current month' ) ?>
                    </h2>

                    <div class="clearfix"></div>

                </div>

                <div class="x_content">

                    <div class="col-md-12 col-sm-12 col-xs-12">

                        <div id="order-statistics" class="statistics-container" data-details="<?= htmlentities( json_encode(
                            ArrayHelper::map(
                                Yii::$app->home->orders_stat(),
                                'created_at',
                                'count'
                            )
                        ) ) ?>"></div>

                    </div>

                </div>

            </div>

        </div>

    </div>
