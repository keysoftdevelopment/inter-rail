<?php /***************************************************************
 *         		 	   	  LAYOUT FOR ERROR PAGE                      *
 *********************************************************************/

/* @var $this \yii\web\View */
/* @var $content string     */

use yii\helpers\Html;

use backend\assets\AppAsset;

// registering all styles and scripts for backend section
AppAsset::register( $this );

// starting html rendering
$this->beginPage() ?>

    <!DOCTYPE html>

    <html lang="<?= Yii::$app->language ?>">

        <head>

            <meta charset="<?= Yii::$app->charset ?>">
            <meta name="viewport" content="width=device-width, initial-scale=1">

            <?= Html::csrfMetaTags() ?>

            <title>
                <?= Html::encode( $this->title ) ?>
            </title>

            <?php $this->head() ?>

        </head>

        <body class="nav-md  pace-done" style="background-color: #bdbdbd;">

            <?php $this->beginBody(); ?>

                <!-- page content -->
                <div class="container body" style="margin-top: 10%">
                    <?= $content ?>
                </div>
                <!-- page content -->

            <?php $this->endBody() ?>

        </body>

    </html>

<?php $this->endPage() ?>