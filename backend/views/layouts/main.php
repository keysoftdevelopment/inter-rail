<?php /***************************************************************
 *         		 	   	   MAIN LAYOUT SECTION                       *
 *********************************************************************/

/* @var $this \yii\web\View */
/* @var $content string     */

use yii\helpers\Html;

use backend\assets\AppAsset;

// registering all styles and scripts for backend section
AppAsset::register( $this );

// starting html rendering
$this->beginPage() ?>

    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
        <head>

            <meta charset="<?= Yii::$app->charset ?>">
            <meta name="viewport" content="width=device-width, initial-scale=1">

            <?= Html::csrfMetaTags() ?>

            <title>
                <?= Html::encode( $this->title ) ?>
            </title>

            <?php $this->head() ?>

        </head>

        <body class="nav-md" data-url="<?= \Yii::getAlias( '@web' ) ?>">

            <?php $this->beginBody() ?>

                <div class="container body">

                    <div class="main_container">

                        <!-- header -->
                        <?= $this->render( '//common/header' ); ?>
                        <!-- header -->

                        <!-- page content -->
                        <div class="right_col" role="main">
                            <?= $content ?>
                        </div>
                        <!-- page content -->

                    </div>

                </div>

            <?php $this->endBody() ?>

            <?= $this->render( '//common/footer' ); ?>

        </body>

    </html>

<?php $this->endPage() ?>