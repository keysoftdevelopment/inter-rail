<?php /***************************************************************
 *																	 *
 *	 	       	   		SINGLE CUSTOM RATE SECTION 				     *
 *		          I.E. ALLOWS ADDING CUSTOM RATE DETAILS 			 *
 * 																     *
 *********************************************************************/

use yii\helpers\Html;

/* @var $model array  */
/* @var $places mixed */ ?>

<div class="option-section">

	<label for="place" class="col-md-1 col-sm-1 col-xs-12">
		<?= __( 'Tariffication' ) ?>
	</label>

	<div class="col-md-11 col-sm-11 col-xs-12">

		<div class="col-md-6 col-sm-6 col-xs-12">

			<?= Html::textInput(
				'customs[territory][]',
				isset( $model[ 'territory' ] )
					? $model[ 'territory' ]
					: '', [
					'class' => 'form-control col-md-12 col-sm-12 col-xs-12 custom-territory-field',
					__( 'Territory' )
				]
			) ?>

		</div>

		<div class="col-md-5 col-sm-5 col-xs-12">

			<?= Html::textInput(
				'customs[rate][]',
				isset( $model[ 'rate' ] )
					? $model[ 'rate' ]
					: '', [
					'class' 	  => 'form-control col-md-12 col-sm-12 col-xs-12 custom-rate-field',
					'placeholder' => __( 'Rate' )
				]
			) ?>

		</div>

	</div>

	<a href="javascript:void(0)" class="remove-custom-rate remove-current-option" data-parent="option-section" data-translations="<?= htmlentities( json_encode( [
		'title'   => __( 'CONFIRMATION MODULE' ),
		'message' => __( 'Do you really want to delete this option?' )
	] ) ) ?>">
		x
	</a>

</div>