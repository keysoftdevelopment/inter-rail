<?php /***************************************************************
 *	  	   	     THE LIST OF ALL AVAILABLE RATES PAGE                *
 *********************************************************************/

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;

/* @var $this yii\web\View                        */
/* @var $searchModel common\models\RatesSearch    */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $types common\models\Taxonomies[]         */
/* @var $rates array                              */

// current page title
$this->title = __(  'Rates' );

//the list of available terms
$terms = Yii::$app->common->transportationDetails( 'terms' ); ?>

    <div class="page-title">

        <!-- title section -->
        <div class="title_left">

            <h3>
                <?= $this->title ?>
            </h3>

            <a href="<?= Url::to( [
				'rates/create'
			] ) ?>">
                <i class="fa fa-plus-circle"></i>
				<?= __( 'Add New' ) ?>

            </a>

        </div>
        <!-- title section -->

    </div>

    <div class="clearfix"></div>

    <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12">

            <!-- filters section -->
            <div class="main-filters-container rates-calculation-form">

				<?= $this->render( 'filters/calculations', [
					'searchModel' => $searchModel
				] ) ?>

            </div>
            <!-- filters section -->

            <div class="x_panel">

                <div class="x_content">

                    <!-- rate lists -->
                    <table class="table table-striped projects">

                        <thead>

                            <tr>

                                <th style="width: 1%">
                                    #
                                </th>

                                <th>
									<?= __(  'Route' ) ?>
                                </th>

                                <th>
									<?= __(  'Container Type' ) ?>
                                </th>

                                <th>
									<?= __(  'Section' ) ?>
                                </th>

                                <th>
									<?= __(  'THC' ) ?>
                                </th>

                                <th>
									<?= __(  'Wagon Detention' ) ?>
                                </th>

                                <th>
									<?= __(  'Rental' ) ?>
                                </th>

                                <th>
                                    <?= __( 'Customs' ) ?>
                                </th>

                                <th style="width: 1%"></th>

                            </tr>

                        </thead>

                        <tbody>

                            <?php if ( !empty( $dataProvider->models ) ) {

                                foreach ( $dataProvider->models as $model ) { ?>

                                    <tr>

                                        <td>
                                            #
                                        </td>

                                        <td class="route">

                                            <div class="route-polyline">

                                                <div class="route-from">

                                                    <div class="point-a"></div>

                                                    <label>

														<?= ! empty( $model->from )
															? $model->from . ( isset( $terms[ $model->term_from ] )
                                                                ? ' (' . $terms[ $model->term_from ] . ')'
                                                                : ''
                                                            )
															: __( 'Not chosen' )
														?>

                                                    </label>

                                                    <div class="point-b"></div>

                                                </div>

                                                <hr>

                                                <div class="route-to">

                                                    <div class="point-a"></div>

                                                    <label>
														<?= ! empty( $model->to )
															? $model->to . ( isset( $terms[ $model->term_to ] )
																? ' (' . $terms[ $model->term_to ] . ')'
																: ''
															)
															: __( 'Not chosen' )
														?>
                                                    </label>

                                                    <div class="point-b"></div>

                                                </div>

                                            </div>

                                        </td>

                                        <td>

											<?= isset( $types[ $model->id ] )
												? $types[ $model->id ][ 'name' ]
												: __( 'Container Type Not Chosen' )
											?>

                                        </td>

                                        <td>
                                            <?= __( $model->transportation ) ?>
                                        </td>

                                        <td>
											<?= $model->thc ?>
                                        </td>

                                        <td>
											<?= $model->wd ?>
                                        </td>

                                        <td>
											<?= $model->rental ?>
                                        </td>

                                        <td>

											<?= array_reduce( !empty( $model->customs )
                                                ? (array)json_decode( $model->customs )->rate
                                                : [], function( $result, $custom ) {

											    if( ! empty( $custom ) ) {
											        $result += (float)$custom;
                                                }

											    return $result;

                                            }, 0 ); ?>

                                        </td>

                                        <td class="text-center model-actions">

                                            <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                <i class="fa fa-ellipsis-v"></i>
                                            </a>

                                            <ul class="dropdown-menu pull-right">

                                                <li>

													<?= Html::a( '<i class="fa fa-pencil"></i> ' .  __(  'Edit' ), [
														'update',
														'id' => $model->id
													] ); ?>

                                                </li>

                                                <li>

													<?= Html::a( '<i class="fa fa-trash"> </i> '. __(  'Delete' ), [
														'delete',
														'id' => $model->id
													], [
														'data'  => [
															'confirm' => __(  'Do you really want to delete this page?' ),
															'method'  => 'post',
														]
													] ); ?>

                                                </li>

                                            </ul>

                                        </td>

                                    </tr>

                                <?php }

                            } else {

                                echo '<tr>';

                                    echo '<td colspan="8" class="text-center">
                                        ' . __(  'There is no rates yet' ) . '
                                    </td>';

                                echo '</tr>';

                            } ?>

                        </tbody>

                        <?php if( ! empty( $rates ) ) { ?>

                            <tfoot>

                                <tr>

                                    <th colspan="2"></th>

                                    <th>
                                        <?= __( 'Quantity' ) ?>
                                    </th>

                                    <th>
										<?= __( 'Prime Cost' ) ?>
                                    </th>

                                    <th>
										<?= __( 'Expenses' ) ?>
                                    </th>

                                    <th colspan="2">
										<?= __( 'Total' ) ?>
                                    </th>

                                    <th colspan="2">
										<?= __( 'Income' ) ?>
                                    </th>

                                </tr>

                                <?php foreach ( $rates as $rate ) { ?>

                                    <tr>

                                        <td colspan="2">

											<?= __( ucfirst( $rate[ 'transportation' ] ) ); ?>

                                        </td>

                                        <td>
											<?= $rate[ 'qty' ] ?>
                                        </td>

                                        <td>

											<?= number_format( (float)$rate[ 'prime' ],
												2,
												'.',
												''
											); ?>

                                        </td>

                                        <td>

											<?= number_format( (float)$rate[ 'prime' ] * $rate[ 'qty' ],
                                                2,
                                                '.',
                                                ''
                                            ); ?>

                                        </td>

                                        <td colspan="2">

											<?= number_format( (float)$rate[ 'rate' ] * $rate[ 'qty' ],
												2,
												'.',
												''
											); ?>

                                        </td>

                                        <td colspan="2">

											<?= number_format( ( (float)$rate[ 'rate' ] * $rate[ 'qty' ] ) - ( (float)$rate[ 'prime' ] * $rate[ 'qty' ] ),
												2,
												'.',
												''
											); ?>

                                        </td>

                                    </tr>

                                <?php } ?>

                                <tr>

                                    <td colspan="2">
                                        <?= __( 'TOTALS' ) ?>
                                    </td>

                                    <td>

                                        <?= array_reduce( ! empty( Yii::$app->getRequest()->getQueryParam( 'filters' ) )
											? Yii::$app->getRequest()->getQueryParam( 'filters' )[ 'quantity' ]
											: [], function( $result, $qty ) {

											if( (int)$qty > 0 ) {
												$result += (int)$qty;
                                            }

											return $result;

										} ); ?>

                                    </td>

                                    <td>

										<?= number_format( array_reduce( ! empty( $rates )
											? $rates
											: [], function( $result, $rate ) {

											if( ! empty( $rate ) ) {
												$result += (float)$rate[ 'prime' ];
                                            }

											return $result;

										}, 0 ),
											2,
											'.',
											''
										); ?>

                                    </td>

                                    <td>

										<?= number_format( array_reduce( ! empty( $rates )
											? $rates
											: [], function( $result, $rate ) {

											if( ! empty( $rate ) ) {
												$result += (float)$rate[ 'prime' ] * (float)$rate[ 'qty' ];
                                            }

											return $result;

										}, 0 ),
											2,
											'.',
											''
										); ?>

                                    </td>

                                    <td colspan="2">

										<?= number_format( array_reduce( ! empty( $rates )
											? $rates
											: [], function( $result, $rate ) {

											if( ! empty( $rate ) ) {
												$result += (float)$rate[ 'rate' ] * (float)$rate[ 'qty' ];
											}

											return $result;

										}, 0 ),
											2,
											'.',
											''
										); ?>

                                    </td>

                                    <td colspan="2">

										<?= number_format( array_reduce( ! empty( $rates )
											? $rates
											: [], function( $result, $rate ) {

											if( ! empty( $rate ) ) {
												$result += ( (float)$rate[ 'rate' ] * $rate[ 'qty' ] ) - ( (float)$rate[ 'prime' ] * $rate[ 'qty' ] );
											}

											return $result;

										}, 0 ),
											2,
											'.',
											''
										); ?>

                                    </td>

                                </tr>

                            </tfoot>

                        <?php } ?>

                    </table>
                    <!-- rate lists -->

                </div>

                <!-- pagination section -->
                <div class="pagination-container">

                    <?= LinkPager::widget( [
                        'pagination' => $dataProvider->pagination,
                    ] ); ?>

                </div>
                <!-- pagination section -->

            </div>

        </div>

    </div>