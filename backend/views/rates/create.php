<?php /***************************************************************
 *	     		 	   	  CREATE RATE PART                           *
 *********************************************************************/

/* @var $this yii\web\View                 */
/* @var $model \common\models\Rates        */
/* @var $types \common\models\Taxonomies[] */

// current page title, that will displayed in head tags
$this->title = __(  'Create Rate' ); ?>

    <div class="col-md-12 col-sm-12 col-xs-12 edit-container-block">

        <!-- title section -->
        <div class="page-title">

            <div class="title_left">

                <h3>
                    <?= $this->title ?>
                </h3>

            </div>

        </div>
        <!-- title section -->

        <!-- rates details section -->
        <div class="col-md-12 col-sm-12 col-xs-12 rates-form" style="padding: 0px">

            <?= $this->render( '_form', [
                'model' => $model,
                'types' => $types,
            ] ); ?>

        </div>
        <!-- rates details section -->

    </div>