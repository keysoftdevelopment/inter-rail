<?php /***************************************************************
 * 	    	 	   	    FORM SECTION FOR RATE                        *
 *********************************************************************/

use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View                 */
/* @var $model common\models\Rates         */
/* @var $types \common\models\Taxonomies[] */

$form = ActiveForm::begin( [
    'options'                => [
        'class'                 => 'form-horizontal form-label-left',
        'id'                    => 'rate-form',
        'data-func'             => 'formCallback',
        'data-redirectonfinish' => Url::toRoute( [
            '/rates'
        ] )
    ],
    'enableAjaxValidation'   => false,
    'enableClientValidation' => false
] ); ?>

    <!-- form details -->
    <div class="x_panel">

        <div class="x_content">

            <div class="col-sm-12 col-md-12 col-xs-12 container-type-lists">

                <label class="control-label" for="container-type">
					<?= __( 'Container Type' ) ?>
                </label>

                <ul>

					<?php foreach ( $types as $type ) { ?>

                        <li>

                            <label class="radio">

								<?= $type->name ?>
                                <input type="radio" name="Rates[containers_type]" value="<?= $type->id ?>" <?= $model->containers_type == $type->id ? 'checked' : '' ?>>
                                <span class="checkmark"></span>

                            </label>

                        </li>

					<?php } ?>

                </ul>

            </div>

            <div class="col-sm-12 col-md-12 col-xs-12">

                <div class="col-sm-3 col-md-3 col-xs-12">

					<?= $form
						-> field( $model, 'from' )
						-> textInput( [
							'class'       => 'form-control',
							'id'          => 'transportation-from-field',
							'placeholder' => __( 'From' )
						] )
						-> label(
							__( 'From' )
						);
					?>

                </div>

                <div class="col-sm-3 col-md-3 col-xs-12">

					<?= $form
						-> field( $model, 'to' )
						-> textInput( [
							'class'       => 'form-control',
							'id'          => 'transportation-to-field',
							'placeholder' => __( 'To' )
						] )
						-> label(
							__( 'To' )
						);
					?>

                </div>

                <div class="col-sm-2 col-md-2 col-xs-12">

					<?= $form
						-> field( $model, 'term_from' )
						-> dropDownList( Yii::$app->common->transportationDetails( 'terms' ), [
							'class' => 'form-control',
							'prompt' => __(  'Term From' )
						] )
						-> label( __(  'Term From' ) );
					?>

                </div>

                <div class="col-sm-2 col-md-2 col-xs-12">

					<?= $form
						-> field( $model, 'term_to' )
						-> dropDownList( Yii::$app->common->transportationDetails( 'terms' ), [
							'class' => 'form-control',
							'prompt' => __(  'Term To' )
						] )
						-> label( __(  'Term To' ) );
					?>

                </div>

                <div class="col-sm-2 col-md-2 col-xs-12">

					<?= $form
						-> field( $model, 'transportation' )
						-> dropDownList( Yii::$app->common->transportationTypes( 'transportation' ), [
							'class' => 'form-control',
							'prompt' => __(  'Type' )
						] )
						-> label( __(  'Type' ) );
					?>

                </div>

            </div>

            <div class="col-sm-12 col-md-12 col-xs-12 rates-lists" style="padding: 0px;">

                <h4>
                    <?= __( 'Rates Details' ) ?>
                </h4>

                <div class="col-sm-12 col-md-12 col-xs-12">

                    <div class="col-sm-4 col-md-4 col-xs-12">

						<?= $form
							-> field( $model, 'thc' )
							-> textInput( [
								'class'       => 'form-control',
								'id'          => 'thc-field',
								'placeholder' => __( 'THC' )
							] )
							-> label(
								__( 'THC' )
							);
						?>

                    </div>

                    <div class="col-sm-4 col-md-4 col-xs-12">

						<?= $form
							-> field( $model, 'forming' )
							-> textInput( [
								'class'       => 'form-control',
								'id'          => 'forming-field',
								'placeholder' => __( 'Forming BT' )
							] )
							-> label(
								__( 'Forming BT' )
							);
						?>

                    </div>

                    <div class="col-sm-4 col-md-4 col-xs-12">

						<?= $form
							-> field( $model, 'forwarding' )
							-> textInput( [
								'class'       => 'form-control',
								'id'          => 'forwarding-field',
								'placeholder' => __( 'Forwarding' )
							] )
							-> label(
								__( 'Forwarding' )
							);
						?>

                    </div>

                    <div class="col-sm-4 col-md-4 col-xs-12">

						<?= $form
							-> field( $model, 'wd' )
							-> textInput( [
								'class'       => 'form-control',
								'id'          => 'wagon-detention-field',
								'placeholder' => __( 'Wagon Detention' )
							] )
							-> label(
								__( 'Wagon Detention' ) . ' <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title=" ' . __( 'Import: Wagon Detention Cost; Export: Railway Surcharges' ) .' "></i>'
							);
						?>

                    </div>

                    <div class="col-sm-4 col-md-4 col-xs-12">

						<?= $form
							-> field( $model, 'rental' )
							-> textInput( [
								'class'       => 'form-control',
								'id'          => 'rental-field',
								'placeholder' => __( 'Rental' )
							] )
							-> label(
								__( 'Rental' )
							);
						?>

                    </div>

                    <div class="col-sm-4 col-md-4 col-xs-12">

						<?= $form
							-> field( $model, 'margin' )
							-> textInput( [
								'class'       => 'form-control',
								'id'          => 'margin-field',
								'placeholder' => __( 'TYK Margin' )
							] )
							-> label(
								__( 'TYK Margin' )
							);
						?>

                    </div>

                    <div class="col-sm-4 col-md-4 col-xs-12">

						<?= $form
							-> field( $model, 'rental_period' )
							-> textInput( [
								'class'       => 'form-control',
								'id'          => 'rental-period-field',
								'placeholder' => __( 'Rental Period' )
							] )
							-> label(
								__( 'Rental Period' )
							);
						?>

                    </div>

                    <div class="col-sm-4 col-md-4 col-xs-12">

						<?= $form
							-> field( $model, 'surcharge' )
							-> textInput( [
								'class'       => 'form-control',
								'id'          => 'surcharge-field',
								'placeholder' => __( 'Surcharge' )
							] )
							-> label(
								__( 'Surcharge' ) . ' <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title=" ' . __( 'Import: Shunting works up to 10 wagon; Export: Permission for lading' ) .' "></i>'
							);
						?>

                    </div>

                </div>

            </div>

            <div class="form-group">

                <div class="col-md-12 col-md-12 col-xs-12 text-center">

                    <input class="btn btn-primary" data-value="test" type="submit" value="<?= ( $model->isNewRecord
						? __(  'Publish' )
						: __(  'Update' ) )
					?>">

                </div>

            </div>

        </div>

    </div>
    <!-- form details -->

    <!-- customs rate section -->
    <div class="x_panel no-top-border">

        <div class="x_title" style="padding: 10px;">

            <h2>
				<?= __(  'Customs' ) ?>
            </h2>

            <ul class="nav navbar-right panel_toolbox slides" style="min-width: 20px;">

                <li>

                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>

                </li>

                <li class="dropdown">

                    <a href="#" class="dropdown-toggle" id="add-custom-rate" data-toggle="dropdown" data-type="customs" data-translations="<?= htmlentities( json_encode( [
						'title'   => __( 'Custom was added successfully' ) . ' !!!',
						'message' => __( 'Custom was added successfully' ) . '. ' . __( 'Check the bottom of current list' ),
						'type'    => 'success'
					] ) ) ?>">
                        +
                    </a>

                </li>

            </ul>

            <div class="clearfix"></div>

        </div>

        <div class="x_content" style="padding: 0px;">

            <div class="customs-container">

				<?php if ( ! empty( $model->customs )
                    && isset( $model->customs->territory )
                ) {

					foreach ( $model->customs->territory as $key => $territory ) { ?>

						<?= $this->render( 'form-sections/customs-rate', [
							'model' => [
								'territory' => $territory,
                                'rate'      => isset( $model->customs->rate[ $key ] )
                                    ? $model->customs->rate[ $key ]
                                    : 0
						    ]
                        ] ); ?>

					<?php }

				} ?>

            </div>

        </div>

    </div>
    <!-- customs rate section -->

<?php ActiveForm::end(); ?>

<script type="text/html" id="customs-template">

	<?= $this->render( 'form-sections/customs-rate', [
		'model' => [
			'territory' => '',
			'rate'      => 0
		]
	] ); ?>

</script>
