<?php /***************************************************************
 *        		 	      RATES FILTERS SECTION                      *
 *********************************************************************/

use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

use common\models\Taxonomies;

/** @var $searchModel common\models\RatesSearch */

$form = ActiveForm::begin( [
	'id'      => 'main-filter-form',
	'method'  => 'get',
	'action'  => Url::to( [
		'/rates'
	] )
] ); ?>

    <ul class="list-filters">

        <li class="filter-element">

            <div class="form-group">

				<?= Html::textInput( 'from',
					Yii::$app->getRequest()->getQueryParam( 'from' ), [
						'placeholder' => __(  'From' ),
						'class'       => 'form-control'
				] ); ?>

            </div>

        </li>

        <li class="filter-element">

            <div class="form-group">

				<?= Html::dropDownList(
					'term_from',
					Yii::$app->getRequest()->getQueryParam( 'term_from' ),
					Yii::$app->common->transportationDetails( 'terms' ),
					[
						'prompt' => __(  'Term From' ),
						'class'  => 'form-control'
					]
				); ?>

            </div>

        </li>

        <li class="filter-element">

            <div class="form-group">

				<?= Html::textInput( 'to',
					Yii::$app->getRequest()->getQueryParam( 'to' ), [
						'placeholder' => __(  'To' ),
						'class'       => 'form-control'
					] ); ?>

            </div>

        </li>

        <li class="filter-element">

            <div class="form-group">

				<?= Html::dropDownList(
					'term_to',
					Yii::$app->getRequest()->getQueryParam( 'term_to' ),
					Yii::$app->common->transportationDetails( 'terms' ),
					[
						'prompt' => __(  'Term From' ),
						'class'  => 'form-control'
					]
				); ?>

            </div>

        </li>

        <li class="filter-element">

            <div class="form-group">

				<?= Html::dropDownList(
					'containers_type',
					Yii::$app->getRequest()->getQueryParam( 'containers_type' ),
					ArrayHelper::map( Taxonomies::findAll( [
						'type'      => 'containers',
						'isDeleted' => false
					] ), 'id', 'name' ),
					[
						'prompt' => __(  'Select container type' ),
						'class'  => 'form-control'
					]
				); ?>

            </div>

        </li>

        <li class="filter-element">

            <div class="form-group">

				<?= Html::checkbox(
					'import',
					Yii::$app->getRequest()->getQueryParam( 'import' ),
					[
						'class' => 'form-control switchery'
					]
				); ?> <?= __( 'Import' ) ?>

            </div>

        </li>

        <li class="filter-element">

            <div class="form-group">

				<?= Html::checkbox(
					'export',
					Yii::$app->getRequest()->getQueryParam( 'export' ),
					[
						'class' => 'form-control switchery'
					]
				); ?> <?= __( 'Export' ) ?>

            </div>

        </li>

        <li class="filter-element">

            <div class="form-group">

				<?= Html::textInput(
					'weight',
					Yii::$app->getRequest()->getQueryParam( 'weight' ),
					[
						'placeholder' => __(  'Weight' ),
						'class'       => 'form-control'
					]
				); ?>

            </div>

        </li>

        <li class="filter-element">

            <div class="form-group">

				<?= Html::textInput(
					'days',
					Yii::$app->getRequest()->getQueryParam( 'days' ),
					[
						'placeholder' => __(  'Days' ),
						'class'       => 'form-control'
					]
				); ?>

            </div>

        </li>

        <li class="filter-element">

            <div class="form-group">

				<?= Html::textInput(
					'quantity',
					Yii::$app->getRequest()->getQueryParam( 'quantity' ),
					[
						'placeholder' => __(  'Quantity' ),
						'class'       => 'form-control'
					]
				); ?>

            </div>

        </li>

        <li class="filter-element">

            <div class="form-group">

				<?= Html::textInput(
					'rate',
					Yii::$app->getRequest()->getQueryParam( 'rate' ),
					[
						'placeholder' => __(  'Rate' ),
						'class'       => 'form-control'
					]
				); ?>

            </div>

        </li>

        <li class="filter-element">

            <button class="btn btn-default">
                <?= __(  'Calculate' ); ?>
            </button>

        </li>

    </ul>

<?php ActiveForm::end(); ?>


