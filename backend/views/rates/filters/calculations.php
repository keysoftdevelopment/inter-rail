<?php /***************************************************************
 *        		 	      RATES FILTERS SECTION                      *
 *********************************************************************/

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $searchModel common\models\RatesSearch */

$form = ActiveForm::begin( [
	'id'      => 'main-filter-form',
	'method'  => 'get',
	'action'  => Url::to( [
		'/rates'
	] )
] ); ?>

    <a href="javascript:void(0)" id="add-filter-params" data-type="rate-filters">
        <i class="fa fa-plus-circle"></i> <?= __( 'Add filter' ) ?>
    </a>

    <ul class="list-filters">

        <li class="filter-element">

            <div class="form-group">

				<?= Html::textInput( 'from',
					Yii::$app->getRequest()->getQueryParam( 'from' ), [
						'placeholder' => __(  'From' ),
						'class'       => 'form-control'
				] ); ?>

            </div>

        </li>

        <li class="filter-element">

            <div class="form-group">

				<?= Html::dropDownList(
					'term_from',
					Yii::$app->getRequest()->getQueryParam( 'term_from' ),
					Yii::$app->common->transportationDetails( 'terms' ),
					[
						'prompt' => __(  'Term From' ),
						'class'  => 'form-control'
					]
				); ?>

            </div>

        </li>

        <li class="filter-element">

            <div class="form-group">

				<?= Html::textInput( 'to',
					Yii::$app->getRequest()->getQueryParam( 'to' ), [
						'placeholder' => __(  'To' ),
						'class'       => 'form-control'
					] ); ?>

            </div>

        </li>

        <li class="filter-element">

            <div class="form-group">

				<?= Html::dropDownList(
					'term_to',
					Yii::$app->getRequest()->getQueryParam( 'term_to' ),
					Yii::$app->common->transportationDetails( 'terms' ),
					[
						'prompt' => __(  'Term From' ),
						'class'  => 'form-control'
					]
				); ?>

            </div>

        </li>

    </ul>

    <div class="rate-filters-container">

        <?php if( ! empty( Yii::$app->getRequest()->getQueryParam( 'filters' ) ) ) {

            //param initialization
            $filters = Yii::$app->getRequest()->getQueryParam( 'filters' );

            foreach ( $filters[ 'containers_type' ] as $key => $type ) {

                echo $this->render( 'calculation', [
                    'model' => [
                        'container_type' => $type,
                        'quantity'       => isset( $filters[ 'quantity' ][ $key ] )
                            ? $filters[ 'quantity' ][ $key ]
                            : 0,
                        'weight' => isset( $filters[ 'weight' ][ $key ] )
							? $filters[ 'weight' ][ $key ]
							: 0,
						'days' => isset( $filters[ 'days' ][ $key ] )
							? $filters[ 'days' ][ $key ]
							: 0,
						'transportation' => [
                            'key'   => $key,
                            'value' => isset( $filters[ 'transportation' ][ $key ] )
                                ? $filters[ 'transportation' ][ $key ]
                                : 0,
                        ],
						'rate' => isset( $filters[ 'rate' ][ $key ] )
							? $filters[ 'rate' ][ $key ]
							: 0,
                    ]
                ] );
            }

        } ?>


    </div>

    <div class="col-sm-12 col-md-12 col-xs-12" style="padding: 0px">

        <button class="btn btn-default">
			<?= __(  'Calculate' ); ?>
        </button>

    </div>

<?php ActiveForm::end(); ?>

<script type="text/html" id="rate-filters-template">

	<?= $this->render(
        'calculation'
    ); ?>

</script>
