<?php /***************************************************************
 *      		 	      SINGLE RATE FILTER SECTION                 *
 *********************************************************************/


use kartik\helpers\Html;
use yii\helpers\ArrayHelper;

use common\models\Taxonomies;

/** @var $model array */ ?>

    <div class="rate-filter col-sm-12 col-md-12 col-xs-12">

        <div class="col-sm-3 col-md-3 col-xs-12">

            <div class="form-group">

                <?= Html::dropDownList(
                    'filters[containers_type][]',
                    isset( $model[ 'container_type' ] )
                        ? $model[ 'container_type' ]
                        : '',
                    ArrayHelper::map( Taxonomies::findAll( [
                        'type'      => 'containers',
                        'isDeleted' => false
                    ] ), 'id', 'name' ),
                    [
                        'prompt' => __(  'Select container type' ),
                        'class'  => 'form-control'
                    ]
                ); ?>

            </div>

        </div>

        <div class="col-sm-2 col-md-2 col-xs-12">

            <div class="rate-transportation-fields">

                <div class="form-group">

                    <input type="radio" name="filters[transportation][<?= isset( $model[ 'transportation' ] )
                        ? $model[ 'transportation' ][ 'key' ]
                        : 0
                    ?>]" class="transportation-field" value="export" <?= ( isset( $model[ 'transportation' ] ) && $model[ 'transportation' ][ 'value' ] == 'export' )
                    || ! isset( $model[ 'transportation' ] )
                        ? 'checked'
                        : ''
                    ?>>

                    <label for="export">
                        <?= __( 'Export' ) ?>
                    </label>

                </div>

                <div class="form-group">

                    <input type="radio" name="filters[transportation][<?= isset( $model[ 'transportation' ] )
                        ? $model[ 'transportation' ][ 'key' ]
                        : 0
                    ?>]" class="transportation-field" value="import" <?= ( isset( $model[ 'transportation' ] ) && $model[ 'transportation' ][ 'value' ] == 'import' )
                        ? 'checked'
                        : ''
                    ?>>

                    <label for="import">
                        <?= __( 'Import' ) ?>
                    </label>

                </div>

            </div>

        </div>

        <div class="col-sm-2 col-md-2 col-xs-12">

            <div class="form-group">

                <?= Html::textInput(
                    'filters[weight][]',
                    isset( $model[ 'weight' ] )
                        ? $model[ 'weight' ]
                        : 0,
                    [
                        'placeholder' => __(  'Weight' ),
                        'class'       => 'form-control'
                    ]
                ); ?>

            </div>

        </div>

        <div class="col-sm-1 col-md-1 col-xs-12">

            <div class="form-group">

                <?= Html::textInput(
                    'filters[days][]',
                    isset( $model[ 'days' ] )
                        ? $model[ 'days' ]
                        : 0, [
                        'placeholder' => __(  'Days' ),
                        'class'       => 'form-control'
                    ]
                ); ?>

            </div>

        </div>

        <div class="col-sm-1 col-md-1 col-xs-12">

            <div class="form-group">

                <?= Html::textInput(
                    'filters[quantity][]',
                    isset( $model[ 'quantity' ] )
                        ? $model[ 'quantity' ]
                        : 0, [
                        'placeholder' => __(  'Quantity' ),
                        'class'       => 'form-control'
                    ]
                ); ?>

            </div>

        </div>

        <div class="col-sm-2 col-md-2 col-xs-12">

            <div class="form-group">

                <?= Html::textInput(
                    'filters[rate][]',
                    isset( $model[ 'rate' ] )
                        ? $model[ 'rate' ]
                        : 0, [
                        'placeholder' => __(  'Rate' ),
                        'class'       => 'form-control'
                    ]
                ); ?>

            </div>

        </div>

        <div class="col-sm-1 col-md-1 col-xs-12 remove-rate-option">

            <a href="javascript:void(0)" class="remove-current-option" data-parent="rate-filter" data-translations="<?= htmlentities( json_encode( [
                'title'   => __( 'CONFIRMATION MODULE' ),
                'message' => __( 'Do you really want to delete this option?' )
            ] ) ) ?>">
                <i class="fa fa-remove"></i>
            </a>

        </div>

    </div>
