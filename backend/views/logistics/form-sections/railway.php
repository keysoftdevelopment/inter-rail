<?php /***************************************************************
 * 	    	 	 FORM SECTION FOR LOGISTICS TYPE RAILWAY             *
 *********************************************************************/

use dosamigos\tinymce\TinyMce;

use common\models\Taxonomies;

/* @var $model common\models\Posts             */
/* @var $form yii\widgets\ActiveForm           */
/* @var $containers common\models\Taxonomies[] */

//railway types lists
$railway_types = Taxonomies::find()
	-> where( [
		'type'      => 'railway',
		'isDeleted' => false
	] )
	-> all();
?>

    <div class="col-sm-12 col-md-12 col-xs-12 railway-type-lists">

        <label class="control-label" for="railway-type">
            <?= __( 'Railway Type' ) ?>
        </label>

        <ul>

            <?php foreach ( $railway_types as $type ) { ?>

                <li>

                    <label class="radio">

                        <?= $type->name ?>
                        <input type="radio" name="Logistics[railway_type]" value="<?= $type->id ?>" <?= $model->railway_type == $type->id ? 'checked' : '' ?>>
                        <span class="checkmark"></span>

                    </label>

                </li>

            <?php } ?>

        </ul>

    </div>

    <div class="col-sm-12 col-md-12 col-xs-12">

        <div class="col-sm-3 col-md-3 col-xs-12">

            <?= $form
                -> field( $model, 'status' )
                -> dropDownList( \Yii::$app->common->transportationTypes(), [
                    'prompt' => __(  'Choose Status' )
                ] )
                -> label( __(  'Choose Status' ) );
            ?>

        </div>

        <div class="col-sm-3 col-md-3 col-xs-12">

			<?= $form
				-> field( $model, 'weight' )
				-> textInput( [
					'id'          => 'weight',
					'placeholder' => __(  'Weight' )
				] )
				-> label( __(  'Weight' ) );
			?>

        </div>

        <div class="col-sm-3 col-md-3 col-xs-12">

            <?= $form
                -> field( $model, 'duration' )
                -> textInput( [
                    'id'          => 'duration',
                    'placeholder' => __(  'Transit Time' ),
					'value'       => ! empty( $model->duration )
						? (  ( $model->duration / 24 ) / 3600 )
						: $model->duration
                ] )
                -> label( __(  'Transit Time' ) );
            ?>

        </div>

        <div class="col-sm-3 col-md-3 col-xs-12">

            <?= $form
                -> field( $model, 'rate' )
                -> textInput( [
                    'id'          => 'rate',
                    'placeholder' => __(  'Rate' )
                ] )
                -> label( __(  'Rate' ) );
            ?>

        </div>

    </div>

    <div class="col-sm-12 col-md-12 col-xs-12 container-type-lists">

        <label class="control-label" for="container-type">
            <?= __( 'Container Type' ) ?>
        </label>

        <ul>

            <?php foreach ( $containers as $container ) { ?>

                <li>

                    <label class="radio">

                        <?= $container->name ?>
                        <input type="radio" name="Logistics[containers_type]" value="<?= $container->id ?>" <?= $model->containers_type == $container->id ? 'checked' : '' ?>>
                        <span class="checkmark"></span>

                    </label>

                </li>

            <?php } ?>

        </ul>

    </div>

	<?= $form
		-> field( $model, 'description' )
		-> widget( TinyMce::className(), [
			'options' => [
				'rows' => 15
			],
			'language'      => 'en_GB',
			'clientOptions' => [
				'menubar'   => false,
				'plugins'   => [
					"advlist image autolink lists link charmap print preview anchor",
					"searchreplace visualblocks code fullscreen",
					"insertdatetime media table contextmenu paste"
				],
				'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | visualblocks | code "
			]
		] )
		-> label( false );
	?>