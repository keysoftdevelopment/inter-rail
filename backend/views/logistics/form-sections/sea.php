<?php /***************************************************************
 * 	    	 	   FORM SECTION FOR LOGISTICS TYPE SEA               *
 *********************************************************************/

use dosamigos\tinymce\TinyMce;

/* @var $model common\models\Posts             */
/* @var $form yii\widgets\ActiveForm           */
/* @var $containers common\models\Taxonomies[] */
 ?>

	<div class="col-sm-12 col-md-12 col-xs-12">

		<div class="col-sm-3 col-md-3 col-xs-12">

			<?= $form
				-> field( $model, 'name' )
				-> textInput( [
					'id'          => 'name',
					'placeholder' => __(  'Name' )
				] )
				-> label( __(  'Name' ) );
			?>

		</div>

		<div class="col-sm-3 col-md-3 col-xs-12">

			<?= $form
				-> field( $model, 'transportation' )
				-> dropDownList( \Yii::$app->common->transportationTypes( 'transportation' ), [
					'prompt' => __(  'Choose Transportation Type' )
				] )
				-> label( __(  'Transportation Type' ) );
			?>

		</div>

        <div class="col-sm-3 col-md-3 col-xs-12">

			<?= $form
				-> field( $model, 'c_type' )
				-> dropDownList(
					\Yii::$app->common->transportationTypes( 'containers_type' )
				)
				-> label( __(  'COC/SOC' ) );
			?>

        </div>

        <div class="col-sm-3 col-md-3 col-xs-12">

			<?= $form
				-> field( $model, 'duration' )
				-> textInput( [
					'id'          => 'duration',
					'placeholder' => __(  'Transit Time' ),
					'value'       => ! empty( $model->duration )
						? (  ( $model->duration / 24 ) / 3600 )
						: $model->duration
				] )
				-> label( __(  'Transit Time' ) );
			?>

        </div>

	</div>

    <div class="col-sm-12 col-md-12 col-xs-12 container-type-lists">

        <label class="control-label" for="container-type">
            <?= __( 'Container Type' ) ?>
        </label>

        <ul>

            <?php foreach ( $containers as $container ) { ?>

                <li>

                    <label class="radio">

                        <?= $container->name ?>
                        <input type="radio" name="Logistics[containers_type]" value="<?= $container->id ?>" <?= $model->containers_type == $container->id ? 'checked' : '' ?>>
                        <span class="checkmark"></span>

                    </label>

                </li>

            <?php } ?>

        </ul>

    </div>

	<div class="col-sm-12 col-md-12 col-xs-12 rate-details-container">

		<div class="col-sm-2 col-md-2 col-xs-12">

			<?= $form
				-> field( $model, 'rate' )
				-> textInput( [
					'id'          => 'rate',
					'placeholder' => __(  'Rate' ),
                    'class'       => 'price-field form-control'
				] )
				-> label( __(  'Rate' ) );
			?>

		</div>

        <div class="col-sm-2 col-md-2 col-xs-12">

			<?= $form
				-> field( $model, 'dthc' )
				-> textInput( [
					'id'          => 'dthc',
					'placeholder' => __(  'DTHC' ),
					'class'       => 'price-field form-control'
				] )
				-> label( __(  'DTHC' ) );
			?>

        </div>

        <div class="col-sm-2 col-md-2 col-xs-12">

			<?= $form
				-> field( $model, 'othc' )
				-> textInput( [
					'id'          => 'othc',
					'placeholder' => __(  'OTHC' ),
					'class'       => 'price-field form-control'
				] )
				-> label( __(  'OTHC' ) );
			?>

        </div>

        <div class="col-sm-2 col-md-2 col-xs-12">

			<?= $form
				-> field( $model, 'bl' )
				-> textInput( [
					'id'          => 'bl',
					'placeholder' => __(  'BL' ),
					'class'       => 'price-field form-control'
				] )
				-> label( __(  'BL' ) );
			?>

        </div>

		<div class="col-sm-2 col-md-2 col-xs-12">

			<?= $form
				-> field( $model, 'surcharge' )
				-> textInput( [
					'id'          => 'surcharge',
					'placeholder' => __(  'Surcharge' ),
					'class'       => 'price-field form-control'
				] )
				-> label( __(  'Surcharge' ) );
			?>

		</div>

        <div class="col-sm-2 col-md-2 col-xs-12">

			<?= $form
				-> field( $model, 'total' )
				-> textInput( [
					'id'          => 'total',
					'placeholder' => __(  'Total' ),
					'class'       => 'total-sum form-control'
				] )
				-> label( __(  'Total' ) );
			?>

        </div>

	</div>

	<?= $form
		-> field( $model, 'description' )
		-> widget( TinyMce::className(), [
			'options' => [
				'rows' => 15
			],
			'language'      => 'en_GB',
			'clientOptions' => [
				'menubar'   => false,
				'plugins'   => [
					"advlist image autolink lists link charmap print preview anchor",
					"searchreplace visualblocks code fullscreen",
					"insertdatetime media table contextmenu paste"
				],
				'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | visualblocks | code "
			]
		] )
		-> label( false );
	?>