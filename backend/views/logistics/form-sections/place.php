<?php /***************************************************************
 *	 	       			   SINGLE PLACE SECTION 				     *
 *		             	I.E. ALLOW CHOOSING PLACE 					 *
 * 						   FROM REQUESTED LIST  					 *
 *********************************************************************/

use yii\helpers\Html;

/* @var $model array  */
/* @var $places mixed */ ?>

<div class="option-section">

	<label for="place" class="col-md-1 col-sm-1 col-xs-12">
		<?= $model[ 'label' ] ?>
	</label>

	<div class="col-md-11 col-sm-11 col-xs-12">

        <?= Html::dropDownList(
                'place[]',
			$model[ 'value' ],
			$places,
			[
				'class'    => 'place-field select2_single form-control',
				'style'    => 'width:100%',
				'data-msg' => json_encode( [
					'option'    => __( 'Choose place' ),
					'no_result' => __( 'No result found' )
				] )
			] )
        ?>

	</div>

	<a href="javascript:void(0)" class="remove-current-place remove-current-option" data-parent="option-section" data-translations="<?= htmlentities( json_encode( [
		'title'   => __( 'CONFIRMATION MODULE' ),
		'message' => __( 'Do you really want to delete this option?' )
	] ) ) ?>">
		x
	</a>

</div>