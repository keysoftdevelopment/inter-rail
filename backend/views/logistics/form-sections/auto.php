<?php /***************************************************************
 * 	    	 	  FORM SECTION FOR LOGISTICS TYPE AUTO               *
 *********************************************************************/

use dosamigos\tinymce\TinyMce;

/* @var $model common\models\Logistics         */
/* @var $form yii\widgets\ActiveForm           */
/* @var $containers common\models\Taxonomies[] */ ?>

    <div class="col-sm-12 col-md-12 col-xs-12 container-type-lists">

        <label class="control-label" for="container-type">
            <?= __( 'Container Type' ) ?>
        </label>

        <ul>

            <?php foreach ( $containers as $container ) { ?>

                <li>

                    <label class="radio">

                        <?= $container->name ?>
                        <input type="radio" name="Logistics[containers_type]" value="<?= $container->id ?>" <?= $model->containers_type == $container->id ? 'checked' : '' ?>>
                        <span class="checkmark"></span>

                    </label>

                </li>

            <?php } ?>

        </ul>

    </div>

    <div class="col-sm-12 col-md-12 col-xs-12">

        <div class="col-sm-3 col-md-3 col-xs-12">

			<?= $form
				-> field( $model, 'transportation' )
				-> dropDownList( \Yii::$app->common->transportationTypes( 'transportation' ), [
					'prompt' => __(  'Transportation Type' )
				] )
				-> label( __(  'Transportation Type' ) );
			?>

        </div>

        <div class="col-sm-3 col-md-3 col-xs-12">

            <?= $form
                -> field( $model, 'status' )
                -> dropDownList( \Yii::$app->common->transportationTypes(), [
                    'prompt' => __(  'Choose Status' )
                ] )
                -> label( __(  'Choose Status' ) );
            ?>

        </div>

        <div class="col-sm-3 col-md-3 col-xs-12">

			<?= $form
				-> field( $model, 'duration' )
				-> textInput( [
					'id'          => 'duration',
					'placeholder' => __(  'Transit Time' ),
                    'value'       => ! empty( $model->duration )
                        ? (  ( $model->duration / 24 ) / 3600 )
                        : $model->duration
				] )
				-> label( __(  'Transit Time' ) );
			?>

        </div>

        <div class="col-sm-3 col-md-3 col-xs-12">

			<?= $form
				-> field( $model, 'rate' )
				-> textInput( [
					'id'          => 'rate',
					'placeholder' => __(  'Rate' )
				] )
				-> label( __(  'Rate' ) );
			?>

        </div>

    </div>

	<?= $form
		-> field( $model, 'description' )
		-> widget( TinyMce::className(), [
			'options' => [
				'rows' => 15
			],
			'language'      => 'en_GB',
			'clientOptions' => [
				'menubar'   => false,
				'plugins'   => [
					"advlist image autolink lists link charmap print preview anchor",
					"searchreplace visualblocks code fullscreen",
					"insertdatetime media table contextmenu paste"
				],
				'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | visualblocks | code "
			]
		] )
		-> label( false );
	?>