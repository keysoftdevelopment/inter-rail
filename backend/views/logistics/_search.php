<?php /***************************************************************
 *         		 	   	  SEARCH FORM SECTION                        *
 *********************************************************************/

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View                   */
/* @var $model common\models\LogisticsSearch */
/* @var $type string                         */ ?>

    <div class="col-md-6 col-sm-6 col-xs-12 form-group pull-right top_search">

        <div class="input-group common_search">

            <?php $form = ActiveForm::begin( [
                'action' => [
                    'index' ,
                    'type' => $type
                ],
                'method' => 'get',
                'options' => [
                    'style' => 'float: right;'
                ]
            ] ); ?>

                <?= $form->field( $model, $type == 'sea'
                    ? 'name'
                    : 'transport_no'
                )->textInput( [
                    'class'       => 'form-control',
                    'placeholder' => __(  'Search for..' )
                ] )->label( false ); ?>

                <?= Html::submitButton( __(  'Go!' ), [
                    'class' => 'btn btn-default'
                ] ) ?>

            <?php ActiveForm::end(); ?>

        </div>

    </div>