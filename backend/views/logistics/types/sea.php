<?php /***************************************************************
 *	     	       THE LIST OF ALL SEA LOGISTICS TYPE                *
 *********************************************************************/

use yii\helpers\Html;

/* @var $models common\models\Logistics[]	   */
/* @var $taxonomies common\models\Taxonomies[] */ ?>

<table class="table table-striped projects">

	<thead>

		<tr>

			<th>
				#
			</th>

			<th>
				<?= __(  'Name' ) ?>
			</th>

            <th>
				<?= __(  'Route' ) ?>
            </th>

			<th>
				<?= __(  'Section' ) ?>
			</th>

			<th>
				<?= __(  'COC/SOC' ) ?>
			</th>

			<th>
				<?= __(  'Container Type' ) ?>
			</th>

			<th>
				<?= __( 'Total' ) ?>
			</th>

			<th></th>

		</tr>

	</thead>

	<tbody>

        <?php if ( !empty( $models ) ) {

            //params initializations
            $container_types = \Yii::$app->common->transportationTypes( 'containers_type' );
            $sections        = \Yii::$app->common->transportationTypes( 'transportation' );

            foreach ( $models as $model ) { ?>

                <tr>

                    <td>
                        #
                    </td>

                    <td>

                        <?= Html::a( $model->name, [
                            'update',
                            'id' => $model->id
                        ], [
                            'role' => "button"
                        ] ) ?>

                    </td>

                    <td class="route">

                        <div class="route-polyline">

                            <div class="route-from">

                                <div class="point-a"></div>

                                <label>

                                    <?= ! empty( $model->from )
                                        ? $model->from
                                        : __( 'Not chosen' )
                                    ?>

                                </label>

                                <div class="point-b"></div>

                            </div>

                            <hr>

                            <div class="route-to">

                                <div class="point-a"></div>

                                <label>
                                    <?= ! empty( $model->to )
                                        ? $model->to
                                        : __( 'Not chosen' )
                                    ?>
                                </label>

                                <div class="point-b"></div>

                            </div>

                        </div>

                    </td>

                    <td>

                        <?= isset( $sections[ $model->transportation ] )
                            ? $sections[ $model->transportation ]
                            : ''
                        ?>

                    </td>

                    <td>

                        <?= isset( $container_types[ $model->c_type ] )
                            ? $container_types[ $model->c_type ]
                            : ''
                        ?>

                    </td>

                    <td>

                        <?= isset( $taxonomies[ 'container' ][ $model->id ]  )
                            ? $taxonomies[ 'container' ][ $model->id ][ 'name' ]
                            : __( 'Container Type Not Chosen' )
                        ?>

                    </td>

                    <td>
                        <?= $model->total ?>
                    </td>

                    <td class="text-center model-actions">

                        <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                            <i class="fa fa-ellipsis-v"></i>
                        </a>

                        <ul class="dropdown-menu pull-right">

                            <li>

                                <?= Html::a( '<i class="fa fa-pencil"></i> ' .  __(  'Edit' ), [
                                    'update',
                                    'id'    => $model->id
                                ] ); ?>

                            </li>

                            <li>

                                <?= Html::a( '<i class="fa fa-trash"> </i> '. __(  'Delete' ), [
                                    'delete',
                                    'id' => $model->id
                                ], [
                                    'data'  => [
                                        'confirm' => __(  'Do you really want to delete this type?' ),
                                        'method'  => 'post',
                                    ]
                                ] ); ?>

                            </li>

                        </ul>

                    </td>

                </tr>

            <?php }

        } else {

            echo '<tr>';

                echo '<td colspan="8" class="aligncenter">
                    ' . __(  'There is no transportation by sea yet' ) . '
                </td>';

            echo '</tr>';

        } ?>

	</tbody>

</table>