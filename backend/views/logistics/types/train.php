<?php /***************************************************************
 *	     	         THE LIST OF ALL AVAILABLE TRAIN`s               *
 *********************************************************************/

use yii\helpers\ArrayHelper;
use yii\helpers\Url;

use common\models\ContainersRelation;
use common\models\Expenses;
use common\models\LogisticsRelation;
use common\models\Orders;
use common\models\Tracking;

/* @var $models common\models\Logistics[] */ ?>

<table class="table table-striped bt">

	<thead>

		<tr>

			<th>
				#
			</th>

			<th>
				<?= __(  'Train number' ) ?>
			</th>

			<th>
				<?= __(  'Section' ) ?>
			</th>

			<th>
				<?= __(  'Departure/Arrival Dates' ) ?>
			</th>

			<th>
				<?= __(  'Wagon numbers' ) ?>
			</th>

			<th>
				<?= __(  'Containers quantity' ) ?>
			</th>

			<th>
				<?= __(  'Status' ) ?>
			</th>

		</tr>

	</thead>

	<tbody>

		<?php if ( !empty( $models ) ) {

			//bt relations due to requested logistic ids
			$tran_rel = LogisticsRelation::find()
				-> where( [
					'logistic_id' => ArrayHelper::getColumn(
						$models, 'id'
					),
					'type' => 'train'
				] )
				-> asArray()
				-> all();

			//containers quantity due to order ids
			$containers = ContainersRelation::find()
				-> select( [
					'foreign_id',
					'count(*) as qty'
				] )
				-> where( [
					'foreign_id' => ArrayHelper::getColumn(
						$tran_rel, 'foreign_id'
					),
					'type' => 'order'
				] )
				-> indexBy( 'foreign_id' )
				-> groupBy( 'foreign_id' )
				-> asArray()
				-> all();

			//tracking details
			$tracking = Tracking::find()
				-> select( [
					'foreign_id',
					'min(departure) as departure',
					'max(arrival) as arrival',
					'status'
				] )
				-> where( [
					'foreign_id' => ArrayHelper::getColumn(
						$tran_rel, 'foreign_id'
					),
					'type' => 'order'
				] )
				-> indexBy( 'foreign_id' )
				-> groupBy( 'foreign_id' )
				-> asArray()
				-> all();

			//expenses_total_sum
			$expenses = Expenses::find()
				-> select( [
					'order_id',
					'sum(total) as total'
				] )
				-> where( [
					'order_id' => ArrayHelper::getColumn(
						$tran_rel, 'foreign_id'
					)
				] )
				-> indexBy( 'order_id' )
				-> groupBy( 'order_id' )
				-> asArray()
				-> all();

			if ( ! empty( $tran_rel ) ) {

				$order_numbers = array_reduce( Orders::find()
					-> where( [
						'id' => ArrayHelper::getColumn(
							$tran_rel, 'foreign_id'
						),
						'isDeleted' => false
					] )
					-> andWhere( [
						'NOT IN', 'status', [
							'rejection'
						]
					] )
					-> andWhere( ! empty( Yii::$app->getRequest()->getQueryParam( 'status' ) ) ? [
						'=', 'status', Yii::$app->getRequest()->getQueryParam( 'status' )
					] : [] )
					-> asArray()
					-> all(), function( $result, $order ) use ( $tran_rel, $containers, $tracking, $expenses ) {

					if ( ! empty( $order ) ) {

						//param initialization
						$expense = isset( $expenses[ $order[ 'id' ] ] )
							? $expenses[ $order[ 'id' ] ][ 'total' ]
							: 0;

						foreach ( $tran_rel as $key => $value ) {

							if ( $value[ 'foreign_id' ] == $order[ 'id' ] ) {

								//set containers count option
								$count = isset( $containers[ $order[ 'id' ] ] )
									? $containers[ $order[ 'id' ] ][ 'qty' ]
									: 0;

								//set train status
								$status = isset( $tracking[ $order[ 'id' ] ][ 'status' ] )
									? $tracking[ $order[ 'id' ] ][ 'status' ]
									: 'not chosen';

								$result[ $value[ 'logistic_id' ] ] = [
									'total' => isset( $result[ $value[ 'logistic_id' ] ] )
										? $result[ $value[ 'logistic_id' ] ][ 'total' ] + $order[ 'total' ]
										: $order[ 'total' ],
									'expenses' => isset( $result[ $value[ 'logistic_id' ] ] )
										? (float)$result[ $value[ 'logistic_id' ] ][ 'expenses' ] + (float)$expense
										: (float)$expense,
									'count' => isset( $result[ $value[ 'logistic_id' ] ] )
										? (int)$result[ $value[ 'logistic_id' ] ][ 'count' ] + (int)$count
										: $count,
									'status' => isset( $result[ $value[ 'logistic_id' ] ] ) && $result[ $value[ 'logistic_id' ] ][ 'status' ] != 'not chosen'
										? $result[ $value[ 'logistic_id' ] ][ 'status' ]
										: $status,
									'section' => [
										'import' => ( isset( $result[ $value[ 'logistic_id' ] ] ) && (int)$result[ $value[ 'logistic_id' ] ][ 'section' ][ 'import' ] > 0 ) || $order[ 'transportation' ] == 'import'
											? 1
											: 0,
										'export' => ( isset( $result[ $value[ 'logistic_id' ] ] ) && (int)$result[ $value[ 'logistic_id' ] ][ 'section' ][ 'export' ] > 0 ) || $order[ 'transportation' ] == 'export'
											? 1
											: 0,
									],
									'arrival' => isset( $tracking[ $order[ 'id' ] ][ 'arrival' ] )
										? date( 'd-m-Y', strtotime( $tracking[ $order[ 'id' ] ][ 'arrival' ] ) )
										: __( 'arrival date not added yet' ),
									'departure' => isset( $tracking[ $order[ 'id' ] ][ 'departure' ] )
										? date( 'd-m-Y', strtotime( $tracking[ $order[ 'id' ] ][ 'departure' ] ) )
										: __( 'departure date not added yet' ),
									'order' => isset( $result[ $value[ 'logistic_id' ] ][ 'order' ] )
										? $result[ $value[ 'logistic_id' ] ][ 'order' ]
										: []
								];

								//push order details
								array_push( $result[ $value[ 'logistic_id' ] ][ 'order' ], [
									'id'     => $order[ 'id' ],
									'number' => $order[ 'number' ]
								] );

							}

						}

					}

					return $result;

				}, []

				);

			}

			foreach ( $models as $model ) { ?>

				<tr class="show-details">

					<td>
						#
					</td>

					<td>
						<?= $model->transport_no ?>
					</td>

					<td>

						<?= Yii::$app->common->transportationTypes( 'transportation' )[ $model->transportation ] ?>

					</td>

					<td>

						<?= isset( $order_numbers[ $model->id ][ 'departure' ] )
							? $order_numbers[ $model->id ][ 'departure' ]
							: __( 'departure date not added yet' )
						?> / <?= isset( $order_numbers[ $model->id ][ 'arrival' ] )
							? $order_numbers[ $model->id ][ 'arrival' ]
							: __( 'arrival date not added yet' )
						?>

					</td>

					<td>

						<p>
							<?= $model->railway_details ?>
						</p>

					</td>

					<td>

						<?= isset( $order_numbers[ $model->id ][ 'count' ] )
							? $order_numbers[ $model->id ][ 'count' ]
							: 0
						?>

					</td>

					<td>

						<?= isset( $order_numbers[ $model->id ][ 'status' ] )
							? __( $order_numbers[ $model->id ][ 'status' ] )
							: __( 'not chosen' )
						?>

					</td>

				</tr>

				<tr class="additional-details">

					<td colspan="8">

						<?php if ( isset( $order_numbers[ $model->id ] ) ) { ?>

							<h4>
								<?= __( 'Orders numbers' ) ?>
							</h4>

							<ul>

								<?php foreach ( $order_numbers[ $model->id ][ 'order' ] as $order ) { ?>

									<li class="col-sm-1 col-md-1 col-xs-2">

										<a href="<?= Url::to( [
											'/orders/update?id=' . $order[ 'id' ]
										] ) ?>">

											<?= empty( $order[ 'number' ] )
												? '#' . $order[ 'id' ]
												: $order[ 'number' ]
											?>

										</a>

									</li>

								<?php } ?>

							</ul>

							<div class="total-sum">

								<div class="total-expenses">

									<?= __( 'Expenses' ) ?> : <?= number_format(
										$order_numbers[ $model->id ][ 'expenses' ],
										2,
										'.',
										''
									);  ?>

								</div>

								<div class="total-expenses">

									<?= __( 'Income' ) ?> : <?= number_format(
										(float)( $order_numbers[ $model->id ][ 'total' ] ) - (float)$order_numbers[ $model->id ][ 'expenses' ],
										2,
										'.',
										''
									); ?>

								</div>

								<div class="total-sum">

									<?= __( 'Total' ) ?> : <?= number_format( $order_numbers[ $model->id ][ 'total' ],
										2,
										'.',
										''
									); ?>

								</div>

							</div>

						<?php } else {
							echo __( 'There is no order details yet' );
						} ?>

					</td>

				</tr>

			<?php }

		} else {

			echo '<tr>';

			echo '<td colspan="8" class="aligncenter">    
						' . __(  'There is no transportation by train yet' ) . '                    
					</td>';

			echo '</tr>';

		} ?>

	</tbody>

	<tfoot>

		<tr>

			<td colspan="8">

				<div class="col-sm-12 col-md-12 col-xs-12">

					<label class="col-sm-8 col-md-8 col-xs-12">
						<?= __(  'Containers quantity' ) ?>
					</label>

					<span class="pull-right">

							<?= array_reduce( isset( $order_numbers )
							&& ! empty( $order_numbers )
								? $order_numbers
								: [], function( $result, $order ) {

								if( ! empty( $order ) ) {
									$result += $order[ 'count' ];
								}

								return $result;

							}, 0 ); ?>

						</span>

				</div>

			</td>

		</tr>

		<tr>

			<td colspan="8">

				<div class="col-sm-12 col-md-12 col-xs-12">

					<label class="col-sm-8 col-md-8 col-xs-12">
						<?= __(  'Expenses' ) ?>
					</label>

					<span class="pull-right">

							<?= number_format( array_reduce( isset( $order_numbers )
							&& ! empty( $order_numbers )
								? $order_numbers
								: [], function( $result, $expenses ) {

								if( ! empty( $expenses ) ) {
									$result += $expenses[ 'expenses' ];
								}

								return $result;

							}, 0 ), 2, '.', '' ); ?>

						</span>

				</div>


			</td>

		</tr>

		<tr>

			<td colspan="8">

				<div class="col-sm-12 col-md-12 col-xs-12">

					<label class="col-sm-8 col-md-8 col-xs-12">
						<?= __(  'Income' ) ?>
					</label>

					<span class="pull-right">

							<?= number_format( array_reduce( isset( $order_numbers )
								&& ! empty( $order_numbers )
									? $order_numbers
									: [], function( $result, $expenses ) {

									if( ! empty( $expenses ) ) {
										$result += $expenses[ 'total' ];
									}

									return $result;

								}, 1 ) - array_reduce( isset( $order_numbers )
								&& ! empty( $order_numbers )
									? $order_numbers
									: [], function( $result, $expenses ) {

									if( ! empty( $expenses ) ) {
										$result += $expenses[ 'expenses' ];
									}

									return $result;

								}, 1 ), 2, '.', '' ); ?>

							</span>

				</div>

			</td>

		</tr>

		<tr>

			<td colspan="8">

				<div class="col-sm-12 col-md-12 col-xs-12">

					<label class="col-sm-8 col-md-8 col-xs-12">
						<?= __(  'Total' ) ?>
					</label>

					<span class="pull-right">

							<?= number_format( array_reduce( isset( $order_numbers )
							&& ! empty( $order_numbers )
								? $order_numbers
								: [], function( $result, $expenses ) {

								if( ! empty( $expenses ) ) {
									$result += $expenses[ 'total' ];
								}

								return $result;

							}, 0 ), 2, '.', '' ); ?>

							</span>

				</div>

			</td>

		</tr>

	</tfoot>

</table>