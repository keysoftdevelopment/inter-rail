<?php /***************************************************************
 *	     		 	   	  UPDATE LOGISTICS PART                      *
 *********************************************************************/

/* @var $this yii\web\View                     */
/* @var $model \common\models\Posts            */
/* @var $containers common\models\Taxonomies[] */
/* @var $places common\models\Places[]         */

// current page title
$this->title = __(  'Update logistic' ); ?>

    <div class="col-md-12 col-sm-12 col-xs-12 edit-container-block">

        <!-- title section -->
        <div class="page-title">

            <div class="title_left">

                <h3>
                    <?= $this->title ?>
                </h3>

            </div>

        </div>
        <!-- title section -->

        <!-- logistic details section -->
        <div class="col-md-12 col-sm-12 col-xs-12 logistics-form" style="padding: 0px">

            <?= $this->render( '_form', [
                'model'      => $model,
                'containers' => $containers,
                'places'     => $places
            ] ); ?>

        </div>
        <!-- logistic details section -->

    </div>