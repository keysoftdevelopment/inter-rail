<?php /***************************************************************
 *	                LOGISTIC SINGLE SETTING SECTION                  *
 *********************************************************************/

use yii\helpers\Html;

/* @var $model array */ ?>

	<div class="setting-option">

        <label for="<?= $model[ 'key' ] ?>" class="control-label col-md-2 col-sm-2 col-xs-12">
            <?= $model[ 'label' ] ?>
        </label>

        <div class="col-md-10 col-sm-10 col-xs-12">

            <?= Html::textInput(
                'SettingsForm[' . $model[ 'key' ] . '][' . $model[ 'lang_id' ] . '][]',
                isset( $model[ 'value' ] )
                    ? $model[ 'value' ]
                    : '', [
                    'class' => 'form-control col-md-12 col-sm-12 col-xs-12 ' . $model[ 'key' ] . '-field'
                ]
            ) ?>

        </div>

		<a href="javascript:void(0)" class="remove-current-option" data-parent="setting-option" data-translations="<?= htmlentities( json_encode( [
			'title'   => __( 'CONFIRMATION MODULE' ),
			'message' => __( 'Do you really want to delete this option?' )
		] ) ) ?>">
			x
		</a>

	</div>