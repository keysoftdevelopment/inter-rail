<?php /***************************************************************
 *         		 	    LOGISTIC FILTERS SECTION                     *
 *********************************************************************/

use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

use common\models\Places;
use common\models\Taxonomies;

/** @var $searchModel common\models\LogisticsSearch */
/** @var $type string  	                            */

$form = ActiveForm::begin( [
	'id'      => 'main-filter-form',
	'method'  => 'get',
	'action'  => Url::to( [
		'/logistics',
		'type' => $type
	] )
] ); ?>

    <ul class="list-filters">

		<?php if ( ! in_array( $type, [ 'blockTrain', 'train' ] )  ) { ?>

            <li class="filter-element">

				<?= $form
					-> field( $searchModel, 'from' )
					-> dropDownList( ArrayHelper::map( Places::findAll( [
						'type' => $type
					] ), 'name', 'name' ), [
						'prompt'           => __(  'Route From' ),
						'class'            => 'form-control select2_single',
						'style'            => 'width: 100%;',
						'data-live-search' => 'true',
						'data-default'     => $searchModel->from,
						'data-msg'         => json_encode( [
							'option'    => __( 'Route' ),
							'no_result' => __( 'No result found' )
						] )
					] )
					-> label( false );
				?>

            </li>

            <li class="filter-element">

				<?= $form
					-> field( $searchModel, 'to' )
					-> dropDownList( ArrayHelper::map( Places::findAll( [
						'type' => $type
					] ), 'name', 'name' ), [
						'prompt'           => __(  'Route To' ),
						'class'            => 'form-control select2_single',
						'style'            => 'width: 100%;',
						'data-live-search' => 'true',
						'data-default'     => $searchModel->to,
						'data-msg'         => json_encode( [
							'option'    => __( 'Route' ),
							'no_result' => __( 'No result found' )
						] )
					] )
					-> label( false );
				?>

            </li>

        <?php }

        if ( in_array( $type, [ 'railway', 'auto' ] ) ) { ?>

            <li class="filter-element">

				<?= $form
					-> field( $searchModel, 'status' )
					-> dropDownList( Yii::$app->common->transportationTypes( 'status' ), [
						'prompt' => __(  'Select status' ),
						'class'  => 'form-control'
					] )
					-> label( false );
				?>

            </li>

		<?php }

		if ( $type == 'sea'  ) { ?>

            <li class="filter-element">

				<?= $form
					-> field( $searchModel, 'c_type' )
					-> dropDownList( Yii::$app->common->transportationTypes( 'containers_type' ), [
						'prompt' => __(  'Select COC/SOC' ),
						'class'  => 'form-control'
					] )
					-> label( false );
				?>

            </li>

		<?php }

		if ( $type != 'railway'  ) { ?>

            <li class="filter-element">

                <?= $form
                    -> field( $searchModel, 'transportation' )
                    -> dropDownList( Yii::$app->common->transportationTypes( 'transportation' ), [
                        'prompt'     => __(  'Select section' ),
                        'class'      => 'form-control',
                        'data-style' => 'btn-primary',
                    ] )
                    -> label( false );
                ?>

            </li>

        <?php }

        if ( in_array( $type, [ 'sea', 'auto' ] ) ) { ?>

            <li class="filter-element">

                <div class="form-group">

                    <?= Html::dropDownList(
                        'containers_type',
                        Yii::$app->getRequest()->getQueryParam( 'containers_type' ),
                        ArrayHelper::map( Taxonomies::findAll( [
                            'type'      => 'containers',
                            'isDeleted' => false
                        ] ), 'id', 'name' ),
                        [
                            'prompt' => __(  'Select container type' ),
                            'class'  => 'form-control'
                        ]
                    ); ?>

                </div>

            </li>

		<?php }

		if ( in_array( $type, [ 'blockTrain', 'train' ] ) ) { ?>

            <li class="filter-element">

                <div class="form-group">

                    <?= Html::dropDownList(
                        'status',
                        Yii::$app->getRequest()->getQueryParam( 'status' ),
                        Yii::$app->common->statuses( 'order' ),
                        [
                            'prompt' => __(  'Select status' ),
                            'class'  => 'form-control'
                        ]
                    ); ?>

                </div>

            </li>

        <?php } ?>

        <li class="filter-element">

            <button class="btn btn-default">
                <?= __(  'Filter' ); ?>
            </button>

        </li>

    </ul>

<?php ActiveForm::end(); ?>


