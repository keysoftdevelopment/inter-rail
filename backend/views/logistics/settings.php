<?php /***************************************************************
 *	       THE LIST OF ALL AVAILABLE LOGISTICS SETTINGS PAGE         *
 *********************************************************************/

/* @var $model common\models\SettingsForm */

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

use common\models\Languages;
use common\models\Places;

use common\widgets\Alert;

//current page title
$this->title = __(  'Settings' );

// the list of available languages
$languages = ArrayHelper::map(
	Languages::find()->all(),
	'id',
	'description'
);

//list of settings
$settings = [
    'packages' => [
		'type'  => 'packages',
		'label' => __(  'Package' )
    ],
    'terms' => [
		'type'  => 'terms',
		'label' => __(  'Term' )
    ],
    'import_settings' => [
        'type'  => 'import_settings',
        'items' => [
            [
				'label'    => __( 'Choose Default Port' ),
				'name'     => 'sea_port',
				'type'     => 'dropdown',
				'elements' => ArrayHelper::map(
					Places::findAll( [
						'type' => 'sea'
					] ),
					'id',
					'name'
				)

            ]
        ]
    ]
];

$form = ActiveForm::begin( [
	'options' => [
		'class'   => 'form-horizontal form-label-left',
		'enctype' => 'multipart/form-data'
	]
] ); ?>

    <!-- title section -->
	<div class="page-title">

		<div class="title_left">

			<h3>
				<?= $this->title ?>
			</h3>

		</div>

	</div>
    <!-- title section -->

	<div class="clearfix"></div>

    <!-- logistic settings -->
	<div class="row" id="settings-tab-page-panel">

		<div class="col-md-12 col-xs-12">

			<div class="x_panel">

				<div class="x_content">

                    <!-- flash messages -->
					<?= Alert::widget() ?>
                    <!-- flash messages -->

					<div class="row">

						<div class="clearfix"></div>

						<div role="tabpanel" data-id="settings-tabs" id="custom-settings">

							<ul id="settings-tab" class="nav nav-tabs bar_tabs" role="tablist">

								<li class="active">

                                    <a href="#packages_tab" role="tab" id="packages-tab" data-toggle="tab" aria-expanded="false">
										<?= __(  'Packages' ) ?>
                                    </a>

                                </li>

                                <li>

                                    <a href="#terms_tab" role="tab" id="terms-tab" data-toggle="tab" aria-expanded="false">
										<?= __(  'Terms' ) ?>
                                    </a>

                                </li>

                                <li>

                                    <a href="#import_settings_tab" role="tab" id="import_settings-tab" data-toggle="tab" aria-expanded="false">
										<?= __(  'Import Settings' ) ?>
                                    </a>

                                </li>

							</ul>

							<div id="settings-container" class="tab-content">

                                <?php foreach ( $settings as $setting_key => $setting ) { ?>

                                    <div role="tabpanel" class="tab-pane fade <?= $setting_key == 'packages'
                                        ? 'active in'
                                        : ''
                                    ?>" id="<?= $setting_key ?>_tab" aria-labelledby="<?= $setting_key ?>-tab">

                                        <?php if ( $setting_key == 'import_settings' ) {

                                            foreach ( $setting[ 'items' ] as $key => $value ) { ?>

                                                <div class="col-sm-12 col-md-12 col-xs-12" style="margin-bottom: 8px;">

                                                    <label for="dlang" class="control-label col-md-4 col-sm-4 col-xs-12">
                                                        <?= $value[ 'label' ] ?>
                                                    </label>

                                                    <div class="col-md-8 col-sm-8 col-xs-12">

                                                        <?= Html::dropDownList(
                                                            'SettingsForm[' . $value[ 'name' ] . ']',
                                                            $model->{ $value[ 'name' ] },
															$value[ 'elements' ], [
                                                                'class'    => 'form-control select2_single',
                                                                'id'       => 'import-options',
                                                                'style'    => 'width:100%;',
																'data-msg' => json_encode( [
																	'option'    => $value[ 'label' ],
																	'no_result' => __( 'No result found' )
																] )
                                                            ]
                                                        ) ?>

                                                    </div>

                                                </div>

										    <?php }

                                        } else { ?>

                                            <div class="form-group language-list">

                                                <label for="<?= $setting[ 'type' ] ?>-lang" class="control-label col-md-2 col-sm-2 col-xs-12">
													<?= __(  'Choose language' ) ?>
                                                </label>

                                                <div class="col-md-10 col-sm-10 col-xs-12">

                                                    <select id="<?= $setting[ 'type' ] ?>-lang" class="form-control">

														<?php foreach ( $languages as $id => $lang ) { ?>

                                                            <option value="<?= $id ?>">
																<?= $lang ?>
                                                            </option>

														<?php } ?>

                                                    </select>

                                                </div>

                                            </div>

                                            <div class="add-new-option" data-type="<?= $setting[ 'type' ] ?>">

                                                <a href="javascript:void(0)">
                                                    <i>+</i>
                                                </a>

                                            </div>

											<?php foreach ( $languages as $id => $lang ) { ?>

                                                <div class="setting-option-container <?= $setting[ 'type' ] ?>-container" id="<?= $setting[ 'type' ] ?>-container-<?= $id ?>" data-lang="<?= $id ?>">

													<?php if ( isset( $model->{$setting_key}[ $id ] ) ) {

														foreach ( $model->{$setting_key}[ $id ] as $key => $value ) {

															echo $this->render( 'settings-section/setting', [
																'model' => [
																	'index'   => $key,
																	'lang_id' => $id,
																	'label'   => $setting[ 'label' ],
																	'key'     => $setting_key,
																	'value'   => $value
																]
															] );

														}

													} ?>

                                                    <script type="text/html" id="<?= $setting[ 'type' ] ?>-container-template-<?= $id ?>">

														<?= $this->render( 'settings-section/setting', [
															'model' => [
																'index'   => 0,
																'lang_id' => $id,
																'label'   => $setting[ 'label' ],
																'key'     => $setting_key
															]

														] ); ?>

                                                    </script>

                                                </div>

											<?php }

                                        } ?>

                                    </div>

                                <?php } ?>

							</div>

						</div>

						<div class="settings-save">

							<?= Html::submitButton( __(  'Save' ), [
								'class' => 'btn btn-primary'
							] ) ?>

						</div>

					</div>

				</div>

			</div>

		</div>

	</div>
    <!-- logistic settings -->

<?php ActiveForm::end() ?>