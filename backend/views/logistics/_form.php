<?php /***************************************************************
 *      	 	   	    FORM SECTION FOR LOGISTICS                   *
 *********************************************************************/

use yii\helpers\ArrayHelper;
use yii\helpers\Url;

use yii\widgets\ActiveForm;

use backend\widgets\GoogleMaps;

/* @var $this yii\web\View                     */
/* @var $model common\models\Logistics         */
/* @var $containers common\models\Taxonomies[] */
/* @var $places common\models\Places[]         */

//main logistics form
$form = ActiveForm::begin( [
    'options'                => [
        'class'                 => 'form-horizontal form-label-left ajax-submitting-form need-image need-calculation',
        'id'                    => 'logistics-form',
        'data-func'             => 'formCallback',
        'data-redirectonfinish' => Url::toRoute( [
            '/pages'
        ] )
    ],
    'enableAjaxValidation'   => false,
    'enableClientValidation' => true
] ); ?>

    <!-- form section -->
    <div class="x_panel">

        <div class="x_content">

            <div class="col-sm-12 col-md-12 col-xs-12" style="display: none">

                <?= $form
                    -> field( $model, 'type' )
                    -> hiddenInput( [
                        'id'       => 'logistic-type',
                        'readonly' => true
                    ] )
                    -> label( false );
                ?>

            </div>

			<?= $this->render( 'form-sections/' . str_replace(
			    [ ' ', '-', '_', '.', ',' ], '', strtolower( $model->type )
            ), [
                'form'       => $form,
				'model'      => $model,
                'containers' => $containers
			] ); ?>

            <div class="form-group">

                <div class="col-md-12 col-md-12 col-xs-12 text-center">

                    <input class="btn btn-primary" data-value="test" type="submit" value="<?= ( $model->isNewRecord
                        ? __(  'Publish' )
                        : __(  'Update' ) )
                    ?>">

                </div>

            </div>

        </div>

    </div>
    <!-- form section -->

    <!-- routes section -->
    <div class="x_panel no-top-border">

        <div class="x_title" style="padding: 10px;">

            <h2>
                <?= __(  'Route' ) ?>
            </h2>

            <ul class="nav navbar-right panel_toolbox slides" style="min-width: 20px;">

                <li>

                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>

                </li>

                <li class="dropdown">

                    <a href="#" class="dropdown-toggle" id="add-place" data-toggle="dropdown">
                        +
                    </a>

                </li>

            </ul>

            <div class="clearfix"></div>

        </div>

        <div class="x_content" style="padding: 0px;">

            <div class="places-container" data-details="<?= htmlentities( json_encode( $places ) ) ?>">

                <?php if ( ! empty( $model->location_ids ) && ! empty( $places )
                    && ( is_array( $model->location_ids ) || ! empty( json_decode( $model->location_ids ) ) )
                ) {

                    foreach ( $model->location_ids as $place ) { ?>

                        <?= $this->render( 'form-sections/place', [
                            'model' => [
                                'label' => __( 'Point' ),
                                'value' => $place
                            ],
                            'places' => ArrayHelper::map(
								$places,
                                'id',
                                'name'
                            )
                        ] ); ?>

                    <?php }

                } ?>

            </div>

            <a href="javascript:void(0)" id="show-on-map">
                <?= __( 'Show on map' ) ?>
            </a>

            <div class="general-map">

				<?= GoogleMaps::widget( [
					'view'   => $this,
					'hideUI' => true,
					'type'   => 'direction'
				] ); ?>

            </div>

        </div>

    </div>
    <!-- routes section -->

<?php ActiveForm::end(); ?>

<script type="text/html" id="places-template">

	<?= $this->render( 'form-sections/place', [
		'model' => [
			'label' => __( 'Point' ),
			'key'   => 0,
			'value' => ''
		],
		'places' => ! empty( $places )
			? ArrayHelper::map(
				$places,
				'id',
				'name'
			)
			: []
	] ); ?>

</script>
