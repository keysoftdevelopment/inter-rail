<?php /***************************************************************
 *	  	   	     THE LIST OF ALL AVAILABLE PAGES PAGE                *
 *********************************************************************/

use yii\helpers\Url;
use yii\widgets\LinkPager;

/* @var $this yii\web\View                         */
/* @var $searchModel common\models\LogisticsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider  */
/* @var $type string                               */
/* @var $taxonomies array                          */

// current page title, that will displayed in head tags
$this->title = __( ucfirst( $type ) ); ?>

    <div class="page-title">

        <!-- title section -->
        <div class="title_left">

            <h3>
                <?= $this->title ?>
            </h3>

            <?php if ( in_array ( $type, [ 'auto', 'sea', 'railway' ] ) ) { ?>

                <a href="<?= Url::to( [
					'logistics/create',
					'type' => $type
				] ) ?>">
                    <i class="fa fa-plus-circle"></i>
					<?= __( 'Add New' ) ?>

                </a>

            <?php } ?>

        </div>
        <!-- title section -->

        <!-- search section -->
        <div class="title_right">

            <?php if ( ! in_array( $type, [ 'blockTrain', 'train' ] ) ) { ?>

                <div class="col-xs-12 form-group xslx-details <?= $type == 'sea' ? ' col-md-6 col-sm-6 sea-import-details' : ' text-right col-md-12 col-sm-12 import-details ' . $type ?> ">

                    <ul>

                        <li>

                            <a href="javascript:void(0)" class="import-xlsx-file" data-details="<?= htmlentities( json_encode( [
								'url' => '/logistics/import?type=' . $type
							] )) ?>">

                                <i class="fa fa-file-text"></i>
								<?= __( 'Import From Excel' ) ?>

                            </a>

                        </li>

                        <li>
                            /
                        </li>

                        <li>

                            <a href="<?= Url::to( [
                                '/logistics/export',
                                'type'                            => $type,
                                'LogisticsSearch[from]'           => $searchModel->from,
								'LogisticsSearch[to]'             => $searchModel->to,
                                'LogisticsSearch[status]'         => $searchModel->status,
                                'LogisticsSearch[transportation]' => $searchModel->transportation,
                                'LogisticsSearch[c_type]'         => $searchModel->c_type,
                                'containers_type'                 => Yii::$app->getRequest()->getQueryParam( 'containers_type' )
                            ] ) ?>" target="_blank">

                                <i class="fa fa-file-text"></i>
								<?= __( 'Export to Excel' ) ?>

                            </a>

                        </li>

                    </ul>

                </div>

            <?php }

            if ( in_array( $type, [ 'train', 'blockTrain', 'sea' ] ) ) {

			    echo $this->render( '_search', [
				    'model' => $searchModel,
                    'type'  => $type
			    ] );

            } ?>

        </div>
        <!-- search section -->

    </div>

    <div class="clearfix"></div>

    <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12">

            <!-- filters section -->
            <div class="main-filters-container">

				<?= $this->render( 'filters', [
					'searchModel' => $searchModel,
					'type'        => $type
				] ) ?>

            </div>
            <!-- filters section -->

            <div class="x_panel">

                <!-- logistic lists -->
                <div class="x_content">

                    <?= $this->render('types/' . str_replace( [ ' ', '-', '_', '.', ',' ], '', strtolower( $type ) ), [
                        'models'     => $dataProvider->models,
                        'taxonomies' => in_array ( $type, [ 'auto', 'sea', 'railway' ] )
                            ? $taxonomies
                            : []
                    ] ) ?>

                </div>
                <!-- logistic lists -->

                <!-- pagination section -->
                <div class="pagination-container">

                    <?= LinkPager::widget( [
                        'pagination' => $dataProvider->pagination,
                    ] ); ?>

                </div>
                <!-- pagination section -->

            </div>

        </div>

    </div>