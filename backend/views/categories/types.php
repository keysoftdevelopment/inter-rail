<?php /***************************************************************
 *                                                                   *
 *	 	   	    THE LIST OF ALL AVAILABLE CATEGORIES                 *
 *            I.E used for logistics and containers only             *
 *                                                                   *
 *********************************************************************/

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

use common\models\Languages;

use common\widgets\Alert;

/* @var $this yii\web\View                           */
/* @var $searchModel common\models\TaxonomiesSearch  */
/* @var $dataProvider yii\data\ActiveDataProvider    */
/* @var $model common\models\Taxonomies              */
/* @var $lang_id int                                 */

// current page title, that will displayed in head tags
$this->title = __(  $searchModel->type == 'weights'
    ? ucfirst( $searchModel->type )
    : ucfirst( $searchModel->type ) . ' Types' );
?>

    <!-- taxonomy title section -->
    <div class="page-title">

        <div class="title_left">

            <h3>
                <?= $this->title ?>
            </h3>

        </div>

        <?php if ( ! in_array( $searchModel->type, [ 'weights', 'containers' ] ) ) { ?>

            <div class="title_right">

                <div class="pull-right posts_language text-right">

                    <span>
                        <?= __(  'Choose language' ) ?>:
                    </span>

                    <?php foreach ( Languages::find()->all() as $lang ) { ?>

                        <?= Html::a( $lang->description, [
                            '/categories/types',
                            'type'    => $searchModel->type,
                            'lang_id' => $lang->id
                        ], [
                            'class' => $lang_id == $lang->id || $model->lang_id == $lang->id
                                ? 'btn btn-primary btn-xs'
                                : 'btn btn-default btn-xs'
                        ] ) ?>

                    <?php } ?>

                </div>

            </div>

        <?php } ?>

    </div>
    <!-- taxonomy title section -->

    <div class="clearfix"></div>

    <!-- taxonomy details section -->
    <div class="row">

        <div class="col-md-4 col-sm-4 col-xs-12">

            <div class="x_panel">

                <div class="x_title">

                    <h2>

                        <?= $searchModel->type == 'weights'
                            ? __(  'Add weight' )
                            : __(  'Add type' )
                        ?>

                    </h2>

                    <div class="clearfix"></div>

                </div>

                <!-- taxonomy form section -->
                <div class="x_content">

                    <?php $form = ActiveForm::begin( [
                        'options' => [
                            'class'                 => 'form-horizontal form-label-left ajax-submitting-form',
                            'data-redirectonfinish' => Url::toRoute( [
                                '/categories/types'
                            ] )
                        ]
                    ] ); ?>

                        <?= $form
                            -> field( $model, 'type', [
                                'template' => '{input}',
                                'options'  => [
                                    'tag' => false, // Don't wrap with "form-group" div
                                ]
                            ] )
                            -> hiddenInput( [
                                'value' => $searchModel->type
                            ] )
                            -> label( false );
                        ?>

                        <?= $form->field( $model, 'name' )
                            -> textInput( [
                                'class' => 'form-control'
                            ] )
                            -> label( $searchModel->type == 'weights'
                                ? __(  ucfirst( $searchModel->type ) . ' Name' ) . " *"
                                : __(  ucfirst( $searchModel->type ) . ' Type' ) . " *"
                            );
                        ?>

                        <?php if ( $searchModel->type == 'weights' ) {

                            echo $form
                                -> field( $model, 'description' )
                                -> textarea( [
                                    'id'          => 'description',
                                    'placeholder' => __(  'Description' )
                                ] )
                                -> label( false );

                        } ?>

                        <div class="col-sm-12 col-md-12 col-xs-12 text-center">

                            <?= Html::submitButton( $searchModel->type == 'weights'
                                ? __(  'Add weight' )
                                : __(  'Add type' ), [
                                'class' => 'btn btn-primary'
                            ] ); ?>

                        </div>

                    <?php ActiveForm::end(); ?>

                </div>
                <!-- taxonomy form section -->

            </div>

        </div>

        <div class="col-md-8 col-sm-8 col-xs-12">

            <div class="x_panel">

                <div class="x_title">

                    <h2>
                        <?= $this->title ?>
                    </h2>

                    <div class="clearfix"></div>

                </div>

                <div class="x_content">

                    <?= Alert::widget() ?>

                    <?= Html::input(
                        'hidden',
                        'type',
                        $searchModel->type
                    ) ?>

                    <?= Html::input(
                        'hidden',
                        'lang_id',
                        $lang_id
                    ) ?>

                    <!-- taxonomy lists -->
                    <table class="table table-striped responsive-utilities jambo_table" id="categories-table">

                        <thead>

                            <tr>

                                <th width="1%">
                                    #
                                </th>

                                <th>

                                    <?= $searchModel->type == 'weights'
                                        ?  __(  'Weight' )
                                        : __(  'Type Name' )
                                    ?>

                                </th>

                                <?php if ( $searchModel->type == 'weights' ) { ?>

                                    <th>
                                        <?= __(  'Description' )?>
                                    </th>

                                <?php } ?>

                                <th style="width: 1%"></th>

                            </tr>

                        </thead>

                        <tbody>

                            <?php foreach ( $dataProvider->models as $key => $model ) { ?>

                                <tr class="even pointer">

                                    <td class="vertical-middle">
                                        <?= ( $key + 1 ) ?>
                                    </td>

                                    <td class="vertical-middle">

                                        <?= Html::input(
                                            'hidden',
                                            'type_id',
                                            $model->id
                                        ) ?>

                                        <span class="item-name">
                                            <?= $model->name ?>
                                        </span>

                                        <div class="update-type-name" style="display: none">
                                            <input type="text" class="form-control" name="type_name" value="<?= $model->name ?>" />
                                        </div>

                                    </td>

                                    <?php if ( $searchModel->type == 'weights' ) { ?>

                                        <td class="vertical-middle">

                                            <span class="description">
                                                <?= $model->description ?>
                                            </span>

                                            <div class="update-description" style="display: none">
                                                <input type="text" class="form-control description-field" name="description" value="<?= $model->description ?>" />
                                            </div>

                                        </td>

                                    <?php } ?>

                                    <td class="model-actions">

                                        <div class="actions">

                                            <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                <i class="fa fa-ellipsis-v"></i>
                                            </a>

                                            <ul class="dropdown-menu pull-right">

                                                <li>

                                                    <?= Html::a( '<i class="fa fa-pencil"></i> ' .  __(  'Edit' ), 'javascript:void(0)', [
                                                        'class' => 'update-current-type'
                                                    ] ); ?>

                                                </li>

                                                <li>

                                                    <?= Html::a( '<i class="fa fa-trash"> </i> '. __(  'Delete' ), [
                                                        Url::to( 'categories/delete' ),
                                                        'id'   => $model->id,
                                                        'type' => $searchModel->type
                                                    ], [
                                                        'data'  => [
                                                            'confirm' => __(  'Do you really want to delete this type?' ),
                                                            'method'  => 'post',
                                                        ]
                                                    ] ); ?>

                                                </li>

                                            </ul>

                                        </div>

                                        <a href="javascript:void(0)" class="save-type">

                                            <?= $searchModel->type == 'weights'
                                                ? __( 'Update Weight' )
                                                : __( 'Update Type' )
                                            ?>

                                        </a>

                                    </td>

                                </tr>

                            <?php } ?>

                        </tbody>

                    </table>
                    <!-- taxonomy lists -->

                </div>

            </div>

        </div>

        <div class="clearfix"></div>

    </div>
    <!-- taxonomy details section -->