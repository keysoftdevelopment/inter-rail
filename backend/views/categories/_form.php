<?php /***************************************************************
 *		 	   	  GENERAL FORM SECTION FOR CATEGORIES                 *
 *********************************************************************/

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

use backend\widgets\MediaPopup;

/* @var $this yii\web\View              */
/* @var $model common\models\Taxonomies */
/* @var $lang_id integer                */
/* @var $type string                    */

$form = ActiveForm::begin( [
	'options' => [
		'class' => 'form-horizontal form-label-left ajax-submitting-form need-image'
	],
	'enableAjaxValidation'   => false,
	'enableClientValidation' => false
] );

    if ( $model->isNewRecord )
        $form->action = Url::toRoute( [
            '/categories/create'
        ] );
    ?>

    <?= $form
        -> field( $model, 'lang_id', [
            'template' => '{input}',
            'options'  => [
                'tag' => false, // Don't wrap with "form-group" div
            ]
        ] )
        -> hiddenInput( [
            'value' => $lang_id == 0
                ? $model->lang_id
                : $lang_id
        ] )
        -> label( false );
    ?>

    <?= $form
        -> field( $model, 'type', [
            'template' => '{input}',
            'options'  => [
                'tag' => false, // Don't wrap with "form-group" div
            ]
        ] )
        -> hiddenInput( [
            'value' => $type
        ] )
        -> label( false );
    ?>

    <?= $this->render( 'form-sections/' . $type, [
            'form'    => $form,
            'model'   => $model,
            'lang_id' => $lang_id
        ]
    ) ?>

    <div class="col-sm-12 col-md-12 col-xs-12 text-center">

        <?= Html::submitButton( $model->isNewRecord
            ? __(  'Add Category' )
            : __(  'Update Category' ), [
                'class' => $model->isNewRecord ? 'btn btn-primary' : 'btn btn-primary'
            ]
        ); ?>

    </div>

<?php ActiveForm::end();

echo MediaPopup::widget( [
	'formType'   => 'taxonomy-modal',
	'uploadForm' => $this->render( 'upload' )
] ); ?>