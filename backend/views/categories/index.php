<?php /***************************************************************
 *	  	   	  THE LIST OF ALL AVAILABLE CATEGORIES PAGE              *
 *********************************************************************/

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

use common\models\Languages;
use common\models\Taxonomies;

use common\widgets\Alert;

/* @var $this yii\web\View                                */
/* @var $searchModel common\models\TaxonomiesSearch       */
/* @var $dataProvider yii\data\ActiveDataProvider         */
/* @var $is_models_translated common\models\Translation[] */

// current page title, that will displayed in head tags
$this->title = __(  'Categories' );

// the list of all available languages
$languages = Languages::find()
	-> asArray()
    -> all(); ?>

    <!-- title section -->
    <div class="page-title">

        <div class="title_left">

            <h3>
                <?= __(  'Categories' ) ?>
            </h3>

        </div>

    </div>
    <!-- title section -->

    <div class="clearfix"></div>

    <!-- taxonomy details section -->
    <div class="row taxonomy-page">

        <div class="col-md-4 col-sm-4 col-xs-12">

            <div class="x_panel">

                <div class="x_title">

                    <h2>
                        <?= __(  'Add Category' ) ?>
                    </h2>

                    <div class="clearfix"></div>

                </div>

                <!-- form section -->
                <div class="x_content taxonomy-create">

                    <?= $this->render( '_form', [
                        'model' => new Taxonomies(),
                        'type'  => $searchModel->type
                    ] ); ?>

                </div>
                <!-- form section -->

            </div>

        </div>

        <div class="col-md-8 col-sm-8 col-xs-12">

            <div class="x_panel">

                <div class="x_title">

                    <h2>
                        <?= __(  'Categories' ) ?>
                    </h2>

                    <div class="clearfix"></div>

                </div>

                <div class="x_content">

                    <?= Alert::widget() ?>

                    <?php ActiveForm::begin( [
                        'action' => [
                            'sorter'
                        ],
                        'method' => 'post',
                        'options' => [
                            'style' => 'float: right; width: 100%;'
                        ]
                    ] ); ?>

                        <?= Html::input(
                            'hidden',
                            'type',
                            $searchModel->type
                        ) ?>

                        <!-- taxonomy lists -->
                        <table class="table table-striped responsive-utilities jambo_table" id="categories-table">

                            <thead>

                                <tr>

                                    <th>
                                        #
                                    </th>

                                    <th>
                                        <?= __(  'Category Name' ) ?>
                                    </th>

                                    <?php foreach ( $languages as $language ) { ?>

                                        <th style="width: 1%;">
                                            <?= $language[ 'description' ] ?>
                                        </th>

                                    <?php } ?>

                                    <?php if ( $searchModel->type == 'posts' ) { ?>

                                        <th>
                                            <?= __(  'Parent' ) ?>
                                        </th>

                                    <?php } ?>

                                    <th style="width: 1%"></th>

                                </tr>

                            </thead>

                            <tbody>

                                <?php foreach ( $dataProvider->models as $model ) :

                                    if ( $searchModel->type == 'posts' ) {

                                        $parent_cat_id = array_search(
                                            $model->parent, ArrayHelper::getColumn(
                                                $dataProvider->models, 'id'
                                            )
                                        );

                                    } ?>

                                    <tr class="even pointer">

                                        <td class="handle">

                                            <?= Html::input('hidden', 'categories[]', $model->id ) ?>

                                            <div class="ui-sortable-handle">
                                                <i class="fa fa-bars"></i>
                                            </div>

                                        </td>

                                        <td>

                                            <?= $searchModel->type != 'posts'
                                                ? $model->icon
                                                : ''
                                            ?>

                                            <?= Html::a( $model->name, [
                                                'update',
                                                'id' => $model->id
                                            ] ) ?>

                                        </td>

                                        <?php foreach ( $languages as $language ) {

                                            if ( $language[ 'id' ] == $model->lang_id ) { ?>

                                                <td style="color: #00b100">
                                                    &#10003;
                                                </td>

                                            <?php } else {

                                                $foreign_ids = ArrayHelper::getColumn(
                                                    $is_models_translated,
                                                    'foreign_id'
                                                );

                                                $lang_ids = ArrayHelper::getColumn(
                                                    $is_models_translated,
                                                    'lang_id'
                                                );

                                                if ( in_array( (string)$model->id, $foreign_ids )
                                                    && in_array( (string)$language[ 'id' ], $lang_ids )
                                                ) { ?>

                                                    <td style="color: #00b100">
                                                        &#10003;
                                                    </td>

                                                <?php } else { ?>

                                                    <td>

                                                        <?= Html::a( '&plus;', [
                                                            'update',
                                                            'id'      => $model->id,
                                                            'lang_id' => $language[ 'id' ]
                                                        ], [
                                                            'class' => 'btn btn-info btn-xs',
                                                            'role'  => 'button'
                                                        ] ) ?>

                                                    </td>

                                                <?php }
                                            }

                                        } ?>

                                        <?php if ( $searchModel->type == 'posts' ) { ?>

                                            <td>

                                                <?= isset( $parent_cat_id )
                                                    ? $dataProvider->models[ $parent_cat_id ]->name
                                                    : __(  'Main' )
                                                ?>

                                            </td>

                                        <?php } ?>

                                        <td class="text-center model-actions">

                                            <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                <i class="fa fa-ellipsis-v"></i>
                                            </a>

                                            <ul class="dropdown-menu pull-right">

                                                <li>

                                                    <?= Html::a( '<i class="fa fa-pencil"></i> ' .  __(  'Edit' ),[
                                                        Url::to( 'categories/update' ),
                                                        'id' => $model->id
                                                    ], [
                                                        'class' => 'update-current-type'
                                                    ] ); ?>

                                                </li>

                                                <li>

                                                    <?= Html::a( '<i class="fa fa-trash"> </i> '. __(  'Delete' ), [
                                                        Url::to( 'categories/delete' ),
                                                        'id'   => $model->id,
                                                        'type' => $searchModel->type
                                                    ], [
                                                        'data'  => [
                                                            'confirm' => __(  'Do you really want to delete this type?' ),
                                                            'method'  => 'post',
                                                        ]
                                                    ] ); ?>

                                                </li>

                                            </ul>

                                        </td>

                                    </tr>

                                <?php endforeach; ?>

                            </tbody>

                        </table>
                        <!-- taxonomy lists -->

                        <div class="bottom-save-sorting-cats">

                            <?= Html::submitButton( __(  'save sorting' ), [
                                'class' => 'btn btn-default saving-sorting'
                            ] ) ?>

                        </div>

                    <?php ActiveForm::end(); ?>

                </div>

            </div>

        </div>

        <div class="clearfix"></div>

    </div>
    <!-- taxonomy details section -->