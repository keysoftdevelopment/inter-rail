<?php /***********************************************************
 *		 	   	        RAILWAY TYPE FORM SECTION                *
 *****************************************************************/

use common\models\Taxonomies;

/* @var $form yii\widgets\ActiveForm    */
/* @var $model common\models\Taxonomies */
/* @var $lang_id integer                */ ?>

	<?= $form
		-> field( $model, 'name', [
			'template' => '<div class="col-sm-12 col-md-12 col-xs-12">
            	<div class="form-group field-name">{label}{input}{hint}{error}</div>
            </div>',
			'options'  => [
				'tag' => false, // Don't wrap with "form-group" div
			]
		] )
		-> textInput( [
			'class'          => 'form-control',
			'placeholder'    => __(  'Railway' )
		] )
		-> label( __(  'Railway' ) )
	?>

	<?php if ( (int)$lang_id > 0
		&& $model->lang_id == $lang_id
		|| (int)$lang_id == 0
	) {

	    //selected weights
        $selected_weights = ! empty( $model->weights )
            ? json_decode( stripslashes( $model->weights ) )
            : $model->weights; ?>

		<div class="col-sm-12 col-md-12 col-xs-12 wagon-weights">

            <label class="col-sm-12 col-md-12 col-xs-12" for="taxonomy-weights">
                <?= __( 'Weights' ) ?>
            </label>

            <div class="weights-list">

                <?php foreach ( Taxonomies::findAll( [
                    'type' 	    => 'weights',
                    'isDeleted' => false
                ] ) as $key => $value ) { ?>

                    <div class="col-sm-4 col-md-4 col-xs-6">

                        <label class="checkbox">

                            <?= $value->name ?> <?= __( 'tons' ) ?>

                            <input name="Taxonomies[weights][]" <?= is_array( $selected_weights ) && in_array( $value->id, $selected_weights )
                                ? 'checked'
                                : ''
                            ?> type="checkbox" value="<?= $value->id ?>">

                            <span class="checkmark"></span>

                        </label>

                    </div>

                <?php } ?>

            </div>

		</div>

 	<?php } ?>

	<div class="col-sm-12 col-md-12 col-xs-12">

		<?= $form
			-> field( $model, 'description' )
			-> textarea( [
				'id'          => 'description',
				'placeholder' => __(  'Description' )
			] )
			-> label( __(  'Description' ) );
		?>

	</div>
