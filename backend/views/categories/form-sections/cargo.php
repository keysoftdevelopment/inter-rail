<?php /***********************************************************
 *		 	   	          CARGO FORM SECTION                     *
 *****************************************************************/

use common\models\Taxonomies;

/* @var $form yii\widgets\ActiveForm    */
/* @var $model common\models\Taxonomies */
/* @var $lang_id integer                */ ?>

	<div class="col-sm-12 col-md-12 col-xs-12">

		<?php if ( (int)$lang_id > 0
			&& $model->lang_id == $lang_id
			|| (int)$lang_id == 0
		) { ?>

			<?= $form
				-> field( $model, 'icon', [
					'template' => ! $model->isNewRecord
						? '<div class="col-sm-6 col-md-6">
								   <div class="form-group field-name">{input}{hint}{error}</div>
							   </div>'
						: '<div class="form-group field-name">{label}{input}{hint}{error}</div>',
					'options'  => [
						'tag' => false, // Don't wrap with "form-group" div
					]
				] )
				-> textInput( [
					'class'          => 'form-control',
					'data-emojiable' => 'true',
					'placeholder'    => __(  'Category Icon' ) . " *"
				] )
				-> label( __(  'Category Icon' ) )
			?>

		<?php } ?>

		<?= $form
			-> field( $model, 'name', [
				'template' => ! $model->isNewRecord
					? (int)$lang_id > 0
					&& $model->lang_id == $lang_id
					|| (int)$lang_id == 0
						? '<div class="col-sm-6 col-md-6 col-xs-12">
									<div class="form-group field-name">{input}{hint}{error}</div>
							  </div>'
						: '<div class="col-sm-12 col-md-12 col-xs-12">
									<div class="form-group field-name">{input}{hint}{error}</div>
							  </div>'
					: '<div class="form-group field-name">{label}{input}{hint}{error}</div>',
				'options'  => [
					'tag' => false, // Don't wrap with "form-group" div
				]
			] )
			-> textInput( [
				'id'          => 'name',
				'placeholder' => __(  'Name' )
			] )
			-> label( __(  'Name' ) );
		?>

	</div>

	<div class="col-sm-12 col-md-12 col-xs-12 cargo-container-types">

        <label class="col-sm-12 col-md-12 col-xs-12" for="taxonomies-containers">
			<?= __( 'Container Types' ) ?>
        </label>

        <div class="containers-list">

            <?php foreach ( Taxonomies::findAll( [
                'type'      => 'containers',
                'isDeleted' => false
            ] ) as $container ) { ?>

                <div class="col-sm-4 col-md-4 col-xs-6">

                    <label class="checkbox">

                        <?= $container->name ?>

                        <input name="container_types[]" <?= ! empty( $model->containers ) && in_array( $container->name, explode( ',', $model->containers ) )
                            ? 'checked'
                            : ''
                        ?> type="checkbox" value="<?= $container->name ?>">

                        <span class="checkmark"></span>

                    </label>

                </div>

            <?php } ?>

        </div>

	</div>

	<div class="col-sm-12 col-md-12 col-xs-12 cargo-wagon-types">

        <label class="col-sm-12 col-md-12 col-xs-12" for="taxonomies-wagons">
			<?= __( 'Wagons' ) ?>
        </label>

        <div class="wagons-list">

			<?php foreach ( Taxonomies::findAll( [
				'type'      => 'railway',
				'isDeleted' => false
			] ) as $wagon ) { ?>

                <div class="<?= $model->isNewRecord ? 'col-sm-6 col-md-6' : 'col-sm-4 col-md-4' ?> col-xs-6">

                    <label class="checkbox">

						<?= $wagon->name ?>

                        <input name="wagons[]" <?= ! empty( $model->wagons ) && in_array( $wagon->name, explode( ',', $model->wagons ) )
							? 'checked'
							: ''
						?> type="checkbox" value="<?= $wagon->name ?>">

                        <span class="checkmark"></span>

                    </label>

                </div>

			<?php } ?>

        </div>

	</div>

    <div class="col-sm-12 col-md-12 col-xs-12">

        <?= $form
            -> field( $model, 'description' )
            -> textarea( [
                'id'          => 'description',
                'placeholder' => __(  'Description' )
            ] )
            -> label( false );
        ?>

    </div>