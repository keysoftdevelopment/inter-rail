<?php /***********************************************************
 *		 	   	        CATEGORY FORM SECTION                    *
 *****************************************************************/

/* @var $form yii\widgets\ActiveForm    */
/* @var $model common\models\Taxonomies */
/* @var $lang_id integer                */ ?>

	<?= $form
		-> field( $model, 'file_id', [
			'template' => '{input}',
			'options'  => [
				'tag' => false, // Don't wrap with "form-group" div
			]
		] )
		-> hiddenInput( [
			'id' => 'file_id'
		] )
		-> label( false );
	?>

	<?php if ( $model->file_id === null
        && $model->isNewRecord
    ) { ?>

		<div class="form-group text-center">
			<img src="<?= Yii::getAlias( '@web' ) . '/images/image-not-found.png' ?>" id="thumbnail" />
		</div>

	<?php } ?>

    <div class="col-sm-12 col-md-12 col-xs-12">

		<?= $form
			-> field( $model, 'name', [
				'template' => ! $model->isNewRecord
                    ? (int)$lang_id > 0
						&& $model->lang_id == $lang_id
						|| (int)$lang_id == 0
                        ? '<div class="col-sm-6 col-md-6 col-xs-12">
                                <div class="form-group field-name">{input}{hint}{error}</div>
                          </div>'
                        : '<div class="col-sm-12 col-md-12 col-xs-12">
                                <div class="form-group field-name">{input}{hint}{error}</div>
                          </div>'
					: '<div class="form-group field-name">{label}{input}{hint}{error}</div>',
				'options'  => [
					'tag' => false, // Don't wrap with "form-group" div
				]
            ] )
			-> textInput( [
				'id'          => 'name',
				'placeholder' => __(  'Name' )
			] )
			-> label( __(  'Name' ) );
		?>

		<?php if ( (int)$lang_id > 0
            && $model->lang_id == $lang_id
            || (int)$lang_id == 0
		) { ?>

            <?= $form
                -> field( $model, 'parent', [
                    'template' => ! $model->isNewRecord
                        ? '<div class="col-sm-6 col-md-6">
                               <div class="form-group field-name">{input}{hint}{error}</div>
                           </div>'
                        : '<div class="form-group field-name">{label}{input}{hint}{error}</div>',
                    'options'  => [
                        'tag' => false, // Don't wrap with "form-group" div
                    ]
                ] )
                -> dropDownList( $model->parentTaxonomies(), [
                    'class' => 'form-control',
                    'prompt' => 'None'
                ] )
                -> label( __(  'Parent' ) )
            ?>

		<?php } ?>

	</div>

	<div class="col-sm-12 col-md-12 col-xs-12">

		<?= $form
			-> field( $model, 'description' )
			-> textarea( [
				'id'          => 'description',
				'placeholder' => __(  'Description' )
			] )
			-> label( false );
		?>

	</div>

	<?php if (  (int)$lang_id > 0
		&& $model->lang_id == $lang_id
		|| (int)$lang_id == 0
	)  { ?>

		<div class="form-group">

			<a href="#taxonomy-modal" data-toggle="modal" data-target="#taxonomy-modal" id="taxonomies-choose-image-btn">
				<?= __(  'Select Image' ) ?>
			</a>

		</div>

	<?php } ?>