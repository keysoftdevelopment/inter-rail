<?php /***************************************************************
 *			 	   	  CREATE CATEGORY FOR PRODUCTS                   *
 *********************************************************************/

use yii\helpers\Html;

/* @var $this yii\web\View              */
/* @var $model common\models\Taxonomies */

// current page title, that will displayed in head tags
$this->title = __(  'Create Category' ); ?>

<div class="categories-create">

    <h1>
        <?= Html::encode( $this->title ) ?>
    </h1>

    <?= $this->render( '_form', [
        'model' => $model,
    ] ); ?>

</div>
