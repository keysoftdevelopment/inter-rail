<?php /***************************************************************
 *			 	   	  UPDATE CURRENT CATEGORY PAGE                   *
 *********************************************************************/

use yii\helpers\Html;

use common\models\Languages;

/* @var $this yii\web\View              */
/* @var $model common\models\Taxonomies */
/* @var $lang_id integer                */

// current page title, that will displayed in head tags
$this->title = __(  'Update Category' ) . ': ' . $model->name; ?>

    <div class="row">

        <!-- title section -->
        <div class="col-md-12 col-sm-12 col-xs-12">

            <div class="page-title">

                <div class="title_left">

                    <h3>
                        <?= $model->name ?>
                    </h3>

                </div>

                <div class="title_right">

                    <div class="pull-right posts_language text-right">

                        <span>
                            <?= __(  'Choose language' ) ?>:
                        </span>

                        <?php foreach ( Languages::find()->all() as $lang ) { ?>

                            <?= Html::a( $lang->description, [
                                '/categories/update',
                                'id'      => $model->id,
                                'lang_id' => $lang->id
                            ], [
								'class' => (int)$lang_id == $lang->id
									? 'btn btn-primary btn-xs'
									: 'btn btn-default btn-xs'
                            ] ) ?>

                        <?php } ?>

                    </div>

                </div>

            </div>

        </div>
        <!-- title section -->

        <!-- taxonomy details section -->
        <?= $model->type == 'posts'
            ? '<div class="col-md-12 col-sm-12 col-xs-12 taxonomy-details"><div class="col-sm-5 col-md-5 col-xs-12">'
            : '<div class="col-md-12 col-sm-12 col-xs-12">'
        ?>

            <div class="x_panel">

                <div class="x_title">

                    <h2>
                        <?= __(  'Update Category' ) ?>
                    </h2>

                    <div class="clearfix"></div>

                </div>

                <!-- form section -->
                <div class="x_content taxonomy-update">

                    <?= $this->render( '_form', [
                        'model'   => $model,
                        'lang_id' => $lang_id,
                        'type'    => $model->type
                    ] ); ?>

                </div>
                <!-- form section -->

            </div>

        </div>

        <?php if ( $model->type == 'posts' ) { ?>

                <div class="col-md-7 col-sm-7 col-xs-12">
                    <img class="category_image" src="<?= Yii::$app->common->thumbnail( $model->file_id ); ?>" id="thumbnail" />
                </div>

            </div>

        <?php } ?>
        <!-- taxonomy details section -->

    </div>