<?php /**********************************************************
 *	     		 	   	  UPDATE ROLE PART                      *
 ****************************************************************/

/* @var $this yii\web\View   */
/* @var $model yii\rbac\Role */
/* @var $permissions array   */

// current page title
$this->title = __(  'Update Role' ); ?>

    <div class="col-md-12 col-sm-12 col-xs-12 edit-container-block">

        <!-- title section -->
        <div class="page-title">

            <div class="title_left">

                <h3>
                    <?= $this->title; ?>
                </h3>

            </div>

        </div>
        <!-- title section -->

        <!-- form section -->
        <div class="col-md-12 col-sm-12 col-xs-12 create-item-form">

            <?= $this->render( '_form', [
                'model'       => $model,
				'permissions' => $permissions,
				'type'        => 'update'
            ] ); ?>

        </div>
        <!-- form section -->

    </div>