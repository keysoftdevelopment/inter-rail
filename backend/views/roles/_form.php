<?php /***************************************************************
 * 	    	 	   	      FORM SECTION FOR ROLE                      *
 *********************************************************************/

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

use dosamigos\tinymce\TinyMce;

/* @var $this yii\web\View   */
/* @var $model yii\rbac\Role */
/* @var $permissions array   */
/* @var $type string         */

//roles main form
$form = ActiveForm::begin( [
    'options'                => [
        'class'                 => 'form-horizontal form-label-left ajax-submitting-form',
        'id'                    => 'roles-form',
        'data-func'             => 'formCallback',
        'data-redirectonfinish' => Url::toRoute( [
            '/containers'
        ] )
    ],
    'enableAjaxValidation'   => false,
    'enableClientValidation' => true
] ); ?>

    <div class="x_panel">

        <div class="x_content">

            <div class="col-sm-12 col-md-12 col-xs-12">

                <div class="form-group">

                    <?= Html::textInput(
                        'name',
                        isset( $model->name )
                            ? $model->name
                            : '', [
                            'class' => 'form-control col-md-12 col-sm-12 col-xs-12 role-name-field',
                            'disabled' => $type == 'create'
                                ? false
                                : true
                        ]
                    ) ?>

                </div>

                <?php if ( $type == 'create' ) { ?>

                    <div class="form-group">

						<?= TinyMce::widget( [
							'name'    => 'description',
							'model'   => $model,
							'options' => [
								'rows' => 15
							],
							'language'      => 'en_GB',
							'clientOptions' => [
								'menubar'   => false,
								'plugins'   => [
									"advlist image autolink lists link charmap print preview anchor",
									"searchreplace visualblocks code fullscreen",
									"insertdatetime media table contextmenu paste"
								],
								'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | visualblocks | code "
							]
						] ) ?>

                    </div>

                <?php } ?>

            </div>

            <div class="col-sm-12 col-md-12 col-xs-12">

                <h4>
                    <?= __( 'Permission Details' ) ?>
                </h4>

                <!-- general permissions -->
                <div class="col-sm-6 col-md-6 col-xs-12">

                    <h5>
                        <?= __( 'General Permissions' ) ?>
                    </h5>

                    <ul>

                        <?php foreach ( $permissions[ 'list' ][ 'general' ] as $key => $value ) { ?>

                            <li>

                                <input name="<?= $value->name ?>" type="checkbox" class="switchery" <?= isset( $permissions[ 'selected' ][ 'general' ][ $value->name ] )
                                    ? 'checked'
                                    : ''
                                ?> /> <?= __( ucfirst( $value->name ) ) ?>

                            </li>

                        <?php } ?>

                    </ul>

                </div>
                <!-- general permissions -->

                <!-- order permissions -->
                <div class="col-sm-6 col-md-6 col-xs-12">

                    <h5>
						<?= __( 'Order Details Permissions' ) ?>
                    </h5>

                    <ul>

						<?php foreach ( $permissions[ 'list' ][ 'order' ] as $key => $value ) {

						    $selected_options = isset( $permissions[ 'selected' ][ 'order' ]->description ) && ! empty( $permissions[ 'selected' ][ 'order' ]->description )
                                ? json_decode( $permissions[ 'selected' ][ 'order' ]->description )
                                : false; ?>

                            <li>

                                <input name="permissions[<?= $value ?>]" type="checkbox" class="switchery" <?= isset( $selected_options->{$value} ) && $selected_options->{$value} == 'on'
									? 'checked'
									: ''
								?> /> <?= __( ucfirst( str_replace( '_', ' ', $value ) ) ); ?>

                            </li>

						<?php } ?>

                    </ul>

                </div>
                <!-- order permissions -->

            </div>

            <div class="col-sm-12 col-md-12 col-xs-12">

                <div class="ln_solid"></div>

                <div class="form-group text-center">

                    <input class="btn btn-primary" data-value="test" type="submit" value="<?= ( $type == 'create'
                        ? __(  'Publish' )
                        : __(  'Update' ) )
                    ?>">

                </div>

            </div>

        </div>

    </div>

<?php ActiveForm::end(); ?>