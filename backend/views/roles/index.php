<?php /***************************************************************
 *	              THE LIST OF ALL AVAILABLE ROLES PAGE               *
 *********************************************************************/

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View      */
/* @var $models yii\rbac\Role[] */
/* @var $permissions array      */

// current page title
$this->title = __(  'Roles' ); ?>

    <div class="page-title">

        <!-- title section -->
        <div class="title_left">

            <h3>
                <?= $this->title ?>
            </h3>

            <a href="<?= Url::to( [
				'roles/create'
			] ) ?>">
                <i class="fa fa-plus-circle"></i>
				<?= __( 'Add New Role' ) ?>

            </a>

        </div>
        <!-- title section -->

    </div>

    <div class="clearfix"></div>

    <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12">

            <div class="x_panel">

                <div class="x_content">

                    <!-- roles lists -->
                    <table class="table table-striped">

                        <thead>

                            <tr>

                                <th style="width: 1%">
                                    #
                                </th>

                                <th>
                                    <?= __(  'Role Name' ) ?>
                                </th>

                                <th>
                                    <?= __(  'Description' ) ?>
                                </th>

                                <th>
                                    <?= __(  'Permissions' ) ?>
                                </th>

                                <th></th>

                            </tr>

                        </thead>

                        <tbody>

                            <?php foreach ( $models as $model ) { ?>

                                <tr>

                                    <td>
                                        #
                                    </td>

                                    <td>

                                        <?= Html::a( $model->name, [
                                            'update',
                                            'name' => $model->name
                                        ], [
                                            'role' => "button"
                                        ] ) ?>

                                    </td>

                                    <td>
                                        <?= $model->description ?>
                                    </td>

                                    <td>

                                        <?= isset( $permissions[ $model->name ] )
                                            ? $permissions[ $model->name ]
                                            : __( 'There is no any permissions yet' )
                                        ?>

                                    </td>

                                    <td class="text-center model-actions">

                                        <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                            <i class="fa fa-ellipsis-v"></i>
                                        </a>

                                        <ul class="dropdown-menu pull-right">

                                            <li>

                                                <?= Html::a( '<i class="fa fa-pencil"></i> ' .  __(  'Edit' ), [
                                                    'update',
                                                    'name' => $model->name
                                                ] ); ?>

                                            </li>

                                        </ul>

                                    </td>

                                </tr>

                            <?php } ?>

                        </tbody>

                    </table>
                    <!-- roles lists -->

                </div>

            </div>

        </div>

    </div>