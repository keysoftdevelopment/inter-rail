<?php /***************************************************************
 *	     		 	   	  CREATE POST PART                           *
 *********************************************************************/

use common\models\Posts;

/* @var $this yii\web\View                    */
/* @var $model \common\models\Posts           */
/* @var $categories \common\models\Taxonomies */

// current page title
$this->title = __(  'Create post' ); ?>

    <div class="col-md-12 col-sm-12 col-xs-12 edit-container-block">

        <!-- title section -->
        <div class="page-title">

            <div class="title_left">

                <h3>
                    <?= $this->title ?>
                </h3>

            </div>

        </div>
        <!-- title section -->

        <!-- form section -->
        <div class="col-md-9 col-sm-12 col-xs-12 post-form">

            <?= $this->render( '_form', [
                'model' => $model
            ] ); ?>

        </div>
        <!-- form section -->

        <!-- sidebar -->
        <div class="col-md-3 col-sm-12 col-xs-12 sidebar">

            <!-- categories lists -->
            <div class="x_panel">

                <div class="x_title post-categories">

                    <h2>
						<?= __(  'Categories' ) ?>
                    </h2>

                    <div class="clearfix"></div>

                </div>

                <div class="x_content taxonomies-sidebar" role="tabpanel" data-id="cats-tabs" style="padding: 5px 0px;">

                    <ul class="taxonomies-list">

						<?php if ( isset( $categories )
							&& !empty( $categories )
						) {

							foreach ( $categories as $category ) { ?>

                                <li>

                                    <label class="checkbox">

										<?= $category[ 'name' ] ?>

                                        <input name="post_categories" type="checkbox" data-id="<?= $category[ 'id' ] ?>" value="<?= $category[ 'name' ] ?>" >

                                        <span class="checkmark"></span>

                                    </label>

                                </li>

							<?php }

						} else { ?>

                            <li>

                                <label>
									<?= __(  'Category is absent' ) ?>
                                </label>

                            </li>

						<?php } ?>

                    </ul>

                </div>

            </div>
            <!-- categories lists -->

            <!-- slider details -->
            <div class="x_panel slider-sidebar">

                <div class="x_title">

                    <h2>
						<?= __(  'Slider' ) ?>
                    </h2>

                    <div class="clearfix"></div>
                </div>

                <div class="x_content">

                    <form class="form-horizontal form-label-left" id="select-template">

                        <div class="form-group">

                            <div class="col-md-12 col-sm-12 col-xs-12">

                                <select class="form-control slider-lists">

                                    <option value="">
										<?= __( 'Choose Slider' ) ?>
                                    </option>

									<?php foreach ( Posts::find()
                                        -> where( [
                                            'type'      => 'slider',
                                            'isDeleted' => false
                                        ] )
                                        -> all() as $slider
                                    ) { ?>

                                        <option value="<?= $slider->id ?>">
											<?= $slider->title; ?>
                                        </option>

									<?php } ?>

                                </select>

                            </div>

                        </div>

                    </form>

                </div>

            </div>
            <!-- slider details -->

            <!-- post thumbnail -->
            <div class="x_panel thumbnail-sidebar">

                <div class="x_title">

                    <h2>
                        <?= __(  'Thumbnail' ) ?>
                    </h2>

                    <div class="clearfix"></div>
                </div>

                <div class="container">

                    <div class="row">

                        <div class="col-md-12 col-sm-12 col-xs-12 text-center">

                            <img src="" class="thumbnail-view" />

                            <a href="#post-modal" data-toggle="modal" data-target="#post-modal" class="choose-thumbnail">
                                <?= __(  'Select Image' ) ?>
                            </a>

                        </div>

                    </div>

                </div>

            </div>
            <!-- post thumbnail -->

            <!-- seo details -->
            <div class="x_panel seo-sidebar">

                <div class="x_title">

                    <h2>
                        <?= __(  'SEO' ) ?>
                    </h2>

                    <div class="clearfix"></div>

                </div>

                <div class="x_content">

                    <div class="form-group">

                        <div class="col-md-12 col-sm-12 col-xs-12">

                            <label for="seo_title">
								<?= __(  'SEO title' ) ?>
                            </label>

                            <input class="form-control" id="seo-title"/>

                        </div>

                    </div>

                    <div class="form-group">

                        <div class="col-md-12 col-sm-12 col-xs-12">

                            <label for="seo_description">
								<?= __(  'SEO description' ) ?>
                            </label>

                            <textarea class="form-control" id="seo-description"></textarea>

                        </div>

                    </div>

                    <div class="form-group">

                        <div class="col-md-12 col-sm-12 col-xs-12">

                            <label for="seo_tags">
								<?= __(  'SEO tags' ) ?>
                            </label>

                            <input class="form-control" id="seo-tags" type="text"/>

                        </div>

                    </div>

                </div>

            </div>
            <!-- seo details -->

        </div>
        <!-- sidebar -->

    </div>