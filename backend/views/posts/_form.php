<?php /***************************************************************
 * 	    	 	   	    FORM SECTION FOR POST                        *
 *********************************************************************/

use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

use dosamigos\tinymce\TinyMce;

use common\models\Languages;

use backend\widgets\MediaPopup;

/* @var $this yii\web\View         */
/* @var $model common\models\Posts */
/* @var $lang_id integer           */

//main post form
$form = ActiveForm::begin( [
    'options'                => [
        'class'                 => 'form-horizontal form-label-left ajax-submitting-form need-image need-seo',
        'id'                    => 'post-form',
        'data-func'             => 'formCallback',
        'data-redirectonfinish' => Url::toRoute( [
            '/posts'
        ] )
    ],
    'enableAjaxValidation'   => false,
    'enableClientValidation' => true
] ); ?>

    <div class="x_panel">

        <div class="x_content">

            <?= $form
                -> field( $model, 'seo', [
					'template' => '{input}',
					'options'  => [
						'tag' => false, // Don't wrap with "form-group" div
					]
				] )
                -> hiddenInput( [
                    'id' => 'seo-model'
                ] )
                -> label( false );
            ?>

            <?= $form
                -> field( $model, 'file_id', [
					'template' => '{input}',
					'options'  => [
						'tag' => false, // Don't wrap with "form-group" div
					]
				] )
                -> hiddenInput( [
                    'id' => 'file_id'
                ] )
                -> label( false );
            ?>

            <?= $form
                -> field( $model, 'categories', [
					'template' => '{input}',
					'options'  => [
						'tag' => false, // Don't wrap with "form-group" div
					]
				] )
                -> hiddenInput( [
                    'id' => 'taxonomy-lists'
                ] )
                -> label( false );
            ?>

            <?= $form
                -> field( $model, 'slider', [
					'template' => '{input}',
					'options'  => [
						'tag' => false, // Don't wrap with "form-group" div
					]
				] )
                -> hiddenInput( [
                    'id' => 'slider-id'
                ] )
                -> label( false );
            ?>

            <div class="form-group">

				<?= $form
					-> field( $model, 'title' )
					-> textInput( [
						'id'          => 'title',
						'placeholder' => __(  'Title' )
					] )
					-> label( false );
				?>

				<?= $form
					-> field( $model, 'lang_id' )
					-> dropDownList( ArrayHelper::map(
						Languages::find()->all(),
						'id',
						'description'
					), [
						'options' => [
							$lang_id => [
								'Selected'=>'selected'
							]
						]
					], [
						'prompt' => __(  'Choose language' )
					] )
					-> label( false );
				?>

            </div>

			<?= $form
                -> field( $model, 'description' )
                -> widget( TinyMce::className(), [
                    'options' => [
                        'rows' => 15
                    ],
                    'language'      => 'en_GB',
				    'clientOptions' => [
					    'menubar'   => false,
					    'plugins'   => [
						    "advlist image autolink lists link charmap print preview anchor",
						    "searchreplace visualblocks code fullscreen",
						    "insertdatetime media table contextmenu paste"
					    ],
					    'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | visualblocks | code "
				    ]
			    ] )
                -> label( false );
			?>

            <?= $form
                -> field( $model, 'type' )
                -> hiddenInput( [
                    'readonly' => true,
                    'value'    => 'posts'
                ] )
                -> label( false );
            ?>

            <div class="ln_solid"></div>

            <div class="form-group">

                <div class="col-md-12 col-md-12 col-xs-12 text-center" style="width: 100%">

                    <input class="btn btn-primary" data-value="test" type="submit" value="<?= ( $model->isNewRecord
                        ? __(  'Publish' )
                        : __(  'Update' ) )
                    ?>">

                </div>

            </div>

        </div>

    </div>

<?php ActiveForm::end(); ?>

<!-- media popup window -->
<?= MediaPopup::widget( [
	'formType'   => 'post-modal',
	'uploadForm' => $this->render(
        'upload'
    )
] ) ?>
<!-- media popup window -->
