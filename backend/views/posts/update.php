<?php /***************************************************************
 *	     		 	   	  UPDATE POST PART                           *
 *********************************************************************/

use yii\helpers\Html;

use common\models\Files;
use common\models\Languages;
use common\models\Posts;

/* @var $this yii\web\View                    */
/* @var $model \common\models\Posts           */
/* @var $lang_id string                       */
/* @var $categories \common\models\Taxonomies */

// current page title
$this->title = __(  'Update post' );

//seo details for current post
$seo = ! empty( $model->seo )
    ? json_decode( $model->seo )
    : ''; ?>

    <div class="col-md-12 col-sm-12 col-xs-12 edit-container-block">

        <div class="page-title">

            <!-- title section -->
            <div class="title_left">

                <h3>
                    <?= $this->title ?>
                </h3>

            </div>
            <!-- title section -->

            <!-- languages section -->
            <div class="title_right">

                <div class="pull-right posts_language text-right">

                    <span>
                        <?= __(  'Choose language' ) ?>:
                    </span>

					<?php foreach ( Languages::find()->all() as $lang ) { ?>

						<?= Html::a( $lang->description, [
							'/posts/update',
							'id'      => $model->id,
							'lang_id' => $lang->id
						], [
							'class' => (int)$lang_id == $lang->id
								? 'btn btn-primary btn-xs'
								: 'btn btn-default btn-xs'
						] ) ?>

					<?php } ?>

                </div>

            </div>
            <!-- languages section -->

        </div>

        <!-- form section -->
        <div class="col-md-9 col-sm-12 col-xs-12 post-form">

            <?= $this->render( '_form', [
                'model'   => $model,
                'lang_id' => $lang_id
            ] ); ?>

        </div>
        <!-- form section -->

        <!-- sidebar -->
        <div class="col-md-3 col-sm-12 col-xs-12 sidebar">

            <?php if ( (int)$lang_id > 0
                && $model->lang_id == $lang_id || (int)$lang_id == 0
            ) { ?>

                <!-- categories lists -->
                <div class="x_panel">

                    <div class="x_title post-categories">

                        <h2>
							<?= __(  'Categories' ) ?>
                        </h2>

                        <div class="clearfix"></div>

                    </div>

                    <div class="x_content taxonomies-sidebar" role="tabpanel" data-id="cats-tabs" style="padding: 5px 0px;">

                        <ul class="taxonomies-list">

							<?php if ( isset( $categories )
								&& !empty( $categories )
							) {

							    //the list of selected categories
							    $selected_categories = json_decode(
                                    $model->categories
                                );

								foreach ( $categories as $category ) { ?>

                                    <li>

                                        <label class="checkbox">

											<?= $category[ 'name' ] ?>

                                            <input name="post_categories" type="checkbox" <?= isset( $selected_categories->{ $category[ 'id' ] } )
                                                ? 'checked'
                                                : ''
                                            ?> data-id="<?= $category[ 'id' ] ?>" value="<?= $category[ 'name' ] ?>" >

                                            <span class="checkmark"></span>

                                        </label>

                                    </li>

								<?php }

							} else { ?>

                                <li>

                                    <label>
										<?= __(  'Category is absent' ) ?>
                                    </label>

                                </li>

							<?php } ?>

                        </ul>

                    </div>

                </div>
                <!-- categories lists -->

                <!-- slider details -->
                <div class="x_panel slider-sidebar">

                    <div class="x_title">

                        <h2>
							<?= __(  'Slider' ) ?>
                        </h2>

                        <div class="clearfix"></div>
                    </div>

                    <div class="x_content">

                        <form class="form-horizontal form-label-left" id="select-template">

                            <div class="form-group">

                                <div class="col-md-12 col-sm-12 col-xs-12">

                                    <select class="form-control slider-lists">

                                        <option value="">
											<?= __( 'Choose Slider' ) ?>
                                        </option>

										<?php foreach ( Posts::find()
                                            -> where( [
                                                'type'      => 'slider',
                                                'isDeleted' => false
                                            ] )
                                            -> all() as $slider
                                        ) { ?>

                                            <option value="<?= $slider->id ?>" <?= $slider->id == $model->slider ? 'selected' : '' ?>>
												<?= $slider->title; ?>
                                            </option>

										<?php } ?>

                                    </select>

                                </div>

                            </div>

                        </form>

                    </div>

                </div>
                <!-- slider details -->

                <!-- post thumbnail -->
                <div class="x_panel thumbnail-sidebar">

                    <div class="x_title">

                        <h2>
                            <?= __(  'Thumbnail' ) ?>
                        </h2>

                        <div class="clearfix"></div>
                    </div>

                    <div class="container">

                        <div class="row">

                            <div class="col-md-12 col-sm-12 col-xs-12 text-center">

                                <img class="thumbnail-view" src="<?= Yii::$app->common->thumbnail( $model->file_id ) ?>" style="<?= is_null( $model->file_id )
									? 'display: none;'
									: ''
								?>"/>

                                <a href="#post-modal" data-toggle="modal" data-target="#post-modal" class="choose-thumbnail">
                                    <?= __(  'Select Image' ) ?>
                                </a>

                            </div>

                        </div>

                    </div>

                </div>
                <!-- post thumbnail -->

            <?php } ?>

            <!-- seo details -->
            <div class="x_panel seo-sidebar">

                <div class="x_title">

                    <h2>
                        <?= __(  'SEO' ) ?>
                    </h2>

                    <div class="clearfix"></div>
                </div>

                <div class="x_content">

                    <div class="form-group">

                        <div class="col-md-12 col-sm-12 col-xs-12">

                            <label for="seo_title">
								<?= __(  'SEO title' ) ?>
                            </label>

                            <input type="text" class="form-control" id="seo-title" value="<?= ! empty( $seo ) && isset( $seo->title )
								? $seo->title
								: ''
							?>" />

                        </div>

                    </div>

                    <div class="form-group">

                        <div class="col-md-12 col-sm-12 col-xs-12">

                            <label for="seo_description">
								<?= __(  'SEO description' ) ?>
                            </label>

                            <textarea class="form-control" id="seo-description"><?= ! empty( $seo ) && isset( $seo->description ) ? $seo->description : '' ?></textarea>

                        </div>

                    </div>

                    <div class="form-group">

                        <div class="col-md-12 col-sm-12 col-xs-12">

                            <label for="seo_tags">
								<?= __(  'SEO tags' ) ?>
                            </label>

                            <input type="text" class="form-control" id="seo-tags" value="<?= ! empty( $seo ) && isset( $seo->tags )
								? $seo->tags
								: ''
							?>"/>

                        </div>

                    </div>

                </div>

            </div>
            <!-- seo details -->

        </div>
        <!-- sidebar -->

    </div>