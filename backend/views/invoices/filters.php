<?php /***************************************************************
 *         		 	   	  ORDER FILTERS SECTION                      *
 *********************************************************************/

/** @var $searchModel common\models\LogisticsSearch */

use kartik\date\DatePicker;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

use common\models\User;

//get request short option
$get_request = Yii::$app->getRequest();
$form 		 = ActiveForm::begin( [
	'id'      => 'orders-filter-form',
	'method'  => 'get',
	'action'  => Url::to( [
		'/orders'
	] )
] ); ?>

	<ul class="list-filters">

        <li class="col-sm-4 col-md-4 col-xs-12" style="padding-right: 0px;">

			<?= $form
				-> field( $searchModel, 'customer_id' )
				-> dropDownList( ArrayHelper::map( User::findAll( [
					'id'        => Yii::$app->authManager->getUserIdsByRole( 'client' ),
					'isDeleted' => false
				] ), 'id', function( $model ) {
					return $model[ 'first_name'] . ' ' . $model[ 'last_name' ];
				} ), [
					'prompt' => __(  'Select Customer' ),
					'class'  => 'form-control'
				] )
				-> label( false );
			?>

        </li>

		<li class="col-sm-3 col-md-3 col-xs-12" style="padding-right: 0px;">

			<?= DatePicker::widget( [
				'name'    => 'date_from',
				'type'    => DatePicker::TYPE_INPUT,
				'value'   => ! is_null( $get_request->getQueryParam('date_from' ) )
					? date('d.m.Y', strtotime( $get_request->getQueryParam('date_from' ) ) )
					: date('d.m.Y' ) ,
				'options' => [
					'placeholder' => __(  'Date From' ),
					'class'       => 'order-filter-datetime form-control'
				],
				'pluginOptions' => [
					'autoclose' => true,
					'format'    => 'dd.mm.yyyy',
					'weekStart' => '1'
				]
			] ); ?>

		</li>

		<li class="col-sm-3 col-md-3 col-xs-12" style="padding-right: 0px;">

			<?= DatePicker::widget( [
				'name'    => 'date_to',
				'type'    => DatePicker::TYPE_INPUT,
				'value'   => ! is_null( $get_request->getQueryParam('date_to' ) )
					? date('d.m.Y', strtotime( $get_request->getQueryParam('date_to' ) ) )
					: date('d.m.Y' ) ,
				'options' => [
					'placeholder' => __(  'Date To' ),
					'class'       => 'order-filter-datetime form-control'
				],
				'pluginOptions' => [
					'autoclose' => true,
					'format'    => 'dd.mm.yyyy',
					'weekStart' => '1'
				]
			] ); ?>

		</li>

        <li class="col-sm-2 col-md-2 col-xs-12" style="padding-right: 0px;">

            <button class="btn btn-default" style="width: 100%">
				<?= __(  'Filter' ); ?>
            </button>

        </li>

	</ul>

<?php ActiveForm::end(); ?>