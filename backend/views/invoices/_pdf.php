<?php /***************************************************************
 *	  		             PDF DETAILS FOR INVOICE                     *
 *********************************************************************/

/* @var $model common\models\Orders      */
/* @var $transportation array            */
/* @var $type common\models\Taxonomies   */
/* @var $invoice common\models\Invoices  */
/* @var $company common\models\Companies */

//delivery terms lists
$terms = Yii::$app->common->transportationDetails( 'terms' ); ?>

<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">

        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Bootstrap -->
        <link href="<?= Yii::getAlias( '@web' ) ?>/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
        <!-- Bootstrap -->

    </head>

    <body style="position: relative;">

        <!-- header -->
        <header>

            <!-- header top navigation section -->
            <section class="top">

                <div class="container" style="width: 970px;">

                    <div class="row">

                        <!-- logo section -->
                        <div class="col-sm-3 col-md-3 col-xs-3" style="float: left; width: 35%; padding: 0px;">

                            <p style="font-size: 12px">
                                <b>InterRail Services AG</b><br>
                                Postfach 230<br>
                                Winkelriedstrasse 19<br>
                                CH-9001 St. Gallen/Switzerland
                            </p>

                            <h4 style="font-weight: bold; margin-top: 35px;">

                                <?= isset( $company->name )
                                    ? $company->name
                                    : ''
                                ?>

                            </h4>

                        </div>
                        <!-- logo section -->

                        <!-- top navigation -->
                        <div class="col-sm-9 col-md-9 col-xs-9" style="width: 55%; float: right; text-align: right; padding-top: 0px;">

                            <a href="javascript:void(0)" class="logo">
                                <img src="<?= Yii::getAlias( '@web' ) ?>/images/logo.jpg" style="max-width: 80%;" />
                            </a>

                            <h4 style="font-weight: bold; margin-top: 35px;">

                                INVOICE NO: <?= isset( $invoice->number )
                                    ? $invoice->number
                                    : ''
                                ?>

                            </h4>

                        </div>
                        <!-- top navigation -->

                    </div>

                </div>

            </section>
            <!-- header top navigation section -->

        </header>
        <!-- header -->

        <!-- our-services -->
        <section class="container-details">

            <div class="container" style="width: 970px;">

                <div class="row">

                    <div class="col-sm-12 col-md-12 col-xs-12" style="margin: 30px 0px; padding: 0px;   ">

                        <div class="col-md-6 col-sm-6" style="padding: 0px; width: 47.5%; float: left;">

                            <div style="font-weight: bold; width: 30%; text-transform: uppercase; float: left;">
                                <?= __( 'YOUR REF' ) ?> : <br>
								<?= __( 'SHIPPER' ) ?>  : <?= $model->shipper ?>
								<?= __( 'CONSIGNEE' ) ?>: <?= $model->consignee ?>
								<?= __( 'FROM' ) ?>:
                            </div>

                            <div style="text-transform: uppercase; float: left; word-break: break-all; max-width: 60%;">
                                <br>
                                <br>
                                <br>
                               <?= isset( $terms[ $model->delivery_terms[ 'from' ] ] )
                                   ? $terms[ $model->delivery_terms[ 'from' ] ] . ' ' . $model->trans_from
                                   : $model->trans_from
                               ?>

                            </div>

                        </div>

                        <div class="col-md-6 col-sm-6" style="width: 47.5%; float: left;">

                            <br>
                            <br>
                            <br>

                            <div style="font-weight: bold; text-transform: uppercase; float: left; width: 20%;">
								<?= __( 'TO' ) ?> :
                            </div>

                            <div style="text-transform: uppercase; width: 70%; float: left;">

								<?= isset( $terms[ $model->delivery_terms[ 'to' ] ] )
									? $terms[ $model->delivery_terms[ 'to' ] ] . ' ' . $model->trans_to
									: $model->trans_to
								?>

                            </div>

                        </div>

                    </div>

                    <?php if ( ! empty( $transportation ) ) { ?>

                        <h4 style="font-weight: bold; float: left; width: 100%; margin-bottom: 25px;">
                            <?= __( 'SHIPMENT CONTAINER NUMBERS' ) ?> :
                        </h4>

                        <div style="float: left; padding: 0px; width: 100%; list-style: none; border-bottom: 1px solid #000000; padding-bottom: 15px; margin-bottom: 50px;">

                            <?php foreach ( $transportation as $key => $value ) { ?>

                                <div style="padding-left: 5px; float: left; width: 18.5%; padding-right: 5px;">
                                    <?= $value[ 'number' ] ?>
                                </div>

                            <?php } ?>

                        </div>

                        <div class="col-sm-12 col-md-12 col-xs-12" style="padding: 0px;">

                            <div class="col-sm-6 col-md-6 col-xs-6" style="padding-left: 0px; width: 47%; float: left;">

                                <h4 style="font-weight: bold; margin-top: 50px;">
                                    <?= __( 'Freight charges' ); ?>
                                </h4>

                            </div>

                            <div class="col-sm-6 col-md-6 col-xs-6" style="padding: 0px; width: 50.6%; float: left;">

                                <table style="width: 100%">

                                    <thead>

                                        <tr style="border-color: #ffffff;">

                                            <th style="text-align: center; background: #d2d2d2; padding: 10px; border-color: #ffffff">
                                                <?= __( 'Quantity' ) ?>
                                            </th>

                                            <th style="text-align: center; background: #d2d2d2; padding: 10px; border-color: #ffffff">
												<?= __( 'Unit' ) ?>
                                            </th>

                                            <th style="text-align: center; background: #d2d2d2; padding: 10px; border-color: #ffffff">
												<?= __( 'Amount' ) ?>
                                            </th>

                                            <th style="text-align: center; background: #d2d2d2; padding: 10px; border-color: #ffffff">
												<?= __( 'USD' ) ?>
                                            </th>

                                        </tr>

                                    </thead>

                                    <tbody>

                                        <tr>

                                            <td style="background: #d2d2d2; text-align: center; padding: 15px 15px 100px; font-weight: bold; border-color: #ffffff;">
                                                <?= count( $transportation ) ?>
                                            </td>

                                            <td style="background: #d2d2d2; text-align: center; padding: 15px 15px 100px; font-weight: bold; border-color: #ffffff;">

												<?= ! is_null( $type )
                                                    ? $type->name
                                                    : __( 'Unit not defined' )
                                                ?>

                                            </td>

                                            <td style="background: #d2d2d2; text-align: center; padding: 15px 15px 100px; font-weight: bold; border-color: #ffffff;">

												<?= ! empty( $invoice )
													? number_format(
                                                        (float)$invoice->amount/count( $transportation ),
														2,
														'.',
														''
													)
													: 0
												?>

                                            </td>

                                            <td style="background: #d2d2d2; text-align: center; padding: 15px 15px 100px; font-weight: bold; border-color: #ffffff;">

												<?= ! empty( $invoice )
													? number_format(
													        $invoice->amount,
                                                            2,
                                                            '.',
                                                            ''
                                                    )
													: 0
												?>

                                            </td>

                                        </tr>

                                    </tbody>

                                </table>

                            </div>

                        </div>

                        <div class="col-sm-12 col-md-12 col-xs-12" style="float: left; width: 100%; text-align: right; margin-top: 50px; padding: 0px; text-transform: uppercase; font-weight: bold;">

                            <div style="font-size: 14px; float: left; width: 76%; padding: 5px 15px;">
                                <?= __( 'Total' ) ?>
                            </div>

                            <div style="float:left; background: #d2d2d2; font-weight: bold; padding: 5px 10px; width: 16%; text-align: center">

								<?= ! empty( $invoice )
									? number_format(
										$invoice->amount,
										2,
										'.',
										''
									)
									: 0
								?>

                            </div>

                        </div>

                    <?php } ?>

                </div>

            </div>

        </section>

        <!-- footer -->
        <footer style="font-size: 9px; position: absolute; bottom: 0px; height: 50px;">

            <div class="container" style="width: 970px;">

                <div class="row">

                    <div style="width: 30%; float: left; margin-right: 20px;">
                        Wir arbeiten ausschliesslich
                        aufgrund unserer Transportübernahmebedingungen.
                    </div>

                    <div style="width: 16%; float: left; margin-right: 20px;">
                        Credit Suisse
                        CH-9001 St. Gallen
                        SWIFT: CHRESCHZZ90A
                    </div>

                    <div style="width: 25%; float: left; margin-right: 30px;">
                        USD CH05 0483 5044 2540 8200 0 <br>
                        CHF CH39 0483 5044 2540 8100 0 <br>
                        EUR CH75 0483 5044 2540 8200 1 <br>
                    </div>

                    <div style="width: 18%; float: left; padding-right: 5px;">
                        Tel.: +41 71 227 15 40 <br>
                        Fax: +41 71 227 15 30 <br>
                        E-Mail: info@interrail.ag <br>
                    </div>

                    <div style="width: 10%; float: left;">
                        Website<br>
                        www.interrail.ag
                    </div>

                </div>

            </div>

        </footer>
        <!-- footer -->

    </body>

</html>