<?php /***************************************************************
 *	  	   	    THE LIST OF ALL AVAILABLE INVOICES PAGE              *
 *********************************************************************/

use yii\helpers\Html;
use yii\widgets\LinkPager;

/* @var $this yii\web\View                        */
/* @var $searchModel common\models\InvoicesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $users array                              */
/* @var $orders array                             */

// current page title
$this->title = __(  'Invoices' );

//get request short option
$request = Yii::$app->getRequest(); ?>

<div class="expenses-list">

	<div class="page-title">

        <!-- title section -->
		<div class="title_left">

			<h3>

				<?= $this->title ?>

			</h3>

		</div>
        <!-- title section -->

        <!-- search section -->
        <div class="title_right">

			<?= $this->render( '_search', [
				'model' => $searchModel
			] ); ?>

        </div>
        <!-- search section -->

	</div>

	<div class="clearfix"></div>

	<div class="row">

		<div class="col-md-12 col-sm-12 col-xs-12">

            <!-- filters section -->
            <div class="main-filters-container">

				<?= $this->render( 'filters', [
					'searchModel' => $searchModel
				] ) ?>

            </div>
            <!-- filters section -->

			<div class="x_panel">

				<div class="x_content">

                    <!-- invoices lists -->
					<table class="table table-striped projects">

						<thead>

							<tr>

								<th>
									#
								</th>

                                <th>
									<?= __(  'Invoice Number' ) ?>
                                </th>

								<th>
									<?= __(  'Order Number' ) ?>
								</th>

								<th>
									<?= __(  'Customer' ) ?>
								</th>

								<th>
									<?= __(  'Invoice Date' ) ?>
								</th>

								<th>
									<?= __(  'Invoice Amount' ) ?>
								</th>

								<th>
									<?= __(  'Balance Due' ) ?>
								</th>

								<th>
									<?= __(  'Status' ) ?>
								</th>

								<th></th>

							</tr>

						</thead>

						<tbody>

							<?php if ( !empty( $dataProvider->models ) ) {

								foreach ( $dataProvider->models as $model ) { ?>

									<tr>

										<td>
											#
										</td>

                                        <td>
                                            <?= $model->number ?>
                                        </td>

										<td>

											<?= Html::a( isset( $orders[ $model->foreign_id ] )
												? $orders[ $model->foreign_id ][ 'number' ]
												: $model->foreign_id, [
												'/orders/update',
												'id' => $model->foreign_id
											], [
												'role' => "button"
											] ) ?>

										</td>

										<td>

											<?= isset( $users[ $model[ 'customer_id' ] ] )
												? $users[ $model[ 'customer_id' ] ][ 'first_name' ] . ' ' . $users[ $model[ 'customer_id' ] ][ 'last_name' ]
												: __( 'Unknown Customer' )
											?>

										</td>

										<td>
											<?= date( 'Y-m-d', strtotime( $model->date ) ); ?>
										</td>

										<td>
											<?= $model->amount; ?>
										</td>

										<td>
											<?= $model->balance_due; ?>
										</td>

										<td>
											<?= __( $model->status ); ?>
										</td>

										<td class="text-center model-actions">

											<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
												<i class="fa fa-ellipsis-v"></i>
											</a>

											<ul class="dropdown-menu pull-right">

												<li>

													<?= Html::a( '<i class="fa fa-pencil"></i> ' .  __(  'Edit' ), [
														'orders/update',
														'id' => $model->foreign_id
													] ); ?>

												</li>

											</ul>

										</td>

									</tr>

								<?php }

							} else {

								echo '<tr>';

								    echo '<td colspan="9" class="text-center">
                                        ' . __(  'There is no invoices yet' ) . '
                                    </td>';

								echo '</tr>';

							} ?>

						</tbody>

					</table>
                    <!-- invoices lists -->

				</div>

                <!-- pagination section -->
				<div class="pagination-container">

					<?= LinkPager::widget( [
						'pagination' => $dataProvider->pagination,
					] ); ?>

				</div>
                <!-- pagination section -->

			</div>

		</div>

	</div>

</div>