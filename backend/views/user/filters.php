<?php /***************************************************************
 *         		 	   	  USER FILTERS SECTION                       *
 *********************************************************************/

/* @var $model common\models\User */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

//get request short option
$get_request = Yii::$app->getRequest();
$form 		 = ActiveForm::begin( [
	'id'      => 'users-filter-form',
	'method'  => 'get',
	'action'  => Url::to( [
		'/user'
	] ),
	'fieldConfig' => [
		'options' => [
			'tag' => false,
		]
	]
] ); ?>

    <ul class="list-filters">

        <li>

			<?= Html::dropDownList(
				'role',
				$get_request->getQueryParam( 'role' ) == ''
                    ? null
                    : $get_request->getQueryParam( 'role' ),
				Yii::$app->common->roles(), [
					'class'  => 'form-control',
					'style'  => 'width:100%'
				]
			); ?>

        </li>

        <li class="filter-element">

            <button>
				<?= __(  'Filter' ); ?>
            </button>

        </li>

    </ul>

<?php ActiveForm::end(); ?>