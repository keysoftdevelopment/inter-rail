<?php /***************************************************************
 * 	    	 	   	    FORM SECTION FOR USER PAGE                   *
 *********************************************************************/

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

use common\models\Languages;

/* @var $this yii\web\View        */
/* @var $model common\models\User */

//users main form
$form = ActiveForm::begin( [
    'options'                => [
        'class' => 'form-horizontal form-label-left need-image',
        'id'    => 'userform'
    ],
    'enableAjaxValidation'   => false,
    'enableClientValidation' => true
] ); ?>

    <?= $form
        -> field( $model, 'file_id' )
        -> hiddenInput( [
            'id' => 'file_id'
        ] )
        -> label( false );
    ?>

    <div class="col-sm-12 col-md-12 col-xs-12">

        <label for="username" class="control-label col-md-4 col-sm-4 col-xs-12">
            <?= __(  'Username' ) ?>
            <span class="required">*</span>
        </label>

        <div class="col-md-6 col-sm-6 col-xs-12">

            <?= $form->field( $model, 'username' )
                -> textInput( [
                    'class' => 'form-control col-md-7 col-xs-12',
                    'id'    => 'username'
                ] )
                -> label( false );
            ?>

        </div>

    </div>

    <div class="col-sm-12 col-md-12 col-xs-12">

        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="email">
            <?= __(  'Email' ) ?>
            <span class="required">*</span>
        </label>

        <div class="col-md-6 col-sm-6 col-xs-12">

            <?= $form->field( $model, 'email' )
                -> textInput( [
                    'class' => 'form-control col-md-7 col-xs-12',
                    'id'    => 'email'
                ] )
                -> label( false );
            ?>

        </div>

    </div>

    <div class="col-sm-12 col-md-12 col-xs-12">

        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="password">

            <?= __(  'Password' ) ?>
            <span class="required">*</span>

        </label>

        <div class="col-md-6 col-sm-6 col-xs-12">

            <?= $form->field( $model, 'password' )
                -> passwordInput( [
                    'class' => 'form-control col-md-7 col-xs-12',
                    'id'    => 'password'
                ] )
                -> label( false );
            ?>

        </div>

    </div>

    <?php if ( Yii::$app->user->can( 'users' ) ) { ?>

        <div class="col-sm-12 col-md-12 col-xs-12">

            <label class="control-label col-md-4 col-sm-4 col-xs-12" for="userrole">
                <?= __(  'User Type' ) ?>
                <span class="required">*</span>
            </label>

            <div class="col-md-6 col-sm-6 col-xs-12">

                <?= $form->field( $model, 'role' )
                    -> dropDownList( Yii::$app->common->roles(), [
                        'class' => 'form-control col-md-7 col-xs-12',
                        'id'    => 'userRole'
                    ] )
                    -> label( false );
                ?>

            </div>

        </div>

    <?php } ?>

    <div class="col-sm-12 col-md-12 col-xs-12">

        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="phone">
            <?= __(  'Phone' ) ?>
            <span class="required">*</span>
        </label>

        <div class="col-md-6 col-sm-6 col-xs-12">

            <?= $form->field( $model, 'phone' )
                -> textInput( [
                    'id'    => 'phone',
                    'class' => 'form-control col-md-7 col-xs-12'
                ] )
                -> label( false );
            ?>

        </div>

    </div>

    <div class="col-sm-12 col-md-12 col-xs-12">

        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="first_name">

            <?= __(  'First Name' ); ?>
            <span class="required">*</span>

        </label>

        <div class="col-md-6 col-sm-6 col-xs-12">

            <?= $form->field( $model, 'first_name' )
                -> textInput( [
                    'id'    => 'first-name',
                    'class' => 'form-control col-md-7 col-xs-12'
                ] )
                -> label( false );
            ?>

        </div>

    </div>

    <div class="col-sm-12 col-md-12 col-xs-12">

        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="last_name">

            <?= __(  'Last name' ); ?>
            <span class="required">*</span>

        </label>

        <div class="col-md-6 col-sm-6 col-xs-12">

            <?= $form->field( $model, 'last_name' )
                -> textInput( [
                    'id'    => 'last-name',
                    'class' => 'form-control col-md-7 col-xs-12'
                ] )
                -> label( false );
            ?>

        </div>

    </div>

    <div class="col-sm-12 col-md-12 col-xs-12">

        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="company-name">
			<?= __(  'Company Name' ); ?>
        </label>

        <div class="col-md-6 col-sm-6 col-xs-12">

			<?= $form->field( $model, 'company_name' )
				-> textInput( [
                    'id'    => 'company_name',
					'class' => 'form-control col-md-7 col-xs-12'
				] )
				-> label( false );
			?>

        </div>

    </div>

    <div class="col-sm-12 col-md-12 col-xs-12">

        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="country">
			<?= __(  'Country' ); ?>
        </label>

        <div class="col-md-6 col-sm-6 col-xs-12">

			<?= $form->field( $model, 'country' )
				-> textInput( [
					'id'    => 'country',
					'class' => 'form-control col-md-7 col-xs-12'
				] )
				-> label( false );
			?>

        </div>

    </div>

    <div class="col-sm-12 col-md-12 col-xs-12">

        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="city">
			<?= __(  'City' ); ?>
        </label>

        <div class="col-md-6 col-sm-6 col-xs-12">

			<?= $form->field( $model, 'city' )
				-> textInput( [
					'id'    => 'city',
					'class' => 'form-control col-md-7 col-xs-12'
				] )
				-> label( false );
			?>

        </div>

    </div>

    <div class="col-sm-12 col-md-12 col-xs-12">

        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="address">
			<?= __(  'Address' ); ?>
        </label>

        <div class="col-md-6 col-sm-6 col-xs-12">

			<?= $form->field( $model, 'street' )
				-> textInput( [
					'id'    => 'address',
					'class' => 'form-control col-md-7 col-xs-12'
				] )
				-> label( false );
			?>

        </div>

    </div>

    <div class="col-sm-12 col-md-12 col-xs-12">

        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="bank">
			<?= __(  'Bank' ); ?>
        </label>

        <div class="col-md-6 col-sm-6 col-xs-12">

			<?= $form->field( $model, 'bank' )
				-> textInput( [
					'id'    => 'bank',
					'class' => 'form-control col-md-7 col-xs-12'
				] )
				-> label( false );
			?>

        </div>

    </div>

    <div class="col-sm-12 col-md-12 col-xs-12">

        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="mfo">
			<?= __(  'MFO' ); ?>
        </label>

        <div class="col-md-6 col-sm-6 col-xs-12">

			<?= $form->field( $model, 'mfo' )
				-> textInput( [
					'id'    => 'mfo',
					'class' => 'form-control col-md-7 col-xs-12'
				] )
				-> label( false );
			?>

        </div>

    </div>

    <div class="col-sm-12 col-md-12 col-xs-12">

        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="oked">
			<?= __(  'OKED' ); ?>
        </label>

        <div class="col-md-6 col-sm-6 col-xs-12">

			<?= $form->field( $model, 'oked' )
				-> textInput( [
					'id'    => 'oked',
					'class' => 'form-control col-md-7 col-xs-12'
				] )
				-> label( false );
			?>

        </div>

    </div>

    <?php if ( ! $model->isNewRecord ) { ?>

        <div class="col-sm-12 col-md-12 col-xs-12">

            <label class="control-label col-md-4 col-sm-4 col-xs-12" for="oked">
                <?= __(  'Choose Language' ); ?>
            </label>

            <div class="col-md-6 col-sm-6 col-xs-12">

				<?= Html::dropDownList(
					'language',
					! empty( Yii::$app->common->getCookies( 'language' ) )
						? Yii::$app->common->getCookies( 'language' )
						: '',
					ArrayHelper::map( Languages::find()->all(), 'id', 'description' ),
					[
						'class' => 'form-control'
					] )
				?>

            </div>

        </div>

    <?php } ?>


    <div class="clearfix"></div>

    <div class="col-sm-12 col-md-12 col-xs-12 text-center update-profile-btn">

        <input type="submit" class="btn btn-primary" value="<?= ! $model->isNewRecord
            ? __(  'Update Profile' )
            : __( 'Create User' )?>"
        >

    </div>

<?php ActiveForm::end(); ?>