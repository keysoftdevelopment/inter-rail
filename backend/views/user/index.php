<?php /***************************************************************
 *	  	   	     THE LIST OF ALL AVAILABLE USERS PAGE                *
 *********************************************************************/

use yii\helpers\Html;
use yii\widgets\LinkPager;

use common\models\User;

use common\widgets\Alert;

/* @var $this yii\web\View                        */
/* @var $searchModel common\models\UserSearch     */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $model common\models\User                 */
/* @var $invoices common\models\Invoices[]        */

// current page title, that will displayed in head tags
$this->title = __(  'Users' ); ?>

    <div class="page-title">

        <!-- title section -->
        <div class="title_left">

            <h3>
                <?= __(  'Users' ) ?>
            </h3>

        </div>
        <!-- title section -->

        <!-- search section -->
        <div class="title_right">

            <?= $this->render( '_search', [
                'model' => $searchModel
            ] ); ?>

        </div>
        <!-- search section -->

    </div>

    <div class="clearfix"></div>

    <div class="row" id="users-lists">

        <div class="col-md-12 col-sm-12 col-xs-12">

            <!-- filters section -->
            <div class="user-filters-container">

				<?= $this->render( 'filters', [
                    'model'  => User::findOne(
                        \Yii::$app->user->id
                    )
				] ) ?>

            </div>
            <!-- filters section -->

            <div class="x_panel">

                <div class="x_content">

                    <!-- flash messages -->
                    <?= Alert::widget() ?>
                    <!-- flash messages -->

                    <!-- users lists -->
                    <div class="row">

                        <?php if ( ! empty( $dataProvider->models ) ) {

                            foreach ( $dataProvider->models as $model ) { ?>

                                <div class="col-md-3 col-sm-3 col-xs-12 user-list">

                                    <div class="well profile_view">

                                        <div class="col-sm-12 col-md-12 col-xs-12">

                                            <ul>

                                                <li class="col-sm-6 col-md-6 col-xs-6">

                                                    <label>
                                                        <?= __( 'Debt' ) ?> :
                                                    </label>

                                                    <?= isset( $invoices[ 'debt' ][ $model->id ] )
                                                        ? (float)$invoices[ 'debt' ][ $model->id ][ 'total' ] - (float)( isset( $invoices[ 'paid' ][ $model->id ][ 'paid' ] )
                                                            ? $invoices[ 'paid' ][ $model->id ][ 'paid' ]
                                                            : 0 )
                                                        : 0
                                                    ?>

                                                </li>

                                                <li class="col-sm-6 col-md-6 col-xs-6 text-right">

                                                    <label>
                                                        <?= __( 'Paid' ) ?> :
                                                    </label>

                                                    <?= isset( $invoices[ 'paid' ][ $model->id ][ 'paid' ] )
														? $invoices[ 'paid' ][ $model->id ][ 'paid' ]
														: 0
                                                    ?>

                                                </li>

                                            </ul>

                                        </div>

                                        <div class="col-sm-12 col-xs-12">

                                            <div class="left col-xs-7">

                                                <h2 class="text-capitalize">
                                                    <?= $model->username; ?>
                                                </h2>

                                                <ul class="list-unstyled">

                                                    <li>
                                                        <i class="fa fa-envelope user_mail"></i>
                                                        <?= $model->email; ?>
                                                    </li>

                                                </ul>

                                            </div>

                                            <div class="right col-xs-5 text-center user_avatar">
                                                <img src="<?= $model->getAvatar( 'white' ); ?>" alt="" class="img-circle img-responsive">
                                            </div>

                                        </div>

                                        <div class="col-xs-12 bottom text-center">

                                            <div class="col-xs-6 col-sm-6 emphasis text-left">

                                                <?= Html::a( '<i class="fa fa-remove"> </i> ' . __(  'Delete' ), [
                                                    'delete',
                                                    'id' => $model->id
                                                ], [
                                                    'class' => 'btn btn-danger btn-xs',
                                                    'data'  => [
                                                        'confirm' => __(  'Do you really want to delete this user?' ),
                                                        'method'  => 'post',
                                                    ],
                                                ] ); ?>

                                            </div>

                                            <div class="col-xs-6 col-sm-6 emphasis text-right">

                                                <?= Html::a( '<i class="fa fa-user"> </i> ' . __(  'View Profile' ), [
                                                    'update',
                                                    'id' => $model->id
                                                ], [
                                                    'class' => 'btn btn-primary btn-xs'
                                                ] ); ?>

                                            </div>

                                        </div>

                                    </div>

                                </div>

                            <?php }

                        } else { ?>

                            <div class="col-sm-12 col-md 12 col-xs-12 text-center no-users-msg">
                                <?= __(  'There is no users yet' ) ?>
                            </div>

                        <?php } ?>

                    </div>
                    <!-- users lists -->

                </div>

                <!-- pagination section -->
                <div class="pagination-container">

                    <?= LinkPager::widget( [
                        'pagination' => $dataProvider->pagination,
                    ] ); ?>

                </div>
                <!-- pagination section -->

            </div>

        </div>

    </div>