<?php /***************************************************************
 *          		 	   	  FILE UPLOAD SECTION                    *
 *********************************************************************/

use yii\widgets\ActiveForm;

/* @var $model common\models\User */

$form = ActiveForm::begin( [
    'id'      => 'upload-user-form',
    'options' => [
        'enctype' => 'multipart/form-data',
        'class'   => 'dropzone'
    ]
] ); ?>

    <?php if ( $model->isNewRecord ) { ?>

        <div class="form-group">
            <img src="<?= Yii::getAlias( '@web' ) . '/images/image-not-found.png' ?>" />
        </div>

    <?php } ?>

<?php ActiveForm::end(); ?>