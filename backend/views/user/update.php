<?php /***************************************************************
 *	     		 	   	  UPDATE USER PART                           *
 *********************************************************************/

use backend\widgets\MediaPopup;

/* @var $this yii\web\View               */
/* @var $model common\models\User        */
/* @var $avatar common\models\UploadForm */

// current page title, that will displayed in head tags
$this->title = __(  'Update User' ) . ': ' . $model->username; ?>

    <!-- title section -->
    <div class="page-title">

        <div class="title_left">

            <h3>
                <?= __(  'Profile' ) ?>
            </h3>

        </div>

    </div>
    <!-- title section -->

    <div class="clearfix"></div>

    <div class="row">

        <!-- user details -->
        <div class="col-md-12 col-sm-12 col-xs-12 user-page">

            <div class="x_panel">

                <div class="x_content">

                    <div class="col-sm-12 col-md-12 col-xs-12">

                        <!-- avatar section -->
                        <div class="col-md-4 col-sm-4 col-xs-12">

                            <div class="profile-img">
                                <img class="img-responsive thumbnail-view" src="<?= $model->getAvatar(); ?>" alt="Avatar" title=" <?= __(  'Change the avatar' ) ?>">
                            </div>

                            <a href="#user-modal" data-toggle="modal" data-target="#user-modal" class="choose-thumbnail">
                                <?= __(  'Select Image' ) ?>
                            </a>

                        </div>
                        <!-- avatar section -->

                        <!-- form section -->
                        <div class="col-md-8 col-sm-8 col-xs-12">

                            <div class="" role="tabpanel" data-example-id="togglable-tabs">

                                <?= $this->render( '_form', [
                                    'model' => $model
                                ] ); ?>

                            </div>

                        </div>
                        <!-- form section -->

                    </div>

                </div>

            </div>

        </div>
        <!-- user details -->

    </div>

    <!-- media popup window -->
    <?= MediaPopup::widget( [
        'formType'   => 'user-modal',
        'uploadForm' => $this->render( 'upload', [
            'model' => $model
        ] )
    ] ) ?>
    <!-- media popup window -->
