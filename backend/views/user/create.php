<?php /***************************************************************
 *	     		 	   	  CREATE USER PART                           *
 *********************************************************************/

/* @var $this yii\web\View               */
/* @var $model common\models\User        */

// current page title, that will displayed in head tags
$this->title = __(  'Create User' ); ?>

    <!-- title section -->
    <div class="page-title">

        <div class="title_left">

            <h3>
                <?= __(  'Create User' ) ?>
            </h3>

        </div>

    </div>
    <!-- title section -->

    <div class="clearfix"></div>

    <div class="row">

        <!-- user details -->
        <div class="col-md-12 col-sm-12 col-xs-12 user-page">

            <div class="x_panel">

                <div class="x_content">

                    <div class="col-sm-12 col-md-12 col-xs-12">

                        <!-- avatar upload section -->
                        <div class="col-sm-4 col-md-4 col-xs-12">

                            <?= $this->render( 'upload', [
                                'model' => $model
                            ] ) ?>

                        </div>
                        <!-- avatar upload section -->

                        <!-- form section -->
                        <div class="col-sm-8 col-md-8 col-xs-12">

                            <?= $this->render( '_form', [
                                'model' => $model
                            ] ) ?>

                        </div>
                        <!-- form section -->

                    </div>

                </div>

            </div>

        </div>
        <!-- user details -->

    </div>