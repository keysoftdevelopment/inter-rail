<?php /***************************************************************
 *                         DEFAULT ERROR PAGE                        *
 *********************************************************************/

/* @var $this yii\web\View */
/* @var $name string       */

//current page title, that will displayed in head tags
$this->title = $name; ?>

<div class="main_container">

    <!-- page content -->
    <div class="col-md-12">

        <div class="col-middle">

            <div class="text-center text-center">

                <h1 class="error-number error">
                    404
                </h1>

                <h2 style="color: #7b7b7b;">
                    <?= __(  'Sorry, but we could`nt find this page' ); ?>
                </h2>

                <p style="color: #7b7b7b;">
                    <?= __(  'This page you are looking for does not exist' ); ?>
                </p>

            </div>

        </div>

    </div>
    <!-- /page content -->

</div>
