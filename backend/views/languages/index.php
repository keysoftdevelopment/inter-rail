<?php /***************************************************************
 *	    	     THE LIST OF ALL AVAILABLE LANGUAGES PAGE            *
 *********************************************************************/

use yii\helpers\Html;
use yii\widgets\LinkPager;

use common\models\Languages;

use common\widgets\Alert;

/* @var $this yii\web\View                         */
/* @var $dataProvider yii\data\ActiveDataProvider  */

// current page title, that will displayed in head tags
$this->title = __(  'Languages' ); ?>

    <!-- title section -->
    <div class="page-title">

        <div class="title_left">

            <h3>
                <?= $this->title ?>
            </h3>

        </div>

    </div>
    <!-- title section -->

    <div class="clearfix"></div>

    <div class="row">

        <!-- form section -->
        <div class="col-md-5 col-sm-5 col-xs-12">

            <div class="x_panel">

                <div class="x_title">

                    <h2>
                        <?= __(  'Add Language' ) ?>
                    </h2>

                    <div class="clearfix"></div>

                </div>

                <div class="x_content">

                    <?= $this->render( '_form', [
                        'model' => new Languages(),
                    ] ); ?>

                </div>

            </div>

        </div>
        <!-- form section -->

        <div class="col-md-7 col-sm-7 col-xs-12">

            <div class="x_panel">

                <div class="x_title">

                    <h2>
                        <?= __(  'Languages' ) ?>
                    </h2>

                    <div class="clearfix"></div>

                </div>

                <div class="x_content">

                    <!-- flash messages -->
                    <?= Alert::widget() ?>
                    <!-- flash messages -->

                    <!-- languages lists -->
                    <table class="table table-striped responsive-utilities jambo_table">

                        <thead>

                            <tr class="headings">

                                <th class="column-title">
                                    <?= __(  'Language Name' ) ?>
                                </th>

                                <th class="column-title">
                                    <?= __(  'Description' ) ?>
                                </th>

                                <th class="column-title no-link last">
                                    <span class="nobr">
                                        <?= __(  'Action' ) ?>
                                    </span>
                                </th>

                                <th class="bulk-actions" colspan="7">

                                    <a class="antoo">

                                        <?= __(  'Bulk Actions' ) ?> ( <span class="action-cnt"> </span> )

                                        <i class="fa fa-chevron-down"></i>

                                    </a>

                                </th>

                            </tr>

                        </thead>

                        <tbody>

                            <?php foreach ( $dataProvider->models as $model ): ?>

                                <tr class="even pointer">

                                    <td>
                                        <?= $model->name ?>
                                    </td>

                                    <td>
                                        <?= $model->description ?>
                                    </td>

                                    <td class="last">

                                        <?= Html::a( __(  'Edit' ), [
                                            'update',
                                            'id'    => $model->id
                                        ], [
                                            'class' => 'btn btn-primary btn-xs'
                                        ] ); ?>

                                        <?= Html::a( '<i class="fa fa-remove"> </i> ' . __(  'Delete' ), [
                                            'delete',
                                            'id'    => $model->id
                                        ], [
                                            'class' => 'btn btn-danger btn-xs',
                                            'data'  => [
                                                'confirm' => __(  'Do you really want to delete this item?' ),
                                                'method'  => 'post',
                                            ],
                                        ] ); ?>

                                    </td>

                                </tr>

                            <?php endforeach; ?>

                        </tbody>

                    </table>
                    <!-- languages lists -->

                    <!-- pagination section -->
                    <div class="pagination-container">

                        <?= LinkPager::widget( [
                            'pagination' => $dataProvider->pagination,
                        ] ); ?>

                    </div>
                    <!-- pagination section -->

                </div>

            </div>

        </div>

        <div class="clearfix"></div>

    </div>