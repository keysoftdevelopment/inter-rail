<?php /***************************************************************
 * 		 	   	    FORM SECTION FOR LANGUAGE PAGE                   *
 *********************************************************************/

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View             */
/* @var $model common\models\Languages */ ?>

<?php $form = ActiveForm::begin();

    if ( $model->isNewRecord ) {

        $form->action = Url::toRoute( [
			'/languages/create'
		] );

    } ?>

    <?= $form->field( $model, 'name' )
        -> textInput( [
            'class' => 'form-control'
        ] )
        -> label( __(  'Language Name' ) . " * :" );
    ?>

    <?= $form->field( $model, 'description' )
        -> textarea( [
            'class' => 'form-control lang_description',
        ] )
        -> label( __(  'Description' ) . ":" );
    ?>

    <div class="ln_solid"></div>

    <div class="col-sm-12 col-md-12 col-xs-12 text-center">

        <?= Html::submitButton( $model->isNewRecord
            ? __(  'Add Language' )
            : __(  'Update Language' ),
            [
                'class' => $model->isNewRecord
                    ? 'btn btn-primary'
                    : 'btn btn-primary'
            ]
        ); ?>

    </div>

<?php ActiveForm::end(); ?>
