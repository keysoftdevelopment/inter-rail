<?php /***************************************************************
 *	     		 	   	  CREATE LANGUAGE PART                       *
 *********************************************************************/

use yii\helpers\Html;

/* @var $this yii\web\View             */
/* @var $model common\models\Languages */

// current page title
$this->title = __(  'Create Languages' ); ?>

    <div class="row">

        <div class="col-md-5 col-sm-5 col-xs-12">

            <div class="x_panel">

                <!-- title section -->
                <div class="x_title">

                    <h2>
                        <?= $this->title ?>
                    </h2>

                    <div class="clearfix"></div>

                </div>
                <!-- title section -->

                <!-- form section -->
                <div class="x_content">

                    <?= $this->render( '_form', [
                        'model' => $model,
                    ] ); ?>

                </div>
                <!-- form section -->

            </div>

        </div>

        <!-- language thumbnail -->
        <div class="col-md-5 col-sm-5 col-xs-12">

            <h3>
                <?= __(  'Image' ) ?>
            </h3>

            <img class="category_image" src="<?= Yii::$app->common->thumbnail( $model->file_id ); ?>" />

        </div>
        <!-- language thumbnail -->

    </div>
