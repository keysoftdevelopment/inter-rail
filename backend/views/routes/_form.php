<?php /***************************************************************
 *      	 	   	    FORM SECTION FOR DIRECTION                   *
 *********************************************************************/

use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

use kartik\typeahead\Typeahead;

use common\models\PlacesRelation;
use common\models\Places;

use backend\widgets\GoogleMaps;

/* @var $model \common\models\Routes          */
/* @var $logistics \common\models\Logistics[] */

//the list of available cities
$cities = Places::find()
	-> where( [
		'!=', 'city', ''
	] )
    -> indexBy( 'id' )
    -> asArray()
	-> all();

//location details
$location_options = array_reduce( $cities, function( $filter, $place ) {

	if ( ! empty( $place ) )
		$filter[] = [
			'id'       => $place[ 'id' ],
			'name'     => $place[ 'city' ],
			'type'     => 'city',
			'syno_nym' => $place[ 'city' ]
		];

	return $filter;

}, [] );

if ( ! empty( $logistics ) ) {

    //places ids due to relations with logistics
	$place_relations = PlacesRelation::find()
		-> where( [
			'foreign_id' => ArrayHelper::getColumn(
                $logistics, 'id'
            )
		] )
		-> asArray()
		-> all();

	if ( ! empty( $place_relations ) ) {

	    //places details
		$places = Places::find()
			-> where( [
				'id' => ArrayHelper::getColumn(
                    $place_relations, 'place_id'
                )
			] )
            -> indexBy( 'id' )
			-> asArray()
			-> all();

		//routes lists
		$routes = array_reduce( $place_relations, function( $filter, $place ) use ( $places, $logistics ) {

		    if ( isset( $places[ $place[ 'place_id' ] ] ) ) {

				$filter[ $place[ 'foreign_id' ] ][ 'direction' ] = $places[ $place[ 'place_id' ] ][ 'type' ];
				$filter[ $place[ 'foreign_id' ] ][ 'duration' ] = isset( $logistics[ $place[ 'foreign_id' ] ] )
				    && (int)$logistics[ $place[ 'foreign_id' ] ][ 'duration' ] > 0
					? (float)( $logistics[ $place[ 'foreign_id' ] ][ 'duration' ] )/( 24 * 3600 )
					: 0;

				$filter[ $place[ 'foreign_id' ] ][ 'places' ][]  = [
					'id'           => $places[ $place[ 'place_id' ] ][ 'id' ],
					'title'        => $places[ $place[ 'place_id' ] ][ 'name' ],
					'name'         => $places[ $place[ 'place_id' ] ][ 'name' ],
					'lat'          => $places[ $place[ 'place_id' ] ][ 'lat' ],
					'lng'          => $places[ $place[ 'place_id' ] ][ 'lng' ],
					'city'         => $places[ $place[ 'place_id' ] ][ 'city' ],
					'country_code' => $places[ $place[ 'place_id' ] ][ 'country_code' ]
				];

            }

			return $filter;

        }, [] );

	}

}

//routes main form
$form = ActiveForm::begin( [
    'options'                => [
        'class'                 => 'form-horizontal form-label-left ajax-submitting-form need-locations',
        'id'                    => 'routes-form',
        'data-func'             => 'formCallback',
        'data-redirectonfinish' => Url::toRoute( [
            '/routes'
        ] )
    ],
    'enableAjaxValidation'   => false,
    'enableClientValidation' => true
] ); ?>

    <input type="hidden" id="logistics-type" class="form-control" value="sea" readonly="">

    <div class="x_panel">

        <div class="x_content">

            <!-- google map section -->
            <div class="general-map">

				<?= GoogleMaps::widget( [
					'view'   => $this,
                    'hideUI' => true,
                    'type'   => 'direction'
				] ); ?>

            </div>
            <!-- google map section -->

            <!-- routes location details -->
            <div class="route-locations">

                <h4>
                    <?= __( 'Transportation' ) ?>
                </h4>

                <div class="col-sm-6 col-md-6 col-xs-12" style="padding-left: 0">

                    <?= Typeahead::widget( [
                        'name'          => 'from',
                        'value'         => ! empty( $model->from ) && isset( $cities[ $model->from ] )
                            ? $cities[ $model->from ][ 'city' ]
                            : '' ,
                        'pluginOptions' => [
                            'highlight' => true,
                        ],
                        'dataset'       => [
                            [
                                'display'        => 'name',
                                'datumTokenizer' => "Bloodhound.tokenizers.obj.whitespace('syno_nym')",
                                'local'          => $location_options,
                            ]
                        ],
                        'pluginEvents' => [
                            "typeahead:select" => "function(e, suggestion) {
                                 $( '#place-from' ).val( suggestion.id ) 
                            }"
                        ],
                        'options' => [
                            'placeholder' => __( 'From' ),
                            'style'       => 'text-transform: uppercase; border-color: #efefef;'
                        ]
                    ] ); ?>

                </div>

                <div class="col-sm-6 col-md-6 col-xs-12" style="padding-right: 0">

                    <?= Typeahead::widget( [
                        'name'          => 'to',
                        'value'         => ! empty( $model->to ) && isset( $cities[ $model->to ] )
                            ? $cities[ $model->to ][ 'city' ]
                            : '' ,
                        'pluginOptions' => [
                            'highlight' => true,
                        ],
                        'dataset'       => [
                            [
                                'display'        => 'name',
                                'datumTokenizer' => "Bloodhound.tokenizers.obj.whitespace('syno_nym')",
                                'local'          => $location_options,
                            ]
                        ],
                        'pluginEvents' => [
                            "typeahead:select" => "function(e, suggestion) {
                                 $('#place-to').val( suggestion.id ) 
                            }"
                        ],
                        'options' => [
                            'placeholder' => __( 'To' ),
							'style'       => 'text-transform: uppercase; border-color: #efefef;'
                        ]
                    ] ); ?>

                </div>

            </div>
            <!-- routes location details -->

            <!-- routes additional details -->
            <div class="col-sm-12 col-md-12 col-xs-12">

                <div class="col-sm-6 col-md-6 col-xs-12" style="padding-left: 0px">

					<?= $form
						-> field( $model, 'duration' )
						-> textInput( [
							'id'          => 'duration',
							'placeholder' => __(  'Transit Time' ),
							'value'       => ! empty( $model->duration )
								? (  ( $model->duration / 24 ) / 3600 )
								: $model->duration
						] )
						-> label( __(  'Transit Time' ), [
							'class' => 'form-label'
						] );
					?>

                </div>

                <div class="col-sm-6 col-md-6 col-xs-12" style="padding-right: 0px">

                    <?= $form
                        -> field( $model, 'rate' )
                        -> textInput( [
                            'class'       => 'form-control',
                            'placeholder' => __( 'Approximate Sum' )
                        ] )
                        -> label( __( 'Approximate Sum' ), [
                            'class' => 'form-label'
                        ] );
                    ?>

                </div>

            </div>
            <!-- routes additional details -->

            <?= $form->field( $model, 'from' )
				-> hiddenInput( [
					'id' => 'place-from'
				] )
				-> label( false );
            ?>

			<?= $form->field( $model, 'to' )
				-> hiddenInput( [
					'id' => 'place-to'
				] )
				-> label( false );
			?>

            <div class="form-group">

                <div class="col-md-12 col-md-12 col-xs-12 text-center">

                    <input class="btn btn-primary" data-value="test" type="submit" value="<?= ( $model->isNewRecord
                        ? __(  'Publish' )
                        : __(  'Update' ) )
                    ?>">

                </div>

            </div>

        </div>

    </div>

    <!-- list of available routes -->
    <div class="x_panel no-top-border">

        <div class="x_title" style="padding: 10px;">

            <h2>
                <?= __(  'Logistics Route' ) ?>
            </h2>

            <ul class="nav navbar-right panel_toolbox slides" style="min-width: 20px;">

                <li>

                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>

                </li>

                <li class="dropdown">

                    <a href="#" class="dropdown-toggle add-item" id="add-route" data-toggle="dropdown" data-type="logistic" data-msg="<?= json_encode( [
						'option'    => __( 'Choose route' ),
						'no_result' => __( 'No result found' )
					] ) ?>">
                        +
                    </a>

                </li>

            </ul>

            <div class="clearfix"></div>

        </div>

        <div class="x_content" style="padding: 0px;">

            <div class="logistic-container" data-details="<?= htmlentities( json_encode( isset( $routes )
                ? $routes
                : []
            ) ) ?>">

                <?php if ( ! empty( $model->logistics ) ) {

                    foreach ( $model->logistics as $logistic ) { ?>

                        <?= $this->render( 'logistic', [
                            'model' => [
                                'value' => $logistic[ 'logistic_id' ]
                            ],
                            'logistics' => $logistics
                        ] ); ?>

                    <?php }

                } ?>

            </div>

            <a href="javascript:void(0)" id="show-on-map">
                <?= __( 'Show on map' ) ?>
            </a>

        </div>

    </div>
    <!-- list of available routes -->

<?php ActiveForm::end(); ?>

<script type="text/html" id="logistic-template">

	<?= $this->render( 'logistic', [
		'model' => [
            'value' => ''
		],
		'logistics' => $logistics
	] ); ?>

</script>
