<?php /***************************************************************
 *	 	       			   SINGLE LOGISTICS SECTION 				 *
 *		             	I.E. ALLOW CHOOSING LOGISTICS 			     *
 * 						     FROM REQUESTED LIST  					 *
 *********************************************************************/

use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var $model array  */
/* @var $logistics mixed */ ?>

<div class="option-section">

	<label for="logistic" class="col-md-1 col-sm-1 col-xs-12">
		<?= __( 'Route' ) ?>
	</label>

	<div class="col-md-11 col-sm-11 col-xs-12">

		<?= Html::dropDownList(
			'Routes[logistics][]',
			$model[ 'value' ],
			ArrayHelper::map(
				$logistics,
				'id',
				'route'
			), [
				'class'    => 'logistic-field select2_single form-control',
				'style'    => 'width:100%',
				'data-msg'         => json_encode( [
					'option'    => __( 'Choose logistic' ),
					'no_result' => __( 'No result found' )
				] )
			] )
		?>

	</div>

	<a href="javascript:void(0)" class="remove-current-option" data-parent="option-section" data-translations="<?= htmlentities( json_encode( [
		'title'   => __( 'CONFIRMATION MODULE' ),
		'message' => __( 'Do you really want to delete this option?' )
	] ) ) ?>">
		x
	</a>

</div>