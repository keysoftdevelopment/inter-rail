<?php /***************************************************************
 *	 	         THE LIST OF ALL AVAILABLE ROUTES PAGE               *
 *********************************************************************/

use yii\helpers\Html;
use yii\widgets\LinkPager;

/* @var $this yii\web\View                        */
/* @var $searchModel common\models\RoutesSearch   */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $locations array                          */

// current page title
$this->title = __(  'Routes' ); ?>

    <!-- title section -->
    <div class="page-title">

        <div class="title_left">

            <h3>
                <?= $this->title ?>
            </h3>

        </div>

    </div>
    <!-- title section -->

    <div class="clearfix"></div>

    <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12">

            <div class="x_panel">

                <div class="x_content">

                    <!-- routes lists -->
                    <table class="table table-striped projects">

                        <thead>

                            <tr>

                                <th style="width: 1%">
                                    #
                                </th>

                                <th>
									<?= __(  'Route' ) ?>
                                </th>

                                <th>
									<?= __(  'Charging' ) ?>
                                </th>

                                <th></th>

                            </tr>

                        </thead>

                        <tbody>

                            <?php if ( !empty( $dataProvider->models ) ) {

                                foreach ( $dataProvider->models as $model ) { ?>

                                    <tr>

                                        <td>
                                            #
                                        </td>

                                        <td class="route">

                                            <div class="route-polyline">

                                                <div class="route-from">

                                                    <div class="point-a"></div>

                                                    <label>

                                                        <?= isset( $locations[ $model->from ] )
                                                            ? $locations[ $model->from ][ 'city' ]
                                                            : __( 'Origin not chosen' )
                                                        ?>

                                                    </label>

                                                    <div class="point-b"></div>

                                                </div>

                                                <hr>

                                                <div class="route-to">

                                                    <div class="point-a"></div>

                                                    <label>

                                                        <?= isset( $locations[ $model->to ] )
															? $locations[ $model->to ][ 'city' ]
															: __( 'Destination not chosen' )
														?>

                                                    </label>

                                                    <div class="point-b"></div>

                                                </div>

                                            </div>

                                        </td>

                                        <td>
                                            <?= $model->rate ?>
                                        </td>

                                        <td class="text-center model-actions">

                                            <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                <i class="fa fa-ellipsis-v"></i>
                                            </a>

                                            <ul class="dropdown-menu pull-right">

                                                <li>

													<?= Html::a( '<i class="fa fa-pencil"></i> ' .  __(  'Edit' ), [
														'update',
														'id'    => $model->id
													] ); ?>

                                                </li>

                                                <li>

													<?= Html::a( '<i class="fa fa-trash"> </i> '. __(  'Delete' ), [
														'delete',
														'id' => $model->id
													], [
														'data'  => [
															'confirm' => __(  'Do you really want to delete this type?' ),
															'method'  => 'post',
														]
													] ); ?>

                                                </li>

                                            </ul>

                                        </td>

                                    </tr>

                                <?php }

                            } else {

                                echo '<tr>';

                                    echo '<td colspan="6" class="aligncenter">
                                        ' . __(  'There is no routes yet' ) . '
                                    </td>';

                                echo '</tr>';

                            } ?>

                        </tbody>

                    </table>
                    <!-- routes lists -->

                </div>

                <!-- pagination section -->
                <div class="pagination-container">

                    <?= LinkPager::widget( [
                        'pagination' => $dataProvider->pagination,
                    ] ); ?>

                </div>
                <!-- pagination section -->

            </div>

        </div>

    </div>