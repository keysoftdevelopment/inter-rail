<?php /***************************************************************
 *	   	      THE LIST OF ALL AVAILABLE RAILWAY CODES PAGE           *
 *********************************************************************/

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\LinkPager;

/* @var $this yii\web\View                            */
/* @var $searchModel common\models\RailwayCodesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider     */
/* @var $orders common\models\Orders[]                */

// current page title, that will displayed in head tags
$this->title = __(  'Railway Codes' ); ?>

    <div class="page-title">

        <!-- title section -->
        <div class="title_left">

            <h3>
                <?= $this->title ?>
            </h3>

        </div>
        <!-- title section -->

        <!-- search section -->
        <div class="title_right">

			<?= $this->render( '_search', [
				'model' => $searchModel
			] ); ?>

        </div>
        <!-- search section -->

    </div>

    <div class="clearfix"></div>

    <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12">

            <div class="x_panel">

                <div class="x_content">

                    <!-- railway codes lists -->
                    <table class="table table-striped">

                        <thead>

                            <tr>

                                <th style="width: 1%">
                                    #
                                </th>

                                <th>
									<?= __(  'Order Number' ) ?>
                                </th>

                                <th>
									<?= __(  'Country' ) ?>
                                </th>

                                <th>
									<?= __(  'Route' ) ?>
                                </th>

                                <th>
									<?= __(  'Forwarder' ) ?>
                                </th>

                                <th>
									<?= __(  'Code' ) ?>
                                </th>

                                <th>
									<?= __(  'Code Range From' ) ?>
                                </th>

                                <th>
									<?= __(  'Code Range To' ) ?>
                                </th>

                                <th>
									<?= __( 'Surcharge' ) ?>
                                </th>

                                <th>
									<?= __( 'Rate' ) ?>
                                </th>

                                <th>
									<?= __( 'Total' ) ?>
                                </th>

                            </tr>

                        </thead>

                        <tbody>

                            <?php if ( !empty( $dataProvider->models ) ) {

                                foreach ( $dataProvider->models as $model ) { ?>

                                    <tr>

                                        <td>
                                            #
                                        </td>

                                        <td>

											<?= Html::a( ! is_null( $orders[ $model->order_id ][ 'number' ] )
												? $orders[ $model->order_id ][ 'number' ]
												:  $orders[ $model->order_id ][ 'id' ],
												[
													'orders/update/',
													'id' => $model->order_id
												], [
													'role' => "button"
												] ) ?>

                                        </td>

                                        <td>
                                            <?= $model->country ?>
                                        </td>

                                        <td>
											<?= $model->route ?>
                                        </td>

                                        <td>
											<?= $model->forwarder ?>
                                        </td>

                                        <td>
											<?= $model->code ?>
                                        </td>

                                        <td>
											<?= $model->code_rg_start ?>
                                        </td>

                                        <td>
											<?= $model->code_rg_end ?>
                                        </td>

                                        <td>
											<?= $model->surcharge ?>
                                        </td>

                                        <td>
											<?= $model->rate ?>
                                        </td>

                                        <td>

											<?= number_format( (float)$model->surcharge + (float)$model->rate,
                                                2,
                                                '.',
                                                ''
                                            ); ?>

                                        </td>

                                    </tr>

                                <?php }
                            } else {

                                echo '<tr>';

                                    echo '<td colspan="6" class="text-center">
                                        ' . __(  'There is no railway codes yet' ) . '
                                    </td>';

                                echo '</tr>';

                            } ?>

                        </tbody>

                    </table>
                    <!-- railway codes lists -->

                </div>

                <!-- pagination section -->
                <div class="pagination-container">

                    <?= LinkPager::widget( [
                        'pagination' => $dataProvider->pagination,
                    ] ); ?>

                </div>
                <!-- pagination section -->

            </div>

        </div>

    </div>