<?php /***************************************************************
 *	     		 	   	  UPDATE SLIDER PART                         *
 *********************************************************************/

use yii\helpers\Html;

use common\models\Languages;

/* @var $this yii\web\View          */
/* @var $model \common\models\Posts */
/* @var $lang_id integer            */

// current page title
$this->title = __(  'Update Slider' ); ?>

    <div class="col-md-12 col-sm-12 col-xs-12 edit-container-block">

        <div class="page-title">

            <!-- title section -->
            <div class="title_left">

                <h3>
                    <?= $this->title ?>
                </h3>

            </div>
            <!-- title section -->

            <!-- languages section -->
            <div class="title_right">

                <div class="pull-right posts_language text-right">

                    <span>
                        <?= __(  'Choose Language' ) ?>:
                    </span>

					<?php foreach ( Languages::find()->all() as $lang ) { ?>

						<?= Html::a( $lang->description, [
							'/sliders/update',
							'id'      => $model->id,
							'lang_id' => $lang->id
						], [
							'class' => (int)$lang_id == $lang->id
								? 'btn btn-primary btn-xs'
								: 'btn btn-default btn-xs'
						] ) ?>

					<?php } ?>

                </div>

            </div>
            <!-- languages section -->

        </div>

        <!-- form section -->
        <div class="col-md-12 col-sm-12 col-xs-12 create-item-form">

            <?= $this->render( '_form', [
                'model'   => $model,
                'lang_id' => $lang_id
            ] ); ?>

        </div>
        <!-- form section -->

    </div>