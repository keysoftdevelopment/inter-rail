<?php /***************************************************************
 *          		 	   	  FILE UPLOAD SECTION                    *
 *********************************************************************/

use yii\widgets\ActiveForm;

$form = ActiveForm::begin( [
    'id'      => 'upload-sliders-form',
    'options' => [
        'enctype' => 'multipart/form-data',
        'class'   => 'dropzone',
        'style'   => "background-color: #4d494a; color: white;"
    ]
] );

ActiveForm::end(); ?>