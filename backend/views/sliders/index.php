<?php /***************************************************************
 *	 	   	    THE LIST OF ALL AVAILABLE SLIDERS PAGE               *
 *********************************************************************/

use yii\helpers\Html;
use yii\widgets\LinkPager;

use common\widgets\Alert;

/* @var $this yii\web\View                        */
/* @var $searchModel common\models\PostsSearch    */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $thumbnails common\models\Files[]         */

// current page title, that will displayed in head tags
$this->title = __(  'Sliders' ); ?>

    <!-- title section -->
    <div class="page-title">

        <div class="title_left">

            <h3>
                <?= $this->title ?>
            </h3>

        </div>

    </div>
    <!-- title section -->

    <div class="clearfix"></div>

    <div class="row">

        <div class="col-md-12 col-xs-12">

            <div class="x_panel">

                <div class="x_content">

                    <!-- flash messages -->
                    <?= Alert::widget() ?>
                    <!-- flash messages -->

                    <!-- sliders list -->
                    <div class="col-sm-12 col-md-12 col-xs-12">

                        <?php if ( !empty( $dataProvider->models ) ) {

                            foreach ( $dataProvider->models as $model ) {

                                //current slider thumbnail
                                $thumbnail = isset( $thumbnails[ 'files' ][ $thumbnails[ 'ids' ][ $model->id ] ] )
                                    ? Yii::getAlias( '@frontend_link' ) . $thumbnails[ 'files' ][ $thumbnails[ 'ids' ][ $model->id ] ][ 'guide' ]
                                    : Yii::getAlias( '@web' ) . '/images/image-not-found.png'; ?>

                                <div class="col-sm-3 col-md-3 col-xs-12">

                                    <div class="thumbnail media_file">

                                        <div class="image view view-first">

                                            <img src="<?= $thumbnail ?>" alt="image"/>

                                            <div class="mask">

                                                <p>
                                                    <?= $model->title ?>
                                                </p>

                                                <div class="tools tools-bottom">

                                                    <?= Html::a( '<i class="fa fa-pencil"></i>', [
                                                        'update',
                                                        'id'   => $model->id
                                                    ], [
                                                        'role' => "button"
                                                    ] ); ?>

                                                    <?= Html::a( '<i class="fa fa-times"></i>', [
                                                        'delete',
                                                        'id'   => $model->id
                                                    ], [
                                                        'data' => [
                                                            'confirm' => __(  'Do you really want to delete this slide?' ),
                                                            'method'  => 'post',
                                                        ],
                                                    ] ); ?>

                                                </div>

                                            </div>

                                        </div>

                                        <div class="caption">

											<?= Html::a( $model->title, [
												'update',
												'id' => $model->id
											], [
												'role' => "button"
											] ) ?>

                                        </div>

                                    </div>

                                </div>

                            <?php }

                        } else {
                            echo __(  'There is no sliders yet' );
                        } ?>

                        <!-- pagination section -->
                        <div class="pagination-container">

                            <?= LinkPager::widget( [
                                'pagination' => $dataProvider->pagination,
                            ] ); ?>

                        </div>
                        <!-- pagination section -->

                    </div>
                    <!-- sliders list -->

                </div>

            </div>

        </div>

    </div>