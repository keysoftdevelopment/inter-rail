<?php /***************************************************************
 * 	    	 	   	    FORM SECTION FOR SLIDER                      *
 *********************************************************************/

use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

use common\models\Languages;

use backend\widgets\MediaPopup;

/* @var $this yii\web\View         */
/* @var $model common\models\Posts */
/* @var $lang_id integer           */

//main slider form
$form = ActiveForm::begin( [
    'options'                => [
        'class'                 => 'form-horizontal form-label-left ajax-submitting-form need-image',
        'id'                    => 'slider-form',
        'data-func'             => 'formCallback',
        'data-redirectonfinish' => Url::toRoute( [
            '/sliders'
        ] )
    ],
    'enableAjaxValidation'   => false,
    'enableClientValidation' => true
] ); ?>

    <?= $form
        -> field( $model, 'type' )
        -> hiddenInput( [
            'readonly' => true,
            'value'    => 'slider'
        ] )
        -> label( false );
    ?>

    <div class="x_panel">

        <div class="x_content">

            <div class="col-sm-6 col-md-6 col-xs-6">

                <?= $form
                    -> field( $model, 'title' )
                    -> textInput( [
                        'id'          => 'title',
                        'placeholder' => __(  'Title' )
                    ] )
                    -> label( false );
                ?>

            </div>

            <div class="col-sm-6 col-md-6 col-xs-6">

                <?= $form
                    -> field( $model, 'lang_id' )
                    -> dropDownList( ArrayHelper::map(
                        Languages::find()->all(),
                        'id',
                        'description'
                    ), [
						'options' => [
							$lang_id => [
								'Selected'=>'selected'
							]
						]
					], [
                        'prompt' => __(  'Choose Language' )
                    ] )
                    -> label( false );
                ?>

            </div>

        </div>

    </div>

    <!-- slides section -->
    <div class="x_panel no-top-border">

        <div class="x_title">

            <h2>
				<?= __(  'Slides' ) ?>
            </h2>

            <ul class="nav navbar-right panel_toolbox slides" style="min-width: 20px;">

                <li>

                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>

                </li>

				<?php if ( (int)$lang_id > 0
					&& $model->lang_id == $lang_id
					|| (int)$lang_id == 0
				) { ?>

                    <li class="dropdown">

                        <a href="#" class="dropdown-toggle add-item" id="add-slide" data-toggle="dropdown" data-type="slider" data-translations="<?= htmlentities( json_encode( [
							'title'   => __( 'Slider was added successfully' ) . ' !!!',
							'message' => __( 'Slider was added successfully' ) . '. ' . __( 'Please check the bottom of current' ) . ' ' . __( 'slider options list' ),
							'type'    => 'success'
						] ) ) ?>">
                            +
                        </a>

                    </li>

				<?php } ?>

            </ul>

            <div class="clearfix"></div>

        </div>

        <div class="x_content">

            <div class="col-sm-12 col-md-12 col-xs-12 slider-container">

                <?php if ( ! empty( $model->description ) ) {

                    $slides = json_decode( $model->description );

                    if ( isset( $slides->title ) ) {

                        $thumbnails = Yii::$app->common->thumbnails( array_map(
							function ( $slide ) {
								return is_string( $slide )
                                    ? trim( $slide )
                                    : $slide;
							},
							$slides->file_id
						) );

                        foreach ( $slides->title as $key => $slide ) { ?>

                            <?= $this->render( 'form-sections/slide', [
                                'lang_id' => $lang_id,
                                'model'   => [
                                    'lang_id'   => $model->lang_id,
                                    'thumbnail' => isset( $thumbnails[ trim( $slides->file_id[ $key ] ) ] )
                                        ? $thumbnails[ trim( $slides->file_id[ $key ] ) ][ 'guide' ]
                                        : Yii::getAlias( '@web' ) . '/images/image-not-found.png',
                                    'file_id'         => $slides->file_id[ $key ],
                                    'title'           => $slide,
                                    'btn_name'        => $slides->btn_name[ $key ],
                                    'link'            => $slides->link[ $key ],
									'desc'            => $slides->desc[ $key ],
									'additional_desc' => $slides->additional_desc[ $key ],
									'additional_link' => $slides->additional_link[ $key ],
                                ]
                            ] ); ?>

                        <?php }

                    }

                } ?>

            </div>

            <script type="text/html" id="slider-template">

				<?= $this->render( 'form-sections/slide', [
					'lang_id' => $lang_id,
					'model'   => [
						'lang_id' => $model->lang_id
					]
				] ); ?>

            </script>

            <div class="col-md-12 col-md-12 col-xs-12 text-center" style="margin-top: 10px">

                <input class="btn btn-primary" data-value="test" type="submit" value="<?= $model->isNewRecord
                    ? __(  'Publish' )
                    : __(  'Update' )
                ?>">

            </div>

        </div>

    </div>
    <!-- slides section -->

<?php ActiveForm::end(); ?>

<!-- media popup window -->
<?= MediaPopup::widget( [
	'formType'   => 'slider-modal',
	'uploadForm' => $this->render( 'upload' )
] ) ?>
<!-- media popup window -->
