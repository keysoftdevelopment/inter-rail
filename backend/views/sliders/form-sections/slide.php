<?php /***************************************************************
 *	 	       			     SINGLE SLIDE SECTION 				     *
 *		             	I.E. ALLOW CONTROLLING SLIDE  				 *
 * 						          ELEMENTS  					     *
 *********************************************************************/

/* @var $model array   */
/* @var $lang_id mixed */ ?>

<div class="slide col-sm-4 col-md-4 col-xs-12">

	<div class="slide-image">

		<img class="center-block" src="<?= isset( $model[ 'thumbnail' ] )
            ? Yii::getAlias( '@frontend_link' ) . $model[ 'thumbnail' ]
            : Yii::getAlias( '@web' ) . '/images/image-not-found.png'
        ?>">


            <a href="#slider-modal" class="slide-modal-btn text-center" data-thumbnail-index="0" data-toggle="modal" data-target="#slider-modal">
                <?= __( 'Select Image' ) ?>
            </a>

            <input type="hidden" name="slide[file_id][]" class="file-id" value="<?= isset( $model[ 'file_id' ] )
                ? $model[ 'file_id' ]
                : ''
            ?>"/>

		<?php if ( (int)$lang_id > 0
			&& $model[ 'lang_id' ]  == $lang_id || (int)$lang_id == 0
		) { ?>

            <div class="remove-slide remove-current-option" data-parent="slide" data-translations="<?= htmlentities( json_encode( [
				'title'   => __( 'CONFIRMATION MODULE' ),
				'message' => __( 'Do you really want to delete this option?' )
			] ) ) ?>">
                x
            </div>

        <?php } ?>

	</div>

	<div class="slide-caption">

        <div class="form-group">

            <input name="slide[title][]" type="text" class="title form-control" placeholder="<?= __( 'Title' ) ?>" value="<?= isset( $model[ 'title' ] )
                ? $model[ 'title' ]
                : ''
            ?>" />

        </div>

        <div class="form-group">

            <div class="col-sm-6 col-md-6 col-xs-12">

                <input name="slide[btn_name][]" type="text" class="link-txt form-control" placeholder="<?= __( 'Button Name' ) ?>" value="<?= isset( $model[ 'btn_name' ] )
					? $model[ 'btn_name' ]
					: ''
				?>" />

            </div>

            <div class="col-sm-6 col-md-6 col-xs-12">

                <input name="slide[link][]" type="text" class="link form-control" placeholder="<?= __( 'Link' ) ?>" value="<?= isset( $model[ 'link' ] )
					? $model[ 'link' ]
					: ''
				?>" />

            </div>

        </div>

        <div class="form-group">
            <textarea name="slide[desc][]" class="desc form-control" placeholder="<?= __( 'Description' ) ?>"><?= isset( $model[ 'desc' ] ) ? $model[ 'desc' ] : '' ?></textarea>
        </div>

        <div class="form-group">
            <textarea name="slide[additional_desc][]" class="additional-desc form-control" placeholder="<?= __( 'Description Above Icon' ) ?>"><?= isset( $model[ 'additional_desc' ] ) ? $model[ 'additional_desc' ] : '' ?></textarea>
        </div>

        <div class="form-group">

            <input type="text" name="slide[additional_link][]" class="additional-link form-control" placeholder="<?= __( 'Link' ) ?>" value="<?= isset( $model[ 'additional_link' ] )
				? $model[ 'additional_link' ]
				: ''
			?>" />

        </div>

	</div>

</div>