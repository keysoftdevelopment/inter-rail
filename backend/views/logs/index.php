<?php /***************************************************************
 *	  	   	                THE LIST OF LOGS PAGE                    *
 *********************************************************************/

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;

/* @var $this yii\web\View                         */
/* @var $searchModel common\models\LogisticsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider  */
/* @var $orders common\models\Orders[]             */
/* @var $users common\models\User[]                */

// current page title, that will displayed in head tags
$this->title = __( ucfirst( 'Logs' ) ); ?>

    <div class="page-title">

        <!-- title section -->
        <div class="title_left">

            <h3>
                <?= $this->title ?>
            </h3>

        </div>
        <!-- title section -->

    </div>

    <div class="clearfix"></div>

    <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12">

            <!-- filters section -->
            <div class="main-filters-container">

				<?= $this->render( 'filters', [
					'searchModel' => $searchModel,
                    'users'       => $users
				] ) ?>

            </div>
            <!-- filters section -->

            <div class="x_panel">

                <!-- logs lists -->
                <div class="x_content">

                    <table class="table table-striped">

                        <thead>

                            <tr>

                                <th style="width: 1%">
                                    #
                                </th>

                                <th>
									<?= __(  'Order Number' ) ?>
                                </th>

                                <th>
									<?= __(  'Customer' ) ?>
                                </th>

                                <th>
									<?= __( 'Before Changes' ) ?>
                                </th>

                                <th>
									<?= __( 'After Changes' ) ?>
                                </th>

                            </tr>

                        </thead>

                        <tbody>

						<?php if ( !empty( $dataProvider->models ) ) {

							foreach ( $dataProvider->models as $model ) {

							    if( isset( $orders[ $model->foreign_id ] ) ) {

                                    //logs details
                                    $action = ! empty( $model->action )
                                        ? json_decode( $model->action )
                                        : $model->action; ?>

                                    <tr>

                                        <td>
                                            #
                                        </td>

                                        <td>

                                            <?= Html::a( ! is_null( $orders[ $model->foreign_id ][ 'number' ] )
												? $orders[ $model->foreign_id ][ 'number' ]
												:  $orders[ $model->foreign_id ][ 'id' ],
												[
													'orders/update/',
													'id' => $model->foreign_id
												], [
                                                'role' => "button"
                                            ] ) ?>

                                        </td>

                                        <td>

                                            <?= isset( $users[ $model->changed_by ] )
                                                ? ( ! empty( $users[ $model->changed_by ][ 'first_name' ] ) || ! empty( $users[ $model->changed_by ][ 'last_name' ] )
                                                    ? $users[ $model->changed_by ][ 'first_name' ] . ' ' . $users[ $model->changed_by ][ 'last_name' ]
                                                    : $users[ $model->changed_by ][ 'username' ]
                                                )
                                                : __( 'No user' )
                                            ?>

                                        </td>

                                        <td>

                                            <ul class="changes-lists">

                                                <?php foreach ( $action as $key => $value ) { ?>

                                                    <li>

                                                        <label>
                                                            <?= __( ucfirst( $key ) ); ?>
                                                        </label>

                                                        <span>

                                                            <?= is_object( $value->old )
																? array_reduce( (array)$value->old, function( $result, $action ) {

																	if( is_array( $action ) ) {
																		$result .= implode( $action, ',' );
																	} else {
																		$result .= ' ' . $action . ',';
																	}

																	return $result;

																}, '' )
																: $value->old
															?>

                                                        </span>

                                                    </li>

                                                <?php } ?>

                                            </ul>

                                        </td>

                                        <td>

                                            <ul class="changes-lists">

												<?php foreach ( $action as $key => $value ) { ?>

                                                    <li>

                                                        <label>
															<?= __( ucfirst( $key ) ); ?> :
                                                        </label>

                                                        <span>

                                                            <?= is_object( $value->new )
                                                                ? array_reduce( (array)$value->new, function( $result, $action ) {

                                                                    if( is_array( $action ) ) {
																		$result .= implode( $action, ',' );
                                                                    } else {
																		$result .= ' ' . $action . ',';
                                                                    }

                                                                    return $result;

                                                                }, '' )
                                                                : $value->new
                                                            ?>

                                                        </span>

                                                    </li>

												<?php } ?>

                                            </ul>

                                        </td>

                                    </tr>

							<?php }

							}

						} else {

							echo '<tr>';

							echo '<td colspan="8" class="text-center">
                                        ' . __(  'There is no logs yet' ) . '
                                    </td>';

							echo '</tr>';

						} ?>

                        </tbody>

                    </table>

                </div>
                <!-- logs lists -->

                <!-- pagination section -->
                <div class="pagination-container">

                    <?= LinkPager::widget( [
                        'pagination' => $dataProvider->pagination,
                    ] ); ?>

                </div>
                <!-- pagination section -->

            </div>

        </div>

    </div>