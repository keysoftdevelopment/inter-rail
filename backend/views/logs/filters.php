<?php /***************************************************************
 *         		 	   	   LOGS FILTERS SECTION                      *
 *********************************************************************/

/* @var $searchModel common\models\LogsSearch */
/* @var $users common\models\User[]           */


use kartik\date\DatePicker;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

use common\models\Orders;

//get request short option
$get_request = Yii::$app->getRequest();
$form        = ActiveForm::begin( [
	'id'      => 'main-filter-form',
	'method'  => 'get',
	'action'  => Url::to( [
		'/logs'
	] )
] ); ?>

    <ul class="list-filters">

        <li class="filter-element">

            <div class="form-group">

				<?= $form
                    -> field( $searchModel, 'changed_by' )
                    -> dropDownList( ArrayHelper::map( $users, 'id', function( $model ) {

						return ! empty( $model[ 'first_name'] ) || ! empty( $model[ 'last_name'] )
                            ? $model[ 'first_name' ] . ' ' . $model[ 'last_name' ]
                            : $model[ 'username' ];

					} ), [
						'prompt' => __(  'Select customer' ),
						'class'  => 'form-control'
					] )
                    -> label( false );
				?>

            </div>

        </li>

        <li class="filter-element">

            <div class="form-group">

				<?= $form
					-> field( $searchModel, 'foreign_id' )
					-> dropDownList( ArrayHelper::map( Orders::find()
                        -> where( [
                            'isDeleted' => false
                        ] )
						-> andWhere( [
							'NOT IN', 'status', [
								'rejection', 'completion'
							]
						] )
                        -> asArray()
                        -> all(), 'id', function( $model ) {

                            return ! empty( $model[ 'number'] )
                                ? $model[ 'number']
                                : $model[ 'id' ];

					} ), [
						'prompt' => __(  'Select Order' ),
						'class'  => 'form-control select2_single',
						'data-msg'         => json_encode( [
							'option'    => __( 'Select Order' ),
							'no_result' => __( 'No result found' )
						] )
					] )
					-> label( false );
				?>

            </div>

        </li>

        <li class="filter-element" style="padding-right: 0px;">

			<?= DatePicker::widget( [
				'name'    => 'date_from',
				'type'    => DatePicker::TYPE_INPUT,
				'value'   => ! is_null( $get_request->getQueryParam('date_from' ) )
					? date('d.m.Y', strtotime( $get_request->getQueryParam('date_from' ) ) )
					: date('d.m.Y' ) ,
				'options' => [
					'placeholder' => __(  'Date From' ),
					'class'       => 'order-filter-datetime form-control'
				],
				'pluginOptions' => [
					'autoclose' => true,
					'format'    => 'dd.mm.yyyy',
					'weekStart' => '1'
				]
			] ); ?>

        </li>

        <li class="filter-element" style="padding-right: 0px;">

			<?= DatePicker::widget( [
				'name'    => 'date_to',
				'type'    => DatePicker::TYPE_INPUT,
				'value'   => ! is_null( $get_request->getQueryParam('date_to' ) )
					? date('d.m.Y', strtotime( $get_request->getQueryParam('date_to' ) ) )
					: date('d.m.Y' ) ,
				'options' => [
					'placeholder' => __(  'Date To' ),
					'class'       => 'order-filter-datetime form-control'
				],
				'pluginOptions' => [
					'autoclose' => true,
					'format'    => 'dd.mm.yyyy',
					'weekStart' => '1'
				]
			] ); ?>

        </li>

        <li class="filter-element">

            <button class="btn btn-default">
				<?= __(  'Filter' ); ?>
            </button>

        </li>

    </ul>

<?php ActiveForm::end(); ?>