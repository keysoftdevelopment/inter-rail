<?php /***************************************************************
 *      	 	   	      FORM SECTION FOR PLACES                    *
 *********************************************************************/

use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

use backend\widgets\GoogleMaps;

/* @var $this yii\web\View          */
/* @var $model common\models\Places */

//the list of logistics types
$logistic_types = Yii::$app->common->logisticTypes( [
	'serialize' => true
] );

//places main form
$form = ActiveForm::begin( [
    'options'                => [
        'class'                 => 'form-horizontal form-label-left ajax-submitting-form',
        'id'                    => 'place-form',
        'data-func'             => 'formCallback',
        'data-redirectonfinish' => Url::toRoute( [
            '/places'
        ] )
    ],
    'enableAjaxValidation'   => false,
    'enableClientValidation' => true
] ); ?>

    <div class="x_panel">

        <div class="x_content">

            <div class="form-group">

                <div class="col-sm-6 col-md-6 col-xs-12">

					<?= $form
						-> field( $model, 'name' )
						-> textInput( [
							'id'          => 'name',
							'placeholder' => __(  'Name' )
						] )
						-> label( __(  'Name' ) );
					?>

                </div>

                <div class="col-sm-6 col-md-6 col-xs-12">

					<?= $form
						-> field( $model, 'type' )
						-> dropDownList( ! empty( $logistic_types )
                            ? ArrayHelper::map(
                                $logistic_types,
                                'slug',
                                'name'
                            )
                            : [], [
							'prompt'   => __(  'Choose Type' ),
                            'disabled' => $model->isNewRecord
                                ? false
                                : true
						] )
						-> label( __(  'Type' ) );
					?>

                </div>

            </div>

            <div class="form-group">

                <div class="col-sm-2 col-md-2 col-xs-12">

					<?= $form
						-> field( $model, 'country_code' )
						-> dropDownList( [], [
							'class'            => 'form-control select2_single countrypicker',
							'style'            => 'width: 100%;',
                            'data-live-search' => 'true',
                            'data-default'     => $model->country_code,
							'data-msg'         => json_encode( [
								'option'    => __( 'Choose country code' ),
								'no_result' => __( 'No result found' )
							] )
						] )
						-> label( __(  'Country Code' ) );
					?>

                </div>

                <div class="col-sm-2 col-md-2 col-xs-12">

					<?= $form
						-> field( $model, 'country' )
						-> textInput( [
							'id'          => 'country',
							'placeholder' => __(  'Country' )
						] )
						-> label( __(  'Country' ) );
					?>

                </div>

                <div class="col-sm-2 col-md-2 col-xs-12">

					<?= $form
						-> field( $model, 'city' )
						-> textInput( [
							'id'          => 'city',
							'placeholder' => __(  'City' )
						] )
						-> label( __(  'City' ) );
					?>

                </div>

                <div class="col-sm-3 col-md-3 col-xs-12">

					<?= $form
						-> field( $model, 'district' )
						-> textInput( [
							'id'          => 'district',
							'placeholder' => __(  'District' )
						] )
						-> label( __(  'District' ) );
					?>

                </div>

                <div class="col-sm-3 col-md-3 col-xs-12">

					<?= $form
						-> field( $model, 'street' )
						-> textInput( [
							'id'          => 'address',
							'placeholder' => __(  'Address' )
						] )
						-> label( __(  'Address' ) );
					?>

                </div>

            </div>

            <!-- google maps section -->
            <div class="general-map">

				<?= GoogleMaps::widget( [
					'view'  => $this,
					'model' => $model
				] ); ?>

            </div>
            <!-- google maps section -->

			<?= $form->field( $model, 'lat' )
				-> hiddenInput( [
					'id' => 'lat-field'
				] )
				-> label( false );
			?>

			<?= $form->field( $model, 'lng' )
				-> hiddenInput( [
					'id' => 'lng-field'
				] )
				-> label( false );
			?>

            <div class="form-group">

                <div class="col-md-12 col-md-12 col-xs-12 text-center">

                    <input class="btn btn-primary" data-value="test" type="submit" value="<?= ( $model->isNewRecord
                        ? __(  'Publish' )
                        : __(  'Update' ) )
                    ?>">

                </div>

            </div>

        </div>

    </div>

<?php ActiveForm::end(); ?>