<?php /***************************************************************
 *	            THE LIST OF ALL AVAILABLE PLACES PAGE                *
 *********************************************************************/

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;

/* @var $this yii\web\View                         */
/* @var $searchModel common\models\PostsSearch     */
/* @var $dataProvider yii\data\ActiveDataProvider  */

// current page title
$this->title = __(  'Places' ); ?>

<div class="page-title">

    <!-- title section -->
	<div class="title_left">

		<h3>
			<?= $this->title ?>
		</h3>

        <a href="<?= Url::to( [
			'places/create'
		] ) ?>">
            <i class="fa fa-plus-circle"></i>
			<?= __( 'New Place' ) ?>

        </a>

	</div>
    <!-- title section -->

    <!-- logistic types -->
    <div class="title_right">

        <div class="places-types pull-right">

            <ul>

                <?php foreach ( Yii::$app->common->logisticTypes() as $type ) {

                    if ( ! empty( $type ) ) { ?>

                        <li>

                            <a href="<?= Url::to( [
                                '/places',
                                'logistic_type' => strtolower( $type[ 'name' ] )
                            ] ) ?>" class="btn btn-primary btn-xs <?= $searchModel->type == strtolower( $type[ 'name' ] ) ? 'active' : '' ?>">
                                <?= __( $type[ 'name' ] ) ?>
                            </a>

                        </li>

                    <?php } ?>

                <?php } ?>

            </ul>

        </div>

    </div>
    <!-- logistic types -->

</div>

<div class="clearfix"></div>

<div class="row">

	<div class="col-md-12 col-sm-12 col-xs-12">

		<div class="x_panel">

			<div class="x_content">

                <!-- places lists -->
				<table class="table table-striped projects">

					<thead>

                        <tr>

                            <th>
                                #
                            </th>

                            <th>
                                <?= __(  'Name' ) ?>
                            </th>

                            <th>
                                <?= __(  'Location' ) ?>
                            </th>

                            <th>
                                <?= __(  'Type' ) ?>
                            </th>

                            <th></th>

                        </tr>

					</thead>

					<tbody>

                        <?php if ( !empty( $dataProvider->models ) ) {

                            foreach ( $dataProvider->models as $model ) { ?>

                                <tr>

                                    <td>
                                        #
                                    </td>

                                    <td>

                                        <?= Html::a( $model->name, [
                                            'update',
                                            'id' => $model->id
                                        ] ) ?>

                                    </td>

                                    <td>
                                        <?= $model->country ?>, <?= $model->city ?> <?= $model->district ?>
                                    </td>

                                    <td>

                                        <?= ! empty( $model->type )
                                            ? __( $model->type )
                                            : __( 'Type not chosen yet' )
                                        ?>

                                    </td>

                                    <td class="text-center model-actions">

                                        <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                            <i class="fa fa-ellipsis-v"></i>
                                        </a>

                                        <ul class="dropdown-menu pull-right">

                                            <li>

                                                <?= Html::a( '<i class="fa fa-pencil"></i> ' .  __(  'Edit' ), [
                                                    'update',
                                                    'id'    => $model->id
                                                ] ); ?>

                                            </li>

                                            <li>

                                                <?= Html::a( '<i class="fa fa-trash"> </i> '. __(  'Delete' ), [
                                                    'delete',
                                                    'id' => $model->id
                                                ], [
                                                    'data'  => [
                                                        'confirm' => __(  'Do you really want to delete this type?' ),
                                                        'method'  => 'post',
                                                    ]
                                                ] ); ?>

                                            </li>

                                        </ul>

                                    </td>

                                </tr>

                            <?php }

                        } else {

                            echo '<tr>';

                                echo '<td colspan="5" class="aligncenter">
                                    ' . __(  'There is no places yet' ) . '
                                </td>';

                            echo '</tr>';

                        } ?>

					</tbody>

				</table>
                <!-- places lists -->

			</div>

            <!-- pagination section -->
			<div class="pagination-container">

				<?= LinkPager::widget( [
					'pagination' => $dataProvider->pagination,
				] ); ?>

			</div>
            <!-- pagination section -->

		</div>

	</div>

</div>