<?php /***************************************************************
 *	    		 	   	  UPDATE ITEMS IN MENU                       *
 *********************************************************************/

use yii\helpers\Html;

use common\models\Languages;

/* @var $this yii\web\View            */
/* @var $model common\models\Settings */
/* @var $pages common\models\Posts    */

// current page title, that will displayed in head tags
$this->title = __(  'Menu' );  ?>

    <div class="col-sm-12 col-xs-12">

        <div class="row">

            <!-- title section -->
            <div class="page-title">

                <div class="title_left">

                    <h3>
                        <?= $this->title ?>
                    </h3>

                </div>
                <!-- title section -->

                <!-- languages lists -->
                <div class="title_right">

                    <div class="pull-right posts_language text-right">

                        <span>
                            <?= __(  'Choose Language' ) ?>:
                        </span>

						<?php foreach ( Languages::find()->all() as $lang ) { ?>

							<?= Html::a( $lang->description, [
								'/settings/create',
								'lang_id' => $lang->id
							], [
								'class' => $model->lang_id == $lang->id
                                    ? 'btn btn-primary btn-xs'
                                    : 'btn btn-default btn-xs'
							] ) ?>

						<?php } ?>

                    </div>

                </div>
                <!-- languages lists -->

            </div>

        </div>

    </div>

    <div class="row">

        <!-- menu details -->
        <div class="col-md-12 col-xs-12 menu-page" style="padding: 0px">

            <div class="x_panel">

                <div class="x_content">

                    <div class="col-md-4 col-sm-4 col-xs-12">

                        <div id="nav-menu-body">

                            <!-- pages list -->
                            <div class="panel panel-primary">

                                <?= $this->render( 'menu-sections/pages', [
                                    'pages' => $pages,
                                ] ); ?>

                            </div>
                            <!-- pages list -->

                            <!-- custom links -->
                            <div class="panel panel-primary">

                                <?= $this->render( 'menu-sections/custom-links', [
                                    'pages' => $pages,
                                ] ); ?>

                            </div>
                            <!-- custom links -->

                        </div>

                    </div>

                    <!-- form section -->
                    <div class="col-md-8 col-sm-8 col-xs-12 update-option-form" style="padding: 0px">

                        <div id="nav-menu-header-1" style="padding-bottom: 2px;">

                            <?= $this->render( 'menu-sections/_form', [
                                'model' => $model,
                            ] ); ?>

                        </div>

                        <div class="dd menu-items" style="padding: 10px;">

                            <li class="dd-item-blueprint">

                                <button class="collapse" data-action="collapse" type="button" style="display: none;">
                                    –
                                </button>

                                <button class="expand" data-action="expand" type="button" style="display: none;">
                                    +
                                </button>

                                <div class="dd-handle dd3-handle">
                                    <?= __(  'Drag' ) ?>
                                </div>

                                <div class="dd3-content">

                                    <span class="item-name">
                                        [item_name]
                                    </span>

                                    <div class="dd-button-container">

                                        <button class="custom-button-example" data-toggle="modal" data-target="#backend-option-modal">
                                            &#x270E;
                                        </button>

                                        <button class="item-remove" data-confirm-class="item-remove-confirm">
                                            &times;
                                        </button>

                                    </div>

                                    <div class="dd-edit-box" style="display: none;">

                                        <input type="text" name="name" autocomplete="off" placeholder="<?= __( 'Menu Item' ) ?>" data-placeholder="<?= __( 'Any nice idea for the title?' ) ?>">

                                        <input type="text" name="id" hidden>

                                        <input type="text" name="link">

                                        <i class="end-edit">
                                            <?= __(  'save') ?>
                                        </i>

                                    </div>

                                </div>

                            </li>

                            <ol class="dd-list"></ol>

                        </div>

                    </div>
                    <!-- form section -->

                </div>

            </div>

        </div>
        <!-- menu details -->

    </div>