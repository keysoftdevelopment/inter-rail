<?php /***************************************************************
 *         	    THE LIS OF ALL AVAILABLE PAGES SECTION               *
 *********************************************************************/

use yii\widgets\ListView;

/* @var $this yii\web\View         */
/* @var $pages common\models\Posts */ ?>

    <div class="x_panel menu-items lang_menu_settings">

        <div class="x_title">

            <h2>
                <?= __(  'Pages' ) ?>
            </h2>

            <ul class="nav navbar-right panel_toolbox" style="min-width: 20px;">

                <li>
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                </li>

            </ul>

            <div class="clearfix"></div>

        </div>

        <div class="x_content">

            <?= ListView::widget( [
                'dataProvider' => $pages,
                'pager'        => [],
                'layout'       => "{items}\n{pager}",
                'itemView' => function ( $model ) {

                    return $this->render( '_list',[
                        'model' => $model,
                        'link'  => 'page',
                    ] );
                },
                'emptyText' => '',
            ] ); ?>

            <div class="publishing-action add-to-menu-action ">
                <input type="submit" name="save_menu" class="button button-primary menu-save add-new-menu-item" value="<?= __(  'Add to menu' ) ?>">
            </div>

        </div>

    </div>