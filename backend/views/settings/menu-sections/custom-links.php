<?php /***************************************************************
 *         		 	   	 CUSTOM MENU LINKS SECTION                   *
 *********************************************************************/

/* @var $this yii\web\View */ ?>

    <div class="x_panel menu-items">

        <div class="x_title">

            <h2>
                <?= __(  'Custom Links' ) ?>
            </h2>

            <ul class="nav navbar-right panel_toolbox" style="min-width: 20px;">

                <li>
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                </li>

            </ul>

            <div class="clearfix"></div>

        </div>

        <div class="x_content" style="background-color: white;">

            <div class="form-group custom-link-title">
                <input type="text" id="customTitle" placeholder="<?= __(  'Title' ) ?>">
            </div>

            <div class="form-group custom-link-link">
                <input type="text" id="customLink" placeholder="<?= __(  'Custom Link' ) ?>">
            </div>

            <div class="publishing-action add-to-menu-action">
                <input type="submit" name="save_menu" class="button button-primary menu-save add-new-menu-item" value="<?= __(  'Add to menu' ) ?>">
            </div>

        </div>

    </div>