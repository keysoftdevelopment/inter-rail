<?php /***************************************************************
 *         		 	   	  LIST OF ITEMS SECTION                      *
 *********************************************************************/

use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model mixed       */
/* @var $link string       */ ?>

    <label class="checkbox-element">

        <?= Html::encode( $model->title ) ?>

        <input type="checkbox" name="title" class="elements-in-menu" data-name="<?= Html::encode( $model->title ) ?>" data-id="<?= HtmlPurifier::process( $model->id ) ?>" data-link="<?= HtmlPurifier::process(
            $model->slug === "/" ? "/" : Url::to( "/" . $link ."/" ) . $model->slug
        ) ?>">

        <span class="checkmark"></span>

    </label>