<?php /***************************************************************
 * 		 	   	    FORM SECTION FOR MENU PAGE                       *
 *********************************************************************/

use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

use common\models\Languages;

/* @var $this yii\web\View            */
/* @var $model common\models\Settings */

$form = ActiveForm::begin( [
    'options' => [
        'class'                 => 'form-horizontal form-label-left ajax-submitting-form',
        'data-func'             => 'formCallback',
        'data-redirectonfinish' => Url::toRoute( [
                '/options/create'
        ] )
    ],
] ); ?>

    <?= $form
        -> field( $model, 'name', [
            'template' => '{input}',
            'options'  => [
                'tag' => false, // Don't wrap with "form-group" div
            ]
        ] )
        -> hiddenInput( [
            'value' => 'menu-data'
        ] )
        -> label( false );
    ?>


    <div class="major-publishing-actions">

        <label class="menu-name-label howto open-label" for="menu-name">
            <?= __(  'Menu' ) ?>
        </label>

        <div id="domenu-1-output" class="output-preview-container" style="display: none">

            <?= $form->field( $model, 'description' )
                -> textarea( [
                    'class' => 'jsonOutput'
                ] )
                -> label( false );
            ?>

        </div>

        <div class="publishing-action pull-right">
            <input type="submit" name="save_menu" id="save_menu_header" class="button button-primary menu-save" value="<?= __(  'Save Menu' ) ?>">
        </div>

        <?= $form->field( $model, 'lang_id' )
            -> dropDownList( ArrayHelper::map(
                Languages::find()->all(),
                'id',
                'description'
            ), [
                'prompt' => __(  'Select Language' )
            ] )
            -> label( false );
        ?>

    </div>

<?php ActiveForm::end(); ?>