<?php /***************************************************************
 *	                     SINGLE SETTING SECTION                      *
 *********************************************************************/

use yii\helpers\Html;

/* @var $model array */ ?>

	<div class="setting-option">

		<label for="<?= $model[ 'key' ] ?>" class="control-label col-md-1 col-sm-1 col-xs-12">
			<?= $model[ 'label' ] ?>
		</label>

        <?php if ( $model[ 'key' ] == 'order_options' ) { ?>

            <div class="col-md-11 col-sm-11 col-xs-12">

				<?= Html::textInput(
					'SettingsForm[' . $model[ 'key' ] . '][]',
					isset( $model[ 'value' ] )
						? $model[ 'value' ]
						: '', [
						'class' => 'form-control col-md-12 col-sm-12 col-xs-12 ' . $model[ 'key' ] . '-field'
					]
				) ?>

            </div>

        <?php } else { ?>

            <div class="col-md-6 col-sm-6 col-xs-12">

				<?= Html::textInput(
					'SettingsForm[' . $model[ 'key' ] . '][' . $model[ 'lang_id' ] . '][name][]',
					isset( $model[ 'value' ] )
						? $model[ 'value' ]
						: '', [
						'class' => 'form-control col-md-12 col-sm-12 col-xs-12 ' . $model[ 'key' ] . '-field'
					]
				) ?>

            </div>

            <div class="col-md-5 col-sm-5 col-xs-12">

				<?= Html::dropDownList(
					'SettingsForm[' . $model[ "key" ] . '][' . $model[ 'lang_id' ] . '][role][]',
					isset( $model[ 'role' ] )
						? $model[ 'role' ]
						: '',
					Yii::$app->common->roles(), [
						'class'  => 'form-control col-md-12 col-sm-12 col-xs-12 ' . $model[ 'key' ] . ' -field',
					]
				); ?>

            </div>

        <?php } ?>

		<a href="javascript:void(0)" class="remove-current-option" data-parent="setting-option" data-translations="<?= htmlentities( json_encode( [
			'title'   => __( 'CONFIRMATION MODULE' ),
			'message' => __( 'Do you really want to delete this option?' )
		] ) ) ?>">
			x
		</a>

	</div>
