<?php /***************************************************************
 *	                       MAIL SETTING SECTION                      *
 *********************************************************************/

/* @var $model common\models\SettingsForm */
/* @var $form yii\widgets\ActiveForm 	  */ ?>

<div role="tabpanel" class="tab-pane fade" id="mail" aria-labelledby="mail-tab">

	<div class="col-sm-12 col-md-12 col-xs-12">

		<label for="from_name" class="control-label col-md-3 col-sm-3 col-xs-12">
			<?= __(  'From Name' ) ?>
		</label>

		<div class="col-md-6 col-sm-6 col-xs-12">

			<?= $form->field( $model, 'from_name' )
				-> textInput( [
					'class' => 'form-control col-md-7 col-xs-12'
				] )
				-> label( false );
			?>

			<div id="info_oh" class="info_oh"></div>

		</div>

	</div>

	<div class="col-sm-12 col-md-12 col-xs-12">

		<label for="from_mail" class="control-label col-md-3 col-sm-3 col-xs-12">
			<?= __(  'From E-mail' ) ?>
		</label>

		<div class="col-md-6 col-sm-6 col-xs-12">

			<?= $form->field( $model, 'from_mail' )
				-> textInput( [
					'class' => 'form-control col-md-7 col-xs-12'
				] )
				-> label( false );
			?>

			<div class="info_oh" id="info_oh"></div>

		</div>

	</div>

    <div class="col-sm-12 col-md-12 col-xs-12">

        <label for="email" class="control-label col-md-3 col-sm-3 col-xs-12">
			<?= __(  'Support E-mail' ) ?>
        </label>

        <div class="col-md-6 col-sm-6 col-xs-12">

			<?= $form->field( $model, 'e_mail' )
				-> textInput( [
					'class' => 'form-control col-md-7 col-xs-12'
				] )
				-> label( false );
			?>

            <div class="info_oh" id="info_oh"></div>

        </div>

    </div>

</div>
