<?php /***************************************************************
 *	                     GENERAL SETTING SECTION                     *
 *********************************************************************/

/* @var $model common\models\SettingsForm    */
/* @var $form yii\widgets\ActiveForm 	     */
/* @var $languages common\models\Languages[] */ ?>

	<div role="tabpanel" class="tab-pane fade active in" id="general" aria-labelledby="general-tab">

		<div class="col-sm-12 col-md-12 col-xs-12">

			<label for="google_analytics_code" class="control-label col-md-3 col-sm-3 col-xs-12">
				<?= __(  'Google Analytics CODE' ) ?>
			</label>

			<div class="col-md-8 col-sm-8 col-xs-12">

				<?= $form->field( $model, 'ga_code' )
					-> textInput( [
						'type'  => 'text',
						'class' => 'form-control col-md-12 col-xs-12'
					] )
					-> label( false );
				?>

			</div>

		</div>

		<div class="col-sm-12 col-md-12 col-xs-12">

			<label for="posts_per_page" class="control-label col-md-3 col-sm-3 col-xs-12">
				<?= __(  'Posts Per Page' ) ?>
			</label>

			<div class="col-md-8 col-sm-8 col-xs-12">

				<?= $form->field( $model, 'posts_per_page' )
					-> textInput( [
						'type'  => 'number',
						'class' => 'form-control col-md-12 col-xs-12'
					] )
					-> label( false );
				?>

			</div>

		</div>

		<div class="col-sm-12 col-md-12 col-xs-12">

			<label for="dlang" class="control-label col-md-3 col-sm-3 col-xs-12">
				<?= __(  'Default Language' ) ?>
			</label>

			<div class="col-md-8 col-sm-8 col-xs-12">

				<?= $form->field( $model, 'default_language' )
					-> dropDownList( $languages, [
						'class'  => 'select2_single form-control',
						'style'  => 'display: none; width: 100%;',
						'prompt' => ''
					] )
					-> label( false )
				?>

			</div>

		</div>

	</div>
