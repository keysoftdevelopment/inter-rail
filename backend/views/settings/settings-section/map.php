<?php /***************************************************************
 *	                        MAP SETTING SECTION                      *
 *********************************************************************/

/* @var $model common\models\SettingsForm */
/* @var $form yii\widgets\ActiveForm 	  */ ?>

<div role="tabpanel" class="tab-pane fade" id="map" aria-labelledby="map-tab">

	<div class="col-sm-12 col-md-12 col-xs-12">

		<label for="map" class="control-label col-md-3 col-sm-3 col-xs-12">
			<?= __(  'Google Map Api Key' ) ?>
		</label>

		<div class="col-md-6 col-sm-6 col-xs-12">

			<?= $form->field( $model, 'gmap_api_key' )
				-> textInput( [
					'class' => 'form-control col-md-7 col-xs-12'
				] )
				-> label( false );
			?>

			<div id="info_oh" class="info_oh"></div>

		</div>

	</div>

</div>
