<?php /***************************************************************
 *	                     FOOTER SETTING SECTION                      *
 *********************************************************************/

use yii\helpers\Html;

/* @var $model common\models\SettingsForm */ ?>


	<div class="col-sm-12 col-md-12 col-xs-12" style="margin: 10px 0px;">

		<label for="phone_number" class="control-label col-md-3 col-sm-3 col-xs-12">
			<?= __(  'Phone Number' ) ?>
		</label>

		<div class="col-md-6 col-sm-6 col-xs-12">

			<?= Html::textInput(
				'SettingsForm[phone]',
				$model[ 'phone' ], [
					'class' => 'form-control col-md-7 col-sm-7 col-xs-12 phone-field'
				]
			) ?>

			<div id="info_oh" class="info_oh"></div>

		</div>

	</div>

	<div class="col-sm-12 col-md-12 col-xs-12" style="margin: 10px 0px;">

		<label for="address" class="control-label col-md-3 col-sm-3 col-xs-12">
			<?= __(  'Address' ) ?>
		</label>

		<div class="col-md-6 col-sm-6 col-xs-12">

			<?= Html::textInput(
				'SettingsForm[address][' . $model[ 'lang_id' ] . ']',
				$model[ 'address' ], [
                    'class' => 'form-control col-md-7 col-sm-7 col-xs-12 address-field'
				]
			) ?>

			<div class="info_oh" id="info_oh"></div>

		</div>

	</div>
