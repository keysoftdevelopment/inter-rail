<?php /***************************************************************
 *	                      LOTUS SETTING SECTION                      *
 *********************************************************************/

use yii\helpers\Html;

/* @var $model common\models\SettingsForm */
/* @var $form yii\widgets\ActiveForm 	  */ ?>

<div role="tabpanel" class="tab-pane fade" id="lotus" aria-labelledby="lotus-tab">

	<div class="col-sm-12 col-md-12 col-xs-12">

		<label for="link" class="control-label col-md-3 col-sm-3 col-xs-12">
			<?= __(  'URL' ) ?>
		</label>

		<div class="col-md-6 col-sm-6 col-xs-12">

			<?= $form->field( $model, 'lotus_link' )
				-> textInput( [
					'class' => 'form-control col-md-7 col-xs-12'
				] )
				-> label( false );
			?>

			<div id="info_oh" class="info_oh"></div>

		</div>

	</div>

    <div class="col-sm-12 col-md-12 col-xs-12">

        <label for="is-sandbox" class="control-label col-md-3 col-sm-3 col-xs-12">
			<?= __(  'IS Sandbox' ) ?> ?
        </label>

        <div class="col-md-6 col-sm-6 col-xs-12" style="margin: 5px 0px 10px;">

            <?= Html::checkbox( 'SettingsForm[is_sandbox]', $model->is_sandbox, [
                'class' => 'lotus-sandbox switchery'
            ] ) ?>

        </div>

    </div>

	<div class="col-sm-12 col-md-12 col-xs-12 <?= (int)$model->is_sandbox > 0
        ? ''
        : 'hide'
    ?>">

		<label for="address" class="control-label col-md-3 col-sm-3 col-xs-12">
			<?= __(  'Sandbox URL' ) ?>
		</label>

		<div class="col-md-6 col-sm-6 col-xs-12">

			<?= $form->field( $model, 'lotus_sandbox_link' )
				-> textInput( [
					'class' => 'form-control col-md-7 col-xs-12'
				] )
				-> label( false );
			?>

			<div class="info_oh" id="info_oh"></div>

		</div>

	</div>

</div>
