<?php /***************************************************************
 *	                     SOCIAL SETTING SECTION                      *
 *********************************************************************/

/* @var $model common\models\SettingsForm */
/* @var $form yii\widgets\ActiveForm 	  */ ?>

<div role="tabpanel" class="tab-pane fade" id="social" aria-labelledby="social-tab">

	<div class="col-sm-12 col-md-12 col-xs-12">

		<label for="facebook" class="control-label col-md-3 col-sm-3 col-xs-12">
			<?= __(  'Facebook' ) ?>
		</label>

		<div class="col-md-6 col-sm-6 col-xs-12">

			<?= $form->field( $model, 'facebook' )
				-> textInput( [
					'class' => 'form-control col-md-7 col-xs-12'
				] )
				-> label( false );
			?>

		</div>

	</div>

	<div class="col-sm-12 col-md-12 col-xs-12">

		<label for="weechat" class="control-label col-md-3 col-sm-3 col-xs-12">
			<?= __(  'WeeChat' ) ?>
		</label>

		<div class="col-md-6 col-sm-6 col-xs-12">

			<?= $form->field( $model, 'weechat' )
				-> textInput( [
					'class' => 'form-control col-md-7 col-xs-12'
				] )
				-> label( false );
			?>

		</div>

	</div>

	<div class="col-sm-12 col-md-12 col-xs-12">

		<label for="whatsapp" class="control-label col-md-3 col-sm-3 col-xs-12">
			<?= __(  'Whatsapp' ) ?>
		</label>

		<div class="col-md-6 col-sm-6 col-xs-12">

			<?= $form->field( $model, 'whatsapp' )
				-> textInput( [
					'class' => 'form-control col-md-7 col-xs-12'
				] )
				-> label( false );
			?>

		</div>

	</div>

	<div class="col-sm-12 col-md-12 col-xs-12">

		<label for="telegram" class="control-label col-md-3 col-sm-3 col-xs-12">
			<?= __(  'Telegram' ) ?>
		</label>

		<div class="col-md-6 col-sm-6 col-xs-12">

			<?= $form->field( $model, 'telegram' )
				-> textInput( [
					'class' => 'form-control col-md-7 col-xs-12'
				] )
				-> label( false );
			?>

		</div>

	</div>

	<div class="col-sm-12 col-md-12 col-xs-12">

		<label for="youtube" class="control-label col-md-3 col-sm-3 col-xs-12">
			<?= __(  'Youtube' ) ?>
		</label>

		<div class="col-md-6 col-sm-6 col-xs-12">

			<?= $form->field( $model, 'youtube' )
				-> textInput( [
					'class' => 'form-control col-md-7 col-xs-12'
				] )
				-> label( false );
			?>

		</div>

	</div>

	<div class="col-sm-12 col-md-12 col-xs-12">

		<label for="gplus" class="control-label col-md-3 col-sm-3 col-xs-12">
			<?= __(  'Google Plus' ) ?>
		</label>

		<div class="col-md-6 col-sm-6 col-xs-12">

			<?= $form->field( $model, 'gplus' )
				-> textInput( [
					'class' => 'form-control col-md-7 col-xs-12'
				] )
				-> label( false );
			?>

		</div>

	</div>

</div>
