<?php /***************************************************************
 *	    	     THE LIST OF ALL AVAILABLE SETTINGS PAGE             *
 *********************************************************************/

/* @var $model common\models\SettingsForm */

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

use common\models\Languages;
use common\widgets\Alert;

//current page title, that will displayed in head tags
$this->title = __(  'Settings' );

// the list of available languages
$languages = ArrayHelper::map(
    Languages::find()->all(),
    'id',
    'description'
);

//the list of all available roles
$roles = Yii::$app->authManager;

//settings main form
$form = ActiveForm::begin( [
    'options' => [
        'class'   => 'form-horizontal form-label-left',
		'enctype' => 'multipart/form-data'
    ]
] ); ?>

    <!-- title section -->
    <div class="page-title">

        <div class="title_left">

            <h3>
                <?= __(  'Settings' ) ?>
            </h3>

        </div>

    </div>
    <!-- title section -->

    <div class="clearfix"></div>

    <!-- settings details -->
    <div class="row" id="settings-tab-page-panel">

        <div class="col-md-12 col-xs-12">

            <div class="x_panel">

                <div class="x_content">

                    <!-- flash messages -->
                    <?= Alert::widget() ?>
                    <!-- flash messages -->

                    <div class="row">

                        <div class="clearfix"></div>

                        <div role="tabpanel" data-id="settings-tabs" id="custom-settings">

                            <!-- setting tabs -->
                            <ul id="settings-tab" class="nav nav-tabs bar_tabs" role="tablist">

                                <li class="active">

                                    <a href="#general" id="general-tab" role="tab" data-toggle="tab" aria-expanded="true">
                                        <?= __(  'General' ) ?>
                                    </a>

                                </li>

                                <li>

                                    <a href="#social" role="tab" id="social-tab" data-toggle="tab" aria-expanded="false">
                                        <?= __(  'Social' ) ?>
                                    </a>

                                </li>

                                <li>

                                    <a href="#mail" role="tab" id="mail-tab" data-toggle="tab" aria-expanded="false">
										<?= __(  'Mail' ) ?>
                                    </a>

                                </li>

                                <li>

                                    <a href="#footer" role="tab" id="footer-tab" data-toggle="tab" aria-expanded="false">
                                        <?= __(  'Footer' ) ?>
                                    </a>

                                </li>

                                <li>

                                    <a href="#map" role="tab" id="map-tab" data-toggle="tab" aria-expanded="false">
										<?= __(  'Map' ) ?>
                                    </a>

                                </li>

                                <li>

                                    <a href="#order" role="tab" id="order-tab" data-toggle="tab" aria-expanded="false">
										<?= __(  'Order' ) ?>
                                    </a>

                                </li>

                                <li>

                                    <a href="#statuses" role="tab" id="statuses-tab" data-toggle="tab" aria-expanded="false">
										<?= __(  'Statuses' ) ?>
                                    </a>

                                </li>

                                <li>

                                    <a href="#lotus" role="tab" id="lotus-tab" data-toggle="tab" aria-expanded="false">
										<?= __(  'Lotus' ) ?>
                                    </a>

                                </li>

                            </ul>
                            <!-- setting tabs -->

                            <div id="settings-container" class="tab-content">

                                <!-- settings general details -->
								<?= $this->render( 'settings-section/general', [
									'model'     => $model,
                                    'form'      => $form,
                                    'languages' => $languages
								] ); ?>
                                <!-- settings general details -->

                                <!-- settings social details -->
								<?= $this->render( 'settings-section/social', [
									'model' => $model,
									'form'  => $form
								] ); ?>
                                <!-- settings social details -->

                                <!-- settings mail details -->
								<?= $this->render( 'settings-section/mail', [
									'model' => $model,
									'form'  => $form
								] ); ?>
                                <!-- settings mail details -->

                                <!-- settings footer details -->
                                <div role="tabpanel" class="tab-pane fade" id="footer" aria-labelledby="footer-tab">

                                    <div class="col-sm-12 col-md-12 col-xs-12 language-list">

                                        <label for="footer-lang" class="control-label col-md-2 col-sm-2 col-xs-12">
											<?= __(  'Choose language' ) ?>
                                        </label>

                                        <div class="col-md-10 col-sm-10 col-xs-12">

                                            <select id="footer-lang" class="form-control">

												<?php foreach ( $languages as $id => $lang ) { ?>

                                                    <option value="<?= $id ?>">
														<?= $lang ?>
                                                    </option>

												<?php } ?>

                                            </select>

                                        </div>

                                    </div>

									<?php foreach ( $languages as $id => $lang ) { ?>

                                        <div class="setting-option-container footer-container" id="footer-container-<?= $id ?>" data-lang="<?= $id ?>">

											<?= $this->render( 'settings-section/footer', [
												'model' => [
													'address' => $model->address[ $id ],
                                                    'phone'   => $model->phone,
                                                    'lang_id' => $id
                                                ]
											] ); ?>

                                        </div>

                                        <script type="text/html" id="footer-container-template-<?= $id ?>">

											<?= $this->render( 'settings-section/footer', [
												'model' => [
													'address' => $model->address[ $id ],
													'phone'   => $model->phone,
													'lang_id' => $id
												]
											] ); ?>

                                        </script>

                                    <?php } ?>

                                </div>
                                <!-- settings footer details -->

                                <!-- settings map details -->
								<?= $this->render( 'settings-section/map', [
									'model' => $model,
									'form'  => $form
								] ); ?>
                                <!-- settings map details -->

                                <!-- settings order details -->
                                <div role="tabpanel" class="tab-pane fade" id="order" aria-labelledby="order-tab">

                                    <div class="col-sm-12 col-md-12 col-xs-12">

                                        <label for="confirm" class="control-label col-md-4 col-sm-4 col-xs-12">
											<?= __(  'Show Confirm Option' ) ?>
                                        </label>

                                        <div class="col-md-8 col-sm-8 col-xs-12">

											<?= $form->field( $model, 'confirm_option' )
											    -> dropDownList( Yii::$app->common->statuses( 'order' ), [
												    'class'  => 'form-control',
												    'prompt' => __( 'Show Confirm Option' )
											    ] )
												-> label( false )
											?>

                                        </div>

                                    </div>

                                    <div class="col-sm-12 col-md-12 col-xs-12">

                                        <label for="invoice" class="control-label col-md-4 col-sm-4 col-xs-12">
											<?= __(  'Show Invoice Option' ) ?>
                                        </label>

                                        <div class="col-md-8 col-sm-8 col-xs-12">

											<?= $form->field( $model, 'invoice_option' )
												-> dropDownList( Yii::$app->common->statuses( 'order' ), [
													'class'  => 'form-control',
													'prompt' => __( 'Show Invoice Option' )
												] )
												-> label( false )
											?>

                                        </div>

                                    </div>

                                    <div class="col-sm-12 col-md-12 col-xs-12">

                                        <label for="invoice" class="control-label col-md-4 col-sm-4 col-xs-12">
											<?= __(  'Choose Contractor Role' ) ?>
                                        </label>

                                        <div class="col-md-8 col-sm-8 col-xs-12">

											<?= $form->field( $model, 'contractor_role' )
												-> dropDownList( ArrayHelper::map(
												    $roles->getRoles(),
                                                    'name',
                                                    'name'
                                                ), [
													'class'  => 'form-control',
													'prompt' => __( 'Choose Contractor Role' )
												] )
												-> label( false )
											?>

                                        </div>

                                    </div>

                                    <div class="col-sm-12 col-md-12 col-xs-12 order-numbers">

                                        <label for="order-number" class="control-label col-md-12 col-sm-12 col-xs-12">
                                            <?= __(  'Order Number Option' ) ?>
                                        </label>

                                        <div class="add-new-option" data-type="order-option" data-translations="<?= htmlentities( json_encode( [
											'title'   => __( 'Order Number was added successfully' ) . ' !!!',
											'message' => __( 'Order Number was added successfully' ) . '. ' . __( 'Please check the bottom of current' ) . ' ' . __( 'order number options list' ),
											'type'    => 'success'
										] ) ) ?>">

                                            <a href="javascript:void(0)">
                                                <i class="fa fa-plus"></i>
                                            </a>

                                        </div>

                                        <div class="order-option-container setting-option-container">

                                            <?php if ( isset( $model->order_options )
                                                && ! empty( $model->order_options )
                                            ) {

                                                foreach ( $model->order_options as $key => $value ) {

                                                    echo $this->render( 'settings-section/setting', [
                                                        'model' => [
                                                            'index'   => $key,
                                                            'label'   => __(  'Option' ),
                                                            'key'     => 'order_options',
                                                            'value'   => $value
                                                        ],
                                                    ] );

                                                }
                                            } ?>

                                            <script type="text/html" id="order-option-template">

                                                <?= $this->render( 'settings-section/setting', [
                                                    'model' => [
                                                        'index'   => 0,
                                                        'label'   => __(  'Option' ),
                                                        'key'     => 'order_options'
                                                    ]

                                                ] ); ?>

                                            </script>

                                        </div>

                                    </div>

                                </div>
                                <!-- settings order details -->

                                <!-- settings status details -->
                                <div role="tabpanel" class="tab-pane fade" id="statuses" aria-labelledby="statuses-tab">

                                    <div class="col-sm-12 col-md-12 col-xs-12 language-list">

                                        <label for="statuses-lang" class="control-label col-md-2 col-sm-2 col-xs-12">
											<?= __(  'Choose language' ) ?>
                                        </label>

                                        <div class="col-md-10 col-sm-10 col-xs-12">

                                            <select id="statuses-lang" class="form-control">

												<?php foreach ( $languages as $id => $lang ) { ?>

                                                    <option value="<?= $id ?>">
														<?= $lang ?>
                                                    </option>

												<?php } ?>

                                            </select>

                                        </div>

                                    </div>

                                    <div class="add-new-option" data-type="statuses" data-translations="<?= htmlentities( json_encode( [
										'title'   => __( 'Status option was added successfully' ) . ' !!!',
										'message' => __( 'Status option was added successfully' ) . '. ' . __( 'Please check the bottom of current' ) . ' ' . __( 'status options list' ),
										'type'    => 'success'
									] ) ) ?>">

                                        <a href="javascript:void(0)">
                                            <i>+</i>
                                        </a>

                                    </div>

									<?php foreach ( $languages as $id => $lang ) { ?>

                                        <div class="setting-option-container statuses-container" id="statuses-container-<?= $id ?>" data-lang="<?= $id ?>">

											<?php if ( isset( $model->statuses[ $id ]->name ) ) {

												foreach ( $model->statuses[ $id ]->name as $key => $status ) {

													echo $this->render( 'settings-section/setting', [
														'model' => [
															'index'   => $key,
															'lang_id' => $id,
															'label'   => __(  'Status' ),
															'key'     => 'statuses',
															'value'   => $status,
                                                            'role'    => isset( $model->statuses[ $id ]->role[ $key ] )
                                                                ? $model->statuses[ $id ]->role[ $key ]
                                                                : ''
														],
													] );

												}
											} ?>

                                            <script type="text/html" id="statuses-container-template-<?= $id ?>">

												<?= $this->render( 'settings-section/setting', [
													'model' => [
														'index'   => 0,
														'lang_id' => $id,
														'label'   => __(  'Status' ),
														'key'     => 'statuses'
													]

												] ); ?>

                                            </script>

                                        </div>

									<?php } ?>

                                </div>
                                <!-- settings status details -->

                                <!-- settings map details -->
								<?= $this->render( 'settings-section/lotus', [
									'model' => $model,
									'form'  => $form
								] ); ?>
                                <!-- settings map details -->

                            </div>

                        </div>

                        <div class="settings-save">

                            <?= Html::submitButton( __(  'Save' ), [
                                'class' => 'btn btn-primary'
                            ] ) ?>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>
    <!-- settings details -->

<?php ActiveForm::end() ?>