<?php /***************************************************************
 *	     		 	   	  UPDATE COMPANY PART                        *
 *********************************************************************/

/* @var $this yii\web\View              */
/* @var $model \common\models\Companies */

// current page title
$this->title = __(  'Update company' ); ?>

    <div class="col-md-12 col-sm-12 col-xs-12 edit-company-block">

        <!-- title section -->
        <div class="page-title">

            <div class="title_left">

                <h3>
                    <?= $this->title ?>
                </h3>

            </div>

        </div>
        <!-- title section -->

        <!-- form section -->
        <div class="col-md-9 col-sm-12 col-xs-12 companies-form" style="padding: 0px;">

            <?= $this->render( '_form', [
                'model' => $model
            ] ); ?>

        </div>
        <!-- form section -->

        <!-- sidebar section -->
        <div class="col-md-3 col-sm-12 col-xs-12 sidebar">

            <div class="x_panel thumbnail-sidebar">

                <div class="x_title">

                    <h2>
                        <?= __(  'Thumbnail' ) ?>
                    </h2>

                    <div class="clearfix"></div>

                </div>

                <div class="container">

                    <div class="row">

                        <div class="col-md-12 col-sm-12 col-xs-12 text-center">

                            <img class="thumbnail-view" src="<?= Yii::getAlias( '@frontend_link' ) . Yii::$app->common->thumbnail( $model->file_id ) ?>" />

                            <a href="#company-modal" data-toggle="modal" data-target="#company-modal" class="choose-thumbnail">
                                <?= __(  'Select Image' ) ?>
                            </a>

                        </div>

                    </div>

                </div>

            </div>

        </div>
        <!-- sidebar section -->

    </div>