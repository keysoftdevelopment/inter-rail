<?php /***************************************************************
 * 	    	 	   	    FORM SECTION FOR COMPANY                     *
 *********************************************************************/

use yii\widgets\ActiveForm;
use yii\helpers\Url;

use dosamigos\tinymce\TinyMce;

use backend\widgets\MediaPopup;
use backend\widgets\GoogleMaps;

/* @var $this yii\web\View             */
/* @var $model common\models\Companies */

$form = ActiveForm::begin( [
    'options'                => [
        'class'                 => 'form-horizontal form-label-left need-image',
        'id'                    => 'company-form',
        'data-func'             => 'formCallback',
        'data-redirectonfinish' => Url::toRoute( [
            '/companies'
        ] )
    ],
    'enableAjaxValidation'   => false,
    'enableClientValidation' => false
] ); ?>

    <div class="x_panel">

        <div class="x_content">

            <?= $form
                -> field( $model, 'file_id', [
                    'template' => '{input}',
                    'options'  => [
                        'tag' => false, // Don't wrap with "form-group" div
                    ]
                ] )
                -> hiddenInput( [
                    'id' => 'file_id'
                ] )
                ->label( false );
            ?>

            <div class="col-sm-12 col-md-12 col-xs-12">

                <?= $form
                    -> field( $model, 'name' )
                    -> textInput( [
                        'id'          => 'company-name',
                        'placeholder' => __(  'Name' )
                    ] )
                    -> label( false );
               ?>

            </div>

            <div class="col-sm-12 col-md-12 col-xs-12">

                <div class="col-sm-4 col-md-4 col-xs-12">

					<?= $form
						-> field( $model, 'bank' )
						-> textInput( [
							'id'          => 'bank',
							'placeholder' => __(  'Bank' )
						] )
						-> label( __(  'Bank' ) );
					?>

                </div>

                <div class="col-sm-4 col-md-4 col-xs-12">

					<?= $form
						-> field( $model, 'mfo' )
						-> textInput( [
							'id'          => 'mfo',
							'placeholder' => __(  'MFO' )
						] )
						-> label( __(  'MFO' ) );
					?>

                </div>

                <div class="col-sm-4 col-md-4 col-xs-12">

					<?= $form
						-> field( $model, 'oked' )
						-> textInput( [
							'id'          => 'oked',
							'placeholder' => __(  'OKED' )
						] )
						-> label( __(  'OKED' ) );
					?>

                </div>

            </div>

            <div class="col-sm-12 col-md-12 col-xs-12">

                <?= $form
                    -> field( $model, 'description' )
                    -> widget( TinyMce::className(), [
                        'options' => [
                            'rows' => 15
                        ],
                        'language'      => 'en_GB',
                        'clientOptions' => [
                            'menubar'   => false,
                            'plugins'   => [
                                "advlist image autolink lists link charmap print preview anchor",
                                "searchreplace visualblocks code fullscreen",
                                "insertdatetime media table contextmenu paste"
                            ],
                            'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | visualblocks | code "
                        ]
                    ] )
                    -> label( false );
                ?>

            </div>

            <div class="col-sm-12 col-md-12 col-xs-12">

                <div class="col-sm-3 col-md-3 col-xs-12">

					<?= $form
						-> field( $model, 'country' )
						-> textInput( [
							'id'          => 'country',
							'placeholder' => __(  'Country' )
						] )
						-> label( __(  'Country' ) );
					?>

                </div>

                <div class="col-sm-3 col-md-3 col-xs-12">

					<?= $form
						-> field( $model, 'city' )
						-> textInput( [
							'id'          => 'city',
							'placeholder' => __(  'City' )
						] )
						-> label( __(  'City' ) );
					?>

                </div>

                <div class="col-sm-3 col-md-3 col-xs-12">

					<?= $form
						-> field( $model, 'district' )
						-> textInput( [
							'id'          => 'district',
							'placeholder' => __(  'District' )
						] )
						-> label( __(  'District' ) );
					?>

                </div>

                <div class="col-sm-3 col-md-3 col-xs-12">

					<?= $form
						-> field( $model, 'street' )
						-> textInput( [
							'id'          => 'address',
							'placeholder' => __(  'Address' )
						] )
						-> label( __(  'Address' ) );
					?>

                </div>

            </div>

            <div class="col-md-12 col-md-12 col-xs-12">

                <div class="general-map">

					<?= GoogleMaps::widget( [
						'view'  => $this,
						'model' => [
							'country'  => $model->country,
							'city'     => $model->city,
							'district' => $model->district,
							'street'   => $model->street
						]
					] ); ?>

                </div>

            </div>

			<?= $form->field( $model, 'location' )
				-> hiddenInput( [
					'id' => 'location'
				] )
				-> label( false );
			?>

            <div class="form-group">

                <div class="col-md-12 col-md-12 col-xs-12 text-center">

                    <input class="btn btn-primary" data-value="test" type="submit" value="<?= ( $model->isNewRecord
                        ? __(  'Publish' )
                        : __(  'Update' ) )
                    ?>">

                </div>

            </div>

        </div>

    </div>

<?php ActiveForm::end(); ?>

<?= MediaPopup::widget( [
	'formType'   => 'company-modal',
	'uploadForm' => $this->render( 'upload' )
] ) ?>
