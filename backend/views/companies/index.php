<?php /***************************************************************
 *	  	   	    THE LIST OF ALL AVAILABLE COMPANIES PAGE             *
 *********************************************************************/

use yii\helpers\Html;
use yii\widgets\LinkPager;

/* @var $this yii\web\View                                */
/* @var $searchModel common\models\PostsSearch            */
/* @var $dataProvider yii\data\ActiveDataProvider         */
/* @var $thumbnails array                                 */

// current page title
$this->title = __(  'Companies' ); ?>

    <!-- title section -->
    <div class="page-title">

        <div class="title_left">
            <h3>
                <?= $this->title ?>
            </h3>
        </div>

    </div>
    <!-- title section -->

    <div class="clearfix"></div>

    <!-- company lists-->
    <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12">

            <div class="x_panel">

                <div class="x_content">

                    <!-- company details -->
                    <table class="table table-striped projects">

                        <thead>

                            <tr>

                                <th style="width: 1%">
                                    #
                                </th>

                                <th style="width: 10%">
                                    <?= __(  'Company Logo' ) ?>
                                </th>

                                <th style="width: 15%">
                                    <?= __(  'Name' ) ?>
                                </th>

                                <th style="width: 20%">
                                    <?= __(  'Description' ) ?>
                                </th>

                                <th style="width: 1%"></th>

                            </tr>

                        </thead>

                        <tbody>

                            <?php if ( !empty( $dataProvider->models ) ) {

                                foreach ( $dataProvider->models as $model ) {

                                    //param initialization
                                    $thumbnail = isset( $thumbnails[ $model->file_id ][ 'guide' ] )
                                        ? Yii::getAlias( '@frontend_link' ) . $thumbnails[ $model->file_id ][ 'guide' ]
                                        : Yii::getAlias( '@web' ) . '/images/image-not-found.png'; ?>

                                    <tr>

                                        <td>
                                            #
                                        </td>

                                        <td>
                                            <img class="page-image" src="<?= $thumbnail ?>"/>
                                        </td>

                                        <td>

                                            <?= Html::a( $model->name, [
                                                'update',
                                                'id' => $model->id
                                            ], [
                                                'role' => "button"
                                            ] ) ?>

                                            <br/>

                                            <small>
                                                <?= $model->created_at ?>
                                            </small>

                                        </td>

                                        <td>

                                            <?= strlen( strip_tags( $model->description ) ) > 150
                                                ? \Yii::$app->common->excerpt( strip_tags( $model->description ) ) . ' ...'
                                                : $model->description;
                                            ?>

                                        </td>

                                        <td class="text-center model-actions">

                                            <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                <i class="fa fa-ellipsis-v"></i>
                                            </a>

                                            <ul class="dropdown-menu pull-right">

                                                <li>

													<?= Html::a( '<i class="fa fa-pencil"></i> ' .  __(  'Edit' ), [
														'update',
														'id' => $model->id
													] ); ?>

                                                </li>

                                                <li>

													<?= Html::a( '<i class="fa fa-trash"> </i> '. __(  'Delete' ), [
														'delete',
														'id' => $model->id
													], [
														'data'  => [
															'confirm' => __(  'Do you really want to delete this company?' ),
															'method'  => 'post',
														]
													] ); ?>

                                                </li>

                                            </ul>

                                        </td>

                                    </tr>

                                <?php }

                            } else {

                                echo '<tr>';

                                    echo '<td colspan="6">
                                        ' . __(  'There is no companies yet' ) . '
                                    </td>';

                                echo '</tr>';

                            } ?>

                        </tbody>

                    </table>
                    <!-- company details -->

                </div>

                <!-- pagination section -->
                <div class="pagination-container">

                    <?= LinkPager::widget( [
                        'pagination' => $dataProvider->pagination,
                    ] ); ?>

                </div>
                <!-- pagination section -->

            </div>

        </div>

    </div>
    <!-- company lists-->