<?php /***************************************************************
 *         		 	   	EXPENSES FILTERS SECTION                     *
 *********************************************************************/

/* @var $searchModel common\models\ExpensesSearch */

use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

use common\models\User;

//get request short option
$get_request = Yii::$app->getRequest();
$form        = ActiveForm::begin( [
	'id'      => 'main-filter-form',
	'method'  => 'get',
	'action'  => Url::to( [
		'/expenses'
	] )
] ); ?>

    <ul class="list-filters">

        <li class="filter-element">

            <div class="form-group">

				<?= Html::dropDownList(
					'user_id',
					$get_request->getQueryParam( 'user_id' ),
					ArrayHelper::map( User::findAll( [
						'id'        => Yii::$app->authManager->getUserIdsByRole( 'client' ),
						'isDeleted' => false
					] ), 'id', function( $model ) {

						return ! empty( $model[ 'first_name'] ) || ! empty( $model[ 'last_name'] )
							? $model[ 'first_name' ] . ' ' . $model[ 'last_name' ]
							: $model[ 'username' ];

					} ), [
						'prompt' => __(  'Select customer' ),
						'class'  => 'form-control'
					]
				); ?>

            </div>

        </li>

        <li class="filter-element">

            <div class="form-group">

				<?= Html::dropDownList(
					'transportation',
					$get_request->getQueryParam( 'transportation' ),
					Yii::$app->common->transportationTypes( 'transportation' ), [
						'prompt' => __(  'Select section' ),
						'class'  => 'form-control'
					]
				); ?>

            </div>

        </li>

        <li class="filter-element">

            <button class="btn btn-default">
				<?= __(  'Filter' ); ?>
            </button>

        </li>

    </ul>

<?php ActiveForm::end(); ?>