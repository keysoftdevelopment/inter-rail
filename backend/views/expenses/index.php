<?php /***************************************************************
 *	  	   	    THE LIST OF ALL AVAILABLE EXPENSES PAGE              *
 *********************************************************************/

use yii\helpers\Html;
use yii\widgets\LinkPager;

use common\widgets\Alert;

/* @var $this yii\web\View                        */
/* @var $searchModel common\models\OrdersSearch   */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $orders array                             */
/* @var $customers array                          */

// current page title
$this->title = __(  'Expenses' );

//get request short option
$request = Yii::$app->getRequest(); ?>

<div class="expenses-list">

    <!-- title section -->
	<div class="page-title">

		<div class="title_left">

			<h3>
				<?= $this->title ?>
			</h3>

		</div>

	</div>
    <!-- title section -->

	<div class="clearfix"></div>

	<div class="row">

		<div class="col-md-12 col-sm-12 col-xs-12">

            <!-- filters section -->
            <div class="main-filters-container">

				<?= $this->render( 'filters', [
					'searchModel' => $searchModel
				] ) ?>

            </div>
            <!-- filters section -->

			<div class="x_panel">

				<div class="x_content">

                    <!-- flash messages -->
					<?= Alert::widget() ?>
                    <!-- flash messages -->

                    <!-- expenses lists -->
					<table class="table table-striped">

						<thead>

							<tr>

								<th>
									#
								</th>

								<th>
									<?= __(  'Order Number' ) ?>
								</th>

								<th>
									<?= __(  'Section' ) ?>
								</th>

								<th>
									<?= __(  'Customer' ) ?>
								</th>

								<th>
									<?= __(  'Terminal Sum' ) ?>
								</th>

								<th>
									<?= __(  'Auto Sum' ) ?>
								</th>

								<th>
									<?= __(  'Rail sum' ) ?>
								</th>

								<th>
									<?= __(  'Sea Freight Sum' ) ?>
								</th>

								<th>
									<?= __(  'Port Forwarding' ) ?>
								</th>

								<th>
									<?= __(  'Total' ) ?>
								</th>

								<th></th>

							</tr>

						</thead>

						<tbody>

							<?php if ( !empty( $dataProvider->models ) ) {

								foreach ( $dataProvider->models as $model ) {

								    //param initialization
									$railway_sum = 0;

									if ( ! empty( $model->rf_details ) ) {

									    //railway rates
									    $rf_rates = json_decode( $model->rf_details );

									    //re-assign railway sum
										$railway_sum = array_reduce( ! empty( $rf_rates->rate )
                                            ? $rf_rates->rate
                                            : [], function( $result, $rate ) use( $rf_rates ) {

										    //param key
											$key = array_search( $rate, $rf_rates->rate );

											$result += (float)$rate + ( isset( $rf_rates->surcharge[ $key ] )
												? (float)$rf_rates->surcharge[ $key ]
                                                : 0
											);

										    return $result;

                                        }, 0 );

                                    } ?>

									<tr>

										<td>
											#
										</td>

										<td>

											<?= Html::a( isset( $orders[ $model->order_id ] )
												? $orders[ $model->order_id ][ 'number' ]
												: $model->order_id, [
												'/orders/update',
												'id' => $model->order_id
											], [
												'role' => "button"
											] ) ?>

										</td>

										<td></td>

										<td>

											<?= isset( $customers[ $orders[ $model->order_id ][ 'user_id' ] ] )
												? $customers[ $orders[ $model->order_id ][ 'user_id' ] ][ 'first_name' ] . ' ' . $customers[ $orders[ $model->order_id ][ 'user_id' ] ][ 'last_name' ]
												: __( 'Unknown Customer' )
											?>

										</td>

										<td>
											<?= (float)$model->storage + (float)$model->lading + (float)$model->exp_price + (float)$model->imp_price + (float)$model->unloading + (float)$model->terminal_surcharge ?>
										</td>

										<td>
											<?= (float)$model->auto_rate + (float)$model->auto_surcharge ?>
										</td>

										<td>
											<?= (float)$model->rf_rental + (float)$model->rf_commission + $railway_sum ?>
										</td>

										<td>
											<?= (float)$model->sf_rate + (float)$model->dthc + (float)$model->othc + (float)$model->bl + (float)$model->sf_surcharge ?>
										</td>

										<td>
											<?= (float)$model->pf_rate + (float)$model->pf_surcharge + (float)$model->pf_commission ?>
										</td>

										<td>
											<?= $model->total ?>
										</td>

										<td class="text-center model-actions">

											<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
												<i class="fa fa-ellipsis-v"></i>
											</a>

											<ul class="dropdown-menu pull-right">

												<li>

													<?= Html::a( '<i class="fa fa-pencil"></i> ' .  __(  'Edit' ), [
														'/orders/update',
														'id' => $model->order_id
													] ); ?>

												</li>

											</ul>

										</td>

									</tr>

								<?php }

							} else {

								echo '<tr>';

								    echo '<td colspan="11" class="aligncenter">
                                        ' . __(  'There is no expenses yet' ) . '
                                    </td>';

								echo '</tr>';

							} ?>

						</tbody>

					</table>
                    <!-- expenses lists -->

				</div>

                <!-- pagination section -->
				<div class="pagination-container">

					<?= LinkPager::widget( [
						'pagination' => $dataProvider->pagination,
					] ); ?>

				</div>
                <!-- pagination section -->

			</div>

		</div>

	</div>

</div>