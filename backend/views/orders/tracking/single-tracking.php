<?php /***************************************************************
 *	 	    		    SINGLE TRACKING DETAIL SECTION 				 *
 *********************************************************************/

use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

?>

<li class="single-tracking">

    <div class="tracking">

        <div class="tracking-top">

            <div class="place-name">

				<?= Html::textInput(
					'tracking[name][]',
					isset( $model[ 'name' ] )
						? $model[ 'name' ]
						: '',
					[
						'class'       => 'place-name-field',
						'placeholder' => __( 'Place Name' )
					]
				); ?>

            </div>

            <div class="tracking-status">

				<?= Html::dropDownList(
					'tracking[status][]',
					isset( $model[ 'status' ] )
						? $model[ 'status' ]
						: '',
					Yii::$app->common->statuses( 'tracking' ),
					[
						'data-msg' => __( 'Choose status' )
					] )
				?>

            </div>

            <div class="remove-current-option" data-parent="single-tracking">
                <i class="fa fa fa-close"></i>
            </div>

        </div>

        <div class="tracking-details" style="float: left; width: 100%; margin-top: 10px; text-align: left;">

            <div class="col-sm-12 col-md-12 col-xs-12">

                <label style="font-size: 11px; text-transform: uppercase; font-weight: bold; color: #b3b3b3; float: left; width: 100%">
                    <?= __( 'Arrival and departure' ) ?>
                </label>

                <div class="col-sm-6 col-md-6 col-xs-12">

                    <?= $this->render( 'date-picker', [
                        'model' => [
                            'type'  => 'arrival',
                            'label' => __( 'Arrival date' ),
                            'class' => 'arrival-date',
                            'value' => isset( $model[ 'arrival' ] )  && ! empty( $model[ 'arrival' ] )
                                ? $model[ 'arrival' ]
                                : ''
                        ]
                    ] ) ?>

                </div>

                <div class="col-sm-6 col-md-6 col-xs-12">

					<?= $this->render( 'date-picker', [
						'model' => [
							'type'  => 'departure',
							'label' => __( 'Departure Date' ),
							'class' => 'departure-date',
							'value' => isset( $model[ 'departure' ] )  && ! empty( $model[ 'departure' ] )
								? $model[ 'departure' ]
								: ''
						]
					] ) ?>

                </div>

            </div>

            <div class="col-sm-12 col-md-12 col-xs-12" style="margin-top: 10px;">

                <label style="font-size: 11px; text-transform: uppercase; font-weight: bold; color: #b3b3b3; float: left; width: 100%">
					<?= __( 'Location' ) ?>
                </label>

				<?= Html::dropDownList(
					'tracking[place_id][]',
					isset( $model[ 'container' ] )
						? $model[ 'container' ]
						: '',
					ArrayHelper::map( $places, 'id', 'city' ),
					[
                        'class'    => 'form-control select2_single',
						'data-msg' => __( 'Choose location' ),
                        'style'    => 'width:100%'
					] )
				?>


            </div>

            <div class="col-sm-12 col-md-12 col-xs-12" style="margin-top: 10px;">

				<?= Html::textarea(
					'tracking[description][]',
					isset( $model[ 'name' ] )
						? $model[ 'name' ]
						: '',
					[
						'class'       => 'description-field form-control',
						'placeholder' => __( 'Description' ),
                        'style'       => '    height: 50px;'
					]
				); ?>

            </div>

        </div>

    </div>

</li>