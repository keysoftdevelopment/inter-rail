<?php /***************************************************************
 *  	     	 FORM SECTION PART FOR ORDERS TRACKING            	 *
 *       	  I.E WILL BE DISPLAYED AT THE ORDER EDIT PAGE  		 *
 *********************************************************************/

use common\models\Places;
use kartik\date\DatePicker;
use yii\helpers\Html;

/* @var $model common\models\Orders         */
/* @var $tracking common\models\Tracking [] */

//the list of available tracking options
$places = Places::find()
	-> where( [
		'!=', 'city', ''
	] )
    -> asArray()
	-> all();
?>

<div class="col-md-12 col-sm-12 col-xs-12">

	<div class="order-form-title">

		<h4>
			<a class="dropdown-toggle" data-toggle="collapse" href="#tracking" aria-expanded="false">
				<?= __( 'Tracking' ) ?>
			</a>
		</h4>

		<ul class="nav navbar-right panel_toolbox" style="min-width: 30px;">

			<li>
				<a class="dropdown-toggle" data-toggle="collapse" href="#tracking" aria-expanded="false">
					<i class="dropdown fa fa-chevron-down"></i>
				</a>
			</li>

            <li>

                <a href="javascript:void(0)" id="add-tracking" data-toggle="dropdown" data-type="tracking">
                    <i class="fa fa-plus"></i>
                </a>

            </li>

            <li>

                <a href="javascript:void(0)" class="import-xlsx-file text-right" data-details="<?= htmlentities( json_encode( [
					'url'  => '/orders/tracking-import?order_id=' . $model->id,
                    'type' => 'tracking'
				] )) ?>">
                    <i class="dropdown fa fa-file-excel-o"></i>
                </a>

            </li>

		</ul>

	</div>

	<div class="order-form-content collapse" id="tracking">

        <ul class="tracking-container">

            <?php if ( ! empty( $tracking ) ) {

                foreach ( $tracking as $trace ) { ?>

					<?= $this->render( 'single-tracking', [
						'model'  => $trace,
						'places' => $places
					] ); ?>

                <?php }

            } ?>

        </ul>

	</div>

</div>

<script type="text/html" id="tracking-template">

    <?= $this->render( 'single-tracking', [
        'form'  => $form,
        'model' => $model,
		'places' => $places
    ] ) ?>

</script>