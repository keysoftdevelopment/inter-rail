<?php /***************************************************************
 *  	   	    FORM SECTION PART FOR ORDERS CODE DETAILS            *
 *       	  I.E WILL BE DISPLAYED AT THE ORDER EDIT PAGE  		 *
 *********************************************************************/

/* @var $model common\models\Orders  */
/* @var $form yii\widgets\ActiveForm */
/* @var $full_access boolean         */
/* @var $expenses_access boolean     */ ?>

<div class="col-md-12 col-sm-12 col-xs-12">

	<div class="order-form-title">

		<h4>
			<a class="dropdown-toggle" data-toggle="collapse" href="#code-details" aria-expanded="false">
				<?= __( 'Code Details' ) ?>
			</a>
		</h4>

		<ul class="nav navbar-right panel_toolbox" style="min-width: 30px;">

			<li>
				<a class="dropdown-toggle" data-toggle="collapse" href="#code-details" aria-expanded="false">
					<i class="dropdown fa fa-chevron-down"></i>
				</a>
			</li>

            <?php if ( $full_access ) { ?>

                <li>

                    <a href="javascript:void(0)" class="add-item" id="add-code" data-toggle="dropdown" data-type="code" data-translations="<?= htmlentities( json_encode( [
						'title'   => __( 'Code was added successfully' ) . ' !!!',
						'message' => __( 'Code was added successfully' ) .'. ' . __( 'Please check the bottom of current' ) . ' ' . __( 'code list' ),
						'type'    => 'success'
					] ) ) ?>">
                        <i class="fa fa-plus"></i>
                    </a>

                </li>

			<?php } ?>

		</ul>

	</div>

	<div class="order-form-content collapse code-container" id="code-details">

        <?php if ( ! empty( $model->codes ) ) {

            foreach ( $model->codes as $key => $code ) { ?>

                <?= $this->render( 'templates/code', [
					'full_access'     => $full_access,
					'expenses_access' => $expenses_access,
                    'model'           => [
                        'index'         => $key,
                        'country'       => $code->country,
                        'route'         => $code->route,
                        'forwarder'     => $code->forwarder,
                        'code'          => $code->code,
						'code_rg_start' => $code->code_rg_start,
						'code_rg_end'   => $code->code_rg_end,
						'surcharge'     => $code->surcharge,
						'rate'          => $code->rate
                    ]
                ] ) ?>

            <?php }

        } ?>

	</div>

</div>

<script type="text/html" id="code-template">

	<?= $this->render( 'templates/code', [
        'full_access'     => $full_access,
        'expenses_access' => $expenses_access,
    ] ); ?>

</script>
