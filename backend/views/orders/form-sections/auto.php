<?php /***************************************************************
 *  	   	   FORM SECTION PART FOR ORDERS AUTO SECTION             *
 *       	  I.E WILL BE DISPLAYED AT THE ORDER EDIT PAGE  		 *
 *********************************************************************/

/* @var $model common\models\Orders            */
/* @var $form yii\widgets\ActiveForm           */
/* @var $containers common\models\Containers[] */ ?>

<div class="col-md-12 col-sm-12 col-xs-12">

	<div class="order-form-title">

		<h4>

			<a class="dropdown-toggle" data-toggle="collapse" href="#auto-details" aria-expanded="false">
				<?= __( 'Auto Details' ) ?>
			</a>

		</h4>

		<ul class="nav navbar-right panel_toolbox" style="min-width: 30px;">

			<li>

				<a class="dropdown-toggle" data-toggle="collapse" href="#auto-details" aria-expanded="false">
					<i class="dropdown fa fa-chevron-down"></i>
				</a>

			</li>

			<li>

				<a href="javascript:void(0)" class="add-item" id="add-auto" data-toggle="dropdown" data-type="auto" data-translations="<?= htmlentities( json_encode( [
					'title'   => __( 'Auto option was added successfully' ) . ' !!!',
					'message' => __( 'Auto option was added successfully' ) .'. ' . __( 'Please check the bottom of current' ) . ' ' . __( 'auto options list' ),
					'type'    => 'success'
				] ) ) ?>">
					<i class="fa fa-plus"></i>
				</a>

			</li>

		</ul>

	</div>

	<div class="order-form-content collapse auto-container" id="auto-details">

        <?php if ( ! empty( $model->auto ) ) {

            //param initialization
            $auto = json_decode( $model->auto );

            if ( isset( $auto->transport_no ) ) {

                foreach ( $auto->transport_no as $key => $value ) { ?>

					<?= $this->render( 'templates/single-auto', [
						'containers' => $containers,
                        'model'      => [
							'index'        => $key,
							'transport_no' => $value,
                            'container'    => isset( $auto->container[ $key ] )
                                ? $auto->container[ $key ]
                                : '',
							'status' => isset( $auto->status[ $key ] )
								? $auto->status[ $key ]
								: '',
                            'owner' => isset( $auto->owner[ $key ] )
								? $auto->owner[ $key ]
								: ''
                        ]
					] ) ?>

                <?php }

            }

        } ?>

	</div>

</div>

<script type="text/html" id="auto-template">

	<?= $this->render( 'templates/single-auto', [
		'containers' => $containers
	] ) ?>

</script>
