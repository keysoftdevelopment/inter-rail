<?php /***************************************************************
 *  	 FORM SECTION PART FOR ORDERS INVOICES DETAILS SECTION       *
 *      	 I.E WILL BE DISPLAYED AT THE ORDER EDIT PAGE  		 	 *
 *********************************************************************/

use yii\helpers\Url;

use common\models\Invoices;

/* @var $model common\models\Orders 	   */
/* @var $form yii\widgets\ActiveForm       */
/* @var $invoices common\models\Invoices[] */
/* @var $full_access boolean               */

//current order invoices quantity
$last_invoice = Invoices::find()
	-> where( [
		'type' => 'Lotus'
	] )
	-> count() ?>

<div class="col-md-12 col-sm-12 col-xs-12">

	<div class="order-form-title">

		<h4>

			<a class="dropdown-toggle" data-toggle="collapse" href="#invoices-details" aria-expanded="false">
				<?= __( 'Invoices' ) ?>
			</a>

		</h4>

		<ul class="nav navbar-right panel_toolbox" style="min-width: 30px;">

			<li>

				<a class="dropdown-toggle" data-toggle="collapse" href="#invoices-details" aria-expanded="false">
					<i class="dropdown fa fa-chevron-down"></i>
				</a>

			</li>

            <?php if ( $full_access ) { ?>

                <li>

                    <a href="javascript:void(0)" class="add-item" id="add-invoice-details" data-toggle="dropdown" data-type="invoice-details" data-translations="<?= htmlentities( json_encode( [
						'title'   => __( 'Invoice was added successfully' ) . ' !!!',
						'message' => __( 'Invoice was added successfully' ) .'. ' . __( 'Please check the bottom of current' ) . ' ' . __( 'invoices list' ),
						'type'    => 'success'
					] ) ) ?>">
                        <i class="fa fa-plus"></i>
                    </a>

                </li>

                <li>

                    <a href="<?= Url::to( [
						'/invoices/generate-invoice',
						'id' => $model->id
					] ) ?>" target="_blank">
                        <i class="fa fa-file-pdf-o"></i>
                    </a>

                </li>

			<?php } ?>

		</ul>

	</div>

	<div class="order-form-content collapse invoice-details-container" id="invoices-details">

		<?php if ( ! empty( $invoices ) ) {

			foreach ( $invoices as $key => $value ) { ?>

				<?= $this->render( 'templates/invoice', [
					'count'       => $last_invoice,
					'full_access' => $full_access,
					'model'       => [
						'index'  => $key,
						'id'     => $value->id,
						'number' => $value->number,
						'date'   => $value->date,
						'amount' => $value->amount,
						'status' => $value->status
					]
				] ) ?>

			<?php }

		} ?>

	</div>

</div>

<script type="text/html" id="invoice-details-template">

	<?= $this->render( 'templates/invoice', [
		'full_access' => $full_access,
		'count'       => $last_invoice,
	] ) ?>

</script>
