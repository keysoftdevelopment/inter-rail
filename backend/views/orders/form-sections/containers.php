<?php /***************************************************************
 *  	   	 FORM SECTION PART FOR ORDERS CONTAINERS DETAIL`s        *
 *       	  I.E WILL BE DISPLAYED AT THE ORDER EDIT PAGE  		 *
 *********************************************************************/

use yii\helpers\ArrayHelper;
use yii\helpers\Url;

use common\models\ContainersRelation;
use common\models\WagonsRelation;

/* @var $model common\models\Orders          */
/* @var $form yii\widgets\ActiveForm         */
/* @var $containers common\models\Containers */
/* @var $wagons common\models\Wagons         */
/* @var $expenses common\models\Expenses[]   */
/* @var $full_access boolean                 */
/* @var $expenses_export boolean             */
/* @var $is_expenses_visable boolean         */

//selected containers
$selected_containers = $model->getContainers(); ?>

<div class="col-md-12 col-sm-12 col-xs-12">

	<div class="order-form-title">

		<h4>
			<a class="dropdown-toggle" data-toggle="collapse" href="#containers" aria-expanded="false">
				<?= __( 'Containers' ) ?>
			</a>
		</h4>

		<ul class="nav navbar-right panel_toolbox">

			<li>
				<a class="dropdown-toggle" data-toggle="collapse" href="#containers" aria-expanded="false">
					<i class="dropdown fa fa-chevron-down"></i>
				</a>
			</li>

            <?php if ( $full_access ) { ?>

                <li>

                    <a href="javascript:void(0)" class="add-item" id="add-container" data-toggle="dropdown" data-type="containers" data-translations="<?= htmlentities( json_encode( [
                        'title'   => __( 'Container was added successfully' ) . ' !!!',
                        'message' => __( 'Container was added successfully' ) .'. ' . __( 'Please check the bottom of current' ) . ' ' . __( 'container list' ),
                        'type'    => 'success'
                    ] ) ) ?>">
                        <i class="fa fa-plus"></i>
                    </a>

                </li>

			<?php } if ( $expenses_export ) { ?>

                <li>

                    <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        <i class="dropdown fa fa-file-excel-o"></i>
                    </a>

                    <ul class="dropdown-menu pull-right">

                        <li>

                            <a href="<?= Url::to( [
                                '/expenses/exports',
                                'type'     => 'containers',
                                'order_id' => $model->id
                            ] ) ?>" target="_blank">
                                <i class="dropdown fa fa-file-excel-o"></i> <?= __( 'Export' ) ?>
                            </a>

                        </li>

                        <li>

                            <a href="javascript:void(0)" class="import-xlsx-file" data-details="<?= htmlentities( json_encode( [
                                'url' => '/containers/import?order_id=' . $model->id
                            ] )) ?>">
                                <i class="dropdown fa fa-file-excel-o"></i> <?= __( 'Import' ) ?>
                            </a>

                        </li>

                    </ul>

                </li>

            <?php } ?>

		</ul>

	</div>

	<div class="order-form-content collapse containers-container" id="containers">

        <?php if ( ! empty( $selected_containers ) || ! empty( $expenses ) ) {

            //param initialization
            $index = 0;

			//container wagons
			$wagon = WagonsRelation::find()
				-> where( [
					'type'  => 'container-' . $model->id
				] )
				-> indexBy( 'foreign_id' )
				-> asArray()
				-> all();

            //container expenses details
            $container_expenses = ContainersRelation::find()
                -> where( [
                    'foreign_id' => ArrayHelper::getColumn(
                        $expenses, 'id'
                    ),
                    'type' => 'expenses'
                ] )
                -> indexBy( 'container_id' )
                -> asArray()
                -> all();

            foreach ( ! empty( $selected_containers )
                ? $selected_containers
                : $expenses as $key => $value
            ) {

                //expenses id due to container id
                $expenses_id = isset( $container_expenses[ $value[ 'id' ] ] )
                    ? $container_expenses[ $value[ 'id' ] ][ 'foreign_id' ]
                    : $value[ 'id' ];

                echo $this->render( 'templates/container', [
                    'model' => [
						'index'     => $index++,
						'container' => ! empty( $selected_containers )
                            ? $value[ 'id' ]
                            : '',
						'expenses_id' => $expenses_id,
						'order_id'    => $model->id,
                        'wagon'       => isset( $wagon[ $value[ 'id' ] ] )
                            ? $wagon[ $value[ 'id' ] ][ 'wagon_id' ]
                            : '',
                        'expenses' => isset( $expenses[ $expenses_id ] )
                            ? $expenses[ $expenses_id ]
                            : []
                    ],
                    'containers'          => $containers,
                    'wagons'              => $wagons,
                    'full_access'         => $full_access,
                    'is_expenses_visable' => $is_expenses_visable
                ] );

            }

        } ?>

	</div>

</div>

<script type="text/html" id="containers-template">

	<?= $this->render( 'templates/container', [
		'containers'          => $containers,
		'wagons'              => $wagons,
		'full_access'         => $full_access,
		'is_expenses_visable' => $is_expenses_visable,
        'model'               => [
            'order_id' => $model->id
        ]
	] ) ?>

</script>
