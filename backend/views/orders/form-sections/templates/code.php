<?php /***************************************************************
 *	 	    		    SINGLE CODE DETAIL SECTION 				     *
 *********************************************************************/

use yii\helpers\Html;

/* @var $model array             */
/* @var $full_access boolean     */
/* @var $expenses_access boolean */ ?>

<div class="option-section">

    <div class="option-index">
        <?= isset( $model[ 'index' ] )
            ? $model[ 'index' ] + 1
            : 1
        ?>
    </div>

	<div class="col-md-2 col-sm-2 col-xs-12">

		<?= Html::textInput(
			'codes[country][]',
			isset( $model[ 'country' ] )
				? $model[ 'country' ]
				: '',
			[
				'class'       => 'country-field form-control',
				'placeholder' => __( 'Country' )
			]
		) ?>

	</div>

	<div class="col-md-2 col-sm-2 col-xs-12">

		<?= Html::textInput(
			'codes[route][]',
			isset( $model[ 'route' ] )
				? $model[ 'route' ]
				: '',
			[
				'class'       => 'route-field form-control',
				'placeholder' => __( 'Route' )
			]
		) ?>

	</div>

    <div class="col-md-2 col-sm-2 col-xs-12">

		<?= Html::textInput(
			'codes[forwarder][]',
			isset( $model[ 'forwarder' ] )
				? $model[ 'forwarder' ]
				: '',
			[
				'class'       => 'forwarder-field form-control',
				'placeholder' => __( 'Forwarder' )
			]
		) ?>

    </div>

	<div class="col-md-2 col-sm-2 col-xs-12">

		<?= Html::textInput(
			'codes[code][]',
			isset( $model[ 'code' ] )
				? $model[ 'code' ]
				: '',
			[
				'class'       => 'code-field form-control',
                'placeholder' => __( 'Code' )
			]
		) ?>

	</div>

    <div class="col-md-1 col-sm-1 col-xs-12">

		<?= Html::textInput(
			'codes[code_rg_start][]',
			isset( $model[ 'code_rg_start' ] )
				? $model[ 'code_rg_start' ]
				: '',
			[
				'class'       => 'code-rg-start-field form-control',
				'placeholder' => __( 'Code Range From' )
			]
		) ?>

    </div>

    <div class="col-md-1 col-sm-1 col-xs-12">

		<?= Html::textInput(
			'codes[code_rg_end][]',
			isset( $model[ 'code_rg_end' ] )
				? $model[ 'code_rg_end' ]
				: '',
			[
				'class'       => 'code-rg-end-field form-control',
				'placeholder' => __( 'Code Range To' )
			]
		) ?>

    </div>

    <?php if ( $expenses_access ) { ?>

        <div class="col-md-1 col-sm-1 col-xs-12">

            <?= Html::textInput(
                'codes[surcharge][]',
                isset( $model[ 'surcharge' ] )
                    ? $model[ 'surcharge' ]
                    : '',
                [
                    'class'       => 'surcharge-field form-control',
                    'placeholder' => __( 'Surcharge' ),
                    'readonly'    => true
                ]
            ) ?>

        </div>

        <div class="col-md-1 col-sm-1 col-xs-12">

            <?= Html::textInput(
                'codes[rate][]',
                isset( $model[ 'rate' ] )
                    ? $model[ 'rate' ]
                    : '',
                [
                    'class'       => 'rate-field form-control',
                    'placeholder' => __( 'Rate' ),
                    'readonly'    => true
                ]
            ) ?>

        </div>

	<?php }  if ( $full_access ) { ?>

        <div class="remove-current-option" data-parent="option-section" data-translations="<?= htmlentities( json_encode( [
			'title'   => __( 'CONFIRMATION MODULE' ),
			'message' => __( 'Do you really want to delete this option?' )
		] ) ) ?>">
            <i class="fa fa fa-close"></i>
        </div>

	<?php } ?>

</div>