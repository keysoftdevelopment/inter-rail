<?php /***************************************************************
 *	 	    		    SINGLE CONTAINER SECTION 				     *
 *********************************************************************/

use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var $model array                           */
/* @var $containers common\models\Containers[] */
/* @var $wagons common\models\Wagons[]         */
/* @var $full_access boolean                   */
/* @var $is_expenses_visable boolean           */

//expenses for railway
$railway_expenses = array_reduce( ! empty( json_decode( $model[ 'expenses' ][ 'rf_details' ] ) )
    && isset( json_decode( $model[ 'expenses' ][ 'rf_details' ] )->rate )
    ? json_decode( $model[ 'expenses' ][ 'rf_details' ] )->rate
    : [], function( $result, $rate ) use ( $model ) {

        $rates = json_decode( $model[ 'expenses' ][ 'rf_details' ] );
        $key   = array_search( $rate, $rates->rate );

        if ( ! empty( $rate ) ) {

            $result += (float)$rate + ( isset( $rates->surchargep[ $key ] )
                ? (float)$rates->surchargep[ $key ]
                : 0
            );

        }

    return $result;

}, 0 ); ?>

<div class="option-section <?= isset( $model[ 'expenses_id' ] )
    ? 'expenses-id-' . $model[ 'expenses_id' ]
    : ''
?>">

	<?php if ( $is_expenses_visable ) { ?>

        <?= Html::hiddenInput( 'total_expenses', isset( $model[ 'expenses' ][ 'total' ] )
            ? $model[ 'expenses' ][ 'total' ]
            : 0, [
                'class' => 'total-expenses'
        ] ); ?>

		<?= Html::hiddenInput( 'Orders[expenses_ids][]', isset( $model[ 'expenses_id' ] )
			? $model[ 'expenses_id' ]
			: '', [
			'class' => 'order-expenses-ids'
		] ); ?>

    <?php } ?>

    <div class="option-index">

		<?= isset( $model[ 'index' ] )
			? $model[ 'index' ] + 1
			: 1
		?>

    </div>

	<div class="col-md-2 col-sm-2 col-xs-12">

		<label for="code" class="control-label">
			<?= __( 'Container' ) ?>
		</label>

	</div>

	<div class="col-md-4 col-sm-4 col-xs-12">

		<?= Html::dropDownList(
			'containers[id][]',
			isset( $model[ 'container' ] )
                ? $model[ 'container' ]
                : '',
			! empty( $containers )
                ?   ArrayHelper::map(
				    $containers,
				    'id',
				    'number'
			    )
                : [],
			[
				'class'    => 'container-field form-control select2-tags',
				'style'    => 'width:100%',
				'prompt'   => __( 'Choose option' ),
				'data-msg' => json_encode( [
                    'option'    => __( 'Choose option' ),
                    'no_result' => __( 'No result found, but you can add new option by typing it and pressing enter' )
                ] )
			] )
		?>

	</div>

    <div class="col-md-4 col-sm-4 col-xs-12">

		<?= Html::dropDownList(
			'containers[wagon][]',
			isset( $model[ 'wagon' ] )
				? $model[ 'wagon' ]
				: '',
			! empty( $wagons )
				?   ArrayHelper::map(
				$wagons,
				'id',
				'number'
			)
				: [],
			[
				'prompt'   => __( 'Choose option' ),
				'class'    => 'wagon-field form-control select2-tags',
				'style'    => 'width:100%',
				'data-msg' => json_encode( [
					'option'    => __( 'Choose option' ),
					'no_result' => __( 'No result found' )
				] )
			] )
		?>

    </div>

	<?php if ( $is_expenses_visable ) { ?>

        <div class="col-sm-2 col-md-2 col-xs-12" style="padding: 0px; margin: 0px;">

            <a href="#expenses-form" class="expenses-form-show" data-toggle="modal" data-target="" data-details="<?= htmlentities( json_encode( [
                'id' => isset( $model[ 'expenses_id' ] )
                    ? $model[ 'expenses_id' ]
                    : '',
                'container_id' => isset( $model[ 'container' ] )
                    ? $model[ 'container' ]
                    : '',
                'wagon_id' => isset( $model[ 'wagon' ] )
                    ? $model[ 'wagon' ]
                    : '',
                'order_id' => $model[ 'order_id' ]
            ] ) ) ?>">
                <?= __( 'Expenses' ) ?>
            </a>

        </div>

    <?php } if ( $full_access ) { ?>

        <div class="remove-current-option" data-parent="option-section" data-translations="<?= htmlentities( json_encode( [
			'title'   => __( 'CONFIRMATION MODULE' ),
			'message' => __( 'Do you really want to delete this option?' )
		] ) ) ?>">
            <i class="fa fa fa-close"></i>
        </div>

    <?php } ?>

</div>