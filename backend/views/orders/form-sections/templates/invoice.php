<?php /***************************************************************
 *	 	    		  SINGLE INVOICE DETAIL SECTION 				 *
 *********************************************************************/

use kartik\date\DatePicker;
use yii\helpers\Html;

/* @var $model array         */
/* @var $count integer       */
/* @var $full_access boolean */ ?>

<div class="option-section">

    <?php if ( isset( $model[ 'id' ] ) ) {

        echo Html::hiddenInput(
            'Orders[invoices][id][]',
            $model[ 'id' ]
        );

    } ?>

	<div class="option-index">

		<?= isset( $model[ 'index' ] )
			? $model[ 'index' ] + 1
			: 1
		?>

	</div>

	<div class="col-md-3 col-sm-3 col-xs-12">

		<?= Html::textInput(
			'Orders[invoices][number][]',
			isset( $model[ 'number' ] )
				? $model[ 'number' ]
				: '', [
				'class'       => 'invoice-number-field form-control',
				'data-count'  => $count,
				'disabled'    => true,
				'placeholder' => __( 'Number' )
			]
		) ?>

	</div>

	<div class="col-md-3 col-sm-3 col-xs-12">

		<?php if ( isset( $model[ 'date' ] )
            && ! empty( $model[ 'date' ] )
        ) { ?>

			<?= DatePicker::widget( [
				'name'    => 'Orders[invoices][date][]',
				'type'    => DatePicker::TYPE_INPUT,
				'value'   => date('d.m.Y', strtotime( $model[ 'date' ] ) ),
				'options' => [
					'placeholder' => $model[ 'label' ],
					'class'       => 'invoice-date-field form-control'
				],
				'pluginOptions' => [
					'autoclose' => true,
					'format'    => 'dd.mm.yyyy',
					'weekStart' => '1'
				]
			] ); ?>

		<?php } else { ?>

			<?= Html::textInput(
				'Orders[invoices][date][]',
				'',
				[
					'class'       => 'form-control invoice-date',
					'placeholder' => __( 'Invoice Date' )
				]
			); ?>

		<?php } ?>

	</div>

	<div class="col-md-3 col-sm-3 col-xs-12">

		<?= Html::textInput(
			'Orders[invoices][amount][]',
			isset( $model[ 'amount' ] )
				? $model[ 'amount' ]
				: '',
			[
				'class'       => 'invoice-amount-field form-control',
				'placeholder' => __( 'Amount' )
			]
		) ?>

	</div>

	<div class="col-md-3 col-sm-3 col-xs-12">

		<?= Html::dropDownList(
			'Orders[invoices][status][]',
			isset( $model[ 'status' ] )
				? $model[ 'status' ]
				: '',
			[
				'pending' => __( 'Pending' ),
				'paid'	  => __( 'Paid' )
			], [
				'class'	   => 'form-control invoice-status-field',
				'data-msg' => json_encode( [
					'option'    => __( 'Choose status' ),
					'no_result' => __( 'No result found' )
				] )
			] )
		?>

	</div>

    <?php if ( $full_access ) { ?>

        <div class="remove-current-option" data-parent="option-section" data-translations="<?= htmlentities( json_encode( [
			'title'   => __( 'CONFIRMATION MODULE' ),
			'message' => __( 'Do you really want to delete this option?' )
		] ) ) ?>">
            <i class="fa fa fa-close"></i>
        </div>

	<?php } ?>

</div>