<?php /***************************************************************
 *  	   	  FORM SECTION PART FOR ORDERS WAGONS DETAIL`s        	 *
 *       	  	    DISPLAYED AT THE ORDER EDIT PAGE  		 		 *
 *********************************************************************/

use yii\helpers\ArrayHelper;
use yii\helpers\Url;

use common\models\WagonsRelation;

/* @var $model common\models\Orders         */
/* @var $form yii\widgets\ActiveForm        */
/* @var $wagons common\models\Wagons        */
/* @var $expenses common\models\Expenses[]  */
/* @var $full_access boolean                */
/* @var $expenses_export boolean            */
/* @var $is_expenses_visable boolean        */

//current order wagons list
$order_wagons = $model->getWagons(); ?>

<div class="col-md-12 col-sm-12 col-xs-12">

	<div class="order-form-title">

		<h4>

			<a class="dropdown-toggle" data-toggle="collapse" href="#wagons" aria-expanded="false">
				<?= __( 'Wagons' ) ?>
			</a>

		</h4>

		<ul class="nav navbar-right panel_toolbox" style="min-width: 30px;">

			<li>
				<a class="dropdown-toggle" data-toggle="collapse" href="#wagons" aria-expanded="false">
					<i class="dropdown fa fa-chevron-down"></i>
				</a>
			</li>

			<?php if ( $full_access ) { ?>

                <li>

                    <a href="javascript:void(0)" class="add-item" id="add-wagon" data-toggle="dropdown" data-type="wagons" data-translations="<?= htmlentities( json_encode( [
						'title'   => __( 'Wagon was added successfully' ) . ' !!!',
						'message' => __( 'Wagon was added successfully' ) . '. ' . ( 'Please check the bottom of current' ) . ' ' . __( 'wagon list' ),
						'type'    => 'success'
					] ) ) ?>">
                        <i class="fa fa-plus"></i>
                    </a>

                </li>

			<?php } if ( $expenses_export ) { ?>

                <li>

                    <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        <i class="dropdown fa fa-file-excel-o"></i>
                    </a>

                    <ul class="dropdown-menu pull-right">

                        <li>

                            <a href="<?= Url::to( [
								'/expenses/exports',
								'type'     => 'wagons',
								'order_id' => $model->id
							] ) ?>" target="_blank">
                                <i class="dropdown fa fa-file-excel-o"></i> <?= __( 'Export' ) ?>
                            </a>

                        </li>

                        <li>

                            <a href="javascript:void(0)" class="import-xlsx-file" data-details="<?= htmlentities( json_encode( [
								'url' => '/wagons/import?order_id=' . $model->id
							] )) ?>">
                                <i class="dropdown fa fa-file-excel-o"></i> <?= __( 'Import' ) ?>
                            </a>

                        </li>

                    </ul>

                </li>

			<?php } ?>

		</ul>

	</div>

	<div class="order-form-content collapse wagons-container" id="wagons">

		<?php if ( ! empty( $order_wagons )
            || ! empty( $expenses )
        ) {

			//param initialization
			$index = 0;

			//wagon expenses details
			$wagon_expenses = WagonsRelation::find()
				-> where( [
					'foreign_id' => ArrayHelper::getColumn( $expenses, 'id' ),
					'type'       => 'expenses'
				] )
				-> indexBy( 'wagon_id' )
				-> asArray()
				-> all();

			foreach ( ! empty( $order_wagons )
                ? $order_wagons
                : $expenses as $key => $value
            ) {

			    //expenses id due to wagon expenses details
				$expenses_id = isset( $wagon_expenses[ $value[ 'id' ] ] )
					? $wagon_expenses[ $value[ 'id' ] ][ 'foreign_id' ]
					: $value[ 'id' ];

				echo $this->render( 'templates/wagon', [
					'model' => [
						'index'    => $index++,
						'order_id' => $model->id,
						'wagon'    => ! empty( $order_wagons )
							? $value[ 'id' ]
							: '',
						'expenses_id' => $expenses_id,
						'expenses'    => isset( $expenses[ $expenses_id ] )
							? $expenses[ $expenses_id ]
							: []
					],
					'wagons'              => $wagons,
					'full_access'         => $full_access,
                    'is_expenses_visable' => $is_expenses_visable
				] );

			}

		} ?>

	</div>

</div>

<script type="text/html" id="wagons-template">

	<?= $this->render( 'templates/wagon', [
		'wagons'              => $wagons,
		'full_access'         => $full_access,
		'is_expenses_visable' => $is_expenses_visable,
		'model'               => [
			'order_id' => $model->id
		]
	] ); ?>

</script>
