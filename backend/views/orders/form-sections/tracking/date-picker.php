<?php /***************************************************************
 *	 	    		    DATE PICKER HELPER SECTION 				     *
 *********************************************************************/

use yii\helpers\Html;

use kartik\date\DatePicker;

/* @var $model array */ ?>

<?php if ( ! empty( $model[ 'value' ] ) ) { ?>

	<?= DatePicker::widget( [
		'name'    => 'tracking[' .  $model[ 'type' ] . '][]',
		'type'    => DatePicker::TYPE_INPUT,
		'value'   => date('d.m.Y', strtotime( $model[ 'value' ] ) ),
		'options' => [
			'placeholder' => $model[ 'label' ],
			'class'       => 'form-control ' . $model[ 'class' ]
		],
		'pluginOptions' => [
			'autoclose' => true,
			'format'    => 'dd.mm.yyyy',
			'weekStart' => '1'
		]
	] ); ?>

<?php } else { ?>

	<?= Html::textInput(
		'tracking[' .  $model[ 'type' ] . '][]',
		'',
		[
			'class'       => 'form-control tracking-date ' . $model[ 'class' ],
			'placeholder' => $model[ 'label' ]
		]
	); ?>

<?php } ?>
