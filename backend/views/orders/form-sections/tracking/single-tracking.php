<?php /***************************************************************
 *	 	    		    SINGLE TRACKING DETAIL SECTION 				 *
 *********************************************************************/

use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var $model common\models\Tracking  */
/* @var $places common\models\Places[] */ ?>

<li class="single-tracking">

    <div class="tracking">

        <div class="tracking-top">

            <div class="place-name">

				<?= Html::textInput(
					'tracking[name][]',
					isset( $model[ 'name' ] )
						? $model[ 'name' ]
						: '',
					[
						'class'       => 'place-name-field',
						'placeholder' => __( 'Place Name' )
					]
				); ?>

            </div>

            <div class="tracking-status">

				<?= Html::dropDownList(
					'tracking[status][]',
					isset( $model[ 'status' ] )
						? $model[ 'status' ]
						: '',
					Yii::$app->common->statuses( 'tracking' ), [
						'data-msg' => htmlentities( json_encode( [
							'option'    => __( 'Choose status' ),
							'no_result' => __( 'No result found' )
						] ) )
					] )
				?>

            </div>

            <div class="remove-current-option" data-parent="single-tracking" data-translations="<?= htmlentities( json_encode( [
				'title'   => __( 'CONFIRMATION MODULE' ),
				'message' => __( 'Do you really want to delete this option?' )
			] ) ) ?>">
                <i class="fa fa fa-close"></i>
            </div>

        </div>

        <div class="tracking-details">

            <div class="col-sm-12 col-md-12 col-xs-12">

                <label>
                    <?= __( 'Arrival and departure' ) ?>
                </label>

                <div class="col-sm-6 col-md-6 col-xs-12">

                    <?= $this->render( 'date-picker', [
                        'model' => [
                            'type'  => 'arrival',
                            'label' => __( 'Arrival Date' ),
                            'class' => 'arrival-date',
                            'value' => isset( $model[ 'arrival' ] )  && ! empty( $model[ 'arrival' ] )
                                ? $model[ 'arrival' ]
                                : ''
                        ]
                    ] ) ?>

                </div>

                <div class="col-sm-6 col-md-6 col-xs-12">

					<?= $this->render( 'date-picker', [
						'model' => [
							'type'  => 'departure',
							'label' => __( 'Departure Date' ),
							'class' => 'departure-date',
							'value' => isset( $model[ 'departure' ] )  && ! empty( $model[ 'departure' ] )
								? $model[ 'departure' ]
								: ''
						]
					] ) ?>

                </div>

            </div>

            <div class="col-sm-12 col-md-12 col-xs-12" style="margin-top: 10px;">

                <label>
					<?= __( 'Location' ) ?>
                </label>

				<?= Html::dropDownList(
					'tracking[place_id][]',
					isset( $model[ 'container' ] )
						? $model[ 'container' ]
						: '',
					ArrayHelper::map( $places, 'id', 'city' ),
					[
                        'class'    => 'form-control select2_single',
                        'style'    => 'width:100%',
						'data-msg' => json_encode( [
							'option'    => __( 'Choose location' ),
							'no_result' => __( 'No result found' )
						] )
					] )
				?>


            </div>

            <div class="col-sm-12 col-md-12 col-xs-12" style="margin-top: 10px;">

				<?= Html::textarea(
					'tracking[description][]',
					isset( $model[ 'name' ] )
						? $model[ 'name' ]
						: '',
					[
						'class'       => 'description-field form-control',
						'placeholder' => __( 'Description' ),
                        'style'       => '    height: 50px;'
					]
				); ?>

            </div>

        </div>

    </div>

</li>