<?php /***************************************************************
 *  	   	  FORM SECTION PART FOR ORDERS GENERAL DETAILS           *
 *       	  I.E WILL BE DISPLAYED AT THE ORDER EDIT PAGE  		 *
 *********************************************************************/

use yii\helpers\ArrayHelper;

use common\models\Settings;
use common\models\Taxonomies;
use common\models\User;

/* @var $model common\models\Orders   */
/* @var $form yii\widgets\ActiveForm  */
/* @var $logistics array              */

//param initialization
$number_options = Settings::findOne( [
    'name' => 'order_options'
] );

//otstoy per day field displaying condition
$othc_display = Settings::findOne( [
    'name' => 'dthc_option'
] );

//contractor role name
$contractor_role = Settings::findOne( [
	'name' => 'contractor_role'
] );

//the list of general detail form information
$general_details = [
	'trans_from' => [
		'label'    => __(  'From' ),
		'id'       => 'transportation-from',
		'disabled' => true
	],
	'trans_to' => [
		'label'    => __(  'To' ),
		'id'       => 'transportation-to',
		'disabled' => true
	],
    'delivery_terms[from]' => [
		'label'   => __(  'Delivery Term From' ),
		'type'    => 'dropdown',
		'id'      => 'delivery-term-from',
        'options' => Yii::$app->common->transportationDetails( 'terms' )
    ],
	'delivery_terms[to]' => [
		'label'   => __(  'Delivery Term To' ),
        'type'    => 'dropdown',
		'id'      => 'delivery-term-to',
		'options' => Yii::$app->common->transportationDetails( 'terms' )
	],
	'cargo_tax' => [
		'label'    => __(  'Cargo Category' ),
		'id'       => 'cargo-taxonomy',
		'disabled' => false
	],
	'weight_dimensions[wagon]' => [
		'label' => __(  'Wagon Weight' ),
		'id'    => 'wagon-weight'
	],
	'weight_dimensions[net]' => [
		'label' => __(  'Weight Net Dimensions' ),
		'id'    => 'weight-net-dimensions'
	],
	'weight_dimensions[gross]' => [
		'label' => __(  'Weight Gross Dimensions' ),
		'id'    => 'weight-gross-dimensions'
	],
	'packages[quantity]'  => [
		'label' => __(  'Quantity of Packages' ),
		'id'    => 'packages-quantity',
	],
	'packages[type]' => [
		'label'   => __(  'Type of Packages' ),
		'id'      => 'packages-type',
        'type'    => 'dropdown',
        'options' => Yii::$app->common->transportationDetails( 'packages' )
	],
	'containers_type' => [
		'label'    => __(  'Container Type' ),
		'id'       => 'container-type',
		'disabled' => false
	],
	'wagons_type' =>  [
		'label'    => __(  'Wagon Type' ),
		'id'       => 'wagon-type',
		'disabled' => false
	]
]; ?>

    <div class="col-md-12 col-sm-12 col-xs-12">

        <div class="col-sm-3 col-md-3 col-xs-12">

            <?= $form
                -> field( $model, 'number' )
                -> dropDownList( is_null( $number_options )
                    ? []
                    : array_reduce( json_decode(
						$number_options->description
					), function( $order_number, $option ) use ( $model ) {

						if ( ! empty( $option ) )
							$order_number[ $option . '' . $model->id ] = $option;

						return $order_number;

					}, [] ), [
					    'class'       => 'form-control',
                        'id'          => 'order-number',
                        'placeholder' => __(  'Order Number' ),
                    ] )
                -> label( __(  'Order Number' ) );
            ?>

        </div>

        <div class="col-sm-3 col-md-3 col-xs-12">

			<?= $form
				-> field( $model, 'manager_id' )
				-> dropDownList( ArrayHelper::map(
					User::find()
						-> where( [
							'isDeleted' => false
						] )
						-> andWhere( [
							'NOT IN', 'id', array_merge(
                                Yii::$app->authManager->getUserIdsByRole( 'client' ),
                                Yii::$app->authManager->getUserIdsByRole( 'admin' )
                            )
						] )
						-> asArray()
						-> all(),
					'id',
					'username'
                ), [
					'class' => 'form-control',
					'prompt' => __(  'Assigned' )
				] )
				-> label( __(  'Assigned' ) );
			?>

        </div>

        <div class="col-sm-3 col-md-3 col-xs-12">

			<?= $form
				-> field( $model, 'status' )
				-> dropDownList( Yii::$app->common->statuses( 'order' ), [
					'class'  => 'form-control',
					'prompt' => __(  'Status' )
				] )
				-> label( __(  'Status' ) );
			?>

        </div>

        <div class="col-sm-3 col-md-3 col-xs-12">

			<?= $form
				-> field( $model, 'transportation' )
				-> dropDownList( Yii::$app->common->transportationTypes( 'transportation' ), [
					'class' => 'form-control',
					'prompt' => __(  'Type' )
				] )
				-> label( __(  'Type' ) );
			?>

        </div>

        <div class="col-sm-12 col-md-12 col-xs-12" style="padding: 0px;">

            <div class="col-sm-3 col-md-3 col-xs-12">

				<?= $form
					-> field( $model, 'contractors' )
					-> dropDownList( ArrayHelper::map( ( empty( $contractor_role )
					    ? []
						: User::find()
						    -> where( [
							    'id'       => Yii::$app->authManager->getUserIdsByRole( $contractor_role->description ),
								'isDeleted' => false
							] )
							-> all()
					), 'id', 'username' ), [
						'class'    => 'form-control',
                        'multiple' => 'multiple',
						'style'    => 'min-height: 150px;',
                        'value'    => $model->contractors
					] )
					-> label( __(  'Contractors' ) );
				?>

            </div>

            <div class="col-sm-9 col-md-9 col-xs-12">

				<?= $form
					-> field( $model, 'manager_note' )
					-> textarea( [
						'class'       => 'form-control',
						'id'          => 'manager-note',
						'placeholder' => __(  'Comment' )
					] )
					-> label( __(  'Comment' ) );
				?>

            </div>

        </div>

    </div>

    <div class="col-md-12 col-sm-12 col-xs-12">

        <div class="order-form-title">

            <h4>

                <a data-toggle="collapse" href="#general-form" aria-expanded="false">
                    <?= __( 'General Information' ) ?>
                </a>

            </h4>

            <ul class="nav navbar-right panel_toolbox" style="min-width: 30px;">

                <li>

                    <a class="dropdown-toggle" data-toggle="collapse" href="#general-form" aria-expanded="false">
                        <i class="dropdown fa fa-chevron-down"></i>
                    </a>

                </li>

            </ul>

        </div>

        <div class="order-form-content collapse" id="general-form">

            <?php foreach ( $general_details as $key => $detail ) {

                if ( $key == 'weight_dimensions[wagon]' && ! is_null( $model->wagons_type )
                    || $key == 'containers_type' && ! is_null( $model->containers_type  )
					|| $key == 'wagons_type' && ! is_null( $model->wagons_type  )
                    || ! in_array( $key, [ 'weight_dimensions[wagon]', 'containers_type', 'wagons_type' ] )
                ) { ?>

                    <div class="col-sm-4 col-md-4 col-xs-12">

                        <?php if ( in_array( $key, [ 'cargo_tax', 'containers_type', 'wagons_type' ] ) ) {

                            if ( ( $key == 'containers_type' && ! is_null( $model->containers_type ) )
                                || ( $key == 'wagons_type' && ! is_null( $model->wagons_type ) )
                                || $key == 'cargo_tax'
                            ) { ?>

                                <?= $form
                                    -> field( $model, $key )
                                    -> dropDownList( ArrayHelper::map(
                                        Taxonomies::find()
                                            -> where( [
                                                'type' => $key == 'wagons_type'
                                                    ? 'railway'
                                                    : str_replace( [ '_type', '_tax' ], '', $key ),
                                                'isDeleted' => false
                                            ] )
                                            -> asArray()
                                            -> all(),
                                        'id',
                                        'name'
                                    ), [
                                        'class'  => 'form-control',
                                        'id'     => $detail[ 'id' ],
                                        'prompt' => $detail[ 'label' ]
                                    ] )
                                    -> label(
                                        $detail[ 'label' ]
                                    );
                                ?>

                            <?php }

                        } else {

                            if ( isset( $detail[ 'type' ] ) ) {

                                echo $form
                                    -> field( $model, $key )
                                    -> dropDownList( $detail[ 'options' ], [
                                        'class'  => 'form-control',
                                        'id'     => $detail[ 'id' ],
                                        'prompt' => 'None'
                                    ] )
                                    -> label(
                                        $detail[ 'label' ]
                                    );

                            } else {

                                echo $form
                                    -> field( $model, $key )
                                    -> textInput( [
                                        'class'       => 'form-control',
                                        'id'          => $detail[ 'id' ],
                                        'placeholder' => $detail[ 'label' ],
                                        'disabled'    => $detail[ 'disabled' ]
                                    ] )
                                    -> label(
                                        $detail[ 'label' ]
                                    );

                            }

                        } ?>

                    </div>

                <?php  }

            } ?>

            <div class="col-sm-4 col-md-4 col-xs-12 order-dthc" style="display: <?= ! is_null( $othc_display )
            && $model->status == $othc_display->description
                ? 'block'
                : 'none'
            ?>" data-display="<?= htmlentities( json_encode( [
                'status' => ! is_null( $othc_display )
                    ? $othc_display->description
                    : ''
            ] ) ); ?>">

                <?= $form
					-> field( $model, 'dthc' )
					-> textInput( [
						'class'       => 'form-control',
						'id'          => 'dthc-field',
						'placeholder' => __( 'DTHC' ),
					] )
					-> label(
						__( 'DTHC' )
					);
                ?>

            </div>

            <div class="col-sm-4 col-md-4 col-xs-12 order-insurance">

                <label class="checkbox">

					<?= __( 'Insurance' ) ?>

                    <input name="Orders[insurance]" type="checkbox" value="<?= $model->insurance ?>" >

                    <span class="checkmark"></span>

                </label>

            </div>

        </div>

    </div>