<?php /***************************************************************
 *  	  FORM SECTION PART FOR ORDERS TERMINAL DETAILS SECTION      *
 *       	  I.E WILL BE DISPLAYED AT THE ORDER EDIT PAGE  		 *
 *********************************************************************/

use yii\helpers\ArrayHelper;

use common\models\Terminals;
use yii\helpers\Html;

/* @var $model common\models\Orders  */
/* @var $form yii\widgets\ActiveForm */

//the list of all available terminals
$terminals = Terminals::find()
    -> where( [
	    'isDeleted' => false
    ] )
    -> indexBy( 'id' )
    -> asArray()
    -> all();

//generated report link
$report_file = isset( $terminals[ $model->terminal_id ] )
    ? Yii::getAlias( '@backend_link' ) . '/reports/terminals/' . $model->number . '/' . (
	isset( $model->terminals[ 'vgm_document' ] ) && $model->terminals[ 'vgm_document' ] == 'on'
		? 'VGM-' . $terminals[ $model->terminal_id ][ 'name' ]
		: $terminals[ $model->terminal_id ][ 'name' ]
	) . '.pdf'
    : false; ?>

    <div class="col-md-12 col-sm-12 col-xs-12">

        <div class="order-form-title">

            <h4>

                <a class="dropdown-toggle" data-toggle="collapse" href="#terminal-form" aria-expanded="false">
                    <?= __( 'Terminals Details' ) ?>
                </a>

            </h4>

            <ul class="nav navbar-right panel_toolbox" style="min-width: 30px;">

                <li>

                    <a class="dropdown-toggle" data-toggle="collapse" href="#terminal-form" aria-expanded="false">
                        <i class="fa fa-chevron-down"></i>
                    </a>

                </li>

            </ul>

        </div>

        <div class="order-form-content collapse" id="terminal-form">

            <div class="col-sm-4 col-md-4 col-xs-12">

                <?= $form
                    -> field( $model, 'terminal_id' )
                    -> dropDownList( ArrayHelper::map(
                            $terminals,
                            'id',
                            'name'
                    ), [
                        'class'        => 'form-control',
                        'prompt'       => __( 'Terminal' ),
                        'data-details' => json_encode( $terminals )
                    ] )
                    -> label( __( 'Terminal' ) )
                ?>

            </div>

            <div class="col-sm-4 col-md-4 col-xs-12">

                <?= $form
                    -> field( $model, 'terminals[free_detention]' )
                    -> textInput( [
                        'class'       => 'form-control free-detention-field',
                        'placeholder' => __( 'Free Detention' )
                    ] )
                    -> label( __( 'Free Detention' ) )
                ?>

            </div>

            <div class="col-sm-4 col-md-4 col-xs-12">

                <?= $form
                    -> field( $model, 'terminals[status]' )
                    -> dropDownList( Yii::$app->common->transportationTypes(), [
                        'class'   => 'form-control',
                        'prompt'  => __( 'Status' )
                    ] )
                    -> label( __( 'Status' ) )
                ?>

            </div>

            <div class="col-sm-4 col-md-4 col-xs-12">

                <div class="form-group">

                    <label class="control-label" for="terminals-transportation">
                        <?= __( 'Transportation' ) ?>
                    </label>

                    <?= Html::dropDownList('transportation',
                        'import',
                        Yii::$app->common->transportationTypes( 'transportation' ), [
                            'class'   => 'form-control',
                            'prompt'  => __( 'Transportation' ),
                            'id'      => 'terminal-transportation'
                    ] ); ?>

                </div>

            </div>

            <div class="col-sm-4 col-md-4 col-xs-12" style="padding: 18px 0px;">

                <div class="form-group col-sm-3 col-md-3 col-xs-12" style="margin-top: 12px;">

                    <label class="checkbox-element">

                        <?= __( 'VGM' ) ?>

                        <input type="checkbox" name="Orders[terminals][vgm_document]" <?= isset( $model->terminals[ 'vgm_document' ] ) && $model->terminals[ 'vgm_document' ] == 'on'
                            ? 'checked'
                            : ''
                        ?> />

                        <span class="checkmark"></span>

                    </label>

                </div>

            </div>

            <div class="col-sm-4 col-md-4 col-xs-12">

                <label class="control-label">
					<?= __( 'Document' ) ?>
                </label>

                <a href="<?= $report_file && stripos(get_headers( $report_file )[0],"200" )
					? $report_file
					: 'javascript:void(0)'
				?>" <?= $report_file && stripos( get_headers( $report_file )[0],"200" )
					? 'target="_blank"'
					: ''
				?>>

					<?php if ( $report_file && stripos(get_headers( $report_file )[0],"200" ) ) { ?>

                        <i class="fa fa-file-zip-o"></i>
						<?= __( 'Download ZIP File' ) ?>

					<?php } else {
						echo __( 'No any attachment' );
					} ?>

                </a>


            </div>

            <div class="form-group col-sm-12 col-md-12 col-xs-12 pull-right generate-report text-right">

                <a href="javascript:void(0)" id="generate-report" class="btn btn-primary" data-details="<?= htmlentities( json_encode( [
					'order_id' => $model->id
				] ) ) ?>" data-message="<?= htmlentities( json_encode( [
					'title'  => __( '' ),
					'submit' => __( 'Send Report' ),
					'cancel' => __( 'Cancel' )
				] ) ) ?>" data-translations="<?= htmlentities( json_encode( [
					'title'   => __( 'ERROR' ),
					'message' => __( 'Select containers with store periods' )
				] ) ) ?>" >
                    <i class="fa fa-spinner fa-spin" style="display: none"></i>
					<?= __( 'Generate Report' ) ?>
                </a>

            </div>

        </div>

    </div>