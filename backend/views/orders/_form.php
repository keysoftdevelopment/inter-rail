<?php /***************************************************************
 * 		 	   	    FORM SECTION FOR ORDER PAGE                      *
 *********************************************************************/

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

use common\models\Tracking;

use backend\widgets\ExpensesForm;

/* @var $this yii\web\View                     */
/* @var $model common\models\Orders            */
/* @var $permissions array                     */
/* @var $blockTrain common\models\Logistics    */
/* @var $logistics array                       */
/* @var $expenses common\models\Expenses[]     */
/* @var $containers common\models\Containers[] */
/* @var $wagons common\models\Wagons[]         */
/* @var $expenses_sum integer                  */
/* @var $invoices common\models\Invoices[]     */

//the list of permissions for current user
$permission = isset( $permissions[ 'access' ]->description )
    ? json_decode( $permissions[ 'access' ]->description )
    : false;

$form = ActiveForm::begin( [
    'options' => [
        'class'   => 'form-horizontal form-label-left input_mask need-calculation',
        'enctype' => 'multipart/form-data',
    ],
    'id'                     => 'order-form',
    'enableClientValidation' => false
] ); ?>

    <?= $form
        -> field( $blockTrain, 'type', [
            'template' => '{input}',
			'options'  => [
				'tag' => false, // Don't wrap with "form-group" div
			]
        ] )
        -> hiddenInput( [
            'value' => 'blockTrain'
        ] )
        -> label( false );
    ?>

    <?= $form
        -> field( $model, 'gu_file_id', [
			'template' => '{input}',
			'options'  => [
				'tag' => false, // Don't wrap with "form-group" div
			]
		] )
        -> hiddenInput( [
            'id' => 'gu-file-id'
        ] )
        -> label( false );
    ?>

    <!-- order general form details -->
    <?= $this->render( 'form-sections/general', [
        'model'        => $model,
        'form'         => $form,
        'permissions'  => $permission,
        'current_role' => $permissions[ 'role' ],
        'logistics'    => isset( $logistics[ 'railway' ] )
			? ArrayHelper::map(
				$logistics[ 'railway' ],
				'id',
				'name'
			)
			: []
    ] ); ?>
    <!-- order general form details -->

    <!-- order container details form -->
    <?php if ( ( ! is_null( $model->containers_type ) && ! empty( $model->containers_type ) )
        && ( ( isset( $permission->containers ) && $permission->containers == 'on' ) || $permissions[ 'role' ] == 'admin' )
    ) {

        echo $this->render( 'form-sections/containers', [
			'model'       => $model,
			'form'        => $form,
			'containers'  => $containers,
			'wagons'      => $wagons,
			'expenses'    => $expenses,
			'full_access' => $permissions[ 'role' ] == 'admin'
            || ( isset( $permission->containers_and_wagons_add_delete_options ) && $permission->containers_and_wagons_add_delete_options == 'on' )
				? true
				: false,
            'expenses_export' => $permissions[ 'role' ] == 'admin'
			|| ( isset( $permission->containers_and_wagons_export_options ) && $permission->containers_and_wagons_export_options == 'on' )
				? true
				: false,
            'is_expenses_visable' => ( count( array_intersect_key(array_flip( [
				'expenses_general',
				'expenses_container',
				'expenses_sea_freight',
				'expenses_auto',
				'expenses_railway',
				'expenses_port_forwarding',
				'expenses_terminal',
				'expenses_terminal_costs',
				'expenses_invoices',
			] ), ( array )$permission ) ) > 0 ) || $permissions[ 'role' ] == 'admin'
		] );

	} ?>
    <!-- order container details form -->

    <!-- order wagon details form -->
    <?php if ( ( ! is_null( $model->wagons_type ) && ! empty( $model->wagons_type ) )
        &&  ( ( isset( $permission->wagons ) && $permission->wagons == 'on' ) || $permissions[ 'role' ] == 'admin' )
    ) {

        echo $this->render( 'form-sections/wagons', [
            'model'       => $model,
            'form'        => $form,
            'wagons'      => $wagons,
            'expenses'    => $expenses,
			'full_access' => $permissions[ 'role' ] == 'admin'
			|| ( isset( $permission->containers_and_wagons_add_delete_options ) && $permission->containers_and_wagons_add_delete_options == 'on' )
				? true
				: false,
			'expenses_export' => $permissions[ 'role' ] == 'admin'
			|| ( isset( $permission->containers_and_wagons_export_options ) && $permission->containers_and_wagons_export_options == 'on' )
				? true
				: false,
			'is_expenses_visable' => ( count( array_intersect_key(array_flip( [
			    'expenses_general',
				'expenses_container',
				'expenses_sea_freight',
				'expenses_auto',
				'expenses_railway',
				'expenses_port_forwarding',
				'expenses_terminal',
				'expenses_terminal_costs',
				'expenses_invoices'
            ] ), (array)$permission ) ) > 0 ) || $permissions[ 'role' ] == 'admin'
        ] );

    } ?>
    <!-- order wagon details form -->

    <!-- order customer form details -->
    <?php if ( ( isset( $permission->customer_information )
            && $permission->customer_information == 'on' )
        || $permissions[ 'role' ] == 'admin'
    ) {

        echo $this->render( 'form-sections/customer', [
            'model' => $model,
            'form'  => $form
        ] );

    } ?>
    <!-- order customer form details -->

    <!-- order block train form details -->
    <?php if ( ( isset( $permission->blockTrain )
            && $permission->blockTrain == 'on' )
        || $permissions[ 'role' ] == 'admin'
    ) {

        echo $this->render( 'form-sections/railway', [
            'model'      => $model,
            'form'       => $form,
            'blockTrain' => $blockTrain,
            'logistics'  => isset( $logistics[ 'railway' ] )
                ? ArrayHelper::map(
                    $logistics[ 'railway' ],
                    'id',
                    'name'
                )
                : []
        ] );

    } ?>
    <!-- order block train form details -->

    <!-- order code details form -->
    <?php if ( ( isset( $permission->code_details )
            && $permission->code_details == 'on' )
        || $permissions[ 'role' ] == 'admin'
    ) {

        echo $this->render( 'form-sections/codes', [
            'model'       => $model,
			'permissions' => $permission,
			'full_access' => $permissions[ 'role' ] == 'admin'
                || ( isset( $permission->code_details_add_delete_options ) && $permission->code_details_add_delete_options == 'on' )
                ? true
                : false,
            'expenses_access' => $permissions[ 'role' ] == 'admin'
			|| ( isset( $permission->code_details_expenses ) && $permission->code_details_expenses == 'on' )
				? true
				: false,
        ] );

    } ?>
    <!-- order code details form -->

    <!-- order auto form details -->
    <?php if ( ( isset( $permission->auto )
            && $permission->auto == 'on' )
        || $permissions[ 'role' ] == 'admin'
    ) {

        echo $this->render( 'form-sections/auto', [
			'model'      => $model,
			'form'       => $form,
			'containers' => $containers
        ] );

    } ?>
    <!-- order auto form details -->

    <!-- order terminal form details -->
    <?php if ( ( isset( $permission->terminals )
            && $permission->terminals == 'on' )
        || $permissions[ 'role' ] == 'admin'
    ) {

        echo $this->render( 'form-sections/terminal', [
            'model'      => $model,
            'form'       => $form,
            'containers' => $containers
        ] );

    } ?>
    <!-- order terminal form details -->

    <!-- order tracking form details -->
    <?php if ( ( isset( $permission->tracking )
            && $permission->tracking == 'on' )
        || $permissions[ 'role' ] == 'admin'
    ) {

        echo $this->render( 'form-sections/tracking/main', [
			'model'    => $model,
			'form'     => $form,
			'tracking' => Tracking::find()
				-> where( [
					'foreign_id' => $model->id,
					'type'       => 'order'
				] )
				-> asArray()
				-> all()
        ] );

    } ?>
    <!-- order tracking form details -->

    <!-- order invoices form details -->
    <?php if ( ( isset( $permission->invoices )
            && $permission->invoices == 'on' )
        || $permissions[ 'role' ] == 'admin'
    ) {

        echo $this->render( 'form-sections/invoices', [
			'model'    => $model,
			'form'     => $form,
			'invoices' => $invoices,
			'full_access' => $permissions[ 'role' ] == 'admin'
			|| ( isset( $permission->invoices_add_delete_options ) && $permission->invoices_add_delete_options == 'on' )
				? true
				: false,
        ] );

    } ?>
    <!-- order invoices form details -->

    <!-- order payments form details -->
    <?php if ( ( isset( $permission->payment )
		&& $permission->payment == 'on' )
	|| $permissions[ 'role' ] == 'admin'
    ) { ?>

        <div class="col-md-12 col-sm-12 col-xs-12">

            <div class="order-form-title">

                <h4>
                    <?= __( 'Payment Details' ) ?>
                </h4>

            </div>

            <div class="order-form-content payment-details rate-details-container">

                <ul>

                    <?php foreach ( [
                        'expenses'   => __( 'Expenses' ),
                        'zad'        => __( 'Zad' ),
                        'commission' => __( 'Commission' ),
                        'surcharge'  => __( 'Surcharge' ),
                        'total'      => __( 'Total Sum' )
                    ] as $key => $label ) { ?>

                        <li>

                            <label class="col-sm-3 col-md-3 col-xs-12">
                                <?= $label ?>
                            </label>

                            <div class="col-sm-7 col-md-7 col-xs-12">

                                <?php if ( $key =='expenses' ) { ?>

                                    <?= Html::textInput( $key, $expenses_sum, [
                                        'disabled' => true,
                                        'class'    => 'form-control price-field'
                                    ] ) ?>

                                <?php } else {

                                    $class = $key == 'total'
                                        ? 'total-sum'
                                        : 'price-field';
                                    ?>

                                    <?= $form
                                        -> field( $model, $key, [
                                            'template' => '{input}', // Leave only input (remove label, error and hint)
                                            'options'  => [
                                                'tag' => false, // Don't wrap with "form-group" div
                                            ]
                                        ] )
                                        -> textInput( [
                                            'class' => 'form-control ' . $key == 'surcharge'
                                                ? 'surcharge-field'
                                                : $class,
                                            'placeholder' => $label,
                                            'data-price'  => $key == 'surcharge'
                                                ? $model->surcharge
                                                : 0
                                        ] )
                                        -> label( false );
                                    ?>

                                <?php } ?>

                            </div>


                        </li>

                    <?php } ?>

                </ul>

            </div>

        </div>

    <?php } ?>
    <!-- order payments form details -->

    <div class="col-md-12 col-md-12 col-xs-12 text-center">

        <?= Html::submitButton( __(  'Update' ), [
            'class' => 'btn btn-primary'
        ] ); ?>

    </div>

<?php ActiveForm::end(); ?>

<?= ExpensesForm::widget() ?>