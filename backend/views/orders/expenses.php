<?php /***************************************************************
 *	  	   	    THE LIST OF ALL AVAILABLE EXPENSES PAGE              *
 *********************************************************************/

use common\models\User;
use common\widgets\Alert;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;

/* @var $this yii\web\View                        */
/* @var $searchModel common\models\OrdersSearch   */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $orders array                             */
/* @var $customers array                          */

// current page title
$this->title = __(  'Expenses' );

//get request short option
$request = Yii::$app->getRequest(); ?>

<div class="expenses-list">

	<div class="page-title">

		<div class="title_left">

			<h3>

				<?= $this->title ?>

			</h3>

		</div>

	</div>

	<div class="clearfix"></div>

	<div class="row">

		<div class="col-md-12 col-sm-12 col-xs-12">

            <div class="main-filters-container">

				<?php $form = ActiveForm::begin( [
					'id'      => 'main-filter-form',
					'method'  => 'get',
					'action'  => Url::to( [
						'/orders/expenses'
					] )
				] ); ?>

                    <ul class="list-filters">

                        <li class="filter-element">

                            <div class="form-group">

								<?= Html::dropDownList(
									'user_id',
									Yii::$app->getRequest()->getQueryParam( 'user_id' ),
									ArrayHelper::map( User::findAll( [
										'id'        => Yii::$app->authManager->getUserIdsByRole( 'client' ),
										'isDeleted' => false
									] ), 'id', function( $model ) {
										return $model[ 'first_name'] . ' ' . $model[ 'last_name' ];
									} ), [
										'prompt' => __(  'Select customer' ),
										'class'  => 'form-control'
									]
								); ?>

                            </div>

                        </li>

                        <li class="filter-element">

                            <div class="form-group">

								<?= Html::dropDownList(
									'transportation',
									Yii::$app->getRequest()->getQueryParam( 'transportation' ),
									Yii::$app->common->transportationTypes( 'transportation' ), [
										'prompt' => __(  'Select section' ),
										'class'  => 'form-control'
									]
								); ?>

                            </div>

                        </li>

                        <li class="filter-element">

                            <button class="btn btn-default">
								<?= __(  'Filter' ); ?>
                            </button>

                        </li>

                    </ul>

				<?php ActiveForm::end(); ?>

            </div>

			<div class="x_panel">

				<div class="x_content">

					<?= Alert::widget() ?>

					<table class="table table-striped projects">

						<thead>

							<tr>

								<th>
									#
								</th>

								<th>
									<?= __(  'Order Number' ) ?>
								</th>

								<th>
									<?= __(  'Section' ) ?>
								</th>

								<th>
									<?= __(  'Customer' ) ?>
								</th>

								<th>
									<?= __(  'Terminal Sum' ) ?>
								</th>

								<th>
									<?= __(  'Auto Sum' ) ?>
								</th>

								<th>
									<?= __(  'Rail sum' ) ?>
								</th>

								<th>
									<?= __(  'Sea Freight Sum' ) ?>
								</th>

								<th>
									<?= __(  'Port Forwarding' ) ?>
								</th>

								<th>
									<?= __(  'Total' ) ?>
								</th>

								<th></th>

							</tr>

						</thead>

						<tbody>

							<?php if ( !empty( $dataProvider->models ) ) {

								foreach ( $dataProvider->models as $model ) {

									$railway_sum = 0;

									if ( ! empty( $model->rf_details ) ) {

									    $rf_rates = json_decode( $model->rf_details );

									    if ( isset( $rf_rates->rate ) )
									        foreach ( $rf_rates->rate as $key => $value )
											    $railway_sum = $railway_sum + (float)$value + ( isset( $rf_rates->surcharge[ $key ] )
                                                    ? (float)$rf_rates->surcharge[ $key ]
                                                    : 0
                                                );

                                    } ?>

									<tr>

										<td>
											#
										</td>

										<td>

											<?= Html::a( isset( $orders[ $model->order_id ] )
												? $orders[ $model->order_id ][ 'number' ]
												: $model->order_id, [
												'update',
												'id' => $model->order_id
											], [
												'role' => "button"
											] ) ?>

										</td>

										<td></td>

										<td>

											<?= isset( $customers[ $orders[ $model->order_id ][ 'user_id' ] ] )
												? $customers[ $orders[ $model->order_id ][ 'user_id' ] ][ 'first_name' ] . ' ' . $customers[ $orders[ $model->order_id ][ 'user_id' ] ][ 'last_name' ]
												: __( 'Unknown Customer' )
											?>

										</td>

										<td>
											<?= (float)$model->storage + (float)$model->lading + (float)$model->ex_price + (float)$model->im_price + (float)$model->unloading + (float)$model->terminal_surcharge ?>
										</td>

										<td>
											<?= (float)$model->auto_rate + (float)$model->auto_surcharge ?>
										</td>

										<td>
											<?= (float)$model->rf_rental + (float)$model->rf_commission + $railway_sum ?>
										</td>

										<td>
											<?= (float)$model->sf_rate + (float)$model->dthc + (float)$model->othc + (float)$model->bl + (float)$model->sf_surcharge ?>
										</td>

										<td>
											<?= (float)$model->pf_rate + (float)$model->pf_surcharge + (float)$model->pf_commission ?>
										</td>

										<td>
											<?= $model->total ?>
										</td>

										<td class="text-right" style="position: relative; vertical-align: middle;">

											<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" style="font-size: 18px;">
												<i class="fa fa-ellipsis-v" style="color: #00a2b3;"></i>
											</a>

											<ul class="dropdown-menu pull-right">

												<li style="padding: 5px;">

													<?= Html::a( '<i class="fa fa-pencil"></i> ' .  __(  'Edit' ), [
														'update',
														'id' => $model->id
													] ); ?>

												</li>

												<li style="padding: 5px;">

													<?= Html::a( '<i class="fa fa-trash"> </i> '. __(  'Delete' ), [
														'delete',
														'id' => $model->id
													], [
														'data'  => [
															'confirm' => __(  'Do you really want to delete this order?' ),
															'method'  => 'post',
														]
													] ); ?>

												</li>

											</ul>

										</td>

									</tr>

								<?php }

							} else {

								echo '<tr>';

								echo '<td colspan="10">
                                                ' . __(  'There is no expenses yet' ) . '
                                            </td>';

								echo '</tr>';

							} ?>

						</tbody>

					</table>

				</div>

				<div class="pagination-container">

					<?= LinkPager::widget( [
						'pagination' => $dataProvider->pagination,
					] ); ?>

				</div>

			</div>

		</div>

	</div>

</div>