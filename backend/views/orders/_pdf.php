<?php /***************************************************************
 *	  	   	               PDF DETAILS FOR TERMINAL                  *
 *********************************************************************/

use yii\helpers\ArrayHelper;

/* @var $model common\models\Orders       */
/* @var $dates array                      */
/* @var $terminal common\models\Terminals */
/* @var $is_vgm string                    */
/* @var $status string                    */
/* @var $containers array                 */ ?>

<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">

        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Bootstrap -->
        <link href="<?= Yii::getAlias( '@web' ) ?>/css/bootstrap.min.css" rel="stylesheet" />
        <!-- Bootstrap -->

    </head>

    <body style="position: relative;">

        <!-- header -->
        <header>

            <section class="top">

                <div class="container" style="width: 970px;">

                    <div class="row">

                        <div class="col-sm-3 col-md-3 col-xs-3" style="float: left; width: 35%;">

                            <p style="font-size: 12px">
                                <b>Representative Office in Tashkent<br>
                                    InterRail Services AG</b><br>
                                2 Rakatboshi street<br>
                                Yakkasaroy district<br>
                                100031 Tashkent, Uzbekistan<br>
                                Tel/Fax: +998 71 2525600<br>
                                Website: www.interrail.ag<br>
                            </p>

                        </div>

                        <div class="col-sm-9 col-md-9 col-xs-9" style="width: 55%; float: right; text-align: right; padding-top: 20px;">

                            <a href="javascript:void(0)" class="logo">
                                <img src="<?= Yii::getAlias( '@web' ) ?>/images/logo.jpg" style="max-width: 80%;" />
                            </a>

                        </div>

                    </div>

                </div>

            </section>

        </header>
        <!-- header -->

        <section class="container-details">

            <div class="container" style="width: 970px;">

                <div class="row">

                    <h4 class="title text-center" style="font-size: 14px; font-weight: bold;">
                        <?= __( 'Request Order For' ) ?> <?= $is_vgm == "true"
                            ? __( 'VGM' )
                            :  __( $model->transportation )
                        ?> <?= __( 'large-capacity of containers' ) ?>
                        <?= __( 'of' ) ?> <?= date( 'd.m.Y', strtotime( $dates[ 'from' ] ) ) ?> № <?= $model->id ?>

                    </h4>

                    <?php if ( $is_vgm != "true" ) { ?>

                        <p style="margin-top: 30px;">

                            <?= __( 'Ask permission to' ) ?> <?= $model->transportation == 'import'
                                ? __( 'exportation' )
                                : __( 'delivery' )
                            ?> <?= __( 'containers given below' ) ?> <?= $model->transportation == 'import'
                                ? __( 'from terminal' )
                                : __( 'to main terminal' )
                            ?> <?= ! is_null( $terminal )
                                ? $terminal->name
                                : __( 'Terminal not chosen' )
                            ?> :

                        </p>

                        <table style="float: left; width: 100%; font-size: 14px; margin-top: 30px;">

                            <thead>

                                <tr style="border-top: 2px solid;">

                                    <th style="padding: 5px; border: 1px solid black; text-align: center;">
                                        №
                                    </th>

                                    <th style="padding: 5px; border: 1px solid black; text-align: center;">
                                        <?= __( 'Container number' ) ?>
                                    </th>

                                    <th style="padding: 5px; border: 1px solid black; text-align: center;">
                                        <?= __( 'Type sizes' ) ?> ( 20, 40 <?= __( 'or' ) ?> 45 )
                                    </th>

                                    <th style="padding: 5px; border: 1px solid black; text-align: center;">

                                        <?= $model->transportation == 'import'
                                            ? __( 'State' ) . ' ( ' . __( 'laden' ) . ' ' . __( 'or' ) . ' ' . __( 'empty' ) . ' ) '
                                            : __( 'Vehicle number' )
                                        ?>

                                    </th>

                                    <th style="padding: 5px; border: 1px solid black; text-align: center;">
                                        <?= __( 'Owner' ) ?>
                                    </th>

                                </tr>

                            </thead>

                            <tbody>

                                <?php if ( ! empty( $containers[ 'details' ] ) ) {

                                    foreach ( $containers[ 'details' ] as $key => $container ) {

                                        if ( in_array( $container[ 'id' ], $dates[ 'container_ids' ] ) ) { ?>

                                            <tr>

                                                <td style="padding: 5px; border: 1px solid black; text-align: center;">
                                                    <?= $key + 1 ?>
                                                </td>

                                                <td style="padding: 5px; border: 1px solid black; text-align: center;">
                                                    <?= $container[ 'number' ] ?>
                                                </td>

                                                <td style="padding: 5px; border: 1px solid black; text-align: center;">

                                                    <?= isset( $containers[ 'types' ][ $container[ 'id' ] ] ) && ! empty( $containers[ 'types' ][ $container[ 'id' ] ] )
                                                        ? $containers[ 'types' ][ $container[ 'id' ] ][ 'name' ]
                                                        : __( 'Type not chosen' )
                                                    ?>

                                                </td>

                                                <td style="padding: 5px; border: 1px solid black; text-align: center;">

                                                    <?= ! empty( $model->auto ) && isset( json_decode( $model->auto )->{ $model->transportation == 'import'
                                                        ? 'status'
                                                        : 'transport_no'
                                                    }[ $key ] )
                                                        ? json_decode( $model->auto )->{
                                                            $model->transportation == 'import' ? 'status' : 'transport_no'
                                                        }[ $key ]
                                                        : __( $status )
                                                    ?>

                                                </td>

                                                <td style="padding: 5px; border: 1px solid black; text-align: center;">
                                                    INTERRAIL SERVICES AG
                                                </td>

                                            </tr>

                                        <?php }
                                    }

                                } else {

                                    echo '<tr>
                                        <td colspan="5">
                                            ' . __( 'Containers was not chosen for current order' ) . '
                                        </td>
                                    </tr>';

                                } ?>

                            </tbody>

                        </table>

					<?php } else { ?>

                        <p>
                            “InterRail Services AG” <?= __( 'requests you to weight next containers' ) ?> <?= implode( ',', ArrayHelper::getColumn( $containers[ 'details' ], 'number' ) ) ?>
                            <?= __( 'destination' ) ?> : <?= $model->trans_from . ' - ' . $model->trans_to ?> <?= __( 'and issue VGM certificates after arriving at the terminal ' ) ?> <?= ! is_null( $terminal )
								? $terminal->name
								: __( 'Terminal not chosen' )
							?>.
                        </p>

					<?php } ?>

                    <ul style="float: left; width: 100%; list-style: none; padding-left: 0px; font-size: 16px; font-weight: bold; margin-top: 40px;">

                        <li style="float: left; line-height: 49px; width: 33%;">
                            <?= __( 'Best Regards' ) ?> <br>
                            <?= __( 'Chief' ) ?>
                        </li>

                        <li style="text-align: right; float: left; width: 34%;">
                            <img style="max-width: 70%;" src="<?= Yii::getAlias( '@web' ) ?>/images/interrail-stamp.jpg" />
                        </li>

                        <li style="float: left; width: 33%; padding-top: 50px;">
                            Сулейманов Д.А.
                        </li>

                    </ul>

                </div>

            </div>

        </section>

        <!-- footer -->
        <footer style="font-size: 9px; position: absolute; bottom: 0px; height: 50px;">

            <div class="container" style="width: 970px;">

                <div class="row">

                    <div style="width: 30%; float: left; margin-right: 20px;">
                        Wir arbeiten ausschliesslich
                        aufgrund unserer Transportübernahmebedingungen.
                    </div>

                    <div style="width: 16%; float: left; margin-right: 20px;">
                        Credit Suisse
                        CH-9001 St. Gallen
                        SWIFT: CHRESCHZZ90A
                    </div>

                    <div style="width: 25%; float: left; margin-right: 20px;">
                        USD CH05 0483 5044 2540 8200 0 <br>
                        CHF CH39 0483 5044 2540 8100 0 <br>
                        EUR CH75 0483 5044 2540 8200 1 <br>
                    </div>

                    <div style="width: 18%; float: left; padding-right: 5px;">
                        Tel.: +41 71 227 15 40 <br>
                        Fax: +41 71 227 15 30 <br>
                        E-Mail: info@interrail.ag <br>
                    </div>

                    <div style="width: 10%; float: left;">
                        Website<br>
                        www.interrail.ag
                    </div>

                </div>

            </div>

        </footer>
        <!-- footer -->

    </body>

</html>