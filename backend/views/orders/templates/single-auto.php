<?php /***************************************************************
 *	 	    		    SINGLE AUTO DETAIL SECTION 				     *
 *********************************************************************/

use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var $model array	                       */
/* @var $containers common\models\Containers[] */

?>

<div class="option-section">

    <div class="option-index">
		<?= isset( $model[ 'index' ] )
			? $model[ 'index' ] + 1
			: 1
		?>
    </div>

	<div class="col-md-3 col-sm-3 col-xs-12">

		<?= Html::dropDownList(
			'Orders[auto][container][]',
			isset( $model[ 'container' ] )
				? $model[ 'container' ]
				: '',
			ArrayHelper::map(
				$containers,
				'id',
				'number'
			),
			[
				'class'    => 'container-auto-field form-control',
				'style'    => 'width:100%',
				'data-msg' => __( 'Choose container' )
			] )
		?>

	</div>

	<div class="col-md-3 col-sm-3 col-xs-12">

		<?= Html::textInput(
			'Orders[auto][transport_no][]',
			isset( $model[ 'transport_no' ] )
				? $model[ 'transport_no' ]
				: '',
			[
				'class'       => 'transport-number-field form-control',
				'placeholder' => __( 'Transport Number' )
			]
		) ?>

	</div>

    <div class="col-md-3 col-sm-3 col-xs-12">

		<?= Html::dropDownList(
			'Orders[auto][status][]',
			isset( $model[ 'status' ] )
				? $model[ 'status' ]
				: '',
			Yii::$app->common->transportationTypes( 'status' ),
			[
				'class'    => 'term-auto-field form-control',
				'style'    => 'width:100%',
				'data-msg' => __( 'Choose status' )
			] )
		?>

    </div>

	<div class="col-md-3 col-sm-3 col-xs-12">

		<?= Html::textInput(
			'Orders[auto][owner][]',
			isset( $model[ 'owner' ] )
				? $model[ 'owner' ]
				: '',
			[
				'class'       => 'owner-field form-control',
				'placeholder' => __( 'Owner' )
			]
		) ?>

	</div>

    <div class="remove-current-option" data-parent="option-section">
        <i class="fa fa fa-close"></i>
    </div>

</div>