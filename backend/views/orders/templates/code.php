<?php /***************************************************************
 *	 	    		    SINGLE CODE DETAIL SECTION 				     *
 *********************************************************************/

use yii\helpers\Html;

/**/ ?>

<div class="option-section">

    <div class="option-index">
        <?= isset( $model[ 'index' ] )
            ? $model[ 'index' ] + 1
            : 1
        ?>
    </div>

	<div class="col-md-3 col-sm-3 col-xs-12">

		<?= Html::textInput(
			'Orders[codes][country][]',
			isset( $model[ 'country' ] )
				? $model[ 'country' ]
				: '',
			[
				'class'       => 'country-field form-control',
				'placeholder' => __( 'Country' )
			]
		) ?>

	</div>

	<div class="col-md-3 col-sm-3 col-xs-12">

		<?= Html::textInput(
			'Orders[codes][route][]',
			isset( $model[ 'route' ] )
				? $model[ 'route' ]
				: '',
			[
				'class'       => 'route-field form-control',
				'placeholder' => __( 'Route' )
			]
		) ?>

	</div>

    <div class="col-md-3 col-sm-3 col-xs-12">

		<?= Html::textInput(
			'Orders[codes][forwarder][]',
			isset( $model[ 'forwarder' ] )
				? $model[ 'forwarder' ]
				: '',
			[
				'class'       => 'forwarder-field form-control',
				'placeholder' => __( 'Forwarder' )
			]
		) ?>

    </div>

	<div class="col-md-3 col-sm-3 col-xs-12">

		<?= Html::textInput(
			'Orders[codes][code][]',
			isset( $model[ 'code' ] )
				? $model[ 'code' ]
				: '',
			[
				'class'       => 'code-field form-control',
                'placeholder' => __( 'Code' )
			]
		) ?>

	</div>

	<div class="remove-current-option" data-parent="option-section">
        <i class="fa fa fa-close"></i>
	</div>

</div>

