<?php /***************************************************************
 *         		 	   	  ORDER FILTERS SECTION                      *
 *********************************************************************/

/** @var $searchModel common\models\OrdersSearch */

use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;
use yii\helpers\Html;
use yii\helpers\Url;

use common\models\Logistics;
use common\models\Places;
use common\models\RailwayCodes;
use common\models\Settings;
use common\models\Terminals;
use common\models\User;
use common\models\Wagons;

//contractor role name
$contractor_role = Settings::findOne( [
    'name' => 'contractor_role'
] );

//get request short option
$get_request = Yii::$app->getRequest();
$form 		 = ActiveForm::begin( [
	'id'      => 'orders-filter-form',
	'method'  => 'get',
	'action'  => Url::to( [
		'/orders'
	] )
] ); ?>

    <?= Html::hiddenInput(
    'status', $searchModel->status
    ); ?>

	<ul class="list-filters">

        <li class="col-sm-2 col-md-2 col-xs-12" style="padding-right: 0px;">

			<?= $form
				-> field( $searchModel, 'user_id' )
				-> dropDownList( ArrayHelper::map( User::findAll( [
					'id'        => Yii::$app->authManager->getUserIdsByRole( 'client' ),
					'isDeleted' => false
				] ), 'id', function( $model ) {
					return $model[ 'first_name'] . ' ' . $model[ 'last_name' ];
				} ), [
					'prompt' => __(  'Select Customer' ),
					'class'  => 'form-control'
				] )
				-> label( false );
			?>

        </li>

        <li class="col-sm-2 col-md-2 col-xs-12" style="padding-right: 0px;">

			<?= $form
				-> field( $searchModel, 'terminal_id' )
				-> dropDownList( ArrayHelper::map(
                    Terminals::findAll( [
                        'isDeleted' => false
                    ] ),
                    'id',
                    'name'
                ), [
					'prompt' => __(  'Select Terminal' ),
					'class'  => 'form-control'
				] )
				-> label( false );
			?>

        </li>

        <li class="col-sm-2 col-md-2 col-xs-12" style="padding-right: 0px;">

			<?= $form
				-> field( $searchModel, 'transportation' )
				-> dropDownList( Yii::$app->common->transportationTypes( 'transportation' ), [
					'prompt' => __(  'Select section' ),
					'class'  => 'form-control'
				] )
				-> label( false );
			?>

        </li>

        <li class="col-sm-2 col-md-2 col-xs-12" style="padding-right: 0px;">

			<?= $form
				-> field( $searchModel, 'contractors' )
				-> dropDownList( ArrayHelper::map( ( empty( $contractor_role )
                    ? []
                    : User::find()
                        -> where( [
                            'id'       => Yii::$app->authManager->getUserIdsByRole( $contractor_role->description ),
					        'isDeleted' => false
				        ] )
                        -> all()
                    ),
                    'id', function( $model ) {
					    return $model[ 'first_name'] . ' ' . $model[ 'last_name' ];
				    }
				), [
					'prompt' => __(  'Select Contractor' ),
					'class'  => 'form-control'
				] )
				-> label( false );
			?>

        </li>

        <li class="col-sm-2 col-md-2 col-xs-12" style="padding-right: 0px;">

            <div class="form-group">

				<?= Html::dropDownList(
					'wagon',
					$get_request->getQueryParam( 'wagon' ),
					ArrayHelper::map( Wagons::findAll( [
						'isDeleted' => false
					] ), 'id', 'number' ), [
						'prompt' => __(  'Select wagon' ),
						'class'  => 'form-control'
					]
				); ?>

            </div>

        </li>

        <li class="col-sm-2 col-md-2 col-xs-12" style="padding-right: 0px;">

            <div class="form-group">

				<?= Html::dropDownList(
					'blockTrain',
					$get_request->getQueryParam( 'blockTrain' ),
					ArrayHelper::map( Logistics::findAll( [
						'type' => 'blockTrain'
					] ), 'id', 'transport_no' ), [
						'prompt' => __(  'Select train number' ),
						'class'  => 'form-control'
					]
				); ?>

            </div>

        </li>

        <li class="col-sm-3 col-md-3 col-xs-12" style="padding-right: 0px;">

			<?= $form
				-> field( $searchModel, 'trans_from' )
				-> dropDownList( ArrayHelper::map( Places::find()
					-> where( [
						'!=', 'name', ' '
					] )
					-> all(), 'name', 'name' ), [
					'prompt' => __(  'Route From' ),
					'class'            => 'form-control select2_single',
					'style'            => 'width: 100%;',
					'data-live-search' => 'true',
					'data-default'     => $searchModel->trans_from,
					'data-msg'         => json_encode( [
						'option'    => __( 'Route' ),
						'no_result' => __( 'No result found' )
					] )
				] )
				-> label( false );
			?>

        </li>

        <li class="col-sm-2 col-md-2 col-xs-12" style="padding-right: 0px;">

			<?= $form
				-> field( $searchModel, 'trans_to' )
				-> dropDownList( ArrayHelper::map( Places::find()
                    -> where( [
						'!=', 'name', ' '
                    ] )
                    -> all(), 'name', 'name' ), [
					'prompt' => __(  'Route To' ),
					'class'            => 'form-control select2_single',
					'style'            => 'width: 100%;',
					'data-live-search' => 'true',
					'data-default'     => $searchModel->trans_to,
					'data-msg'         => json_encode( [
						'option'    => __( 'Route' ),
						'no_result' => __( 'No result found' )
					] )
				] )
				-> label( false );
			?>

        </li>

        <li class="col-sm-2 col-md-2 col-xs-12" style="padding-right: 0px;">

            <?= Html::dropDownList(
                'forwarder',
                $get_request->getQueryParam('forwarder' ),
                ArrayHelper::map( RailwayCodes::find()
                    -> where( [
                        '!=', 'forwarder', ''
                    ] )
                    -> all(), 'forwarder', 'forwarder'
                ), [
                    'class' => 'form-control',
					'prompt' => __(  'Select Forwarder' ),
                ]
            ) ?>

        </li>

        <li class="col-sm-1 col-md-1 col-xs-12" style="padding-right: 0px;">

			<?= DatePicker::widget( [
				'name'    => 'date_from',
				'type'    => DatePicker::TYPE_INPUT,
				'value'   => ! is_null( $get_request->getQueryParam('date_from' ) )
					? date('d.m.Y', strtotime( $get_request->getQueryParam('date_from' ) ) )
					: date('d.m.Y' ) ,
				'options' => [
					'placeholder' => __(  'Date From' ),
					'class'       => 'order-filter-datetime form-control'
				],
				'pluginOptions' => [
					'autoclose' => true,
					'format'    => 'dd.mm.yyyy',
					'weekStart' => '1'
				]
			] ); ?>

        </li>

        <li class="col-sm-1 col-md-1 col-xs-12" style="padding-right: 0px;">

			<?= DatePicker::widget( [
				'name'    => 'date_to',
				'type'    => DatePicker::TYPE_INPUT,
				'value'   => ! is_null( $get_request->getQueryParam('date_to' ) )
					? date('d.m.Y', strtotime( $get_request->getQueryParam('date_to' ) ) )
					: date('d.m.Y' ) ,
				'options' => [
					'placeholder' => __(  'Date To' ),
					'class'       => 'order-filter-datetime form-control'
				],
				'pluginOptions' => [
					'autoclose' => true,
					'format'    => 'dd.mm.yyyy',
					'weekStart' => '1'
				]
			] ); ?>

        </li>

        <li class="col-sm-3 col-md-3 col-xs-12" style="padding-right: 0px;">

            <button class="btn btn-default" style="width: 100%">
				<?= __(  'Filter' ); ?>
            </button>

        </li>

	</ul>

<?php ActiveForm::end(); ?>