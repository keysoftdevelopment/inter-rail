<?php /***************************************************************
 *	     		 	   	  UPDATE ORDER PART                          *
 *********************************************************************/

/* @var $this yii\web\View                     */
/* @var $model common\models\Orders            */
/* @var $blockTrain common\models\Logistics    */
/* @var $expenses common\models\Expenses[]     */
/* @var $logistics array                       */
/* @var $containers common\models\Containers[] */
/* @var $wagons common\models\Wagons[]         */
/* @var $expenses_sum common\models\Expenses[] */
/* @var $invoices common\models\Invoices[]     */
/* @var $permissions array                     */

// current page title
$this->title = __(  'Update Order' ) . ' - ' . ( ! is_null( $model->number )
    ? $model->number
    : $model->id
); ?>

    <div class="col-md-12 col-sm-12 col-xs-12 update-order-block">

        <!-- title section -->
        <div class="page-title">

            <div class="title_left">

                <h3>
                    <?= $this->title ?>
                </h3>

            </div>

        </div>
        <!-- title section -->

        <!-- form section -->
        <div class="col-md-12 col-sm-12 col-xs-12 update-item-form" style="padding: 0px;">

            <div class="x_panel">

                <div class="x_content">

					<?= $this->render( '_form', [
						'model'        => $model,
						'permissions'  => $permissions,
                        'blockTrain'   => $blockTrain,
                        'expenses'     => $expenses,
                        'logistics'    => $logistics,
                        'containers'   => $containers,
                        'wagons'       => $wagons,
						'invoices'     => $invoices,
                        'expenses_sum' => isset( $expenses_sum[0] )
                            ? $expenses_sum[0]
                            : 0
					] ); ?>

                </div>

            </div>

        </div>
        <!-- form section -->

    </div>
