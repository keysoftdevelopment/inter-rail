<?php /***************************************************************
 *         		 	   	  ORDER EXPORT SECTION                       *
 *********************************************************************/

/** @var $model common\models\OrdersSearch */

use yii\helpers\Url;

//get request short option
$get_request = Yii::$app->getRequest(); ?>

<div class="export-xlsx">

	<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">

		<i class="fa fa-file-text"></i>
		<?= __( 'Export to Excel' ) ?>

	</a>

	<ul class="dropdown-menu" id="excel-export-links">

		<li>

			<a href="<?= Url::to( [
				'/orders/exports',
				'type'  			           => 'balance_report',
				'OrdersSearch[user_id]'        => $model->user_id,
				'OrdersSearch[terminal_id]'    => $model->terminal_id,
				'OrdersSearch[transportation]' => $model->transportation,
				'OrdersSearch[contractors]'    => $model->contractors,
				'OrdersSearch[trans_from]'     => $model->trans_from,
				'OrdersSearch[trans_to]'       => $model->trans_to,
				'status'                       => $model->status,
				'wagon' 				       => $get_request->getQueryParam( 'wagon' ),
				'blockTrain' 			       => $get_request->getQueryParam( 'blockTrain' ),
				'date_from' 			       => $get_request->getQueryParam( 'date_from' ),
				'date_to' 				       => $get_request->getQueryParam( 'date_to' ),
                'forwarder'                    => $get_request->getQueryParam( 'forwarder' )
			] ) ?>" target="_blank">

				<i class="fa fa-file-excel-o"></i>
				<?= __( 'Balance Report' ) ?>

			</a>

		</li>

        <li>

            <a href="<?= Url::to( [
				'/orders/exports',
				'type'  			           => 'balance_detailed_report',
				'OrdersSearch[user_id]'        => $model->user_id,
				'OrdersSearch[terminal_id]'    => $model->terminal_id,
				'OrdersSearch[transportation]' => $model->transportation,
				'OrdersSearch[contractors]'    => $model->contractors,
				'OrdersSearch[trans_from]'     => $model->trans_from,
				'OrdersSearch[trans_to]'       => $model->trans_to,
				'status'                       => $model->status,
				'wagon' 				       => $get_request->getQueryParam( 'wagon' ),
				'blockTrain' 			       => $get_request->getQueryParam( 'blockTrain' ),
				'date_from' 			       => $get_request->getQueryParam( 'date_from' ),
				'date_to' 				       => $get_request->getQueryParam( 'date_to' ),
				'forwarder'                    => $get_request->getQueryParam( 'forwarder' )
			] ) ?>" target="_blank">

                <i class="fa fa-file-excel-o"></i>
				<?= __( 'Balance Detailed Report' ) ?>

            </a>

        </li>

		<li>

			<a href="<?= Url::to( [
				'/orders/exports',
				'type'  			           => 'profit_lost',
				'OrdersSearch[user_id]'        => $model->user_id,
				'OrdersSearch[terminal_id]'    => $model->terminal_id,
				'OrdersSearch[transportation]' => $model->transportation,
				'OrdersSearch[contractors]'    => $model->contractors,
				'OrdersSearch[trans_from]'     => $model->trans_from,
				'OrdersSearch[trans_to]'       => $model->trans_to,
				'status'                       => $model->status,
				'wagon' 				       => $get_request->getQueryParam( 'wagon' ),
				'blockTrain' 			       => $get_request->getQueryParam( 'blockTrain' ),
				'date_from' 			       => $get_request->getQueryParam( 'date_from' ),
				'date_to' 				       => $get_request->getQueryParam( 'date_to' ),
				'forwarder'                    => $get_request->getQueryParam( 'forwarder' )
			] ) ?>" target="_blank">

				<i class="fa fa-file-excel-o"></i>
				<?= __( 'Profit And Lost Report' ) ?>

			</a>

		</li>

		<li>

			<a href="<?= Url::to( [
				'/orders/exports',
				'type'  			           => 'customers_receivables',
				'OrdersSearch[user_id]'        => $model->user_id,
				'OrdersSearch[terminal_id]'    => $model->terminal_id,
				'OrdersSearch[transportation]' => $model->transportation,
				'OrdersSearch[contractors]'    => $model->contractors,
				'OrdersSearch[trans_from]'     => $model->trans_from,
				'OrdersSearch[trans_to]'       => $model->trans_to,
				'status'                       => $model->status,
				'wagon' 				       => $get_request->getQueryParam( 'wagon' ),
				'blockTrain' 			       => $get_request->getQueryParam( 'blockTrain' ),
				'date_from' 			       => $get_request->getQueryParam( 'date_from' ),
				'date_to' 				       => $get_request->getQueryParam( 'date_to' ),
				'forwarder'                    => $get_request->getQueryParam( 'forwarder' )
			] ) ?>" target="_blank">

				<i class="fa fa-file-excel-o"></i>
				<?= __( 'Receivables by Customers Report' ) ?>

			</a>

		</li>

		<li>

			<a href="<?= Url::to( [
				'/orders/exports',
				'type'  			           => 'conter_agent_receivables',
				'OrdersSearch[user_id]'        => $model->user_id,
				'OrdersSearch[terminal_id]'    => $model->terminal_id,
				'OrdersSearch[transportation]' => $model->transportation,
				'OrdersSearch[contractors]'    => $model->contractors,
				'OrdersSearch[trans_from]'     => $model->trans_from,
				'OrdersSearch[trans_to]'       => $model->trans_to,
				'status'                       => $model->status,
				'wagon' 				       => $get_request->getQueryParam( 'wagon' ),
				'blockTrain' 			       => $get_request->getQueryParam( 'blockTrain' ),
				'date_from' 			       => $get_request->getQueryParam( 'date_from' ),
				'date_to' 				       => $get_request->getQueryParam( 'date_to' ),
				'forwarder'                    => $get_request->getQueryParam( 'forwarder' )
			] ) ?>" target="_blank">

				<i class="fa fa-file-excel-o"></i>
				<?= __( 'Receivables by Conter-Agents Report' ) ?>

			</a>

		</li>

		<li>

			<a href="<?= Url::to( [
				'/orders/exports',
				'type'  			           => 'codes',
				'OrdersSearch[user_id]'        => $model->user_id,
				'OrdersSearch[terminal_id]'    => $model->terminal_id,
				'OrdersSearch[transportation]' => $model->transportation,
				'OrdersSearch[contractors]'    => $model->contractors,
				'OrdersSearch[trans_from]'     => $model->trans_from,
				'OrdersSearch[trans_to]'       => $model->trans_to,
				'status'                       => $model->status,
				'wagon' 				       => $get_request->getQueryParam( 'wagon' ),
				'blockTrain' 			       => $get_request->getQueryParam( 'blockTrain' ),
				'date_from' 			       => $get_request->getQueryParam( 'date_from' ),
				'date_to' 				       => $get_request->getQueryParam( 'date_to' ),
				'forwarder'                    => $get_request->getQueryParam( 'forwarder' )
			] ) ?>" target="_blank">

				<i class="fa fa-file-excel-o"></i>
				<?= __( 'Codes Report' ) ?>

			</a>

		</li>

		<li>

			<a href="<?= Url::to( [
				'/orders/exports',
				'type'  			           => 'transportations',
				'OrdersSearch[user_id]'        => $model->user_id,
				'OrdersSearch[terminal_id]'    => $model->terminal_id,
				'OrdersSearch[transportation]' => $model->transportation,
				'OrdersSearch[contractors]'    => $model->contractors,
				'OrdersSearch[trans_from]'     => $model->trans_from,
				'OrdersSearch[trans_to]'       => $model->trans_to,
				'status'                       => $model->status,
				'wagon' 				       => $get_request->getQueryParam( 'wagon' ),
				'blockTrain' 			       => $get_request->getQueryParam( 'blockTrain' ),
				'date_from' 			       => $get_request->getQueryParam( 'date_from' ),
				'date_to' 				       => $get_request->getQueryParam( 'date_to' ),
				'forwarder'                    => $get_request->getQueryParam( 'forwarder' )
			] ) ?>" target="_blank">

				<i class="fa fa-file-excel-o"></i>
				<?= __( 'Transportation Report' ) ?>

			</a>

		</li>

		<li>

			<a href="<?= Url::to( [
				'/orders/exports',
				'type'  			           => 'bt_transportations',
				'OrdersSearch[user_id]'        => $model->user_id,
				'OrdersSearch[terminal_id]'    => $model->terminal_id,
				'OrdersSearch[transportation]' => $model->transportation,
				'OrdersSearch[contractors]'    => $model->contractors,
				'OrdersSearch[trans_from]'     => $model->trans_from,
				'OrdersSearch[trans_to]'       => $model->trans_to,
				'status'                       => $model->status,
				'wagon' 				       => $get_request->getQueryParam( 'wagon' ),
				'blockTrain' 			       => $get_request->getQueryParam( 'blockTrain' ),
				'date_from' 			       => $get_request->getQueryParam( 'date_from' ),
				'date_to' 				       => $get_request->getQueryParam( 'date_to' ),
				'forwarder'                    => $get_request->getQueryParam( 'forwarder' )
			] ) ?>" target="_blank">

				<i class="fa fa-file-excel-o"></i>
				<?= __( 'Block Train Transportation Report' ) ?>

			</a>

		</li>

		<li>

			<a href="<?= Url::to( [
				'/orders/exports',
				'type'  			           => 'incomes',
				'OrdersSearch[user_id]'        => $model->user_id,
				'OrdersSearch[terminal_id]'    => $model->terminal_id,
				'OrdersSearch[transportation]' => $model->transportation,
				'OrdersSearch[contractors]'    => $model->contractors,
				'OrdersSearch[trans_from]'     => $model->trans_from,
				'OrdersSearch[trans_to]'       => $model->trans_to,
				'status'                       => $model->status,
				'wagon' 				       => $get_request->getQueryParam( 'wagon' ),
				'blockTrain' 			       => $get_request->getQueryParam( 'blockTrain' ),
				'date_from' 			       => $get_request->getQueryParam( 'date_from' ),
				'date_to' 				       => $get_request->getQueryParam( 'date_to' ),
				'forwarder'                    => $get_request->getQueryParam( 'forwarder' )
			] ) ?>" target="_blank">

				<i class="fa fa-file-excel-o"></i>
				<?= __( 'Incomes Report' ) ?>

			</a>

		</li>

		<li>

			<a href="<?= Url::to( [
				'/orders/exports',
				'type'  			           => 'terminal_storage',
				'OrdersSearch[user_id]'        => $model->user_id,
				'OrdersSearch[terminal_id]'    => $model->terminal_id,
				'OrdersSearch[transportation]' => $model->transportation,
				'OrdersSearch[contractors]'    => $model->contractors,
				'OrdersSearch[trans_from]'     => $model->trans_from,
				'OrdersSearch[trans_to]'       => $model->trans_to,
				'status'                       => $model->status,
				'wagon' 				       => $get_request->getQueryParam( 'wagon' ),
				'blockTrain' 			       => $get_request->getQueryParam( 'blockTrain' ),
				'date_from' 			       => $get_request->getQueryParam( 'date_from' ),
				'date_to' 				       => $get_request->getQueryParam( 'date_to' ),
				'forwarder'                    => $get_request->getQueryParam( 'forwarder' )
			] ) ?>" target="_blank">

				<i class="fa fa-file-excel-o"></i>
				<?= __( 'Terminal Storage Report' ) ?>

			</a>

		</li>

	</ul>

</div>
