<?php /***************************************************************
 *  	   	 FORM SECTION PART FOR ORDERS CONTAINERS DETAIL`s        *
 *       	  I.E WILL BE DISPLAYED AT THE ORDER EDIT PAGE  		 *
 *********************************************************************/

use common\models\WagonsRelation;

/* @var $model common\models\Orders          */
/* @var $form yii\widgets\ActiveForm         */
/* @var $containers common\models\Containers */
/* @var $wagons common\models\Wagons         */
/* @var $expenses common\models\Expenses[]   */

//
$selected_containers = $model->getContainers();

//container wagons
$wagon = WagonsRelation::find()
    -> where( [
        'type'  => 'container-' . $model->id
    ] )
    -> indexBy( 'foreign_id' )
    -> asArray()
    -> all()
?>

<div class="col-md-12 col-sm-12 col-xs-12">

	<div class="order-form-title">

		<h4>
			<a class="dropdown-toggle" data-toggle="collapse" href="#containers" aria-expanded="false">
				<?= __( 'Containers' ) ?>
			</a>
		</h4>

		<ul class="nav navbar-right panel_toolbox">

			<li>
				<a class="dropdown-toggle" data-toggle="collapse" href="#containers" aria-expanded="false">
					<i class="dropdown fa fa-chevron-down"></i>
				</a>
			</li>

			<li>

				<a href="javascript:void(0)" id="add-container" data-toggle="dropdown" data-type="containers">
					<i class="fa fa-plus"></i>
				</a>

			</li>

            <li>

                <a href="javascript:void(0)" class="import-xlsx-file text-right" data-details="<?= htmlentities( json_encode( [
					'url'  => '/orders/expenses-import?order_id=' . $model->id,
					'type' => 'expenses'
				] )) ?>">
                    <i class="dropdown fa fa-file-excel-o"></i>
                </a>

            </li>

		</ul>

	</div>

	<div class="order-form-content collapse containers-container" id="containers">

        <?php if ( ! empty( $selected_containers ) || ! empty( $expenses ) ) {

            foreach ( ! empty( $selected_containers ) ? $selected_containers : $expenses as $key => $container ) { ?>

                <?= $this->render( '../templates/container', [
                    'model' => [
						'index'     => $key,
						'container' => ! empty( $selected_containers )
                            ? $container[ 'id' ]
                            : '',
						'expenses_id' => empty( $selected_containers )
                            ? $container[ 'id' ]
                            : '',
						'order_id'    => $model->id,
                        'wagon'       => isset( $wagon[ $container[ 'id' ] ] )
                            ? $wagon[ $container[ 'id' ] ][ 'wagon_id' ]
                            : ''
                    ],
                    'containers' => $containers,
                    'wagons'     => $wagons
                ] ) ?>

			<?php }

        } ?>

	</div>

</div>

<script type="text/html" id="containers-template">

	<?= $this->render( '../templates/container', [
		'containers' => $containers,
		'wagons'     => $wagons,
        'model'      => [
            'order_id' => $model->id
        ]
	] ) ?>

</script>
