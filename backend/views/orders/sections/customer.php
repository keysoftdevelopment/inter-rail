<?php /***************************************************************
 *  	   	  FORM SECTION PART FOR ORDERS CUSTOMER DETAILS          *
 *       	  I.E WILL BE DISPLAYED AT THE ORDER EDIT PAGE  		 *
 *********************************************************************/

use common\models\Files;
use common\models\User;

/* @var $model common\models\Orders  */
/* @var $form yii\widgets\ActiveForm */

$customer = User::findOne( $model->user_id );

if ( ! empty( $model->document_id ) )
    $attached_document = Files::findOne(
        $model->document_id
    )
?>

	<div class="col-md-12 col-sm-12 col-xs-12">

        <div class="order-form-title">

            <h4>
                <a class="dropdown-toggle" data-toggle="collapse" href="#customer-form" aria-expanded="false">
				    <?= __( 'Customer Information ' ) ?>
                </a>
            </h4>

            <ul class="nav navbar-right panel_toolbox" style="min-width: 30px;">

                <li>
                    <a class="dropdown-toggle" data-toggle="collapse" href="#customer-form" aria-expanded="false">
                        <i class="dropdown fa fa-chevron-down"></i>
                    </a>
                </li>

            </ul>

        </div>

        <div class="order-form-content collapse" id="customer-form">

            <div class="col-sm-4 col-md-4 col-xs-12">

                <label class="control-label">
                    <?= __( 'Last and First Name' ) ?>
                </label>

                <input type="text" class="form-control" value="<?= ! is_null( $customer )
                    ? $customer->first_name . ' ' . $customer->last_name
                    : __( 'No information' )
                ?>" disabled>

            </div>

            <div class="col-sm-3 col-md-3 col-xs-12">

                <label class="control-label">
                    <?= __( 'E-mail address' ) ?>
                </label>

                <input type="text" class="form-control" value="<?= ! is_null( $customer )
                    ? $customer->email
                    : __( 'No information' )
                ?>" disabled>

            </div>

            <div class="col-sm-3 col-md-3 col-xs-12">

                <label class="control-label">
                    <?= __( 'Phone number' ) ?>
                </label>

                <input type="text" class="form-control" value="<?= ! is_null( $customer )
                    ? $customer->phone
                    : __( 'No information' )
                ?>" disabled>

            </div>

            <div class="col-sm-2 col-md-2 col-xs-12">

                <label class="control-label">
					<?= __( 'Documents' ) ?>
                </label>

                <a href="<?= isset( $attached_document ) && ! empty( $attached_document )
                    ? $attached_document->guide
                    : 'javascript:void(0)'
                ?>" <?= isset( $attached_document ) && ! empty( $attached_document )
                    ? 'target="_blank"'
                    : ''
                ?> style="color: #00a2b3; text-transform: uppercase; margin-top: 8px; float: left; width: 100%;">

                    <?php if ( isset( $attached_document ) && ! empty( $attached_document ) ) { ?>

                        <i class="fa fa-file-zip-o" style="font-size: 32px; float: left; margin-right: 10px; margin-top: -6px;"></i>
                        <?= __( 'Download ZIP File' ) ?>

                    <?php } else {
                        echo __( 'There is no any attachments' );
                    } ?>

                </a>

            </div>

	    </div>

    </div>