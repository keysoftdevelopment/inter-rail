<?php /***************************************************************
 *  	  FORM SECTION PART FOR ORDERS TERMINAL DETAILS SECTION      *
 *       	  I.E WILL BE DISPLAYED AT THE ORDER EDIT PAGE  		 *
 *********************************************************************/

use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;

use common\models\Terminals;

/* @var $model common\models\Orders  */
/* @var $form yii\widgets\ActiveForm */

$terminal = ! empty( $model->terminals )
    ? json_decode( $model->terminals )
    : '';

//the list of all available terminals
$terminals = Terminals::find()
    -> where( [
	    'isDeleted' => false
    ] )
    -> indexBy( 'id' )
    -> asArray()
    -> all();

?>

<div class="col-md-12 col-sm-12 col-xs-12">

    <div class="order-form-title">

        <h4>
            <a class="dropdown-toggle" data-toggle="collapse" href="#terminal-form" aria-expanded="false">
			    <?= __( 'Terminals Details' ) ?>
            </a>
        </h4>

        <ul class="nav navbar-right panel_toolbox" style="min-width: 30px;">

            <li>
                <a class="dropdown-toggle" data-toggle="collapse" href="#terminal-form" aria-expanded="false">
                    <i class="fa fa-chevron-down"></i>
                </a>
            </li>

        </ul>

    </div>

    <div class="order-form-content collapse" id="terminal-form">

        <div class="col-sm-2 col-md-2 col-xs-12">

            <?= $form
                -> field( $model, 'terminal_id' )
                -> dropDownList( ArrayHelper::map(
                        $terminals,
                        'id',
                        'name'
                ), [
                    'class'        => 'form-control',
                    'prompt'       => __( 'Terminal' ),
                    'data-details' => json_encode( $terminals )
                ] )
                -> label( __( 'Terminal' ) )
            ?>

        </div>

        <div class="col-sm-2 col-md-2 col-xs-12">

			<?= $form
				-> field( $model, 'terminals[free_detention]' )
				-> textInput( [
					'class'       => 'form-control free-detention-field',
					'placeholder' => __( 'Free Detention' )
				] )
				-> label( __( 'Free Detention' ) )
			?>

        </div>

        <div class="col-sm-2 col-md-2 col-xs-12">

            <label class="control-label" for="orders-terminals-date_from">
                <?= __(  'Date From' ) ?>
            </label>

			<?= DatePicker::widget( [
				'name'    => 'Orders[terminals][date_from]',
				'type'    => DatePicker::TYPE_INPUT,
				'value'   => isset( $terminal->date_from )
					? date('d.m.Y', strtotime( $terminal->date_from ) )
					: date('d.m.Y' ) ,
				'options' => [
					'placeholder' => __(  'Date From' ),
					'class'       => 'form-control terminal-dates date-from-field'
				],
				'pluginOptions' => [
					'autoclose' => true,
					'format'    => 'dd.mm.yyyy',
					'weekStart' => '1'
				]
			] ); ?>

        </div>

        <div class="col-sm-2 col-md-2 col-xs-12">

            <label class="control-label" for="orders-terminals-date_to">
                <?= __(  'Date To' ) ?>
            </label>

			<?= DatePicker::widget( [
				'name'    => 'Orders[terminals][date_to]',
				'type'    => DatePicker::TYPE_INPUT,
				'value'   => isset( $terminal->date_to )
					? date('d.m.Y', strtotime( $terminal->date_to ) )
					: date('d.m.Y' ) ,
				'options' => [
					'placeholder' => __(  'Date To' ),
					'class'       => 'form-control terminal-dates date-to-field'
				],
				'pluginOptions' => [
					'autoclose' => true,
					'format'    => 'dd.mm.yyyy',
					'weekStart' => '1'
				]
			] ); ?>

        </div>

        <div class="col-sm-2 col-md-2 col-xs-12">

            <?= $form
                -> field( $model, 'terminals[status]' )
                -> dropDownList( Yii::$app->common->transportationTypes(), [
                    'class'   => 'form-control',
                    'prompt'  => __( 'Status' ),
					'options' => [
                        isset( $terminal->status )
                            ? $terminal->status
                            : '' => [
                            'Selected' => 'selected'
                        ]
                    ]
                ] )
                -> label( __( 'Status' ) )
            ?>

        </div>

        <div class="col-sm-2 col-md-2 col-xs-12">
            <input type="button" value="<?= __( 'Generate and send report' ) ?>" />
        </div>

    </div>

</div>