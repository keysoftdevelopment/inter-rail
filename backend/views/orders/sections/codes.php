<?php /***************************************************************
 *  	   	    FORM SECTION PART FOR ORDERS CODE DETAILS            *
 *       	  I.E WILL BE DISPLAYED AT THE ORDER EDIT PAGE  		 *
 *********************************************************************/

/* @var $model common\models\Orders  */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="col-md-12 col-sm-12 col-xs-12">

	<div class="order-form-title">

		<h4>
			<a class="dropdown-toggle" data-toggle="collapse" href="#code-details" aria-expanded="false">
				<?= __( 'Code Details' ) ?>
			</a>
		</h4>

		<ul class="nav navbar-right panel_toolbox" style="min-width: 30px;">

			<li>
				<a class="dropdown-toggle" data-toggle="collapse" href="#code-details" aria-expanded="false">
					<i class="dropdown fa fa-chevron-down"></i>
				</a>
			</li>

			<li>

				<a href="javascript:void(0)" id="add-code" data-toggle="dropdown" data-type="code">
					<i class="fa fa-plus"></i>
				</a>

			</li>

		</ul>

	</div>

	<div class="order-form-content collapse code-container" id="code-details">

        <?php if ( ! empty( $model->codes ) ) {

            $codes = json_decode( $model->codes );

            if ( isset( $codes->country ) ) {

				foreach ( $codes->country as $key => $code ) { ?>

					<?= $this->render( '../templates/code', [
					    'model' => [
                            'index'   => $key,
						    'country' => $code,
						    'route'   => isset( $codes->route[ $key ] )
                                ? $codes->route[ $key ]
                                : '',
						    'forwarder' => isset( $codes->forwarder[ $key ] )
								? $codes->forwarder[ $key ]
								: '',
						    'code' => isset( $codes->code[ $key ] )
                                ? $codes->code[ $key ]
                                : ''
                        ]
					] ) ?>

				<?php }

            }

        } ?>

	</div>

</div>

<script type="text/html" id="code-template">

	<?= $this->render(
		'../templates/code'
	); ?>

</script>
