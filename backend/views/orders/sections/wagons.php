<?php /***************************************************************
 *  	   	  FORM SECTION PART FOR ORDERS WAGONS DETAIL`s        	 *
 *       	  	    DISPLAYED AT THE ORDER EDIT PAGE  		 		 *
 *********************************************************************/

use common\models\Containers;

/* @var $model common\models\Orders         */
/* @var $form yii\widgets\ActiveForm        */
/* @var $wagons common\models\Wagons        */
/* @var $expenses common\models\Expenses[]  */

//
$order_wagons = $model->getWagons(); ?>

<div class="col-md-12 col-sm-12 col-xs-12">

	<div class="order-form-title">

		<h4>
			<a class="dropdown-toggle" data-toggle="collapse" href="#wagons" aria-expanded="false">
				<?= __( 'Wagons' ) ?>
			</a>
		</h4>

		<ul class="nav navbar-right panel_toolbox" style="min-width: 30px;">

			<li>
				<a class="dropdown-toggle" data-toggle="collapse" href="#wagons" aria-expanded="false">
					<i class="dropdown fa fa-chevron-down"></i>
				</a>
			</li>

			<li>

				<a href="javascript:void(0)" id="add-wagon" data-toggle="dropdown" data-type="wagons">
					<i class="fa fa-plus"></i>
				</a>

			</li>

            <li>

                <a href="javascript:void(0)" class="import-xlsx-file text-right" data-details="<?= htmlentities( json_encode( [
					'url'  => '/orders/expenses-import?order_id=' . $model->id .'&type=wagons',
					'type' => 'expenses'
				] )) ?>">
                    <i class="dropdown fa fa-file-excel-o"></i>
                </a>

            </li>

		</ul>

	</div>

	<div class="order-form-content collapse wagons-container" id="wagons">

		<?php if ( ! empty( $order_wagons ) ) {

			foreach ( $order_wagons as $key => $wagon ) { ?>

				<?= $this->render( '../templates/wagon', [
					'model' => [
						'index'     => $key,
						'wagon'     => $wagon[ 'id' ],
						'order_id'  => $model->id
					],
					'wagons' => $wagons
				] ) ?>

			<?php }

		} ?>

	</div>

</div>

<script type="text/html" id="wagons-template">

	<?= $this->render( '../templates/wagon', [
		'wagons'     => $wagons,
		'model'      => [
			'order_id' => $model->id
		]
	] ) ?>

</script>
