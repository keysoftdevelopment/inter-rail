<?php /***************************************************************
 *  	   	  FORM SECTION PART FOR ORDERS GENERAL DETAILS           *
 *       	  I.E WILL BE DISPLAYED AT THE ORDER EDIT PAGE  		 *
 *********************************************************************/

use yii\helpers\ArrayHelper;

use common\models\Settings;
use common\models\Taxonomies;
use common\models\User;

/* @var $model common\models\Orders   */
/* @var $form yii\widgets\ActiveForm  */
/* @var $logistics array              */

$number_options = Settings::findOne( [
    'name' => 'order_options'
] );

//the list of general detail form information
$general_details = [
	'trans_from' => [
		'label'    => __(  'From' ),
		'id'       => 'transportation-from',
		'disabled' => true
	],
	'trans_to' => [
		'label'    => __(  'To' ),
		'id'       => 'transportation-to',
		'disabled' => true
	],
    'delivery_terms[from]' => [
		'label' => __(  'Delivery Term From' ),
		'type'  => 'dropdown',
		'id'    => 'delivery-term-from',
    ],
	'delivery_terms[to]' => [
		'label' => __(  'Delivery Term To' ),
        'type'  => 'dropdown',
		'id'    => 'delivery-term-to',
	],
	'cargo_tax' => [
		'label'    => __(  'Cargo Category' ),
		'id'       => 'cargo-taxonomy',
		'disabled' => false
	],
	'weight_dimensions' => [
		'label'    => __(  'Weight Dimensions' ),
		'id'       => 'weight-dimensions',
		'disabled' => true
	],
	'packages'  => [
		'label'    => __(  'Packages' ),
		'id'       => 'packages',
		'disabled' => true
	],
	'container_type' => [
		'label'    => __(  'Container Type' ),
		'id'       => 'container-type',
		'disabled' => false
	],
	'wagon_type' =>  [
		'label'    => __(  'Wagon Type' ),
		'id'       => 'wagon-type',
		'disabled' => false
	]
] ?>

    <div class="col-md-12 col-sm-12 col-xs-12">

        <div class="col-sm-3 col-md-3 col-xs-12">

            <?= $form
                -> field( $model, 'number' )
                -> dropDownList( is_null( $number_options )
                    ? []
                    : array_reduce( json_decode(
						$number_options->description
					), function( $order_number, $option ) use ( $model ) {

						if ( ! empty( $option ) )
							$order_number[ $option . '' . $model->id ] = $option;

						return $order_number;

					}, [] ), [
					    'class'       => 'form-control',
                        'id'          => 'order-number',
                        'placeholder' => __(  'Order Number' ),
                    ] )
                -> label( __(  'Order Number' ) );
            ?>

        </div>

        <div class="col-sm-3 col-md-3 col-xs-12">

			<?= $form
				-> field( $model, 'manager_id' )
				-> dropDownList( ArrayHelper::map(
					User::find()
						-> where( [
							'isDeleted' => false
						] )
						-> andWhere( [
							'NOT IN', 'id', \Yii::$app->authManager->getUserIdsByRole( 'client' )
						] )
						-> asArray()
						-> all(),
					'id',
					'username'
                ), [
					'class' => 'form-control',
					'prompt' => __(  'Assigned' )
				] )
				-> label( __(  'Assigned' ) );
			?>

        </div>

        <div class="col-sm-3 col-md-3 col-xs-12">

			<?= $form
				-> field( $model, 'status' )
				-> dropDownList( Yii::$app->common->statuses( 'order' ), [
					'class' => 'form-control',
					'prompt' => __(  'Status' )
				] )
				-> label( __(  'Status' ) );
			?>

        </div>

        <div class="col-sm-3 col-md-3 col-xs-12">

			<?= $form
				-> field( $model, 'transportation' )
				-> dropDownList( Yii::$app->common->transportationTypes( 'transportation' ), [
					'class' => 'form-control',
					'prompt' => __(  'Type' )
				] )
				-> label( __(  'Type' ) );
			?>

        </div>

        <div class="col-sm-12 col-md-12 col-xs-12">

			<?= $form
				-> field( $model, 'manager_note' )
				-> textarea( [
					'class'       => 'form-control',
					'id'          => 'manager-note',
					'placeholder' => __(  'Comment' )
				] )
				-> label( false );
			?>

        </div>

    </div>

    <div class="col-md-12 col-sm-12 col-xs-12">

        <div class="order-form-title">

            <h4>
                <a data-toggle="collapse" href="#general-form" aria-expanded="false">
                    <?= __( 'General Information ' ) ?>
                </a>
            </h4>

            <ul class="nav navbar-right panel_toolbox" style="min-width: 30px;">

                <li>
                    <a class="dropdown-toggle" data-toggle="collapse" href="#general-form" aria-expanded="false">
                        <i class="dropdown fa fa-chevron-down"></i>
                    </a>
                </li>

            </ul>

        </div>

        <div class="order-form-content collapse" id="general-form">

            <?php foreach ( $general_details as $key => $detail ) { ?>

                <div class="col-sm-3 col-md-3 col-xs-12">

                    <?php if ( in_array( $key, [ 'cargo_tax', 'container_type', 'wagon_type' ] ) ) { ?>

                        <?= $form
                            -> field( $model, $key )
                            -> dropDownList( ArrayHelper::map(
								Taxonomies::find()
									-> where( [
										'type'      => $key == 'wagon_type'
                                            ? 'railway'
                                            : str_replace( [ '_type', '_tax' ], '', $key ),
										'isDeleted' => false
									] )
									-> asArray()
									-> all(),
								'id',
								'name'
							), [
                                'class'  => 'form-control',
                                'id'     => $detail[ 'id' ],
                                'prompt' => $detail[ 'label' ]
                            ] )
                            -> label(
                                $detail[ 'label' ]
                            );
                        ?>

                    <?php } else {

                        if ( isset( $detail[ 'type' ] ) ) {

                            echo $form
								-> field( $model, $key )
								-> dropDownList( Yii::$app->common->transportationDetails( 'terms' ), [
									'class'  => 'form-control',
									'id'     => $detail[ 'id' ],
									'prompt' => 'None'
								] )
								-> label(
									$detail[ 'label' ]
								);

                        } else {

                            echo $form
								-> field( $model, $key )
								-> textInput( [
									'class'       => 'form-control',
									'id'          => $detail[ 'id' ],
									'placeholder' => $detail[ 'label' ],
									'disabled'    => $detail[ 'disabled' ]
								] )
								-> label(
									$detail[ 'label' ]
								);

                        }

                    } ?>

                </div>

            <?php } ?>

            <div class="col-sm-3 col-md-3 col-xs-12">

                <label class="checkbox">

					<?= __( 'Insurance' ) ?>

                    <input name="Orders[insurance]" type="checkbox" value="<?= $model->insurance ?>" >

                    <span class="checkmark"></span>

                </label>

            </div>

        </div>

    </div>