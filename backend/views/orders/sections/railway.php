<?php /***************************************************************
 *  	   	FORM SECTION PART FOR ORDERS BLOCK TRAIN SECTION         *
 *       	  I.E WILL BE DISPLAYED AT THE ORDER EDIT PAGE  		 *
 *********************************************************************/
use yii\helpers\ArrayHelper;

/* @var $model common\models\Orders          */
/* @var $form yii\widgets\ActiveForm         */
/* @var $block_train common\models\Logistics */
/* @var $logistics array                     */ ?>

    <div class="col-md-12 col-sm-12 col-xs-12">

        <div class="order-form-title">

            <h4>
                <a class="dropdown-toggle" data-toggle="collapse" href="#block-train-form" aria-expanded="false">
				    <?= __( 'Block Train Information ' ) ?>
                </a>
            </h4>

            <ul class="nav navbar-right panel_toolbox" style="min-width: 30px;">

                <li>
                    <a class="dropdown-toggle" data-toggle="collapse" href="#block-train-form" aria-expanded="false">
                        <i class="fa fa-chevron-down"></i>
                    </a>
                </li>

            </ul>

        </div>

        <div class="order-form-content collapse" id="block-train-form">

            <div class="col-sm-1 col-md-1 col-xs-12">

                <?= $form
                    -> field( $block_train, 'transport_no' )
                    -> textInput( [
                        'id'          => 'transport-number',
                        'placeholder' => __(  'Rail Number' )
                    ] )
                    -> label( __(  'Rail Number' ) );
                ?>

            </div>

            <div class="col-sm-2 col-md-2 col-xs-12">

                <?= $form
                    -> field( $block_train, 'transport_ind' )
                    -> textInput( [
                        'id'          => 'transport-index',
                        'placeholder' => __(  'Rail Index' )
                    ] )
                    -> label( __(  'Rail Index' ) );
                ?>

            </div>

            <div class="col-sm-3 col-md-3 col-xs-12">

				<?= $form
					-> field( $model, 'consignee' )
					-> textInput( [
						'id'          => 'consignee',
						'placeholder' => __(  'Consignee' ),
					] )
					-> label( __(  'Consignee' ) );
				?>

            </div>

            <div class="col-sm-3 col-md-3 col-xs-12">

				<?= $form
					-> field( $model, 'shipper' )
					-> textInput( [
						'id'          => 'shipper',
						'placeholder' => __(  'Shipper' ),
					] )
					-> label( __(  'Shipper' ) );
				?>

            </div>

            <div class="col-sm-2 col-md-2 col-xs-12">

				<?= $form
					-> field( $model, 'gu_number' )
					-> textInput( [
						'id'          => 'gu-number',
						'placeholder' => __(  'GU Number' ),
					] )
					-> label( __(  'GU Number' ) );
				?>

            </div>

            <div class="col-sm-1 col-md-1 col-xs-12">

                <label class="control-label" for="gu-file">
                    GU File
                </label>

                <div class="gu-file-upload-wrapper" data-text="Select!">
                    <input name="UploadForm[file]" type="file">
                </div>

            </div>

        </div>

    </div>