<?php /***************************************************************
 *	  	   	     THE LIST OF ALL AVAILABLE ORDERS PAGE               *
 *********************************************************************/

use yii\helpers\Html;
use yii\widgets\LinkPager;

use common\widgets\Alert;

/* @var $this yii\web\View                        */
/* @var $searchModel common\models\OrdersSearch   */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $users array                              */
/* @var $status_count common\models\Orders[]      */
/* @var $notes array                              */

// current page title
$this->title = __(  'Orders' );

//get request short option
$request = Yii::$app->getRequest();

//get date from from get request
$date_from = $request->getQueryParam( 'date_from' );

//get date to from get request
$date_to = $request->getQueryParam( 'date_to' ); ?>

    <div class="orders-lists-data">

        <div class="page-title">

            <!-- title section -->
            <div class="title_left">

                <h3>
                    <?= $this->title ?>
                </h3>

                <?php if ( Yii::$app->user->can( 'orders' ) ) {

                    echo $this->render( 'export', [
						'model' => $searchModel
					] );

                } ?>

            </div>
            <!-- title section -->

            <!-- search and export sections -->
            <div class="title_right">

                <div class="col-sm-12 col-md-12 col-xs-12">

                    <div class="col-sm-7 col-md-7 col-xs-12">

                        <ul class="order-statuses">

                            <?php foreach ( Yii::$app->common->statuses( 'order' ) as $key => $value ) { ?>

                                <li>

									<?= Html::a( __(  $value ), [
										'index',
										'status' => $key
									], [
										'class' => $searchModel->status == $key ? 'btn btn-primary btn-xs active' : 'btn btn-default btn-xs'
									] ) ?>

                                    <?= isset( $status_count[ $key ][ 'count' ] )
                                        ?  '<span class="fa fa-bell"></span>'
                                        : ''
                                    ?>

                                </li>

                            <?php } ?>

                        </ul>

                    </div>

                    <?= $this->render( '_search', [
                        'model' => $searchModel
                    ] ); ?>

                </div>

            </div>
            <!-- search and export sections -->

        </div>

        <div class="clearfix"></div>

        <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">

                <!-- filters section -->
                <div class="main-filters-container">

					<?= $this->render( 'filters', [
						'searchModel' => $searchModel
					] ) ?>

                </div>
                <!-- filters section -->

                <div class="x_panel">

                    <!-- order lists -->
                    <div class="x_content">

                        <!-- flash messages -->
                        <?= Alert::widget() ?>
                        <!-- flash messages -->

                        <div class="row">

                            <div class="col-md-12 col-sm-12 col-xs-12 order-items">

                                <?php if ( !empty( $dataProvider->models ) )  {

                                    foreach ( $dataProvider->models as $model ) {

                                        //param initialization
                                        $order_details = [
                                            'number' => [
                                                'label' => __( 'Order Number' ),
                                                'value' => $model->number
                                            ],
                                            'status' => [
                                                'label' => __( 'Status' ),
                                                'value' => $model->status
                                            ],
                                            'customer' => [
                                                'label' => __( 'Customer Info' ),
                                                'value' => isset( $users[ $model->user_id ] )
                                                    ? $users[ $model->user_id ][ 'first_name' ] . ' ' . $users[ $model->user_id ][ 'last_name' ]
                                                    : __( 'Unknown Customer' )
                                            ],
                                            'transportation' => [
                                                'label' => __( 'From/To' ),
                                                'value' => $model->trans_from . '/' . $model->trans_to
                                            ],
                                            'comment' => [
                                                'label' => __( 'Note' ),
                                                'value' => ! empty( $notes ) && isset( $notes[ 'manager_note_for_order_' . $model->id ] )
                                                    ? $notes[ 'manager_note_for_order_' . $model->id ][ 'description' ]
                                                    : ''
                                            ],
                                            'assigned' => [
                                                'label' => __( 'Assigned' ),
                                                'value' => isset( $users[ $model->manager_id ] )
                                                    ? ( ! empty( $users[ $model->manager_id ][ 'username' ] ) )
                                                        ? $users[ $model->manager_id ][ 'username' ]
                                                        : $users[ $model->manager_id ][ 'first_name' ] . ' ' . $users[ $model->manager_id ][ 'last_name' ]
                                                    : __( 'Not Assigned' )
                                            ],
                                            'surcharge' => [
                                                'label' => __( 'Surcharge' ),
                                                'value' => $model->surcharge
                                            ],
                                            'commission' => [
                                                'label' => __( 'Commission' ),
                                                'value' => $model->commission
                                            ],
                                            'total' => [
                                                'label' => __( 'Total Sum' ),
                                                'value' => $model->total
                                            ]
                                        ]; ?>

                                        <div class="col-md-4 col-sm-4 col-xs-12 order-item">

                                            <div class="x_content">

                                                <div class="order-details">

                                                    <ul>

                                                        <?php foreach ( $order_details as $order ) { ?>

                                                            <li>

                                                                <label class="col-sm-6 col-md-6 col-xs-12">
                                                                    <?= $order[ 'label' ] ?>
                                                                </label>

                                                                <div class="col-sm-6 col-md-6 col-xs-12 ">
                                                                    <?= $order[ 'value' ] ?>
                                                                </div>

                                                            </li>

                                                        <?php } ?>

                                                    </ul>

                                                </div>

                                                <div class="order-actions">

                                                    <div class="<?= ! Yii::$app->user->can( 'orders' )
                                                        ? 'col-sm-12 col-md-12'
                                                        : 'col-sm-6 col-md-6'
                                                    ?> col-xs-12">

                                                        <?= Html::a( '<i class="fa fa-pencil"></i> ' . __(  'Edit' ), [
                                                            'update',
                                                            'id' => $model->id
                                                        ] ); ?>

                                                    </div>

													<?php if ( Yii::$app->user->can( 'orders' ) ) { ?>


                                                        <div class="col-sm-6 col-md-6 col-xs-12">

                                                            <?= Html::a( '<i class="fa fa-trash"></i> ' . __(  'Delete' ), [
                                                                'delete',
                                                                'id'     => $model->id,
                                                                'status' => $searchModel->status
                                                            ], [
                                                                'data' => [
                                                                    'confirm' => __(  'Do you really want to delete this order?' ),
                                                                    'method'  => 'post',
                                                                ]
                                                            ] ) ?>

                                                        </div>

													<?php } ?>

                                                </div>

                                            </div>

                                        </div>

                                    <?php }

                                } else { ?>

                                    <div class="col-sm-12 col-md-12 col-xs-12 aligncenter" style="text-transform: uppercase; background: #ffffff;">

                                        <h4>
                                            <?= __(  'There is no orders yet' ) ?>
                                        </h4>

                                    </div>

                                <?php } ?>

                            </div>

                        </div>

                    </div>
                    <!-- order lists -->

                    <!-- pagination section -->
                    <div class="pagination-container">

                        <?= LinkPager::widget( [
                            'pagination' => $dataProvider->pagination,
                        ] ); ?>

                    </div>
                    <!-- pagination section -->

                </div>

            </div>

        </div>

    </div>