<?php /***************************************************************
 *	           THE LIST OF ALL AVAILABLE TERMINALS PAGE              *
 *********************************************************************/

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;

/* @var $this yii\web\View                         */
/* @var $searchModel common\models\TerminalsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider  */
/* @var $containers array                          */
/* @var $types array                               */

// current page title, that will displayed in head tags
$this->title = __(  'Terminals' ); ?>

    <div class="page-title">

        <!-- title section -->
        <div class="title_left">

            <h3>
                <?= $this->title ?>
            </h3>

        </div>
        <!-- title section -->

        <!-- search section -->
        <div class="title_right">

			<?= $this->render( '_search', [
				'model' => $searchModel
			] ); ?>

        </div>
        <!-- search section -->

    </div>

    <div class="clearfix"></div>

    <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12">

            <div class="x_panel">

                <div class="x_content">

                    <!-- terminal lists -->
                    <table class="table table-striped">

                        <thead>

                            <tr>

                                <th style="width: 1%">
                                    #
                                </th>

                                <th>
                                    <?= __(  'Name' ) ?>
                                </th>

                                <th>
                                    <?= __(  'Price' ) ?> (<?= __( 'storage' ) ?>)
                                </th>

                                <th>
                                    <?= __(  'Price' ) ?> (<?= __( 'crane operations' ) ?>)
                                </th>

                                <th>
									<?= __(  'Price' ) ?> (<?= __( 'import' ) ?>)
                                </th>

                                <th>
									<?= __(  'Price' ) ?> (<?= __( 'export' ) ?>)
                                </th>

                                <th>
									<?= __(  'Price' ) ?> (<?= __( 'surcharge' ) ?>)
                                </th>

                                <th>
                                    <?= __( 'Container Type' ); ?>
                                </th>

                                <th>
                                    <?= __(  'Notice' ) ?>
                                </th>

                                <th></th>

                            </tr>

                        </thead>

                        <tbody>

                            <?php if ( !empty( $dataProvider->models ) ) {

                                foreach ( $dataProvider->models as $model ) { ?>

                                    <tr class="show-details">

                                        <td>
                                            #
                                        </td>

                                        <td>
                                            <?= Html::a( $model->name, [
                                                'update',
                                                'id' => $model->id
                                            ], [
                                                'role' => "button"
                                            ] ) ?>

                                        </td>

                                        <td>
                                            <?= $model->storage ?>
                                        </td>

                                        <td>
                                            <?= (float)$model->lading + (float)$model->unloading ?>
                                        </td>

                                        <td>
                                            <?= $model->exp_price ?>
                                        </td>

                                        <td>
                                            <?= $model->imp_price ?>
                                        </td>

                                        <td>
                                            <?= $model->surcharge ?>
                                        </td>

                                        <td>

                                            <?= isset( $types[ $model->id ] )
                                                ? $types[ $model->id ][ 'name' ]
												: __( 'Container Type Not Chosen' )
                                            ?>

                                        </td>

                                        <td>
                                            <?= $model->description ?>
                                        </td>

                                        <td class="text-center model-actions">

                                            <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                <i class="fa fa-ellipsis-v"></i>
                                            </a>

                                            <ul class="dropdown-menu pull-right">

                                                <li>

                                                    <?= Html::a( '<i class="fa fa-pencil"></i> ' .  __(  'Edit' ), [
                                                        'update',
                                                        'id'    => $model->id
                                                    ] ); ?>

                                                </li>

                                                <li>

                                                    <?= Html::a( '<i class="fa fa-trash"> </i> '. __(  'Delete' ), [
                                                        'delete',
                                                        'id' => $model->id
                                                    ], [
                                                        'data'  => [
                                                            'confirm' => __(  'Do you really want to delete this terminal?' ),
                                                            'method'  => 'post',
                                                        ]
                                                    ] ); ?>

                                                </li>

                                            </ul>

                                        </td>

                                    </tr>

                                    <tr class="additional-details">

                                        <td colspan="9">

                                            <?php if ( isset( $containers[ $model->id ] ) ) { ?>

                                                <h4>
                                                    <?= __( 'Containers' ) ?>
                                                </h4>

                                                <ul>

                                                    <?php foreach ( $containers[ $model->id ] as $container ) { ?>

                                                        <li class="col-sm-1 col-md-1 col-xs-2">

                                                            <a href="<?= Url::to( [
                                                                '/containers/update?id=' . $container[ 'id' ]
                                                            ] ) ?>">
                                                                <?= $container[ 'number' ] ?>
                                                            </a>

                                                        </li>

                                                    <?php } ?>

                                                </ul>

                                            <?php } else {
                                                echo __( 'There is no container details yet' );
                                            } ?>

                                        </td>

                                    </tr>

                                <?php }

                            } else {

                                echo '<tr>';

                                    echo '<td colspan="6">
                                                ' . __(  'There is no terminals yet' ) . '
                                          </td>';

                                echo '</tr>';

                            } ?>

                        </tbody>

                    </table>
                    <!-- terminal lists -->

                </div>

                <!-- pagination container -->
                <div class="pagination-container">

                    <?= LinkPager::widget( [
                        'pagination' => $dataProvider->pagination
                    ] ); ?>

                </div>
                <!-- pagination container -->

            </div>

        </div>

    </div>