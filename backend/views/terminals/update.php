<?php /***************************************************************
 *	     		 	   	  UPDATE TERMINAL PART                       *
 *********************************************************************/

/* @var $this yii\web\View              */
/* @var $model \common\models\Terminals */

// current page title
$this->title = __(  'Update Terminal' ); ?>

    <div class="col-md-12 col-sm-12 col-xs-12 edit-terminal-block">

        <!-- title section -->
        <div class="page-title">

            <div class="title_left">

                <h3>
                    <?= $this->title; ?>
                </h3>

            </div>

        </div>
        <!-- title section -->

        <!-- form section -->
        <div class="col-md-12 col-sm-12 col-xs-12 create-item-form">

            <?= $this->render( '_form', [
                'model' => $model
            ] ); ?>

        </div>
        <!-- form section -->

    </div>