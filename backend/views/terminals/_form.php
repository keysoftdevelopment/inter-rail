<?php /***************************************************************
 * 	    	 	   	    FORM SECTION FOR TERMINAL                    *
 *********************************************************************/

use yii\widgets\ActiveForm;
use yii\helpers\Url;

use dosamigos\tinymce\TinyMce;

use common\models\Taxonomies;

use backend\widgets\GoogleMaps;

/* @var $this yii\web\View             */
/* @var $model common\models\Terminals */

//terminal main form
$form = ActiveForm::begin( [
    'options'                => [
        'class'                 => 'form-horizontal form-label-left ajax-submitting-form',
        'id'                    => 'terminal-form',
        'data-func'             => 'formCallback',
        'data-redirectonfinish' => Url::toRoute( [
            '/terminals'
        ] )
    ],
    'enableAjaxValidation'   => false,
    'enableClientValidation' => true
] ); ?>

    <div class="x_panel">

        <div class="x_content">

            <div class="col-sm-12 col-md-12 col-xs-12">

                <div class="col-sm-4 col-md-4 col-xs-12">

                    <?= $form
                        -> field( $model, 'name' )
                        -> textInput( [
                            'id'          => 'name',
                            'placeholder' => __(  'Name' )
                        ] )
                        -> label( __(  'Name' ) );
                    ?>

                </div>

                <div class="col-sm-4 col-md-4 col-xs-12">

					<?= $form
						-> field( $model, 'email' )
						-> textInput( [
							'id'          => 'email',
							'placeholder' => __(  'Email' )
						] )
						-> label( __(  'Email' ) );
					?>

                </div>

                <div class="col-sm-4 col-md-4 col-xs-12">

					<?= $form
						-> field( $model, 'phone' )
						-> textInput( [
							'id'          => 'phone',
							'placeholder' => __(  'Phone' )
						] )
						-> label( __(  'Phone' ) );
					?>

                </div>

            </div>

            <div class="col-sm-12 col-md-12 col-xs-12 container-type-lists">

                <label class="control-label" for="container-type">
                    <?= __( 'Container Type' ) ?>
                </label>

                <ul>

                    <?php foreach ( Taxonomies::find()
                        -> where( [
                            'type'      => 'containers',
                            'isDeleted' => false
                        ] )
                        -> all() as $type
                    ) { ?>

                        <li>

                            <label class="radio">

                                <?= $type->name ?>
                                <input type="radio" name="Terminals[containers_type]" value="<?= $type->id ?>" <?= $model->containers_type == $type->id ? 'checked' : '' ?>>
                                <span class="checkmark"></span>

                            </label>

                        </li>

                    <?php } ?>

                </ul>

            </div>

            <div class="col-sm-12 col-md-12 col-xs-12">

                <div class="col-sm-2 col-md-2 col-xs-12">

					<?= $form
						-> field( $model, 'storage' )
						-> textInput( [
							'id'          => 'storage-field',
							'placeholder' => __(  'Storage' )
						] )
						-> label( __(  'Storage' ) );
					?>

                </div>

                <div class="col-sm-2 col-md-2 col-xs-12">

					<?= $form
						-> field( $model, 'lading' )
						-> textInput( [
							'id'          => 'lading-crane-price-feild',
							'placeholder' => __(  'Lading Crane' )
						] )
						-> label( __(  'Lading Crane' ) );
					?>

                </div>

                <div class="col-sm-2 col-md-2 col-xs-12">

					<?= $form
						-> field( $model, 'unloading' )
						-> textInput( [
							'id'          => 'unloading-crane-price-field',
							'placeholder' => __(  'Unloading Crane' )
						] )
						-> label( __(  'Unloading Crane' ) );
					?>

                </div>

                <div class="col-sm-2 col-md-2 col-xs-12">

					<?= $form
						-> field( $model, 'exp_price' )
						-> textInput( [
							'id'          => 'export-price-field',
							'placeholder' => __(  'Export Price' )
						] )
						-> label( __(  'Export Price' ) );
					?>

                </div>

                <div class="col-sm-2 col-md-2 col-xs-12">

					<?= $form
						-> field( $model, 'imp_price' )
						-> textInput( [
							'id'          => 'import-price-field',
							'placeholder' => __(  'Import Price' )
						] )
						-> label( __(  'Import Price' ) );
					?>

                </div>

                <div class="col-sm-2 col-md-2 col-xs-12">

					<?= $form
						-> field( $model, 'surcharge' )
						-> textInput( [
							'id'          => 'surcharge-field',
							'placeholder' => __(  'Surcharge' )
						] )
						-> label( __(  'Surcharge' ) );
					?>

                </div>

            </div>

            <div class="col-sm-12 col-md-12 col-xs-12">

				<?= $form
					-> field( $model, 'description' )
					-> widget( TinyMce::className(), [
						'options' => [
							'rows' => 15
						],
						'language'      => 'en_GB',
						'clientOptions' => [
							'menubar'   => false,
							'plugins'   => [
								"advlist image autolink lists link charmap print preview anchor",
								"searchreplace visualblocks code fullscreen",
								"insertdatetime media table contextmenu paste"
							],
							'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | visualblocks | code "
						]
					] )
					-> label( false );
				?>

            </div>

            <div class="col-sm-12 col-md-12 col-xs-12">

                <div class="col-sm-3 col-md-3 col-xs-12">

					<?= $form
						-> field( $model, 'country' )
						-> textInput( [
							'id'          => 'country-field',
							'placeholder' => __(  'Country' )
						] )
						-> label( __(  'Country' ) );
					?>

                </div>

                <div class="col-sm-3 col-md-3 col-xs-12">

					<?= $form
						-> field( $model, 'city' )
						-> textInput( [
							'id'          => 'city-field',
							'placeholder' => __(  'City' )
						] )
						-> label( __(  'City' ) );
					?>

                </div>

                <div class="col-sm-3 col-md-3 col-xs-12">

					<?= $form
						-> field( $model, 'district' )
						-> textInput( [
							'id'          => 'district-field',
							'placeholder' => __(  'District' )
						] )
						-> label( __(  'District' ) );
					?>

                </div>

                <div class="col-sm-3 col-md-3 col-xs-12">

					<?= $form
						-> field( $model, 'street' )
						-> textInput( [
							'id'          => 'address-field',
							'placeholder' => __(  'Address' )
						] )
						-> label( __(  'Address' ) );
					?>

                </div>

            </div>

            <div class="col-md-12 col-md-12 col-xs-12">

                <div class="general-map">

                    <?= GoogleMaps::widget( [
                        'view'  => $this,
                        'model' => [
                            'country'  => $model->country,
                            'city'     => $model->city,
                            'district' => $model->district,
                            'street'   => $model->street
                        ]
                    ] ); ?>

                </div>

            </div>

			<?= $form->field( $model, 'location' )
				-> hiddenInput( [
					'id' => 'location'
				] )
				-> label( false );
			?>

            <div class="clearfix"></div>

            <div class="col-md-12 col-md-12 col-xs-12 text-center">

                <input class="btn btn-primary" data-value="test" type="submit" value="<?= ( $model->isNewRecord
                    ? __(  'Publish' )
                    : __(  'Update' ) )
                ?>">

            </div>

        </div>

    </div>

<?php ActiveForm::end(); ?>