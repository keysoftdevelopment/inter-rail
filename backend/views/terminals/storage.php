<?php /***************************************************************
 *	          THE LIST OF TERMINALS STORAGE DETAILS PAGE             *
 *********************************************************************/

use yii\widgets\LinkPager;

/* @var $this yii\web\View                         		 */
/* @var $searchModel common\models\TerminalStorageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider  	     */
/* @var $containers common\models\Containers[]           */
/* @var $terminals common\models\Terminals[]             */
/* @var $taxonomies common\models\Taxonomies[]           */

// current page title, that will displayed in head tags
$this->title = __(  'Terminals Storage' ); ?>

    <!-- title section -->
	<div class="page-title">

		<div class="title_left">

			<h3>
				<?= $this->title ?>
			</h3>

		</div>

	</div>
    <!-- title section -->

	<div class="clearfix"></div>

	<div class="row">

		<div class="col-md-12 col-sm-12 col-xs-12">

            <!-- filters section -->
			<div class="main-filters-container">

				<?= $this->render( 'filters', [
					'searchModel' => $searchModel
				] ) ?>

			</div>
            <!-- filters section -->

			<div class="x_panel">

				<div class="x_content">

                    <!-- terminal storage details -->
					<table class="table table-striped">

						<thead>

							<tr>

								<th style="width: 1%">
									#
								</th>

								<th>
									<?= __(  'Terminal Name' ) ?>
								</th>

								<th>
									<?= __(  'Container Number' ) ?>
								</th>

								<th>
									<?= __(  'Container Owner' ) ?>
								</th>

								<th>
									<?= __(  'Container Type' ) ?>
								</th>

								<th>
									<?= __(  'Enter Date' ) ?>
								</th>

								<th>
									<?= __(  'Export Date' ) ?>
								</th>

							</tr>

						</thead>

						<tbody>

						<?php if ( !empty( $dataProvider->models ) ) {

							foreach ( $dataProvider->models as $model ) { ?>

								<tr class="show-details">

									<td>
										#
									</td>

									<td>

										<?= isset( $terminals[ $model->terminal_id ] )
											? $terminals[ $model->terminal_id ][ 'name' ]
											: __( 'Unknown Terminal' )
										?>

									</td>

									<td>

										<?= isset( $containers[ $model->container_id ] )
											? $containers[ $model->container_id ][ 'number' ]
											: __( 'Unknown Container Number' )
										?>

									</td>

									<td>

										<?= isset( $containers[ $model->container_id ] )
											? $containers[ $model->container_id ][ 'owner' ]
											: __( 'Unknown Container Owner' )
										?>

									</td>

									<td>

										<?= isset( $taxonomies[ $model->container_id ] )
											? $taxonomies[ $model->container_id ][ 'name' ]
											: __( '	Type is absent' )
										?>

									</td>

									<td>

										<?= ! empty( $model->from )
											? date( 'd.m.Y', strtotime(  $model->from ) )
											: __( 'Date was not chosen' )
										?>

									</td>

									<td>

										<?= ! empty( $model->to )
											? date( 'd.m.Y', strtotime(  $model->to ) )
											: __( 'Date was not chosen' )
										?>

									</td>

								</tr>
							<?php }
						} else {

							echo '<tr>';

								echo '<td colspan="7" class="text-center">
									' . __(  'There is no storage details yet' ) . '
								</td>';

							echo '</tr>';

						} ?>

						</tbody>

					</table>
                    <!-- terminal storage details -->

				</div>

                <!-- pagination section -->
				<div class="pagination-container">

					<?= LinkPager::widget( [
						'pagination' => $dataProvider->pagination
					] ); ?>

				</div>
                <!-- pagination section -->

			</div>

		</div>

	</div>
