<?php /***************************************************************
 *         		  TERMINAL STORAGE FILTERS SECTION                   *
 *********************************************************************/

/** @var $searchModel common\models\TerminalStorageSearch */

use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

use kartik\date\DatePicker;

use common\models\Containers;
use common\models\Terminals;

//filters form
$form = ActiveForm::begin( [
	'id'      => 'storage-filter-form',
	'method'  => 'get',
	'action'  => Url::to( [
		'/terminals/storage'
	] )
] ); ?>

	<ul class="list-filters">

        <li class="col-sm-2 col-md-2 col-xs-12" style="padding-right: 0px">

			<?= DatePicker::widget( [
				'name'    => 'TerminalStorageSearch[from]',
				'type'    => DatePicker::TYPE_INPUT,
				'value'   => date('d.m.Y', strtotime( 'today' ) ),
				'options' => [
					'placeholder' => __(  'Enter Date' ),
					'class'       => 'form-control'
				],
				'pluginOptions' => [
					'autoclose' => true,
					'format'    => 'dd.mm.yyyy',
					'weekStart' => '1'
				]
			] ); ?>

        </li>

        <li class="col-sm-2 col-md-2 col-xs-12" style="padding-right: 0px">

			<?= DatePicker::widget( [
				'name'    => 'TerminalStorageSearch[to]',
				'type'    => DatePicker::TYPE_INPUT,
				'value'   => date('d.m.Y', strtotime( 'today' ) ),
				'options' => [
					'placeholder' => __(  'Export Date' ),
					'class'       => 'form-control'
				],
				'pluginOptions' => [
					'autoclose' => true,
					'format'    => 'dd.mm.yyyy',
					'weekStart' => '1'
				]
			] ); ?>

        </li>

        <li class="col-sm-3 col-md-3 col-xs-12" style="padding-right: 0px">

			<?= $form
				-> field( $searchModel, 'container_id' )
				-> dropDownList( ArrayHelper::map( Containers::find()
					-> where( [
						'isDeleted' => false
					] )
                    -> andWhere( [
                        '!=', 'owner', ''
                    ] )
					-> asArray()
					-> all(),
					'id',
					'owner'
				), [
					'prompt' => __(  'Select Container Owner' ),
					'class'  => 'form-control'
				] )
				-> label( false );
			?>

        </li>

        <li class="col-sm-3 col-md-3 col-xs-12" style="padding-right: 0px">

			<?= $form
				-> field( $searchModel, 'terminal_id' )
				-> dropDownList( ArrayHelper::map( Terminals::find()
                    -> where( [
                        'isDeleted' => false
                    ] )
                    -> asArray()
                    -> all(),
                    'id',
                    'name'
                ), [
					'prompt' => __(  'Select Terminal' ),
					'class'  => 'form-control'
				] )
				-> label( false );
			?>

        </li>

        <li class="col-sm-2 col-md-2 col-xs-12">

            <button class="btn btn-default" style="width: 100%">
				<?= __(  'Filter' ); ?>
            </button>

        </li>

	</ul>

<?php ActiveForm::end(); ?>