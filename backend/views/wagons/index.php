<?php /***************************************************************
 *	           THE LIST OF ALL AVAILABLE CONTAINERS PAGE             *
 *********************************************************************/

use yii\helpers\Html;
use yii\widgets\LinkPager;

/* @var $this yii\web\View                          */
/* @var $searchModel common\models\ContainersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider   */
/* @var $types common\models\Taxonomies[]           */

// current page title
$this->title = __(  'Wagon Numbers' ); ?>

    <div class="page-title">

        <!-- title section -->
        <div class="title_left">

            <h3>
                <?= $this->title ?>
            </h3>

        </div>
        <!-- title section -->

        <!-- search section -->
        <div class="title_right">

			<?= $this->render( '_search', [
				'model' => $searchModel
			] ); ?>

        </div>
        <!-- search section -->

    </div>

    <div class="clearfix"></div>

    <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12">

            <!-- filters section -->
            <div class="main-filters-container">

				<?= $this->render( 'filters', [
					'searchModel' => $searchModel
				] ); ?>

            </div>
            <!-- filters section -->

            <div class="x_panel">

                <div class="x_content">

                    <!-- wagons lists -->
                    <table class="table table-striped">

                        <thead>

                            <tr>

                                <th style="width: 1%">
                                    #
                                </th>

                                <th>
                                    <?= __(  'Wagon Number' ) ?>
                                </th>

                                <th>
                                    <?= __(  'Type' ) ?>
                                </th>

                                <th>
                                    <?= __(  'Owner' ) ?>
                                </th>

                                <th></th>

                            </tr>

                        </thead>

                        <tbody>

                            <?php if ( !empty( $dataProvider->models ) ) {

                                foreach ( $dataProvider->models as $model ) { ?>

                                    <tr>

                                        <td>
                                            #
                                        </td>

                                        <td>

                                            <?= Html::a( $model->number, [
                                                'update',
                                                'id' => $model->id
                                            ], [
                                                'role' => "button"
                                            ] ) ?>

                                        </td>

                                        <td>

                                            <?= isset( $types[ $model->id ] )
                                                ? $types[ $model->id ][ 'name' ]
                                                : __( 'Type is absent' )
                                            ?>

                                        </td>

                                        <td>
                                            <?= $model->owner ?>
                                        </td>

                                        <td class="text-center model-actions">

                                            <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                <i class="fa fa-ellipsis-v"></i>
                                            </a>

                                            <ul class="dropdown-menu pull-right">

                                                <li>

                                                    <?= Html::a( '<i class="fa fa-pencil"></i> ' .  __(  'Edit' ), [
                                                        'update',
                                                        'id'    => $model->id
                                                    ] ); ?>

                                                </li>

                                                <li>

                                                    <?= Html::a( '<i class="fa fa-trash"> </i> '. __(  'Delete' ), [
                                                        'delete',
                                                        'id' => $model->id
                                                    ], [
                                                        'data'  => [
                                                            'confirm' => __(  'Do you really want to delete this wagon number?' ),
                                                            'method'  => 'post',
                                                        ]
                                                    ] ); ?>

                                                </li>

                                            </ul>

                                        </td>

                                    </tr>

                                <?php }

                            } else {

                                echo '<tr>';

                                    echo '<td colspan="6" class="aligncenter">
                                        ' . __(  'There is no wagon numbers yet' ) . '
                                    </td>';

                                echo '</tr>';

                            } ?>

                        </tbody>

                    </table>
                    <!-- wagons lists -->

                </div>

                <!-- pagination section -->
                <div class="pagination-container">

                    <?= LinkPager::widget( [
                        'pagination' => $dataProvider->pagination
                    ] ); ?>

                </div>
                <!-- pagination section -->

            </div>

        </div>

    </div>