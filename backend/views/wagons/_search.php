<?php /***************************************************************
 *         		 	   	  SEARCH FORM SECTION                        *
 *********************************************************************/

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View                */
/* @var $model common\models\WagonsSearch */ ?>

    <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">

        <div class="input-group common_search">

            <?php $form = ActiveForm::begin( [
                'action'  => [ 'index' ],
                'method'  => 'get',
                'options' => [
                    'style' => 'float: right;'
                ]
            ] ); ?>

                <?= $form->field( $model, 'number' )
                    -> textInput( [
                        'class'       => 'form-control',
                        'placeholder' => __(  'Search for..' )
                    ] )
                    -> label( false );
                ?>

                <?= Html::submitButton( __(  'Go!' ), [
                    'class' => 'btn btn-default'
                ] ) ?>

            <?php ActiveForm::end(); ?>

        </div>

    </div>
