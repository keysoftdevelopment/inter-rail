<?php /***********************************************************
 *	     		 	   	 CREATE WAGON PART                       *
 *****************************************************************/

/* @var $this yii\web\View                */
/* @var $model \common\models\Wagons      */
/* @var $types common\models\Taxonomies[] */

// current page title
$this->title = __(  'Add wagon number' ); ?>

    <div class="col-md-12 col-sm-12 col-xs-12 create-wagon-block">

        <!-- title section -->
        <div class="page-title">

            <div class="title_left">

                <h3>
                    <?= $this->title ?>
                </h3>

            </div>

        </div>
        <!-- title section -->

        <!-- form section -->
        <div class="col-md-12 col-sm-12 col-xs-12 create-item-form">

            <?= $this->render( '_form', [
                'model' => $model,
				'types' => $types
            ] ); ?>

        </div>
        <!-- form section -->

    </div>