<?php /***************************************************************
 *         		 	     WAGONS FILTERS SECTION                      *
 *********************************************************************/

use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

use common\models\Taxonomies;
use common\models\Wagons;

/** @var $searchModel common\models\WagonsSearch */

//wagons filter form
$form = ActiveForm::begin( [
	'id'      => 'main-filter-form',
	'method'  => 'get',
	'action'  => Url::to( [
		'/wagons'
	] )
] ); ?>

    <ul class="list-filters">

        <li class="filter-element">

            <div class="form-group">

                <?= Html::dropDownList(
                    'type',
                    Yii::$app->getRequest()->getQueryParam( 'type' ),
                    ArrayHelper::map( Taxonomies::findAll( [
                        'type'      => 'railway',
                        'isDeleted' => false
                    ] ), 'id', 'name' ),
                    [
                        'prompt' => __(  'Select Type' ),
                        'class'  => 'form-control'
                    ]
                ); ?>

            </div>

        </li>

        <li class="filter-element">

            <?= $form
                -> field( $searchModel, 'owner' )
                -> dropDownList( ArrayHelper::map( Wagons::find()
                    -> where( [
                        'isDeleted' => false
                    ] )
                    -> andWhere( [
                        'IS NOT', 'owner', null
                    ] )
                    -> all(), 'owner', 'owner' ), [
                    'prompt' => __(  'Select Owner' ),
                    'class'  => 'form-control'
                ] )
                -> label( false )
            ?>

        </li>

        <li class="filter-element">

            <button class="btn btn-default">
                <?= __(  'Filter' ); ?>
            </button>

        </li>

    </ul>

<?php ActiveForm::end(); ?>


