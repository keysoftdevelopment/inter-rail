<?php /***************************************************************
 * 	    	 	   	     FORM SECTION FOR WAGONS                     *
 *********************************************************************/

use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View                */
/* @var $model common\models\Containers   */
/* @var $types common\models\Taxonomies[] */

//wagons main form
$form = ActiveForm::begin( [
    'options'                => [
        'class'                 => 'form-horizontal form-label-left ajax-submitting-form',
        'id'                    => 'wagon-form',
        'data-func'             => 'formCallback',
        'data-redirectonfinish' => Url::toRoute( [
            '/wagons'
        ] )
    ],
    'enableAjaxValidation'   => false,
    'enableClientValidation' => true
] ); ?>

    <div class="x_panel">

        <div class="x_content">

            <div class="col-sm-12 col-md-12 col-xs-12">

                <div class="col-sm-4 col-md-4 col-xs-12" style="padding-left: 0px">

					<?= $form
						-> field( $model, 'number' )
						-> textInput( [
							'id'          => 'number',
							'placeholder' => __(  'Number' )
						] )
						-> label( false );
					?>

                </div>

                <?php if ( ! empty( $types ) ) { ?>

                    <div class="col-sm-4 col-md-4 col-xs-12">

                        <?= $form
                            -> field( $model, 'type' )
                            -> dropDownList( ArrayHelper::map(
                                $types,
                                'id',
                                'name'
                            ), [
                                'prompt' => __(  'Choose Type' )
                            ] )
                            -> label( false );
                        ?>

                    </div>

				<?php } ?>

                <div class="col-sm-4 col-md-4 col-xs-12" style="padding-right: 0px">

					<?= $form
						-> field( $model, 'owner' )
						-> textInput( [
							'id'          => 'owner',
							'placeholder' => __(  'Owner' )
						] )
						-> label( false );
					?>

                </div>

            </div>

            <div class="clearfix"></div>

            <div class="col-md-12 col-md-12 col-xs-12 text-center">

                <input class="btn btn-primary" data-value="test" type="submit" value="<?= ( $model->isNewRecord
                    ? __(  'Publish' )
                    : __(  'Update' ) )
                ?>">

            </div>

        </div>

    </div>

<?php ActiveForm::end(); ?>