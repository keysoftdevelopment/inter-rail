<?php /***************************************************************
 *         		 	    CONTAINER FILTERS SECTION                    *
 *********************************************************************/

use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

use common\models\Containers;
use common\models\Taxonomies;

/** @var $searchModel common\models\ContainersSearch */

$form = ActiveForm::begin( [
	'id'      => 'main-filter-form',
	'method'  => 'get',
	'action'  => Url::to( [
		'/containers'
	] )
] ); ?>

    <ul class="list-filters">

        <li class="filter-element">

            <div class="form-group">

                <?= Html::dropDownList(
                    'type',
                    Yii::$app->getRequest()->getQueryParam( 'type' ),
                    ArrayHelper::map( Taxonomies::findAll( [
                        'type'      => 'containers',
                        'isDeleted' => false
                    ] ), 'id', 'name' ),
                    [
                        'prompt' => __(  'Select type' ),
                        'class'  => 'form-control'
                    ]
                ); ?>

            </div>

        </li>

        <li class="filter-element">

            <?= $form
                -> field( $searchModel, 'owner' )
                -> dropDownList( ArrayHelper::map( Containers::find()
                    -> where( [
                        'isDeleted' => false
                    ] )
                    -> andWhere( [
                        'IS NOT', 'owner', null
                    ] )
                    -> andWhere( [
                        '!=', 'owner', ''
                    ] )
                    -> all(), 'owner', 'owner' ), [
                    'prompt' => __(  'Select owner' ),
                    'class'  => 'form-control'
                ] )
                -> label( false )
            ?>

        </li>

        <li class="filter-element">

            <?= $form
                -> field( $searchModel, 'status' )
                -> dropDownList( ArrayHelper::map( Containers::find()
                    -> where( [
                        'isDeleted' => false
                    ] )
                    -> andWhere( [
                        'IS NOT', 'status', null
                    ] )
                    -> andWhere( [
                        '!=', 'status', ''
                    ] )
                    -> all(), 'status', 'status' ), [
                    'prompt' => __(  'Select status' ),
                    'class'  => 'form-control'
                ] )
                -> label( false ) ?>

        </li>

        <li class="filter-element">

            <button class="btn btn-default">
                <?= __(  'Filter' ); ?>
            </button>

        </li>

    </ul>

<?php ActiveForm::end(); ?>


