<?php /***************************************************************
 *	           THE LIST OF ALL AVAILABLE CONTAINERS PAGE             *
 *********************************************************************/

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;

use common\models\Orders;

/* @var $this yii\web\View                          */
/* @var $searchModel common\models\ContainersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider   */
/* @var $types common\models\Taxonomies[]           */
/* @var $orders common\models\ContainersRelation[]  */

// current page title
$this->title = __(  'Containers' ); ?>

    <div class="page-title">

        <!-- title section -->
        <div class="title_left">

            <h3>
                <?= $this->title ?>
            </h3>

        </div>
        <!-- title section -->

        <div class="title_right">

            <!-- import and export section -->
            <div class="col-md-6 col-sm-6 col-xs-12 xslx-details form-group">

                <ul>

                    <li>

                        <a href="javascript:void(0)" class="import-xlsx-file" data-details="<?= htmlentities( json_encode( [
							'url' => '/containers/import'
						] )) ?>">

                            <i class="fa fa-file-text"></i>
							<?= __( 'Import From Excel' ) ?>

                        </a>

                    </li>

                    <li>
                        /
                    </li>

                    <li>

                        <a href="<?= Url::to( [
							'/containers/export',
							'type'                     => Yii::$app->getRequest()->getQueryParam( 'type' ),
							'ContainersSearch[owner]'  => $searchModel->owner,
							'ContainersSearch[status]' => $searchModel->status
						] ) ?>" target="_blank">

                            <i class="fa fa-file-text"></i>
							<?= __( 'Export to Excel' ) ?>

                        </a>

                    </li>

                </ul>

            </div>
            <!-- import and export section -->

            <!-- search section -->
			<?= $this->render( '_search', [
				'model' => $searchModel
			] ); ?>
            <!-- search section -->

        </div>

    </div>

    <div class="clearfix"></div>

    <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12">

            <!-- filter section -->
            <div class="main-filters-container">

				<?= $this->render( 'filters', [
					'searchModel' => $searchModel
				] ); ?>

            </div>
            <!-- filter section -->

            <div class="x_panel">

                <div class="x_content">

                    <!-- container lists -->
                    <table class="table table-striped projects">

                        <thead>

                            <tr>

                                <th style="width: 1%">
                                    #
                                </th>

                                <th>
                                    <?= __(  'Container Number' ) ?>
                                </th>

                                <th>
                                    <?= __(  'Type' ) ?>
                                </th>

                                <th>
                                    <?= __(  'Owner' ) ?>
                                </th>

                                <th>
                                    <?= __(  'Status' ) ?>
                                </th>

                                <th></th>

                            </tr>

                        </thead>

                        <tbody>

                            <?php if ( !empty( $dataProvider->models ) ) {

                                //param initialization
                                $order_numbers = array_reduce( Orders::find()
                                    -> where( [
                                        'id' => ArrayHelper::getColumn(
                                                $orders, 'foreign_id'
                                        ),
                                        'isDeleted' => false
                                    ] )
                                    -> asArray()
                                    -> all(), function( $order_number, $order ) use ( $orders ) {

                                        if ( ! empty( $order ) ) {

                                            foreach ( $orders as $key => $value ) {

                                                if ( $value[ 'foreign_id' ] == $order[ 'id' ] ) {

                                                    $order_number[ $value[ 'container_id' ] ][] = [
                                                        'id'     => $order[ 'id' ],
                                                        'number' => $order[ 'number' ]
                                                    ];

                                                }
                                            }

                                        }

                                    return $order_number;

                                }, [] );

                                foreach ( $dataProvider->models as $model ) { ?>

                                    <tr class="show-details">

                                        <td>
                                            #
                                        </td>

                                        <td>

                                            <?= Html::a( $model->number, [
                                                'update',
                                                'id' => $model->id
                                            ], [
                                                'role' => "button"
                                            ] ) ?>

                                        </td>

                                        <td>

                                            <?= isset( $types[ $model->id ] )
                                                ? $types[ $model->id ][ 'name' ]
                                                : __( 'Type is absent' )
                                            ?>

                                        </td>

                                        <td>
                                            <?= $model->owner ?>
                                        </td>

                                        <td class="<?= $model->status ?>">
                                            <?= $model->status ?>
                                        </td>

                                        <td class="text-center model-actions">

                                            <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                <i class="fa fa-ellipsis-v"></i>
                                            </a>

                                            <ul class="dropdown-menu pull-right">

                                                <li>

                                                    <?= Html::a( '<i class="fa fa-pencil"></i> ' .  __(  'Edit' ), [
                                                        'update',
                                                        'id'    => $model->id
                                                    ] ); ?>

                                                </li>

                                                <li>

                                                    <?= Html::a( '<i class="fa fa-trash"> </i> '. __(  'Delete' ), [
                                                        'delete',
                                                        'id' => $model->id
                                                    ], [
                                                        'data'  => [
                                                            'confirm' => __(  'Do you really want to delete this container?' ),
                                                            'method'  => 'post',
                                                        ]
                                                    ] ); ?>

                                                </li>

                                            </ul>

                                        </td>

                                    </tr>

                                    <tr class="additional-details">

                                        <td colspan="6">

                                            <?php if ( isset( $order_numbers[ $model->id ] ) ) { ?>

                                                <h4>
                                                    <?= __( 'Order numbers' ) ?>
                                                </h4>

                                                <ul>

                                                    <?php foreach ( $order_numbers[ $model->id ] as $order ) { ?>

                                                        <li class="col-sm-1 col-md-1 col-xs-2">

                                                            <a href="<?= Url::to( [
                                                                '/orders/update?id=' . $order[ 'id' ]
                                                            ] ) ?>">
                                                                <?= $order[ 'number' ] ?>
                                                            </a>

                                                        </li>

                                                    <?php } ?>

                                                </ul>

                                            <?php } else {
                                                echo __( 'There is no order details yet' );
                                            } ?>

                                        </td>

                                    </tr>

                                <?php }

                            } else {

                                echo '<tr>';

                                    echo '<td colspan="6" class="aligncenter">
                                        ' . __(  'There is no containers yet' ) . '
                                    </td>';

                                echo '</tr>';

                            } ?>

                        </tbody>

                    </table>
                    <!-- container lists -->

                </div>

                <!-- pagination section -->
                <div class="pagination-container">

                    <?= LinkPager::widget( [
                        'pagination' => $dataProvider->pagination
                    ] ); ?>

                </div>
                <!-- pagination section -->

            </div>

        </div>

    </div>