<?php /***************************************************************
 *	   	            THE LIST OF AVAILABLE SERVICES PAGE              *
 *********************************************************************/

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\LinkPager;

use common\models\Languages;

/* @var $this yii\web\View                                */
/* @var $searchModel common\models\PostsSearch            */
/* @var $dataProvider yii\data\ActiveDataProvider         */
/* @var $is_models_translated common\models\Translation[] */

// current page title, that will displayed in head tags
$this->title = __(  'Services' );

// the list of all available languages
$languages = Languages::find()
    -> asArray()
    -> all(); ?>

    <div class="page-title">

        <!-- title section -->
        <div class="title_left">

            <h3>
                <?= $this->title ?>
            </h3>

        </div>
        <!-- title section -->

        <!-- search section -->
        <div class="title_right">

            <?= $this->render( '_search', [
                    'model' => $searchModel
            ] ); ?>

        </div>
        <!-- search section -->

    </div>

    <div class="clearfix"></div>

    <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12">

            <div class="x_panel">

                <div class="x_content">

                    <!-- services lists -->
                    <table class="table table-striped">

                        <thead>

                            <tr>

                                <th style="width: 1%">
                                    #
                                </th>

                                <th style="width: 10%">
                                    <?= __(  'Image' ) ?>
                                </th>

                                <th style="width: 15%">
                                    <?= __(  'Name' ) ?>
                                </th>

								<?php foreach ( $languages as $language ) { ?>

                                    <th style="width: 2%">
										<?= $language[ 'description' ] ?>
                                    </th>

								<?php } ?>

                                <th style="width: 20%">
                                    <?= __(  'Description' ) ?>
                                </th>

                                <th style="width: 1%"></th>

                            </tr>

                        </thead>

                        <tbody>

                            <?php if ( !empty( $dataProvider->models ) ) {

                                foreach ( $dataProvider->models as $model ) {

                                    //service thumbnail
									$thumbnail = isset( $thumbnails[ $model->file_id ][ 'guide' ] )
                                        ? Yii::getAlias( '@frontend_link' ) . $thumbnails[ $model->file_id ][ 'guide' ]
                                        : Yii::getAlias( '@web' ) . '/images/image-not-found.png';
									?>

                                    <tr>

                                        <td>
                                            #
                                        </td>

                                        <td>
                                            <img class="page-image" src="<?= $thumbnail ?>"/>
                                        </td>

                                        <td>

                                            <?= Html::a( $model->title, [
                                                'update',
                                                'id' => $model->id
                                            ], [
                                                'role' => "button"
                                            ] ) ?>

                                            <br/>

                                            <small>
                                                <?= $model->created_at ?>
                                            </small>

                                        </td>

										<?php foreach ( $languages as $language ) {

											if ( $language[ 'id' ] == $model->lang_id ) { ?>

                                                <td style="color: #00b100">
                                                    &#10003;
                                                </td>

											<?php } else {

												if ( in_array( (string)$model->id, is_null( $is_models_translated )
														? []
														: ArrayHelper::getColumn(
															$is_models_translated,
															'foreign_id'
														) )
													&& in_array( (string)$language[ 'id' ], is_null( $is_models_translated )
														? []
														: ArrayHelper::getColumn(
															$is_models_translated,
															'lang_id'
														) )
												) {  ?>

                                                    <td style="color: #00b100">
                                                        &#10003;
                                                    </td>


												<?php } else { ?>

                                                    <td>

														<?= Html::a( '&plus;', [
															'update', 'id' => $model->id, 'lang_id' => $language[ 'id' ]
														], [
															'class' => 'btn btn-info btn-xs', 'role' => 'button'
														] ) ?>

                                                    </td>

												<?php }
											}

										} ?>

                                        <td>

                                            <?= strlen( strip_tags( $model->description ) ) > 150
                                                ? \Yii::$app->common->excerpt( strip_tags( $model->description ) ) . ' ...'
                                                : $model->description;
                                            ?>

                                        </td>

                                        <td class="text-center model-actions">

                                            <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                <i class="fa fa-ellipsis-v"></i>
                                            </a>

                                            <ul class="dropdown-menu pull-right">

                                                <li>

													<?= Html::a( '<i class="fa fa-pencil"></i> ' .  __(  'Edit' ), [
														'update',
														'id' => $model->id
													] ); ?>

                                                </li>

                                                <li>

													<?= Html::a( '<i class="fa fa-trash"> </i> '. __(  'Delete' ), [
														'delete',
														'id' => $model->id
													], [
														'data'  => [
															'confirm' => __(  'Do you really want to delete this service?' ),
															'method'  => 'post',
														]
													] ); ?>

                                                </li>

                                            </ul>

                                        </td>

                                    </tr>

                                <?php }

                            } else {

                                echo '<tr>';

                                    echo '<td colspan="8" class="aligncenter">
                                        ' . __(  'There is no services yet' ) . '
                                    </td>';

                                echo '</tr>';

                            } ?>

                        </tbody>

                    </table>
                    <!-- services lists -->

                </div>

                <!-- pagination section -->
                <div class="pagination-container">

                    <?= LinkPager::widget( [
                        'pagination' => $dataProvider->pagination,
                    ] ); ?>

                </div>
                <!-- pagination section -->

            </div>

        </div>

    </div>