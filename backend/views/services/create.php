<?php /***************************************************************
 *	     		 	   	  CREATE SERVICE PART                        *
 *********************************************************************/

/* @var $this yii\web\View          */
/* @var $model \common\models\Posts */

// current page title
$this->title = __(  'Create Service' ); ?>

    <div class="col-md-12 col-sm-12 col-xs-12">

        <!-- title section -->
        <div class="page-title">

            <div class="title_left">

                <h3>
                    <?= $this->title ?>
                </h3>

            </div>

        </div>
        <!-- title section -->

        <!-- form section -->
        <div class="col-md-9 col-sm-12 col-xs-12 service-form">

            <?= $this->render( '_form', [
                'model' => $model
            ] ); ?>

        </div>
        <!-- form section -->

        <!-- sidebar -->
        <div class="col-md-3 col-sm-12 col-xs-12 sidebar">

            <!-- service thumbnail -->
            <div class="x_panel thumbnail-sidebar">

                <div class="x_title">

                    <h2>
                        <?= __(  'Thumbnail' ) ?>
                    </h2>

                    <div class="clearfix"></div>
                </div>

                <div class="container">

                    <div class="row">

                        <div class="col-md-12 col-sm-12 col-xs-12 text-center">

                            <img src="" class="thumbnail-view" />

                            <a href="#service-modal" data-toggle="modal" data-target="#service-modal" class="choose-thumbnail">
                                <?= __(  'Select Image' ) ?>
                            </a>

                        </div>

                    </div>

                </div>

            </div>
            <!-- service thumbnail -->

            <!-- seo details -->
            <div class="x_panel seo-sidebar">

                <div class="x_title">

                    <h2>
                        <?= __(  'SEO' ) ?>
                    </h2>

                    <div class="clearfix"></div>

                </div>

                <div class="x_content">

                    <div class="form-group">

                        <div class="col-md-12 col-sm-12 col-xs-12">

                            <label for="seo_title">
								<?= __(  'SEO title' ) ?>
                            </label>

                            <input class="form-control" id="seo-title"/>

                        </div>

                    </div>

                    <div class="form-group">

                        <div class="col-md-12 col-sm-12 col-xs-12">

                            <label for="seo_description">
								<?= __(  'SEO description' ) ?>
                            </label>

                            <textarea class="form-control" id="seo-description"></textarea>

                        </div>

                    </div>

                    <div class="form-group">

                        <div class="col-md-12 col-sm-12 col-xs-12">

                            <label for="seo_tags">
								<?= __(  'SEO tags' ) ?>
                            </label>

                            <input class="form-control" id="seo-tags" type="text"/>

                        </div>

                    </div>

                </div>

            </div>
            <!-- seo details -->

        </div>
        <!-- sidebar -->

    </div>