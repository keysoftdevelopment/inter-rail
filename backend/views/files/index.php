<?php /***************************************************************
 *	  	   	     THE LIST OF ALL AVAILABLE FILES PAGE                *
 *********************************************************************/

use yii\helpers\Html;
use yii\widgets\LinkPager;

use common\widgets\Alert;

/* @var $this yii\web\View                        */
/* @var $searchModel common\models\FilesSearch    */
/* @var $dataProvider yii\data\ActiveDataProvider */

// current page title, that will displayed in head tags
$this->title = __(  'Media' ); ?>

    <!-- title section -->
    <div class="page-title">

        <div class="title_left">

            <h3>
                <?= $this->title ?>
            </h3>

        </div>

    </div>
    <!-- title section -->

    <div class="clearfix"></div>

    <!-- media lists -->
    <div class="row">

        <div class="col-md-12 col-xs-12">

            <div class="x_panel">

                <div class="x_content">

                    <!-- flash messages -->
                    <?= Alert::widget() ?>
                    <!-- flash messages -->

                    <div class="row">

                        <?php if ( !empty( $dataProvider->models ) ) {

                            foreach ( $dataProvider->models as $model ) {

                                //param initialization
                                $file_type = pathinfo(
                                    Yii::getAlias( '@frontend_link' ) . $model->guide, PATHINFO_EXTENSION
                                ); ?>

                                <div class="col-sm-3 col-md-3 col-xs-12">

                                    <div class="thumbnail media_file">

                                        <div class="image view view-first">

                                            <?php if ( in_array( $file_type, [ 'zip', 'xlsx', 'xls', 'word', 'sql' ] ) ) { ?>
                                                <i class="fa fa-file"></i>
                                            <?php } else { ?>
                                                <img src="<?= Yii::getAlias( '@frontend_link' ) . $model->guide ?>" alt="image"/>
                                            <?php } ?>

                                            <div class="mask">

                                                <p>
                                                    <?= basename( $model->guide ) ?>
                                                </p>

                                                <div class="tools tools-bottom">

                                                    <?= Html::a( '<i class="fa fa-times"></i>' . __( 'Delete' ), [
                                                        'delete',
                                                        'id'   => $model->id
                                                    ], [
                                                        'data' => [
                                                            'confirm' => __(  'Do you really want to delete this file?' ),
                                                            'method'  => 'post',
                                                        ],
                                                    ] ); ?>

                                                </div>

                                            </div>

                                        </div>

                                        <div class="caption">

                                            <p>
                                                <?= basename( $model->guide ) ?>
                                            </p>

                                        </div>

                                    </div>

                                </div>

                            <?php }

                        } else {
                            echo __(  'There is no files yet' );
                        } ?>

                        <!-- pagination section -->
                        <div class="pagination-container">

                            <?= LinkPager::widget( [
                                'pagination' => $dataProvider->pagination,
                            ] ); ?>

                        </div>
                        <!-- pagination section -->

                    </div>

                </div>

            </div>

        </div>

    </div>
    <!-- media lists -->