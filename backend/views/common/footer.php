<?php /***************************************************************
 *         		 	   	      FOOTER SECTION                         *
 *********************************************************************/

use yii\helpers\Html;

use common\models\Settings;

//social links to company profile added in admin panel from settings tab
$socials= Settings::find()
    -> where( [
        'name' => [
            'facebook',
            'whatsapp',
            'youtube',
            'gplus_link'
        ]
    ] )
    -> indexBy( 'name' )
    -> asArray()
    -> all(); ?>

<!-- footer -->
<footer>

	<div class="container">

		<div class="row">

			<!-- copyright -->
			<div class="copyright text-center">

				<h5 class="text-center">
					<?= __( 'Deliver on time' ) ?>
				</h5>

				<ul>

                    <li>

						<?= Html::a( '<i class="fa fa-facebook"></i>', isset( $settings[ 'facebook' ][ 'description' ] )
						&& ! empty( $settings[ 'facebook' ][ 'description' ] )
							? $settings[ 'facebook' ][ 'description' ]
							: 'javascript:void(0)', [
								'class' => 'facebook'
							]
						); ?>

                    </li>

                    <li>

						<?= Html::a( '<i class="whatsapp-icon"></i>', isset( $settings[ 'whatsapp' ][ 'description' ] )
						&& ! empty( $settings[ 'whatsapp' ][ 'description' ] )
							? $settings[ 'whatsapp' ][ 'description' ]
							: 'javascript:void(0)', [
								'class' => 'whatsapp'
							]
						); ?>

                    </li>

                    <li>

						<?= Html::a( '<i class="fa fa-youtube-play"></i>', isset( $settings[ 'youtube' ][ 'description' ] )
						&& ! empty( $settings[ 'youtube' ][ 'description' ] )
							? $settings[ 'youtube' ][ 'description' ]
							: 'javascript:void(0)', [
								'class' => 'youtube'
							]
						); ?>

                    </li>

                    <li>

						<?= Html::a( '<i class="fa fa-google-plus"></i>', isset( $settings[ 'gplus' ][ 'description' ] )
						&& ! empty( $settings[ 'gplus' ][ 'description' ] )
							? $settings[ 'gplus' ][ 'description' ]
							: 'javascript:void(0)', [
								'class' => 'gplus'
							]
						); ?>

                    </li>

				</ul>

				<div class="site-info text-center">
					© <?= date( 'Y', strtotime( 'today' ) ) ?> InterRail Services AG
				</div>

			</div>
            <!-- copyright -->

		</div>

	</div>

</footer>
<!-- footer -->
