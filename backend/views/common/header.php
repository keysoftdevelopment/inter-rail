<?php /***************************************************************
 *	    		 	   	  ADMIN PANEL HEADER SECTION                 *
 *********************************************************************/

use yii\bootstrap\Nav;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */ ?>

    <div class="col-md-3 left_col">

        <div class="left_col scroll-view">

            <!-- logo section -->
            <div class="navbar nav_title dashboard_logo">
                <img src="<?= Yii::getAlias( '@web' ) ?>/images/logo.png">
            </div>
            <!-- logo section -->

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile">

                <div class="profile_pic">
                    <img src="<?= Yii::$app->user->identity->getAvatar( 'blue' ) ?>" alt="..." class="img-circle profile_img">
                </div>

                <div class="profile_info">

                    <span>
                        <?= __(  'Welcome' ) ?>,
                    </span>

                    <h2>
                        <?= Yii::$app->user->identity->username ?>
                    </h2>

                </div>

            </div>
            <!-- menu profile quick info -->

            <!-- sidebar menu section -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

                <div class="menu_section">

                    <ul class="nav side-menu">

                        <?php foreach ( Yii::$app->params[ 'menu' ] as $menu ) {

                            //param initialization
							$active = $menu[ 'action' ] == Yii::$app->request->get( 'type' )
                                || $menu[ 'action' ] == Yii::$app->controller->id && ! in_array( Yii::$app->request->get( 'type' ), [
                                    'train',
                                    'blockTrain'
                                ] )
                                || ( isset( $menu[ 'foreign' ][ Yii::$app->controller->id ] )
                                    && ( ! isset( $menu[ 'foreign' ][ Yii::$app->controller->id ][ 'type' ] )
                                    || in_array(
                                        Yii::$app->request->get( 'type' ),
                                        $menu[ 'foreign' ][ Yii::$app->controller->id ][ 'type' ]
                                    ) )
                                )
                                    ? true
                                    : false;

							//check current user permissions
							if ( ! in_array( false, array_map( function( $permission ) {
								return ! Yii::$app->user->can( $permission );
							}, $menu[ 'permissions' ] ) ) ) {
								continue;
                            }

                            //is user has access
							for ( $index = 0; $index < count( $menu[ 'items' ] ); $index++ ) {

								$no_item_access = array_map( function( $permission ) {

									return ! Yii::$app->user->can( $permission )
                                        ? true
                                        : false;

								}, $menu[ 'items' ][ $index ][ 'permissions' ] );

								if ( ! in_array( false, $no_item_access ) ) {
									unset( $menu[ 'items' ][ $index ] );
                                } else {
                                    $menu[ 'items' ][ $index ][ 'label' ] = __(  $menu[ 'items' ][ $index ][ 'label' ] );
								}

                            }

                            //adding additional sections to transportation menu section
							if ( $menu[ 'name' ] == 'Transportation' ) {

								// additional menu items for logistics menu section
							    $logistic_types = Yii::$app->common->logisticTypes();

							    if (  ! empty( $logistic_types ) ) {

							        //adding logistic types to menu sections
									foreach ( $logistic_types as $type ) {

										if ( Yii::$app->user->can( $type[ 'name' ] ) ) {

											$menu[ 'items' ][] = [
												'label' => __( $type[ 'name' ] ),
												'url'   => [
													'/logistics?type=' . str_replace( [
														' ', '-', '_'
													], '', strtolower( $type[ 'name' ] )
													)
												]
											];

										}

									}

                                }

                                //sorting menu items
								krsort( $menu[ 'items' ] );

                            } ?>

                            <li class="<?= $active
                                ? 'active'
                                : ''
                            ?>">

                                <a>
                                    <i class="fa  <?= $menu[ 'icon' ] ?>"></i>
                                    <?= __(  $menu[ 'name' ] ) ?>
                                    <span class="fa fa-chevron-down"></span>
                                </a>

                                <?= Nav::widget( [
                                    'encodeLabels' => false,
                                    'options'      => [
                                        'class' => 'nav child_menu',
                                        'style' => $active
                                            ? ''
                                            : 'display: none'
                                    ],
                                    'items' => $menu[ 'items' ]
                                ] ); ?>

                            </li>

                        <?php } ?>

                    </ul>

                </div>

            </div>
            <!-- sidebar menu section -->

        </div>

    </div>

    <!-- top navigation -->
    <div class="top_nav">

        <div class="nav_menu">

            <nav class="" role="navigation">

                <div class="nav toggle">

                    <a id="menu_toggle">
                        <i class="fa fa-bars"></i>
                    </a>

                </div>

                <ul class="nav navbar-nav navbar-right">

                    <li>

                        <a href="javascript:void(0);" class="user-profile dropdown-toggle dashboard" data-toggle="dropdown" aria-expanded="false">
                            <img src="<?= Yii::$app->user->identity->getAvatar( 'black' ) ?>" alt=""><?= Yii::$app->user->identity->username ?>
                            <span class=" fa fa-angle-down"></span>
                        </a>

                        <ul class="dropdown-menu dropdown-usermenu pull-right">

                            <li>

                                <?= Html::a( __(  'Profile' ), Url::to( [
                                    'user/update'
                                ] ) ) ?>

                            </li>

                            <li>

								<?= Html::a( '<i class="fa fa-sign-out pull-right"></i>' . ' ' .  __(  'Log Out' ), Url::to( [
									'dashboard/logout'
								] ) ) ?>

                            </li>

                        </ul>

                    </li>

                </ul>

            </nav>

        </div>

    </div>
    <!-- top navigation -->