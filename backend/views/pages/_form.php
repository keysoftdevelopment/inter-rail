<?php /***************************************************************
 * 	    	 	   	    FORM SECTION FOR PAGE                        *
 *********************************************************************/

use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

use dosamigos\tinymce\TinyMce;

use common\models\Languages;

use backend\widgets\MediaPopup;

/* @var $this yii\web\View         */
/* @var $model common\models\Posts */
/* @var $lang_id integer           */

$form = ActiveForm::begin( [
    'options'                => [
        'class'                 => 'form-horizontal form-label-left need-image need-seo',
        'id'                    => 'page-form',
        'data-func'             => 'formCallback',
        'data-redirectonfinish' => Url::toRoute( [
            '/pages'
        ] )
    ],
    'enableAjaxValidation'   => false,
    'enableClientValidation' => false
] ); ?>

    <!-- form details -->
    <div class="x_panel">

        <div class="x_content">

            <?= $form
                -> field( $model, 'file_id', [
                    'template' => '{input}',
                    'options'  => [
                        'tag' => false, // Don't wrap with "form-group" div
                    ]
                ] )
                -> hiddenInput( [
                    'id' => 'file_id'
                ] )
                -> label( false );
            ?>

            <?= $form
                -> field( $model, 'type', [
					'template' => '{input}',
					'options'  => [
						'tag' => false, // Don't wrap with "form-group" div
					]
				] )
                -> hiddenInput( [
                    'readonly' => true,
                    'value'    => 'page'
                ] )
                -> label( false );
            ?>

            <?= $form
                -> field( $model, 'seo', [
                    'template' => '{input}',
                    'options'  => [
                        'tag' => false, // Don't wrap with "form-group" div
                    ]
                ] )
                -> hiddenInput( [
                    'id' => 'seo-model'
                ] )
                -> label( false );
            ?>

            <?= $form
                -> field( $model, 'company', [
					'template' => '{input}',
					'options'  => [
						'tag' => false, // Don't wrap with "form-group" div
					]
				] )
                -> hiddenInput( [
                    'id' => 'company'
                ] )
                -> label( false );
            ?>

            <?= $form
                -> field( $model, 'statistics', [
					'template' => '{input}',
					'options'  => [
						'tag' => false, // Don't wrap with "form-group" div
					]
				] )
                -> hiddenInput( [
                    'id' => 'statistics'
                ] )
                -> label( false );
            ?>

            <?= $form
                -> field( $model, 'slider', [
					'template' => '{input}',
					'options'  => [
						'tag' => false, // Don't wrap with "form-group" div
					]
				] )
                -> hiddenInput( [
                    'id' => 'slider-id'
                ] )
                -> label( false );
            ?>

            <?= $form
                -> field( $model, 'template', [
					'template' => '{input}',
					'options'  => [
						'tag' => false, // Don't wrap with "form-group" div
					]
				] )
                -> hiddenInput( [
                    'id'    => 'theme-template',
                    'value' => empty( $model->template )
                        ? 'default'
                        : $model->template
                ] )
                -> label( false );
            ?>

            <div class="form-group">

                <?= $form
                    -> field( $model, 'title' )
                    -> textInput( [
                        'id'          => 'title',
                        'placeholder' => __(  'Title' )
                    ] )
                    -> label( false );
               ?>

                <?= $form
                    -> field( $model, 'lang_id' )
                    -> dropDownList( ArrayHelper::map(
                        Languages::find()->all(),
                        'id',
                        'description'
                    ), [
						'options' => [
							$lang_id => [
								'Selected'=>'selected'
							]
						]
					], [
                        'prompt' => __(  'Choose language' )
                    ] )
                    -> label( false );
                ?>

            </div>

			<?= $form
                -> field( $model, 'description' )
                -> widget( TinyMce::className(), [
                    'options' => [
                        'rows' => 15
                    ],
                    'language'      => 'en_GB',
				    'clientOptions' => [
					    'menubar'   => false,
					    'plugins'   => [
						    "advlist image autolink lists link charmap print preview anchor",
						    "searchreplace visualblocks code fullscreen",
						    "insertdatetime media table contextmenu paste"
					    ],
					    'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | visualblocks | code "
				    ]
			    ] )
                -> label( false );
			?>

            <div class="form-group">

                <div class="col-md-12 col-md-12 col-xs-12 text-center">

                    <input class="btn btn-primary" data-value="test" type="submit" value="<?= ( $model->isNewRecord
                        ? __(  'Publish' )
                        : __(  'Update' ) )
                    ?>">

                </div>

            </div>

        </div>

    </div>
    <!-- form details -->

    <!-- company details section -->
    <?= $this->render( 'form-sections/company-info', [
        'model' => $model,
        'form'  => $form
    ] ); ?>
    <!-- company details section -->

    <!-- statistics section -->
    <?= $this->render( 'form-sections/statistics', [
        'model'   => $model,
        'lang_id' => $lang_id
    ] ); ?>
    <!-- statistics section -->

    <?php if ( isset( $model[ 'lang_id' ] )
        && (int)$lang_id > 0
        && $model[ 'lang_id' ] == $lang_id
        || (int)$lang_id == 0
    ) { ?>

        <!-- contact details section -->
        <?= $this->render( 'form-sections/contact-info', [
            'model' => $model,
            'form'  => $form
        ] ); ?>
        <!-- contact details section -->

    <?php } ?>

<?php ActiveForm::end(); ?>

<!-- media details section -->
<?= MediaPopup::widget( [
	'formType'   => 'page-modal',
	'uploadForm' => $this->render( 'upload' )
] ) ?>
<!-- media details section -->
