<?php /***************************************************************
 * 		     	   FORM SECTION PART FOR CONTACT INFO         		 *
 *********************************************************************/

/* @var $model common\models\Posts   */
/* @var $form yii\widgets\ActiveForm */

use backend\widgets\GoogleMaps; ?>

    <div class="x_panel" id="contact-section" style="display: none">

        <div class="x_title">

            <h2>
                <?= __(  'Contact Info' ) ?>
            </h2>

            <ul class="nav navbar-right panel_toolbox" style="min-width: 30px;">

                <li>
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                </li>

            </ul>

            <div class="clearfix"></div>

        </div>

        <div class="x_content">

            <div class="general-map">

                <?= GoogleMaps::widget( [
                    'view'  => $this,
                    'model' => json_decode( stripslashes( $model->contact_info ) )
                ] ); ?>

            </div>

            <?= $form->field( $model, 'contact_info' )
                -> hiddenInput( [
                    'id'    => 'location',
                    'value' => $model->contact_info
                ] )
                -> label( false );
            ?>

        </div>

    </div>