<?php /***************************************************************
 * 		     	   FORM SECTION PART FOR COMPANY INFO         		 *
 * 		 		I.E WILL BE DISPLAYED ONLY ON HOME PAGE 			 *
 *********************************************************************/

use dosamigos\tinymce\TinyMce;

/* @var $model common\models\Posts   */
/* @var $form yii\widgets\ActiveForm */

//decoding for translated details
$model->company = ! empty( $model->company )
    && ! is_array( $model->company )
    ? (array)json_decode( $model->company )
    : $model->company;
?>

    <div class="x_panel" id="company-section">

        <div class="x_title">

            <h2>
                <?= __(  'Company Info' ) ?>
            </h2>

            <ul class="nav navbar-right panel_toolbox">

                <li>
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                </li>

            </ul>

            <div class="clearfix"></div>

        </div>

        <div class="x_content">

            <div class="company-info-thumbnail">

                <img class="center-block" src="<?= isset( $model->company[ 'file_id' ] )
                    ? Yii::$app->common->thumbnail( $model->company[ 'file_id' ] )
                    : ''
                ?>" >

                <a href="#page-modal" data-thumbnail-index="0" data-toggle="modal" data-target="#page-modal">
                    <?= __( 'Select Image' ) ?>
                </a>

            </div>

            <?= $form
                -> field( $model, 'company[file_id]' )
                -> hiddenInput( [
                    'class' => 'file-id'
                ] )
                ->label( false );
            ?>

            <?= $form
                -> field( $model, 'company[title]' )
                -> textInput( [
                    'id'          => 'company-info-title',
                    'placeholder' => __(  'Title' )
                ] )
                -> label( false );
            ?>


            <?= $form->field( $model, 'company[desc]' )
                -> widget( TinyMce::className(), [
                    'options' => [
                        'rows' => 15
                    ],
                    'language'      => 'en_GB',
                    'clientOptions' => [
                        'menubar'   => false,
                        'plugins'   => [
                            "advlist autolink lists link charmap print preview anchor",
                            "searchreplace visualblocks code fullscreen",
                            "insertdatetime media table contextmenu paste"
                        ],
                        'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist | link image | code "
                    ]
                ] )
                -> label( false );
            ?>

        </div>

    </div>