<?php /***************************************************************
 * 		     	   FORM SECTION PART FOR STATISTICS         		 *
 * 		 		I.E WILL BE DISPLAYED ONLY ON HOME PAGE 			 *
 *********************************************************************/

/* @var $model common\models\Posts */
/* @var $lang_id integer           */

//the list of icons, used on home page statistic section
$icons = [
    'train-icon-black' => __( 'Cargo Delivery Icon' ),
    'world-icon'       => __( 'World Icon' ),
	'customers-icon'   => __( 'Satisfied Customers Icon' ),
	'products-icon'    => __( 'Products Icon' )
]; ?>

    <div class="x_panel" id="statistics-section" style="display: none">

        <div class="x_title">

            <h2>
                <?= __(  'STATISTICS' ) ?>
            </h2>

            <ul class="nav navbar-right panel_toolbox" style="min-width: 30px;">

                <li>
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                </li>

                <?php if ( (int)$lang_id > 0
                    && $model->lang_id == $lang_id
                    || (int)$lang_id == 0
                ) { ?>

                    <li class="dropdown">

                        <a href="#" class="dropdown-toggle add-item" id="add-statistic" data-toggle="dropdown" data-type="statistics">
                            +
                        </a>

                    </li>

                <?php } ?>

            </ul>

            <div class="clearfix"></div>

        </div>

        <div class="x_content">

            <div class="statistics-container" style="float: left; width: 100%;">

                <?php if ( ! empty( $model->statistics ) ) {

                    //param initialization
                    $statistics = json_decode(
                        $model->statistics
                    );

                    if ( isset( $statistics->title ) ) {

                        foreach ( $statistics->title as $key => $statistic ) { ?>

                            <?= $this->render( 'statistic', [
                                'lang_id' => $lang_id,
                                'icons'   => $icons,
                                'model'   => [
                                    'lang_id' => $model->lang_id,
                                    'icon'    => isset( $statistics->icon[ $key ] )
										? $statistics->icon[ $key ]
										: '',
                                    'title' => $statistic,
                                    'count' => isset( $statistics->count[ $key ] )
										? $statistics->count[ $key ]
										: '',
                                    'desc' => isset( $statistics->desc[ $key ] )
										? $statistics->desc[ $key ]
										: ''
                                ]
                            ] ) ?>

                    <?php }
                    }
                } ?>

            </div>

            <script type="text/html" id="statistics-template">

                <?= $this->render( 'statistic', [
                    'lang_id' => $lang_id,
                    'icons'   => $icons,
                    'model'   => [
                        'lang_id' => $model->lang_id
                    ]
                ] ) ?>

            </script>

        </div>

    </div>