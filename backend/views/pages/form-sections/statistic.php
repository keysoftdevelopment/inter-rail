<?php /***************************************************************
 *	                    SINGLE STATISTIC SECTION                     *
 *********************************************************************/

use yii\helpers\Html;

/* @var $lang_id mixed */
/* @var $icons array   */
/* @var $model array   */ ?>

	<div class="statistic">

        <?= Html::hiddenInput( 'Posts[statistics][icon][]',
            isset( $model[ 'icon' ] )
                ? $model[ 'icon' ]
                : '', [
                'class' => 'statistic-icon'
            ]
        ); ?>

		<?php if ( isset( $model[ 'lang_id' ] ) && (int)$lang_id > 0
			&& $model[ 'lang_id' ] == $lang_id || (int)$lang_id == 0
		) : ?>

			<div class="remove-statistic remove-current-option" data-parent="statistic" data-translations="<?= htmlentities( json_encode( [
				'title'   => __( 'CONFIRMATION MODULE' ),
				'message' => __( 'Do you really want to delete this option?' )
			] ) ) ?>">
				x
			</div>

		<?php endif; ?>

		<div class="col-sm-12 col-md-12 col-xs-12" style="margin-bottom: 5px;">

			<div class="col-sm-1 col-md-1 col-xs-12">

                <div class="form-group stats-icons-lists dropdown">

                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">

                        <span class="selected-icon">

                            <i class="fa fa-<?= isset( $model[ 'icon' ] )
                                ? str_replace( [ 'world-icon', 'customers-icon', 'products-icon', '-icon-black'  ], [ 'globe', 'male', 'hdd-o', '' ], $model[ 'icon' ] )
                                : 'automobile'
                            ?>"></i>

                        </span>

                        <i class="fa fa-caret-down"></i>

                    </a>

                    <ul class="dropdown-menu">

						<?php foreach ( Yii::$app->common->icons( 'statistics' ) as $key => $value ) { ?>

                            <li data-option="<?= htmlentities( json_encode( [
								'key'   => $key,
								'value' => $value
							] ) ) ?>">
                                <i class="fa <?= $value ?>"></i>
                            </li>

						<?php } ?>

                    </ul>

                </div>

			</div>

            <div class="col-sm-11 col-md-11 col-xs-12">

                <div class="col-sm-2 col-md-2 col-xs-12 statistic-count" style="padding: 0px;">

                    <?= Html::textInput(
                        'Posts[statistics][count][]',
                        isset( $model[ 'count' ] )
                            ? $model[ 'count' ]
                            : '', [
                             'class'      => 'count form-control',
                            'placeholder' => __( 'QUANTITY' )
                        ]
                    ) ?>

                </div>

                <div class="col-sm-10 col-md-10 col-xs-12 statistic-title">

					<?= Html::textInput(
						'Posts[statistics][title][]',
						isset( $model[ 'title' ] )
							? $model[ 'title' ]
							: '', [
							'class'      => 'title form-control',
							'placeholder' => __( 'TITLE' )
						]
					) ?>

                </div>

            </div>

		</div>

		<div class="col-sm-12 col-md-12 col-xs-12">

			<?= Html::textarea(
				'Posts[statistics][desc][]',
				isset( $model[ 'desc' ] )
					? $model[ 'desc' ]
					: '', [
					'class'      => 'description form-control',
					'placeholder' => __( 'DESCRIPTION' )
				]
			) ?>

		</div>

	</div>
