<?php /********************************/
/*                                    */
/*          COMMON MAIN CONFIG        */
/*                                    */
/**************************************/

return [
    'name'       => 'Inter Rail website',
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [

        'db'    => require( dirname(__DIR__) . '/config/db.php' ),
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],

        'commandBus' => [
            'class' => 'trntv\bus\CommandBus'
        ],

        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName'  => false,
            'rules' 		  => [],
        ],

		'lotus' => [
			'class' => 'common\components\LotusAPI',
		],

        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],

		'mailer' => [
			'class' 		   => 'yii\swiftmailer\Mailer',
			'viewPath' 		   => '@common/mail',
			'useFileTransport' => false,
			'transport' 	   => [
				'class'      => 'Swift_SmtpTransport',
				'host' 	     => 'ssl://smtp.yandex.com',
				'username'   => 'ceo@kssd.uz',
				'password'   => 'Abkrf101ngi7ugo',
				'port' 	     => '465'
			]
		]

    ]

];