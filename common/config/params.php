<?php /********************************/
/*                                    */
/*      COMMON ADDITIONAL PARAMS      */
/*                                    */
/**************************************/

return [
    'adminEmail'                    => 'admin@interrail.uz',
    'supportEmail'                  => 'info@interrail.uz',
    'contactEmail'                  => 'support@interrail.uz',
    'user.passwordResetTokenExpire' => 3600,
	'lotus'						    => [
		'invoices' => [
			'details' => [
				'invoicetype' 		  => 'IO',
				'invoicenumber'       => '',
				'invoicenumberorigin' => '',
				'foreignkey' 		  => '',
				'costcenter' 		  => '',
				'creditoraccount'	  => '',
				'debitoraccount'	  => '',
				'performdate' 		  => '',
				'invoicedate' 		  => '',
				'netamount'			  => '',
				'taxamount'			  => 0,
				'amount' 			  => '',
				'currency' 		      => 'USD',
				'exchangerate'	      => '',
				'exchangecurrency'    => '',
				'exchangeamount'      => '',
				'payconditions'       => '',
				'salesmanager'        => '',
				'creator'			  => ''
			],
			'transportdata' => [
				'forwardingnumber' => '',
				'shipment' 		   => '',
				'sender' 		   => '',
				'receiver' 		   => '',
				'pickupcity' 	   => '',
				'deliverycity' 	   => '',
				'incoterm' 		   => '',
				'colli' 		   => '',
				'weight' 		   => '',
				'payableweight'    => '',
				'volume' 		   => 0,
				'ldm' 			   => 0,
				'regionfrom' 	   => '',
				'regionto' 		   => '',
				'value' 		   => 0,
				'distance' 	       => '',
				'pallet' 		   => 0,
				'tid' 			   => '',
				'reference' 	   => '',
				'ordernumber' 	   => '',
			],
			'customer' => [
				'customernumber'  => '',
				'controlkey' 	  => '',
				'foreignkey' 	  => '',
				'name1' 		  => '',
				'name2' 		  => '',
				'street' 		  => '',
				'city' 		      => '',
				'zip' 			  => '',
				'countrycode' 	  => '',
				'phone' 		  => '',
				'fax' 		      => '',
				'mail' 			  => '',
				'vatid' 		  => '',
				'taxid' 		  => '',
				'companyid' 	  => '',
				'currency' 	      => 'USD',
				'creditoraccount' => '',
				'debitoraccount'  => '',
				'freeaddress' 	  => [
					'', ''
				]
			],
			'subpositions' => [
		    	'costocde'    => '',
				'description' => '',
				'unit' 		  => '',
				'quantity'    => 1,
				'unitprice'   => '',
				'amount' 	  => '',
				'tax' 	      => 0,
				'taxcat' 	  => '',
				'taxamount'   => 0,
				'totalamount' => ''
			],
			'taxtotals' => [
				'tax' 		     => 0,
				'amount' 		 => '',
				'taxamount' 	 => 0,
				'totalamount'    => '',
				'exchangeamount' => 0
			]
		],
		'company' => [
			'type'   	  => 'Client',	  //Client, Agent, Carrier - required
			'sector' 	  => 'Logistics', //Automotive - not required
			'code'	      => '6',		  //Unique ID of Address / Addressnumber - required
			'parent_code' => '',	      //Unique ID of ParentAddress / ParentAddressnumber - not required
			'foreignkey'  => '',		  //Unique ID of Address / Addressnumber in Partner-EDI-System - not required
			'name1' 	  => '',		  // Companyname - not required
			'name2' 	  => '',		  // Companyname second line   - not required
			'match'		  => '',		  // Searchname - not required
			'street'	  => '',		  //not required
			'building'	  => '',		  //not required
			'appartment'  => '',		  //not required
			'zip'	  	  => '',		  //required
			'city'	      => '',		  //required
			'country'	  => '',		  //required
			'language'    => 'EN',		  //not required
			'taxnumber'   => '',		  //not required
			'email'  	  => '',		  //not required
			'phone'  	  => '',		  //not required
			'website'  	  => '',		  //not required
			'comment'  	  => ''		      //not required
		],
		'person' => [
			'type'   	  => 'Client',	  //Client, Agent, Carrier - required
			'code'	      => '6',		  //Unique ID of Address / Addressnumber - required
			'parent_code' => '',	      //Unique ID of ParentAddress / ParentAddressnumber - not required
			'foreignkey'  => '',		  //Unique ID of Address / Addressnumber in Partner-EDI-System - not required
			'name' 	 	  => '',		  //required
			'gname' 	  => '',		  //required
			'title'		  => '',		  //Mr or Mrs - required
			'degree'	  => '',		  //not required
			'position'	  => '',		  //not required
			'department'  => '',		  //not required
			'street'	  => '',		  //not required
			'building'	  => '',		  //not required
			'appartment'  => '',		  //not required
			'zip'	  	  => '',		  //required
			'city'	      => '',		  //required
			'country'	  => '',		  //required
			'language'    => 'EN',		  //not required
			'email'  	  => '',		  //not required
			'comment'  	  => ''		      //not required
		]
	]
];