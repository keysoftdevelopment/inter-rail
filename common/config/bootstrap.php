<?php /********************************/
/*                                    */
/*            COMMON CONFIG           */
/*                                    */
/**************************************/

Yii::setAlias( '@common', dirname( __DIR__ ) );
Yii::setAlias( '@frontend', dirname( dirname( __DIR__ ) ) . '/frontend' );
Yii::setAlias( '@backend', dirname( dirname( __DIR__ ) ) . '/backend' );
Yii::setAlias( '@console', dirname( dirname( __DIR__ ) ) . '/console' );
Yii::setAlias( '@frontend_web', '/' );
Yii::setAlias( '@frontend_link', 'http://dev-interrail.uz/' );
Yii::setAlias( '@backend_link', 'http://admin.dev-interrail.uz/' );

if ( isset( $_SERVER[ 'HTTP_X_FORWARDED_FOR' ] )
	&& !empty( $_SERVER[ 'HTTP_X_FORWARDED_FOR' ] )
) {
    $ips                    = explode( ',', $_SERVER[ 'HTTP_X_FORWARDED_FOR' ] );
    $_SERVER[ 'REMOTE_ADDR' ] = trim( $ips[0] );
} else if ( isset( $_SERVER[ 'HTTP_X_REAL_IP' ] )
	&& !empty( $_SERVER[ 'HTTP_X_REAL_IP' ] )
) {
    $_SERVER[ 'REMOTE_ADDR' ] = $_SERVER[ 'HTTP_X_REAL_IP' ];
} else if ( isset( $_SERVER[ 'HTTP_CLIENT_IP' ] )
	&& !empty( $_SERVER[ 'HTTP_CLIENT_IP' ] )
) {
    $_SERVER[ 'REMOTE_ADDR' ] = $_SERVER[ 'HTTP_CLIENT_IP' ];
}

/**
 * Translations main method
 * @param mixed $message
 * @param array $params
 * @param null  $language
 * @return string
**/
function __( $message, $params = [], $language = null )
{

    return Yii::t(
    	'app',
		$message,
		$params,
		$language
	);

}

/**
 * Merge requested objects
 * @param mixed $model
 * @param array $params
 * @return object
**/
function merge_objects( $model, $params )
{

	foreach ( $params as $key => $value )
		$model->{ $key } = $value;

	return $model;

}