<?php namespace common\components;

/**************************************/
/*                                    */
/*        LOTUS API COMPONENT         */
/*                                    */
/**************************************/

use Yii;

use yii\base\Component;

use common\models\Settings;

class LotusAPI extends Component
{

    /**
     * New invoice in lotus system
     * @param array $params
	 * @return mixed
    **/
    public function invoice( $params )
	{

		//param initialization
		$lotus 	  = \Yii::$app->params[ 'lotus' ][ 'invoices' ];
		$response = [];

		foreach ( ! empty( $params[ 'transportation' ][ 'containers' ] )
			? $params[ 'transportation' ][ 'containers' ]
			: $params[ 'transportation' ][ 'wagons' ]
		as $key => $transportation ) {

			if ( ! empty( $transportation ) ) {

				//customer and company details
				$current_customer = $params[ 'users' ][ $params[ 'customer_id' ] ];
				$current_company  = isset( $params[ 'companies' ][ $current_customer[ 'company_id' ] ] )
					? $params[ 'companies' ][ $current_customer[ 'company_id' ] ]
					: [];

				//requested company location details
				$location = isset( $params[ 'places' ][ $current_company[ 'place_id' ] ] )
					? $params[ 'places' ][ $current_company[ 'place_id' ] ]
					: [];

				//weight per container or wagon
				$weight	= ! empty( $params[ 'order' ][ 'weight_dimensions' ] ) && isset( json_decode( $params[ 'order' ][ 'weight_dimensions' ] )->gross )
					? (float)json_decode( $params[ 'order' ][ 'weight_dimensions' ] )->gross
					: '';

				//amount per container or wagon
				$amount	= (float)( $params[ 'amount' ]/( count( ! empty( $params[ 'transportation' ][ 'containers' ] )
					? $params[ 'transportation' ][ 'containers' ]
					: $params[ 'transportation' ][ 'wagons' ] ) )
				);

				//lotus customer info
				$customer = array_replace( $lotus[ 'customer' ], [
					'name1'  => $current_customer[ 'last_name' ],
					'name2'  => $current_customer[ 'first_name' ],
					'street' => ! empty( $location )
						? $location[ 'street' ]
						: '',
					'city' 		      => ! empty( $location )
						? $location[ 'city' ]
						: '',
					'zip' 			  => ! empty( $location )
						? $location[ 'postal_code' ]
						: '',
					'countrycode' 	  => ! empty( $location )
						? $location[ 'country_code' ]
						: '',
					'phone' => $current_customer[ 'phone' ],
					'mail' 	=> $current_customer[ 'email' ],
				] );

				$response[] = $this->request( [
					'url'	  => 'Invoice/CREATE ',
					'options' => [
						'invoice' => [
							'invoicedata' => array_replace( $lotus[ 'details' ], [
								'invoicenumber' => $params[ 'invoice_number' ],
								'performdate'	=> $params[ 'invoice_date' ],
								'invoicedate'	=> $params[ 'invoice_date' ],
								'netamount'		=> $amount,
								'amount'		=> $amount,
								'creator'		=> $params[ 'users' ][ $params[ 'creator' ] ][ 'first_name' ] . ' ' . $params[ 'users' ][ $params[ 'creator' ] ][ 'last_name' ]
							] ),
							'transportdata' => array_replace( $lotus[ 'transportdata' ], [
								'sender' 	    => $params[ 'order' ]->shipper,
								'receiver' 	    => $params[ 'order' ]->consignee,
								'pickupcity'    => $params[ 'order' ]->trans_from,
								'deliverycity'  => $params[ 'order' ]->trans_to,
								'weight' 	    => $weight,
								'payableweight' => $weight,
								'tid' 		    => $transportation[ 'number' ],
								'ordernumber'   => $params[ 'order' ]->number,
							] ),
							'customer'	     => $customer,
							'client' 		 => $customer,
							'creatoraddress' => $customer,
							'subpositions' 	 => [
								array_replace( $lotus[ 'subpositions' ], [
									'amount' 	  => $amount,
									'totalamount' => $amount
								] )
							],
							'taxtotals' => array_replace( $lotus[ 'taxtotals' ], [
								'amount' 	  => $amount,
								'totalamount' => $amount
							] )
						]
					]
				] );

			}

		}

		return $response;

	}

	protected function request( $params )
	{

		//params initializations
		$request     = curl_init();
		$lotus_links = Settings::find()
			-> select( [
				'name',
				'value' => 'description'
			] )
			-> where( [
				'name' => [
					'lotus_link',
					'is_sandbox',
					'lotus_sandbox_link'
				]
			] )
			-> indexBy( 'name' )
			-> asArray()
			-> all();

		//set request api url
		curl_setopt(
			$request,
			CURLOPT_URL,
			isset( $lotus_links[ 'is_sandbox' ] ) && (int)$lotus_links[ 'is_sandbox' ][ 'value' ] > 0
				? $lotus_links[ 'lotus_sandbox_link' ][ 'value' ] . $params[ 'url' ]
				: $lotus_links[ 'lotus_link' ][ 'value' ] . $params[ 'url' ]
		);

		//enable post requests
		curl_setopt(
			$request,
			CURLOPT_POST,
			TRUE
		);

		//adding headers to request
		curl_setopt(
			$request,
			CURLOPT_HTTPHEADER, [
				'Content-Type: application/json'
			]
		);

		//enable return transfer
		curl_setopt(
			$request,
			CURLOPT_RETURNTRANSFER,
			TRUE
		);

		//enable verbose option
		curl_setopt(
			$request,
			CURLOPT_VERBOSE,
			TRUE
		);

		//disable ssl verify peer option
		curl_setopt(
			$request,
			CURLOPT_SSL_VERIFYPEER,
			FALSE
		);

		//disable ssl verify host option
		curl_setopt(
			$request,
			CURLOPT_SSL_VERIFYHOST,
			FALSE
		);

		//set body params
		curl_setopt(
			$request,
			CURLOPT_POSTFIELDS,
			utf8_encode( json_encode(
				$params[ 'options' ]
			) )
		);

		$result = curl_exec( $request );

		curl_close( $request );

		return $result;

	}

}