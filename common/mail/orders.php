<?php /***************************************************************
 *	                      ORDER MAIL TEMPLATE                        *
 *********************************************************************/

/** @var $this \yii\web\View */
/** @var $order array        */ ?>

<div class="order">

    <h4 style="text-align: center; color: #ffffff; text-transform: uppercase;">

        <?php if ( $order[ 'type' ] == 'client'
            && in_array( $order[ 'status' ], [ 'prequote', 'completion' ] )
        ) {

            echo $order[ 'status' ] == 'prequote'
				? __( 'Your request was applied. Our manager will take a look your request and we will give you updates soon. Thank you' )
                : __( 'Your request was successfully completed. Thank you for being with us' );

        } else if ( $order[ 'type' ] == 'client'
            && in_array( $order[ 'status' ], [
                'confirmation',
                'invoice'
            ] )
        ) {

			echo $order[ 'status' ] == 'confirmation'
                ? __( 'Your request waiting confirmation. Please follow' )
                    . ' <a href="' . Yii::getAlias( '@frontend_link' ) . '/cabinet/orders">'
                        . __( 'this link' )
                    . '</a> ' . __( 'and confirm your request' )
                : __( 'Your request waiting invoice payment. You can download invoice document in you personal cabinet. Please, follow' )
				    . ' <a href="' . Yii::getAlias( '@frontend_link' ) . '/cabinet/orders">'
                        . __( 'this link' )
                    .'</a> ' . __( 'and make invoice payment. Thank you' );

        } else if ( $order[ 'type' ] == 'manager' ) {

			echo $order[ 'status' ] == 'confirmation'
				? __( 'Customer was confirmed order request. Please follow' )
				    . ' <a href="' . Yii::getAlias( '@backend_link' ) . '/orders/update?id=' . $order[ 'id' ] . '">'
				        . __( 'this link' )
				. '</a> '
				: __( 'Order was assigned to you. Please follow' )
				    . ' <a href="' . Yii::getAlias( '@backend_link' ) . '/orders/update?id=' . $order[ 'id' ] . '">'
				        . __( 'this link' )
				    . '</a> ' . __( 'and begin working with requested order' );

        } else if ( $order[ 'type' ] == 'admin' ) {

			echo $order[ 'status' ] == 'prequote'
                ? __( 'New order request. Please follow' )
                    . ' <a href="' . Yii::getAlias( '@backend_link' ) . '/orders/update?id=' . $order[ 'id' ] . '">'
                        . __( 'this link' )
                    . '</a> ' . __( 'and chose manager' )
			    : __( 'Order was successfully completed. Please follow' )
                    . ' <a href="' . Yii::getAlias( '@backend_link' ) . '/orders/update?id=' . $order[ 'id' ] . '">'
                        . __( 'this link' )
                    . '</a> ' . __( 'and take a look order status' );

        } else {

			echo __( 'Requested order was assigned to you. Please follow' )
				. ' <a href="' . Yii::getAlias( '@backend_link' ) . '/orders/update?id=' . $order[ 'id' ] . '">'
				    . __( 'this link' )
				. '</a> ' . __( 'and begin working with requested order' );

        } ?>

    </h4>

    <table style="width:100%">

        <thead>

            <tr>

                <th style="color: #ffffff; text-transform: uppercase; padding: 10px 0px; background: #3fadb9">
                    <?= __( 'Order Number' ) ?>
                </th>

                <th style="color: #ffffff; text-transform: uppercase; padding: 10px 0px; background: #3fadb9">
                    <?= __( 'From' ) ?>
                </th>

                <th style="color: #ffffff; text-transform: uppercase; padding: 10px 0px; background: #3fadb9">
					<?= __( 'To' ) ?>
                </th>

                <?php if ( $order[ 'type' ] !== 'contractor' ) { ?>

                    <th style="color: #ffffff; text-transform: uppercase; padding: 10px 0px; background: #3fadb9">
                        <?= __( 'Status' ) ?>
                    </th>

                <?php } ?>

            </tr>

        </thead>

        <tbody>

            <tr>

                <td style="padding: 5px; text-transform: uppercase; color: #ffffff; font-weight: bold; background: #2e95a0; text-align: center">

                    <a href="<?= $order[ 'type' ] == 'client'
                        ? Yii::getAlias( '@frontend_link' ) . '/cabinet/orders'
                        : Yii::getAlias( '@backend_link' ) . '/orders/update?id=' . $order[ 'id' ]
                    ?>">

					    <?= ! empty( $order[ 'number' ] )
                            ? $order[ 'number' ]
                            : $order[ 'id' ]
                        ?>

                    </a>

                </td>

                <td style="padding: 5px; text-transform: uppercase; color: #ffffff; font-weight: bold; background: #2e95a0; text-align: center">
					<?= $order[ 'from' ] ?>
                </td>

                <td style="padding: 5px; text-transform: uppercase; color: #ffffff; font-weight: bold; background: #2e95a0; text-align: center">
					<?= $order[ 'to' ] ?>
                </td>

				<?php if ( $order[ 'type' ] !== 'contractor' ) { ?>

                    <td style="padding: 5px; text-transform: uppercase; color: #ffffff; font-weight: bold; background: #2e95a0; text-align: center">
                        <?= $order[ 'status' ] ?>
                    </td>

                <?php } ?>

            </tr>

        </tbody>

        <?php if ( in_array( $order[ 'type' ], [ 'manager', 'contractor' ] ) ) { ?>

            <tfoot>

                <tr>

                    <td colspan="<?= $order[ 'type' ] == 'manager'
                        ? 4
                        : 3
                    ?>">
                        <?= $order[ 'note' ] ?>
                    </td>

                </tr>

            </tfoot>

        <?php } ?>

    </table>

</div>