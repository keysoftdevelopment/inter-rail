<?php /***************************************************************
 *	                  PASSWORD RESET MAIL TEMPLATE                   *
 *********************************************************************/

use yii\helpers\Html;

/* @var $this yii\web\View       */
/* @var $user common\models\User */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl( [
    'token' => $user->password_reset_token
] ); ?>

<div class="password-reset" style="padding: 20px; text-align: center; text-transform: uppercase; color: #ffff;">

    <p>
        <?= __( 'Hello' ) ?> <?= Html::encode( $user->first_name ) ?> <?= Html::encode( $user->last_name ) ?>
    </p>

    <p>
        <?= __( 'Please, follow the ling below to reset your password' ) ?>:
		<?= Html::a( __( 'Reset Password' ), $resetLink ) ?>
    </p>

</div>
