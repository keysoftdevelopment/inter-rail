<?php /***************************************************************
 *	                    CONTACT US MAIL TEMPLATE                     *
 *********************************************************************/

/** @var $this \yii\web\View               */
/** @var $contact \frontend\models\Contact */ ?>

<table style="width: 100%;">

    <thead>

        <tr>

            <th style="color: #ffffff; text-transform: uppercase; padding: 10px 0px; background: #3fadb9;">
				<?= __( 'Company' ) ?>
            </th>

            <th style="color: #ffffff; text-transform: uppercase; padding: 10px 0px; background: #3fadb9;">
				<?= __( 'Name' ) ?>
            </th>

            <th style="color: #ffffff; text-transform: uppercase; padding: 10px 0px; background: #3fadb9;">
				<?= __( 'Phone' ) ?>
            </th>

            <th style="color: #ffffff; text-transform: uppercase; padding: 10px 0px; background: #3fadb9;">
				<?= __( 'Email' ) ?>
            </th>

        </tr>

    </thead>

    <tbody>

        <tr>

            <td style="padding: 5px; text-transform: uppercase; color: #ffffff; font-weight: bold; background: #2e95a0; text-align: center;">
				<?= $contact[ 'company' ] ?>
            </td>

            <td style="padding: 5px; text-transform: uppercase; color: #ffffff; font-weight: bold; background: #2e95a0; text-align: center;">
                <?= $contact[ 'name' ] ?>
            </td>

            <td style="padding: 5px; text-transform: uppercase; color: #ffffff; font-weight: bold; background: #2e95a0; text-align: center;">
                <?= $contact[ 'phone' ] ?>
            </td>

            <td style="padding: 5px; text-transform: uppercase; color: #ffffff; font-weight: bold; background: #2e95a0; text-align: center;">
                <?= $contact[ 'email' ] ?>
            </td>

        </tr>

    </tbody>

    <tfoot>

        <tr>

            <td colspan="4" style="color: #ffffff; background: #3fadb9; padding: 5px; font-size: 14px; text-align: justify;">
			    <?= $contact[ 'message' ] ?>
            </td>

        </tr>

    </tfoot>

</table>