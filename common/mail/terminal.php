<?php /***************************************************************
 *	                      TERMINAL MAIL TEMPLATE                     *
 *********************************************************************/

/** @var $this \yii\web\View */
/** @var $order array        */
/** @var $dates array        */
/** @var $status string      */ ?>

<div class="terminal">

	<h4 style="text-align: center; color: #ffffff; text-transform: uppercase;">

		<?= __( 'Hello, sending you this mail with container details in attachment file, please take a look it' ) ?>
        <br>
		<?=  __( 'Details for period and type of containers given bellow' ); ?>

	</h4>

	<table style="width:100%">

		<thead>

			<tr>

				<th style="color: #ffffff; text-transform: uppercase; padding: 10px 0px; background: #3fadb9">
					<?= __( 'Date From' ) ?>
				</th>

				<th style="color: #ffffff; text-transform: uppercase; padding: 10px 0px; background: #3fadb9">
					<?= __( 'Date To' ) ?>
				</th>

				<th style="color: #ffffff; text-transform: uppercase; padding: 10px 0px; background: #3fadb9">
					<?= __( 'Container Status' ) ?>
				</th>

			</tr>

		</thead>

		<tbody>

			<tr>

				<td style="padding: 5px; text-transform: uppercase; color: #ffffff; font-weight: bold; background: #2e95a0; text-align: center">
					<?= $dates[ 'from' ] ?>
				</td>

				<td style="padding: 5px; text-transform: uppercase; color: #ffffff; font-weight: bold; background: #2e95a0; text-align: center">
					<?= $dates[ 'to' ] ?>
				</td>

				<td style="padding: 5px; text-transform: uppercase; color: #ffffff; font-weight: bold; background: #2e95a0; text-align: center">
					<?= $status ?>
				</td>

			</tr>

		</tbody>

	</table>

</div>