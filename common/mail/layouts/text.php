<?php /***************************************************************
 *	                         MAIL TEXT LAYOUT                        *
 *********************************************************************/

/* @var $this \yii\web\View view component instance */
/* @var $content string main view render result */

$this->beginPage();

	$this->beginBody() ?>

		<?= $content ?>

	<?php $this->endBody();

$this->endPage(); ?>
