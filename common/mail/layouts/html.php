<?php /***************************************************************
 *	                         MAIL HTML LAYOUT                        *
 *********************************************************************/

use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $content string main view render result     */

$this->beginPage(); ?>

    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

    <html xmlns="http://www.w3.org/1999/xhtml">

        <head>

            <meta http-equiv="Content-Type" content="text/html; charset=<?= Yii::$app->charset ?>"/>

            <title>
                <?= Html::encode( $this->title ) ?>
            </title>

            <?php $this->head() ?>

        </head>

        <body style="background: #efefef;padding: 10px;">

            <table border="0" cellpadding="0" cellspacing="0" width="600" style="background-color:#fdfdfd; border:1px solid #efefef; border-radius:3px !important; margin: 20px auto;">

                <tbody>

                    <tr>

                        <td align="center" valign="top">

                            <table border="0" cellpadding="0" cellspacing="0" width="600" style="width: 100%; background-color: #ffffff; border-radius:3px 3px 0 0 !important;color:#ffffff;border-bottom:0;font-weight:bold;line-height:100%;vertical-align:middle;">

                                <tbody>

                                    <tr>

                                        <td style="padding: 10px 0px; display: block; float: left; width: 100%; text-align: center;">
                                            <img src="http://interrail.uz/images/logo.png" style="max-width: 100%;">
                                        </td>

                                    </tr>

                                </tbody>

                            </table>

                        </td>

                    </tr>

                    <tr>

                        <td align="center" valign="top">

                            <table border="0" cellpadding="0" cellspacing="0" width="600">

                                <tbody>

                                    <tr>

                                        <td valign="top" style="background-color:#fdfdfd;">

                                            <table border="0" cellpadding="20" cellspacing="0" width="100%" style="background: #7fc6ce">

                                                <tbody>

                                                    <tr>

                                                        <td valign="top" style="padding: 0px;">

                                                            <h4 style="text-align: center; color: #1f191a; text-transform: uppercase; background: #efefef; padding: 10px; font-size: 15px; font-weight: bold; margin: 3px 0px 0px;">
                                                                <?= __( 'Mail Details' ); ?>
                                                            </h4>

                                                            <?php $this->beginBody();

                                                                echo $content;

                                                            $this->endBody(); ?>

                                                        </td>

                                                    </tr>

                                                </tbody>

                                            </table>

                                        </td>

                                    </tr>

                                </tbody>

                            </table>

                        </td>

                    </tr>

                    <tr>

                        <td align="center" valign="top">

                            <table border="0" cellpadding="10" cellspacing="0" width="600">

                                <tbody>

                                    <tr>

                                        <td valign="top" style="padding:0;">

                                            <table border="0" cellpadding="10" cellspacing="0" width="100%">

                                                <tbody>

                                                    <tr>

                                                        <td colspan="2" valign="middle" style="padding: 0px; text-align: center;">

                                                            <ul style="padding-left: 0px; list-style: none;">

                                                                <li style="display: inline-block;">

                                                                    <a href="javascript:void(0)" class="facebook">
                                                                        <img src="http://interrail.uz/images/fb-icon.png" alt=""/>
                                                                    </a>

                                                                </li>

                                                                <li style="display: inline-block;">

                                                                    <a href="javascript:void(0)" class="whatsapp">
                                                                        <img src="http://interrail.uz/images/whatsapp-icon.png" alt=""/>
                                                                    </a>

                                                                </li>

                                                                <li style="display: inline-block;">

                                                                    <a href="javascript:void(0)" class="youtube">
                                                                        <img src="http://interrail.uz/images/youtube-icon.png" alt=""/>
                                                                    </a>

                                                                </li>

                                                                <li style="display: inline-block;">

                                                                    <a href="javascript:void(0)" class="gplus">
                                                                        <img src="http://interrail.uz/images/gplus-icon.png" alt=""/>
                                                                    </a>

                                                                </li>

                                                            </ul>

                                                        </td>

                                                    </tr>

                                                </tbody>

                                            </table>

                                        </td>

                                    </tr>

                                </tbody>

                            </table>

                        </td>

                    </tr>

                </tbody>

            </table>

        </body>

    </html>

<?php $this->endPage() ?>