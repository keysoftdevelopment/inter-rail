<?php /***************************************************************
 *	                REGISTRATION COMPLETE MAIL TEMPLATE              *
 *********************************************************************/

/** @var $this \yii\web\View */
/** @var $user array         */ ?>

<div class="registration">

    <h4 style="text-align: center; color: #ffffff; text-transform: uppercase;">
		<?= __( 'You was successfully registered in. Your credentials are given bellow' ) ?>
    </h4>

    <table style="width:100%">

        <thead>

            <tr>

                <th style="color:#ffffff;text-transform:uppercase;padding:10px 0px;background:#3fadb9">
                    <?= __( 'Login' ) ?>
                </th>

                <th style="color:#ffffff;text-transform:uppercase;padding:10px 0px;background:#3fadb9">
                    <?= __( 'Password' ) ?>
                </th>

            </tr>

        </thead>

        <tbody>

            <tr>

                <td style="padding:5px;text-transform:uppercase;color:#ffffff;font-weight:bold;background:#2e95a0;text-align:center">
                    <?= $user[ 'login' ] ?>
                </td>

                <td style="padding:5px;text-transform:uppercase;color:#ffffff;font-weight:bold;background:#2e95a0;text-align:center">
                    <?= $user[ 'password' ] ?>
                </td>

            </tr>

        </tbody>

    </table>

</div>
