<?php namespace common\models;

/********************************************/
/*                                          */
/*               COMPANIES MODEL            */
/*                                          */
/********************************************/

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii2tech\ar\softdelete\SoftDeleteBehavior;

use common\behaviors\LoggingBehavior;

/**
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $bank
 * @property string $mfo
 * @property string $oked
 * @property integer $file_id
 * @property integer $place_id
 * @property bool $isDeleted
 * @property string $updated_at
 * @property string $created_at
 *
 * @property Files $files
 * @property Places $places
**/
class Companies extends ActiveRecord
{

	/**
	 * @inheritdoc
	**/
	public $country;

	/**
	 * @inheritdoc
	**/
	public $city;

	/**
	 * @inheritdoc
	**/
	public $district;

	/**
	 * @inheritdoc
	**/
	public $location;

	/**
	 * @inheritdoc
	**/
	public $street;

    /**
     * @inheritdoc
    **/
    public static function tableName()
    {
        return '{{%companies}}';
    }

	/**
	 * @inheritdoc
	**/
	public function behaviors()
	{

		return [

			'softDeleteBehavior' => [
				'class'                     => SoftDeleteBehavior::className(),
				'softDeleteAttributeValues' => [
					'isDeleted' => true
				],
				'replaceRegularDelete' => true
			],

			'timestamp'          => [
				'class' 	 => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => [
						'updated_at',
						'created_at'
					],
					ActiveRecord::EVENT_BEFORE_UPDATE => [
						'updated_at'
					]
				],
				'value' => function () {
					return date( 'Y-m-d H:i:s', strtotime( 'now' ) );
				}
			], [
				'class'  	 	=> LoggingBehavior::className(),
				'type'	  	 	=> 'companies',
				'foreign_id'    => 'id',
				'trace_options' => [
					'name',
					'description',
					'bank',
					'mfo',
					'oked',
					'place_id'
				]
			]

		];

	}

    /**
     * @inheritdoc
	**/
    public function rules()
    {

        return [
            [
            	[
            		'name'
				], 'required'
			], [
				[
					'description', 'bank', 'mfo', 'oked'
				], 'string'
			], [
				[
					'file_id', 'place_id'
				], 'integer'
			], [
				[
					'updated_at', 'created_at'
				], 'safe'
			], [
				[
					'name'
				], 'string',
				'max' => 255
			], [
				[
					'place_id'
				], 'exist',
				'skipOnError'     => true,
				'targetClass'     => Places::className(),
				'targetAttribute' => [
					'place_id' => 'id'
				]
			], [
				[
					'file_id'
				], 'exist',
				'skipOnError'     => true,
				'targetClass'     => Files::className(),
				'targetAttribute' => [
					'file_id' => 'id'
				]
			]
        ];

    }

    /**
     * @inheritdoc
	**/
    public function attributeLabels()
    {

        return [
            'id'          => __( 'ID' ),
            'name'        => __( 'Name' ),
            'description' => __( 'Description' ),
			'bank'		  => __( 'Bank' ),
			'mfo'		  => __( 'MFO' ),
			'oked'		  => __( 'OKED' ),
            'file_id'     => __( 'File ID' ),
			'place_id'    => __( 'Place ID' ),
			'updated_at'  => __( 'Updated At' ),
            'created_at'  => __( 'Created At' )
        ];

    }

	/**
	 * @inheritdoc
	 * @return CompaniesQuery the active query used by this AR class.
	**/
	public static function find()
	{

		//param initialization
		$query = new CompaniesQuery(
			get_called_class()
		);

		return $query->where( [
			'isDeleted' => false
		] );

	}

	/**
	 * Filling requested company with places data
	**/
	public function getLocation()
	{

		//param initialization
		$location = Places::findOne(
			$this->place_id
		);

		//set company location details
		if ( ! is_null( $location ) ) {

			foreach ( [ 'country', 'city', 'street' ] as $param )
				$this->{$param} = $location->{$param};

			$this->location = json_encode( [
				'lat' => $location->lat,
				'lng' => $location->lng
			] );

		}

	}

	/**
	 * This method is called at the beginning of inserting or updating a record.
	 * The default implementation will trigger an [[EVENT_BEFORE_INSERT]] event when `$insert` is `true`,
	 * or an [[EVENT_BEFORE_UPDATE]] event if `$insert` is `false`.
	 * When overriding this method, make sure you call the parent implementation like the following:
	 * @param bool $insert whether this method called while inserting a record.
	 * If `false`, it means the method is called while updating a record.
	 * @return bool whether the insertion or updating should continue.
	 * If `false`, the insertion or updating will be cancelled.
	**/
    public function beforeSave( $insert )
    {

        if ( parent::beforeSave( $insert )
			&& $this->validate()
		) {

        	//param initialization
        	$location = Places::findOne( [
        		'country'  => $this->country,
				'city'	   => $this->city
			] );

        	//saving company location details
        	if ( is_null( $location ) ) {

        		//set property to location
				$location = merge_objects( new Places(), [
					'country'  => $this->country,
					'city'	   => $this->city,
					'district' => $this->district,
					'street'   => $this->street,
				] );

				//saving location
				$location->save( false );

			}

			//set location id to company
			$this->place_id = $location->id;

			return true;

        }

        return false;

    }

}