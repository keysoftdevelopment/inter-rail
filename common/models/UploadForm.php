<?php namespace common\models;

/********************************************/
/*                                          */
/*            UPLOAD FORM MODEL             */
/*                                          */
/********************************************/

use Yii;

use yii\base\Model;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;

use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;

use ZipArchive;

class UploadForm extends Model
{

	/**
	 * @var mixed
	**/
    public $image;

	/**
	 * @var mixed
	**/
	public $file;

	/**
	 * @var int
	**/
    public $id;

	/**
	 * @var string, image path
	**/
    public $path;

	/**
	 * @var string, image url
	**/
    public $url;

    /**
     * @inheritdoc
    **/
    public function rules()
    {

        return [
            [
            	[
            		'image', 'file'
				], 'file',
				'skipOnEmpty' => true,
				'extensions' => 'png, jpg, doc, docx, xlsx, zip, rar',
				'maxFiles' 	  => 50
			]
        ];

    }

    /**
     * Get image from temp object
	 * @param string $type
	 * @return mixed
    **/
    public function upload( $type = 'image' )
    {

    	//uploading requested file
		$this->{$type} = UploadedFile::getInstance(
			$this,
			$type
		);

		//re-assign file name
		if( isset( $this->{$type}->name ) ) {

			$this->{$type}->name = trim(
				preg_replace(
					'/[^A-Za-z0-9.\-]/',
					'-',
					transliterator_transliterate("Any-Latin; NFD; [:Nonspacing Mark:] Remove; NFC; Lower();", $this->{$type}->name )
				), '-'
			);

		}

        return $this->save();

    }

    /**
     * Save requested file
     * @return bool
    **/
    public function save()
    {

		//param initialization
        $path = $this->getPath();

        //saving requested file
		if ( ! empty( $this->image ) && is_string( $path )
			&& FileHelper::createDirectory( dirname( $path ) )
			&& $this->image->saveAs( $path )
		) {
			return true;
		} else if ( ! empty( $this->file ) && is_string( $path )
			&& FileHelper::createDirectory( dirname( $path ) )
			&& $this->file->saveAs( $path )
		) {
			return true;
		}

        return false;

    }

    /**
	 * Get image path
     * @return bool|string image path
	**/
    public function getPath()
    {

		//param initialization
		$file = ! empty( $this->image )
			? $this->image
			: $this->file;

        return Yii::getAlias(
        	$this->path . '/' . preg_replace( '/[^a-zA-Z0-9.]/', '', $file->baseName ) . '.' . $file->extension
		);

    }

    /**
	 * Get image url
     * @return bool|string image url
    **/
    public function getUrl()
    {

    	//param initialization
		$file = ! empty( $this->image )
			? $this->image
			: $this->file;

        return Yii::getAlias(
        	$this->url . '/' . preg_replace( '/[^a-zA-Z0-9.]/', '', $file->baseName ) . '.' . $file->extension
		);

    }

	/**
	 * Zip requested files
	 * @param mixed $files
	 * @param string $zip_name
	 * @return string
	**/
    public function zipArchive( $files, $zip_name )
	{

		//params initializations
		$zip 		 = new ZipArchive();
		$upload_path = Yii::getAlias( '@frontend/web' ) . '/upload/documents';

		foreach ( $files as $file ) {

			//decoding file content
			$current = json_decode( $file );

			//rename file
			$current->name = trim(
				preg_replace(
					'/[^A-Za-z0-9.\-]/',
					'-',
					transliterator_transliterate("Any-Latin; NFD; [:Nonspacing Mark:] Remove; NFC; Lower();", $current->name )
				), '-'
			);

			//saving requested files
			if ( FileHelper::createDirectory( dirname( $upload_path . '/containers/' . $current->name ) ) ) {

				file_put_contents(
					$upload_path . '/containers/' . $current->name,
					base64_decode( str_replace(
						[ 'base64,' ], '', substr(
							$current->content, strpos(
								$current->content, ';base64,'
							) + 1
						)
					) )
				);

			}

		}

		//prepare zip archive for requested files
		$zip->open(
			$upload_path . '/' . $zip_name . '.zip',
			ZipArchive::CREATE | ZipArchive::OVERWRITE
		);

		foreach ( new RecursiveIteratorIterator(
			new RecursiveDirectoryIterator( $upload_path . '/containers' ),
			RecursiveIteratorIterator::LEAVES_ONLY
		) as $name => $file ) {

			//pushing files to zip archive
			if ( !$file->isDir() ) {

				// Get real and relative path for current file
				$filePath 	  = $file->getRealPath();
				$relativePath = substr(
					$filePath,
					strlen($upload_path . '/containers/' ) + 1
				);

				// Add current file to archive
				$zip->addFile( $filePath, $relativePath );

			}

		}

		$zip->close();

		//removing directory
		FileHelper::removeDirectory(
			Yii::getAlias( '@frontend/web' ) . '/upload/documents/containers'
		);

		return Yii::getAlias(
			'@frontend_web/upload/documents/' . $zip_name .'.zip'
		);

	}

}