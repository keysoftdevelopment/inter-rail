<?php namespace common\models;

/********************************************/
/*                                          */
/*      	   RATES SEARCH MODEL      	    */
/*                                          */
/********************************************/

use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

class RatesSearch extends Rates
{

	/**
	 * @inheritdoc
	**/
	public function rules()
	{

		return [ [
			[
				'id'
			], 'exist',
			'skipOnError' => true,
			'allowArray'  => true,
			'when' 		  => function ( $model, $attribute ) {
				return is_array( $model->$attribute );
			}
		], [
			[
				'from', 'to',
				'transportation'
			], 'string'
		] ];

	}

	/**
	 * @inheritdoc
	**/
	public function scenarios()
	{
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 * @return ActiveDataProvider
    **/
	public function search( $params )
	{

		//params initializations
		$query 	      = Rates::find();
		$dataProvider = new ActiveDataProvider( [
			'query' => $query,
		] );

		//loading requested params
		$this->load(
			$params
		);

		if( ! empty( $this->id ) ) {

			$ids = $this->id;

			unset( $this->id );
		}

		//queried params validation
		if ( ! $this->validate() ) {
			return $dataProvider;
		}

		//grid filtering conditions
		$query
			-> andFilterWhere( [
				'transportation' => $this->transportation,
				'term_from' 	 => $this->term_from,
				'term_to' 	 	 => $this->term_to
			] )
			-> andFilterWhere( [
				'like', 'from', $this->from
			] )
			-> andFilterWhere( [
				'like', 'to', $this->to
			] )
			-> andFilterWhere( [
				'id' => ! empty( $ids )
					? $ids
					: ( isset( $params[ 'containers_type' ] ) && ! empty( $params[ 'containers_type' ] )
						? ArrayHelper::getColumn( TaxonomyRelation::findAll( [
							'tax_id' => $params[ 'containers_type' ],
							'type'	 => 'rates'
						] ), 'foreign_id' )
						: []
					)
			] );

		return $dataProvider;

	}

}