<?php namespace common\models;

/********************************************/
/*                                          */
/*           SETTINGS FORM MODEL            */
/*                                          */
/********************************************/

use yii\base\Model;

class SettingsForm extends Model
{

	/**
	 * @inheritdoc
	**/
	public $address;

	/**
	 * @inheritdoc
	**/
	public $confirm_option;

	/**
	 * @inheritdoc
	**/
	public $contractor_role;

	/**
	 * @inheritdoc
	**/
	public $default_language;

	/**
	 * @inheritdoc
	**/
	public $e_mail;

	/**
	 * @inheritdoc
	**/
	public $facebook;

	/**
	 * @inheritdoc
	**/
	public $from_name;

	/**
	 * @inheritdoc
	**/
	public $from_mail;

	/**
	 * @inheritdoc
	**/
	public $ga_code;

	/**
	 * @inheritdoc
	**/
	public $gmap_api_key;

	/**
	 * @inheritdoc
	**/
	public $gplus;

	/**
	 * @inheritdoc
	**/
	public $invoice_option;

	/**
	 * @inheritdoc
	**/
	public $is_sandbox;

	/**
	 * @inheritdoc
	**/
	public $lotus_link;

	/**
	 * @inheritdoc
	**/
	public $lotus_sandbox_link;

	/**
	 * @inheritdoc
	**/
	public $order_options;

	/**
	 * @inheritdoc
	**/
	public $order_permissions;

	/**
	 * @inheritdoc
	**/
	public $packages;

	/**
	 * @inheritdoc
	**/
	public $phone;

	/**
	 * @inheritdoc
	**/
	public $posts_per_page;

	/**
	 * @inheritdoc
	**/
	public $sea_port;

	/**
	 * @inheritdoc
	**/
	public $statuses;

	/**
	 * @inheritdoc
	**/
	public $telegram;

	/**
	 * @inheritdoc
	**/
	public $terms;

	/**
	 * @inheritdoc
	**/
	public $youtube;

	/**
	 * @inheritdoc
	**/
	public $weechat;

	/**
	 * @inheritdoc
	**/
	public $whatsapp;

	/**
	 * @inheritdoc
	**/
	const SCENARIO_UPDATE = 'update';

    /**
     * @inheritdoc
    **/
    public function scenarios()
    {

        return [
            self::SCENARIO_UPDATE => [
				'address',
				'default_language',
				'confirm_option',
				'contractor_role',
				'telegram',
				'terms',
				'e_mail',
				'facebook',
				'from_name',
				'from_mail',
				'ga_code',
				'gmap_api_key',
				'gplus',
				'invoice_option',
				'is_sandbox',
				'lotus_link',
				'lotus_sandbox_link',
				'order_options',
				'order_permissions',
				'packages',
				'phone',
            	'posts_per_page',
				'sea_port',
				'statuses',
				'youtube',
				'whatsapp',
				'weechat'
			]
        ];

    }

	/**
	 * populate object from database
	 * @param string $type
	 **/
	public function fill( $type = 'settings' )
	{

		//fill in setting attributes
		$settings = Settings::findAll( [
			'name' => $type == 'logistics'
				? [
					'packages',
					'sea_port',
					'terms'
				]
				: [
					'address',
					'default_language',
					'confirm_option',
					'contractor_role',
					'e_mail',
					'facebook',
					'from_name',
					'from_mail',
					'ga_code',
					'gmap_api_key',
					'gplus',
					'invoice_option',
					'is_sandbox',
					'lotus_link',
					'lotus_sandbox_link',
					'order_options',
					'phone',
					'posts_per_page',
					'statuses',
					'telegram',
					'youtube',
					'weechat',
					'whatsapp'
				]
		] );

		//assigning setting details
		foreach ( $settings as $setting ) {

			if ( ! empty( $setting->description ) ) {

				//set appropriate options due to setting name
				if ( in_array( $setting->name, [ 'packages', 'terms', 'statuses', 'address' ] ) ) {

					$this->{$setting->name}[ $setting->lang_id ] = $setting->name == 'address'
						? $setting->description
						: json_decode(
							$setting->description
						);

				} else {

					$this->{$setting->name} = $setting->name == 'order_options'
						? json_decode( $setting->description )
						: $setting->description;

				}

			}

		}

	}

    /**
     * Save settings details
    **/
    public function save()
	{

		foreach ( $this->attributes() as $attribute ) {

			if ( ! empty( $this->{$attribute} ) ) {

				//delete requested setting
				Settings::deleteAll( [
					'name' => $attribute
				] );

				if ( in_array( $attribute, [
					'packages',
					'terms',
					'statuses',
					'address'
				] ) ) {

					foreach ( $this->{$attribute} as $key => $value ) {

						//setting main properties
						$setting = merge_objects( new Settings(), [
							'name'    	  => $attribute,
							'lang_id' 	  => $key,
							'description' => $attribute == 'address'
								? $value
								: json_encode( $value )
						] );

						//save logistics settings
						if ( $setting->save() )
							continue;

					}

				} else {

					//setting main properties
					$setting = merge_objects( new Settings(), [
						'name'    	  => $attribute,
						'description' => $attribute == 'order_options' || $attribute == 'order_permissions'
							? json_encode( $this->{$attribute} )
							: $this->{$attribute}
					] );

					//save general settings
					$setting->save();

				}

			}

		}

    }

}