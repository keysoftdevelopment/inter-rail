<?php namespace common\models;

/********************************************/
/*                                          */
/*         LOGISTICS RELATION MODEL         */
/*                                          */
/********************************************/

use yii\db\ActiveRecord;

/**
 * @property integer $id
 * @property integer $foreign_id
 * @property integer $logistic_id
 * @property string $type
 * @property Logistics $logistics
**/
class LogisticsRelation extends ActiveRecord
{

    /**
     * @inheritdoc
    **/
    public static function tableName()
    {
        return '{{%logistics_relation}}';
    }

    /**
     * @inheritdoc
    **/
    public function rules()
    {

        return [
            [
            	[
            		'foreign_id', 'logistic_id'
				], 'integer'
			], [
				[
					'type'
				], 'string'
			], [
            	[
            		'logistic_id'
				], 'exist',
				'skipOnError' 	  => true,
				'targetClass' 	  => Logistics::className(),
				'targetAttribute' => [
					'logistic_id' => 'id'
				]
			]
        ];

    }

    /**
     * @inheritdoc
    **/
    public function attributeLabels()
    {

        return [
            'id'          => __( 'ID' ),
            'foreign_id'  => __( 'Foreign ID' ),
            'logistic_id' => __( 'Logistic ID' ),
			'type'		  => __( 'Type' )
        ];

    }

    /**
     * @return \yii\db\ActiveQuery
    **/
    public function getContainer()
    {

        return $this->hasOne( Logistics::className(), [
            'id' => 'logistic_id'
        ] );

    }
    
}