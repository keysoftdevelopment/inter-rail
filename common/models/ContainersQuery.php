<?php namespace common\models;

/********************************************/
/*                                          */
/*          CONTAINERS QUERY MODEL          */
/*                                          */
/********************************************/

use yii\db\ActiveQuery;

class ContainersQuery extends ActiveQuery
{

    /**
     * @inheritdoc
     * @return Containers[]|array
    **/
    public function all( $db = null )
    {
        return parent::all( $db );
    }

    /**
     * @inheritdoc
     * @return Containers|array|null
    **/
    public function one( $db = null )
    {
        return parent::one( $db );
    }

}