<?php namespace common\models;

/********************************************/
/*                                          */
/*         	    LOGGING MODEL               */
/*                                          */
/********************************************/

use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;

/**
 * @property integer $id
 * @property integer $foreign_id
 * @property integer $changed_by
 * @property string $action
 * @property string $type
 * @property string $updated_at
 * @property string $created_at
 *
 * @property User $user
**/
class Logs extends ActiveRecord
{

	/**
	 * @inheritdoc
	**/
	public static function tableName()
	{
		return '{{%logs}}';
	}

	/**
	 * @inheritdoc
	**/
	public function behaviors()
	{

		return [

			'timestamp' => [
				'class' 	 => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => [
						'updated_at',
						'created_at'
					],
					ActiveRecord::EVENT_BEFORE_UPDATE => [
						'updated_at'
					]
				],
				'value' => function () {
					return date( 'd-m-Y H:i:s', strtotime( 'today' ) );
				}
			]

		];

	}

	/**
	 * @inheritdoc
	**/
	public function rules()
	{

		return [
			[
				[
					'foreign_id', 'changed_by'
				], 'required'
			], [
				[
					'foreign_id', 'changed_by'
				], 'integer'
			], [
				[
					'action'
				], 'string'
			], [
				[
					'type'
				], 'string',
				'max' => 100
			], [
				[
					'updated_at', 'created_at'
				], 'safe'
			], [
				[
					'changed_by'
				], 'exist',
				'skipOnError'     => true,
				'targetClass'     => User::className(),
				'targetAttribute' => [
					'changed_by' => 'id'
				]
			]
		];

	}

	/**
	 * @inheritdoc
	**/
	public function attributeLabels()
	{

		return [
			'id' 		 => __( 'ID' ),
			'foreign_id' => __( 'Foreign ID' ),
			'changed_by' => __( 'Changed By' ),
			'action' 	 => __( 'Action' ),
			'type'  	 => __( 'Type' ),
			'updated_at' => __( 'Updated At' ),
			'created_at' => __( 'Created At' )
		];

	}

	/**
	 * @return \yii\db\ActiveQuery
	**/
	public function getUser()
	{

		return $this->hasOne( User::className(), [
			'id' => 'changed_by'
		] );

	}

}