<?php namespace common\models;

/********************************************/
/*                                          */
/*        CONTAINERS RELATION MODEL         */
/*                                          */
/********************************************/

use yii\db\ActiveRecord;

/**
 * @property integer $id
 * @property integer $foreign_id
 * @property integer $container_id
 * @property string $type
 * @property Containers $containers
**/
class ContainersRelation extends ActiveRecord
{

    /**
     * @inheritdoc
    */
    public static function tableName()
    {
        return '{{%containers_relation}}';
    }

    /**
     * @inheritdoc
    */
    public function rules()
    {

        return [
            [
            	[
            		'foreign_id', 'container_id'
				], 'integer'
			], [
				[
					'type'
				], 'string'
			], [
            	[
            		'container_id'
				], 'exist',
				'skipOnError' 	  => true,
				'targetClass' 	  => Containers::className(),
				'targetAttribute' => [
					'container_id' => 'id'
				]
			]
        ];

    }

    /**
     * @inheritdoc
    */
    public function attributeLabels()
    {

        return [
            'id'           => __( 'ID' ),
            'foreign_id'   => __( 'Foreign ID' ),
            'container_id' => __( 'Container ID' ),
			'type'		   => __( 'Type' )
        ];

    }

    /**
     * @return \yii\db\ActiveQuery
    */
    public function getContainer()
    {

        return $this->hasOne( Containers::className(), [
            'id' => 'container_id'
        ] );

    }

}
