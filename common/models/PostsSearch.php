<?php namespace common\models;

/********************************************/
/*                                          */
/*    POSTS, PAGES AND ETC SEARCH MODEL     */
/*                                          */
/********************************************/

use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Expression;

class PostsSearch extends Posts
{

    /**
     * @inheritdoc
    **/
    public function rules()
    {

        return [
            [
            	[
            		'id', 'lang_id'
				], 'integer'
			], [
				[
					'title', 'type'
				], 'safe'
			]
        ];

    }

    /**
     * @inheritdoc
    **/
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @return ActiveDataProvider
    **/
    public function search( $params )
    {

		//params initializations
		$query 		  = Posts::find();
        $dataProvider = new ActiveDataProvider( [
            'query' => isset( $params[ 'type' ] )
				&& !empty( $params[ 'type' ] )
				? ( isset( $params['id'] ) && ! empty( $params['id'] )
					? $query
						-> where( [
							'type' => $params[ 'type' ]
						] )
						-> orderBy( [
							new Expression( 'FIELD ( id, ' . implode( ", ", $params['id']  ) . ' )' )
						] )
					: $query
						-> where( [
							'type' => $params[ 'type' ]
						] )
				)
				: $query,
        ] );

		//loading requested params
        $this->load(
        	$params
		);

		//queried params validation
        if ( !$this->validate() )
            return $dataProvider;

        //grid filtering conditions
        $query
			-> andFilterWhere( [
				'id'      => $this->id,
				'type'	  => $this->type,
				'lang_id' => $this->lang_id
			] )
			-> andFilterWhere( [
				'like', 'title', $this->title
			] );

		//order by date
		$query
			-> orderBy( [
				'created_at' => SORT_DESC
			] );

        return $dataProvider;

    }

}