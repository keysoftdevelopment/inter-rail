<?php namespace common\models;

/********************************************/
/*                                          */
/*          CONTAINERS SEARCH MODEL         */
/*                                          */
/********************************************/

use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

class ContainersSearch extends Containers
{

    /**
     * @inheritdoc
    */
    public function rules()
    {

        return [
            [
				[
					'number', 'owner',
					'transportation', 'status'
				], 'safe'
			]
        ];

    }

    /**
     * @inheritdoc
    **/
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @return ActiveDataProvider
    **/
    public function search( $params )
    {

    	//params initializations
		$query 		  = Containers::find();
        $dataProvider = new ActiveDataProvider( [
            'query' => $query,
        ] );

		//loading requested params
        $this->load(
        	$params
		);

		//queried params validation
        if ( !$this->validate() )
            return $dataProvider;

        //grid filtering conditions
        $query
            -> andFilterWhere( [
                'id'        => $this->id,
                'number'    => $this->number,
                'status'    => $this->status,
				'isDeleted' => false
            ] )
			-> andFilterWhere( [
				'id' => isset( $params[ 'type' ] )
					&& ! empty( $params[ 'type' ] )
					? ArrayHelper::getColumn( TaxonomyRelation::findAll( [
						'tax_id' => $params[ 'type' ],
						'type'	 => 'containers'
					] ), 'foreign_id' )
					: []
			] )
            -> andFilterWhere( [
            	'like', 'owner', $this->owner
			] )
            -> andFilterWhere( [
            	'like', 'transportation', $this->transportation
			] );

		//order by date
		$query->orderBy( [
			'created_at' => SORT_DESC
		] );

		return $dataProvider;

    }

}