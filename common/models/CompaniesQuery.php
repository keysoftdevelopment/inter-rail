<?php namespace common\models;

/********************************************/
/*                                          */
/*           COMPANIES QUERY MODEL     	    */
/*                                          */
/********************************************/

use yii\db\ActiveQuery;

class CompaniesQuery extends ActiveQuery
{

    /**
     * @inheritdoc
     * @return Companies[]|array
    **/
    public function all( $db = null )
    {
        return parent::all( $db );
    }

    /**
     * @inheritdoc
     * @return Companies|array|null
    **/
    public function one( $db = null )
    {
        return parent::one( $db );
    }

}