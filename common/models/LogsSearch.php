<?php namespace common\models;

/********************************************/
/*                                          */
/*            LOGS SEARCH MODEL             */
/*                                          */
/********************************************/

use yii\base\Model;
use yii\data\ActiveDataProvider;

class LogsSearch extends Logs
{

	/**
	 * @inheritdoc
	**/
	public function rules()
	{

		return [
			[
				[
					 'type'
				], 'string'
			], [
				[
					'changed_by'
				], 'integer'
			]
		];

	}

	/**
	 * @inheritdoc
	**/
	public function scenarios()
	{
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 * @return ActiveDataProvider
	**/
	public function search( $params )
	{

		//params initializations
		$query 		  = Logs::find();
		$dataProvider = new ActiveDataProvider( [
			'query' => isset( $params[ 'type' ] )
			&& ! empty( $params[ 'type' ] )
				? $query
					-> where( [
						'type' => $params[ 'type' ]
					] )
				: $query
		] );

		//filtering logs by requested dates
		if ( ! empty( $params[ 'date_from' ] )
			&& ! empty( $params[ 'date_to' ] )
		) {

			$query
				-> andWhere( [
					'>=', 'created_at', date( 'Y-m-d', strtotime( $params[ 'date_from' ] ) )
				] )
				-> andWhere( [
					'<=', 'created_at', date( 'Y-m-d', strtotime( $params[ 'date_to' ] . ' +1 day' ) )
				] );

		}

		//loading requested params
		$this->load(
			$params
		);

		//queried params validation
		if ( !$this->validate() ) {
			return $dataProvider;
		}

		//grid filtering conditions
		$query
			-> andFilterWhere( [
				'id'      	 => $this->id,
				'changed_by' => $this->changed_by,
				'type'		 => $this->type
			] );

		//order by date
		$query
			-> orderBy( [
				'created_at' => SORT_DESC
			] );

		return $dataProvider;

	}

}
