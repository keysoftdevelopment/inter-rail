<?php namespace common\models;

/*****************************************************************/
/*                                                               */
/*                                                               */
/*      ADDITIONAL FIELDS FOR POSTS, PAGES AND ETC... MODELS     */
/*                                                               */
/*                                                               */
/*****************************************************************/

use yii\db\ActiveRecord;

/**
 * @property integer $id
 * @property integer $foreign_id
 * @property string $name
 * @property string $description
 * @property string $type
**/
class CustomFields extends ActiveRecord
{

    /**
     * @inheritdoc
    **/
    public static function tableName()
    {
        return '{{%custom_fields}}';
    }

    /**
     * @inheritdoc
    **/
    public function rules()
    {

        return [
            [
            	[
            		'foreign_id'
				], 'integer'
			], [
				[
					'name'
				], 'required'
			], [
				[
					'description', 'type'
				], 'string'
			], [
				[
					'name'
				], 'string',
				'max' => 255
			]
        ];

    }

    /**
     * @inheritdoc
    **/
    public function attributeLabels()
    {

        return [
            'id'      	  => __( 'ID' ),
            'foreign_id'  => __( 'Foreign ID' ),
            'name'    	  => __( 'Name' ),
            'description' => __( 'Description' ),
			'type' 		  => __( 'Type' )
        ];

    }

}
