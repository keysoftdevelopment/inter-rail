<?php namespace common\models;

/********************************************/
/*                                          */
/*               MEDIA MODEL                */
/*                                          */
/********************************************/

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii2tech\ar\softdelete\SoftDeleteBehavior;

/**
 * @property integer $id
 * @property string $name
 * @property string $guide
 * @property string $type
 * @property string $size
 * @property bool $isDeleted
 * @property string $created_at
 *
 * @property Languages[] $languages
 * @property Posts[] $posts
 * @property Taxonomies[] $taxonomies
**/
class Files extends ActiveRecord
{

    /**
     * @inheritdoc
    **/
    public static function tableName()
    {
        return '{{%files}}';
    }

    /**
     * @inheritdoc
    **/
    public function behaviors()
    {

        return [

            'softDeleteBehavior' => [
                'class'                     => SoftDeleteBehavior::className(),
                'softDeleteAttributeValues' => [
                    'isDeleted' => true
                ],
                'replaceRegularDelete' => true
            ],

			'timestamp' => [
				'class' 		     => TimestampBehavior::className(),
				'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => false,
				'value'		         => function () {
					return date( 'Y-m-d H:i:s', strtotime( 'now' ) );
				}
			]

        ];

    }

    /**
     * @inheritdoc
    **/
    public function rules()
    {

        return [
            [
            	[
            		'name', 'guide'
				], 'required'
			], [
				[
					'created_at'
				], 'safe'
			], [
				[
					'name', 'guide', 'type', 'size'
				], 'string',
				'max' => 255
			]
        ];

    }

    /**
     * @inheritdoc
    **/
    public function attributeLabels()
    {

        return [
            'id'         => __( 'ID' ),
            'name'       => __( 'Name' ),
            'guide'      => __( 'Guide' ),
            'type'       => __( 'Type' ),
            'size'       => __( 'Size' ),
            'created_at' => __( 'Created At' )
        ];

    }

    /**
     * @return \yii\db\ActiveQuery
    **/
    public function getLanguages()
    {

        return $this->hasMany( Languages::className(), [
            'file_id' => 'id'
        ] );

    }

    /**
     * @return \yii\db\ActiveQuery
    **/
    public function getPosts()
    {

        return $this->hasMany( Posts::className(), [
            'file_id' => 'id'
        ] );

    }


    /**
     * @return \yii\db\ActiveQuery
    **/
    public function getTaxonomies()
    {

        return $this->hasMany( Taxonomies::className(), [
            'file_id' => 'id'
        ] );

    }

    /**
     * @inheritdoc
     * @return FilesQuery the active query used by this AR class.
    **/
    public static function find()
    {

    	//params initialization
        $query = new FilesQuery(
        	get_called_class()
		);

        return $query->where( [
            'isDeleted' => false
        ] );

    }

}
