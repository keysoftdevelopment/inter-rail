<?php namespace common\models;

/********************************************/
/*                                          */
/*           TRANSLATION`s MODEL            */
/*                                          */
/********************************************/

use yii\db\ActiveRecord;

/**
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property integer $lang_id
 * @property integer $foreign_id
 * @property string $seo
 * @property string $type
**/
class Translation extends ActiveRecord
{

	/**
	 * @inheritdoc
	**/
	public static function tableName()
	{
		return '{{%translation}}';
	}

	/**
	 * @inheritdoc
	**/
	public function rules()
	{

		return [
			[
				[
					'foreign_id', 'lang_id'
				], 'integer'
			], [
				[
					'title', 'description',
					'type', 'seo'
				], 'string'
			], [
				[
					'lang_id'
				], 'exist',
				'skipOnError' 	  => true,
				'targetClass' 	  => Languages::className(),
				'targetAttribute' => [
					'lang_id' => 'id'
				]
			]
		];

	}

	/**
	 * @inheritdoc
	**/
	public function attributeLabels()
	{

		return [
			'id'          => __( 'ID' ),
			'title'		  => __( 'Title' ),
			'description' => __( 'Description' ),
			'foreign_id'  => __( 'Foreign ID' ),
			'lang_id'     => __( 'Language ID' ),
			'seo'     	  => __( 'Seo' ),
			'type'		  => __( 'Type' )
		];

	}

	/**
	 * @return \yii\db\ActiveQuery
	**/
	public function getLang()
	{

		return $this->hasOne( Languages::className(), [
			'id' => 'lang_id'
		] );

	}

}