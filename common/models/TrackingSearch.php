<?php namespace common\models;

/********************************************/
/*                                          */
/*            TRACKING SEARCH MODEL         */
/*                                          */
/********************************************/

use yii\base\Model;
use yii\data\ActiveDataProvider;

class TrackingSearch extends Tracking
{

    /**
     * @inheritdoc
    **/
    public function rules()
    {

        return [
            [
            	[
            		'place_id', 'foreign_id'
				], 'integer'
			], [
				[
					'name', 'trans_from',
					'trans_to', 'type',
					'status'
				], 'safe'
			]
        ];

    }

    /**
     * @inheritdoc
    **/
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @return ActiveDataProvider
    **/
    public function search( $params )
    {

    	//params initializations
		$query 		  = Tracking::find();
        $dataProvider = new ActiveDataProvider( [
            'query' => $query,
        ] );

		//loading requested params
        $this->load(
        	$params
		);

		//queried params validation
        if ( !$this->validate() )
            return $dataProvider;

        //grid filtering conditions
        $query
            -> andFilterWhere( [
                'id'     	 => $this->id,
                'place_id' 	 => $this->place_id,
				'foreign_id' => $this->foreign_id,
				'type'		 => $this->type,
				'status'	 => $this->status
            ] )
            -> andFilterWhere( [
            	'like', 'name', $this->name
			] )
            -> andFilterWhere( [
            	'like', 'trans_from', $this->trans_from
			] )
			-> andFilterWhere( [
				'like', 'trans_to', $this->trans_to
			] );

        return $dataProvider;

    }

}