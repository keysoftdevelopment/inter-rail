<?php namespace common\models;

/********************************************/
/*                                          */
/*     		  TERMINALS MODEL         	    */
/*                                          */
/********************************************/

use yii\db\ActiveRecord;

use yii2tech\ar\softdelete\SoftDeleteBehavior;

use yii\behaviors\TimestampBehavior;

use common\behaviors\LoggingBehavior;

/**
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property integer $place_id
 * @property string $email
 * @property string $phone
 * @property float $storage
 * @property float $lading
 * @property float $unloading
 * @property float $exp_price
 * @property float $imp_price
 * @property float $surcharge
 * @property bool $isDeleted
 * @property string $updated_at
 * @property string $created_at
**/
class Terminals extends ActiveRecord
{

	/**
	 * @inheritdoc
	**/
	public $city;

	/**
	 * @inheritdoc
	**/
	public $country;

	/**
	 * @inheritdoc
	**/
	public $containers_type;

	/**
	 * @inheritdoc
	**/
	public $district;

	/**
	 * @inheritdoc
	**/
	public $location;

	/**
	 * @inheritdoc
	**/
	public $street;

    /**
     * @inheritdoc
    **/
    public static function tableName()
    {
        return '{{%terminals}}';
    }

	/**
	 * @inheritdoc
	**/
	public function behaviors()
	{

		return [

			'softDeleteBehavior' => [
				'class'                     => SoftDeleteBehavior::className(),
				'softDeleteAttributeValues' => [
					'isDeleted' => true
				],
				'replaceRegularDelete' => true
			],

			'timestamp' => [
				'class' 	 => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => [
						'updated_at',
						'created_at'
					],
					ActiveRecord::EVENT_BEFORE_UPDATE => [
						'updated_at'
					]
				],
				'value' => function () {
					return date( 'Y-m-d H:i:s', strtotime( 'now' ) );
				}
			], [
				'class'  	 	=> LoggingBehavior::className(),
				'type'	  	 	=> 'terminals',
				'foreign_id'    => 'id',
				'trace_options' => [
					'name', 'description', 'email', 'phone', 'country',
					'city', 'district', 'street', 'storage',
					'lading', 'unloading', 'exp_price',
					'imp_price', 'surcharge'
				]
			]

		];

	}

    /**
     * @inheritdoc
	**/
    public function rules()
    {

        return [
            [
            	[
            		'country', 'city', 'district',
					'location', 'name', 'storage'
				], 'required'
			], [
				[
					'description', 'email', 'phone', 'country',
					'city', 'district', 'street', 'location'
				], 'string'
			], [
				[
					'place_id', 'containers_type'
				], 'integer'
			], [
				[
					'storage', 'lading', 'unloading', 'exp_price',
					'imp_price', 'surcharge', 'updated_at', 'created_at',
				], 'safe'
			], [
				[
					'name'
				], 'string',
				'max' => 255
			], [
				[
					'place_id'
				], 'exist',
				'skipOnError'     => true,
				'targetClass'     => Places::className(),
				'targetAttribute' => [
					'place_id' => 'id'
				]
			]
        ];

    }

    /**
     * @inheritdoc
    **/
    public function attributeLabels()
    {

        return [
            'id'          => __( 'ID' ),
            'name'        => __( 'Name' ),
            'description' => __( 'Description' ),
            'place_id'    => __( 'Place ID' ),
			'email'  	  => __( 'Email' ),
            'phone'  	  => __( 'Phone' ),
			'storage'  	  => __( 'Storage' ),
			'lading'  	  => __( 'Crane Price For Lading' ),
			'unloading'	  => __( 'Crane Price For Unloading' ),
			'exp_price'   => __( 'Export Price' ),
			'imp_price'   => __( 'Import Price' ),
			'surcharge'   => __( 'Surcharge' ),
			'updated_at'  => __( 'Updated At' ),
            'created_at'  => __( 'Created At' )
        ];

    }

	/**
	 * Filling terminal with places data
	**/
	public function getLocation()
	{

		//param initialization
		$location = Places::findOne(
			$this->place_id
		);

		//set terminal location details
		if ( ! is_null( $location ) ) {

			foreach ( [ 'country', 'city', 'district', 'street' ] as $param )
				$this->{$param} = $location->{$param};

			$this->location = json_encode( [
				'lat' => $location->lat,
				'lng' => $location->lng
			] );

		}

	}

	/**
	 * Filling requested terminal with container type id
	**/
	public function containerType()
	{

		//param initialization
		$container_type = TaxonomyRelation::findOne( [
			'foreign_id' => $this->id,
			'type'       => 'terminal'
		] );

		//set container type
		if ( ! is_null( $container_type ) )
			$this->containers_type = $container_type->tax_id;

	}

	/**
	 * This method is called at the beginning of inserting or updating a record.
	 * The default implementation will trigger an [[EVENT_BEFORE_INSERT]] event when `$insert` is `true`,
	 * or an [[EVENT_BEFORE_UPDATE]] event if `$insert` is `false`.
	 * @param bool $insert whether this method called while inserting a record.
	 * @return bool whether the insertion or updating should continue.
	 * If `false`, the insertion or updating will be cancelled.
	**/
	public function beforeSave( $insert )
	{

		if ( parent::beforeSave( $insert )
			&& $this->validate()
		) {

			//params initializations
			$locations = json_decode( $this->location );
			$place 	   = Places::findOne(
				$this->place_id
			);

			//declare place
			$place = is_null( $place )
				? new Places()
				: $place;

			//set place properties
			foreach ( [ 'country', 'city', 'district', 'street' ] as $param )
				$place->{$param} = $this->{$param};

			//set location details
			if ( ! is_null( $locations ) ) {
				$place->lat = $locations->lat;
				$place->lng = $locations->lng;
			}

			//set place type
			$place->type = 'terminal';

			//set place id to model
			if ( $place->save() )
				$this->place_id = $place->id;

			return true;

		}

		return false;

	}

	/**
	 * This method is called at the end of inserting or updating a record.
	 * The default implementation will trigger an [[EVENT_AFTER_INSERT]] event when `$insert` is `true`,
	 * or an [[EVENT_AFTER_UPDATE]] event if `$insert` is `false`. The event class used is [[AfterSaveEvent]].
	 * @param bool $insert
	 * @param array $changedAttributes The old values of attributes that had changed and were saved.
	**/
	public function afterSave( $insert, $changedAttributes )
	{

		parent::afterSave(
			$insert,
			$changedAttributes
		);

		if ( ! empty( $this->containers_type ) ) {

			//delete all container type relations for current terminal
			TaxonomyRelation::deleteAll( [
				'foreign_id' => $this->id,
				'type'       => 'terminal'
			] );

			//taxonomy main properties
			$taxonomy = merge_objects( new TaxonomyRelation(), [
				'foreign_id' => $this->id,
				'tax_id'	 => $this->containers_type,
				'type'		 => 'terminal'
			] );

			//save taxonomy relation
			$taxonomy->save();

		}

	}

    /**
     * @inheritdoc
     * @return TerminalsQuery the active query used by this AR class.
    **/
    public static function find()
    {

		//param initialization
        $query = new TerminalsQuery(
        	get_called_class()
		);

        return $query->where( [
            'isDeleted' => false
        ] );

    }

}