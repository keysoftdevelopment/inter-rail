<?php namespace common\models;

/********************************************/
/*                                          */
/*     		  INVOICES MODEL         	    */
/*                                          */
/********************************************/

use Yii;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * @property integer $id
 * @property integer $customer_id
 * @property integer $foreign_id
 * @property string $number
 * @property string $date
 * @property string $status
 * @property float $tax
 * @property float $amount
 * @property float $balance_due
 * @property string $type
 * @property integer $creator,
 * @property string $updated_at
 * @property string $created_at
 *
 * @property User $users
 * @property Expenses $expenses
**/
class Invoices extends ActiveRecord
{

    /**
     * @inheritdoc
	**/
    public static function tableName()
    {
        return '{{%invoices}}';
    }

	/**
	 * @inheritdoc
	**/
	public function behaviors()
	{

		return [

			'timestamp'          => [
				'class' 	 => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => [
						'updated_at',
						'created_at'
					],
					ActiveRecord::EVENT_BEFORE_UPDATE => [
						'updated_at'
					]
				],
				'value' => function () {
					return date( 'Y-m-d H:i:s', strtotime( 'now' ) );
				}
			]

		];

	}

    /**
     * @inheritdoc
	**/
    public function rules()
    {

        return [
            [
            	[
            		'number', 'customer_id', 'creator',
					'foreign_id', 'type'
				], 'required'
			], [
				[
					'status', 'type'
				], 'string'
			], [
				[
					'customer_id', 'creator', 'foreign_id'
				], 'integer'
			], [
				[
					'date', 'amount',
					'updated_at', 'created_at'
				], 'safe'
			], [
				[
					'number'
				], 'string',
				'max' => 255
			], [
				[
					'customer_id', 'creator'
				], 'exist',
				'skipOnError'     => true,
				'targetClass'     => User::className(),
				'targetAttribute' => [
					'customer_id' => 'id',
					'creator' 	  => 'id'
				]
			]
        ];

    }


    /**
     * @inheritdoc
    **/
    public function attributeLabels()
    {

        return [
            'id'          => __( 'ID' ),
			'customer_id' => __( 'Customer ID' ),
			'foreign_id'  => __( 'Foreign ID' ),
            'number'      => __( 'Number' ),
            'date'     	  => __( 'Date' ),
			'status'  	  => __( 'Status' ),
			'amount'  	  => __( 'Amount' ),
			'type'		  => __( 'Type' ),
			'creator'	  => __( 'Invoice Creator' ),
			'updated_at'  => __( 'Updated At' ),
            'created_at'  => __( 'Created At' )
        ];

    }


	/**
	 * This method is called at the beginning of inserting or updating a record.
	 * The default implementation will trigger an [[EVENT_BEFORE_INSERT]] event when `$insert` is `true`,
	 * or an [[EVENT_BEFORE_UPDATE]] event if `$insert` is `false`.
	 * When overriding this method, make sure you call the parent implementation like the following:
	 * @param bool $insert whether this method called while inserting a record.
	 * If `false`, it means the method is called while updating a record.
	 * @return bool whether the insertion or updating should continue.
	 * If `false`, the insertion or updating will be cancelled.
	**/
	public function beforeSave( $insert )
	{

		//params initializations
		$this->creator  = Yii::$app->user->id;
		$transportation = [];

		if ( $this->status == 'pending'
			&& $this->type = 'Lotus'
		) {

			//container and wagons details
			foreach ( [
				'containers' => [
					'main'	   => new Containers(),
					'relation' => new ContainersRelation()
				],
				'wagons' => [
					'main'	   => new Wagons(),
					'relation' => new WagonsRelation()
				]
			] as $key => $param ) {

				$transportation[ $key ] = $param[ 'main' ]::find()
					-> where( [
						'id' => $param[ 'relation' ]::find()
							-> select( str_replace( 's', '_id', $key ) )
							-> where( [
								'foreign_id' => $this->foreign_id,
								'type'		 => 'order'
							] )
							-> column(),
						'isDeleted' => false
					] )
					-> all();

			}

			//users lists due to
			$users = User::find()
				-> where( [
					'id' => [
						$this->customer_id,
						$this->creator
					]
				] )
				-> indexBy( 'id' )
				-> all();

			//the list of companies
			$companies = Companies::find()
				-> where( [
					'id' => ArrayHelper::getColumn(
						$users, 'company_id'
					)
				] )
				-> indexBy( 'id' )
				-> all();

			//send request to lotus and wait response from that
			$response = \Yii::$app->lotus->invoice( [
				'invoice_number' => $this->number,
				'amount'		 => $this->amount,
				'users'			 => $users,
				'customer_id' 	 => $this->customer_id,
				'creator'	  	 => $this->creator,
				'order'		  	 => Orders::findOne( $this->foreign_id ),
				'companies'	     => $companies,
				'invoice_date'	 => date( 'Ymd', strtotime( $this->date ) ),
				'transportation' => $transportation,
				'places'		 => ! empty( $companies )
					? Places::find()
						-> where( [
							'id' => ArrayHelper::getColumn(
								$companies, 'place_id'
							)
						] )
						-> indexBy( 'id' )
						-> asArray()
						-> all()
					: []
			] );

			return true;

		}

		//params validations
		if ( parent::beforeSave( $insert )
			&& $this->validate()
		)
			return true;

		return false;

	}

}