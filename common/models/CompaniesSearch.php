<?php namespace common\models;

/********************************************/
/*                                          */
/*           COMPANIES SEARCH MODEL         */
/*                                          */
/********************************************/

use yii\base\Model;
use yii\data\ActiveDataProvider;

class CompaniesSearch extends Companies
{

    /**
     * @inheritdoc
    **/
    public function rules()
    {

        return [
            [
            	[
            		'id', 'place_id'
				], 'integer'
			], [
				[
					'name'
				], 'safe'
			]
        ];

    }

    /**
     * @inheritdoc
    **/
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @return ActiveDataProvider
    **/
    public function search( $params )
    {

    	//params initializations
		$query 		  = Companies::find();
        $dataProvider = new ActiveDataProvider( [
            'query' => $query,
        ] );

		//loading requested params
        $this->load(
        	$params
		);

		//queried params validation
        if ( !$this->validate() )
            return $dataProvider;

        //grid filtering conditions
        $query
			-> andFilterWhere( [
				'id'       => $this->id,
				'place_id' => $this->place_id
			] )
			-> andFilterWhere( [
				'like', 'name', $this->name
			] );

		//order by date
		$query->orderBy( [
			'created_at' => SORT_DESC
		] );

        return $dataProvider;

    }

}