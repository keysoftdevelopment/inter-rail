<?php namespace common\models;

/********************************************/
/*                                          */
/*              ROUTES MODEL                */
/*                                          */
/********************************************/

use yii\db\ActiveRecord;

use common\behaviors\LoggingBehavior;

/**
 * @property integer $id
 * @property integer $from
 * @property integer $to
 * @property integer $rate
 * @property integer $duration
 *
 * @property Places $places
**/
class Routes extends ActiveRecord
{

	/**
	 * @inheritdoc
	**/
	public $logistics;

    /**
     * @inheritdoc
	**/
    public static function tableName()
    {
        return '{{%routes}}';
    }

	/**
	 * @inheritdoc
	**/
	public function behaviors()
	{

		return [

			[
				'class'  	 	=> LoggingBehavior::className(),
				'type'	  	 	=> 'routes',
				'foreign_id'    => 'id',
				'trace_options' => [
					'from',
					'to',
					'rate',
					'duration'
				]
			]

		];

	}

    /**
     * @inheritdoc
    **/
    public function rules()
    {

        return [
			[
				[
					'from', 'to', 'rate'
				], 'required'
			], [
            	[
            		'from', 'to', 'rate', 'duration'
				], 'integer'
			], [
				[
					'logistics'
				], 'string'
			], [
				[
					'from', 'to'
				], 'exist',
				'skipOnError'     => true,
				'targetClass'     => Places::className(),
				'targetAttribute' => [
					'from' => 'id',
					'to'   => 'id'
				]
			]

        ];

    }

    /**
     * @inheritdoc
    **/
    public function attributeLabels()
    {

        return [
            'id'       => __( 'ID' ),
			'from'     => __( 'Transportation From' ),
			'to'       => __( 'Transportation To' ),
			'rate'     => __( 'Rate' ),
			'duration' => __( 'Transit Time' )
        ];

    }

	/**
	 * Filling current model with logistics data
	**/
	public function getLogistics()
	{

		$this->logistics = LogisticsRelation::find()
			-> select( 'logistic_id' )
			-> where( [
				'foreign_id' => $this->id,
				'type' 	     => 'route'
			] )
			-> asArray()
			-> all();

	}

	/**
	 * This method is called at the beginning of inserting or updating a record.
	 * The default implementation will trigger an [[EVENT_BEFORE_INSERT]] event when `$insert` is `true`,
	 * or an [[EVENT_BEFORE_UPDATE]] event if `$insert` is `false`.
	 * When overriding this method, make sure you call the parent implementation like the following:
	 * @param bool $insert whether this method called while inserting a record.
	 * If `false`, it means the method is called while updating a record.
	 * @return bool whether the insertion or updating should continue.
	 * If `false`, the insertion or updating will be cancelled.
	**/
	public function beforeSave( $insert )
	{

		if ( $this->validate() ) {

			if ( ! empty( $this->duration ) )
				$this->duration = $this->duration * 24 * 3600;

			return true;

		}

		return false;

	}

	/**
	 * This method is called at the end of inserting or updating a record.
	 * The default implementation will trigger an [[EVENT_AFTER_INSERT]] event when `$insert` is `true`,
	 * or an [[EVENT_AFTER_UPDATE]] event if `$insert` is `false`. The event class used is [[AfterSaveEvent]].
	 * @param bool $insert
	 * @param array $changedAttributes The old values of attributes that had changed and were saved.
	**/
	public function afterSave( $insert, $changedAttributes )
	{

		if ( ! empty( $this->logistics ) ) {

			//remove all direction relation with palces
			LogisticsRelation::deleteAll( [
				'foreign_id' => $this->id,
				'type'		 => 'route'
			] );

			foreach ( $this->logistics as $logistic_id ) {

				//logistic relation main properties
				$logistic = merge_objects( new LogisticsRelation(), [
					'foreign_id'  => $this->id,
					'logistic_id' => $logistic_id,
					'type'		  => 'route'
				] );

				//logistic relations update
				if ( $logistic->save() )
					continue;

			}

		}

	}

}
