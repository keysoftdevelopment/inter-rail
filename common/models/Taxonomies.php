<?php namespace common\models;

/********************************************/
/*                                          */
/*            CATEGORIES MODEL              */
/*                                          */
/********************************************/

use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

use yii\behaviors\SluggableBehavior;
use yii2tech\ar\softdelete\SoftDeleteBehavior;
use yii\behaviors\TimestampBehavior;

use common\behaviors\LoggingBehavior;

/**
 * @property integer $id
 * @property string $icon
 * @property string $name
 * @property string $description
 * @property integer $file_id
 * @property integer $lang_id
 * @property integer $parent
 * @property string $type
 * @property string $containers
 * @property string $wagons
 * @property string $weights
 * @property string $slug
 * @property bool $isDeleted
 * @property string $updated_at
 * @property string $created_at
 *
 * @property Taxonomies[] $children
 * @property TaxonomyRelation[] $taxonomies
 * @property Files $file
 * @property Languages $lang
**/
class Taxonomies extends ActiveRecord
{

	/**
	 * @inheritdoc
	**/
	public $children;

	/*
 	 * Stores old attributes on afterFind() so we can compare
 	 * against them before/after save
	*/
	protected $old_attributes;

    /**
     * @inheritdoc
    **/
    public static function tableName()
    {
        return '{{%taxonomies}}';
    }

	/**
	 * @inheritdoc
	**/
	public function behaviors()
	{

		return [

			'softDeleteBehavior' => [
				'class'                     => SoftDeleteBehavior::className(),
				'softDeleteAttributeValues' => [
					'isDeleted' => true
				],
				'replaceRegularDelete' => true
			],

			'timestamp' => [
				'class' 	 => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => [
						'updated_at',
						'created_at'
					],
					ActiveRecord::EVENT_BEFORE_UPDATE => [
						'updated_at'
					]
				],
				'value' => function () {
					return date( 'Y-m-d H:i:s', strtotime( 'now' ) );
				}
			], [
				'class'        => SluggableBehavior::className(),
				'attribute'    => 'name',
				'ensureUnique' => true
			], [
				'class'  	 	=> LoggingBehavior::className(),
				'type'	  	 	=> 'taxonomies',
				'foreign_id'    => 'id',
				'trace_options' => [
					'name',
					'description',
					'containers',
					'wagons'
				]
			]
		];

	}

    /**
     * @inheritdoc
    **/
    public function rules()
    {

        return [
            [
            	[
            		'name'
				], 'required'
			], [
            	[
            		'description', 'type',
					'icon', 'containers', 'wagons',
					'weights'
				], 'string'
			], [
            	[
            		'file_id', 'lang_id', 'parent'
				], 'integer'
			], [
            	[
					'updated_at', 'created_at'
				], 'safe'
			], [
            	[
            		'name'
				], 'string',
				'max' => 255
			], [
            	[
            		'file_id'
				], 'exist',
				'skipOnError'     => true,
				'targetClass'     => Files::className(),
				'targetAttribute' => [
					'file_id' => 'id'
				]
			], [
            	[
            		'lang_id'
				], 'exist',
				'skipOnError' 	  => true,
				'targetClass'	  => Languages::className(),
				'targetAttribute' => [
					'lang_id' => 'id'
				]
			]
        ];

    }

    /**
     * @inheritdoc
    **/
    public function attributeLabels()
    {

        return [
            'id'          => __( 'ID' ),
			'icon'		  => __( 'Icon' ),
            'name'        => __( 'Name' ),
            'description' => __( 'Description' ),
            'file_id'     => __( 'File ID' ),
            'lang_id'     => __( 'Lang ID' ),
            'parent'      => __( 'Parent' ),
			'type'		  => __( 'Type' ),
			'updated_at'  => __( 'Updated At' ),
            'created_at'  => __( 'Created At' )
        ];

    }

	/**
	 * This method is called when the AR object is created and populated with the query result.
	 * The default implementation will trigger an [[EVENT_AFTER_FIND]] event.
	 * When overriding this method, make sure you call the parent implementation to ensure the
	 * event is triggered.
	**/
	public function afterFind()
	{

		//set attributes before updating them
		$this->old_attributes = $this->attributes;

		return parent::afterFind();

	}

	/**
	 * This method is called at the beginning of inserting or updating a record.
	 * The default implementation will trigger an [[EVENT_BEFORE_INSERT]] event when `$insert` is `true`,
	 * or an [[EVENT_BEFORE_UPDATE]] event if `$insert` is `false`.
	 * When overriding this method, make sure you call the parent implementation like the following:
	 * @param bool $insert whether this method called while inserting a record.
	 * If `false`, it means the method is called while updating a record.
	 * @return bool whether the insertion or updating should continue.
	 * If `false`, the insertion or updating will be cancelled.
	**/
	public function beforeSave( $insert )
	{

		if ( $this->validate() ) {

			if ( ! is_null( $this->old_attributes )
				&& $this->old_attributes[ 'lang_id' ] != (int)$this->lang_id
			) {

				//param initialization
				$translation = Translation::findOne( [
					'foreign_id' => $this->id,
					'lang_id'	 => $this->lang_id,
					'type'		 => 'taxonomy'
				] );

				//set translation details
				$translation = is_null( $translation )
					? merge_objects( new Translation(), [
						'title' 	  => $this->name,
						'description' => $this->description,
						'lang_id'     => $this->lang_id,
						'foreign_id'  => $this->id,
						'type' 		  => 'taxonomy'
					] )
					: merge_objects( $translation, [
						'title' 	  => $this->name,
						'description' => $this->description
					] );

				//save taxonomy translation details
				if ( $translation->save() )
					return false;

			}

			return true;

		}

		return false;

	}

    /**
     * @return \yii\db\ActiveQuery
    **/
    public function getPostTaxonomies()
    {

        return $this->hasMany( TaxonomyRelation::className(), [
            'tax_id' => 'id'
        ] );

    }

    /**
     * @return \yii\db\ActiveQuery
    **/
    public function getLang()
    {

        return $this->hasOne( Languages::className(), [
            'id' => 'lang_id'
        ] );

    }

	/**
	 * Returning all parents taxonomies
	 * i.e this method called in taxonomy create page in admin panel
	 * @return array
	**/
	public static function parentTaxonomies()
	{

		return ArrayHelper::map( Taxonomies::find()
			-> where( [
				'isDeleted' => false,
				'type'		=> 'posts'
			] )
			-> andWhere( [
				'is', 'parent', null
			] )
			-> all(),
			'id',
			'name'
		);

	}

    /**
     * @inheritdoc
     * @return TaxonomiesQuery the active query used by this AR class.
    **/
    public static function find()
    {

        $query = new TaxonomiesQuery(
        	get_called_class()
		);

        return $query->where( [
            'isDeleted' => false
        ] );

    }

}
