<?php namespace common\models;

/********************************************/
/*                                          */
/*                PLACES MODEL              */
/*                                          */
/********************************************/

use yii\db\ActiveRecord;

use common\behaviors\LoggingBehavior;

/**
 * This is the model class for table "places".
 *
 * @property integer $id
 * @property string $name
 * @property integer $lat
 * @property integer $lng
 * @property integer $foreign_id
 * @property string $country_code
 * @property string $country
 * @property string $city
 * @property string $district
 * @property string $street
 * @property string $postal_code
 * @property string $type
**/
class Places extends ActiveRecord
{

    /**
     * @inheritdoc
    **/
    public static function tableName()
    {
        return '{{%places}}';
    }

	/**
	 * @inheritdoc
	**/
	public function behaviors()
	{

		return [

			[
				'class'  	 	=> LoggingBehavior::className(),
				'type'	  	 	=> 'places',
				'foreign_id'    => 'id',
				'trace_options' => [
					'name', 'lat', 'lng',
					'country_code', 'country', 'city',
					'district', 'street', 'postal_code'
				]
			]

		];

	}

    /**
     * @inheritdoc
    **/
    public function rules()
    {

        return [
			[
				[
					'foreign_id'
				], 'integer',
				'skipOnEmpty' => true
			], [
				'foreign_id', 'default', 'value' => null
			], [
            	[
            		'lat', 'lng'
				], 'number'
			], [
				[
					'name', 'country_code', 'country', 'city', 'district', 'street', 'postal_code', 'type'
				], 'string',
				'max' => 255
			]
        ];

    }

    /**
     * @inheritdoc
    **/
    public function attributeLabels()
    {

        return [
            'id'           => __('ID' ),
			'name'		   => __( 'Name' ),
            'lat'          => __( 'Latitude' ),
            'lng'          => __( 'Longitude' ),
			'foreign_id'   => __( 'Foreign ID' ),
			'country_code' => __( 'Country Code' ),
            'country'      => __( 'Country' ),
            'city'         => __( 'City' ),
            'district'     => __( 'District' ),
            'street'       => __( 'Street' ),
			'postal_code'  => __( 'Postal Code' ),
			'type'		   => __( 'Type' )
        ];

    }

}
