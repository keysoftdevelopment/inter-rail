<?php namespace common\models;

/********************************************/
/*                                          */
/*            WAGONS SEARCH MODEL           */
/*                                          */
/********************************************/

use yii\base\Model;
use yii\data\ActiveDataProvider;

class WagonsSearch extends Wagons
{

	/**
	 * @inheritdoc
	**/
	public function rules()
	{

		return [
			[
				[
					'number', 'owner'
				], 'safe'
			]
		];

	}

	/**
	 * @inheritdoc
	**/
	public function scenarios()
	{
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 * @return ActiveDataProvider
	**/
	public function search( $params )
	{

		//params initializations
		$query 		  = Wagons::find();
		$dataProvider = new ActiveDataProvider( [
			'query' => $query,
		] );

		//loading requested params
		$this->load(
			$params
		);

		//queried params validation
		if ( ! $this->validate() )
			return $dataProvider;

		//grid filtering conditions
		$query
			-> andFilterWhere( [
				'id'        => $this->id,
				'number'    => $this->number,
				'isDeleted' => false
			] )
			-> andFilterWhere( [
				'like', 'owner', $this->owner
			] );

		//order by date
		$query
			-> orderBy( [
				'created_at' => SORT_DESC
			] );

		return $dataProvider;

	}

}