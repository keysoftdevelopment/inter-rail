<?php namespace common\models;

/********************************************/
/*                                          */
/*            WAGONS QUERY MODEL            */
/*                                          */
/********************************************/

use yii\db\ActiveQuery;

class WagonsQuery extends ActiveQuery
{

    /**
     * @inheritdoc
     * @return Wagons[]|array
    **/
    public function all( $db = null )
    {
        return parent::all( $db );
    }

    /**
     * @inheritdoc
     * @return Wagons|array|null
    **/
    public function one( $db = null )
    {
        return parent::one( $db );
    }

}