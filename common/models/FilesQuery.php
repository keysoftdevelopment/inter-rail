<?php namespace common\models;

/********************************************/
/*                                          */
/*            MEDIA QUERY MODEL             */
/*                                          */
/********************************************/

use yii\db\ActiveQuery;

class FilesQuery extends ActiveQuery
{

    /**
     * @inheritdoc
     * @return Files[]|array
    **/
    public function all( $db = null )
    {
        return parent::all( $db );
    }

    /**
     * @inheritdoc
     * @return Files|array|null
    **/
    public function one( $db = null )
    {
        return parent::one( $db );
    }

}