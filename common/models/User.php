<?php namespace common\models;

/********************************************/
/*                                          */
/*               USER MODEL                 */
/*                                          */
/********************************************/

use Yii;

use yii\base\NotSupportedException;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii2tech\ar\softdelete\SoftDeleteBehavior;

use common\commands\SendEmailCommand;

/**
 * @property integer $id
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property integer $file_id
 * @property string $email
 * @property integer $company_id
 * @property string $first_name
 * @property string $last_name
 * @property string $phone
 * @property integer $status
 * @property bool $isDeleted
 * @property integer $updated_at
 * @property integer $created_at
 * @property string $password write-only password
**/
class User extends ActiveRecord implements IdentityInterface
{

	/**
	 * @inheritdoc
	**/
	public $bank;

	/**
	 * @inheritdoc
	**/
	public $company_name;

	/**
	 * @inheritdoc
	**/
	public $city;

	/**
	 * @inheritdoc
	**/
	public $country;

	/**
	 * @inheritdoc
	**/
	public $mfo;

	/**
	 * @inheritdoc
	**/
	public $oked;

	/**
	 * @inheritdoc
	**/
	private $reset_auth = true;

	/**
	 * @inheritdoc
	**/
	public $street;

	/**
	 * @inheritdoc
	**/
	public $role;

	/**
	 * @inheritdoc
	**/
    const STATUS_DELETED = 0;

	/**
	 * @inheritdoc
	**/
    const STATUS_ACTIVE = 10;

	/**
	 * @inheritdoc
	**/
    const SCENARIO_REGISTER = 'register';

	/**
	 * @inheritdoc
	**/
    const SCENARIO_UPDATE = 'update';

    /**
     * @inheritdoc
    **/
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
    **/
    public function behaviors()
    {

        return [

            TimestampBehavior::className(),

			'softDeleteBehavior' => [
				'class'                     => SoftDeleteBehavior::className(),
				'softDeleteAttributeValues' => [
					'isDeleted' => true
				],
				'replaceRegularDelete' => true
			]
        ];

    }

    /**
     * @inheritdoc
    **/
    public function rules()
    {

        return [
            [
            	'status', 'default',
				'value' => self::STATUS_ACTIVE
			], [
				'status',
				'in',
				'range' => [
					self::STATUS_ACTIVE,
					self::STATUS_DELETED
				]
			], [
				[
					'file_id', 'company_id'
				], 'integer'
			], [
				'email', 'email'
			], [
				[
					'email', 'username'
				], 'required', 'on' => [
					self::SCENARIO_REGISTER,
					self::SCENARIO_UPDATE
				]
			], [
				[
					'email'
				], 'unique', 'on' => [
					self::SCENARIO_REGISTER,
					self::SCENARIO_UPDATE
				]
			], [
				[
					'bank', 'company_name', 'city', 'country',
					'first_name', 'last_name', 'mfo', 'oked',
					'phone', 'street'
				], 'string'
			]
        ];

    }

	/**
	 * @inheritdoc
	**/
	public function attributeLabels()
	{

		return [
			'id'         		   => __( 'ID' ),
			'username'     	 	   => __( 'Username' ),
			'auth_key'             => __( 'Authorization Key' ),
			'password_hash'        => __( 'Password' ),
			'password_reset_token' => __( 'Password Reset Token' ),
			'file_id'     	       => __( 'Avatar' ),
			'email'      		   => __( 'Email' ),
			'company_id'     	   => __( 'Company' ),
			'first_name'      	   => __( 'First Name' ),
			'last_name'      	   => __( 'Last Name' ),
			'phone'      		   => __( 'Phone Number' ),
			'status'      		   => __( 'Status' ),
			'updated_at' 		   => __( 'Updated At' ),
			'created_at' 		   => __( 'Created At' )
		];

	}

    /**
     * @inheritdoc
    **/
    public function scenarios()
    {

        return [

            self::SCENARIO_REGISTER => [
                'username',
				'file_id',
                'email',
                'password',
                'role',
                'phone',
				'company_id',
                'first_name',
                'last_name',
				'company',
				'product',
				'is_main_provider',
				'is_test_user'
            ],

            self::SCENARIO_UPDATE   => [
                'username',
				'file_id',
                'email',
                'password',
                'role',
                'phone',
				'company_id',
                'first_name',
                'last_name',
				'company',
				'product',
				'is_main_provider',
				'is_test_user'
            ]

        ];

    }

    /**
     * @inheritdoc
    **/
    public static function findIdentity( $id )
    {

        return static::findOne( [
            'id'     => $id,
            'status' => self::STATUS_ACTIVE
        ] );

    }

    /**
     * @inheritdoc
    **/
    public static function findIdentityByAccessToken( $token, $type = null )
    {

        throw new NotSupportedException(
        	'"findIdentityByAccessToken" is not implemented.'
		);

    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
    **/
    public static function findByUsername( $username )
    {

        return static::findOne( [
            'username' => $username,
            'status'   => self::STATUS_ACTIVE
        ] );

    }

	/**
	 * Finds user by email
	 *
	 * @param string $email
	 * @return static|null
	**/
	public static function findByEmail( $email )
	{

		return static::findOne( [
			'email'  => $email,
			'status' => self::STATUS_ACTIVE
		] );

	}

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
    **/
    public static function findByPasswordResetToken( $token )
    {

        if ( !static::isPasswordResetTokenValid( $token ) )
            return null;

        return static::findOne( [
            'password_reset_token' => $token,
            'status'               => self::STATUS_ACTIVE,
        ] );

    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
    **/
    public static function isPasswordResetTokenValid( $token )
    {

        if ( empty( $token ) )
            return false;

        //params initializations
        $timestamp = (int)substr( $token, strrpos( $token, '_' ) + 1 );
        $expire    = Yii::$app->params[ 'user.passwordResetTokenExpire' ];

        return $timestamp + $expire >= time();

    }

	/**
	 * Fill inn company details due to company id
	**/
	public function getCompany()
	{

		//params initializations
		$company = Companies::findOne(
			$this->company_id
		);

		//set company details
		if ( ! is_null( $company ) )
			foreach ( [ 'name', 'bank', 'city', 'country', 'mfo', 'oked', 'street' ] as $attribite )
				$this->{ $attribite == 'name'
					? 'company_name'
					: $attribite
				} = $company->{ $attribite };

	}

    /**
     * @inheritdoc
    **/
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
    **/
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
    **/
    public function validateAuthKey( $authKey )
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
    **/
    public function validatePassword( $password )
    {

        return Yii::$app->security->validatePassword(
        	$password,
			$this->password_hash
		);

    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
    **/
    public function setPassword( $password )
    {

    	//set password to current model
		$this->password = ! empty( $password )
			? $password
			: '123456';

		//generate and set password hash
		$this->password_hash = ! empty( $password )
			? Yii::$app->security->generatePasswordHash( $password )
			: Yii::$app->security->generatePasswordHash( '123456' );

    }

    /**
     * return string prevent showing password
    **/
    public function getPassword()
    {
        return '';
    }

    /**
     * @return string user role name
    **/
    public function getRole()
    {

    	//params initializations
        $role      	  = '';
		$default_role = reset(
			Yii::$app->authManager->getRolesByUser( $this->id )
		);

        //set user role
        if ( ! empty( $default_role ) )
			$role = $this->role = $default_role->name;

        return $role;

    }

	/**
	 * @param string $color
	 * @return bool|string user image getter
	**/
	public function getAvatar( $color = 'blue' )
	{

		//params initializations
		$current_avatar = Files::findOne( [
			'id' => $this->file_id
		] );

		return ! is_null( $current_avatar )
			? Yii::getAlias( '@frontend_link' ) . $current_avatar->guide
			: Yii::getAlias( '@backend_link' ) . '/images/user-avatar-' . $color . '.png';

	}

    /**
     * Generates "remember me" authentication key
    **/
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
    **/
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
    **/
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

	/**
	 * @inheritdoc
	 * @return \yii\db\ActiveQuery get ActiveQuery with isDeleted filter
	**/
	public static function find()
	{

		//params initializations
		$query = new UserQuery(
			get_called_class()
		);

		return $query;


		/*return parent::find()->where( [
			'isDeleted' => false
		] );*/

	}

	/**
	 * This method is called at the beginning of inserting or updating a record.
	 * The default implementation will trigger an [[EVENT_BEFORE_INSERT]] event when `$insert` is `true`,
	 * or an [[EVENT_BEFORE_UPDATE]] event if `$insert` is `false`.
	 * When overriding this method, make sure you call the parent implementation like the following:
	 * @param bool $insert whether this method called while inserting a record.
	 * If `false`, it means the method is called while updating a record.
	 * @return bool whether the insertion or updating should continue.
	 * If `false`, the insertion or updating will be cancelled.
	**/
	public function beforeSave( $insert )
	{

		if ( parent::beforeSave( $insert )
			&& $this->validate()
		) {

			if ( ! empty( $this->company_name ) ) {

				//param initialization
				$company = Companies::findOne( [
					'name' => $this->company_name
				] );

				if ( is_null( $company ) ) {

					//company details
					$company = Companies::findOne(
						$this->company_id
					);

					//assign company
					if ( is_null( $company ) ) {

						$company = merge_objects( new Companies(), [
							'name' => $this->company_name
						] );

					}

				}

				//company main properties
				$company = merge_objects( $company, [
					'bank' 	  => $this->bank,
					'city' 	  => $this->city,
					'country' => $this->country,
					'mfo' 	  => $this->mfo,
					'oked' 	  => $this->oked,
					'street'  => $this->street
				] );

				//save company details
				if ( $company->save() )
					$this->company_id = $company->id;

			}


			//send registration success email
			if ( $insert ) {

				Yii::$app->commandBus->handle( new SendEmailCommand( [
					'to'      => $this->email,
					'subject' => __( 'Registration complete in InterRail' ),
					'view'    => 'registration',
					'params'  => [
						'user' => [
							'login'    => $this->email,
							'password' => $this->password
						]
					]
				] ) );

			}

			//emptify password for current model
			$this->password = $this->getPassword();

			return true;

		}

		return false;

	}

	/**
	 * This method is called at the end of inserting or updating a record.
	 * The default implementation will trigger an [[EVENT_AFTER_INSERT]] event when `$insert` is `true`,
	 * or an [[EVENT_AFTER_UPDATE]] event if `$insert` is `false`. The event class used is [[AfterSaveEvent]].
	 * @param bool $insert
	 * @param array $changedAttributes The old values of attributes that had changed and were saved.
	**/
    public function afterSave( $insert, $changedAttributes )
    {

    	//params initializations
        $auth = Yii::$app->authManager;

		//revoke user role
        if ( !$insert )
            $auth->revokeAll( $this->id );

        //set role
        $auth->assign(
			$this->role
				? $auth->getRole( $this->role )
				: $auth->getRole( 'client' ),
			$this->id
		);

    }

    /**
     * @inheritdoc
    **/
    public function afterDelete()
    {

		UserCustomFields::deleteAll( [
        	'user_id' => $this->id
		] );

    }

}
