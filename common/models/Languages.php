<?php namespace common\models;

/********************************************/
/*                                          */
/*             LANGUAGES MODEL              */
/*                                          */
/********************************************/

use Yii;

use yii\db\ActiveRecord;
use yii2tech\ar\softdelete\SoftDeleteBehavior;

use common\behaviors\LoggingBehavior;

/**
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property integer $file_id
 * @property bool $isDeleted
 *
 * @property Files[] $file
 * @property Settings[] $options
 * @property Posts[] $posts
 * @property Taxonomies[] $taxonomies
**/
class Languages extends ActiveRecord
{

    /**
     * @inheritdoc
    **/
    public static function tableName()
    {
        return '{{%languages}}';
    }

    /**
     * @inheritdoc
    **/
    public function behaviors()
    {

        return [
            'softDeleteBehavior' => [
                'class'                     => SoftDeleteBehavior::className(),
                'softDeleteAttributeValues' => [
                    'isDeleted' => true
                ],
                'replaceRegularDelete' => true
            ], [
				'class'  	 	=> LoggingBehavior::className(),
				'type'	  	 	=> 'languages',
				'foreign_id'    => 'id',
				'trace_options' => [
					'name',
					'description'
				]
			]

        ];

    }

    /**
     * @inheritdoc
    **/
    public function rules()
    {

        return [
            [
            	[
            		'name'
				], 'required'
			], [
				[
					'description'
				], 'string'
			], [
				[
					'file_id'
				], 'integer'
			], [
				[
					'name'
				], 'string',
				'max' => 255
			], [
				[
					'file_id'
				], 'exist',
				'skipOnError'     => true,
				'targetClass'     => Files::className(),
				'targetAttribute' => [
					'file_id' => 'id'
				]
			]
        ];

    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {

        return [
            'id'      	  => __( 'ID' ),
            'name'    	  => __( 'Name' ),
            'description' => __( 'Description' ),
            'file_id' 	  => __( 'File ID' )
        ];

    }

    /**
     * @return \yii\db\ActiveQuery
    **/
    public function getOptions()
    {

        return $this->hasMany( Settings::className(), [
            'lang_id' => 'id'
        ] );

    }

    /**
     * @return \yii\db\ActiveQuery
	**/
    public function getPosts()
    {

        return $this->hasMany( Posts::className(), [
            'lang_id' => 'id'
        ] );

    }

    /**
     * @return \yii\db\ActiveQuery
    **/
    public function getTaxonomies()
    {

        return $this->hasMany( Taxonomies::className(), [
            'lang_id' => 'id'
        ] );

    }


    /**
     * @inheritdoc
     * @return \yii\db\ActiveQuery get ActiveQuery with isDeleted filter
    **/
    public static function find()
    {

        return parent::find()->where( [
            'isDeleted' => false
        ] );

    }

}