<?php namespace common\models;

/********************************************/
/*                                          */
/*            ROUTES SEARCH MODEL           */
/*                                          */
/********************************************/

use yii\base\Model;
use yii\data\ActiveDataProvider;

class RoutesSearch extends Routes
{

	/**
	 * @inheritdoc
	**/
	public function rules()
	{

		return [
			[
				[
					'from', 'to'
				], 'integer'
			]
		];

	}

	/**
	 * @inheritdoc
	**/
	public function scenarios()
	{
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 * @return ActiveDataProvider
	**/
	public function search( $params )
	{

		//params initializations
		$query 	      = Routes::find();
		$dataProvider = new ActiveDataProvider( [
			'query' => $query,
		] );

		//loading requested params
		$this->load(
			$params
		);

		//queried params validation
		if ( !$this->validate() )
			return $dataProvider;

		//grid filtering conditions
		$query
			-> andFilterWhere( [
				'id'   => $this->id,
				'from' => $this->from,
				'to'   => $this->to
			] );

		return $dataProvider;

	}

}