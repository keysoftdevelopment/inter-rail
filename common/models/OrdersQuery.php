<?php namespace common\models;

/********************************************/
/*                                          */
/*           ORDERS QUERY MODEL             */
/*                                          */
/********************************************/

use yii\db\ActiveQuery;

class OrdersQuery extends ActiveQuery
{

    /**
     * @inheritdoc
     * @return Orders[]|array
    **/
    public function all( $db = null )
    {
        return parent::all( $db );
    }

    /**
     * @inheritdoc
     * @return Orders|array|null
    **/
    public function one( $db = null )
    {
        return parent::one( $db );
    }

}
