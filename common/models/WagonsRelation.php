<?php namespace common\models;

/********************************************/
/*                                          */
/*        	 WAGONS RELATION MODEL          */
/*                                          */
/********************************************/

use yii\db\ActiveRecord;

/**
 * @property integer $id
 * @property integer $foreign_id
 * @property integer $wagon_id
 * @property string $type
 *
 * @property Wagons $wagons
**/
class WagonsRelation extends ActiveRecord
{

    /**
     * @inheritdoc
    **/
    public static function tableName()
    {
        return '{{%wagons_relation}}';
    }

    /**
     * @inheritdoc
    **/
    public function rules()
    {

        return [
            [
            	[
            		'foreign_id', 'wagon_id'
				], 'integer'
			], [
				[
					'type'
				], 'string'
			], [
            	[
            		'wagon_id'
				], 'exist',
				'skipOnError' 	  => true,
				'targetClass' 	  => Wagons::className(),
				'targetAttribute' => [
					'wagon_id' => 'id'
				]
			]
        ];

    }

    /**
     * @inheritdoc
    **/
    public function attributeLabels()
    {

        return [
            'id'         => __( 'ID' ),
            'foreign_id' => __( 'Foreign ID' ),
            'wagon_id' 	 => __( 'Wagon ID' ),
			'type'		 => __( 'Type' )
        ];

    }

    /**
     * @return \yii\db\ActiveQuery
    **/
    public function getWagon()
    {

        return $this->hasOne( Wagons::className(), [
            'id' => 'wagon_id'
        ] );

    }

}