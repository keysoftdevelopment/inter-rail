<?php namespace common\models;

/********************************************/
/*                                          */
/*       		LOGISTICS MODEL         	*/
/*                                          */
/********************************************/

use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;

use common\behaviors\LoggingBehavior;

/**
 * @property integer $id
 * @property string $name
 * @property string $from
 * @property string $to
 * @property integer $duration
 * @property string $description
 * @property string $owner
 * @property string $transportation
 * @property string $c_type
 * @property string $status
 * @property string $transport_no
 * @property string $transport_ind
 * @property integer $weight
 * @property string $railway_details
 * @property float $rate
 * @property float $dthc
 * @property float $othc
 * @property float $bl
 * @property float $surcharge
 * @property float $total
 * @property string $type
 * @property string $updated_at
 * @property string $created_at
**/
class Logistics extends ActiveRecord
{

	/**
	 * @inheritdoc
	**/
	public $containers_type;

	/**
	 * @inheritdoc
	**/
	public $railway_type;

	/**
	 * @inheritdoc
	**/
	public $location_ids;

	/**
	 * @inheritdoc
	**/
    public static function tableName()
    {
        return '{{%logistics}}';
    }

	/**
	 * @inheritdoc
	**/
	public function behaviors()
	{

		return [

			'timestamp' => [
				'class' 	 => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => [
						'updated_at',
						'created_at'
					],
					ActiveRecord::EVENT_BEFORE_UPDATE => [
						'updated_at'
					]
				],
				'value' => function () {
					return date( 'Y-m-d H:i:s', strtotime( 'now' ) );
				}
			], [
				'class'  	 	=> LoggingBehavior::className(),
				'type'	  	 	=> 'logistics',
				'foreign_id'    => 'id',
				'trace_options' => [
					'name', 'from', 'to', 'duration',
					'rate', 'dthc', 'othc', 'bl',
					'surcharge', 'total', 'transportation', 'railway_type',
					'containers_type', 'status', 'c_type', 'transport_no',
					'transport_ind'
				]
			]

		];

	}

	/**
	 * @inheritdoc
	**/
    public function rules()
    {

        return [
            [
				[
					'description', 'railway_details', 'location_ids'
				], 'string'
			], [
				[
					'duration', 'weight'
				], 'integer'
			], [
				[
					'rate', 'dthc', 'othc', 'bl',
					'surcharge', 'total',
					'updated_at', 'created_at'
				], 'safe'
			], [
				[
					'from', 'to', 'transportation', 'railway_type',
					'containers_type', 'status', 'c_type'
				], 'string',
				'max' => 100
			], [
				[
					'name', 'type', 'owner',
					'transport_no', 'transport_ind'
				], 'string',
				'max' => 255
			]
        ];

    }

	/**
	 * @inheritdoc
	**/
    public function attributeLabels()
    {

        return [
            'id'          	      => __( 'ID' ),
            'name'        	      => __( 'Name' ),
			'from'        	      => __( 'Transportation From' ),
			'to'        	      => __( 'Transportation To' ),
			'duration'            => __( 'Transit Time' ),
            'description' 	      => __( 'Description' ),
			'owner'		  	      => __( 'Owner' ),
			'transportation'  	  => __( 'Transportation' ),
			'c_type'  	  		  => __( 'Container Type' ),
			'status'  		      => __( 'Status' ),
			'transport_no'	      => __( 'Transport Number' ),
			'transport_ind'	      => __( 'Transport Index' ),
			'weight'			  => __( 'Weight' ),
			'railway_details'     => __( 'Railway Details' ),
			'rate'		  	      => __( 'Rate' ),
			'dthc'		  	      => __( 'DTHC' ),
			'othc'		  	      => __( 'OTHC' ),
			'bl'		  	      => __( 'BL' ),
			'surcharge'		      => __( 'Surcharge' ),
			'total'  	     	  => __( 'Total Sum' ),
			'type'  	  	      => __( 'Type' ),
			'updated_at'  	      => __( 'Updated At' ),
            'created_at'  	      => __( 'Created At' )
        ];

    }

	/**
	 * Filling with places data current logistics
	**/
	public function getLocation()
	{

		//set location ids due to requested logistic id and type
		$this->location_ids = PlacesRelation::find()
			-> select( 'place_id' )
			-> where( [
				'foreign_id' => $this->id,
				'type'		 => $this->type
			] )
			-> asArray()
			-> column();

	}

	/**
	 * Fill in current model with railway and container types
	**/
	public function fillTypes()
	{

		//param initialization
		$types = TaxonomyRelation::find()
			-> where( [
				'foreign_id' => $this->id,
				'type'       => [
					$this->type,
					'railway_type'
				]
			] )
			-> asArray()
			-> all();

		//set container and reailway types
		foreach ( $types as $type ) {

			$this->{ $type[ 'type' ] == 'railway_type'
				? 'railway_type'
				: 'containers_type'
			} = $type[ 'tax_id' ];

		}

	}

	/**
	 * This method is called at the beginning of inserting or updating a record.
	 * The default implementation will trigger an [[EVENT_BEFORE_INSERT]] event when `$insert` is `true`,
	 * or an [[EVENT_BEFORE_UPDATE]] event if `$insert` is `false`.
	 * When overriding this method, make sure you call the parent implementation like the following:
	 * @param bool $insert whether this method called while inserting a record.
	 * If `false`, it means the method is called while updating a record.
	 * @return bool whether the insertion or updating should continue.
	 * If `false`, the insertion or updating will be cancelled.
	**/
	public function beforeSave( $insert )
	{

		if ( $this->validate() ) {

			if ( ! empty( $this->duration ) ) {
				$this->duration = $this->duration * 24 * 3600;
			}

			if ( ! empty( $this->location_ids ) ) {

				//the list of places
				$places = Places::find()
					-> where( [
						'id' => json_decode(
							stripslashes( $this->location_ids )
						)
					] )
					-> asArray()
					-> all();

				//delete all records
				PlacesRelation::deleteAll( [
					'foreign_id' => $this->id,
					'type'       => $this->type
				] );

				//set transportation details
				if( ! empty( $places ) ) {

					$this->from = $places[ $this->transportation == 'export'
						? end( array_keys( $places ) )
						: 0
					][ 'city' ] . ', '. $places[ $this->transportation == 'export'
						? end( array_keys( $places ) )
						: 0
					][ 'country' ];

					$this->to   = $places[ $this->transportation == 'export'
						? 0
						: end( array_keys( $places ) )
					][ 'city' ] . ', '. $places[ $this->transportation == 'export'
						? 0
						: end( array_keys( $places ) )
					][ 'country' ];

				}

			}

			return true;

		}

		return false;

	}

	/**
	 * This method is called at the end of inserting or updating a record.
	 * The default implementation will trigger an [[EVENT_AFTER_INSERT]] event when `$insert` is `true`,
	 * or an [[EVENT_AFTER_UPDATE]] event if `$insert` is `false`. The event class used is [[AfterSaveEvent]].
	 * @param bool $insert
	 * @param array $changedAttributes The old values of attributes that had changed and were saved.
	**/
	public function afterSave( $insert, $changedAttributes )
	{

		//saving logistic details
		foreach ( [
			'location_ids' 	  => new PlacesRelation(),
			'containers_type' => new TaxonomyRelation(),
			'railway_type' 	  => new TaxonomyRelation()
		] as $key => $type ) {

			if ( ! empty( $this->{ $key } ) ) {

				//delete all records
				$type::deleteAll( [
					'foreign_id' => $this->id,
					'type'       => $key == 'railway_type'
						? $key
						: $this->type
				] );

				//taxonomy details
				if ( in_array( $key, [ 'containers_type', 'railway_type' ] ) ) {

					$tax_rel = merge_objects( new TaxonomyRelation(), [
						'foreign_id' => $this->id,
						'tax_id' 	 => $this->{ $key },
						'type'	     => $key == 'railway_type'
							? $key
							: $this->type
					] );

					//saving taxonomy relation details
					if ( $tax_rel->save() )
						continue;

				} else {

					//save logistic location details
					foreach ( json_decode(
						stripslashes(
							$this->location_ids
						)
					) as $place_id ) {

						if ( ! empty( $place_id ) ) {

							//location details
							$place = merge_objects( new PlacesRelation, [
								'foreign_id' => $this->id,
								'place_id'   => (int)$place_id,
								'type'		 => $this->type
							] );

							//save logistic locations
							if ( $place->save() )
								continue;

						}

					}

				}

			}

		}

	}

}