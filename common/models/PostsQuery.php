<?php namespace common\models;

/********************************************/
/*                                          */
/*     POSTS, PAGES AND ETC QUERY MODEL     */
/*                                          */
/********************************************/

use yii\db\ActiveQuery;

class PostsQuery extends ActiveQuery
{

    /**
     * @inheritdoc
     * @return Posts[]|array
    **/
    public function all( $db = null )
    {
        return parent::all( $db );
    }

    /**
     * @inheritdoc
     * @return Posts|array|null
    **/
    public function one( $db = null )
    {
        return parent::one( $db );
    }

}