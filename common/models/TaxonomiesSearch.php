<?php namespace common\models;

/********************************************/
/*                                          */
/*         CATEGORIES SEARCH MODEL          */
/*                                          */
/********************************************/

use yii\base\Model;
use yii\data\ActiveDataProvider;

class TaxonomiesSearch extends Taxonomies
{

    /**
     * @inheritdoc
    **/
    public function rules()
    {
        return [
            [
            	[
            		'id', 'lang_id', 'parent'
				], 'integer'
			], [
            	[
            		'name', 'type'
				], 'safe'
			]
        ];

    }

    /**
     * @inheritdoc
    **/
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @return ActiveDataProvider
    **/
    public function search( $params )
    {

    	//params initializations
		$query 		  = Taxonomies::find();
		$dataProvider = new ActiveDataProvider( [
			'query' => isset( $params[ 'TaxonomiesSearch' ] )
				&& array_key_exists( 'parent', $params[ 'TaxonomiesSearch' ] )
				&& $params[ 'TaxonomiesSearch' ][ 'parent' ] == 0
				? $query->andWhere( [
					'is',
					'parent',
					null
				] )
				: $query
		] );

		//loading requested params
		$this->load(
			$params
		);

		//queried params validation
		if ( !$this->validate() )
			return $dataProvider;

        // grid filtering conditions
        $query
            -> andFilterWhere( [
                'id'      => $this->id,
                'lang_id' => $this->lang_id,
                'parent'  => $this->parent == 0
					? NULL
					: $this->parent,
            ] )
            -> andFilterWhere( [
            	'like', 'name', $this->name
			] )
            -> andFilterWhere( [
            	'like', 'type', $this->type
			] );

		// order by date
        $query->orderBy( [
        	'created_at' => SORT_DESC
		] );

        return $dataProvider;

    }

}