<?php namespace common\models;

/********************************************/
/*                                          */
/*         TAXONOMIES RELATION MODEL        */
/*                                          */
/********************************************/

use yii\db\ActiveRecord;

/**
 * @property integer $id
 * @property integer $foreign_id
 * @property integer $tax_id
 * @property string $type
 *
 * @property Posts $post
 * @property Taxonomies $tax
**/
class TaxonomyRelation extends ActiveRecord
{

    /**
     * @inheritdoc
    **/
    public static function tableName()
    {
        return '{{%taxonomy_relation}}';
    }

    /**
     * @inheritdoc
    **/
    public function rules()
    {

        return [
            [
            	[
            		'foreign_id', 'tax_id'
				], 'integer'
			], [
				[
					'type'
				], 'string'
			], [
            	[
            		'tax_id'
				], 'exist',
				'skipOnError' 	  => true,
				'targetClass' 	  => Taxonomies::className(),
				'targetAttribute' => [
					'tax_id' => 'id'
				]
			]
        ];

    }

    /**
     * @inheritdoc
    **/
    public function attributeLabels()
    {

        return [
            'id'         => __( 'ID' ),
            'foreign_id' => __( 'Foreign ID' ),
            'tax_id'     => __( 'Taxonomy ID' ),
			'type'		 => __( 'Type' )
        ];

    }

    /**
     * @return \yii\db\ActiveQuery
    **/
    public function getTax()
    {

        return $this->hasOne( Taxonomies::className(), [
            'id' => 'tax_id'
        ] );

    }

}