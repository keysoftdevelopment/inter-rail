<?php namespace common\models;

/********************************************/
/*                                          */
/*          SETTINGS SEARCH MODEL           */
/*                                          */
/********************************************/

use yii\base\Model;
use yii\data\ActiveDataProvider;

class SettingsSearch extends Settings
{

    /**
     * @inheritdoc
    **/
    public function rules()
    {

        return [
            [
            	[
            		'id', 'lang_id'
				], 'integer'
			], [
				[
					'name'
				], 'safe'
			]
        ];

    }

    /**
     * @inheritdoc
    **/
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @return ActiveDataProvider
    **/
    public function search( $params )
    {

    	//params initializations
        $query 	      = Settings::find();
        $dataProvider = new ActiveDataProvider( [
            'query' => $query,
        ] );

		//loading requested params
        $this->load(
        	$params
		);

		//queried params validation
        if ( !$this->validate() )
            return $dataProvider;

        // grid filtering conditions
        $query
			-> andFilterWhere( [
                'id'      => $this->id,
                'lang_id' => $this->lang_id,
            ] )
			-> andFilterWhere( [
				'like', 'name', $this->name
			] );

        return $dataProvider;

    }

}