<?php namespace common\models;

/********************************************/
/*                                          */
/*        RAILWAY CODES SEARCH MODEL        */
/*                                          */
/********************************************/

use yii\base\Model;
use yii\data\ActiveDataProvider;

class RailwayCodesSearch extends RailwayCodes
{

	/**
	 * @inheritdoc
	**/
	public function rules()
	{

		return [
			[
				[
					'forwarder', 'code'
				], 'string'
			], [
				[
					'order_id'
				], 'integer'
			]
		];

	}

	/**
	 * @inheritdoc
	**/
	public function scenarios()
	{
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 * @return ActiveDataProvider
	**/
	public function search( $params )
	{

		//params initializations
		$query 	      = RailwayCodes::find();
		$dataProvider = new ActiveDataProvider( [
			'query' => $query,
		] );

		//loading requested params
		$this->load(
			$params
		);

		//queried params validation
		if ( !$this->validate() ) {
			return $dataProvider;
		}

		//grid filtering conditions
		$query
			-> andFilterWhere( [
				'id'      	 => $this->id,
				'code'		=> $this->code,
				'forwarder' => $this->forwarder,
				'order_id'	=> $this->order_id,
			] )
			-> andWhere( [
				'IS NOT', 'code', NULL
			] );

		//order by date
		$query
			-> orderBy( [
				'created_at' => SORT_DESC
			] );

		return $dataProvider;

	}

}
