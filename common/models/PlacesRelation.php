<?php namespace common\models;

/********************************************/
/*                                          */
/*           PLACES RELATION MODEL          */
/*                                          */
/********************************************/

use yii\db\ActiveRecord;

/**
 * @property integer $id
 * @property integer $place_id
 * @property integer $foreign_id
 * @property string $type
 *
 * @property Places $place
**/
class PlacesRelation extends ActiveRecord
{

    /**
     * @inheritdoc
    **/
    public static function tableName()
    {
        return '{{%places_relation}}';
    }

    /**
     * @inheritdoc
    **/
    public function rules()
    {

        return [
            [
            	[
            		'place_id', 'foreign_id'
				], 'integer'
			], [
				[
					'type'
				], 'string'
			], [
            	[
            		'place_id'
				], 'exist',
				'skipOnError' 	  => true,
				'targetClass' 	  => Places::className(),
				'targetAttribute' => [
					'place_id' => 'id'
				]
			]
        ];

    }

    /**
     * @inheritdoc
    **/
    public function attributeLabels()
    {

        return [
            'id'     	 => __( 'ID' ),
            'place_id' 	 => __( 'Place ID' ),
            'foreign_id' => __( 'Foreign ID' ),
			'type'		 => __( 'Type' )
        ];

    }

    /**
     * @return \yii\db\ActiveQuery
    **/
    public function getPlace()
    {

        return $this->hasOne( Places::className(), [
            'id' => 'place_id'
        ] );

    }

}
