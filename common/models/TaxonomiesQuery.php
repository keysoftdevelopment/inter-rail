<?php namespace common\models;

/********************************************/
/*                                          */
/*         CATEGORIES QUERY MODEL           */
/*                                          */
/********************************************/

use yii\db\ActiveQuery;

class TaxonomiesQuery extends ActiveQuery
{

    /**
     * @inheritdoc
	 * @param mixed $db
     * @return Taxonomies[]|array
    **/
    public function all( $db = null )
    {
        return parent::all( $db );
    }

    /**
     * @inheritdoc
	 * @param mixed $db
     * @return Taxonomies|array|null
    **/
    public function one( $db = null )
    {
        return parent::one( $db );
    }

}