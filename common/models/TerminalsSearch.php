<?php namespace common\models;

/********************************************/
/*                                          */
/*          TERMINALS SEARCH MODEL          */
/*                                          */
/********************************************/

use yii\base\Model;
use yii\data\ActiveDataProvider;

class TerminalsSearch extends Terminals
{

    /**
     * @inheritdoc
    **/
    public function rules()
    {

        return [
            [
            	[
            		'place_id'
				], 'integer'
			], [
				[
					'name', 'email'
				], 'safe'
			]
        ];

    }

    /**
     * @inheritdoc
    **/
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @return ActiveDataProvider
    **/
    public function search( $params )
    {

    	//params initializations
		$query 	      = Terminals::find();
        $dataProvider = new ActiveDataProvider( [
            'query' => $query,
        ] );

		//loading requested params
        $this->load(
        	$params
		);

		//queried params validation
        if ( !$this->validate() )
            return $dataProvider;

        //grid filtering conditions
        $query
            -> andFilterWhere( [
                'id' 	   => $this->id,
				'place_id' => $this->place_id
            ] )
			-> andFilterWhere( [
				'like', 'name', $this->name
			] )
			-> andFilterWhere( [
				'like', 'email', $this->email
			] );

		//order by date
		$query->orderBy( [
			'created_at' => SORT_DESC
		] );

        return $dataProvider;

    }

}