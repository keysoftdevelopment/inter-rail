<?php namespace common\models;

/********************************************/
/*                                          */
/*             SETTINGS MODEL               */
/*                                          */
/********************************************/

use yii\db\ActiveRecord;
use yii2tech\ar\softdelete\SoftDeleteBehavior;

use common\behaviors\LoggingBehavior;

/**
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property integer $lang_id
 * @property bool $isDeleted
 *
 * @property Languages $lang
**/
class Settings extends ActiveRecord
{

    /**
     * @inheritdoc
    **/
    public static function tableName()
    {
        return '{{%settings}}';
    }

	/**
	 * @inheritdoc
	**/
    public function behaviors()
    {

        return [

            'softDeleteBehavior' => [
                'class'                     => SoftDeleteBehavior::className(),
                'softDeleteAttributeValues' => [
                    'isDeleted' => true
                ],
                'replaceRegularDelete' => true
            ], [
				'class'  	 	=> LoggingBehavior::className(),
				'type'	  	 	=> 'settings',
				'foreign_id'    => 'id',
				'trace_options' => [
					'name',
					'description'
				]
			]

        ];

    }

    /**
     * @inheritdoc
    **/
    public function rules()
    {

        return [
            [
            	[
            		'name'
				], 'required'
			], [
				[
					'description'
				], 'string'
			], [
				[
					'lang_id'
				], 'integer'
			], [
				[
					'name'
				], 'string',
				'max' => 255
			], [
				[
					'lang_id'
				],
				'exist',
				'skipOnError'     => true,
				'targetClass'     => Languages::className(),
				'targetAttribute' => [
					'lang_id' => 'id'
				]
			]
        ];

    }

    /**
     * @inheritdoc
    **/
    public function attributeLabels()
    {

        return [
            'id'          => __( 'ID' ),
            'name'        => __( 'Name' ),
            'description' => __( 'Description' ),
            'lang_id'     => __( 'Language ID' )
        ];

    }

    /**
     * @return \yii\db\ActiveQuery
    **/
    public function getLang()
    {

        return $this->hasOne( Languages::className(), [
            'id' => 'lang_id'
        ] );

    }

    /**
     * @inheritdoc
     * @return \yii\db\ActiveQuery get ActiveQuery with isDeleted filter
    **/
    public static function find()
    {

        return parent::find()->where( [
            'isDeleted' => false
        ] );

    }

}