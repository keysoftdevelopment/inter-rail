<?php namespace common\models;

/********************************************/
/*                                          */
/*          TERMINALS QUERY MODEL           */
/*                                          */
/********************************************/

use yii\db\ActiveQuery;

class TerminalsQuery extends ActiveQuery
{

    /**
     * @inheritdoc
     * @return Terminals[]|array
    **/
    public function all( $db = null )
    {
        return parent::all( $db );
    }

    /**
     * @inheritdoc
     * @return Terminals|array|null
    **/
    public function one( $db = null )
    {
        return parent::one( $db );
    }

}