<?php namespace common\models;

/********************************************/
/*                                          */
/*           INVOICES SEARCH MODEL          */
/*                                          */
/********************************************/

use yii\base\Model;
use yii\data\ActiveDataProvider;

class InvoicesSearch extends Invoices
{

    /**
     * @inheritdoc
    */
    public function rules()
    {

        return [
            [
				[
					'id', 'number', 'customer_id',
					'foreign_id', 'type', 'status'
				], 'safe'
			]
        ];

    }

    /**
     * @inheritdoc
    **/
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @return ActiveDataProvider
    **/
    public function search( $params )
    {

    	//param initialization
		$query 		  = Invoices::find();
        $dataProvider = new ActiveDataProvider( [
            'query' => $query,
        ] );

		//loading requested params
        $this->load(
        	$params
		);

		//queried params validation
        if ( !$this->validate() )
            return $dataProvider;

        //grid filtering conditions
        $query
            -> andFilterWhere( [
                'id'     	  => $this->id,
				'customer_id' => $this->customer_id,
				'foreign_id'  => $this->foreign_id,
                'number' 	  => $this->number,
                'type'		  => $this->type,
                'status' 	  => $this->status
            ] );

        return $dataProvider;

    }

}