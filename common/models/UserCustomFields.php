<?php namespace common\models;

/********************************************/
/*                                          */
/*         USER CUSTOM FIELDS MODEL         */
/*                                          */
/********************************************/

use yii\db\ActiveRecord;

/**
 * @property integer $id
 * @property integer $user_id
 * @property string $name
 * @property string $description
 *
 * @property User $user
**/
class UserCustomFields extends ActiveRecord
{

    /**
     * @inheritdoc
    **/
    public static function tableName()
    {
        return '{{%user_custom_fields}}';
    }

    /**
     * @inheritdoc
    **/
    public function rules()
    {

        return [
            [
            	[
            		'user_id'
				], 'integer'
			], [
				[
					'name'
				], 'required'
			], [
				[
					'description'
				], 'string'
			], [
				[
					'name'
				], 'string',
				'max' => 255
			], [
				[
					'user_id'
				], 'exist',
				'skipOnError'     => true,
				'targetClass'     => User::className(),
				'targetAttribute' => [
					'user_id' => 'id'
				]
			]
        ];

    }

    /**
     * @inheritdoc
    **/
    public function attributeLabels()
    {

        return [
            'id'      	  => __( 'ID' ),
            'user_id' 	  => __( 'User ID' ),
            'name'        => __( 'Name' ),
            'description' => __( 'Description' )
        ];

    }

    /**
     * @return \yii\db\ActiveQuery
    **/
    public function getUser()
    {

        return $this->hasOne( User::className(), [
            'id' => 'user_id'
        ] );

    }

}