<?php namespace common\models;

/********************************************/
/*                                          */
/*           USER SEARCH MODEL              */
/*                                          */
/********************************************/

use yii\base\Model;
use yii\data\ActiveDataProvider;

class UserSearch extends User
{

    /**
     * @inheritdoc
    **/
    public function rules()
    {

        return [
            [
            	[
            		'id', 'status'
				], 'integer'
			], [
				[
					'username', 'email', 'company',
					'first_name', 'last_name'
				], 'safe'
			]
        ];

    }

    /**
     * @inheritdoc
    **/
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @return ActiveDataProvider
    **/
    public function search( $params )
    {

		//params initializations
        $query 		  = User::find();
        $dataProvider = new ActiveDataProvider( [
            'query' => ! empty( $params[ 'role' ] )
				? $query
					-> andWhere( [
						'id' => \Yii::$app->authManager->getUserIdsByRole( $params[ 'role' ] )
					] )
				: $query
        ] );

		//loading requested params
        $this->load(
        	$params
		);

		//queried params validation
        if ( !$this->validate() )
            return $dataProvider;

        //grid filtering conditions
        $query
            -> andFilterWhere( [
                'id' => ! empty( $params[ 'role' ] )
					? \Yii::$app->authManager->getUserIdsByRole( $params[ 'role' ] )
					: $this->id,
                'status'    => $this->status,
				'isDeleted' => false
            ] )
            -> andFilterWhere( [
            	'like', 'username', $this->username
			] )
			-> andFilterWhere( [
				'like', 'company', $this->company
			] )
			-> andFilterWhere( [
				'like', 'first_name', $this->first_name
			] )
			-> andFilterWhere( [
				'like', 'last_name', $this->last_name
			] )
            -> andFilterWhere( [
            	'like', 'email', $this->email
			] );

		// order by date
		$query->orderBy( [
			'created_at' => SORT_DESC
		] );

        return $dataProvider;

    }

}