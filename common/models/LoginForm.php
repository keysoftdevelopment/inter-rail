<?php namespace common\models;

/********************************************/
/*                                          */
/*            LOGIN FORM MODEL              */
/*                                          */
/********************************************/

use Yii;

use yii\base\Model;

class LoginForm extends Model
{

	/**
	 * @inheritdoc
	**/
    public $email;

	/**
	 * @inheritdoc
	**/
    public $password;

	/**
	 * @inheritdoc
	**/
    public $rememberMe = true;

	/**
	 * @inheritdoc
	**/
    public $_user;

    /**
     * @inheritdoc
    **/
    public function rules()
    {

        return [
            [
            	[
            		'email', 'password'
				], 'required'
			], [
				'rememberMe', 'boolean'
			], [
				'password', 'validatePassword'
			]
        ];

    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
    **/
    public function validatePassword( $attribute, $params )
    {

    	//current user password validation
        if ( !$this->hasErrors() ) {

        	//requested user details
            $user = $this->getUser();

            //set validation error
            if ( !$user || !$user->validatePassword( $this->password ) )
                $this->addError( $attribute, __( 'Incorrect email or password' ) );

        }

    }

    /**
     * Logs in a user using the provided email and password.
     * @return boolean whether the user is logged in successfully
    **/
    public function login()
    {

		return $this->validate()
			? Yii::$app->user->login(
				$this->getUser(), $this->rememberMe
				? 3600 * 24 * 30
				: 0
			)
			: false;

    }

    /**
     * Finds user by [[email]]
     *
     * @return User|null
    **/
    protected function getUser()
    {

    	//user due to requested email
		$this->_user = $this->_user === null
			? $this->_user = User::findByEmail(
				$this->email
			)
			: $this->_user;

        return $this->_user;

    }

}
