<?php namespace common\models;

/********************************************/
/*                                          */
/*     	   TRANSLATION SEARCH MODEL         */
/*                                          */
/********************************************/

use yii\base\Model;
use yii\data\ActiveDataProvider;

class TranslationSearch extends Translation
{

	/**
	 * @inheritdoc
	**/
	public function rules()
	{

		return [
			[
				[
					'foreign_id', 'lang_id'
				], 'integer'
			], [
				[
					'title', 'type'
				], 'string'
			]
		];

	}

	/**
	 * @inheritdoc
	**/
	public function scenarios()
	{
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 * @return ActiveDataProvider
	**/
	public function search( $params )
	{

		//params initializations
		$query 		  = Translation::find();
		$dataProvider = new ActiveDataProvider( [
			'query' => $query,
		] );

		//loading requested params
		$this->load(
			$params
		);

		//queried params validation
		if ( !$this->validate() )
			return $dataProvider;

		// grid filtering conditions
		$query
			-> andFilterWhere( [
				'id'         => $this->id,
				'foreign_id' => $this->foreign_id,
				'lang_id'    => $this->lang_id
			] )
			-> andFilterWhere( [
				'like', 'title', $this->title
			] )
			-> andFilterWhere( [
				'like', 'type', $this->type
			] );

		return $dataProvider;

	}

}