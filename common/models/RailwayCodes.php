<?php namespace common\models;

/********************************************/
/*                                          */
/*         RAILWAY CODES MODEL              */
/*                                          */
/********************************************/

use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;

/**
 * @property integer $id
 * @property integer $order_id
 * @property string $country
 * @property string $route
 * @property string $forwarder
 * @property string $code
 * @property string $code_rg_start
 * @property string $code_rg_end
 * @property float $rate
 * @property float $surcharge
 * @property string $created_at
 *
 * @property Orders $order
**/
class RailwayCodes extends ActiveRecord
{

	/**
	 * @inheritdoc
	**/
	public static function tableName()
	{
		return '{{%railway_codes}}';
	}

	/**
	 * @inheritdoc
	**/
	public function behaviors()
	{

		return [

			'timestamp' => [
				'class' 	 => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => [
						'created_at'
					]
				],
				'value' => function () {
					return date( 'Y-m-d H:i:s', strtotime( 'now' ) );
				}
			]

		];

	}

	/**
	 * @inheritdoc
	**/
	public function rules()
	{

		return [
			[
				[
					'order_id', 'code_rg_start',
					'code_rg_end'
				], 'integer'
			], [
				[
					 'country', 'route',
					'forwarder', 'code'
				], 'string',
				'max' => 255
			], [
				[
					'rate', 'surcharge', 'created_at'
				], 'safe'
			], [
				[
					'order_id'
				], 'exist',
				'skipOnError'     => true,
				'targetClass'     => Orders::className(),
				'targetAttribute' => [
					'order_id' => 'id'
				]
			]
		];

	}

	/**
	 * @inheritdoc
	**/
	public function attributeLabels()
	{

		return [
			'id' 		    => __( 'ID' ),
			'order_id'	    => __( 'Order ID' ),
			'country' 	    => __( 'Country' ),
			'route' 	    => __( 'Route' ),
			'forwarder'     => __( 'Forwarder' ),
			'code' 		    => __( 'Code' ),
			'code_rg_start' => __( 'Code Range From' ),
			'code_rg_end'   => __( 'Code Range To' ),
			'rate' 		 	=> __( 'Rate' ),
			'surcharge'  	=> __( 'Surcharge' ),
			'created_at' 	=> __( 'Created At' )
		];

	}

	/**
	 * @return \yii\db\ActiveQuery
	**/
	public function getOrder()
	{

		return $this->hasOne( Orders::className(), [
			'id' => 'order_id'
		] );

	}

}