<?php namespace common\models;

/********************************************/
/*                                          */
/*      TERMINAL RENTALS SEARCH MODEL      	*/
/*                                          */
/********************************************/

use yii\base\Model;
use yii\data\ActiveDataProvider;

class TerminalStorageSearch extends TerminalStorage
{

	/**
	 * @inheritdoc
	**/
	public function rules()
	{

		return [
			[
				[
					'container_id', 'order_id', 'terminal_id'
				], 'integer'
			], [
				[
					'from', 'to'
				], 'safe'
			]
		];

	}

	/**
	 * @inheritdoc
	**/
	public function scenarios()
	{
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 * @return ActiveDataProvider
	**/
	public function search( $params )
	{

		//params initializations
		$query 	   	  = TerminalStorage::find();
		$dataProvider = new ActiveDataProvider( [
			'query' => $query,
		] );

		//loading requested params
		$this->load(
			$params
		);

		//queried params validation
		if ( !$this->validate() )
			return $dataProvider;

		//grid filtering conditions
		$query
			-> andFilterWhere( [
				'id'       	   => $this->id,
				'container_id' => $this->container_id,
				'order_id' 	   => $this->order_id,
				'terminal_id'  => $this->terminal_id
			] )
			-> andWhere( [
				'>=', 'from', date( 'Y-m-d', strtotime( $this->from ) )
			] )
			-> andWhere( [
				'<=', 'to', date( 'Y-m-d', strtotime( $this->to ) )
			] );

		return $dataProvider;

	}

}