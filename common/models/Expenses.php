<?php namespace common\models;

/********************************************/
/*                                          */
/*             EXPENSES MODEL               */
/*                                          */
/********************************************/

use Yii;

use yii\db\ActiveRecord;

/**
 * @property integer $id
 * @property integer $order_id
 * @property float $sale
 * @property float $purchase
 * @property float $rental
 * @property float $deposit
 * @property float $container_surcharge
 * @property float $container_cost
 * @property float $sf_rate
 * @property float $dthc
 * @property float $othc
 * @property float $bl
 * @property float $sf_id
 * @property float $sf_surcharge
 * @property float $auto_id
 * @property float $auto_rate
 * @property float $auto_surcharge
 * @property string $rf_details
 * @property float $rf_rental
 * @property float $rf_commission
 * @property float $pf_rate
 * @property float $pf_surcharge
 * @property float $pf_commission
 * @property float $storage
 * @property float $lading
 * @property float $unloading
 * @property float $exp_price
 * @property float $imp_price
 * @property float $terminal_surcharge
 * @property float $thc
 * @property float $tracking
 * @property float $surcharge
 * @property float $total
 *
 * @property Orders $order
**/
class Expenses extends ActiveRecord
{

	/**
	 * @inheritdoc
	**/
	public $container_id;

	/**
	 * @inheritdoc
	**/
	public $invoices;

	/**
	 * @inheritdoc
	**/
	public $terminal_id;

	/**
	 * @inheritdoc
	**/
	public $terminal_date_from;

	/**
	 * @inheritdoc
	**/
	public $terminal_date_to;

	/**
	 * @inheritdoc
	**/
	public $wagon_id;

	/**
	 * @inheritdoc
	**/
	public static function tableName()
	{
		return '{{%expenses}}';
	}

	/**
	 * @inheritdoc
	**/
	public function rules()
	{

		return [
			[
				[
					'order_id',
				], 'required'
			], [
				[
					'container_id',
					'sf_id',
					'order_id',
					'terminal_id',
					'auto_id',
					'wagon_id'
				], 'integer'
			], [
				[
					'sale', 'purchase', 'rental', 'deposit', 'container_surcharge', 'container_cost',
					'sf_rate', 'dthc', 'othc', 'bl', 'sf_surcharge', 'auto_rate', 'auto_surcharge',
					'rf_rental', 'rf_commission', 'pf_rate', 'pf_surcharge', 'pf_commission',
					'terminal_date_from', 'terminal_date_to', 'storage', 'lading', 'unloading', 'exp_price',
					'imp_price', 'terminal_surcharge', 'thc', 'tracking', 'surcharge', 'total'
				], 'safe'
			], [
				[
					'rf_details', 'invoices'
				], 'string',
				'max' => 20000
			], [
				[
					'order_id'
				], 'exist',
				'skipOnError'     => true,
				'targetClass'     => Orders::className(),
				'targetAttribute' => [
					'order_id' => 'id'
				]
			]
		];

	}

	/**
	 * @inheritdoc
	**/
	public function attributeLabels()
	{

		return [
			'id' 				  => __( 'ID' ),
			'order_id'			  => __( 'Order ID' ),
			'sale' 				  => __( 'Container Sale' ),
			'purchase' 		      => __( 'Container Purchase' ),
			'rental' 			  => __( 'Container Rental' ),
			'deposit' 			  => __( 'Container Deposit' ),
			'container_surcharge' => __( 'Container Surcharge' ),
			'container_cost' 	  => __( 'Container Cost' ),
			'sf_rate'		      => __( 'Sea Freight Rate' ),
			'dthc' 				  => __( 'DTHC' ),
			'othc' 				  => __( 'OTHC' ),
			'bl' 				  => __( 'BL' ),
			'sf_id'				  => __( 'Sea Freight ID' ),
			'sf_surcharge' 		  => __( 'Sea Freight Surcharge' ),
			'auto_id'		      => __( 'Auto ID' ),
			'auto_rate' 		  => __( 'Auto Rate' ),
			'auto_surcharge' 	  => __( 'Auto Surcharge' ),
			'rf_details' 		  => __( 'Railway Details' ),
			'rf_rental'			  => __( 'Platform Rentals' ),
			'rf_commission' 	  => __( 'Railway Agent Commission' ),
			'pf_rate' 		      => __( 'Port Forwarding Rate' ),
			'pf_surcharge'	      => __( 'Port Forwarding Surcharge' ),
			'pf_commission' 	  => __( 'Port Forwarding Commission' ),
			'storage' 		      => __( 'Storage' ),
			'lading'			  => __( 'Crane Price For Lading' ),
			'unloading'			  => __( 'Crane Price For Unloading' ),
			'exp_price' 			  => __( 'Export Price' ),
			'imp_price' 			  => __( 'Import Price' ),
			'terminal_surcharge'  => __( 'Terminal Surcharge' ),
			'thc' 				  => __( 'THC' ),
			'tracking' 			  => __( 'Tracking' ),
			'surcharge'			  => __( 'Surcharge' ),
			'total'				  => __( 'Total' )
		];

	}

	/**
	 * @return \yii\db\ActiveQuery
	**/
	public function getOrder()
	{

		return $this->hasOne( Orders::className(), [
			'id' => 'order_id'
		] );

	}

	/**
	 * Filling model with terminal storage details
	**/
	public function fillStorageDates()
	{

		//fill in current model with terminal rental days
		if ( ! empty( $this->container_id ) ) {

			//terminal storage details
			$rentals = TerminalStorage::findOne( [
				'container_id' => $this->container_id,
				'order_id'	   => $this->order_id
			] );

			//set storage dates
			if ( ! empty( $rentals ) ) {
				$this->terminal_date_from = $rentals->from;
				$this->terminal_date_to   = $rentals->to;
			}

		}

	}

	/**
	 * This method is called at the end of inserting or updating a record.
	 * The default implementation will trigger an [[EVENT_AFTER_INSERT]] event when `$insert` is `true`,
	 * or an [[EVENT_AFTER_UPDATE]] event if `$insert` is `false`. The event class used is [[AfterSaveEvent]].
	 * @param bool  $insert
	 * @param array $changedAttributes The old values of attributes that had changed and were saved.
	**/
	public function afterSave( $insert, $changedAttributes )
	{

		foreach ( [
			'container_id' => new ContainersRelation(),
			'wagon_id'	   => new WagonsRelation(),
			'invoices' 	   => new Invoices()
		] as $key => $param ) {

			//is requested param has value
			if ( ! empty( $this->{ $key } ) ) {

				//deleting records due to params existence
				if ( $key == 'container_id' ) {

					ContainersRelation::deleteAll( [
						'foreign_id' => $this->id,
						'type'		 => 'expenses'
					] );

				} else if ( $key == 'wagon_id' ) {

					WagonsRelation::deleteAll( [
						'foreign_id' => $this->id,
						'type'		 => 'expenses'
					] );

				} else if ( $key == 'invoices' ) {

					Invoices::deleteAll( [
						'foreign_id' => $this->id,
						'type'		 => 'expenses'
					] );

				}

				if ( in_array( $key, [ 'container_id', 'wagon_id' ] ) ) {

					if ( (int)$this->{ $key } > 0 ) {

						//fill container/wagon relation details
						$properties = merge_objects( $param, [
							'foreign_id' => $this->id,
							$key 		 => $this->{ $key },
							'type'		 => 'expenses'
						] );

						//save details
						$properties->save( false );

						//save rental details
						if ( $key == 'container_id'
							&& ! empty( $this->terminal_date_from )
							&& ! empty( $this->terminal_date_to )
							&& ! empty( $this->terminal_id )
						) {

							//is terminal storage record exists
							$rentals = TerminalStorage::findOne( [
								'container_id' => $this->container_id,
								'order_id' 	   => $this->order_id,
								'terminal_id'  => $this->terminal_id
							] );

							//saving terminal storage details
							$rentals = merge_objects( is_null( $rentals )
								? new TerminalStorage()
								: $rentals, [
								'container_id' => $this->container_id,
								'order_id'	   => $this->order_id,
								'terminal_id'  => $this->terminal_id,
								'from' 		   => date( 'Y-m-d', strtotime(
									str_replace( [ '.', '/' ], '-', $this->terminal_date_from ) )
								),
								'to' => date(
									'Y-m-d', strtotime(
										str_replace( [ '.', '/' ], '-', $this->terminal_date_to )
									)
								)
							] );

							//saving rental details
							$rentals->save( false );

						}

					}

				} else {

					//param initialization
					$invoices = json_decode( $this->{ $key } );

					if( ! is_null( $invoices ) ) {

						foreach ( $invoices->number as $invoice_key => $number ) {

							//saving invoice details
							$invoice = merge_objects( new Invoices(), [
								'foreign_id'  => $this->id,
								'number' 	  => $number,
								'customer_id' => isset( $invoices->customer_id[ $invoice_key ] ) && ! empty( $invoices->customer_id[ $invoice_key ] )
									? $invoices->customer_id[ $invoice_key ]
									: 0,
								'amount' => isset( $invoices->amount[ $invoice_key ] )
									? $invoices->amount[ $invoice_key ]
									: 0,
								'date' => isset( $invoices->date[ $invoice_key ] )
									? date( 'Y-m-d', strtotime( str_replace( '/', '-', $invoices->date[ $invoice_key ] ) ) )
									: '',
								'type' => 'expenses'
							] );

							//save invoice details
							$invoice->save( false );

						}

					}

				}

			}

		}

	}

}