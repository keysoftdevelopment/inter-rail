<?php namespace common\models;

/********************************************/
/*                                          */
/*            USERS QUERY MODEL             */
/*                                          */
/********************************************/

use yii\db\ActiveQuery;

class UserQuery extends ActiveQuery
{

    /**
     * @inheritdoc
     * @return User[]|array
    **/
    public function all( $db = null )
    {
        return parent::all( $db );
    }

    /**
     * @inheritdoc
     * @return User|array|null
    **/
    public function one( $db = null )
    {
        return parent::one( $db );
    }

}