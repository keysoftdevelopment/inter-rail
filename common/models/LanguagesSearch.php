<?php namespace common\models;

/********************************************/
/*                                          */
/*         LANGUAGES SEARCH MODEL           */
/*                                          */
/********************************************/

use yii\base\Model;
use yii\data\ActiveDataProvider;

class LanguagesSearch extends Languages
{

    /**
     * @inheritdoc
    **/
    public function rules()
    {

        return [
            [
            	[
            		'id'
				], 'integer'
			], [
				[
					'name'
				], 'safe'
			]
        ];

    }

    /**
     * @inheritdoc
    **/
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @return ActiveDataProvider
    **/
    public function search( $params )
    {

    	//params initializations
        $query 	      = Languages::find();
        $dataProvider = new ActiveDataProvider( [
            'query' => $query,
        ] );

		//loading requested params
        $this->load(
        	$params
		);

		//queried params validation
        if ( !$this->validate() )
            return $dataProvider;

        //grid filtering conditions
        $query
            -> andFilterWhere( [
                'id' => $this->id
            ] )
			-> andFilterWhere( [
				'like', 'name', $this->name
			] );

        return $dataProvider;

    }

}