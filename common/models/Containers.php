<?php namespace common\models;

/********************************************/
/*                                          */
/*     		  CONTAINERS MODEL         	    */
/*                                          */
/********************************************/

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii2tech\ar\softdelete\SoftDeleteBehavior;

use common\behaviors\LoggingBehavior;

/**
 * @property integer $id
 * @property string $number
 * @property string $owner
 * @property string $transportation
 * @property string $status
 * @property bool $isDeleted
 * @property string $updated_at
 * @property string $created_at
**/
class Containers extends ActiveRecord
{

	/**
	 * @inheritdoc
	**/
	protected $container;

	/**
	 * @inheritdoc
	**/
	public $type;

    /**
     * @inheritdoc
	**/
    public static function tableName()
    {
        return '{{%containers}}';
    }

	/**
	 * @inheritdoc
	**/
	public function behaviors()
	{

		return [

			'softDeleteBehavior' => [
				'class'                     => SoftDeleteBehavior::className(),
				'softDeleteAttributeValues' => [
					'isDeleted' => true
				],
				'replaceRegularDelete' => true
			],

			'timestamp'          => [
				'class' 	 => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => [
						'updated_at',
						'created_at'
					],
					ActiveRecord::EVENT_BEFORE_UPDATE => [
						'updated_at'
					]
				],
				'value' => function () {
					return date( 'Y-m-d H:i:s', strtotime( 'now' ) );
				}
			], [
				'class'  	 	=> LoggingBehavior::className(),
				'type'	  	 	=> 'containers',
				'foreign_id'    => 'id',
				'trace_options' => [
					'number',
					'owner'
				]
			]
		];

	}

    /**
     * @inheritdoc
	**/
    public function rules()
    {

        return [
            [
            	[
            		'number', 'type'
				], 'required'
			], [
				[
					'status'
				], 'string'
			], [
				[
					'type'
				], 'integer'
			], [
				[
					'updated_at', 'created_at'
				], 'safe'
			], [
				[
					'number'
				], 'unique'
			], [
				[
					'number', 'owner', 'transportation'
				], 'string',
				'max' => 255
			]
        ];

    }


    /**
     * @inheritdoc
    **/
    public function attributeLabels()
    {

        return [
            'id'          	 => __( 'ID' ),
            'number'      	 => __( 'Number' ),
            'owner'     	 => __( 'Owner' ),
            'transportation' => __( 'Transportation' ),
			'status'  	     => __( 'Status' ),
			'updated_at'  	 => __( 'Updated At' ),
            'created_at'  	 => __( 'Created At' )
        ];

    }

	/**
	 * Fill in current model with appropriate taxonomy
	**/
	public function getType()
	{
		//params initializations
		$taxonomy = TaxonomyRelation::findOne( [
			'foreign_id' => $this->id,
			'type' 	     => 'containers'
		] );

		//set type to model
		if ( ! is_null( $taxonomy ) )
			$this->type = $taxonomy->tax_id;

	}

    /**
     * @inheritdoc
     * @return ContainersQuery the active query used by this AR class.
	**/
    public static function find()
    {

    	//params initializations
        $query = new ContainersQuery(
        	get_called_class()
		);

        return $query;

    }

	/**
	 * This method is called at the end of inserting or updating a record.
	 * The default implementation will trigger an [[EVENT_AFTER_INSERT]] event when `$insert` is `true`,
	 * or an [[EVENT_AFTER_UPDATE]] event if `$insert` is `false`. The event class used is [[AfterSaveEvent]].
	 * @param bool $insert
	 * @param array $changedAttributes The old values of attributes that had changed and were saved.
	**/
	public function afterSave( $insert, $changedAttributes )
	{

		if ( ! empty( $this->type ) ) {

			//delete all container type relations for current terminal
			TaxonomyRelation::deleteAll( [
				'foreign_id' => $this->id,
				'type'       => 'containers'
			] );

			//fill in taxonomy details with request details
			$taxonomy = merge_objects( new TaxonomyRelation(), [
				'foreign_id' => $this->id,
				'tax_id' 	 => $this->type,
				'type'	 	 => 'containers'
			] );

			//save taxonomy relations
			$taxonomy->save();

		}

	}

}