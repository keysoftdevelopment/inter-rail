<?php namespace common\models;

/********************************************/
/*                                          */
/*               RATES MODEL                */
/*                                          */
/********************************************/

use yii\db\ActiveRecord;

use yii\behaviors\TimestampBehavior;

/**
 * @property integer $id
 * @property string $from
 * @property string $to
 * @property integer $term_from
 * @property integer $term_to
 * @property float $thc
 * @property float $forming
 * @property float $forwarding
 * @property float $rental
 * @property float $wd
 * @property float $margin
 * @property float $surcharge
 * @property integer $rental_period
 * @property string $transportation
 * @property string $customs
 * @property string $updated_at
 * @property string $created_at
**/
class Rates extends ActiveRecord
{

	/**
	 * @inheritdoc
	**/
	public $containers_type;

	/**
	 * @inheritdoc
	**/
	public static function tableName()
	{
		return '{{%rates}}';
	}

	/**
	 * @inheritdoc
	**/
	public function behaviors()
	{

		return [

			'timestamp' => [
				'class' 	 => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => [
						'updated_at',
						'created_at'
					],
					ActiveRecord::EVENT_BEFORE_UPDATE => [
						'updated_at'
					]
				],
				'value' => function () {
					return date( 'Y-m-d H:i:s', strtotime( 'now' ) );
				}
			]

		];

	}

	/**
	 * @inheritdoc
	**/
	public function rules()
	{

		return [
			[
				[
					'from', 'to',
					'term_from', 'term_to',
					'containers_type'
				], 'required'
			], [
				[
					'containers_type', 'term_from',
					'term_to', 'rental_period'
				], 'integer'
			], [
				[
					'thc', 'forming', 'forwarding',
					'rental', 'wd', 'margin',
					'surcharge'
				], 'safe'
			], [
				[
					'customs'
				], 'string',
				'max' => 20000
			], [
			   [
			   		'from', 'to',
				   'transportation'
			   ], 'string'
			], [
				[
					'order_id'
				], 'exist',
				'skipOnError'     => true,
				'targetClass'     => Orders::className(),
				'targetAttribute' => [
					'order_id' => 'id'
				]
			]
		];

	}

	/**
	 * @inheritdoc
	**/
	public function attributeLabels()
	{

		return [
			'id' 			 => __( 'ID' ),
			'from' 		     => __( 'Transportation From' ),
			'to' 			 => __( 'Transportation To' ),
			'term_from' 	 => __( 'Term From' ),
			'term_to' 		 => __( 'Term To' ),
			'thc' 			 => __( 'THC' ),
			'forming' 	     => __( 'Forming BT' ),
			'forwarding' 	 => __( 'Forwarding' ),
			'rental' 		 => __( 'Rental' ),
			'wd' 			 => __( 'Wagon Detention' ),
			'margin'	     => __( 'TYK Margin' ),
			'rental_period'  => __( 'Rental Period' ),
			'surcharge'		 => __( 'Surcharge' ),
			'transportation' => __( 'Transportation' ),
			'customs' 	     => __( 'Customs' ),
			'updated_at' 	 => __( 'Updated At' ),
			'created_at' 	 => __( 'Created At' )
		];

	}

	/**
	 * Fill in current model with container types
	**/
	public function fillModel()
	{

		//param initialization
		$type = TaxonomyRelation::findOne( [
			'foreign_id' => $this->id,
			'type'       => 'rates'
		] );

		//set container type
		$this->containers_type = ! is_null( $type )
			? $type->tax_id
			: $this->containers_type;

		//decoding model customs details
		$this->customs = ! empty( $this->customs )
			? json_decode( $this->customs )
			: '';

	}

	/**
	 * This method is called at the end of inserting or updating a record.
	 * The default implementation will trigger an [[EVENT_AFTER_INSERT]] event when `$insert` is `true`,
	 * or an [[EVENT_AFTER_UPDATE]] event if `$insert` is `false`. The event class used is [[AfterSaveEvent]].
	 * @param bool $insert
	 * @param array $changedAttributes The old values of attributes that had changed and were saved.
	**/
	public function afterSave( $insert, $changedAttributes )
	{

		if( ! empty( $this->containers_type ) ) {

			//delete all records
			TaxonomyRelation::deleteAll( [
				'foreign_id' => $this->id,
				'type'       => 'rates'
			] );

			$tax_rel = merge_objects( new TaxonomyRelation(), [
				'foreign_id' => $this->id,
				'tax_id' 	 => $this->containers_type,
				'type'	     => 'rates'
			] );

			//saving taxonomy relation details
			$tax_rel->save();

		}

	}

}