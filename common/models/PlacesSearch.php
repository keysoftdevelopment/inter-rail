<?php namespace common\models;

/********************************************/
/*                                          */
/*            PLACES SEARCH MODEL           */
/*                                          */
/********************************************/

use yii\base\Model;
use yii\data\ActiveDataProvider;

class PlacesSearch extends Places
{

	/**
	 * @inheritdoc
	**/
	public function rules()
	{

		return [
			[
				[
					'foreign_id'
				], 'integer'
			],[
				[
					'name', 'country', 'city',
					'district', 'type'
				], 'string'
			]
		];

	}

	/**
	 * @inheritdoc
	**/
	public function scenarios()
	{
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 * @return ActiveDataProvider
	**/
	public function search( $params )
	{

		//param initialization
		$query 		  = Places::find();
		$dataProvider = new ActiveDataProvider( [
			'query' => isset( $params[ 'type' ] )
				&& !empty( $params[ 'type' ] )
				? $query -> where( [
					'type' => $params[ 'type' ]
				] )
				: $query,
		] );

		//loading requested params
		$this->load(
			$params
		);

		//grid filtering conditions
		$query
			-> andFilterWhere( [
				'id'         => $this->id,
				'name'		 => $this->name,
				'foreign_id' => $this->foreign_id,
				'type'		 => $this->type
			] )
			-> andFilterWhere( [
				'like', 'country', $this->country
			] )
			-> andFilterWhere( [
				'like', 'city', $this->city
			] )
			-> andFilterWhere( [
				'like', 'district', $this->district
			] );

		return $dataProvider;

	}

}
