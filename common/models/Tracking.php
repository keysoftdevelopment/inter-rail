<?php namespace common\models;

/********************************************/
/*                                          */
/*     		    TRACKING MODEL         	    */
/*                                          */
/********************************************/

use yii\db\ActiveRecord;

/**
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property integer $place_id
 * @property string $check_in_date
 * @property integer $foreign_id
 * @property string $trans_from
 * @property string $trans_to
 * @property string $departure
 * @property string $arrival
 * @property float $distance
 * @property string $type
 * @property string $status
 *
 * @property Places $places
**/
class Tracking extends ActiveRecord
{

    /**
     * @inheritdoc
    **/
    public static function tableName()
    {
        return '{{%tracking}}';
    }

    /**
     * @inheritdoc
    **/
    public function rules()
    {

        return [
            [
            	[
            		'name', 'check_in_date', 'trans_from',
					'trans_to', 'departure', 'arrival'
				], 'required'
			], [
				[
					'description', 'status'
				], 'string'
			], [
				[
					'place_id', 'foreign_id'
				], 'integer'
			], [
				[
					'check_in_date', 'departure',
					'arrival', 'distance'
				], 'safe'
			], [
				[
					'name', 'trans_from', 'trans_to', 'type'
				], 'string',
				'max' => 255
			], [
				[
					'place_id'
				], 'exist',
				'skipOnError'     => true,
				'targetClass'     => Places::className(),
				'targetAttribute' => [
					'place_id' => 'id'
				]
			]
        ];

    }

    /**
     * @inheritdoc
    **/
    public function attributeLabels()
    {

        return [
            'id'          	=> __( 'ID' ),
            'name'        	=> __( 'Name' ),
            'description' 	=> __( 'Description' ),
            'place_id'    	=> __( 'Place ID' ),
			'check_in_date' => __( 'Check In Date' ),
			'foreign_id'  	=> __( 'Foreign ID' ),
			'trans_from'  	=> __( 'Transportation From' ),
            'trans_to'    	=> __( 'Transportation To' ),
			'departure'   	=> __( 'Departure' ),
			'arrival'     	=> __( 'Arrival' ),
			'distance'     	=> __( 'Distance' ),
			'type'		  	=> __( 'Type' ),
			'status'	  	=> __( 'Status' )
        ];

    }

	/**
	 * @return \yii\db\ActiveQuery
	**/
	public function getPlace()
	{

		return $this->hasOne( Places::className(), [
			'id' => 'place_id'
		] );

	}

}