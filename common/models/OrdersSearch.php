<?php namespace common\models;

/********************************************/
/*                                          */
/*           ORDERS SEARCH MODEL            */
/*                                          */
/********************************************/

use yii\base\Model;
use yii\data\ActiveDataProvider;

class OrdersSearch extends Orders
{

    /**
     * @inheritdoc
    **/
    public function rules()
    {

        return [
            [
            	[
					'user_id', 'manager_id', 'terminal_id'
				], 'integer'
			], [
				[
					'number', 'trans_from',
					'trans_to', 'status',
					'transportation', 'contractors'
				], 'string'
			]
        ];

    }

    /**
     * @inheritdoc
    **/
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @return ActiveDataProvider
    **/
    public function search( $params )
    {

    	//params initializations
		$query 	   = Orders::find();
		$order_ids = ! empty( $params[ 'wagon' ] )
			? WagonsRelation::find()
				-> select( 'foreign_id' )
				->where( [
					'wagon_id' => $params[ 'wagon' ],
					'type'	   => 'order'
				] )
				-> column()
			: [];

		//sorting by requested block train
		if ( ! empty( $params[ 'blockTrain' ] ) ) {

			//order id lists in logistics relation table
			$bt_query = LogisticsRelation::find()
				-> select( 'foreign_id' )
				-> where( [
					'logistic_id' => $params[ 'blockTrain' ],
					'type'		  => [
						'blockTrain',
						'train'
					]
				] );

			//sorting result by order ids
			if ( ! empty( $order_ids ) ) {

				$bt_query
					-> andWhere( [
						'foreign_id' => $order_ids,
					] );

			}


			//re-assign order ids param
			$order_ids = $bt_query->column();

		}

		//sorting by requested forwarder
		if ( ! empty( $params[ 'forwarder' ] ) ) {

			//order id lists in railway codes table
			$forwarder_query = RailwayCodes::find()
				-> select( 'order_id' )
				-> where( [
					'like', 'forwarder', $params[ 'forwarder' ]
				] );

			//sorting result by order ids
			if ( ! empty( $order_ids ) ) {

				$forwarder_query
					-> andWhere( [
						'foreign_id' => $order_ids,
					] );

			}

			//re-assign order ids param
			$order_ids = $forwarder_query->column();

		}

		if ( ! empty( $order_ids ) ) {

			$query
				-> where( [
					'id' => $order_ids
				] );

		}

		if ( ! empty( $params[ 'date_from' ] )
			&& ! empty( $params[ 'date_to' ] )
		) {

			$query
				-> andWhere( [
					'>=', 'created_at', date( 'Y-m-d', strtotime( $params[ 'date_from' ] ) )
				] )
				-> andWhere( [
					'<=', 'created_at', date( 'Y-m-d', strtotime( $params[ 'date_to' ] . ' +1 day' ) )
				] );

		}

		//loading requested params
		$this->load(
			$params
		);

        // grid filtering conditions
        $query
            -> andFilterWhere( [
                'id'          	 => $this->id,
				'number'	  	 => $this->number,
				'user_id'     	 => $this->user_id,
				'terminal_id' 	 => $this->terminal_id,
				'transportation' => $this->transportation,
				'status'	  	 => $this->status,
				'isDeleted'   	 => 0
            ] )
			-> andFilterWhere( [
				'like', 'trans_from', $this->trans_from
			] )
			-> andFilterWhere( [
				'like', 'trans_to', $this->trans_to
			] )
			-> andFilterWhere( [
				'or', [
					'manager_id'  => $this->manager_id,
				], [
					'like', 'contractors', $this->contractors
				]
			] );

        //order by date
        $query
			-> orderBy( [
            	'id' => SORT_DESC
        	] )
			-> indexBy( 'id' );

		//add conditions that should always apply here
		$dataProvider = new ActiveDataProvider( [
			'query' => $query
		] );

		//order validations
		if ( ! $this->validate() ) {
			return $dataProvider;
		}

        return $dataProvider;

    }

}