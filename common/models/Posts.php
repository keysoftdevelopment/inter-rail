<?php namespace common\models;

/********************************************/
/*                                          */
/*       POSTS, PAGES AND ETC MODEL         */
/*                                          */
/********************************************/

use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii2tech\ar\softdelete\SoftDeleteBehavior;

use common\behaviors\LoggingBehavior;

/**
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $seo
 * @property integer $file_id
 * @property integer $lang_id
 * @property string $type
 * @property string $slug
 * @property bool $isDeleted
 * @property string $updated_at
 * @property string $created_at
 *
 * @property CustomFields[] $customFields
 * @property TaxonomyRelation[] $postTaxonomies
 * @property Files $file
 * @property Languages $lang
**/
class Posts extends ActiveRecord
{

	/**
	 * @inheritdoc
	**/
	public $categories;

	/**
	 * @inheritdoc
	**/
	public $contact_info;

	/**
	 * @inheritdoc
	**/
	public $company;

	/**
	 * @inheritdoc
	**/
	public $icon;

	/**
	 * @inheritdoc
	**/
	protected $_post;

	/**
	 * Stores old attributes on afterFind() so we can compare
	 * against them before/after save
	**/
	protected $old_attributes;

	/**
	 * @inheritdoc
	**/
	public $slider;

	/**
	 * @inheritdoc
	**/
	public $statistics;

	/**
	 * @inheritdoc
	**/
	public $template;

    /**
     * @inheritdoc
    **/
    public static function tableName()
    {
        return '{{%posts}}';
    }

	/**
	 * @inheritdoc
	**/
	public function behaviors()
	{

		return [

			'softDeleteBehavior' => [
				'class'                     => SoftDeleteBehavior::className(),
				'softDeleteAttributeValues' => [
					'isDeleted' => true
				],
				'replaceRegularDelete' => true
			],

			'timestamp' => [
				'class' 	 => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => [
						'updated_at',
						'created_at'
					],
					ActiveRecord::EVENT_BEFORE_UPDATE => [
						'updated_at'
					]
				],
				'value' => function () {
					return date( 'Y-m-d H:i:s', strtotime( 'now' ) );
				}
			], [
				'class'        => SluggableBehavior::className(),
				'attribute'    => 'title',
				'ensureUnique' => true
			], [
				'class'  	 	=> LoggingBehavior::className(),
				'type'	  	 	=> 'posts',
				'foreign_id'    => 'id',
				'trace_options' => [
					'title',
					'description',
					'statistics',
					'slider',
					'template'
				]
			]

		];

	}

    /**
     * @inheritdoc
	**/
    public function rules()
    {

        return [
            [
            	[
            		'title'
				], 'required'
			], [
				[
					'description', 'seo', 'categories',
					'contact_info', 'company', 'statistics', 'icon'
				], 'string'
			], [
				[
					'file_id', 'lang_id', 'slider'
				], 'integer'
			], [
				[
					'updated_at', 'created_at'
				], 'safe'
			], [
				[
					'title', 'type', 'template'
				], 'string',
				'max' => 255
			], [
				[
					'lang_id'
				], 'exist',
				'skipOnError'     => true,
				'targetClass'     => Languages::className(),
				'targetAttribute' => [
					'lang_id' => 'id'
				]
			], [
				[
					'file_id'
				], 'exist',
				'skipOnError'     => true,
				'targetClass'     => Files::className(),
				'targetAttribute' => [
					'file_id' => 'id'
				]
			]
        ];

    }

    /**
     * @inheritdoc
	**/
    public function attributeLabels()
    {

        return [
            'id'          => __( 'ID' ),
            'title'       => __( 'Title' ),
            'description' => __( 'Description' ),
			'seo'		  => __( 'Seo' ),
            'file_id'     => __( 'File ID' ),
			'lang_id'     => __( 'Language ID' ),
            'type'  	  => __( 'Type' ),
			'updated_at'  => __( 'Updated At' ),
            'created_at'  => __( 'Created At' )
        ];

    }

    /**
     * @return \yii\db\ActiveQuery
	**/
    public function getPostTaxonomies()
    {

        return $this->hasMany( TaxonomyRelation::className(), [
            'foreign_id' => 'id'
        ] );

    }

    /**
     * @return \yii\db\ActiveQuery
    **/
    public function getLang()
    {

        return $this->hasOne( Languages::className(), [
            'id' => 'lang_id'
        ] );

    }

	/**
	 * @inheritdoc
	 * @param $post object
	 * @return boolean.
	**/
	public static function translate( $post )
	{

		//param initialization
		$translation = Translation::findOne( [
			'foreign_id' => $post->id,
			'lang_id'	 => $post->lang_id,
			'type'		 => $post->type
		] );

		//set translation details
		$translation = is_null( $translation )
			? merge_objects( new Translation(), [
				'title' 	  => $post->title,
				'description' => $post->description,
				'lang_id'    => $post->lang_id,
				'foreign_id' => $post->id,
				'seo' 		 => $post->seo,
				'type' 		 => $post->type
			] )
			: merge_objects( $translation, [
				'title' 	  => $post->title,
				'description' => $post->description,
				'seo'		  => $post->seo
			] );

		//queried params validations
		if ( ! $translation->save() )
			return false;

		//translation for additional attribute data
		foreach ( [
			'company',
			'statistics'
		] as $custom_field ) {

			if ( ! empty( $post->{ $custom_field } ) ) {

				//find out translation due to requested params
				$translation = Translation::findOne( [
					'foreign_id' => $post->id,
					'title'		 => $custom_field,
					'lang_id'	 => $post->lang_id,
					'type'		 => $post->type
				] );

				//re-assign requested post properties
				$translation = merge_objects( is_null( $translation )
					? new Translation()
					: $translation, [
						'lang_id'    => $post->lang_id,
						'foreign_id' => $post->id,
						'type' 		 => $post->type,
						'title'		 => $custom_field,
						'description' => $post->{ $custom_field }
				] );

				//saving translation
				if ( ! $translation->save() )
					return false;

			}

		}

		return true;

	}

	/**
	 * Fill in current model with custom fields
	**/
	public function customFields()
	{

		//filling current model with additional params
		foreach ( CustomFields::find()
			-> where( [
				'foreign_id' => $this->id,
				'name'		 => [
					'contact_info',
					'company',
					'slider',
					'statistics',
					'template',
					'icon'
				]
			] )
			-> indexBy( 'name' )
			-> all() as $key => $custom_field ) {

			if ( $key == 'company'
				&& ! empty( $custom_field->description )
			) {

				//set company details
				$company 	   = json_decode( $custom_field->description );
				$this->company = [
					'title' => isset( $company->title )
						? $company->title
						: '',
					'desc' => isset( $company->desc )
						? $company->desc
						: '',
					'file_id' => isset( $company->file_id )
						? $company->file_id
						: ''
				];

			} else if ( ! empty( $custom_field->description ) ) {
				$this->{$key} = $custom_field->description;
			}

		}

	}

	/**
	 * Fill in current model categories attribute with categories ids
	**/
	public function getTaxonomies()
	{

		//set category details
		$this->categories = json_encode( TaxonomyRelation::find()
			-> select( 'tax_id' )
			-> where( [
				'foreign_id' => $this->id,
				'type' 	     => 'posts'
			] )
			-> indexBy( 'tax_id' )
			-> column()
		);

	}

    /**
     * @return Posts
    **/
    public function getPosts()
    {

    	//set model with
        if ( $this->_post === null )
            $this->_post = Posts::findOne(
            	$this->id
			);

        return $this->_post;

    }

	/**
	 * @inheritdoc
	 * @return PostsQuery the active query used by this AR class.
	**/
	public static function find()
	{

		//param initialization
		$query = new PostsQuery(
			get_called_class()
		);

		return $query->where( [
			'isDeleted' => false
		] );

	}

	/**
	 * This method is called when the AR object is created and populated with the query result.
	 * The default implementation will trigger an [[EVENT_AFTER_FIND]] event.
	 * When overriding this method, make sure you call the parent implementation to ensure the
	 * event is triggered.
	**/
	public function afterFind()
	{

		//set attributes before updating them
		$this->old_attributes = $this->attributes;

		return parent::afterFind();

	}

	/**
	 * This method is called at the beginning of inserting or updating a record.
	 * The default implementation will trigger an [[EVENT_BEFORE_INSERT]] event when `$insert` is `true`,
	 * or an [[EVENT_BEFORE_UPDATE]] event if `$insert` is `false`.
	 * When overriding this method, make sure you call the parent implementation like the following:
	 * @param bool $insert whether this method called while inserting a record.
	 * If `false`, it means the method is called while updating a record.
	 * @return bool whether the insertion or updating should continue.
	 * If `false`, the insertion or updating will be cancelled.
	**/
    public function beforeSave( $insert )
    {

    	//param initialization
    	$response = false;

    	//update company and statistics attribute details
		if ( ! empty( $this->company )
			|| ! empty( $this->statistics )
		) {

			//convert statistics and company details from array to string
			foreach ( [ 'company', 'statistics' ] as $attribute ) {

				$this->{$attribute} = is_array( $this->{$attribute} )
					? json_encode( $this->{$attribute} )
					: $this->{$attribute};

			}

		}

        if ( $this->validate() ) {

        	//translations to requested model
			if ( ! is_null( $this->old_attributes ) && ! is_null( $this->old_attributes[ 'lang_id' ] )
				&& $this->old_attributes[ 'lang_id' ] != (int)$this->lang_id
			) {
				$this::translate( $this );
			} else {
				$response = true;
			}

        }

        return $response;

    }

	/**
	 * This method is called at the end of inserting or updating a record.
	 * The default implementation will trigger an [[EVENT_AFTER_INSERT]] event when `$insert` is `true`,
	 * or an [[EVENT_AFTER_UPDATE]] event if `$insert` is `false`. The event class used is [[AfterSaveEvent]].
	 * @param bool $insert
	 * @param array $changedAttributes The old values of attributes that had changed and were saved.
	**/
    public function afterSave( $insert, $changedAttributes )
    {

		parent::afterSave(
        	$insert,
			$changedAttributes
		);

		//taxonomy relations only for post type pages
		if ( $this->type == 'posts' ) {

			//remove all taxonomy relations
			TaxonomyRelation::deleteAll( [
				'foreign_id' => $this->id,
				'type'		 => 'posts'
			] );

			if ( ! empty(  $this->categories ) ) {

				foreach ( json_decode( stripslashes( $this->categories ) ) as $category_id ) {

					//taxonomy relations details
					$taxonomy = merge_objects( new TaxonomyRelation(), [
						'foreign_id' => $this->id,
						'tax_id'	 => $category_id,
						'type'		 => 'posts'
					] );

					//save taxonomy relations
					if ( $taxonomy->save() )
						continue;

				}

			}

		}

		//saving additional fields to current model
		foreach ( [
			'contact_info',
			'company',
			'slider',
			'statistics',
			'template',
			'icon'
		] as $attribute ) {

			if ( ! empty( $this->{$attribute} ) ) {

				//custom field details
				$custom_field = CustomFields::findOne( [
					'foreign_id' => $this->id,
					'name'	     => $attribute
				] );

				//assign properties for custom field
				if ( is_null( $custom_field ) ) {

					$custom_field = merge_objects( new CustomFields(), [
						'name' 		 => $attribute,
						'foreign_id' => $this->id,
						'type'		 => $this->type
					] );

				}

				//set custom field description
				$custom_field->description = $this->{$attribute};

				//save requested custom field
				$custom_field->save( false );

			}

		}

	}

}