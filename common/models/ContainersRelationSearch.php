<?php namespace common\models;

/********************************************/
/*                                          */
/*     CONTAINERS RELATION SEARCH MODEL     */
/*                                          */
/********************************************/

use yii\base\Model;
use yii\data\ActiveDataProvider;

class ContainersRelationSearch extends ContainersRelation
{

	/**
	 * @inheritdoc
	**/
	public function rules()
	{

		return [
			[
				[
					'foreign_id', 'container_id'
				], 'integer'
			], [
				[
					'type'
				], 'string'
			]
		];

	}

	/**
	 * @inheritdoc
	**/
	public function scenarios()
	{
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 * @return ActiveDataProvider
	**/
	public function search( $params )
	{

		//params initializations
		$query 		  = ContainersRelation::find();
		$dataProvider = new ActiveDataProvider( [
			'query' => $query,
		] );

		//loading requested params
		$this->load(
			$params
		);

		//queried params validation
		if ( !$this->validate() )
			return $dataProvider;

		//grid filtering conditions
		$query
			-> andFilterWhere( [
				'id'           => $this->id,
				'foreign_id'   => $this->foreign_id,
				'container_id' => $this->container_id
			] )
			-> andFilterWhere( [
				'like', 'type', $this->type
			] );

		return $dataProvider;

	}

}
