<?php namespace common\models;

/********************************************/
/*                                          */
/*         TERMINAL STORAGE MODEL           */
/*                                          */
/********************************************/

use yii\db\ActiveRecord;

/**
 * @property integer $id
 * @property integer $container_id
 * @property integer $order_id
 * @property integer $terminal_id
 * @property string $from
 * @property string $to
 *
 * @property Containers $container
 * @property Orders $order
 * @property Terminals $terminal
**/
class TerminalStorage extends ActiveRecord
{

	/**
	 * @inheritdoc
	**/
	public static function tableName()
	{
		return '{{%terminal_storage}}';
	}

	/**
	 * @inheritdoc
	**/
	public function rules()
	{

		return [
			[
				[
					'container_id', 'order_id', 'terminal_id'
				], 'required'
			], [
				[
					'container_id', 'order_id', 'terminal_id'
				], 'integer'
			], [
				[
					'from', 'to'
				], 'safe'
			], [
				[
					'container_id'
				], 'exist',
				'skipOnError'     => true,
				'targetClass'     => Containers::className(),
				'targetAttribute' => [
					'container_id' => 'id'
				]
			], [
				[
					'order_id'
				], 'exist',
				'skipOnError'     => true,
				'targetClass'     => Orders::className(),
				'targetAttribute' => [
					'order_id' => 'id'
				]
			], [
				[
					'terminal_id'
				], 'exist',
				'skipOnError'     => true,
				'targetClass'     => Terminals::className(),
				'targetAttribute' => [
					'terminal_id' => 'id'
				]
			]
		];

	}

	/**
	 * @inheritdoc
	**/
	public function attributeLabels()
	{

		return [
			'id' 		   => __( 'ID' ),
			'container_id' => __( 'Container ID' ),
			'order_id'	   => __( 'Order ID' ),
			'terminal_id'  => __( 'Order ID' ),
			'from'  	   => __( 'Rentals From' ),
			'to'  		   => __( 'Rentals To' )
		];

	}

	/**
	 * @return \yii\db\ActiveQuery
	**/
	public function getContainer()
	{

		return $this->hasOne( Containers::className(), [
			'id' => 'container_id'
		] );

	}

	/**
	 * @return \yii\db\ActiveQuery
	**/
	public function getOrder()
	{

		return $this->hasOne( Orders::className(), [
			'id' => 'order_id'
		] );

	}

	/**
	 * @return \yii\db\ActiveQuery
	**/
	public function getTerminal()
	{

		return $this->hasOne( Terminals::className(), [
			'id' => 'terminal_id'
		] );

	}

	/**
	 * This method is invoked before validation starts.
	 * The default implementation raises a `beforeValidate` event.
	 * You may override this method to do preliminary checks before validation.
	 * Make sure the parent implementation is invoked so that the event can be raised.
	 * @return bool whether the validation should be executed. Defaults to true.
	 * If false is returned, the validation will stop and the model is considered invalid.
	**/
	public function beforeValidate()
	{

		//set terminal storage dates dates
		foreach ( [ 'from', 'to' ] as $param )
			$this->{ $param } = date(
				'Y-m-d',
				strtotime( str_replace( '/', '-', $this->{ $param } ) )
			);

		return parent::beforeValidate();

	}

}