<?php namespace common\models;

/********************************************/
/*                                          */
/*              ORDERS MODEL                */
/*                                          */
/********************************************/

use Yii;

use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

use yii2tech\ar\softdelete\SoftDeleteBehavior;
use yii\behaviors\TimestampBehavior;

use common\behaviors\LoggingBehavior;

use common\commands\SendEmailCommand;

/**
 * @property integer $id
 * @property string $number
 * @property integer $user_id
 * @property integer $manager_id
 * @property integer $terminal_id
 * @property string $gu_number
 * @property integer $gu_file_id
 * @property string $contractors
 * @property string $weight_dimensions
 * @property string $packages
 * @property string $trans_from
 * @property string $trans_to
 * @property string $transportation
 * @property string $delivery_terms
 * @property string $consignee
 * @property string $shipper
 * @property integer $document_id
 * @property string $terminals
 * @property string $auto
 * @property bool $insurance
 * @property float $commission
 * @property float $zad
 * @property float $surcharge
 * @property float $total
 * @property string $status
 * @property string $notes
 * @property bool $isDeleted
 * @property string $updated_at
 * @property string $created_at
 *
 * @property User $user
**/
class Orders extends ActiveRecord
{

	/**
	 * @inheritdoc
	**/
	public $cargo_tax;

	/**
	 * @inheritdoc
	**/
	public $codes;

	/**
	 * @inheritdoc
	**/
	public $count;

	/**
	 * @inheritdoc
	**/
	public $containers_type;

	/**
	 * @inheritdoc
	**/
	public $expenses_ids;

	/**
	 * @inheritdoc
	**/
	public $invoices;

	/**
	 * @inheritdoc
	**/
	public $logistics;

	/**
	 * @inheritdoc
	**/
	public $manager_note;

	/**
	 * @inheritdoc
	**/
	public $dthc;

	/**
	 * @inheritdoc
	**/
	public $tracking;

	/**
	 * @inheritdoc
	**/
	public $wagons_type;

    /**
     * @inheritdoc
    **/
    public static function tableName()
    {
        return '{{%orders}}';
    }

    /**
     * @inheritdoc
    **/
    public function rules()
    {

        return [
			[
				[
					'packages', 'trans_from',
					'trans_to', 'user_id'
				], 'required'
			], [
				[
					'user_id', 'manager_id', 'terminal_id',
					'gu_file_id', 'document_id', 'cargo_tax',
					'dthc'
				], 'integer'
			], [
				[
					'commission', 'zad', 'surcharge', 'total',
					'insurance'
				], 'safe'
			], [
				[
					'number'
				], 'unique'
			], [
				[
					'number', 'containers_type', 'wagons_type', 'gu_number', 'contractors', 'weight_dimensions',
					'packages', 'manager_note', 'trans_from', 'trans_to', 'transportation',
					'consignee', 'shipper', 'status'
				], 'string',
				'max' => 255
			], [
				[
					'codes', 'expenses_ids', 'notes', 'terminals',
					'auto', 'tracking', 'delivery_terms', 'logistics'
				], 'string',
				'max' => 20000
			], [
				[
					'invoices'
				],	'exist',
				'skipOnError' => true,
				'allowArray'  => true,
				'when' 		  => function ( $model, $attribute ) {
					return is_array( $model->$attribute );
				}
			], [
				[
					'user_id'
				], 'exist',
				'skipOnError'     => true,
				'targetClass'     => User::className(),
				'targetAttribute' => [
					'user_id' => 'id'
				]
			], [
				[
					'terminal_id'
				], 'exist',
				'skipOnError'     => true,
				'targetClass'     => Terminals::className(),
				'targetAttribute' => [
					'terminal_id' => 'id'
				]
			], [
				[
					'gu_file_id', 'document_id'
				], 'exist',
				'skipOnError'     => true,
				'targetClass'     => Files::className(),
				'targetAttribute' => [
					'gu_file_id'  => 'id',
					'document_id' => 'id'
				]
			]
        ];

    }

	/**
	 * @inheritdoc
	**/
	public function behaviors()
	{

		return [

			'softDeleteBehavior' => [
				'class'                     => SoftDeleteBehavior::className(),
				'softDeleteAttributeValues' => [
					'isDeleted' => true
				],
				'replaceRegularDelete' => true
			],

			[
				'class'  	 	=> LoggingBehavior::className(),
				'type'	  	 	=> 'orders',
				'foreign_id'    => 'id',
				'trace_options' => [
					'number', 'manager_id', 'terminal_id',
					'gu_file_id', 'document_id', 'cargo_tax',
					'commission', 'zad', 'surcharge', 'total',
					'contractors', 'weight_dimensions', 'packages',
					'manager_note',	'transportation', 'codes', 'expenses_ids',
					'notes', 'terminals', 'auto', 'tracking'
				]
			]

		];

	}

    /**
     * @inheritdoc
    **/
    public function attributeLabels()
    {

        return [
			'id' 				=> __( 'ID' ),
			'number'			=> __( 'Number' ),
			'user_id' 		 	=> __( 'User ID' ),
			'manager_id' 		=> __( 'Manager ID' ),
			'terminal_id'		=> __( 'Terminal ID' ),
			'gu_number' 		=> __( 'GU Number' ),
			'gu_file_id' 		=> __( 'GU File ID' ),
			'contractors'		=> __( 'Contractors' ),
			'weight_dimensions' => __( 'Weight Dimensions' ),
			'packages'	     	=> __( 'Packages' ),
			'trans_from' 		=> __( 'Transportation From' ),
			'trans_to'   	    => __( 'Transportation To' ),
			'transportation'	=> __( 'Transportation Type' ),
			'delivery_terms'	=> __( 'Delivery Terms' ),
			'consignee'		    => __( 'Consignee' ),
			'shipper'		    => __( 'Shipper' ),
			'document_id' 		=> __( 'Document ID' ),
			'terminals'	    	=> __( 'Terminal Details' ),
			'auto'	     		=> __( 'Auto Details' ),
			'insurance' 		=> __( 'Insurance' ),
			'commission' 		=> __( 'Commission' ),
			'zad'			    => __( 'ZAD' ),
			'surcharge' 		=> __( 'Surcharge' ),
			'total' 			=> __( 'Total' ),
			'status' 			=> __( 'Status' ),
			'notes' 			=> __( 'Notes' ),
			'updated_at' 		=> __( 'Updated at' ),
			'created_at'		=> __( 'Created at' )
        ];

    }

    /**
     * validate order
     * @return bool
    **/
    public function order()
    {

    	//order validations
        if ( $this->validate() ) {
			return true;
		} else {
			return false;
		}

    }

    /**
     * @return \yii\db\ActiveQuery
    **/
    public function getUser()
    {

        return $this->hasOne( User::className(), [
            'id' => 'user_id'
        ] );

    }

	/**
	 * @inheritdoc
	**/
    public function fillModel()
	{

		//taxonomy relation lists for current model
		$taxonomy = TaxonomyRelation::find()
			-> where( [
				'foreign_id' => $this->id,
				'type' 	     => [
					'cargo_tax',
					'containers_type',
					'wagons_type'
				]
			] )
			-> indexBy( 'type' )
			-> asArray()
			-> all();

		//manager note
		$manager_note = UserCustomFields::findOne( [
			'user_id' => $this->manager_id,
			'name' 	  => 'noteForOrder' . $this->id
		] );

		//fill in current model with code details
		$this->codes = RailwayCodes::findAll( [
			'order_id' => $this->id
		] );

		//set note from manager
		$this->manager_note = ! empty( $manager_note )
			? $manager_note->description
			: $this->manager_note;

		//fill in model with appropriate details
		foreach ( [
			'terminals',
			'delivery_terms',
			'packages',
			'weight_dimensions',
			'cargo_tax',
			'containers_type',
			'wagons_type'
		] as $key => $value ) {

			$this->{ $value } = ! empty( $this->{$value} ) && ! in_array( $value, [ 'cargo_tax', 'containers_type', 'wagons_type' ] )
				? (array)json_decode( $this->{$value} )
				: ( isset( $taxonomy[ $value ] )
					? $taxonomy[ $value ][ 'tax_id' ]
					: $this->{$value}
				);

		}

	}

	/**
	 * The list of all tracking details for current model
	 * @return Tracking[]
	**/
	public function getTracking()
	{

		return Tracking::find()
			-> where( [
				'foreign_id' => $this->id
			] )
			-> asArray()
			-> all();

	}

	/**
	 * The list of logistics for current model
	**/
	public function getLogistics()
	{

		//set logistic details
		$this->logistics = array_reduce( LogisticsRelation::find()
			-> where( [
				'foreign_id' => $this->id
			] )
			-> asArray()
			-> all(), function( $response, $logistic ) {

				if ( $logistic[ 'type' ] == 'auto' ) {
					$response[ 'auto' ][] = $logistic[ 'logistic_id' ];
				} else {
					$response[ $logistic[ 'type' ] ] = $logistic[ 'logistic_id' ];
				}

			return $response;

		}, [] );

	}

	/**
	 * The list of containers for current model
	 * @return Containers[]
	**/
	public function getContainers()
	{

		//param initialization
		$container_ids = ContainersRelation::find()
			-> where( [
				'foreign_id' => $this->id,
				'type'		 => 'order'
			] )
			-> asArray()
			-> all();

		return ! empty( $container_ids )
			? Containers::find()
				-> where( [
					'id' => ArrayHelper::getColumn(
						$container_ids, 'container_id'
					),
					'isDeleted' => false
				] )
				-> asArray()
				-> all()
			: [];

	}

	/**
	 * The list of wagon numbers for current model
	 * @return Wagons[]
	**/
	public function getWagons()
	{

		//param initialization
		$wagon_ids = WagonsRelation::find()
			-> where( [
				'foreign_id' => $this->id,
				'type'		 => 'order'
			] )
			-> asArray()
			-> all();

		return ! empty( $wagon_ids )
			? Wagons::find()
				-> where( [
					'id' => ArrayHelper::getColumn(
						$wagon_ids, 'wagon_id'
					),
					'isDeleted' => false
				] )
				-> asArray()
				-> all()
			: [];

	}

    /**
     * @inheritdoc
     * @return OrdersQuery the active query used by this AR class.
    **/
    public static function find()
    {

    	//param initialization
        $query = new OrdersQuery(
        	get_called_class()
		);

        return $query->where( [
            'isDeleted' => false
        ] );

    }

    /**
     * @return array all users array with id as key and name as value
    **/
    public function getUsers()
    {

        return ArrayHelper::map(
			User::find()
				-> where( [
					'isDeleted' => false
				] )
				-> all(), 'id', 'username'
		);

    }

	/**
	 * This method is called at the beginning of inserting or updating a record.
	 * The default implementation will trigger an [[EVENT_BEFORE_INSERT]] event when `$insert` is `true`,
	 * or an [[EVENT_BEFORE_UPDATE]] event if `$insert` is `false`.
	 * When overriding this method, make sure you call the parent implementation like the following:
	 * @param bool $insert whether this method called while inserting a record.
	 * If `false`, it means the method is called while updating a record.
	 * @return bool whether the insertion or updating should continue.
	 * If `false`, the insertion or updating will be cancelled.
	**/
	public function beforeSave( $insert )
	{

		//imploding array of requested contractors
		if ( ! empty( $this->contractors )
			&& is_array( $this->contractors )
		) {

			$this->contractors = implode(
				',',
				$this->contractors
			);

		}

		//update created and updated fields
		if ( $insert ) {
			$this->created_at = $this->updated_at = date( 'Y-m-d H:i:s' );
		} else {
			$this->updated_at = date( 'Y-m-d H:i:s' );
		}

		//encoding attributes before model saving
		foreach ( [
			'auto',
			'codes',
			'delivery_terms',
			'terminals',
			'tracking',
			'weight_dimensions',
			'invoices',
			'logistics',
			'packages'
		] as $attribute ) {

			//is attribute exists in requests
			if ( ! empty( $this->{ $attribute } ) ) {

				$this->{$attribute} = is_array( $this->{$attribute} )
					? json_encode( $this->{$attribute} )
					: $this->{$attribute};

			}

		}

		//emptify expenses details
		if ( ! empty( $this->expenses_ids ) ) {

			Expenses::deleteAll( [
				'AND',
				'order_id = :order_id',
				[
					'NOT IN',
					'id',
					array_unique(
						$this->expenses_ids
					)
				]
			], [
				':order_id' => $this->id
			] );

			$this->expenses_ids = '';

		}

		//set boolean value
		if ( $this->insurance == 'on' ) {
			$this->insurance = true;
		}

		//set status
		if ( is_null( $this->status ) ) {
			$this->status = 'prequote';
		}

		if( $this->validate( false ) ) {
			return true;
		}

		return false;

	}

	/**
	 * This method is called at the end of inserting or updating a record.
	 * The default implementation will trigger an [[EVENT_AFTER_INSERT]] event when `$insert` is `true`,
	 * or an [[EVENT_AFTER_UPDATE]] event if `$insert` is `false`. The event class used is [[AfterSaveEvent]].
	 * @param bool $insert
	 * @param array $changedAttributes The old values of attributes that had changed and were saved.
	**/
    public function afterSave( $insert, $changedAttributes )
    {

        parent::afterSave(
        	$insert,
			$changedAttributes
		);

        //additional order attributes updating
        foreach ( [
        	'codes' => [
        		'class'  => new RailwayCodes(),
				'key'	 => 'route',
				'delete' => [
					'order_id' => $this->id
				]
			],
			'logistics' => [
				'class' => new LogisticsRelation(),
				'delete' => [
					'foreign_id' => $this->id,
					'type'		 => [
						'blockTrain',
						'train'
					]
				]
			],
			'tracking' => [
				'class'  => new Tracking(),
				'key'	 => 'name',
				'delete' => [
					'foreign_id' => $this->id
				]
			],
			'invoices' => [
				'class' => new Invoices(),
				'key'	=> 'amount'
			],
			'cargo_tax' => [
				'class'  => new TaxonomyRelation(),
				'delete' => [
					'foreign_id' => $this->id,
					'type'       => 'cargo_tax'
				]
			],
			'containers_type' => [
				'class' => new TaxonomyRelation()
			],
			'wagons_type' => [
				'class' => new TaxonomyRelation()
			]
		] as $param_key => $param ) {

        	//delete all records
			if ( ! in_array( $param_key, [
				'invoices',
				'containers_type',
				'wagons_type'
			] ) ) {

				$param[ 'class' ]::deleteAll(
					$param[ 'delete' ]
				);

			}

			if ( ! empty( $this->{ $param_key } ) ) {

				//find out the count of invoices or delete all records for requested param
				if ( $param_key == 'invoices' ) {

					$latest_invoice = Invoices::find()
						-> where( [
							'type' => 'Lotus'
						] )
						-> count();

				}

				//updating order attributes
				if ( in_array( $param_key, [
					'cargo_tax', 'containers_type', 'wagons_type'
				] ) ) {

					//taxonomy by requested taxonomy id
					$current_taxonomy = Taxonomies::findOne(
						$this->{ $param_key }
					);

					if ( ! is_null( $current_taxonomy ) ) {

						//taxonomy relation main properties
						$taxonomy = merge_objects( new TaxonomyRelation(), [
							'foreign_id' => $this->id,
							'tax_id'	 => $current_taxonomy->id,
							'type'		 => $param_key
						] );

						//saving taxonomy relations
						$taxonomy->save();

					}

				} else {

					//re-assign params
					$this->{ $param_key } = in_array( $param_key, [
						'codes',
						'tracking',
						'invoices',
						'logistics'
					] )
						? (array)json_decode( $this->{ $param_key } )
						: $this->{ $param_key };

					foreach ( in_array( $param_key, [ 'logistics' ] )
						|| ( $param_key == 'codes' && ! isset( $this->{ $param_key }[ $param[ 'key' ] ] ) )
						? $this->{ $param_key }
						: $this->{ $param_key }[ $param[ 'key' ] ] as $key => $option
					) {

						if ( $param_key == 'logistics' ) {

							if ( is_array( $option ) ) {

								foreach ( $option as $logistic_id ) {

									//logistics relation main properties
									$logistics = merge_objects( new LogisticsRelation(), [
										'foreign_id'  => $this->id,
										'logistic_id' => $logistic_id,
										'type' 		  => $key
									] );

									//saving logistics relations
									if ( $logistics->save() )
										continue;
								}

							} else {

								//logistics relation properties
								$properties = merge_objects( new LogisticsRelation(), [
									'foreign_id'  => $this->id,
									'logistic_id' => $option,
									'type' 		  => $key
								] );

							}

						} else {

							if ( $param_key == 'tracking' ) {

								//tracking main properties
								$properties = merge_objects( new Tracking(), [
									'name'  	  => $option,
									'description' => $this->tracking[ 'description'][ $key ],
									'arrival'	  => date( 'Y-m-d', strtotime(
										str_replace( [ '.', '/' ], '-', $this->tracking[ 'arrival'][ $key ] )
									) ),
									'departure'=> date( 'Y-m-d', strtotime(
										str_replace( [ '.', '/' ], '-', $this->tracking[ 'departure'][ $key ] )
									) ),
									'place_id'   => $this->tracking[ 'geo_id'][ $key ],
									'status'	 => $this->tracking[ 'status'][ $key ],
									'foreign_id' => $this->id
								] );

							} else if ( $param_key == 'invoices' ) {

								if ( ! empty( $option ) ) {

									//invoice details
									$invoice = ! empty( $this->invoices[ 'id' ][ $key ] )
										? Invoices::findOne( $this->invoices[ 'id' ][ $key ] )
										: new Invoices();

									//invoice main properties
									$properties = merge_objects( $invoice, [
										'number' => is_null( $invoice->number )
											? 'U000' . ( $latest_invoice + $key )
											: $invoice->number,
										'customer_id' => $this->user_id,
										'foreign_id'  => $this->id,
										'status'	  => $this->invoices[ 'status' ][ $key ],
										'amount'	  => $option,
										'date'		  => date( 'Y-m-d', strtotime(  str_replace( '/', '-' , $this->invoices[ 'date' ][ $key ] ) ) ),
										'type'		  => 'Lotus',
										'balance_due' => $invoice->balance_due > 0
											? (float)$invoice->balance_due - (float)$option
											: (float)$this->total - (float)$option
									] );

								}

							} else {

								//railway code main properties
								$properties = merge_objects( new RailwayCodes(), [
									'order_id'  	=> $this->id,
									'country'   	=> isset( $option->country ) ? $option->country : $this->codes[ 'country' ][ $key ],
									'route'	    	=> isset( $option->route ) ? $option->route : $option,
									'forwarder' 	=> isset( $option->route ) ? null : $this->codes[ 'forwarder' ][ $key ],
									'code'			=> isset( $option->route ) ? null : $this->codes[ 'code' ][ $key ],
									'code_rg_start' => isset( $option->route ) ? null : $this->codes[ 'code_rg_start' ][ $key ],
									'code_rg_end'   => isset( $option->route ) ? null : $this->codes[ 'code_rg_end' ][ $key ],
									'rate'			=> isset( $option->rate ) ? $option->rate : $this->codes[ 'rate' ][ $key ],
									'surcharge' 	=> isset( $option->surcharge ) ? $option->surcharge : $this->codes[ 'surcharge' ][ $key ]
								] );

							}

						}

						//order attributes updating
						if ( ! empty( $properties )
							&& ! is_array( $properties )
						) {
							$properties->save( false );
							continue;
						}

					}

				}
			}

		}

		if( ! empty( $this->manager_note ) ) {

			//manager note details
			$manager_note = UserCustomFields::findOne( [
				'user_id' => $this->manager_id,
				'name'	  => 'noteForOrder' . $this->id
			] );

			//set field name and assign user
			if ( is_null( $manager_note ) ) {

				$manager_note = merge_objects( new UserCustomFields(), [
					'user_id' => $this->manager_id,
					'name'	  => 'noteForOrder' . $this->id
				] );

			}

			//set description
			$manager_note->description = $this->manager_note;

			//save manager note
			$manager_note->save();

		}

		//order mail report
		$this->mailReport( [
			'is_insert' => $insert,
			'changes'   => $changedAttributes,
			'users'		=> User::find()
				-> select( [
					'id', 'email'
				] )
				-> where( [
					'id' => ArrayHelper::merge( Yii::$app->authManager->getUserIdsByRole( 'admin' ) , ArrayHelper::merge(
						ArrayHelper::merge(
							(array)$this->manager_id, (array)$this->user_id
						),
						explode( ',', $changedAttributes[ 'contractors' ] )
					) ),
					'isDeleted' => false
				] )
				-> asArray()
				-> indexBy( 'id' )
				-> all(),

		] );

    }

	/**
	 * Send Mail Reports due to custom order conditions
	 * I.E. Conditions like - new order, waiting
	 * confirmation from client and etc...
	 *
	 * @param array $params
	**/
	public function mailReport( $params )
	{

		//params initializations for current method
		$mail_reports = [];
		$order_number = ! empty( $this->number )
			? $this->number
			: $this->id;

		//mail report params
		$mail_params = [
			'id'	 => $this->id,
			'number' => $this->number,
			'from'	 => $this->trans_from,
			'to'	 => $this->trans_to,
			'note'	 => $this->manager_note
		];

		//manager and contractor mail details
		if ( ! empty( $this->manager_id )
			|| ! empty( $this->contractors )
		) {

			//mail notifications for order assign to manager
			if ( ( $params[ 'changes' ][ 'manager_id' ] != $this->manager_id
				&& isset( $params[ 'users' ][ $this->manager_id ] ) )
				|| ( strtolower( $params[ 'changes' ][ 'status' ] ) == 'confirmation'
				&& $this->consignee != $params[ 'changes' ][ 'consignee' ] )
			) {

				$mail_reports[] = [
					'to' 	  => $params[ 'users' ][ $this->manager_id ][ 'email' ],
					'subject' => ( $params[ 'changes' ][ 'manager_id' ] != $this->manager_id && isset( $params[ 'users' ][ $this->manager_id ] ) )
						? __( 'Order #' ) . $order_number
						: __( 'Order Confirmed #' ) . $order_number,
					'params' => [
						'order' => array_replace( $mail_params, [
							'status' => strtolower( $this->status ),
							'type'	 => 'manager'
						] )
					]
				];

			}

			//mail notification for order contractors
			if ( ! empty( $this->contractors )
				&& ! is_null( $params[ 'changes' ][ 'contractors' ] )
			) {

				$mail_reports = array_merge( $mail_reports, array_map( function( $contractor, $_contractor ) use ( $params, $mail_params, $order_number ) {

					return $contractor != $_contractor && isset( $params[ 'users' ][ $contractor ][ 'email' ] )
						? [
							'to' 	  => $params[ 'users' ][ $contractor ][ 'email' ],
							'subject' => __( 'Order #' ) . $order_number,
							'params'  => [
								'order' => array_replace( $mail_params, [
									'type'	 => 'contractor'
								] )
							]
						]
						: [];

				}, explode( ',', $this->contractors ), explode( ',', $params[ 'changes' ][ 'contractors' ] ) ) );

			}

		}

		//admin and client mail details
		if ( $params[ 'is_insert' ] || in_array( strtolower( $this->status ), [
			'confirmation', 'invoice', 'completion'
		] ) ) {

			//client mail details
			if ( $params[ 'is_insert' ]
				|| ! in_array( strtolower( $params[ 'changes' ][ 'status' ] ), [
					'confirmation', 'invoice', 'completion'
				] )
			) {

				//mail subject due to order status
				$subject = in_array( strtolower( $this->status ), [ 'confirmation', 'invoice' ] )
					? __( 'Waiting ' . strtolower( $this->status ) == 'confirmation'
						? __ ( 'Order Confirmation' )
						: __( 'Invoice For Order' ) .' #'
					) . $order_number
					: __( 'Request Order #' ) . $order_number;

				$mail_reports[] = [
					'to' 	  => $params[ 'users' ][ $this->user_id ][ 'email' ],
					'subject' => strtolower( $this->status ) == 'completion'
						? __( 'Order Completed' ) . ' #' . $order_number
						: $subject,
					'params' => [
						'order' => array_replace( $mail_params, [
							'status' => strtolower( $this->status ),
							'type'	 => 'client'
						] )
					]
				];

			}

			//admin user mail details
			if ( $params[ 'is_insert' ]
				|| strtolower( $this->status ) == 'completion'
			) {

				foreach ( Yii::$app->authManager->getUserIdsByRole( 'admin' ) as $admin ) {

					$mail_reports[] = [
						'to' 	  => $params[ 'users' ][ $admin ][ 'email' ],
						'subject' => strtolower( $this->status ) == 'completion'
							? __( 'Order Completed' ) . ' #' . $order_number
							: __( 'New Order Request' ) . ' #' . $order_number,
						'params' => [
							'order' => array_replace( $mail_params, [
								'status' => strtolower( $this->status ),
								'type'	 => 'admin'
							] )
						]
					];

				}

			}

		}

		//send mail notification to requested mail reports
		if ( ! empty( array_filter( $mail_reports ) ) ) {

			foreach ( $mail_reports as $mail_report ) {

				if ( ! empty( $mail_report ) ) {

					Yii::$app->commandBus->handle( new SendEmailCommand( [
						'to'      => $mail_report[ 'to' ],
						'subject' => $mail_report[ 'subject' ],
						'view'    => 'orders',
						'params'  => $mail_report[ 'params' ]
					] ) );

				}

			}

		}

	}

}