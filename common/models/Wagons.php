<?php namespace common\models;

/********************************************/
/*                                          */
/*     		    WAGONS MODEL         	    */
/*                                          */
/********************************************/

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii2tech\ar\softdelete\SoftDeleteBehavior;

use common\behaviors\LoggingBehavior;

/**
 * @property integer $id
 * @property string $number
 * @property string $owner
 * @property bool $isDeleted
 * @property string $updated_at
 * @property string $created_at
**/
class Wagons extends ActiveRecord
{

	/**
	 * @inheritdoc
	**/
	public $type;

	/**
	 * @inheritdoc
	**/
	public static function tableName()
	{
		return '{{%wagons}}';
	}

	/**
	 * @inheritdoc
	**/
	public function behaviors()
	{

		return [

			'softDeleteBehavior' => [
				'class'                     => SoftDeleteBehavior::className(),
				'softDeleteAttributeValues' => [
					'isDeleted' => true
				],
				'replaceRegularDelete' => true
			],

			'timestamp' => [
				'class' 	 => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => [
						'updated_at',
						'created_at'
					],
					ActiveRecord::EVENT_BEFORE_UPDATE => [
						'updated_at'
					]
				],
				'value' => function () {
					return date( 'Y-m-d H:i:s', strtotime( 'now' ) );
				}
			], [
				'class'  	 	=> LoggingBehavior::className(),
				'type'	  	 	=> 'wagons',
				'foreign_id'    => 'id',
				'trace_options' => [
					'number',
					'owner'
				]
			]

		];

	}

	/**
	 * @inheritdoc
	**/
	public function rules()
	{

		return [
			[
				[
					'number'
				], 'required'
			], [
				[
					'type'
				], 'integer'
			], [
				[
					'updated_at', 'created_at'
				], 'safe'
			], [
				[
					'number'
				], 'unique'
			], [
				[
					'number', 'owner'
				], 'string',
				'max' => 255
			]
		];

	}

	/**
	 * @inheritdoc
	**/
	public function attributeLabels()
	{

		return [
			'id'         => __( 'ID' ),
			'number'     => __( 'Number' ),
			'owner'      => __( 'Owner' ),
			'updated_at' => __( 'Updated At' ),
			'created_at' => __( 'Created At' )
		];

	}

	/**
	 * Fill inn current model with appropriate taxonomy
	**/
	public function fillType()
	{

		//params initializations
		$taxonomy = TaxonomyRelation::findOne( [
			'foreign_id' => $this->id,
			'type' 	     => 'wagons'
		] );

		//set type
		if ( ! is_null( $taxonomy ) ) {
			$this->type = $taxonomy->tax_id;
		}

	}

	/**
	 * @inheritdoc
	 * @return WagonsQuery the active query used by this AR class.
	**/
	public static function find()
	{

		//params initializations
		$query = new WagonsQuery(
			get_called_class()
		);

		return $query;

	}

	/**
	 * This method is called at the end of inserting or updating a record.
	 * The default implementation will trigger an [[EVENT_AFTER_INSERT]] event when `$insert` is `true`,
	 * or an [[EVENT_AFTER_UPDATE]] event if `$insert` is `false`. The event class used is [[AfterSaveEvent]].
	 * @param bool $insert
	 * @param array $changedAttributes The old values of attributes that had changed and were saved.
	**/
	public function afterSave( $insert, $changedAttributes )
	{

		if ( ! empty( $this->type ) ) {

			//delete all wagon type relations
			TaxonomyRelation::deleteAll( [
				'foreign_id' => $this->id,
				'type'       => 'wagons'
			] );

			//fill in taxonomy details with request details
			$taxonomy = merge_objects( new TaxonomyRelation(), [
				'foreign_id' => $this->id,
				'tax_id' 	 => $this->type,
				'type'	 	 => 'wagons'
			] );

			//save taxonomy relations
			$taxonomy->save();

		}

	}

}