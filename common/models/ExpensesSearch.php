<?php namespace common\models;

/********************************************/
/*                                          */
/*      	 EXPENSES SEARCH MODEL      	*/
/*                                          */
/********************************************/

use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

class ExpensesSearch extends Expenses
{

	/**
	 * @inheritdoc
	**/
	public function rules()
	{

		return [
			[
				[
					'order_id'
				], 'integer'
			]
		];

	}

	/**
	 * @inheritdoc
	**/
	public function scenarios()
	{
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 * @return ActiveDataProvider
	**/
	public function search( $params )
	{

		//params initializations
		$query 	      = Expenses::find();
		$dataProvider = new ActiveDataProvider( [
			'query' => $query,
		] );

		//loading requested params
		$this->load(
			$params
		);

		//queried params validation
		if ( !$this->validate() )
			return $dataProvider;

		//grid filtering conditions
		$query
			-> andFilterWhere( [
				'id'       => $this->id,
				'order_id' => ( isset( $params[ 'user_id' ] ) && ! empty( $params[ 'user_id' ] ) )
				|| ! empty( $params[ 'transportation' ] )
					? ArrayHelper::getColumn(
						! empty( $params[ 'user_id' ] )
							? Orders::find()
								-> where( [
									'user_id' => $params[ 'user_id' ]
								] )
								-> all()
							: Orders::find()->all(),
						'id'
					)
					: $this->order_id
			] );

		return $dataProvider;

	}

}