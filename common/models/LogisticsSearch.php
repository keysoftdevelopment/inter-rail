<?php namespace common\models;

/********************************************/
/*                                          */
/*          LOGISTICS SEARCH MODEL          */
/*                                          */
/********************************************/

use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

class LogisticsSearch extends Logistics
{

	/**
	 * @inheritdoc
	**/
	public function rules()
	{

		return [
			[
				[
					'name', 'type', 'owner', 'c_type',
					'transportation', 'from', 'to',
					'status', 'transport_no', 'transport_ind'
				], 'string'
			]
		];

	}

	/**
	 * @inheritdoc
	**/
	public function scenarios()
	{
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 * @return ActiveDataProvider
	**/
	public function search( $params )
	{

		//params initializations
		$ids		  = [];
		$query 		  = Logistics::find();
		$dataProvider = new ActiveDataProvider( [
			'query' => isset( $params[ 'type' ] )
				&& !empty( $params[ 'type' ] )
				? $query->where( [
					'type' => $params[ 'type' ]
				] )
				: $query
		] );

		//loading requested params
		$this->load(
			$params
		);

		//filtering block trains by transportation types
		if( $params[ 'type' ] == 'blockTrain'
			&& ! is_null( $this->transportation )
		) {

			//block train relations
			$relations = LogisticsRelation::find()
				-> where( [
					'type' => 'blockTrain'
				] )
				-> indexBy( 'foreign_id' )
				-> asArray()
				-> all();

			//the list of logistic ids
			$ids = array_unique( array_reduce( Orders::find()
				-> where( [
					'id' 			 => array_column( $relations, 'foreign_id' ),
					'transportation' => $this->transportation
				] )
				-> asArray()
				-> all(), function( $result, $order ) use( $relations ) {

				if( isset( $relations[ $order[ 'id' ] ] ) ) {
					$result[] = $relations[ $order[ 'id' ] ][ 'logistic_id' ];
				}

				return $result;

			}, [] ) );

		}

		//queried params validation
		if ( !$this->validate() ) {
			return $dataProvider;
		}

		//grid filtering conditions
		$query
			-> andFilterWhere( [
				'id'         	 => $this->id,
				'name'		 	 => $this->name,
				'from'		 	 => $this->from,
				'to'		 	 => $this->to,
				'c_type'	     => $this->c_type,
				'status'		 => $this->status,
				'transport_no'	 => $this->transport_no,
				'transport_ind'	 => $this->transport_ind,
				'type'			 => $this->type,
				'transportation' => $params[ 'type' ] != 'blockTrain' || empty( $ids )
					? $this->transportation
					: null,
			] )
			-> andFilterWhere( [
				'id' => isset( $params[ 'containers_type' ] ) && ! empty( $params[ 'containers_type' ] )
					? ArrayHelper::getColumn( TaxonomyRelation::findAll( [
						'tax_id' => $params[ 'containers_type' ],
						'type'	 => $params[ 'type' ]
					] ), 'foreign_id' )
					: $ids
			] )
			-> andFilterWhere( [
				'like', 'owner', $this->owner
			] );

		//order by date
		$query->orderBy( [
			'created_at' => SORT_DESC
		] );

		return $dataProvider;

	}

}
