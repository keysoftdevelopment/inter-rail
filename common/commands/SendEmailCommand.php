<?php namespace common\commands;

/**************************************/
/*                                    */
/*          SEND MAIL COMMAND         */
/*                                    */
/**************************************/

use Yii;

use yii\base\Object;

use trntv\bus\interfaces\SelfHandlingCommand;

use common\models\Settings;

class SendEmailCommand extends Object implements SelfHandlingCommand
{

    /**
     * @var mixed
    **/
    public $to;

    /**
     * @var string
    **/
    public $subject = '';

    /**
     * @var string
    **/
    public $view;

    /**
     * @var array
    **/
    public $params;

    /**
     * @var string
    **/
    public $body;

    /**
     * @var bool
    **/
    public $html = true;

	/**
	 * @var mixed
	**/
    public $replyTo;

    /**
     * @return bool
    **/
    public function isHtml()
    {
        return ( bool )$this->html;
    }

    /**
     * @param \common\commands\SendEmailCommand $command
     * @return bool
   	**/
    public function handle( $command )
    {

		//mail settings details
		$settings = Settings::find()
			-> select( [
				'name',
				'description'
			] )
			-> where( [
				'name' => [
					'from_name',
					'from_mail',
					'e_mail'
				]
			] )
			-> indexBy( 'name' )
			-> asArray()
			-> all();

		if ( in_array( '', $settings )
			&& in_array( null, $settings )
		) {
			return false;
		}

		//set mail html template
		$mail = \Yii::$app->mailer->compose(
			$command->view,
			$command->params
		);

		// attach file from local file system
		if ( $command->view == 'terminal' ) {

			$mail->attach(
				$command->params[ 'attachment' ][ 'link' ]
			);

		}

		//mail details
		$mail
			-> setFrom( [
				 $settings[ 'from_mail' ][ 'description' ] => $settings[ 'from_name' ][ 'description' ]
			] )
			-> setTo( ! empty( $command->view ) && in_array( $command->view, [ 'contact' ] )
				? $settings[ 'e_mail' ][ 'description' ]
				: $command->to
			)
			-> setSubject(
				$command->subject
			);

		//mail reply options
		if ( $this->replyTo ) {

			$mail->setReplyTo(
				$this->replyTo
			);

		}

		return $mail->send();

    }

}