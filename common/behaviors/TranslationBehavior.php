<?php namespace common\behaviors;

/**************************************/
/*                                    */
/*       TRANSLATION BEHAVIOR         */
/*                                    */
/**************************************/

use yii\base\ActionFilter;
use yii\helpers\ArrayHelper;

use common\models\Translation;

/**
 * Translation Behavior
**/
class TranslationBehavior extends ActionFilter
{

	/**
	 *
	 * @param $action
	 * @return bool
	**/
	public function beforeAction( $action )
	{

		return parent::beforeAction(
			$action
		);

	}

	/**
	 *
	 * @param $query
	 * @return Translation[]
	**/
	public function query( $query )
	{

		return Translation::find()
			-> select( [
				'foreign_id',
				'title',
				'description'
			] )
			-> where( $query )
			-> groupBy( [
				'foreign_id',
				'title'
			] )
			-> asArray()
			-> all();

	}

	/**
	 * Single model translations
	 * @param $models mixed
	 * @param $options array
	 * @return mixed
	**/
	public function translates( $models, $options )
	{

		if ( $options[ 'is_single' ] == false
			&& !empty( $models )
			|| $options[ 'is_single' ] == true
		) {

			//param initialization
			$translations = $this->query( [
				'foreign_id' => $options[ 'is_single' ] == false
					? ArrayHelper::getColumn(
						$models, 'id'
					)
					: $models->id,
				'lang_id' => $options[ 'lang_id' ],
				'type'    => $options[ 'type' ]
			] );

			//filter translation results
			$translation = array_reduce( $translations, function( $result, $translation ) {

				if ( ! empty( $translation ) )
					$result[ $translation[ 'foreign_id' ] ][] = $translation;

				return $result;
			}, [] );

			//set model translations
			if ( $options[ 'is_single' ] == false ) {

				foreach ( $models as $model ) {

					$this->translation(
						$translation,
						$model,
						$options[ 'type' ]
					);

				}

			} else {

				$this->translation(
					$translation,
					$models,
					$options[ 'type' ]
				);

			}

		}

		return $models;

	}

	/**
	 * Model translations
	 * @param $translations array
	 * @param $model mixed
	 * @param $type string
	 * @return mixed
	**/
	protected function translation( $translations, $model, $type = '' )
	{

		return isset( $translations[ $model->id ] )
			? (object)array_reduce( $translations[ $model->id ], function( $result, $translation ) use ( $model, $type ) {

				if( isset( $model->{ $translation[ 'title' ] } ) ) {
					$model->{ $translation[ 'title' ] } = $translation[ 'description' ];
				}

				if ( ! in_array( $translation[ 'title' ], [
					'company',
					'statistics'
				] ) ) {

					//set model title
					$model->{ $type === 'taxonomy'
						? 'name'
						: 'title'
					} = $translation[ 'title' ];

					//set model description
					$model->description = $translation[ 'description' ];

					//set model seo details
					if ( isset( $model->seo ) ) {
						$model->seo	= $translation[ 'seo' ];
					}

				} else {
					$model->{ $translation[ 'title' ] } = $translation[ 'description' ];
				}

				if ( ! empty( $model ) ) {
					$result[] = $model;
				}

				return $result;

			}, [] )
			: $model;

	}

	/**
	 * This method is invoked right after an action within this module is executed.
	 * @param $action
	 * @param mixed $result
	 * @return mixed
	**/
	public function afterAction( $action, $result )
	{

		return parent::afterAction(
			$action,
			$result
		);

	}

}