<?php namespace common\behaviors;

/**************************************/
/*                                    */
/*       LOGGING TRACE BEHAVIOR       */
/*                                    */
/**************************************/

use Yii;

use yii\base\Event;
use yii\behaviors\AttributeBehavior;
use yii\db\BaseActiveRecord;

use common\models\Logs;

/**
 * Logging Trace Behaviour under Logs model
**/
class LoggingBehavior extends AttributeBehavior
{

	/**
	 * @inheritdoc
	**/
	public $type;

	/**
	 * @inheritdoc
	**/
	public $foreign_id;

	/**
	 * @inheritdoc
	**/
	public $trace_options;

	/**
	 * @inheritdoc
	**/
	public function init()
	{

		parent::init();

		if ( empty( $this->attributes ) ) {

			$this->attributes = [
				BaseActiveRecord::EVENT_AFTER_VALIDATE => $this->foreign_id
			];

		}

	}

	/**
	 * Returns the value for the current attributes.
	 * This method is called by [[evaluateAttributes()]]. Its return value will be assigned
	 * to the attributes corresponding to the triggering event.
	 * @param Event $event the event that triggers the current attribute updating.
	 * @return mixed the attribute value
	**/
	protected function getValue( $event )
	{

		//param initialization
		$trace_option = array_reduce( $this->trace_options, function( $result, $action ) {

			if ( isset( $this->owner->{ $action } )
				&& $this->owner->{ $action } != $this->owner->oldAttributes[ $action ]
			) {

				$result[ $action ] = [
					'old' => $this->owner->oldAttributes[ $action ],
					'new' => $this->owner->{ $action }
				];

			}

			return $result;

		}, [] );

		//save changed logs information
		if ( ! empty( $trace_option ) ) {

			//log main properties
			$log = merge_objects( new Logs(), [
				'action'	 => json_encode( $trace_option ),
				'foreign_id' =>  $this->owner->{ $this->foreign_id },
				'changed_by' => Yii::$app->user->id,
				'type'		 => in_array( $this->type, [ 'logistics', 'posts', 'taxonomies' ] )
					? $this->owner->type
					: $this->type,
				'updated_at' => date( 'Y-m-d H:m', strtotime( 'today' ) ),
				'created_at' => date( 'Y-m-d H:m', strtotime( 'today' ) )
			] );

			//log details update
			if ( $log->save() ) {
				return $this->owner->{ $this->foreign_id };
			}

		}

		return $this->owner->{ $this->foreign_id };

	}

}