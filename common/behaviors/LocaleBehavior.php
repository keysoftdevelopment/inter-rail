<?php namespace common\behaviors;

/**************************************/
/*                                    */
/*         LANGUAGE BEHAVIOR          */
/*                                    */
/**************************************/

use Yii;

use yii\base\ActionFilter;

use common\models\Languages;

class LocaleBehavior extends ActionFilter
{

	/**
	 * Set system default language
	 * @param $action
	 * @return bool
	**/
	public function beforeAction( $action )
	{

		//params initializations
		$lang_name 		  = 'ru';
		$current_language = Yii::$app->common->getCookies( 'language' );

		if ( ! empty( $current_language ) ) {

			//language details
			$language = Languages::findOne(
				$current_language
			);

			//remove no need words from language details
			if ( !is_null( $language ) )
				$lang_name = str_replace( [
					'_EN',
					'_RU'
				],
					'',
					$language->name
				);

		}

		//set language
		Yii::$app->language = $lang_name;

		return parent::beforeAction( $action );

	}

	/**
	 * This method is invoked right after an action within this module is executed.
	 * @param $action
	 * @param mixed $result
	 * @return mixed
	**/
	public function afterAction( $action, $result )
	{

		return parent::afterAction(
			$action,
			$result
		);

	}

}